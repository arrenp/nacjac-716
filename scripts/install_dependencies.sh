if [ "$DEPLOYMENT_GROUP_NAME" == "Development" ]
then
    BUCKET_NAME="289872367244-qa-alb1-logs"
elif [ "$DEPLOYMENT_GROUP_NAME" == "Staging" ]
then
    BUCKET_NAME="289872367244-development-alb1-logs"
elif [ "$DEPLOYMENT_GROUP_NAME" == "Production" ]
then
    BUCKET_NAME="289872367244-production-alb1-logs"
fi
sudo aws s3 cp s3://${BUCKET_NAME}/${APPLICATION_NAME}/scripts/ /opt/codedeploy-agent/deployment-root/${DEPLOYMENT_GROUP_ID}/${DEPLOYMENT_ID}/deployment-archive/scripts/ --recursive
sudo aws s3 cp s3://${BUCKET_NAME}/sites-enabled/ /etc/httpd/sites-enabled/ --recursive