BSD Install Instructions
========================

1. `composer install`
2. `npm install`
3. `npm run encore -- dev`
    * For production: `npm run encore -- production`
    * To watch for changes and build: `npm run encore -- dev --watch`
   