<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('tcpdf/tcpdf.php');

class pdfBase extends TCPDF {
    //Page header
    public function Header() {
        // Logo
        $image_file1 = $this->image_file1;
        $image_file2 = $this->image_file2;
        $titleHeader = $this->titleHeader;
        $partnerNameHeader = $this->partnerNameHeader;
        $planNameHeader = $this->planNameHeader;
        $this->Image($image_file1, 10, 10, 35, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', 'B', 20);
        $this->Cell(120, 0, $titleHeader, 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->Ln();
        $this->SetFont('helvetica', 'B', 12);
        $this->Cell(0, 0, $partnerNameHeader, 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->Ln();
        $this->SetFont('helvetica', 'B', 10);
        $this->Cell(0, 0, $planNameHeader, 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->Image($image_file2, 165, 10, 35, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        // Title
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}