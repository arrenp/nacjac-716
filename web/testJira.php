<?php


class Jira
{

    protected $host;

    public function __construct(array $config = array())
    {
        $this->request = new RestRequest();
        $this->request->username = (isset($config['username'])) ? $config['username'] : null;
        $this->request->password = (isset($config['password'])) ? $config['password'] : null;
        $host = (isset($config['host'])) ? $config['host'] : null; 
        $this->host = 'http://' . $host . '/rest/api/2/';
        echo $this->host;
        
    }

    public function testLogin()
    {
        $user = $this->getUser($this->request->username);
        if (!empty($user) && $this->request->lastRequestStatus()) {
            return true;
        }

        return false;
    }

    public function getUser($username)
    {
        $this->request->openConnect($this->host . 'user/search/?username=' . $username, 'GET');
        $this->request->execute();
        $user = json_decode($this->request->getResponseBody());

        return $user;
    }

    public function getStatuses()
    {
        $this->request->openConnect($this->host . 'status', 'GET');
        $this->request->execute();
        $statuses = json_decode($this->request->getResponseBody());
        $returnStatuses = array();
        foreach ($statuses as $status) {
            $returnStatuses[$status->id] = $status->name;
        }

        return $returnStatuses;
    }

    public function getTransitions($issueKey)
    {
        $this->request->openConnect($this->host . 'issue/' . $issueKey . '/transitions', 'GET');
        $this->request->execute();
        if ($result = json_decode($this->request->getResponseBody())) {
            $returnTransitions = array();
            foreach ($result->transitions as $transition) {
                $returnTransitions[$transition->id] = $transition->name;
            }
            return $returnTransitions;
        }

        return false;
    }

    public function getChangelog($issueKey, $historyAsText = true)
    {
        $this->request->openConnect($this->host . 'issue/' . $issueKey . '/?expand=changelog', 'GET');
        $this->request->execute();
        if ($result = json_decode($this->request->getResponseBody())) {
            if (!isset($result->changelog)) {
                return false;
            }
            $changeLog = array();
            $histories = $result->changelog->histories;
            if ($historyAsText) {
                foreach ($histories as $history) {
                    $changeLog[$history->author->name] = array(
                        'Created:' => $history->created,
                        var_export($history->items, true)
                    );
                }
            } else {
                foreach ($histories as $history) {
                    $changeLog[$history->author->name] = array(
                        'Created:' => $history->created,
                        $history->items, true
                    );
                }
            }
            return $changeLog;
        }

        return false;
    }

    public function getComments($issueKey)
    {
        $this->request->openConnect($this->host . 'issue/' . $issueKey . '/comment?expand', 'GET');
        $this->request->execute();
        $result = json_decode($this->request->getResponseBody());
        if (isset($result->comments)) {
            return $result->comments;
        }

        return false;
    }

    public function queryIssue($query)
    {
        function createPairs($obj) {
            $str = "";
            foreach ($obj as $key => $value) {
                if ($key != 'jql') {
                    $str .= "$key=$value&";
                } else {
                    $str .= trim($value, '"\'@') . '&';
                }
            }
            return rtrim($str, '&');
        }
        $qs = createPairs($query);
        $qs = urlencode($qs);
        $this->request->OpenConnect($this->host . 'search?jql=' . $qs);
        $this->request->execute();
        $result = json_decode($this->request->getResponseBody());
        if (isset($result->issues)) {
            return $result->issues;
        }

        return false;
    }

    public function createIssue($json)
    {
        $this->request->openConnect($this->host . 'issue/', 'POST', $json);
        $this->request->execute();

        return $this->request->lastRequestStatus();
    }

    public function addAttachment($filename, $issueKey)
    {
        $this->request->openConnect($this->host . 'issue/' . $issueKey . '/attachments', 'POST', null, $filename);
        $this->request->execute();

        return $this->request->lastRequestStatus();
    }

    public function updateIssue($json, $issueKey)
    {
        $this->request->openConnect($this->host . 'issue/' . $issueKey, 'PUT', $json);
        $this->request->execute();

        return $this->request->lastRequestStatus();
    }

    public function transitionIssue($issue, $transitionId)
    {
        $transitionId = (int) $transitionId;
        $data = array('transition' => array('id' => $transitionId));
        $this->request->openConnect($this->host . 'issue/' . $issue . '/transitions', 'POST', $data);
        $this->request->execute();

        return $this->request->lastRequestStatus();
    }

    public function addComment($comment, $issueKey)
    {
        $newComment = array(
            "body"	=> $comment,
        );

        $this->request->openConnect($this->host . 'issue/' . $issueKey . '/comment', 'POST', $newComment);
        $this->request->execute();

        return $this->request->lastRequestStatus();
    }
}

class RestRequest
{

    public $username;
    public $password;

    protected $url;
    protected $verb;
    protected $requestBody;
    protected $requestLength;
    protected $acceptType;
    protected $responseBody;
    protected $responseInfo;

    public function openConnect($url = null, $verb = 'GET', $requestBody = null, $filename = null)
    {
        $this->url               = $url;
        $this->verb              = $verb;
        $this->requestBody       = $requestBody;
        $this->requestLength     = 0;
        $this->acceptType        = 'application/json';
        $this->responseBody      = null;
        $this->responseInfo      = null;
        $this->filename          = $filename;
        $this->contentType       = 'Content-Type: application/json';

        if ($this->requestBody !== null || $this->filename !== null)
            $this->buildPostBody();
    }

    public function flush()
    {
        $this->requestBody       = null;
        $this->requestLength     = 0;
        $this->verb              = 'GET';
        $this->responseBody      = null;
        $this->responseInfo      = null;
    }

    public function execute()
    {
        $ch = curl_init();
        $this->setAuth($ch);

        try {
            switch (strtoupper($this->verb)) {
                case 'GET':
                    $this->executeGet($ch);
                    break;
                case 'POST':
                    $this->executePost($ch);
                    break;
                case 'PUT':
                    $this->executePut($ch);
                    break;
                case 'DELETE':
                    $this->executeDelete($ch);
                    break;
                default:
                    throw new \InvalidArgumentException('Current verb (' . $this->verb . ') is an invalid REST verb.');
            }
        } catch (\InvalidArgumentException $e) {
            curl_close($ch);
            throw $e;
        } catch (\Exception $e) {
            curl_close($ch);
            throw $e;
        }
    }

    public function buildPostBody($data = null)
    {
        if ($data == null) {
            if ($this->filename !== null) {
                $fileContents = file_get_contents($this->filename);
                $boundary = "----------------------------".substr(md5(rand(0,32000)), 0, 12);

                $data = "--".$boundary."\r\n";
                $data .= "Content-Disposition: form-data; name=\"file\"; filename=\"".basename($this->filename)."\"\r\n";
                $data .= "Content-Type: ".mime_content_type($this->filename)."\r\n";
                $data .= "\r\n";
                $data .= $fileContents."\r\n";
                $data .= "--".$boundary."--";

                $this->requestBody = $data;
                $this->contentType = 'Content-Type: multipart/form-data; boundary='.$boundary;
            }
            else
                $this->requestBody = json_encode($this->requestBody);
        }
        else
            $this->requestBody = json_encode($data);
    }

    public function getResponseBody()
    {
        return $this->responseBody;
    }

    public function getResponseInfo()
    {
        return $this->responseInfo;
    }

    public function lastRequestStatus()
    {
        $result = $this->getResponseInfo();

        if (isset($result['http_code']) && ($result['http_code'] >= 200 && $result['http_code'] < 300)) {
            return true;
        }

        return false;
    }

    protected function executeGet($ch)
    {
        $this->doExecute($ch);
    }

    protected function executePost($ch)
    {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestBody);

        $this->doExecute($ch);
    }

    protected function executePut($ch)
    {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestBody);

        $this->doExecute($ch);
    }

    protected function executeDelete($ch)
    {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        $this->doExecute($ch);
    }

    protected function doExecute(&$ch)
    {
        $this->setCurlOpts($ch);
        //var_dump($_SESSION);
        //unset($_SESSION);
        $this->responseBody = curl_exec($ch);
        $this->responseInfo = curl_getinfo($ch);
        curl_close($ch);
    }

    protected function setCurlOpts(&$ch)
    {
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        //curl_setopt($ch, CURLOPT_HEADER, true); //displays header in output.
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Accept: ' . $this->acceptType,
                $this->contentType, 'X-Atlassian-Token: nocheck'
            )
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // ignore self signed certs
        //curl_setopt($ch, CURLOPT_VERBOSE, true); // set to true for CURL debug output
    }

    protected function setAuth(&$ch)
    {
        if ($this->username !== null && $this->password !== null) {
            curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
        }
    }
}

$config['host'] = "jira.smartplanenterprise.com:8080";
$config['username'] = "nrobertson";
$config['password'] = "jira1*";

$jira = new Jira($config);
echo $jira->testLogin();

?>