define(['jquery', 'service/data', 'spe/router', 'jwplayer', 'service/playerService', 'view/PlayerView','spe/model/Section'], function ($, service, router, jwplayer, playerService, player,Section) {
    'use strict';

	function logXmlRaw() {
		$.get("/getXmlRaw", function(xmlRawData){
			$.each(xmlRawData, function(key,value) {
				if (typeof console.groupCollapsed === "function") {
					console.groupCollapsed("API Call " + value.id );
				}
				console.log("profileUid: " + value.profileUid);
				console.log("xmlUid: " + value.xmlUid);
				console.log("profileId: " + value.profileId);
				console.log("userid: " + value.userid);
				console.log("planid: " + value.planid);
				console.log("connection: " + value.connection);
				console.log("action: " + value.action);
				console.log("timestamp: " + value.timestamp);
				console.log(value.data);
				if (typeof console.groupEnd === "function") {
					console.groupEnd();
				}
			});
		}, "json");
	}

	function UpdateQueryString(key, value, url) {        
		if (!url) url = window.location.href;
		var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
			hash;

		if (re.test(url)) {
			if (typeof value !== 'undefined' && value !== null)
				return url.replace(re, '$1' + key + "=" + value + '$2$3');
			else {
				hash = url.split('#');
				url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
				if (typeof hash[1] !== 'undefined' && hash[1] !== null) 
					url += '#' + hash[1];
				return url;
			}
		}
		else {
			if (typeof value !== 'undefined' && value !== null) {
				var separator = url.indexOf('?') !== -1 ? '&' : '?';
				hash = url.split('#');
				url = hash[0] + separator + key + '=' + value;
				if (typeof hash[1] !== 'undefined' && hash[1] !== null) 
					url += '#' + hash[1];
				return url;
			}
			else
				return url;
		}
	}

    var $container = $("body"),
            bindEvent = function () {
                $container.on('change', '.language-switch', function() {
					window.location = UpdateQueryString('lang', $('.language-switch').val(),Section.getData("originalUrl"));
					//window.location.reload();
				});

                //JS: APP HEIGHT BASED ON IFRAME MAX HEIGHT
                /*
                 $(window).on('resize', function(){
                 var height = Math.max($("#spe-container").outerHeight() - ($("#header").parent().outerHeight() + $(".navbar-header").outerHeight() + $("#footer").outerHeight() + $("#bottom-logos").parent().outerHeight()),200);
                 var cHeight = $(".disclosure").is(":visible") ? (height - $(".disclosure").outerHeight()) : '100%';
                 $("#container-main").css({height: height, minHeight: height});
                 $(".content-area").css({height: cHeight, minHeight: cHeight});
                 });
                 */
            },
            initialize = function () {
                service.loadData().then(function (view, status, xhr) {
                    service.loadStyle().then(function (response) {
                        var style = document.createElement('style');
                        style.appendChild(document.createTextNode(response));
                        document.head.appendChild(style);

                        if ($('.sf-toolbarreset').length) {
                            if (xhr.getResponseHeader('x-debug-token')) {
                                $('.sf-toolbar, .sf-toolbarreset').remove();
                                $.get('/_wdt/' + xhr.getResponseHeader('x-debug-token'), function (data) {
                                    $('body').append(data);
                                    var icon = '<img width="22" height="28" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAcCAMAAABS8b9vAAAAhFBMVEUAAAA+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj5YeLP+AAAAK3RSTlMAq+u84ec+l8Gzabp+TtaHe/jux451cWJbNygVyq6mn5xVSSQcDPLJMx4OVQqmlgAAALpJREFUKM/N0McSgjAUheEgckliQu/YAdt5//dzpKkIe//NnfnmLDJhZ2c9ZmQl69vhHd/zpmfjgzc2wnqWwR+zDF5M2Xwx1uV0fWzP9T5hr7sWfXEgPeV5SrnZyP2uy+h4Gp9nc5mDs0Bm7LUApEQihrVbRYWfMMKWVcjtgUEshCRCnN9Ax4F9akyksRvpqAAdBpZ0KC+pdFi8q5ErJ2h5pbc4Cdv2tROmUPokFh74Vyzm/zuxVj9Z7hOr80lre/WrCwAAAABJRU5ErkJggg==" alt="XML"/>';
                                    $('.sf-toolbarreset').append('<div class="sf-toolbar-block rkp"><div class="sf-toolbar-icon"><span>' + icon + '</span><div><div class="sf-toolbar-info"></div></div>');
                                });
                            }
                        }
                        
                        $container.html(view);
                        $container.find('#spe-container').fadeIn(300);
                        $(window).resize();
                        router.init();
                        


                    });
                });
				window.logXmlRaw = logXmlRaw;
            };

    bindEvent();

    $( document ).ready(function() 
    {
        $(window).bind('beforeunload', function ()
        { 
            saveStats();
        });  
        $(document).on('click', ".jwnext", function(){
           saveStats();
        });
        $(document).on('click', ".jwprev", function(){
           saveStats();
        });
        $(document).on('click', "#mainNav li a", function(){
           saveStats();
        });            
    });
    
    function saveStats()
    {
        var percentageViewed = (player.videoPlayer().getPosition() / player.videoPlayer().getDuration()) * 100;
        var splitVideoPlayed = $("#vwise-video_media video").attr("src").split("/");
        var videoPlayed = splitVideoPlayed[splitVideoPlayed.length - 1];
        var querystring = "playDuration=" + player.videoPlayer().getPosition() + "&percentageViewed=" + percentageViewed + "&videoLength=" + player.videoPlayer().getDuration() + "&videoPlayed=" + videoPlayed;
        var page = "/postVideoStats";
        $.post(page, querystring).done(function (data)
        {

        });
    }



    return {
        initialize: initialize
    }
});
