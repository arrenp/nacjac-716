define(['jquery', 'jwplayer','service/playerService' ], function($, jw, playerService ){
    "use strict";


    var player = {
            videoPlayer: function(){
                
                return jwplayer("vwise-video");
            },
            initialize : function(){
                jw.key="XkrCEtJjpENgHzaccwWpBjoFwSongLK34QgYqsXxD8g=";
                this.cacheElement();
                this.bindEvents();
            },
            bindEvents : function(){
                $("body").on('click', 'a.audioPanel', this.controlAudio.bind(this));
                $("body").on('click', '.jwnext', this.nextSection.bind(this));
            },
            cacheElement : function(){
                this.$videoTemplate = $("#video-player-template");
                this.$audioTemplate = $("#audio-player-template");
            },
            controlAudio : function(e){
                if (e.timeStamp != $("#audioTimeStamp").val())
                {
                    var current = $(e.currentTarget).attr('rel');
                    if (jw('vwise-audio').getPlaylistIndex() == current){
                        jw('vwise-audio').play();
                    }else{
                        jw('vwise-audio').playlistItem(current);
                    }
                    $("#audioTimeStamp").val(e.timeStamp);
                }
                return false;
            },
            nextSection: function(e){
                var $instance = jwplayer("vwise-video");
                    if($instance.getPlaylistIndex() == 0){
                        $instance.remove();
                        $('body').trigger("spe:router:change", 'planBasics')
                    }
            },
            createPlayer : function(data, type){
                var playerId = type == 'audio' ? 'vwise-audio' : 'vwise-video';
                var config = playerService.buildConfig(data, type);
                config.autostart = 0;
				config.displaytitle = false;

                if (playerId == "vwise-video")
                {
					jw(playerId).setup(config);

					var userAgent = navigator.userAgent || navigator.vendor || window.opera;
					if( !userAgent.match( /iPad/i ) && !userAgent.match( /iPhone/i ) && !userAgent.match( /iPod/i ) && !userAgent.match(/Android/i) ) {
                        jw(playerId).play();
                        
                        // Fix for Chrome when autoplay is disallowed/prevented but video still shows a spinner
                        // https://developers.google.com/web/updates/2017/09/autoplay-policy-changes
                        var counter = 0;
                        var interval = setInterval(function() { 
                            var videoEl = $('#' + playerId + ' video');
                            if (videoEl.length && videoEl[0].paused) {
                                jw(playerId).pause();
                            }
                            if (counter++ >= 1) {
                                clearInterval(interval);
                            }
                        }, 1000);
					}

                    jw(playerId).onComplete( function()
                    {
                        var playerstats = jwplayer("vwise-video");
                        var percentageViewed = (playerstats.getPosition()/playerstats.getDuration()) * 100;
                        var splitVideoPlayed = $("#vwise-video_media video").attr("src").split("/");
                        var videoPlayed = splitVideoPlayed[splitVideoPlayed.length-1];
                        var querystring = "playDuration=" +  playerstats.getPosition() + "&percentageViewed=" +  percentageViewed + "&videoLength=" + playerstats.getDuration() + "&videoPlayed=" + videoPlayed;
                        var page = "/postVideoStats";
                        $.post(page, querystring).done(function(data) 
                        {

                        });
                    });
                    
                    // This is a fix for IE in IE9 emulation mode showing black bars (BSD-1069)
                    function applyTransformFixForIE() {
                        var scaleX = $('#vwise-video_media').width() / $('#vwise-video_media video').width();
                        var scaleY = $('#vwise-video_media').height() / $('#vwise-video_media video').height();
                        $('#vwise-video_media video').css('-ms-transform','scale('+scaleX+','+scaleY+')');
                    } 
                    jw(playerId).onResize(applyTransformFixForIE);
                    jw(playerId).onReady(function() {setTimeout(applyTransformFixForIE,1000);});
                }

				/* Temporary hack for iphone users */
                if (playerId == 'vwise-audio') {
                        $('audio.vwise-audio-player').remove();
						//$('a.audioPanel .audio-state').removeClass().addClass('audio-state audio-play');

                        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
						var current = 0;
						var playing = false;

                        for (var i = 0; i < config.playlist.length; i++) {
							var audio_config = {
									class : 'vwise-audio-player'
							};

							if( i == 0 && !userAgent.match( /iPad/i ) && !userAgent.match( /iPhone/i ) && !userAgent.match( /iPod/i ) && !userAgent.match(/Android/i) ) {
                                audio_config.autoPlay = 'autoplay';
								playing = true;
							}
							else {
								$('a.audioPanel[rel=' + current + '] .audio-state').removeClass().addClass('audio-state audio-play');
							}

							var audio = $('<audio  />', audio_config);

							for(var j = 0; j < config.playlist[i].sources.length; j++) {
									var file = config.playlist[i].sources[j].file;
									$('<source />').attr('src', file).appendTo(audio);
							}
							$('a.audioPanel[rel=' + i + ']').append(audio);
                        }

                        $('a.audioPanel').on('click', function(e) {
							$('a.audioPanel[rel=' + current + '] .audio-state').removeClass().addClass('audio-state audio-play');
							$('a.audioPanel[rel=' + current + '] audio.vwise-audio-player')[0].pause();

							if (current != $(this).attr('rel') || playing == false) {
								$(this).find('.audio-state').removeClass().addClass('audio-state audio-pause');
								$(this).find('audio.vwise-audio-player')[0].play();

								current = $(this).attr('rel');
								playing = true;
							}
							else {
								playing = false;
							}
                        });
                }
                /* End hack */
            },

            render : function(section, data, type){

                $(".jwplayer").each(function(i, item){
                    if($(item).attr('id') == 'vwise-video') {
                        jw(item).remove();
                    }else{
                        jw(item).stop();
                    }
                });

                //build playerMarkup
                var $el = type == 'audio' ? this.$audioTemplate : this.$videoTemplate,
                    tpl = _.template($el.html());

                    //attaching html to dom
                    $(section).html(tpl).removeClass('text');

                    //init Player & catch instance vars
                    this.createPlayer(data, type);

                    if(type == 'audio'){ $("a.audioPanel").slideDown(); }
            },
            play : function(playlist, type,callback){
                if(playlist && type){
                    var container = type == 'audio' ? '.audio-area' : '.content-area';
                    var params = { 'playlist' : [playlist] };
                    if (callback)
                        params.callback = callback;
                    var data = _.extend(JSON.parse(localStorage.getItem('mediaPath')), params);                    
                    this.render(container, data, type);
                }
            },
            completed : function() {
                    console.log('hello');
                
            }
            
        };

        player.initialize();

   return {
       render : player.render.bind(player),
       play : player.play.bind(player),
       completed : player.completed.bind(player),
       videoPlayer: player.videoPlayer.bind(player)
   }
});
