define(['jquery', 'underscore' ], function($, _ ){

    var el = "disclaimer-template",
        $el = function(){
            return $("#"+ el);
        },
        render = function(section){
            var tpl = _.template($el().html());
            $(section).addClass('text').html(tpl);
        };

    return {
        render : render
    }
});