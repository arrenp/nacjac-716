define(['jquery', 'underscore', 'jquery.mousewheel', 'jquery.customscroll'], function($, _, mousewheel, customScroll){

    var LibraryView  =  {
        templateName : 'library-index-template',
        initialize: function(){
            this.cacheElement();
            this.bindEvents();

        },
        cacheElement: function(){
            this.$container = $('body');
            this.$section = $('.library-container');
            this.$list = $(".library-list");
            this.$template = $("#" + this.templateName);
        },
        bindEvents : function(){
            this.$container.on('click', '.library-list a', this.handleList.bind(this));
            this.$container.on('click', '.toggle-playlist', this.togglePlaylist.bind(this));
        },
        handleList : function(e){
            var $target = $(e.target);

            $.publish("spe:library:index", {
                'video' : $target.data('video'),
                'dir' : $target.data('dir')
            });
        },
        togglePlaylist: function(e){
            var wrapper = $(e.currentTarget).parents('#library_container');
            if(wrapper.hasClass('max')){
                wrapper.removeClass('max').addClass('min');
            }else{
                wrapper.removeClass('min').addClass('max');
            }
        },
        createListTree : function(){
             $(".library-list li.category").tree();
        },
        initListScroll : function(){
            $("#library_container_inner").mCustomScrollbar();
        },
        render : function(section, model){
            var tpl = _.template(this.$template.html())({ model : model });
            $(section).html(tpl).addClass('text');
            this.initListScroll();
            this.createListTree();
        }
    };

    LibraryView.initialize();

    return {
        render: LibraryView.render.bind(LibraryView)
    };
});