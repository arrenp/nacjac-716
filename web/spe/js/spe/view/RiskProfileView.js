define(['jquery', 'underscore', 'spe/model/Section', 'spe/service/common'], function($, _, Section, common){

    var hasBind = false;

    var RiskProfileView  =  {
            getTemplate : function(actionName){
                return $("#risk-profile-"+ actionName).html();
            },
            initialize: function(){
                if(!hasBind){
                    this.bindEvents();
                    hasBind = true;
                }
            },
            bindEvents : function(){
                this.$container = $('body');
                this.$container.on('click', '.rp-container .back', this.handleNavs.bind(this));
                this.$container.on('click', '.rp-container .proceed',this.handleNavs.bind(this));
                this.$container.on('click', '.rp-container .start',this.callAction.bind(this, 'index'));
                this.$container.on('click', '.rp-container .finish',this.callAction.bind(this, 'finish'));
                this.$container.on('click', '.rp-advice .advice-option',this.callAction.bind(this, 'advice'));
                this.$container.on('click', '.rp-disclaimer .response',this.callAction.bind(this, 'disclaimer'));
                this.$container.on('click', '.rp-container .check_proceed',this.updatePersonalInvestorProfileDisclaimerButton.bind(this));
                this.$container.on('click', '.rp-container .personalInvestorProfileDisclaimerButton',this.personalInvestorProfileDisclaimerRemove.bind(this));
                this.$container.on('click', '.rp-container .backManagedAccount', this.callAction.bind(this, 'backManagedAccount'));
                this.$container.on('click', '.rp-container .proceedManagedAccount', this.callAction.bind(this, 'proceedManagedAccount'));
                this.$container.on('click', '.rp-container .proceedRetireExpress', this.callAction.bind(this, 'proceedRetireExpress'));
            },
            personalInvestorProfileDisclaimerRemove: function (){
                if ($('body .rp-container .check_proceed').is(":checked")){
                    $('#personalInvestorProfileDisclaimer').remove();
                    $.publish("spe:riskProfile:"+ "index", { do : "index"});                    
                }
            },
            updatePersonalInvestorProfileDisclaimerButton: function (){
                if ($('body .rp-container .check_proceed').is(":checked"))
                {
                    enableButton(".personalInvestorProfileDisclaimerButton");                  
                }
                else
                {
                    disableButton(".personalInvestorProfileDisclaimerButton");
                }
            },
            handleNavs : function(e){
                this.$section = $('.content-area');
                var selected = this.$section.find('input:checked'),
                    translation = Section.getData('translation');

                if(selected.length || $($(e.target)[0].nextSibling.parentElement).hasClass('back')){
                    var data = {
                            action : $($(e.target)[0].nextSibling.parentElement).hasClass('back') ? 'back' : 'next',
                            selected : selected.val()
                        };
                    $.publish("spe:riskProfile:index", data);
                }else{
                    common.showErrMsg(translation.please_select_option);
                }
            },
            callAction: function(actionName, e){
                $.publish("spe:riskProfile:"+ actionName, { do : actionName, target: e.target });
            },
            render : function(section, model, templateName){
                var html = this.getTemplate(templateName || 'index');
                var tpl = _.template(html)({ model : model });
                $(section).html(tpl).addClass('text');
            }
        };

        RiskProfileView.initialize();

    return {
        render: RiskProfileView.render.bind(RiskProfileView)
    };
});
