define([
    'jquery',
    'spe/service/common',
    'spe/service/contributionService',
    'spe/model/Section'
], function($, common, contributionService, section){

    var ContributionsView = {
        tpl : section.getData('common').autoNavigation ? 'contributions-tpl-auto': 'contributions-tpl',
        preSlider : {},
        rothSlider : {},
        postSlider : {},
        tplEl : function(){
            return $('#' + this.tpl);
        },
        cacheElements : function(){
            this.slider1 = $('#slider1');
            this.slider2 = $('#slider2');
            this.slider3 = $('#slider3');
            this.contributionTypePct = $('#contribution_type_pct');
            this.contributionTypeDlr = $('#contribution_type_dlr');
            this.totalSalary = $('#salary_total_value');
            this.totalRoth = $('#roth_total_value');
            this.totalPost = $('#post_total_value');
            this.pctSalary = $('#salary_total_pct');
            this.pctRoth = $('#roth_total_pct');
            this.pctPost = $('#post_total_pct');
            this.yearlyPay = $('#yearly_pay');
            this.getPaid = $('#get_paid');
            this.noticeModal = $('#contributionNotice');
            this.warningModal = $('#contributionWarning');
            this.acaMessageModal = $('#acaMessage');
            this.dollarOptOutModal = $('#dollarOptOutModal');
        },
        bindEvents : function(){
            this.contributionTypePct.on('click', contributionService.switchToPct.bind(this));
            this.contributionTypeDlr.on('click', contributionService.switchToDlrWithOptOutWarning.bind(this));
            this.totalSalary.on('change keyup', contributionService.calculateContribution.bind(this));
            this.totalRoth.on('change keyup', contributionService.calculateContribution.bind(this));
            this.totalPost.on('change keyup', contributionService.calculateContribution.bind(this));
            this.pctSalary.on('change', contributionService.totalPctSalary.bind(this));
            this.pctRoth.on('change', contributionService.totalPctRoth.bind(this));
            this.pctPost.on('change', contributionService.totalPctPost.bind(this));
            this.yearlyPay.on('change keyup', contributionService.totalContribution.bind(this));
            this.getPaid.on('change keyup', contributionService.totalContribution.bind(this));
            this.noticeModal.find('.action').on('click', this.modalAction.bind(this));
            this.warningModal.find('.action').on('click', this.modalAction.bind(this));
            this.acaMessageModal.find('.action').on('click', this.modalAction.bind(this));
            this.dollarOptOutModal.find('.action').on('click', this.dollarOptOutModalAction.bind(this));
        },
        bindACAMessage : function(){
            this.pctSalary.bind("keyup paste mouseup", contributionService.showACAMessage.bind(this));
            this.pctRoth.bind("keyup paste mouseup", contributionService.showACAMessage.bind(this));
            this.pctPost.bind("keyup paste mouseup", contributionService.showACAMessage.bind(this));
            this.slider1.bind('mouseup', contributionService.showACAMessage.bind(this));
            this.slider2.bind('mouseup', contributionService.showACAMessage.bind(this));
            this.slider3.bind('mouseup', contributionService.showACAMessage.bind(this));
        },
        render : function(section, data){
            data.priceFormat = common.formatCurrency;
            var tpl = _.template(this.tplEl().html())( { model : data });
            $(section).addClass('text').html(tpl);
            this.preSlider = contributionService.initSlider('slider1', data.commonData.preCurrent, data.commonData.preMin, data.commonData.preMax, data.commonData.prePctPre, 'salary');
            this.rothSlider = contributionService.initSlider('slider2', data.commonData.rothCurrent, data.commonData.rothMin, data.commonData.rothMax, data.commonData.rothPctPre, 'roth');
            this.postSlider = contributionService.initSlider('slider3', data.commonData.postCurrent, data.commonData.postMin, data.commonData.postMax, data.commonData.postPctPre, 'post');
            this.cacheElements();
            this.bindEvents();
            contributionService.initWidget();
        },
        modalAction : function(e){
            var self = $(e.target),
                action = self.attr('rel');

            if(action == 'Opt Out'){
                $("#ACAoptOut").val(1);
                $('#salary_total_pct, #roth_total_pct, #post_total_pct').val(0).trigger("change");
                section.setData('contributions', {'C_ACAoptOut' : 1});
                _gaq.push(['_trackEvent', 'Contributions', 'Popup', 'ACA - Opt Out']);
            }else if(action == 'Proceed'){
                this.bindACAMessage();
                $("#ACAoptOut").val(0);
                contributionService.restoreDefaults();
                section.setData('contributions', {'C_ACAoptOut' : 0});
                _gaq.push(['_trackEvent', 'Contributions', 'Popup', 'ACA - Proceed']);
            }else if(action == 'Confirm'){
                $("#ACAoptOut").val(1);
                $(".ACAoptOutNotice").show();
                $("#ACAoptOutNotice").val(1);
                section.setData('contributions', {'C_ACAoptOut' : 1});
                _gaq.push(['_trackEvent', 'Contributions', 'Popup', 'ACA Opt Out - Confirm']);
            }else if(action == 'Cancel'){
                $('body').trigger("spe:router:redirect", 'contributions');
                _gaq.push(['_trackEvent', 'Contributions', 'Popup', 'ACA Opt Out - Cancel']);
            }
            this.noticeModal.modal('hide');
            this.warningModal.modal('hide');
            this.acaMessageModal.modal('hide');
        },
        dollarOptOutModalAction: function(e) {
            var self = $(e.target), action = self.attr('rel');
            if (action == 'Confirm') {
                this.contributionTypeDlr.prop("checked", true);
                contributionService.switchToDlr.call(this);
            }
            this.dollarOptOutModal.modal('hide');
        }

    };

    return {
        render : ContributionsView.render.bind(ContributionsView)
    }

});