define(['jquery','spe/service/common','spe/component/Calender','spe/model/Section','spe/service/data'], function($, common, calender, Section, data){

    var PersonalView = {

        tpl : ['new-personal', 'new-axa-403b-personal', 'new-ep-personal', 'new-smart-enroll-personal','new-smart-enroll-personal-prefilled', 'new-smart-enroll-personal-horacemann','new-smart-enroll-personal-securityBenefits'],

        tplEl : function(type){
            return $('#'+ this.tpl[type]);
        },

        cacheElements : function(){
            this.saveNew = $('#save_pi_new');
            this.saveNewAxa = $('#save_pi_new_axa');
            this.saveNewEp = $('#save_pi_new_ep');
            this.saveNewSmartEnroll = $('#save_pi_new_smart_enroll');
            this.saveNewSmartEnrollHoracemann = $('#save_pi_new_smart_enroll_horacemann');
            this.saveNewSmartEnrollSecurityBenefits  = $('#save_pi_new_smart_enroll_securityBenefits');
            this.empFooter = $('#footer .emp_name');
            this.firstName = $('#fname');
            this.lastName = $('#lname');
            this.maritalStatus = $('#marital_status');
            this.gender = $('#gender');
            this.ssnPublic = $('#ssnPublic');
            this.ssn = $('#ssn');
            this.day1 = $('.dob select[name=day]');
            this.month1 = $('.dob select[name=month]');
            this.year1 = $('.dob select[name=year]');
            this.day1h = $('.doh select[name=day]');
            this.month1h = $('.doh select[name=month]');
            this.year1h = $('.doh select[name=year]');
            this.day2 = $('.emp_date select[name=day]');
            this.month2 = $('.emp_date select[name=month]');
            this.year2 = $('.emp_date select[name=year]');
            this.email = $('#email');
            this.email2 = $('#email2');
            this.preferred_email = $('input[name=preferred_email]');
            this.marketing = $('input[name=marketing_opt_in]');
            this.address = $('#address');
            this.city = $('#city');
            this.state = $('#state');
            this.zip = $('#zip');
            this.availability = $('#availability');
            this.phone = $('#phone');
            this.mobile = $('#mobile');
            this.location = $('#location');
            this.advisor = $('#advisor'); 
            this.mailingAddressCheckbox = $("#mailingAddressCheckbox");
            this.mailingAddressCancelButton = $("#mailingAddressCancelButton");
            this.mailingAddressOkButton = $("#mailingAddressOkButton");
            this.mailingFields  = ["Address","City","State","Zip"];
            var that = this;
            $.each(this.mailingFields, function( index, value ) 
            {
                that['mailingAddress' + value] = $('#mailingAddress' + value);
                that['mailingAddress' + value + 'Saved'] = "";
            });
        },

        bindEvents : function(){
            this.ssnPublic.on('keyup', this.ssnPublic, this.typeMask.bind(this));
            this.saveNew.on('click', this.savePiNewAction.bind(this));
            this.saveNewAxa.on('click', this.savePiNewAxaAction.bind(this));
            this.saveNewEp.on('click', this.savePiNewEp.bind(this));
            this.saveNewSmartEnroll.on('click', this.savePiNewSmartEnroll.bind(this));
            this.saveNewSmartEnrollHoracemann.on('click', this.savePiNewSmartEnrollHoracemann.bind(this));
            this.saveNewSmartEnrollSecurityBenefits.on('click', this.savePiNewSmartEnrollSecurityBenefits.bind(this));
            this.mailingAddressCheckbox.on('click', this.mailingAddressLightbox.bind(this));
            this.mailingAddressCancelButton.on('click', this.mailingAddressCancel.bind(this));
            this.mailingAddressOkButton.on('click', this.mailingAddressSubmit.bind(this));
            var that = this;
            $.each(["firstName","lastName","city","address"], function( index, value ) 
            {
                that[value].bind("keyup change", function(e) 
                {                   
                    if (value == "address")
                    {
                        $(this).val($(this).val().replace(/[^a-zA-Z 0-9 ' ( ) -]+/g, ''));
                    }
                    else
                    {
                        $(this).val($(this).val().replace(/[^a-zA-Z 0-9 ']+/g, ''));
                    }
                });  
            });                          
        },

        
        typeMask : function(){
            
            var ssnMasked = $('input:hidden[name="ssnPublic"]').val();
            this.ssn.val(ssnMasked);

        },

        render : function(section, d, type){
            var tpl = _.template(this.tplEl(type).html())({
                model : d,
                calenderDob : calender.getWidget(d.dateOfBirth),
                calenderDoh : calender.getWidget(),
                calenderDobHard: calender.getWidget("01/01/1986"),
                calenderAod : calender.getWidget(d.asOfDate),
                maritalList : this.getMaritalStatus(),
                stateList : data.getStateList()
            });
            $(section).addClass('text').html(tpl);
            this.cacheElements();
            this.bindEvents();
            this.initWidget();
        },

        initWidget : function(){
            this.zip.mask('99999?-9999')
                .blur(function () {
                    var lastFour = $(this).val().substr(6, 4);
                    if (lastFour != "") {
                        if (lastFour.length != 4) {
                            $(this).val("");
                        }
                    }
                });
            this.ssnPublic.unmask().maskSSN('999-99-9999', {maskedChar:'X', maskedCharsLength:9});
            this.phone.mask('(999)-999-9999');
            this.mobile.mask('(999)-999-9999');
            this.mailingAddressZip.mask('99999?-9999');
        },

        getMaritalStatus : function(){
            return {
                '1' : 'Single',
                '2' : 'Married',
                '3' : 'Widow',
                '4' : 'Divorced',
                '5' : 'Separated',
                '6' : 'Widower'
            }
        },

        savePiNewAction : function(){
            if(!this.validate.apply(this)){
                return false;
            }
            var data = {
                'firstName' : this.firstName.val(),
                'lastName' : this.lastName.val(),
                'dateOfBirth' : this.month1.find('option:selected').val()+'/'+this.day1.find('option:selected').val()+'/'+this.year1.find('option:selected').val(),
                'email' : this.email.val(),
                'pipCompleted' : 1
            };
            this.save(data);
            _gaq.push(['_trackEvent', 'Personal Information', 'Save']);
        },

        savePiNewAxaAction : function(){
            if(!this.validate.apply(this, ['new_axa'])){
                return false;
            }
            var data = {
                'firstName' : this.firstName.val(),
                'lastName' : this.lastName.val(),
                'dateOfBirth' : this.month1.find('option:selected').val()+'/'+this.day1.find('option:selected').val()+'/'+this.year1.find('option:selected').val(),
                'email' : this.email.val(),
                'availability' : this.availability.val(),
                'phone' : this.phone.val(),
                'pipCompleted' : 1
            };
            this.save(data);
            _gaq.push(['_trackEvent', 'Personal Information', 'Save', '403b']);
        },

        savePiNewEp : function(e){
            var self = $(e.currentTarget);
            if(self.attr('rel') == 'save'){
                if(!this.validate.apply(this, ['new_ep'])){
                    return false;
                }
                var data = {
                    'firstName' : this.firstName.val(),
                    'lastName' : this.lastName.val(),
                    'address' : this.address.val(),
                    'city' : this.city.val(),
                    'region' : this.state.val(),
                    'postalCode' : this.zip.val(),
                    'maritalStatus' : this.maritalStatus.val(),
                    'dateOfBirth' : this.month1.find('option:selected').val()+'/'+this.day1.find('option:selected').val()+'/'+this.year1.find('option:selected').val(),
                    'asOfDate' : this.month2.find('option:selected').val()+'/'+this.day2.find('option:selected').val()+'/'+this.year2.find('option:selected').val(),
                    'email' : this.email.val(),
                    'updateEPProfile' : 1,
                    'updatePersonal' : 1
                };
                _gaq.push(['_trackEvent', 'Personal Information', 'Save', 'ExperPlan']);
            }else{
                var data = {
                    'updateEPProfile' : 1,
                    'updatePersonal' : 1
                };
                _gaq.push(['_trackEvent', 'Personal Information', 'Proceed', 'ExpertPlan']);
            }
            this.save(data, 'new_ep');
        },

        savePiNewSmartEnroll : function(){
            if(!this.validate.apply(this, ['new_smart_enroll'])){
                return false;
            }
            var data = {
                'firstName' : this.firstName.val(),
                'lastName' : this.lastName.val(),
                'address' : this.address.val(),
                'city' : this.city.val(),
                'state' : this.state.val(),
                'postalCode' : this.zip.val(),
                'maritalStatus' : this.maritalStatus.val(),
                'gender' : this.gender.val(),
                'ssn' : this.ssn.val(),
                'dateOfBirth' : this.month1.find('option:selected').val()+'/'+this.day1.find('option:selected').val()+'/'+this.year1.find('option:selected').val(),
                'dateOfHire' : this.month1h.find('option:selected').val()+'/'+this.day1h.find('option:selected').val()+'/'+this.year1h.find('option:selected').val(),
                'email' : this.email.val(),
                'email2' : this.email2.val(),
                'preferredEmail' : $('input[name=preferred_email]:checked').val() == 'office' ? this.email2.val() : this.email.val(),
                'phone' : this.phone.val(),
                'mobile' : this.mobile.val(),
                'location' : this.location.val(),
                'pipCompleted' : 1
            };
            this.save(data,'new_smart_enroll');
            _gaq.push(['_trackEvent', 'Personal Information', 'Save', 'SmartEnroll']);
        },

		savePiNewSmartEnrollHoracemann : function(){
            if(!this.validate.apply(this, ['new_smart_enroll_hm'])){
                return false;
            }
            var data = {
                'firstName' : this.firstName.val(),
                'lastName' : this.lastName.val(),
                'address' : this.address.val(),
                'city' : this.city.val(),
                'state' : this.state.val(),
                'postalCode' : this.zip.val(),
                'maritalStatus' : this.maritalStatus.val(),
                'gender' : this.gender.val(),
                'ssn' : this.ssn.val(),
                'dateOfBirth' : this.month1.find('option:selected').val()+'/'+this.day1.find('option:selected').val()+'/'+this.year1.find('option:selected').val(),
                'dateOfHire' : this.month1h.find('option:selected').val()+'/'+this.day1h.find('option:selected').val()+'/'+this.year1h.find('option:selected').val(),
                'email' : this.email.val(),
                'email2' : this.email2.val(),
                'preferredEmail' : $('input[name=preferred_email]:checked').val() == 'office' ? this.email2.val() : this.email.val(),
                'phone' : this.phone.val(),
                'mobile' : this.mobile.val(),
                'location' : this.location.val(),
                'pipCompleted' : 1,
				'marketing_opt_in' : $('input[name=marketing_opt_in]:checked').val() == 'yes' ? 1 : 0
            };
            this.save(data,'new_smart_enroll_hm');
            _gaq.push(['_trackEvent', 'Personal Information', 'Save', 'SmartEnroll']);
        },
        savePiNewSmartEnrollSecurityBenefits : function(){
            if(!this.validateSecurityBenefits.apply(this)){
                return false;
            }
            var data = {
                'firstName' : this.firstName.val(),
                'lastName' : this.lastName.val(),
                'address' : this.address.val(),
                'city' : this.city.val(),
                'state' : this.state.val(),
                'postalCode' : this.zip.val(),
                'maritalStatus' : this.maritalStatus.val(),
                'gender' : this.gender.val(),
                'ssn' : this.ssn.val(),
                'dateOfBirth' : this.month1.find('option:selected').val()+'/'+this.day1.find('option:selected').val()+'/'+this.year1.find('option:selected').val(),
                'dateOfHire' : this.month1h.find('option:selected').val()+'/'+this.day1h.find('option:selected').val()+'/'+this.year1h.find('option:selected').val(),
                'email' : this.email.val(),
                'email2' : this.email2.val(),
                'phone' : this.phone.val(),
                'mobile' : this.mobile.val(),
                'location' : this.location.val(),
                'pipCompleted' : 1,
                'advisor': this.advisor.val()
            };
            if (this.mailingAddressCheckbox.is(":checked") && $.trim(this.mailingAddressAddress.val()))
            {               
                var that = this;
                $.each(this.mailingFields, function( index, value ) 
                {
                    data['mailingAddress' + value] = that['mailingAddress' + value].val();
                });                
            }
            this.save(data,'new_smart_enroll_securityBenefits');
            _gaq.push(['_trackEvent', 'Personal Information', 'Save', 'SmartEnroll']);
        },
        validateSecurityBenefits: function()
        {
            var translation = Section.getData('translation');           
            if(!$.trim(this.firstName.val())){
                common.showErrMsg(translation.please_enter_first_name);
                return false;
            }else if(!$.trim(this.lastName.val())){
                common.showErrMsg(translation.please_enter_last_name);
                return false;
            }else if(!$.trim(this.address.val())){
                common.showErrMsg(translation.please_enter_address);
                return false;
            }else if(!$.trim(this.city.val())){
                common.showErrMsg(translation.please_enter_city);
                return false;
            }else if(!$.trim(this.state.find('option:selected').val())){
                common.showErrMsg(translation.please_select_state);
                return false
            }else if(!$.trim(this.zip.val()) || !common.isZip(this.zip.val())){
                common.showErrMsg(translation.please_enter_valid_zip);
                return false;
            }else if(!$.trim(this.maritalStatus.val())){
                common.showErrMsg(translation.please_select_marital_status);
                return false;
            }else if(!common.isSsn($.trim(this.ssn.val()))){
                this.ssn.val('')
                common.showErrMsg(translation.please_enter_valid_ssn);
                return false;
            }else if(!$.trim(this.month1.find('option:selected').val()) || !$.trim(this.day1.find('option:selected').val()) || !$.trim(this.year1.find('option:selected').val())){
                common.showErrMsg(translation.please_enter_date_of_birth);
                return false;
            }else if(!$.trim(this.month1h.find('option:selected').val()) || !$.trim(this.day1h.find('option:selected').val()) || !$.trim(this.year1h.find('option:selected').val())){
                common.showErrMsg(translation.please_enter_date_of_hire);
                return false;
            }else if(!$.trim(this.email.val()) && !$.trim(this.email2.val())){
                common.showErrMsg(translation.please_specify_your_email_address);
                return false;
            }else if( ($.trim(this.email.val()) && !common.isEmail(this.email.val())) || ($.trim(this.email2.val()) && !common.isEmail(this.email2.val())) ){
                common.showErrMsg(translation.please_enter_email_address);
                return false;
            }else if(!$.trim(this.mobile.val())){
                common.showErrMsg(translation.please_enter_mobile);
                return false;
            }else if(this.location.length && !$.trim(this.location.val())){
                common.showErrMsg(translation.please_select_location);
                return false;
            }else if (this.firstName.val().length > 15)
            {
                common.showErrMsg(translation.first_name_exceed_characters.replace("%number%",15));
                return false;
            }else if (this.lastName.val().length > 25)
            {
                common.showErrMsg(translation.last_name_exceed_characters.replace("%number%",25));
                return false;
            }else if (this.lastName.val().length + this.firstName.val().length > 35)
            {
                common.showErrMsg(translation.first_name_and_last_name_exceed_characters.replace("%number%",35));
                return false;
            }
            else{
                var ssnFormat = this.ssn.val();
                this.ssn.val(ssnFormat.replace(/(\d{3})(\d{2})(\d{4})/, "$1-$2-$3"));
                if (!this.preferred_email.is(':checked'))
                {
                    if($.trim(this.email.val()) && !$.trim(this.email2.val()))
                    {
                        $('input[name=preferred_email][value=home]').prop("checked",true);
                    }
                    else if(!$.trim(this.email.val()) && $.trim(this.email2.val()))
                    {
                        $('input[name=preferred_email][value=office]').prop("checked",true);
                    }  
                }
            }
            return true;
        },
        validate : function(type){
            var translation = Section.getData('translation');           

            if(type == 'new_smart_enroll'){
                if(!$.trim(this.firstName.val())){
                    common.showErrMsg(translation.please_enter_first_name);
                    return false;
                }else if(!$.trim(this.lastName.val())){
                    common.showErrMsg(translation.please_enter_last_name);
                    return false;
                }else if(!$.trim(this.address.val())){
                    common.showErrMsg(translation.please_enter_address);
                    return false;
                }else if(!$.trim(this.city.val())){
                    common.showErrMsg(translation.please_enter_city);
                    return false;
                }else if(!$.trim(this.state.find('option:selected').val())){
                    common.showErrMsg(translation.please_select_state);
                    return false
                }else if(!$.trim(this.zip.val()) || !common.isZip(this.zip.val())){
                    common.showErrMsg(translation.please_enter_valid_zip);
                    return false;
                }else if(!$.trim(this.maritalStatus.val())){
                    common.showErrMsg(translation.please_select_marital_status);
                    return false;
                }else if(!$.trim(this.gender.val())){
                    common.showErrMsg(translation.please_select_gender);
                    return false;
                }else if(!common.isSsn($.trim(this.ssn.val()))){
                    this.ssn.val('');
                    common.showErrMsg(translation.please_enter_valid_ssn);
                    return false;
                }else if(!$.trim(this.month1.find('option:selected').val()) || !$.trim(this.day1.find('option:selected').val()) || !$.trim(this.year1.find('option:selected').val())){
                    common.showErrMsg(translation.please_enter_date_of_birth);
                    return false;
                }else if(!$.trim(this.month1h.find('option:selected').val()) || !$.trim(this.day1h.find('option:selected').val()) || !$.trim(this.year1h.find('option:selected').val())){
                    common.showErrMsg(translation.please_enter_date_of_hire);
                    return false;
                }else if(!$.trim(this.email.val()) && !$.trim(this.email2.val())){
                    common.showErrMsg(translation.please_specify_your_email_address);
                    return false;
                }else if( ($.trim(this.email.val()) && !common.isEmail(this.email.val())) || ($.trim(this.email2.val()) && !common.isEmail(this.email2.val())) ){
                    common.showErrMsg(translation.please_enter_email_address);
                    return false;
                }else if(!this.preferred_email.is(':checked')){
                    common.showErrMsg(translation.please_select_preferred_email);
                    return false;
                }else if( ($('input[name=preferred_email]:checked').val() == 'office' && !$.trim(this.email2.val())) || ($('input[name=preferred_email]:checked').val() == 'home' && !$.trim(this.email.val())) ){
                    common.showErrMsg(translation.please_select_valid_preferred_email);
                    return false;
                }else if(this.location.length && !$.trim(this.location.val())){
                    common.showErrMsg(translation.please_select_location);
                    return false;
                }else{
                    var ssnFormat = this.ssn.val();
                    this.ssn.val(ssnFormat.replace(/(\d{3})(\d{2})(\d{4})/, "$1-$2-$3"));
                }
            }
			else if(type == 'new_smart_enroll_hm'){
                if(!$.trim(this.firstName.val())){
                    common.showErrMsg(translation.please_enter_first_name);
                    return false;
                }else if(!$.trim(this.lastName.val())){
                    common.showErrMsg(translation.please_enter_last_name);
                    return false;
                }else if(!$.trim(this.address.val())){
                    common.showErrMsg(translation.please_enter_address);
                    return false;
                }else if(!$.trim(this.city.val())){
                    common.showErrMsg(translation.please_enter_city);
                    return false;
                }else if(!$.trim(this.state.find('option:selected').val())){
                    common.showErrMsg(translation.please_select_state);
                    return false
                }else if(!$.trim(this.zip.val()) || !common.isZip(this.zip.val())){
                    common.showErrMsg(translation.please_enter_valid_zip);
                    return false;
                }else if(!$.trim(this.maritalStatus.val())){
                    common.showErrMsg(translation.please_select_marital_status);
                    return false;
                }else if(!common.isSsn($.trim(this.ssn.val()))){
                    this.ssn.val('')
                    common.showErrMsg(translation.please_enter_valid_ssn);
                    return false;
                }else if(!$.trim(this.month1.find('option:selected').val()) || !$.trim(this.day1.find('option:selected').val()) || !$.trim(this.year1.find('option:selected').val())){
                    common.showErrMsg(translation.please_enter_date_of_birth);
                    return false;
                }else if(!$.trim(this.month1h.find('option:selected').val()) || !$.trim(this.day1h.find('option:selected').val()) || !$.trim(this.year1h.find('option:selected').val())){
                    common.showErrMsg(translation.please_enter_date_of_hire);
                    return false;
                }else if(!$.trim(this.email.val()) && !$.trim(this.email2.val())){
                    common.showErrMsg(translation.please_specify_your_email_address);
                    return false;
                }else if( ($.trim(this.email.val()) && !common.isEmail(this.email.val())) || ($.trim(this.email2.val()) && !common.isEmail(this.email2.val())) ){
                    common.showErrMsg(translation.please_enter_email_address);
                    return false;
                }else if(!this.preferred_email.is(':checked') && $.trim(this.email.val()) && $.trim(this.email2.val())){
                    common.showErrMsg(translation.please_select_preferred_email);
                    return false;
                }else if( ($('input[name=preferred_email]:checked').val() == 'office' && !$.trim(this.email2.val())) || ($('input[name=preferred_email]:checked').val() == 'home' && !$.trim(this.email.val())) ){
                    common.showErrMsg(translation.please_select_valid_preferred_email);
                    return false;
                }else if(!$.trim(this.mobile.val())){
                    common.showErrMsg(translation.please_enter_mobile);
                    return false;
                }else if(this.location.length && !$.trim(this.location.val())){
                    common.showErrMsg(translation.please_select_location);
                    return false;
                }else if(!this.marketing.is(':checked')){
                    common.showErrMsg(translation.please_select_marketing);
                    return false;
		}else{
                    var ssnFormat = this.ssn.val();
                    this.ssn.val(ssnFormat.replace(/(\d{3})(\d{2})(\d{4})/, "$1-$2-$3"));
                    if (!this.preferred_email.is(':checked'))
                    {
                        if($.trim(this.email.val()) && !$.trim(this.email2.val()))
                        {
                            $('input[name=preferred_email][value=home]').prop("checked",true);
                        }
                        else if(!$.trim(this.email.val()) && $.trim(this.email2.val()))
                        {
                            $('input[name=preferred_email][value=office]').prop("checked",true);
                        }  
                    }
                }
            }
            else{
                if(!$.trim(this.firstName.val())){
                    common.showErrMsg(translation.please_enter_first_name);
                    return false;
                }else if(!$.trim(this.lastName.val())){
                    common.showErrMsg(translation.please_enter_last_name);
                    return false;
                }else if(!$.trim(this.month1.find('option:selected').val()) || !$.trim(this.day1.find('option:selected').val()) || !$.trim(this.year1.find('option:selected').val())){
                    common.showErrMsg(translation.please_enter_date_of_birth);
                    return false;
                }else if(!$.trim(this.email.val()) || !common.isEmail(this.email.val())){
                    common.showErrMsg(translation.please_enter_email_address);
                    return false;
                }
            }

            if(type == 'new_axa'){
                if(!$.trim(this.availability.val())){
                    common.showErrMsg(translation.please_enter_good_time_to_reach_you);
                    return false;
                }else if(!$.trim(this.phone.val() || !common.isPhone(this.phone.val()))){
                    common.showErrMsg(translation.please_enter_valid_telephone);
                    return false;
                }
            }else if(type == 'new_ep'){
                if(!$.trim(this.maritalStatus.find('option:selected').val())){
                    common.showErrMsg(translation.please_select_marital_status);
                    return false;
                }else if(!$.trim(this.city.val())){
                    common.showErrMsg(translation.please_enter_city);
                    return false;
                }else if(!$.trim(this.state.find('option:selected').val())){
                    common.showErrMsg(translation.please_select_state);
                    return false
                }else if(!$.trim(this.month2.find('option:selected').val()) || !$.trim(this.day2.find('option:selected').val()) || !$.trim(this.year2.find('option:selected').val())){
                    common.showErrMsg(translation.please_enter_employment_date);
                    return false;
                }else if(!$.trim(this.zip.val()) || !common.isZip(this.zip.val())){
                    common.showErrMsg(translation.please_enter_valid_zip);
                    return false;
                }
            }
            return true;
        },

        save : function(pi_data, type){
                        
            var translation = Section.getData('translation');
            
            Section.setData('common', pi_data);
            var self = this;
            if(pi_data.dateOfBirth){
                if(parseInt(new Date().getFullYear()) - parseInt(pi_data.dateOfBirth.split('/')[2]) >= 50){
                    Section.setData('common', { 'showCatchup' : 1, 'sc' : 1 });
                }else{
                    Section.setData('common', { 'showCatchup' : 0, 'sc' : 0 });
                }
            }
            var pData = {
                'common' : Section.getData('common')
            };

            if(type == 'new_ep'){
                data.updateSession(pData).then(function(resp){
                    if(JSON.parse(resp).status == 'success'){
                        var fullName = (pData.common.firstName ? pData.common.firstName : '') + ' ' + (pData.common.lastName ? pData.common.lastName : '');
                        self.empFooter.text(translation.employee +': ' + fullName);
                        $('body').trigger("spe:router:reload");
                    }
                });
            }else{
                data.saveProfileInfo(pData).then(function(resp){
                    if(JSON.parse(resp).status == 'success'){
                        var fullName = (pData.common.firstName ? pData.common.firstName : '') + ' ' + (pData.common.lastName ? pData.common.lastName : '');
                        self.empFooter.text(translation.employee +': ' + fullName);
                        $('body').trigger("spe:router:reload");
                    }
                });
            }
        },
        mailingAddressLightbox: function()
        {
            if (this.mailingAddressCheckbox.is(":checked"))
                $("#mailingAddressModal").modal("show");
        },
        mailingAddressCancel: function()
        {
            var that = this;
            $.each(this.mailingFields, function( index, value ) 
            {
                that['mailingAddress' + value].val(that['mailingAddress' + value + 'Saved']) ;
            });
        },
        mailingAddressSubmit: function()
        {
            var translation = Section.getData('translation');           
            var that = this;
            var errorMessages = [];
            if(!$.trim(this.mailingAddressAddress.val()))
            {
                errorMessages.push(translation.please_enter_address);               
            }       
            if(!$.trim(this.mailingAddressCity.val()))
            {
                errorMessages.push(translation.please_enter_city);               
            }    
            if(!$.trim(this.mailingAddressState.val()))
            {
                errorMessages.push(translation.please_select_state);               
            }              
            if (!$.trim(this.mailingAddressZip.val()) || !common.isZip(this.mailingAddressZip.val()))
            {
                errorMessages.push(translation.please_enter_valid_zip);
            }
            if (errorMessages.length > 0)
            {
                common.showErrMsg(errorMessages.join("<br/>"));
            }
            else
            {
                $.each(this.mailingFields, function( index, value ) 
                {
                    that['mailingAddress' + value + 'Saved'] = that['mailingAddress' + value].val();
                });
                $("#mailingAddressModal").modal("hide");
            }
        }
    };

    return {
        render : PersonalView.render.bind(PersonalView)
    }

});

