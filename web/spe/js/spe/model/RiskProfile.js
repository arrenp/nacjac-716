define(['jquery', 'underscore', 'service/data', './Section' ], function($, _, data, Section){
    var getXMLData = function(fileName){
            var file = fileName ? fileName : Section.getData('common').sections;
            return data.getRiskProfile({ url: '/riskProfileData', data : { 'filename': file }});
        },
        getInvestorType = function(model){
            return data.ajax({ url: '/riskProfileInvestor', data : { answers : model } });
        };

    return {
        getData:  getXMLData,
        getInvestorType: getInvestorType
    }
});