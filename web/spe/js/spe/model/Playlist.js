define(['jquery', 'underscore'], function($, _){

    var PlayList = {
            initialize : function(){
                this.bindEvents();
                this.setMediaPath();
            },
            fetchData : function(){
                return JSON.parse(localStorage.getItem('playlist'));
            },
            bindEvents : function(){
                var self = this;
                $('body').on("spe:section:update", self.update.bind(self));
            },
            setMediaPath : function(){
                this.mediaPath =  JSON.parse(localStorage.getItem('mediaPath'));
            },
            isCompleted: function(sectionName){
                var data = this.fetchData(),
                    sections = _.difference(_.keys(data), ['welcome', 'planBasics']),
                    to = sections.indexOf(sectionName),
                    result = true;
                    _.each(sections,function(item, index){
                        if(index < to){
                            result = data[item].completed
                        }
                    });
                  return result;
            },
            update: function(e, obj){
                var data = this.fetchData(),
                    sections = _.keys(data),
                    to = sections.indexOf(obj.controller);
                    if(!obj.type){ to = to + 1; }
                    if(obj.type == 'normal'){
                        //to = to + 1;
                    }
                    _.each(sections,function(item, index){
                        if(index < to){
                            data[item].completed = true;
                        }
                    });

                    localStorage.setItem('playlist', JSON.stringify(data));
            },
            build : function(sectionName, type){
                var data = this.getData(sectionName);
                return _.extend(this.mediaPath, {
                    'playlist' : type == 'normal'? data.videos : data.warning,
                    'type': type,
                    'controller': sectionName
                });
            },
            getData : function(sectionName){
                var data = this.fetchData();
                return data[sectionName] ? data[sectionName] : false;
            },
            setData: function(data){
                localStorage.setItem('playlist', JSON.stringify(_.extend(this.fetchData(), data)));
            }
        };

        PlayList.initialize();

    return PlayList;
});
