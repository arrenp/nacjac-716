define(['jquery', 'underscore'], function($, _){

    var Library = {

        fetchData : function(){
            return JSON.parse(localStorage.getItem('library'));
        },
        getData : function(){
            return this.fetchData();
        },
        setData : function(data){
            var localData = this.getData();
            _.extend(localData, data);
            localStorage.setItem('module', JSON.stringify(localData));
        }
    };

    return {
        getData:  Library.getData.bind(Library),
        setData:  Library.setData.bind(Library)
    }
});