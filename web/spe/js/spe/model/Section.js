define([], function(){

    var Section = {

        fetchData : function(section){
            return JSON.parse(localStorage.getItem(section));
        },
        getData : function(section){
            return this.fetchData(section);
        },
        setData : function(section, data){
            var sectionData = this.getData(section) ? this.getData(section) : {};
            _.extend(sectionData, data);
            localStorage.setItem(section, JSON.stringify(sectionData));
        },
        setItem: function(section, item){
            localStorage.setItem(section, item);
        }
    };

    return {
        getData : Section.getData.bind(Section),
        setData : Section.setData.bind(Section),
        setItem : Section.setItem.bind(Section)
    }
});