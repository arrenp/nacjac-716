define(["jquery"], function(){
   return {
       'mediaDirectory' : "/media",
       'player': {
           'autostart': navigator.userAgent.match(/(iPod|iPhone|iPad)/) ? 0 : 1,
           'flashplayer' : '/spe/player/jwplayer.flash.swf',
           'html5player' : '/spe/js/vendor/utils/jwplayer.html5.min.js',
           'width' : "100%",
           'aspectratio' : "16:9",
           'skin': '/spe/player/skins/glow.xml',
           'primary': $('html').hasClass('lte9') ? 'flash' : 'html5',
           'repeat' : false
       },
       playerPoster: '/spe/player/poster.jpg'
   }
});