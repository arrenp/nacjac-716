define(['jquery', 'underscore', 'spe/model/Playlist', 'spe/view/PlayerView', 'spe/model/Section', './PersonalController'], function($, _, playlist, playerView, Section, personal){

    var Controller = function(options){
        this.name = '';
        this.videoPlayed = false;
        this.extend = _.extend(this, options);
        this.videoContainer = '.content-area';
        this.ignoreWarning = ['welcome', 'planBasics','retirementNeeds', 'atBlueprint'];
        this.translation = Section.getData('translation');
        this.initialize = function(){
            this.cacheElements ? this.cacheElements() : undefined;
            this.bindActions ? this.bindActions(): undefined;
        };

        //bind all actions by given class name
        this.bindActions = function(){
            var self = this;
            _.each(this.actions, function(handler, action){
                $.subscribe("spe:"+ self.name +":"+ action, self.actions[action].bind(self));
            });
            $.subscribe("spe:"+ self.name +":complete", this.start.bind(this));
        };

        this.playVideos = function(){

            if(this.actions && playlist.fetchData()[this.name]['videos'] == undefined){  // check to see if playlist is empty for specific section, if empty go to first action
                var actionName = _.keys(this.actions)[0];
                this.actions[actionName].apply(this);
                return true;
            }
            //check if video off
            if(Section.getData('common').video == 'off'){
                if(_.contains(['welcome', 'planBasics'], this.name)){
                    $('body').trigger("spe:router:change", this.name);
                }
            }
            else{
                var hideWarning  = _.contains(this.ignoreWarning, this.name);
                var repeat = ['welcome', 'planBasics'];
                if(!hideWarning){ // play warning
                    if(!playlist.isCompleted(this.name)){
                        var model = playlist.build(this.name, 'warning');
                        playerView.render(this.videoContainer, model);
                        return false;
                    }
                }

                //check if this controller ends playlist
                if(!this.videoPlayed){ // show normal list
                    var model = playlist.build(this.name, 'normal');
                    playerView.render(this.videoContainer, model);
                    this.videoPlayed = _.contains(['welcome', 'planBasics'], this.name) ? false : true;
                    return false;
                }
            }

            // check Personal Profile
            var commonData = Section.getData('common');
            if(!commonData.pipCompleted){
                if(personal.hasProfile(this.name)){
                    personal.start(commonData.pipContent);
                    return false;
                }
            }

            if(this.actions){  // calling first actions from controller if contain
                var actionName = _.keys(this.actions)[0];
                this.actions[actionName].apply(this);
            }

        };

        // call initialize in starting
        this.initialize.apply(this);
    };

    return {
        Controller : Controller
    }
});