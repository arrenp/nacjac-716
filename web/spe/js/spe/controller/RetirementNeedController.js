define(['jquery','moment','underscore','./SpeController','spe/service/data','spe/service/common','spe/view/RetirementNeedView','spe/model/Section', 'service/data', 'spe/view/PlayerView'], function($, moment, _, SPE, service, common, retirementNeedView, Section, data, playerView){
    "use strict";

    var RetirementNeed = new SPE.Controller({

            name : 'retirementNeeds',
            CurrentSocialSecurityOnly : 1,
            EnhancedSocialSecurityOnly : 2,
            PensionOnly : 3,
            EnhancedSocialSecurityAndPension :4,

            loadPage : function(index){
                var model = Section.getData('retirement_needs'),
                    common = Section.getData('common'),
                    idx = index ? index : 1;
                    model.data_source = common.data_source;
                    model.planBalance = common.planBalance;
                    model.age = !isNaN(moment().diff(common.dateOfBirth, 'years')) ? moment().diff(common.dateOfBirth, 'years') : 0;

                retirementNeedView.render('.content-area', model, idx);
                var audio = idx > 4 ? common.RN_Audio[idx] : common.RN_Audio[idx-1];
                if(idx == 4){
                    $('#retirement_age').trigger('change');
                }
                playerView.play(audio, 'audio');
                $('body').trigger('spe:disclosure:hide');
                if(idx == 7){
                    $('body').trigger('spe:disclosure:show', { title: 'targetMonthlyContribution'});
                }
            },

            setMessage : function(msg){
                this.errMessage += msg + '<br>';
            },

            hasError : function(msg){
                if(common.hasError(msg)){
                    this.errMessage = '';
                    return true;
                }
                return false;
            },

            actions : {

                init : function(e, d){
                    var self = this;
                    this.$contentArea = $('.content-area');
                    var autoIncreasePage = $("#autoIncreaseModal").length > 0;
                    service.ajax({ url: '/retirementneeds' }).then(function(view){
                        self.$contentArea.addClass('text').html(view);
                    });


                }
            },

            start : function(){
                this.playVideos();
            }

        });

    return {
        start: RetirementNeed.start.bind(RetirementNeed)
    }
});