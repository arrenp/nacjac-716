define(['jquery','pubsub','spe/service/common','spe/view/PersonalView','spe/model/Section','spe/view/PlayerView'], function($, pubsub, common, personalView, Section, playerView){
        "use strict";

    var Personal = {

        start : function(type){
            var model = Section.getData('common'),
                idx = '';

            if(type == 0){
                idx = 12;
            }else if(type == 1 && model.IRSCode == '403b'){
                idx = 8;
            }else if(type == 2){
                idx = model.EP_Audio;
            }else if(type == 3){
                idx = 15;
            }

            personalView.render('.content-area', model, type);
            playerView.play(model.P_Audio[idx], 'audio');
        },

        hasProfile : function(controller){
            var model = Section.getData('common');
            if(controller == 'contributions'){
                if(parseInt(model.pipPosition) == 0){
                    return true;
                }
            }else{
                if(parseInt(model.pipPosition) == 1){
                    return true;
                }
            }
            return false;
        }
    };

    return {
        start : Personal.start.bind(Personal),
        hasProfile : Personal.hasProfile
    }

});