define(['jquery', './SpeController'], function($, SPE){

    var WelcomeController = new SPE.Controller({
            name : 'welcome',
            start : function(){
                this.playVideos();
            }
        });

    return {
        start: WelcomeController.start.bind(WelcomeController)
    }
});