define(['./SpeController',  'spe/service/data', 'spe/service/investmentsService','spe/model/Section'], function(SPE, service, investmentsService,Section){
    "use strict";
    var hasBind = false;
    var Investments = new SPE.Controller({
        name : 'investments',
        start : function(){
            if(!hasBind){
                this.bindEvents();
                hasBind = true;
            }
            if (Section.getData("module").ATBlueprint)
            {
                this.ignoreWarning.push("investments");
            }
            this.playVideos();
        },
        bindEvents : function(){
            this.$contentArea = $('body');
            this.$contentArea.on('click', 'input[name=investment]', investmentsService.setInvestmentType.bind(this));
            this.$contentArea.on('click', '.iRisk .actionBtn', investmentsService.saveRisk.bind(this));
            this.$contentArea.on('click', '.iModel .actionBtn', investmentsService.saveModel);
            this.$contentArea.on('click', '.iProceed.s .actionBtn', investmentsService.saveTarget.bind(this));
            this.$contentArea.on('click', '.iProceed.m .actionBtn', investmentsService.chooseTarget);
            this.$contentArea.on('click', '.iSelect .actionBtn', investmentsService.saveCustom);
            this.$contentArea.on('click', '.iATArchitect .actionBtn', investmentsService.ATArchitect);
            this.$contentArea.on('click', '.iDefault .actionBtn', investmentsService.saveDefault.bind(this));
            this.$contentArea.on('click', '.actionBtn.iNav', investmentsService.investments.bind(this));
            this.$contentArea.on('click', '.actionBtn.iCalculate', investmentsService.calcCustom.bind(this));
            this.$contentArea.on('click', '.investments .portRadio, .investments .modelRadio', investmentsService.pickInvestment.bind(this));
            this.$contentArea.on('click', '.investments .portTitle', investmentsService.togglePortFunds.bind(this));
            this.$contentArea.on('click', '.actionBtn.iReset', investmentsService.calcReset.bind(this));
            this.$contentArea.on('keypress', '.investments .pct input', function() { $('#next').removeClass('show').addClass('hide');});
            this.$contentArea.on('blur', '.smart401kFund', investmentsService.smart401kNote.bind(this));
            this.$contentArea.on('click', '.investments .redemption-click', investmentsService.redemptionFee.bind(this));
            this.$contentArea.on('click', '.actionBtn.iSave', investmentsService.saveInvestments);
            this.$contentArea.on('click', '.investments .playVideo', investmentsService.playVideo.bind(this));
            this.$contentArea.on('click', '.popupLink', function() { window.open($(this).data('link'),'_blank','width=800,height=600,top=200,left=200,scrollbars=1,location=0') });
        },
        actions: {
            indexPage : function(){
                var self = this;
                this.$contentArea = $('.content-area');
                service.ajax({ url: '/investments' }).then(function(view){
                    self.$contentArea.addClass('text').html('<div id ="investmentsContainer">' +view + '</div>');
                    investmentsService.playAudio();
                    investmentsService.init();
                    $('body').trigger('spe:disclosure:show', { title: 'investmentSelector'});
                });
            }
        }
    });

    return {
        start: Investments.start.bind(Investments)
    }
});
