define(['./SpeController',  'spe/service/data', 'spe/model/Section'], function(SPE, service, Section){
    "use strict";
    var atBlueprint = new SPE.Controller({
        name : 'atBlueprint',
        start : function(){
            this.playVideos();
        },
        actions: {
            indexPage : function(){
                var self = this;
                this.$contentArea = $('.content-area');
                service.ajax({ url: '/atBlueprint/main' }).then(function(view){
                    self.$contentArea.addClass('text').html(view);
                    $('body').trigger('spe:disclosure:show', { title: 'ATBlueprintSelector'});
                });
            }
        }
    });

    return {
        start: atBlueprint.start.bind(atBlueprint)
    }
});
