define(['jquery',
        './SpeController',
        'spe/service/data',
        'spe/model/Section'
        ],
function($, SPE, service,Section){
        "use strict";

    var Contributions = new SPE.Controller({
        name : 'contributions',
        start : function(){
            var riskProfileData = Section.getData('risk_profile');
            if (($("#investments").hasClass("full-lock") && riskProfileData.stadionManagedAccountOn) || Section.getData('common').autoNavigation || riskProfileData.retireExpressOptInValue)
            {
                this.ignoreWarning.push("contributions");
            }
            this.playVideos();
        },
        actions : {
            init: function(){
                var self = this;
                this.$contentArea = $('.content-area');
                var autoIncreasePage = $("#autoIncreaseModal").length > 0;
                service.ajax({ url: '/contributions' }).then(function(view){
                    self.$contentArea.addClass('text').html(view);
                    if (autoIncreasePage)
                    {
                        $('body').trigger('spe:router:redirect','contributions');
                    }
                });
            }
        }
    });
    return {
        start: Contributions.start.bind(Contributions)
    }
});
