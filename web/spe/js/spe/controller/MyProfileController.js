define(['jquery', './SpeController'], function($,SPE){
    "use strict";

    var MyProfile = new SPE.Controller({

        name: 'myProfile',

        start : function(){
            $.post( "/myProfile", function( data )
            {
                $('.content-area').html(data);
            });
        }
		
    });

    return {
        start: MyProfile.start.bind(MyProfile)
    }
});
