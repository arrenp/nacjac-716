define(['jquery', 'underscore', './SpeController', 'spe/model/Section', 'spe/model/RiskProfile', 'spe/view/RiskProfileView', 'spe/view/PlayerView','spe/service/common', 'service/data'  ], function($, _, SPE, Section, riskProfile, riskProfileView, playerView, common, data){
    "use strict";
    var Advice = true;
    var RiskProfile = new SPE.Controller({
            name: 'riskProfile',
            updateModel : function(data){
                this.model[this.current].selected = data.selected;
                this.current = data.action == 'next' ? this.current + 1 : this.current -1;
                Section.setData('risk_profile', { 'answers' : this.model });
            },
            getProfileData : function(){
                return Section.getData('risk_profile');
            },
            getModel : function(){
                var data = this.model[this.current],
                    length = _.keys(this.model).length - 1;
                    if(this.current == length){
                        this.actions.profile.apply(this);
                        return false;
                    }
                    data.start = this.current == 0;
                return data;
            },
            renderPage : function(){
                var model = this.getModel();
                riskProfileView.render('.content-area', model);
                if (!$("#blockAudio").length)
                {
                    playerView.play(model.audio, 'audio');
                }
            },
            loadPage : function(){
                var self = this;
                if(self.model === undefined || self.model === null){
                    
                    var fileName = this.loadXMLFIle ? this.loadXMLFIle  : false;

                    riskProfile.getData(fileName).then(function(data){
                        self.model = JSON.parse(data);
                        self.current = 0;
                        self.renderPage();
                    });
                }else{
                    this.renderPage();
                }
            },
            actions : {
                index : function(e, args){
                    var riskProfileData = Section.getData('risk_profile');
                    
                    if(Advice && Section.getData('common').adviceStatus){
                        riskProfileView.render('.content-area', {}, 'advice');
                        playerView.play(Section.getData('common').RP_Audio, 'audio');
                        return false;
                    }
                    
                    if (riskProfileData.stadionManagedAccountOn && !this.managedAccountOptInCompleted) {
                        riskProfileView.render('.content-area', riskProfileData, 'managed-account-retire-express');
                        playerView.play(Section.getData('common').RP_Audio, 'audio');
                        return false;
                    }

                    if(args){
                        if(args.do == 'index'){
                            this.current = 0;
                        }else{
                            this.updateModel(args); //save data
                        }
                    }
                    this.loadPage();  // render view
                },
                profile : function(){
                    var answers = [],
                        self = this;
                    _.each(this.model, function(item){
                        if(item.selected) {
                            answers.push(item.selected);
                        }
                    });
                    riskProfile.getInvestorType(answers).then(function(json){
                        var data = JSON.parse(json);
                        Section.setData('risk_profile', data);
                        playerView.render('.content-area', _.extend(JSON.parse(localStorage.getItem('mediaPath')), { 'playlist' : [data.RP_video], 'callback' :  self.actions.result.bind(self, data.RP_label ) }));
                    });
                },
                advice: function(e, args){
                    var self = this;
                    var options = $(args.target).val();
                        if(options == 1){
                            riskProfileView.render('.content-area', {}, 'disclaimer');
                        }else{
                            Advice = false;
                            data.updateSession({ smart401k : 0 }).then(function() {
                                self.actions.index.apply(self);
                            });
                        }
                },
                disclaimer: function(e , args){
                    if($(args.target).hasClass('agree')){
                        Advice = false;
                        this.loadXMLFIle = 'smart401k';
                    }
                    this.actions.index.apply(this);
                },
                result: function(label){
                    var riskProfileData = Section.getData('risk_profile');
                    if (riskProfileData.stadionManagedAccountOn && riskProfileData.managedAccountOptInValue)
                    {
                        $.post( "/retirementneeds/stadionResult", function( data ) 
                        {
                            $( ".content-area" ).html( data );
                        });
                    }
                    else
                    {
                        riskProfileView.render('.content-area', { label : label }, 'result');
                    }
                },
                finish : function(){
                    var riskProfileData = Section.getData('risk_profile');
                    window.EventTracking.finishedRiskProfile(riskProfileData);
                    $('body').trigger("spe:router:change", { to: this.name, complete : this.name });
                },
                backManagedAccount: function(e, args) {
                    riskProfileView.render('.content-area', Section.getData('risk_profile'), 'managed-account-retire-express');
                    playerView.play(Section.getData('common').RP_Audio, 'audio');
                    return false;
                },
                proceedManagedAccount: function(e, args) {
                    var self = this;
                    this.loadXMLFIle = false;
                    var managedAccountOptInValue = 0;
                    if ($('#managedAccountOptInYes').is(':checked')) {
                        this.loadXMLFIle = 'stadionManagedAccount';
                        managedAccountOptInValue = 1;
                    }
                    if (Section.getData('risk_profile').managedAccountOptInValue !== managedAccountOptInValue) {
                        delete this.model;
                    }
                    Section.setData('risk_profile', { managedAccountOptInValue: managedAccountOptInValue });
                    this.managedAccountOptInCompleted = true;
                    data.updateSession({ managedAccountOptInValue: managedAccountOptInValue }).then(function() {
                        self.actions.index.apply(self);
                    });
                },
                proceedRetireExpress: function(e, args) {
                    var riskProfileData = Section.getData('risk_profile');
                    riskProfileData.retireExpressOptInValue = 0;
                    if ($('#retireExpressOptInYes').is(':checked')) {
                        riskProfileData.retireExpressOptInValue = 1;
                        Section.setData('risk_profile', riskProfileData);
                        $.post( "/retireExpress", function( data )
                        {
                            $('body').trigger('spe:router:redirect', 'contributions');
                        });
                    } else {
                        Section.setData('risk_profile', riskProfileData);
                        riskProfileView.render('.content-area', riskProfileData, 'managed-account-opt-in');
                    }
                }
            },
            start : function(){
                this.model = null; // will refresh RTQ everytime Risk Profile is clicked
                this.current = 0;
                this.managedAccountOptInCompleted = false;
                if (Section.getData("module").ATBlueprint)
                {
                    this.ignoreWarning.push("riskProfile");
                }
                this.playVideos();
            }
    });

    return {
        start: RiskProfile.start.bind(RiskProfile)
    }
});
