define(['jquery'], function($){

    var SiteController = {
            init: function(){
                this.cacheElements();
                this.bindEvents();
            },
            cacheElements : function(){
                this.$container = $('body');
                this.$page = $(".content-area");
                this.$supportTemplate = $("#site-support-template");
                this.$contactTemplate = $("#site-contact-template");
                this.$privacyTemplate = $("#site-privacy-template");
                this.$termsTemplate = $("#site-terms-template");
            },
            bindEvents : function(){
                var pages = ['support', 'contact', 'privacy', 'terms'],
                    self = this;
                    $.each(pages, function(index, item){
                        self.$container.on('click', '#footer .'+item, self.actions[item].bind(self));
                    });
            },
            actions : {
                support : function(){
                    this.$page.html(this.$supportTemplate.html()).addClass('text');
                },
                contact : function(){
                    /* @todo load content from ajax */
                    //var isAjax;
                    this.$page.html(this.$contactTemplate.html()).addClass('text');
                },
                privacy : function(){
                    this.$page.html(this.$privacyTemplate.html()).addClass('text');
                    window.scrollTo(0, 0);
                },
                terms : function(){
                    this.$page.html(this.$termsTemplate.html()).addClass('text');
                    window.scrollTo(0, 0);
                }
            }
        };


    return SiteController
});