define(['jquery', 'underscore', 'service/data', 'service/DisclosureService', './model/Section'], function($, _, data, DisclosureService, Section){
    "use strict";

    var Disclaimer = true,
        isAliveTimer = '',
		timeoutTimer = '',
        hasBind = false,
        timer = {
            time: 0,
            now: function(){ return (new Date()).getTime(); },
            start: function(){ this.time = this.now(); },
            since: function(){ return this.now()-this.time; }
        },
        Router = {
        init : function(){
            this.cacheElements();

            var self = this;
            this.loadData().then(function(){
                self.bindEvents();
                self.loadPages();
                self.isNavigationLock = Section.getData('common').navigationLocked;
                if(self.isNavigationLock){
                    self.lockNav();
                }
                DisclosureService.initialize();
            });
        },
        cacheElements : function(){
            var $navigation = $("#spe-container");

            this.$planBasics =  $navigation.find("#plan-basic");
            this.$retirementNeeds = $navigation.find("#retirement-needs");
            this.$riskProfile = $navigation.find("#risk-profile");
            this.$investments = $navigation.find("#investments");
            this.$contributions = $navigation.find("#contributions");
            this.$myProfile = $navigation.find("#my-profile");
            this.$library = $navigation.find("#library");
            this.$body = $('body');
            this.$navs = $("#mainNav");
            this.$speMenu = this.$navs.find('.SPEMenu');
        },
        sections : function(){
            if(parseInt(Section.getData('common').flexPlan)) {
                return [this.$planBasics, this.$retirementNeeds, this.$contributions, this.$investments, this.$myProfile, this.$library];
            }
            return [this.$planBasics, this.$retirementNeeds, this.$riskProfile, this.$investments, this.$contributions, this.$myProfile, this.$library];
        },
        bindEvents : function(){
            if (Section.getData('common').autoNavigation)
            {                
                this.$riskProfile.addClass("full-lock");
                this.$investments.addClass("full-lock");
            }
            var self = this;
            _.each(this.sections(), function(item){
                item.on('click', self.loadController.bind(self));
            });

            if(!hasBind) {
                this.$body.on('click', '.agree-disclaimer', this.hideDisclaimer.bind(this));
                this.$body.on('spe:router:change', this.changeRouter.bind(this));
                this.$body.on('spe:router:redirect', this.redirect.bind(this));
                this.$body.on('spe:router:reload', this.reload.bind(this));
                hasBind = true;
            }
        },
        lockNav : function(){
            this.$speMenu.find('.lockable').addClass('lock');
            this.$speMenu.find('.lock').on('click', function(){
               return '';
            });
        },
        loadData : function(){
            var Deferred = $.Deferred(),
                self = this;

            data.getProfileData().then(function(data){
                var resp = JSON.parse(data);
                _.each(resp, function(item, index){
                    localStorage.setItem(index, JSON.stringify(item));
                });
                Disclaimer = resp.messaging && resp.messaging.disclaimer.length > 0 && resp.messaging.disclaimerActive == 1 ? true : false;
                self.loadController('WelcomeController');
                Deferred.resolve();
            });
            return Deferred.promise();
        },
        loadPages : function(){
            require(['controller/SiteController'], function(site){
                site.init();
            });
        },
        completeSection : function(sectionName){
            $('body').trigger("spe:section:update", { controller: sectionName });
        },
        getControllers: function(){
            return this.sections();
        },
        changeRouter : function(e, section){
            if(typeof section == 'object'){
                this.completeSection(section.complete);
                section = section.to;
            }
            this.$body.removeClass('modal-open');

            var controller = '$' + section.charAt(0).toLowerCase() + section.slice(1),
                next = _.indexOf(this.getControllers(), this[controller]),
                find = next + 1,
                controllerName = this.getControllers()[find].find('a').data('controller');
            this.loadController(controllerName);
        },
        redirect : function(e, section){
            if(typeof section == 'object'){
                this.completeSection(section.complete);
                section = section.to;
            }
            this.$body.removeClass('modal-open');

            var controller = '$' + section.charAt(0).toLowerCase() + section.slice(1),
                next = _.indexOf(this.getControllers(), this[controller]),
                data =  this.getControllers()[next].find('a').data(),
                controllerName = data.controller,
                actionName = data.action;
            this.loadController(controllerName,actionName);
        },
        reload : function(e){
            this.$body.removeClass('modal-open');
            var controllerName = this.$navs.find('li.active a').attr('data-controller');
            this.loadController(controllerName);
        },
        hideDisclaimer : function(){
            Disclaimer = false;
            this.loadController('WelcomeController');
        },
        highlightNav : function(controller, index){
            $(".loader").show();
            this.$navs.find("li").removeClass('active');            
            if(index){
                $("a[data-action='"+ index +"']").parent().addClass('active');
                $('.navbar-header h1.title').html($("a[data-action='"+ index +"']").html());
            }else{
                $("a[data-controller='"+ controller +"']").last().parent().addClass('active').removeClass('lock');
                $("a[data-controller='"+ controller +"']").last().parent().addClass('active').removeClass('full-lock');
                $('.navbar-header h1.title').html($("a[data-controller='"+ controller +"']").html());               
                if(typeof moaElementHtml !== 'undefined')
                {
                    moaAddActiveColors();
                    $(".moaElement" ).html("");
                    if (controller != "LibraryController")
                    {
                        $("a[data-controller='"+ controller +"']").append(moaElementHtml);
                        $("a[data-controller='"+ controller +"']").addClass("moaActiveLink");
                    }
                }
                
            }
        },
        beforeLoad : function(action){
            if(action == 'library'){
                return false;
            }
        },
        loadController: function(e, action){

            if(this.isNavigationLock && typeof e !== 'string' && $(e.currentTarget).hasClass('lock') || $(e.currentTarget).hasClass('full-lock')){
                return false;
            }
            this.keepAlive();

            if(!$('.navbar-header .navbar-toggle').hasClass('collapsed') && e.originalEvent){
                $('.navbar-header .navbar-toggle').trigger('click');
            }

            // check if we have to show disclaimer
            if(Disclaimer){
                this.$reffer = e.currentTarget ? $(e.currentTarget).find('a').data('controller'): 'WelcomeController';
                require(['spe/view/DisclaimerView'], function(disclaimerView){
                    disclaimerView.render(".content-area");
                });
                return;
            }

            //hide disclosure
            this.$body.trigger('spe:disclosure:hide');

            // load controller
            var controllerName,
                actionName = 'index';

            if(this.$reffer){
                controllerName = this.$reffer;
                this.$reffer = false;
            }else{
                if(typeof e == 'string'){
                    controllerName = e;
                    actionName = action ? action : 'index';
                }else{
                    var data = $(e.target).data();
                    controllerName = data.controller;
                    actionName = data.action ? data.action : 'index';
                }
            }

            $(".loader").show();
            this.beforeLoad(actionName);
            this.highlightNav(controllerName, '');

            require(['controller/'+ controllerName ], function(controller){
                controller.start(actionName);
                $(".loader").hide();
            });
        },
        keepAlive: function(){
            var model = Section.getData('common'),
                self = this;

            if( !_.contains(model.post_calls, 'ping') || model.data_source == 'educate' || model.environment == 'dev' || parseInt(model.testing)){
                return false;
            }
            data.ping().then(function(data){
                var response = JSON.parse(data);
                if(response){
                    self.resetTime((parseInt(model.session_expiration) * 1000) - (parseInt(model.session_time_to_update) * 1000));
                }else{
                    window.top.location.reload();
                }
            });
        },
        resetTime: function(rtime){
            var self = this;
            window.clearTimeout(isAliveTimer);
            isAliveTimer = window.setTimeout(function(){
                self.ping();
            }, rtime);
            return;
        },
        ping: function(){
            var model = Section.getData('common'),
                self = this;
            data.sessionTimeLeft().then(function(data){
                var resp = JSON.parse(data);
                if(resp.time_left == '' || !resp.time_left){
					if(model.timeoutWebsiteAddress){
						setTimeout(function(){
							window.top.location.href = decodeURIComponent(model.timeoutWebsiteAddress);
						}, 2000);
					}
					else {
						window.top.location.reload();
					}
                }else if((parseInt(resp.time_left) <= (parseInt(model.session_time_to_update) * 1000)) ||  (parseInt(resp.time_left) - 1000 <= (parseInt(model.session_time_to_update) * 1000))){
                    self.sessionTimeOut(parseInt(resp.time_left));
                }else{
                    self.resetTime((parseInt(resp.time_left) - parseInt(model.session_time_to_update)) * 1000);
                }
            })
        },
		redirectTimeout: function() {
            var translation = Section.getData('translation');
            var model = Section.getData('common');

			window.clearInterval(timeoutTimer);
			window.clearTimeout(isAliveTimer);

			$('body').css('background','none').html('<div class="session-timeout-block">'+ translation.you_have_chosen_end_your_smartplan_session +'</div>');

			if(model.timeoutWebsiteAddress){
				setTimeout(function(){
					window.top.location.href = decodeURIComponent(model.timeoutWebsiteAddress);
				}, 2000);
			}
			else {
				window.top.location.reload();
			}
		},
        sessionTimeOut: function(ltime){
            var translation = Section.getData('translation');
            var model = Section.getData('common');
            var self = this;
			var seconds = parseInt(Section.getData('common').session_time_to_update);

			window.clearInterval(timeoutTimer);

			$('#timeoutModal').find('.modal-body').html(translation.you_session_will_expire_in + ' ' + seconds + ' ' + translation.seconds);
			$('#timeoutModal').modal('show');
			
			timeoutTimer = window.setInterval(function() {
				seconds--;

				$('#timeoutModal').find('.modal-body').html(translation.you_session_will_expire_in + ' ' + seconds + ' ' + translation.seconds);

				if (seconds == 0) {
					self.redirectTimeout();
				}
			}, 1000);

			$('#timeoutModal .action').on('click', function(e) {
				$('#timeoutModal').modal('hide');
				var action = $(e.target).attr('rel');

				if (action == 'Proceed') {
					window.clearInterval(timeoutTimer);
					self.keepAlive();
				}
				else if (action == 'Cancel') {
					self.redirectTimeout();
				}
			});
        }
    };

    return Router;
});
