define(['jquery', 'underscore', 'spe/model/Section'], function($, _, Section){
    var Disclaimer = {
        initialize : function(){
            this.cacheElements();
            this.bindEvents();
            this.messages = Section.getData('messaging');
        },
        cacheElements: function(){
             this.$container = $(".disclosure");
             this.$msgHolder = this.$container.find('.disclosure-text');
             this.$close = this.$container.find('#disclosure-close');
             this.$event = $("body");
        },
        bindEvents : function(){
            this.$event.on('spe:disclosure:show', this.show.bind(this));
            this.$event.on('spe:disclosure:hide', this.hide.bind(this));
            this.$close.on('click', this.hide.bind(this));
        },
        getMessages : function(key){
            return this.messages[key];
        },
        getVideoKey : function(title){
             var key = false;
             var title = title.toString().split('_')[0];
             if(_.contains(['0100','0202','0207','0206','0204a','0500','0500a','0500b','0101'],title)){
                switch (title){
                    case '0100': key = 'welcome'; break;
                    case '0101': key = 'welcome'; break;
                    case '0202': key = 'matching'; break;
                    case '0207': key = 'vesting'; break;
                    case '0206': key = 'loans'; break;
                    case '0204a': key = 'autoEnroll'; break;
                    case '0500':
                    case '0500a': 
                    case '0500b': key = 'riskProfile'; break;
                }
            }
            return key;
        },
        hide: function(){
            var self = this;
            this.$container.slideUp(function(){
                self.$msgHolder.html('');
                $(window).resize();
            });
        },
        show : function(e, data){
            var key = data.video ? this.getVideoKey(data.title) : data.title;
            var messaging = Section.getData('messaging');
            var message = messaging ? messaging[key] : false;
            var messageShow = messaging ? messaging[key + 'Active']: false;
            if(message && messageShow){
                var self = this;
                this.$container.slideDown(function(){
                    self.$msgHolder.html(message);
                    $(window).resize();
                });
            }
        }
    };

    return Disclaimer;
});