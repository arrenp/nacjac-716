define(['jquery','underscore','spe/service/common','spe/model/Section'], function($, underscore, common, Section){
    "use strict";

    var RiskProfileService = {

        getAnswers : function(){
            var qa = [];
            if(!_.isEmpty(Section.getData('risk_profile').answers)){
                _.each(Section.getData('risk_profile').answers, function(option, index){
                    if(option.text){
                        qa.push({'question' : common.htmlEntities(option.text), 'answer' : option.selected ? common.htmlEntities(option.options[option.selected]) : ''});
                    }
                })
            }
            return qa;
        }

    };

    return{
        getAnswers: RiskProfileService.getAnswers
    }
});