<?php
$headers = array(
            'Authorization: Bearer m3cpbr7bcv52tjjxwyctz6ks',
            'Content-Type:application/json'
        ); 

        $data = array();
        $data['name'] = "My Email Campaign";
        $data['subject'] = "Checkout my new product";
        $data['message'] = "<!DOCTYPE><html><body>Our inventory is stocked.</body></html>";
        $data['company'] = "Vertical Response";
        $data['street_address'] = "50 Beale St";
        $data['locality'] = "San Francisco";
        $data['region'] = "California";
        $data['postal_code'] = "94105";

        $json = json_encode($data);


        $channel = curl_init();
		curl_setopt( $channel, CURLOPT_URL, "https://vrapi.verticalresponse.com/api/v1/messages/emails");
		curl_setopt( $channel, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $channel, CURLOPT_POST, 1 );
		curl_setopt( $channel, CURLOPT_POSTFIELDS, $json );
		curl_setopt( $channel, CURLOPT_HTTPHEADER, $headers);
        echo curl_exec ( $channel );
