$(document).ready(function(){
    //code for getting charts to render in pdf
    Function.prototype.bind = Function.prototype.bind || function (thisp) {
            var fn = this;
            return function () {
                return fn.apply(thisp, arguments);
            };
        };


    var lineChartData = {
        labels: ["12AM - 8AM", "8AM - 4PM", "4PM - 11:59PM"],
        datasets: [{
            fillColor: "rgba(220,220,220,0)",
            strokeColor: "rgba(220,180,0,1)",
            pointColor: "rgba(220,180,0,1)",
            data: [$('#12-8-transacted').text().replace(/[^0-9.]/g, ""), $('#8-4-transacted').text().replace(/[^0-9.]/g, ""), $('#4-1159-transacted').text().replace(/[^0-9.]/g, "")]
        }]

    };

    Chart.defaults.global.animationSteps = 70;
    Chart.defaults.global.tooltipYPadding = 16;
    Chart.defaults.global.tooltipCornerRadius = 0;
    Chart.defaults.global.tooltipTitleFontStyle = "normal";
    Chart.defaults.global.tooltipFillColor = "rgba(51, 51, 51, 0.87)";
    Chart.defaults.global.animationEasing = "easteOutBack";
    Chart.defaults.global.responsive = true;
    Chart.defaults.global.scaleLineColor = "#BBBBBB";
    Chart.defaults.global.scaleFontSize = 5;
    Chart.defaults.global.scaleFontColor = "#f5f5f5";

    var linectx = document.getElementById("linechart").getContext("2d");
    var LineChartDemo = new Chart(linectx).Line(lineChartData, {
        pointDotRadius: 10,
        bezierCurve: true,
        scaleShowVerticalLines: true,
        scaleGridLineColor: "#BBBBBB"
    });


    //doughnut chart
    var doughnutctx = $("#doughnutchart").get(0).getContext("2d");

    var data = [
        {
            value: $('#advice').text().replace(/[^0-9.]/g, ""),
            color: "#7d6794",
            highlight: "rgba(125, 103, 148, 0.5)",
            label: "Advice"
        },
        {
            value: $('#custom').text().replace(/[^0-9.]/g, ""),
            color: "#ffad32",
            highlight: "rgba(255, 173, 50, 0.5)",
            label: "Custom"
        },
        {
            value: $('#default').text().replace(/[^0-9.]/g, ""),
            color: "#74ccd2",
            highlight: "rgba(116, 204, 210, 0.5)",
            label: "Default"
        },
        {
            value: $('#risk').text().replace(/[^0-9.]/g, ""),
            color: "#1a6d9f",
            highlight: "rgba(26, 109, 159, 0.5)",
            label: "Risk"
        },
        {
            value: $('#target').text().replace(/[^0-9.]/g, ""),
            color: "#24ae4b",
            highlight: "rgba(36, 174, 75, 0.5)",
            label: "Target"
        }
    ];


    var chart = new Chart(doughnutctx).Doughnut(data);

    var browser = $('#top-browser').text().toLowerCase();

    var icon = '';

    if(browser == 'internet explorer'){
        icon = 'fa-internet-explorer';
    } else if(browser == 'android browser') {
        icon = 'fa-android';
    } else if(browser == 'chrome') {
        icon = 'fa-chrome';
    } else if(browser == 'edge') {
        icon = 'fa-edge';
    } else if(browser == 'firefox') {
        icon = 'fa-firefox';
    } else if(browser == 'safari'|| browser == 'safari (in-app)') {
        icon = 'fa-safari';
    }

    if(icon) {
        $('#top-browser').prepend('<i class="fa ' + icon +' fa-1x icon-margin" aria-hidden="true" style="color: #af546d;"></i>');
    }

    $('#download-csv').click(function() {
        var data = "";
        if($("#provider-name").text())
            data += '"Provider Name:","' + $("#provider-name").text() +'"\n';
        else
            data += '"Provider Name:"," All"\n';
        data += '"Total number of plans:","' + $("#num-plans").text() +'"\n';
        data += '"Total number of unique users that completed SmartPlan:","' + $("#unique-users-smartplan").text() +'"\n';
        data += '"Total number of new enrollees:","' + $("#new-enrollees").text() +'"\n';
        data += '"Average deferral new enrollees:","' + $("#avg-deferral-new-enrollees").text() +'"\n';
        data += '"Average salary","' + $("#avg-salary").text() +'"\n';
        data += '"Existing participant average entering deferral rate:","' + $("#entering-deferral-rate").text() +'"\n';
        data += '"Existing participant average exiting deferral rate:","' + $("#exiting-deferral-rate").text() +'"\n';
        data += '"Average Delta Entering vs Exiting SmartPlan:","' + $("#delta-entering-vs-exiting").text() +'"\n';
        data += '"Average account balance:","' + $("#avg-account").text() +'"\n';
        data += '"Percentage of participants choosing each investment type:","' + '"Advice: ' + $("#advice").text() + ' Custom: ' + $("#custom").text() + ' Default: ' + $("#default").text() + ' Risk: ' + $("#risk").text() + ' Target: ' + $("#target").text()+'"\n';
        data += '"Total outside assets:","' + $("#total-outside-assets").text() +'"\n';
        data += '"Number of users with IRA\'s:","' + $("#users-ira").text() +'"\n';
        data += '"Number of users with former retirement plan:","' + $("#users-former-plan").text() +'"\n';
        data += '"Top browser:","' + $("#top-browser").text() +'"\n';
        data += '"Percentage of users who transacted between 12 - 8 am:","' + $("#12-8-transacted").text() +'"\n';
        data += '"Percentage of users who transacted between 8 am - 4 pm:","' + $("#8-4-transacted").text() +'"\n';
        data += '"Percentage of users who transacted between 4 pm - 11:59 pm:","' + $("#4-1159-transacted").text() +'"\n';
        data += '"Viewed Plan Basics:","' + $("#plan-basics").text() +'"\n';
        data += '"Completed RN:","' + $("#retirement-needs").text() +'"\n';
        data += '"Completed RTQ:","' + $("#risk-profile").text() +'"\n';
        data += '"Completed Investments:","' + $("#investments").text() +'"\n';
        data += '"Completed Contributions:","' + $("#contributions").text() +'"\n';


        var csvContent = "data:text/csv;charset=utf-8,";
        csvContent += data;

        var encodedUri = encodeURI(csvContent);
        window.open(encodedUri);

    });

    $('#download-pdf').click(function() {
        var params = window.location.href.split("/");
        params = params[params.length - 1];
        var pdfUrl = "/admin/settings/generatePdf/" + params;
        window.open(pdfUrl);

    });



});
