$(document).ready(function(){
	startWidget();

	$(document).on('click', '#allocate', function(){
		$('#cta, #projections').hide();
		$('.video-controls').hide();
		$('#confirmation').show();
		$('#cta_allocate').fadeIn(1000);
	});

	$(document).on('click', '#allocate_aca', function(){
		$('#cta, #projections').hide();
		$('.video-controls').hide();
		$('#confirmation').show();
		$('#allocate_confirm').fadeIn(1000);
	});

	$(document).on('change', 'input[name=aca_checkbox]', function() {
		if($(this).is(':checked')) {
			$('#connected').prop('disabled', false);
		}
		else {
			$('#connected').prop('disabled', true);
		}
	});

	$(document).on('click', '#connected', function(){
		// If hasroth is true
		$('#cta, #projections').hide();
		$('.video-controls').hide();
		// else
		$('#cta_allocate').hide();
		$('#allocate_confirm').hide();

		$('#confirmation').show();
		$('#spinner').fadeIn();

		var page = "/widgets/ajax/saveMatch";
		var params = {
			pretax: $('#allocate_pretax output').text(),
			roth: $('#allocate_roth output').text(),
		}

		$.post(page, params).done(function(data)
		{
			var object = JSON.parse(data);

			if (object.successOrPostContributionsOff) {
				if (object.postContributions) {
					if (object.confirmation) {
						$('.data_confirmationCode').html(object.confirmation);
						$('#spinner').hide();
						$('#confirmation .connected').fadeIn(1000);
					}
					else {
						$('#spinner').hide();
						$('#confirmation .error').fadeIn(1000);
					}
				}
				else {
					$('#spinner').hide();
					$('#confirmation .cta').fadeIn(1000);
				}
			}
			else {
				$('#spinner').hide();
				$('#confirmation .error').fadeIn(1000);
			}
		});
	});

	$(document).on('click', '#manual', function(){
		$('#cta, #projections').hide();
		$('.video-controls').hide();
		$('#confirmation').show();
		$('#spinner').fadeIn();
		setTimeout(function(){
			$('#spinner').hide();
			$('#confirmation .manual').fadeIn(1000);
		},3000);
	});
	$(document).on('click', '#connected-aca', function(){
            $("#cta_allocate").hide();
            $("#allocate_confirm").show();
	});
	$(document).on('click', '#connected-submit', function(){
		$('#confirmation .connected').hide();
		$('#spinner').fadeIn();
		setTimeout(function(){
			$('#spinner').hide();
			$('#confirmation .connected-thank-you').fadeIn(1000);
		},3000);
	});


	/* Contribution Allocation Rangeslider (noUiSlider) */
	var maxVal = parseInt($('input[name=maxMatch]').val());
        if (maxVal == 0)
        maxVal = 1;
	var pretax = $('#allocate_pretax output');
	var roth = $('#allocate_roth output');
	var range = document.getElementById('allocate_slider');
	var range_all_sliders = {
		'min': 0,
		'max': maxVal
	};

	if (range) {
		var formatVal = wNumb({
			//decimals: 2
			decimals: 0
		});

		noUiSlider.create(range, {
			start: 0, // Handle start position
			//step: 0.25, // Slider moves in increments of '.25'
			step: 1, // Slider moves in increments of '.25'
			direction: 'rtl',
			format: wNumb({
				//decimals: 2
				decimals: 0
			}),
			range: range_all_sliders,
			pips: { // Show a scale with the slider
				mode: 'positions',
				values: [20,40,60,80],
				density: 5,
				stepped: false
			}
		});

		range.noUiSlider.set(maxVal/2);
		var currentVal = range.noUiSlider.get();
		var diff = formatVal.to(maxVal - currentVal);
		pretax.text(currentVal);
		roth.text(diff);

		range.noUiSlider.on('slide', function( values, handle ) {
                        maxVal = parseInt($('input[name=maxMatch]').val());
			var currentVal = range.noUiSlider.get();
			var diff = formatVal.to(maxVal - currentVal);
			pretax.text(values[handle]);
			roth.text(diff);
		});
	}

});
