$(document).ready(function () {


    /* Change Fine Print Button Layout for very small screens */
    var screenHeight = $('#vwise_powerview').height();
    if (screenHeight <= '460') {
        $('#show-fine-print').addClass('right');
    }

    /* Initial Form Button Action */

    $("#salaryForm").submit(function (event) {
        event.preventDefault();
        $('p.input-error-message').hide();
        var querystring = $("#salaryForm").serialize();
        var page = "/widgets/ajax/saveSalary";

        $.post(page, querystring).done(function (data) {
            var object = JSON.parse(data);

            if (object.success) {
                $('#salary-confirmation').fadeOut();
                $('.nav h5').fadeIn();
                $('.nav').toggleClass('lonely hidden').fadeIn();
                $('#languageSelect').hide();
                $('#show-fine-print').removeClass('right');
                sendGoogleAnalyticsRequest();

                var object = JSON.parse(data);
                if ($("allocate_slider").length) {
                    if ($("#partDeferralSetting").val() == "dollar") 
                        $('input[name=maxMatch]').val(object.maxMatchFlatDollarRaw);
                    else 
                        $('input[name=maxMatch]').val(object.maxMatch);
                    if (parseInt($('input[name=maxMatch]').val()) <= 0) 
                        $('input[name=maxMatch]').val(1);
                    document
                        .getElementById('allocate_slider')
                        .noUiSlider
                        .updateOptions({
                            range: {
                                'max': parseInt($('input[name=maxMatch]').val()),
                                'min': 0
                            },
                            start: parseInt($('input[name=maxMatch]').val())
                        });
                    document
                        .getElementById('allocate_slider')
                        .noUiSlider
                        .set($('input[name=maxMatch]').val() / 2);
                    $('#allocate_pretax output').val($('input[name=maxMatch]').val() / 2);
                    $('#allocate_roth output').val($('input[name=maxMatch]').val() / 2);
                }
                $.each(object, function (key, value) {
                    $('.data_' + key).html(value);
                });

                $('.odometer').each(function (k, v) {
                    var value = $(v).html();
                    value = parseFloat(value.replace(/,/g, ''));
                    $(v).attr('data-odo-end', value);
                    $(v).html(0);
                });
            }
            else {
                $('input[name=salary]').addClass('input-error');
                $('p.input-error-message').html(object.message);
                $('p.input-error-message').fadeIn();

                setTimeout(function () {
                    $('p.input-error-message').fadeOut();
                }, 2500);
            }
        });
    });

    $('.flexslider').flexslider({
        animation: "slide",
        selector: ".slides > .slide",
        animationLoop: false,
        slideshow: false,
        controlNav: false,
        directionNav: true,
        keyboard: false,
        customDirectionNav: $(".nav img"),
        after: function () {
            if (!$('.flex-active-slide').is(':first-child') && !$('.flex-active-slide').is(':last-child')) {
                $('.nav').removeClass('lonely');
                $('.nav h5').hide();
                $('#prev-button').fadeIn();
            } else if ($('.flex-active-slide').is(':first-of-type')) {
                $('.nav').addClass('lonely');
                $('.nav h5').show();
                $('#prev-button').fadeOut();
            }
            $('.flex-active-slide .odometer').each(function () {
                var number = $(this).data('odo-end');
                var subWidth = $(this).next('sub').width();
                var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
                $(this).animateNumber({
                    number: number,
                    numberStep: comma_separator_number_step
                }, 1500);
                if (number >= '1000000' || (number >= '10000' && subWidth >= '98') || (number >= '1000' && subWidth >= '100')) {
                    $(this).next('sub').addClass('split');
                    $(this).parent('.figure').css('height', '24vw');
                }
            });
        },
        end: function () {
            $('.nav').fadeOut();
        },
        before: function () {
            $('.nav').fadeIn();
        },
        start: function () {
            $('#prev-button').fadeOut();
            $('.nav h5').fadeIn();
            $('.nav').fadeIn();
        }
    });

    $(document).on('click', '.completed', function () {
        redirect(true);
        return false;
    });
    $(document).on('click', '.return', function () {
        $('#vwise_powerview').fadeOut();
        redirect(false);
    });

    $(".emailTemplateForm").submit(function (event) {
        event.preventDefault();
        var querystring = $(this).serialize();
        var page = "/widgets/ajax/emailTemplate/";

        $.post(page, querystring).done(function (data) {
            var obj = $.parseJSON(data);

            if (obj.success) {
                $('.confirmation').hide();
                $('#spinner').fadeIn();

                $('#spinner').hide();
                $('.confirmation.connected-thank-you').fadeIn(1000);
                setTimeout(function () {
                    redirect(true);
                }, 2000);
            }
            else {
                alert(obj.message);
            }
        });
    });

    $(document).on('click', '#fine-print button', function () {
        $('#fine-print').fadeOut();
    });

    $(document).on('click', '#show-fine-print', function () {
        $('#fine-print').fadeIn();
    });

    $(document).on('click', 'input[type=text],input[type=email]', function () {
        $(this).select();
    });

    /* Language Selector */

    var $LS = $('#languageSelect');
    var $LSopt = $LS.find('.options li');
    var screenHeight = $('#vwise_powerview').height();

    var openLS = function () {
        $LS.addClass('open');
        $LS.find('.menu .options').slideDown();
    };

    var closeLS = function () {
        $LS.removeClass('open');
        $LS.find('.menu .options').slideUp();
    };

    $LSopt.click(function () {
        $LS.find('.selected').removeClass('selected');
        $(this).addClass('selected');
        var currentLanguage = $(this).text();
        var code = $(this).data('language');
        $('.currentLanguage').html(currentLanguage + ' <span></span>');
        $('.currentLanguage').data('language', code);
    });

    if (screenHeight > 460) {
        $LS.click(function () {
            if ($LS.hasClass('open')) {
                var lang = $('.currentLanguage').data('language').trim();
                window.location = UpdateQueryString('lang', lang);
                closeLS();
            } else {
                openLS();
                setTimeout(closeLS, 10000); // auto close after 10 seconds
            }
        });
    } else {
        $LSopt.click(function () {
            var lang = $('.currentLanguage').data('language').trim();
            window.location = UpdateQueryString('lang', lang);
        });
    }

    $('#salary-confirmation .label, #salary-confirmation input, #salary-confirmation .dropdown, #salary-confirmation select, #salary-confirmation h2').click(function () {
        if ($LS.hasClass('open')) {
            closeLS(); // close if user clicks elsewhere in the form\
        }
    });

    /* END Language Selector */


    function UpdateQueryString(key, value, url) {
        if (!url) url = window.location.href;
        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
            hash;

        if (re.test(url)) {
            if (typeof value !== 'undefined' && value !== null)
                return url.replace(re, '$1' + key + "=" + value + '$2$3');
            else {
                hash = url.split('#');
                url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
                if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                    url += '#' + hash[1];
                return url;
            }
        }
        else {
            if (typeof value !== 'undefined' && value !== null) {
                var separator = url.indexOf('?') !== -1 ? '&' : '?';
                hash = url.split('#');
                url = hash[0] + separator + key + '=' + value;
                if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                    url += '#' + hash[1];
                return url;
            }
            else
                return url;
        }
    }


    $('input.currency').autoNumeric('init', {lZero: 'deny', mDec: 0, aPad: false});
});
