$(document).ready(function () {
    var controls = $('.video-controls');
    var player = $('video');
    var video = player[0];
    var playButton = controls.find('.play');
    var pauseButton = controls.find('.pause');
    var seekBar = controls.find('input[name=seek]');
    var progressBar = controls.find('.progress-bar > span');
    var indicator = controls.find('.indicator');

    $("#salaryForm").submit(function (event) {
        event.preventDefault();
        $('p.input-error-message').hide();
        $('input[name=salary]').removeClass('input-error');
        $('p.input-error-message').html('');

        var querystring = $("#salaryForm").serialize();
        var page = "/widgets/ajax/saveSalary";

        $.post(page, querystring).done(function (data) {
            var object = JSON.parse(data);

            if (object.success) {
                $('.container').removeClass('hold');
                $('#languageSelect').addClass('dark');
                $('#salary-confirmation').fadeOut();
                sendGoogleAnalyticsRequest();
                //$('.video-controls').removeClass('show');
                video.play();
                pauseButton.show();
                playButton.hide();
                if ($("#partDeferralSetting").val() == "dollar") 
                    $('input[name=maxMatch]').val(object.maxMatchFlatDollarRaw);
                else 
                    $('input[name=maxMatch]').val(object.maxMatch);
                if (parseInt($('input[name=maxMatch]').val()) <= 0) 
                    $('input[name=maxMatch]').val(1);
                $.each(object, function (key, value) {
                    var obj = $('.data_' + key);

                    obj.html(value);

                    if (key == 'projectedIncome') {
                        obj.attr('data-odo-start', object.balance);
                        obj.attr('data-odo-finish', value);
                        obj.html(object.balance);
                    }

                    if (key == 'projectedMonthlyIncome') {
                        obj.attr('data-odo-finish', value);
                        obj.html(0);
                    }
                });
                if ($("allocate_slider").length) {
                    document
                        .getElementById('allocate_slider')
                        .noUiSlider
                        .updateOptions({
                            range: {
                                'max': parseInt($('input[name=maxMatch]').val()),
                                'min': 0
                            },
                            start: parseInt($('input[name=maxMatch]').val())
                        });
                    document
                        .getElementById('allocate_slider')
                        .noUiSlider
                        .set($('input[name=maxMatch]').val() / 2);
                    $('#allocate_pretax output').val($('input[name=maxMatch]').val() / 2);
                    $('#allocate_roth output').val($('input[name=maxMatch]').val() / 2);
                }
                var yc_fill = $('#your-contribution').find('.fill');
                var yc_odo = $('#your-contribution').find('.odometer');
                var yc_change = (object.currentYTDTotalRaw / object.matchSelfRaw * 100) + '%';

                $(yc_fill).attr('data-start-width', yc_change);
                $(yc_fill).css('width', yc_change);

                $(yc_odo).attr('data-odo-start', object.currentYTDTotal);
                $(yc_odo).attr('data-odo-finish', object.matchSelf);
                $(yc_odo).html(object.currentYTDTotal);

                var ec_fill = $('#employer-contribution').find('.fill');
                var ec_odo = $('#employer-contribution').find('.odometer');
                var ec_change = (object.currentYTDMatchRaw / object.matchEmployerRaw * 100) + '%';

                $(ec_fill).attr('data-start-width', ec_change);
                $(ec_fill).css('width', ec_change);

                $(ec_odo).attr('data-odo-start', object.currentYTDMatch);
                $(ec_odo).attr('data-odo-finish', object.matchEmployer);
                $(ec_odo).html(object.currentYTDMatch);

            }
            else {
                $('input[name=salary]').addClass('input-error');
                $('p.input-error-message').show();
                $('p.input-error-message').html(object.message);
            }
        });
    });

    $('.emailTemplateForm').submit(function (event) {
        event.preventDefault();

        var querystring = $(this).serialize();
        var page = '/widgets/ajax/emailTemplate/';

        $.post(page, querystring).done(function (data) {
            var obj = $.parseJSON(data);

            if (obj.success) {
                $('.confirmation').hide();
                $('#spinner').fadeIn();

                $('#spinner').hide();
                $('.confirmation.connected-thank-you').fadeIn(1000);
                setTimeout(function () {
                    redirect(true);
                }, 2000);
            }
            else {
                alert(obj.message);
            }
        });
    });
});

$('input.currency').autoNumeric('init', {lZero: 'deny', mDec: 0, aPad: false, vMin: 0});
$('input.age').autoNumeric('init', {lZero: 'deny', mDec: 0, aPad: false, aSep: '', vMin: 0, vMax: 120});
