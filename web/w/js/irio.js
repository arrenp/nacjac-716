var transitionSpeed = 300;
var autoDeferralStatus = 1;

$(document).ready(function(){
	startWidget();

	var touch = 'ontouchstart' in document.documentElement;
	(touch) ? $('body').addClass('touch') : $('body').addClass('no-touch');

	$('.vwise button').click(function(){
		$('.veil').fadeIn(transitionSpeed);
		$('body').addClass('no-scroll');
	});

	$('.close').click(function(){
		closeWidget();
	});

	$('.refresh').click(function(){
		location.reload();
	});

	$('.close-layer,#close-disclosure').click(function(){
		$('#disclosure').fadeOut(transitionSpeed);
		$('.close').show();
	});

	$(document).keyup(function(e){
		if (e.keyCode == 27) { // esc key
			closeWidget();
		}
	});

	$('.showFinePrint').click(function(){
		$('#disclosure').fadeIn(transitionSpeed);
	});



	/* Language Selector */

  var $LS = $('#languageSelect');
  var $LSopt = $LS.find('.options li');

  var openLS = function() {
    $LS.addClass('open');
    $LS.find('.menu .options').slideDown();
  };

  var closeLS = function() {
    $LS.removeClass('open');
    $LS.find('.menu .options').slideUp();
  };

  $LS.click(function(){
    if ( $LS.hasClass('open') ) {
		var lang = $('.currentLanguage').text().trim() == 'Spanish' ? 'es' : 'en';
                if ($("#ajaxContentIrio").length)
                {
                    var params = new Object();
                    params.lang= lang;
                    params.sid = widgetSessionId;
                    launchIrio(params);                    
                }
                else
                {
                    window.location = UpdateQueryString('lang', lang);
                }
                
      closeLS();
    } else {
      openLS();
      setTimeout(closeLS, 10000); // auto close after 10 seconds
    }
  });

  $('#salary-confirmation .col.summary').click(function(){
    if ( $LS.hasClass('open') ) {
      closeLS(); // close if user clicks elsewhere in the form\
    }
  });

  $LSopt.click(function(){
    $LS.find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var currentLanguage = $(this).text();
    $('.currentLanguage').html( currentLanguage + ' <span></span>');
  });

	/* END Language Selector */


	/* Contribution Allocation Rangeslider (noUiSlider) */

	//var maxVal = $('#allocate .data_newDeferral').text();
	var maxVal = $('#allocate_slider').data('max');
	var pretax = $('#allocate_pretax output');
	var roth = $('#allocate_roth output');
	var range = document.getElementById('allocate_slider');
	var range_all_sliders = {
		'min': 0,
		'max': 5
	};
	var formatVal = wNumb({
		//decimals: 2
		decimals: 0
	});

	if (range) {
		noUiSlider.create(range, {
			start: 0, // Handle start position
			//step: 0.25, // Slider moves in increments of '.25'
			step: 1, // Slider moves in increments of '.25'
			direction: 'rtl',
			format: wNumb({
				//decimals: 2
				decimals: 0
			}),
			range: range_all_sliders,
			pips: { // Show a scale with the slider
				mode: 'positions',
				values: [20,40,60,80],
				density: 5,
				stepped: false
			}
		});

		//range.noUiSlider.set(maxVal);
		setSliderRange(0,maxVal);

		var currentVal = range.noUiSlider.get();
		var diff = formatVal.to(maxVal - currentVal);
		pretax.text(currentVal);
		roth.text(diff);
		//console.log('Setup // maxVal')


		range.noUiSlider.on('slide', function( values, handle ) {
			//var maxVal =
			var currentVal = range.noUiSlider.get();
			var diff = formatVal.to(maxVal - currentVal);
			pretax.text(values[handle]);
			roth.text(diff);
		});
	}

	function setSliderRange ( min, max ) {
		range.noUiSlider.updateOptions({
			start: max,
			range: {
				'min': min,
				'max': max
			}
		});
	}
	/* END Contribution Slider */



	$('.modal .details h4').click(function(){
		$('#details-form').slideToggle(300);
		$(this).toggleClass('open');
	});

	/*
	$('#confirm-salary').click(function(){
		$('*').scrollTop(0);
		if ($('input[name=salary]').val() == 'max') {
			$('#salary-confirmation').hide();
			$('#max-reached').fadeIn(transitionSpeed);
		} else {
			$('#salary-confirmation').hide();
			$('#analysis').fadeIn(transitionSpeed);
		}
	});
	*/

	$('#blocked-button').click(function(){
		$('*').scrollTop(0);
		$('#analysis').hide();
		//$('#blocked').fadeIn(transitionSpeed);
		// Temporarily show Slider from this button...
		$('#allocate').fadeIn(transitionSpeed);
	});

	$('#congrats .confirm').click(function(){
        $('#spinner').fadeIn();
        setTimeout(function(){
                $('#spinner').hide();
                $('#congrats').hide();
                var showid = "allocate";
                if ($('#allocate_confirm').length)
                showid = "allocate_confirm";
                $('#' + showid).fadeIn(transitionSpeed, function() {
                    sliderValues();
                });
        },1500);
	});

	$('#congrats .allocate_aca').click(function(){
        $('#spinner').fadeIn();
        setTimeout(function(){
                $('#spinner').hide();
                $('#congrats').hide();
                $('#allocate_confirm').fadeIn(transitionSpeed, function() {
                    sliderValues();
                });
        },1500);
	});

	$('#allocate .confirm, #allocate_confirm .confirm').click(function(){
            if (parseInt($('#allocate_pretax output').text()) > 0 && parseInt($('#allocate_roth output').text()) && $(this).attr("id") == "acacontinue")
            {
                $('#spinner').fadeIn();
                setTimeout(function(){
                        $("#allocate_confirm").hide();
                        $('#spinner').hide();
                        $('#congrats').hide();
                        $('#allocate').fadeIn(transitionSpeed, function() {
                            sliderValues();
                        });
                },1500);
                return true;
            }
		$('#congrats').hide();
        $('#allocate_confirm #spinner').fadeIn();
        $('#allocate #spinner').fadeIn();
        var page = widgetHost +"/widgets/ajax/saveMatch";
        var params = {
                pretax: $('#allocate_pretax output').text(),
                roth: $('#allocate_roth output').text(),
                sid:widgetSessionId
        }

            $.ajax({
                url: page,
                xhrFields: {
                   withCredentials: true
                },
                type:"POST",
                data:params
            }).done(function( data ) {
                var object = JSON.parse(data);

                if (object.successOrPostContributionsOff) {
                        $('#allocate').hide();
                        $('#allocate_confirm').hide();
                        $('#spinner').hide();
                        $('#allocate_confirm #spinner').hide();
                        $('#allocate #spinner').hide();

                        if (object.postContributions) {
                                $('#success').fadeIn(transitionSpeed);
                                $('.data_confirmationCode').html(object.confirmation);
                        }
                        else {
                                $('#cta').fadeIn(transitionSpeed);
                        }
                }
                else {
                        alert(object.message);
                }
        });
	});

	$('#congrats .connected').click(function(){
        $('#spinner').fadeIn();
        var page = widgetHost + "/widgets/ajax/saveMatch";

            $.ajax({
                url: page,
                xhrFields: {
                   withCredentials: true
                },
                data: {sid:widgetSessionId },
                type:"POST"
            }).done(function( data ) {
                var object = JSON.parse(data);

                if (object.successOrPostContributionsOff) {
						$('#congrats').hide();
                        $('#spinner').hide();

                        if (object.postContributions) {
                                $('#success').fadeIn(transitionSpeed);
                                $('.data_confirmationCode').html(object.confirmation);
                        }
                        else {
                                $('#cta').fadeIn(transitionSpeed);
                        }
                }
                else {
                        alert(object.message);
                }
        });
	});

	$('#congrats .back,#allocate_confirm .back').click(function(){
		$('*').scrollTop(0);
		$('#congrats').hide();
                $("#allocate_confirm").hide();
		$('#analysis').fadeIn(transitionSpeed);
	});

	$('#cta .prev').click(function(){
		$('*').scrollTop(0);
		$('#cta').hide();
		$('#analysis').fadeIn(transitionSpeed);
	});


	$('#allocate .back').click(function(){
		$('*').scrollTop(0);
		$('#allocate').hide();
		$('#analysis').fadeIn(transitionSpeed);
	});

        function sliderValues()
        {
            if (range != null)
            {
                maxVal = calculateIrioSliderMax();
                setSliderRange(0,maxVal);                
                range.noUiSlider.set(calculateIrioSliderDefault());
                var currentVal = range.noUiSlider.get();
                var diff = formatVal.to(maxVal - currentVal);
                pretax.text(currentVal);
                roth.text(diff);
            }
        }

});

$(document).on('change', 'input[name=aca_checkbox]', function() {
		if($(this).is(':checked')) {
                        $('#allocate_confirm .confirm').addClass("bg-primary");
			$('#allocate_confirm .confirm').prop('disabled', false);
		}
		else {
                        $('#allocate_confirm .confirm').removeClass("bg-primary");
			$('#allocate_confirm .confirm').prop('disabled', true);
		}
	});


