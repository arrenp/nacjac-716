$(document).ready(function(){
	startWidget();

  /* CTA Button Actions */

	$(document).on('change', 'input[name=aca_checkbox]', function() {
		if($(this).is(':checked')) {
			$('.aca-button').prop('disabled', false);
		}
		else {
			$('.aca-button').prop('disabled', true);
		}
	});

	$(document).on('click', '#allocate', function(){
		$('.flexslider, #prev-button, #next-button').fadeOut();
		$('#spinner').fadeIn();
		setTimeout(function(){
			$('#spinner').hide();
			$('.allocate-contribution').fadeIn(1000);
		},3000);
	});
	$(document).on('click', '#allocateaca', function(){
		$('.flexslider, #prev-button, #next-button').fadeOut();
		$('#spinner').fadeIn();
		setTimeout(function(){
			$('#spinner').hide();
			$('.allocate_confirm').fadeIn(1000);
		},3000);
	});

  $(document).on('click', '#connected,#allocatefinish', function(){
    if (this.id == "allocatefinish")
    {
        $('.confirmation.allocate_confirm, .flexslider, #prev-button, #next-button').fadeOut();
    }
    else
    {
        $("#allocateForm").hide();
    }
    $('#spinner').fadeIn();

		var page = "/widgets/ajax/saveMatch";
		var params = {

			pretax: $('#allocate_pretax output').text(),

			roth: $('#allocate_roth output').text(),

		};
		$.post(page,params).done(function(data)
		{
			var object = JSON.parse(data);

			if (object.successOrPostContributionsOff) {
				if (object.postContributions) {
					if (object.confirmation) {
						$('.data_confirmationCode').html(object.confirmation);
						$('#spinner').hide();
						$('.confirmation.connected').fadeIn(1000);
					}
					else {
						$('#spinner').hide();
						$('.confirmation.error').fadeIn(1000);
					}
				}
				else {
					$('#spinner').hide();
					$('.confirmation.cta').fadeIn(1000);
				}
			}
			else {
				$('#spinner').hide();
				$('.confirmation.error').fadeIn(1000);
			}
	  });
  });

  $(document).on('click', '#manual', function(){
    $('.flexslider, #prev-button, #next-button').fadeOut();
    $('#spinner').fadeIn();
    setTimeout(function(){
      $('#spinner').hide();
      $('.confirmation.manual').fadeIn(1000);
    },3000);
  });


/* Contribution Allocation Rangeslider (noUiSlider) */

	var maxVal = parseInt($('input[name=maxMatch]').val());
        if (maxVal == 0)
        maxVal = 1;        
	var pretax = $('#allocate_pretax output');
	var roth = $('#allocate_roth output');
	var range = document.getElementById('allocate_slider');
	var range_all_sliders = {
		'min': 0,
		'max': maxVal
	};
	var formatVal = wNumb({
		//decimals: 2
		decimals: 0
	});

	noUiSlider.create(range, {
		start: 0, // Handle start position
		//step: 0.25, // Slider moves in increments of '.25'
		step: 1, // Slider moves in increments of '.25'
		format: wNumb({
			//decimals: 2
			decimals: 0
		}),
		range: range_all_sliders,
		pips: { // Show a scale with the slider
			mode: 'positions',
			values: [20,40,60,80],
			density: 5,
			stepped: false
		}
	});

	range.noUiSlider.set(maxVal/2);
	var currentVal = range.noUiSlider.get();
	var diff = formatVal.to(maxVal - currentVal);
	pretax.text(currentVal);
	roth.text(diff);

	range.noUiSlider.on('update', function( values, handle ) {
                maxVal = parseInt($('input[name=maxMatch]').val());
		var currentVal = range.noUiSlider.get();
		var diff = formatVal.to(maxVal - currentVal);
		roth.text(values[handle]);
		pretax.text(diff);
	});
});
