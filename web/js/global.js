function getAjaxVariable()//placeholder for manage until manage finished, replace with jquery post
{
    var ajaxRequest;
    try
    {
         // Opera 8.0+, Firefox, Safari
        ajaxRequest = new XMLHttpRequest();
    } 
    catch (e)
    {
        // Internet Explorer Browsers
        try
        {
         ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } 
        catch (e)
        {
            try
            {
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } 
            catch (e)
            {
                // Something went wrong
                //alert("Your browser broke!");
                return false;
            }
        }
    }

    if (ajaxRequest == false)
    {
        alert('Your browser does not support ajax')
        return false;
    }

    return ajaxRequest;
}
function postAjax(ajaxRequest,page,querystring)//placeholder for manage until manage finished, replace with jquery post
{
    ajaxRequest.open("POST", page , true);
    ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajaxRequest.setRequestHeader("Content-length", querystring.length);
    ajaxRequest.setRequestHeader("Connection", "close");
    ajaxRequest.send(querystring);
}

function customParseFloat(id,signed)
{
  if(typeof(signed)==='undefined')
  signed = "signed";
  $("#" +id).val(parseFloat($("#" +id).val()));
  if ($("#" +id).val() == "NaN" ||  ($("#" +id).val() < 0 && signed == "unsigned"))
  $("#" +id).val(0);
}

function customParseInt(id,signed)
{
  if(typeof(signed)==='undefined')
  signed = "signed";
  $("#" +id).val(parseInt($("#" +id).val()));
  if ($("#" +id).val() == "NaN" ||  ($("#" +id).val() < 0 && signed == "unsigned"))
  $("#" +id).val(0);
}

function maxLimit(id,max)
{
  if (parseInt($("#" +id).val()) > max)
  $("#" +id).val(0);
}

function assignCheckboxValue(object)
{
    if (object.checked)
    object.value = 1;
    else
    object.value = 0;
}

function togglePills(elements,selectedElement)
{
    for (var i = 0; i < elements.length; i++)
    {
        document.getElementById(elements[i]).style.display = 'none';
        document.getElementById(elements[i] + "Pill").className = '';
    }
    document.getElementById(selectedElement).style.display = 'block';
    document.getElementById(selectedElement + "Pill").className = 'active';
    $.placeholder.shim();
}

function select_all(id)
{

    var $this = $("#" + id);
    $this.select();

    // Work around Chrome's little problem
    $this.mouseup(function() 
    {
        $this.unbind("mouseup");
        return false;
    });

}

function closeLightBox()
{
  

  $("#ajaxModule").modal("hide");
}

function simpleSearch(searchVariable,id)
{
 var searchedValue = $("#" + id + "Search").val();
 for (var i = 0; i < searchVariable.length;i++)
 {
  var searchFields =  searchVariable[i];
  var valid = false;
  for (var key in searchFields)
  {
    if (key != "id")
    if (searchFields[key].toLowerCase().lastIndexOf(searchedValue.toLowerCase()) != -1 || searchedValue == "")
    valid = true;       
  }
  if (valid)
  $("#" + id + searchFields['id']).show();
  else
  $("#" + id + searchFields['id']).hide();
 }

}


function showalert(id)
{
	$("#" + id).show();
	$('html, body').animate({ scrollTop: 0 }, 0);
}

function showAlertFor(id, timeout) {
    $('#' + id).show();
    $('html, body').animate({ scrollTop: 0 }, 0);

    setTimeout(function(){
        $('#' + id).slideUp();
    },timeout);
}

function showAlertForWithoutScrollUp(id,timeout)//scroll up bad 4 u
{
    var timeout = typeof timeout == 'undefined' ? 3000 : timeout;
    $('#' + id).show();
    setTimeout(function()
    {
        $('#' + id).slideUp();
    },timeout);
}

function toggleSection(section,copyvalue)
{
  if(typeof(copyvalue)==='undefined')
  copyvalue = true;
  var checkedSections = $("." + section + ":checked");
  if (checkedSections.length > 0)
  {
    if (copyvalue)
    $("." + section).val(0);
    $("." + section).prop("checked",false);
  }
  else
  {
    if (copyvalue)
    $("." + section).val(1);
    $("." + section).prop("checked",true);
  }
}

String.prototype.replaceAll = function (stringToFind, stringToReplace) {
    if (stringToFind === stringToReplace) return this;
    var temp = this;
    var index = temp.indexOf(stringToFind);
    while (index != -1) {
        temp = temp.replace(stringToFind, stringToReplace);
        index = temp.indexOf(stringToFind);
    }
    return temp;
};
function modalMessage(message)
{
  $("#messageModule").modal("show");
  $('#messageModuleContent').html(message);
}
function jsonResponseModal(data,boxtype)
{
    if (typeof boxtype === "undefined")
    boxtype = "modal";
    var print = "";
    var seperate = "<br/>";
    if (boxtype != "modal")
    seperate = "\n";
    $.each( data, function( key, value ) 
    {
        print = print + value + seperate;
    });
    if (boxtype == "modal")
    modalMessage(print);
    else
    alert(print);
}