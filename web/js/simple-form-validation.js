$(document).ready(function() {
    $('#simpleFormExample').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            companyLogo: {
                validators: {
                    file: {
                        extension: 'jpeg,png,jpg',
                        type: 'image/jpeg,image/png',
                        maxSize: 2000000,
                        maxFiles: 1,
                        message: 'Please provide a png or jpg image 2MB or smaller'
                    }
                }
            },
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The username must be between 6 and 30 characters'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'The username can only consist of letters, numbers and underscores'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email is required'
                    },
                    emailAddress: {
                        message: 'This is not a valid email address'
                    }
                }
            },
            zip: {
                validators: {
                    regexp: {
                        regexp: /^\d{5}$/,
                        message: 'Please provide a valid 5 digit US zipcode'
                    }
                }
            },
            phone: {
                validators: {
                    phone: {
                        country: 'US',
                        message: 'Please provide a valid US phone number'
                    }
                }
            },
            altPhone: {
                validators: {
                    phone: {
                        country: 'US',
                        message: 'Please provide a valid US phone number'
                    }
                }
            }
        }
    });
});