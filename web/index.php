<?php
use Symfony\Component\HttpFoundation\Request;
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
// all allowed origins must be entered in lowercase
$allowedOrigins = [
    "https://spe2.local",
    "http://ameritas.infinity.com",
    "https://ameritas.infinity.com",
    "https://ameritas.omniasp.com",
    "https://service.ameritas.com",
    "http://widget.dev.com",
    "http://ameritas.local",
    "http://ameritas.demo.smartplanenterprise.com",
    "http://flatdoor.demo.smartplanenterprise.com",
    "http://opendoor.demo.smartplanenterprise.com",
    "https://testameritas.omniasp.com"
];
if (in_array(strtolower($_SERVER["HTTP_ORIGIN"]), $allowedOrigins))
{
    header("Access-Control-Allow-Origin: ".$_SERVER["HTTP_ORIGIN"]);
    header('Access-Control-Allow-Credentials: true');
}
require __DIR__.'/../vendor/autoload.php';
umask(0000);
$mode = 'prod';

$_SERVER['HTTPS']='on';
$_SERVER['SERVER_PORT']=443;

if($mode === 'prod') {
    $key = '2011';
    if ((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') || strpos($_SERVER['REQUEST_URI'],'_profiler') !== false ) {
        session_start();
        if (isset($_SESSION['_sf2_attributes']['REQUEST']['debug']) && $_SESSION['_sf2_attributes']['REQUEST']['debug'] == $key) {
            $mode = 'dev';
        }
    } elseif (isset($_GET['debug']) && ($_GET['debug'] == $key)) {
        $mode = 'dev';
    }
}
$errorReporting = true;
if ($mode == "prod")
{
    //$errorReporting = false;
    error_reporting(0);
}
$kernel = new AppKernel($mode, $errorReporting);
if (PHP_VERSION_ID < 70000) {
    $kernel->loadClassCache();
}
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
?>
