<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;;
//use Rollbar;
class AppKernel extends Kernel
{
    public function registerBundles()
    {

        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
        	new Symfony\Bundle\DebugBundle\DebugBundle(),
        	new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
       		new Index\IndexBundle\IndexIndexBundle(),
            new classes\classBundle\classesclassBundle(),
            new Portal\PortalBundle\PortalPortalBundle(),
            new Templates\TemplatesBundle\TemplatesTemplatesBundle(),
            new Account\AccountBundle\AccountAccountBundle(),
            new Settings\SettingsBundle\SettingsSettingsBundle(),
            new Utilities\UploaderBundle\UtilitiesUploaderBundle(),
            new Plans\PlansBundle\PlansPlansBundle(),
            new Paths\PathsBundle\PathsPathsBundle(),
            new Recordkeepers\ReliusBundle\RecordkeepersReliusBundle(),
            new Recordkeepers\SRTBundle\RecordkeepersSRTBundle(),
            new Recordkeepers\EnvisageBundle\RecordkeepersEnvisageBundle(),
            new Recordkeepers\ExpertPlanBundle\RecordkeepersExpertPlanBundle(),
            new Shared\RecordkeepersBundle\SharedRecordkeepersBundle(),
            new Shared\InvestmentsBundle\SharedInvestmentsBundle(),
            new Shared\ContactBundle\SharedContactBundle(),
            new Shared\PrintMaterialsBundle\SharedPrintMaterialsBundle(),
            new Shared\vadsBundle\SharedvadsBundle(),
            new Support\SupportBundle\SupportSupportBundle(),
            new Shared\DocumentsBundle\SharedDocumentsBundle(),
            new Shared\OutreachBundle\SharedOutreachBundle(),
            new Spe\AppBundle\SpeAppBundle(),
            new Manage\ManageBundle\ManageManageBundle(),
            new Shared\PromoDocsBundle\SharedPromoDocsBundle(),
            new Shared\MessagingBundle\SharedMessagingBundle(),
            new Bootstrap\TestBundle\BootstrapTestBundle(),
            new Braincrafted\Bundle\BootstrapBundle\BraincraftedBootstrapBundle(),
            new Permissions\RolesBundle\PermissionsRolesBundle(),
            new Sessions\AdminBundle\SessionsAdminBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            new Shared\ProfilesBundle\SharedProfilesBundle(),
            new WidgetsBundle\WidgetsBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new AdminStats\AdminStatsBundle\AdminStatsAdminStatsBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
            new HubSpot\HubSpotBundle\HubSpotHubSpotBundle(),
            new Shared\TranslationBundle\SharedTranslationBundle(),
            new Shared\InvestorProfileBundle\SharedInvestorProfileBundle(),
            new Shared\LibraryBundle\SharedLibraryBundle(),
            new ApiBundle\ApiBundle(),
            new Cron\CronBundle\CronCronBundle(),
            new Kinetik\KinetikBundle\KinetikKinetikBundle(),
            new Shared\GeneralBundle\SharedGeneralBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new FOS\OAuthServerBundle\FOSOAuthServerBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Symfony\WebpackEncoreBundle\WebpackEncoreBundle(),
            new Spe\VueBundle\SpeVueBundle()
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {


            
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }
           if (in_array($this->getEnvironment(), array('index'))) {

        }
        //Rollbar::init(array('access_token' => '9421d89825884d84b14e42a9d4a82c70',"included_errno" => 0));
        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
