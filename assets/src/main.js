import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import currency from 'v-currency-field'
import Chart from 'chart.js'
import ChartDataLabels from 'chartjs-plugin-datalabels'
import { VueMaskDirective } from 'v-mask'

Vue.config.productionTip = false
Vue.use(currency)
Vue.directive('mask', VueMaskDirective);


Vue.mixin({
  methods: {
    translate(section, step, label, replace) {
      replace = replace || {};
      let content = this.$store.state.path.sections[section].contents[step][label].content
      for (let key in replace) {
        let replacement = replace[key]

        content = content.replace(key, replacement)
      }
      return content
    },
  },
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
