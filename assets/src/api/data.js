import axios from 'axios'
// import "regenerator-runtime/runtime";
const http = axios.create({
    baseURL: '/api/v2'
});

export default  {
    load(query) {
        return http.post('/default/load', query);
    },
    getData() {
        return http.get('/default/getData');
    },
    initialize() {
        return http.post('/default/initialize');
    },
    saveRetirementNeeds(data) {
        return http.post('/retirementneeds/saveData', data);
    },
    getRetirementNeeds() {
        return http.get('/retirementneeds/getData');
    },
    getResultsData() {
        return http.get('/retirementneeds/resultsData');
    },
    getRiskProfile(request) {
        return http.post('/riskprofile/getRP', request);
    },
    getBeneficiaries(data) {
        return http.get('/myprofile/beneficiaries/all', data);
    },
    saveBeneficiaries(data) {
        return http.post('/myprofile/beneficiaries/save', data);
    },
    deleteBeneficiaries(data) {
        return http.post('/myprofile/beneficiaries/delete', data);
    },
    getRiskScore(data) {
        return http.post('/riskprofile/riskProfileInvestor', data);
    },
    getStadionGraphs(data) {
        return http.post('/riskprofile/getStadionGraphs', data);
    },
    getStadionDisclaimerText(data) {
        return http.post('/riskprofile/getStadionDisclaimerText', data);
    },
    stadionRetireExpress(data) {
        return http.post('/riskprofile/stadionRetireExpress', data);
    },
    getContributions(data) {
        return http.post('/contributions/calculateData', data);
    },
    saveContributions(data) {
        return http.post('/contributions/saveContributions', data)
    },
    saveAutoIncrease(data) {
        return http.post('/contributions/saveAutoIncrease', data)
    },
    getPath(planid,type ="standard",lang = "en") {
        return http.get('/path/getPathPlan/'+planid+'/' + type + "?lang=" + lang );
    },
    setPath(id) {
        return http.post('/path/setPath/', {id: id});
    },
    getProfile(token) {
        return http.post('/myprofile/getProfileData', { cookie: token })
    },
    getInvestmentSections() {
        return http.post('/investments/getInvestmentSections');
    },
    getInvestmentFunds() {
        return http.post('/investments/getFunds');
    },
    getAllInvestmentPortfolios() {
        return http.post('/investments/getPortfolios');
    },
    getInvestmentPortfolioByName($name) {
        return http.post('/investments/getPortfolioByName/' + $name);
    },
    setInvestments(route, request) {
        return http.post(route, request);
    },
    finishInvestments(request) {
        return http.post('/investments/finish', request);
    },
    getUiData() {
        return http.post('/profile/uiData');
    },
    updateTransaction(id) {
        return http.post('/myprofile/updateTransaction', {id: id})
    },
    sendProfileEmail(email) {
        return http.post('/myprofile/sendProfileEmail', {email: email})
    },
    getAvailablePaths(planid)
    {
        return  http.get('/path/getAvailablePaths/' + planid);
    },
    changeLocale(locale)
    {
        return  http.post('/default/changeLocale', {locale: locale});
    },
    updateACAOptOut(ACAOptOut)
    {
        return http.post('/contributions/updateACAOptOut', { ACAOptOut: ACAOptOut });
    },
    getTargetDateFunds()
    {
        return http.get('/investments/getTargetFundsByYearsToRetirementCorrectFormat')
    },
    saveRollover(type, value)
    {
        var obj = {};
        obj[type] = value;
        return http.post('/default/saveRolloverOption', obj)
    },
}
