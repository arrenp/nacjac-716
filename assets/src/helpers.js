export default {
    formatCurrency(n) {
        if(!n){
            return 0;
        }
        n = n.toString().replace(/\$|\,/g, '');
        if (isNaN(n)) n = "0";
        const sign = (n == (n = Math.abs(n)));
        n = Math.round(n).toString();
        for (var i = 0; i < Math.floor((n.length - (1 + i)) / 3); i++)
            n = n.substring(0, n.length - (4 * i + 3)) + ',' + n.substring(n.length - (4 * i + 3));
        return (((sign) ? '' : '-') + n);
    },
    openPopUpLink(link) {
        window.open(link,'_blank','width=800,height=600,top=200,left=200,scrollbars=1,location=0')
    }

}