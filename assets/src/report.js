import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import ProfileReportPage from "./views/ProfileReportPage";

Vue.config.productionTip = false

Vue.mixin({
    methods: {
        translate(section, step, label, replace) {
            replace = replace || {};
            let content = this.$store.state.path.sections[section].contents[step][label].content
            for (let key in replace) {
                let replacement = replace[key] || ''
                content = content.replace(key, replacement)
            }
            return content
        },
    },
})


new Vue({
    components: {
        ProfileReportPage
    },
    router,
    store,
    vuetify,
    el: '#app'
})
