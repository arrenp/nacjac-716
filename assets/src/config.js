export const currency = {
    allowBlank: true,
    defaultValue: 0,
    locale: "en",
    prefix: '$',
    decimalLength: 0,
    suffix: '',
    precision: 0,
    autoDecimalMode: false

}