import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import store from '../store.js'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'mdi'
  },
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary:'#2560A9',
        secondary:'#9e9e9e',
        tertiary:'#012D49',
        accent:'#238798',
        error: '#d34345',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
      },
    },
  },
})
