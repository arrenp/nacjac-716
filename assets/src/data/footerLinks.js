export default [{
  'name': 'SUPPORT',
  'route': '/riskprofile'
}, {
  'name': 'CONTACT',
  'route': '/investments'
}, {
  'name': 'PRIVACY',
  'route': '/contributions'
}, {
  'name': 'TERMS OF USE',
  'route': '/myprofile'
}]
