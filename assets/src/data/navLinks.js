export default [{
  'name': 'PLAN BASICS',
  'route': '/planbasics',
  'index': 1
}, {
  'name': 'RETIREMENT NEEDS',
  'route': '/RN',
  'index': 2
}, {
  'name': 'RISK PROFILE',
  'route': '/riskprofile',
  'index': 3
}, {
  'name': 'INVESTMENTS',
  'route': '/investments',
  'index': 4
}, {
  'name': 'CONTRIBUTIONS',
  'route': '/contributions',
  'index': 5
}, {
  'name': 'MY PROFILE',
  'route': '/myprofile',
  'index': 6
}, {
  'name': 'LIBRARY',
  'route': '/library',
  'index': 7
}]
