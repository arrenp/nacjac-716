import Vue from 'vue'
import vuetify from './plugins/vuetify'
import AdminPreview from "./views/AdminPreview";
import router from "./router";
import store from "./store";
import App from "./App";

Vue.config.productionTip = false

new Vue({
    router,
    store,
    vuetify,
    render: h => h(AdminPreview)
}).$mount('#preview')
