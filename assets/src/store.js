import Vue from 'vue'
import Vuex from 'vuex'
import footerLinks from './data/footerLinks'
import retirementNeedsLinks from './data/retirementNeedsLinks'
import data from './api/data'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        libraryState: null,
        library: {},
        retirementneeds: {
            RN_IncomePct: null,
            RN_EstimatedRetirementIncome: null,
            RN_CurrentYearlyIncome: null,
            RN_EstimatedSocialSecurityIncome: null,
            RN_ReplacementIncome: null,
            RN_NumberOfYearsBeforeRetirement: null,
            RN_InflationAdjustedReplacementIncome: null,
            RN_OtherAssets: null,
            RN_TotalAssets: null,
            RN_AssetTypes: null,
            RN_YearsYouWillLiveInRetirement: null,
            RN_AnnualIncomeNeededInRetirement: null,
            RN_EstimatedSavingsatRetirement: null,
            RN_RecommendedMonthlyPlanContribution: null,
            RN_RetirementAge: null,
            RN_LifeExpectancy: null,
            inflation: null,
            return: null,
            retireNeedsStatus: null,
            replacementIncomePercent: null,
            retirementNeedsFlow: null,
            pensionEstimatorURL: null,
            socialSecurityMultiplier: null,
            RN_EstimatedPensionIncome: null,
            RN_EstimatedMonthlyIncomeAtRetirement: 0
        },
        common: {
            autoNavigation: 0,
            planBalance: 0,
            age: 0,
            autos: {
                autoIncrease: false,
                autoEnroll: false,
                autoEscalate: false,
                defaultInvestment: null,
                autoIncreaseData: {
                    frequency: "Monthly",
                    frequencyEnabled: true,
                    increaseCap: 0,
                    percentLimit: 5,
                    post: 3,
                    pre: 0,
                    roth: 1,
                    startDate: "2019-11-28",
                    autoIncreaseOptIn: false
                },
                autoEscalateData: {
                    pre: null,
                    post: null,
                    roth: null
                },
                autoEnrollData: {
                    pre: null,
                    post: null,
                    roth: null
                }
            }
        },
        serverConfig: {
            estimatedSocialSecurityIncomeMax: 0
        },
        contributions: {
            // C_ACAoptOut: null,
            // C_GetPaid: null,
            // C_PostTaxContributionPct: null,
            // C_PostTaxContributionValue: null,
            // C_PreTaxContributionPct: null,
            // C_PreTaxContributionValue: null,
            // C_RothContributionPct: null,
            // C_RothContributionValue: null,
            // catchup_mode: null,
            // contribution_mode: 'DEFERPCT',
            // contributionsStatus: null,
            // C_MonthlyContributionTotal: null,
            // C_MonthlyContributionTotalEmployer: null,
            // C_CatchUpContributionValue: null,
            // updateCatchUp: null,
            // catchup: false
        },
        selectedDeferrals: {
            prePercent: 0,
            preDollar: 0,
            rothPercent: 0,
            rothDollar: 0,
            postPercent: 0,
            postDollar: 0
        },
        contributionsData: {
            blockPercents: null,
            mode: null,
            payFrequencyValue: null,
            targetSavings: null,
            targetMonthlyIncome: null,
            targetSavingsDisplay: null,
            targetMonthlyIncomeDisplay: null,
            targetMonthlyContribution: null,
            currentYearlyIncome: null,
            deferrals: {
                roth: {
                    tooltip: null,
                    minDollar: null,
                    maxDollar: null,
                    minPercent: null,
                    maxPercent: null,
                    percentStep: null,
                    dollarStep: null,
                    currentPercent: null,
                    currentDollar: null
                },
                pre: {
                    tooltip: null,
                    minDollar: null,
                    maxDollar: null,
                    minPercent: null,
                    maxPercent: null,
                    percentStep: null,
                    dollarStep: null,
                    currentPercent: null,
                    currentDollar: null
                },
                post: {
                    tooltip: null,
                    minDollar: null,
                    maxDollar: null,
                    minPercent: null,
                    maxPercent: null,
                    percentStep: null,
                    dollarStep: null,
                    currentPercent: null,
                    currentDollar: null
                }
            },
            totalContributionPercent: null,
            totalAssets: null,
            yearsBeforeRetirement: null,
            estimatedMonthlyContribution: null,
            projectedSavings: null,
            matchPercent: null,
            matchCap: null,
            secondaryMatchPercent: null,
            secondaryMatchCap: null,
            matchingAmount: null,
            matchingAmountMonthly: null,
            totalSavings: null,
            graphHeight2: null,
            graphHeight: null,
            graphHeightYou: null,
            graphHeightEmployer: null,
            minDeferralDollar: null,
            maxDeferralDollar: null,
            maxDeferral: null,
            minDeferral: null,
            maxMatch: {},
            limit402: null,
            catchupChange: null,
            limit402Monthly: null,
            IRSLimitExceeded: null,
            audioFile: null,
            maxDeferralDollarDisplay: null,
            minDeferralDollarDisplay: null,
            totalSavingsDisplay: null,
            matchingAmountDisplay: null,
            matchingAmountMonthlyDisplay: null,
            projectedSavingsDisplay: null,
            estimatedMonthlyContributionDisplay: null,
            hasDefaults: false,
            projectedMonthlyEmployee: null,
            projectedMonthlyMatching: null,
            projectedMonthlyTotal: null,
            projectedMonthlyTotalDisplay: null,
            monthlyContributionTotal: null,
            monthlyContributionTotalDisplay: null
        },
        results: {
            InitialAnnualIncome: null,
            MonthlyContribution: null,
            ProjectedOutsideAssets: null,
            RetirementAge: null,
            DeferralArray: null,
            InitialContributionDlr: null,
            InitialContributionPct: null,
            InitialContributionLabel: null,
            ContributionDlr: null,
            ContributionPct: null,
            annualIncomeDisplay: null,
            FullContributionCalc: {
                ProjectedBalanceAtRetirement: null,
                MonthlyIncome: null
            }
        },
        module: {},
        plan: {},
        RP: {
            kinetikAccepted: null,
            isStadion: false,
            isKinetik: false,
            stadionAccepted: null,
            retireExpressAccepted: null,
            profileAnswers: [],
            pointArray: [],
            profileResults: {
                RP_label: null
            },
            stadion: {
                graph: [],
            },
        },

        investments: {
            sections: {},
            choice: null,
            funds: null,
            allocations: {},
            allocationsParsed: {},
            currentPath: null,
            allPortfolios: null,
            selectedPortfolio: null,
            portfolio: null,
            realignment: null,
        },
        messaging: {},
        retirementNeedsLinks,
        footerLinks,
        links: [...footerLinks],
        optRadios: '',
        optOutRadios: null,
        maxContributions: false,
        optOutOfAutos: false,
        outOfBoundsDeferral: false,
        maxEmployerMatchPercent: false,
        maxEmployerMatchDollar: false,
        maxEmployerMatchActive: false,
        underCatchup: false,
        underCatchupActive: false,
        catchupConfirmed: false,
        inCatchup: false,
        catchupValue: 0,
        catchupMode: 0,
        overMaxAsi: false,
        validation: false,
        calculateTriggered: false,
        insufficientAllocations: true,
        optOutOfAutosConfirm: false,
        path: {
            currentSection: null,
            currentStep: {
                stepContents: null
            },
            sections: {},
            sectionsArray: []
        },
        spe2profileId: null,
        showNavigation: false,
        disableNavigation: false,
        disableLinks: false,
        currentSectionIndex: null,
        colors: {
            hex1: null,
            hex2: null,
            hex3: null,
            hex4: null
        },
        RNStep: 0,
        RPStep: 0,
        ConStep: 0,
        PBStep: 0,
        InvStep: 0,
        BenStep: 0,
        MPStep: 0,
        express: {
            active: false,
            selectedInvestment: null
        },
        OriginalRN_EstimatedRetirementIncome: 0,
        OriginalRN_InflationAdjustedReplacementIncome: 0,
        OriginalRN_ReplacementIncome: 0,
        OriginalRN_EstimatedSocialSecurityIncome: 0,
        OriginalRN_EstimatedPensionIncome: 0,
        OriginalRN_CurrentYearlyIncome: 0,
        OriginalRN_NumberOfYearsBeforeRetirement: 0,
        fork: {
            title: null,
            box_1_title: null,
            box_1_description: null,
            box_2_title: null,
            box_2_description: null,
            button_1_text: null,
            button_2_text: null,
            button_1_color: null,
            button_2_color: null,
            published: false
        },
        audioPlayer: {
            mode: 'started',
            playing: false,
            volume: 100,
            previousVolume: 35
        },
        initialDiscAcknowledged: false,
    },
    getters: {
        getProfileAnswers: state => () => state.RP.profileAnswers,
        investmentSections: state => {
            let sections = {}
            for (let key in state.investments.sections) {
                if (key === "investmentSelectorRiskBasedPortfolio" && !("risk" in state.path.sections))
                {
                    continue
                }

                if (state.investments.sections.hasOwnProperty(key))
                {
                    sections[key] = state.investments.sections[key];
                }
            }
            return sections;
        },
        getPathSections: state => {
            let sections = {};
            let index = 1;
            for (let key in state.path.sections) {
                if (state.path.sections.hasOwnProperty(key) && state.path.sections[key].active)
                {
                    sections[key] = state.path.sections[key];
                    sections[key].index = index++;
                    sections[key].enabled = true;
                }
            }
            return sections;
        },
        getActivePathSections: (state, getters) => {
            return state.path.sectionsArray.filter(section => section.active);
        },
        getEnabledPathSections: (state, getters) => {
            return state.path.sectionsArray.filter(section => section.enabled && section.active);
        }

    },
    mutations: {
        SET_AUDIO_MODE(state, mode) {
            state.audioPlayer.mode = mode;
        },
        SET_AUDIO_PLAYING(state, playing) {
            state.audioPlayer.playing = playing;
        },
        SET_AUDIO_VOLUME(state, volume) {
            state.audioPlayer.volume = volume;
        },
        SET_AUDIO_PREVIOUS_VOLUME(state, previousVolume) {
            state.audioPlayer.previousVolume = previousVolume;
        },
        SET_STEP(state, step) {
            state.path.currentStep = step;
            // state.path.currentStep = Object.assign(state.path.currentStep, step);
        },
        SET_SECTION(state, active) {
            for (let key in active)
            {
                if (key in state.path.sections && state.path.sections.hasOwnProperty(key))
                {
                    state.path.sections[key].active = active[key];
                }
            }
        },
        SET_SECTION_ENABLED(state, sections) {
            for (let key in sections)
            {
                if (sections.hasOwnProperty(key)) {
                    let index = state.path.sectionsArray.findIndex(section => section.name === key);
                    state.path.sectionsArray.splice(index, 1, {
                        ...state.path.sectionsArray[index], enabled: sections[key]
                    });
                }

            }
        },
        GET_RETIREMENT_NEEDS(state, data) {
            state.retirementneeds = Object.assign(state.retirementneeds, data.data);
            state.common = Object.assign(state.common, data.common);
            state.serverConfig = Object.assign(state.serverConfig, data.serverConfig);
            if(state.retirementneeds.RN_CurrentYearlyIncome != state.OriginalRN_CurrentYearlyIncome){
                state.OriginalRN_CurrentYearlyIncome = state.retirementneeds.RN_CurrentYearlyIncome,
                    state.OriginalRN_EstimatedRetirementIncome = state.retirementneeds.RN_EstimatedRetirementIncome,
                    state.OriginalRN_ReplacementIncome = state.retirementneeds.RN_ReplacementIncome,
                    state.OriginalRN_EstimatedSocialSecurityIncome = state.retirementneeds.RN_EstimatedSocialSecurityIncome,
                    state.OriginalRN_EstimatedPensionIncome = state.retirementneeds.RN_EstimatedPensionIncome,
                    state.OriginalRN_NumberOfYearsBeforeRetirement = state.retirementneeds.RN_NumberOfYearsBeforeRetirement
            }
            if(state.retirementneeds.RN_NumberOfYearsBeforeRetirement != state.OriginalRN_NumberOfYearsBeforeRetirement ){
                state.OriginalRN_InflationAdjustedReplacementIncome = state.retirementneeds.RN_InflationAdjustedReplacementIncome
            }
        },
        GET_BENEFICIARIES(state, data) {
            state.beneficiaries = data;
        },
        SAVE_BENEFICIARIES(state, data) {
            state.beneficiaries = Object.assign(state.beneficiaries, data.data);
        },
        DELETE_BENEFICIARIES(state, data) {
            state.beneficiaries = Object.assign(state.beneficiaries, data.data);
        },
        UPDATE_RETIREMENT_INCOME(state, estimatedRetirementIncome) {
            state.retirementneeds.RN_EstimatedRetirementIncome = estimatedRetirementIncome;
        },
        UPDATE_INFLATION_ADJUSTED_REPLACEMENT_INCOME(state, inflationAdjustedReplacementIncome) {
            state.retirementneeds.RN_InflationAdjustedReplacementIncome = inflationAdjustedReplacementIncome;
            state.retirementneeds.RN_AnnualIncomeNeededInRetirement = inflationAdjustedReplacementIncome;
        },
        SET_DATA(state, data) {
            state.common = Object.assign(state.common, data.common);
            state.retirementneeds = Object.assign(state.retirementneeds, data.retirement_needs);
            state.contributions = data.contributions;
            state.messaging = data.messaging;
            state.module = data.module;
            state.library = data.library;
            state.spe2profileId = data.spe2profileId;
            state.colors = data.colors;
            state.plan = data.plan;
            state.fork = Object.assign(state.fork, data.fork);
        },
        UPDATE_TOTAL_ASSETS(state, totalAssets) {
            state.retirementneeds.RN_TotalAssets = totalAssets;
        },
        UPDATE_ASSET_TYPES(state, assetTypes) {
            state.retirementneeds.RN_AssetTypes = assetTypes;
        },
        GET_RN_RESULTS(state, data) {
            state.results = Object.assign(state.results, data.results);
        },
        GET_RISK_PROFILE(state, data) {
            state.RP.data = Object.assign(state.RP, data.data);
            state.RP.isStadion = data.isStadion;
            state.RP.isKinetik = data.isKinetik;
            state.RP.managedaccountid = data.managedaccountid;
        },
        SET_RISK_SCORE(state, data) {
            state.RP.profileResults = data;
        },
        SET_CONTRIBUTIONS(state, data) {
            Object.keys(state.contributionsData).forEach(function(key, value) {
                state.contributionsData[key] = data[key];
            })
            state.contributions = Object.assign(state.contributions, data.contributions);
        },
        SET_PATH(state, data) {
            let i = 1;
            Object.keys(data.sections).forEach(function(val) {
                data.sections[val].complete = false;
                data.sections[val].index = i;
                i++;
            })
            state.path = Object.assign(state.path, data);

            let sections = [];
            let index = 1;
            for (let key in data.sections) {
                if (data.sections.hasOwnProperty(key) && data.sections[key].active)
                {
                    data.sections[key].index = index++;
                    data.sections[key].enabled = true;
                    sections.push(data.sections[key]);
                }
            }
            Vue.set(state.path, 'sectionsArray', sections);
        },
        SET_SECTION_COMPLETED(state) {
            if (state.path.currentSection) {
                state.path.sections[state.path.currentSection].complete = true;
            }
        },
        UPDATE_PATH(state, data) {

        },
        SET_OPT_RADIOS(state, optRadios) {
            state.optRadios = optRadios
        },
        SET_OPT_OUT_RADIOS(state, optOutRadios) {
            state.optOutRadios = optOutRadios
        },
        SET_RN_STEP(state, RNStep) {
            state.RNStep = RNStep
        },
        SET_RP_STEP(state, RPStep) {
            state.RPStep = RPStep
        },
        SET_CON_STEP(state, ConStep) {
            state.ConStep = ConStep
        },
        SET_PB_STEP(state, PBStep) {
            state.PBStep = PBStep
        },
        SET_INV_STEP(state, InvStep) {
            state.InvStep = InvStep
        },
        SET_BEN_STEP(state, BenStep) {
            state.BenStep = BenStep
        },
        SET_MP_STEP(state, MPStep) {
            state.MPStep = MPStep
        },
        SET_INITIAL_DISC_ACKNOWLEDGED(state, initialDiscAcknowledged) {
            state.initialDiscAcknowledged = initialDiscAcknowledged
        },
        SET_UNDER_CATCHUP (state, underCatchup) {
            state.underCatchup = underCatchup
        },
        SET_UNDER_CATCHUP_ACTIVE (state, underCatchupActive) {
            state.underCatchupActive = underCatchupActive
        },
        SET_CATCHUP_CONFIRMED (state, catchupConfirmed) {
            state.catchupConfirmed = catchupConfirmed;
        },
        SET_IN_CATCHUP(state, inCatchup) {
            state.inCatchup = inCatchup;
        },
        SET_CATCHUP_VALUE(state, catchupValue) {
            state.catchupValue = catchupValue;
        },
        SET_CATCHUP_MODE(state, catchupMode) {
            state.catchupMode = catchupMode;
        },
        SET_MAX_CONTRIBUTIONS (state, maxContributions) {
            state.maxContributions = maxContributions
        },
        SET_MAX_EMPLOYER_MATCH_PERCENT (state, maxEmployerMatchPercent) {
            state.maxEmployerMatchPercent = maxEmployerMatchPercent
        },
        SET_MAX_EMPLOYER_MATCH_DOLLAR (state, maxEmployerMatchDollar) {
            state.maxEmployerMatchDollars = maxEmployerMatchDollar
        },
        SET_MAX_EMPLOYER_MATCH_ACTIVE (state, maxEmployerMatchActive) {
            state.maxEmployerMatchActive = maxEmployerMatchActive
        },
        SET_OPT_OUT_OF_AUTOS (state, optOutOfAutos) {
            state.optOutOfAutos = optOutOfAutos
        },
        SET_OUT_OF_BOUNDS_DEFERRAL (state, outOfBoundsDeferral) {
            state.outOfBoundsDeferral = outOfBoundsDeferral
        },
        SET_OVER_MAX_ASI (state, overMaxAsi) {
            state.overMaxAsi = overMaxAsi
        },
        SET_CALCULATE_TRIGGERED (state, calculateTriggered) {
            state.calculateTriggered = calculateTriggered
        },
        SET_VALIDATION (state, validation) {
            state.validation = validation
        },
        GET_INVESTMENT_SECTIONS(state, data) {
            state.investments.sections = data;
        },
        GET_INVESTMENT_FUNDS(state, data) {
            state.investments.funds = data;
        },
        GET_INSUFFICIENT_ALLOCATIONS(state, data) {
            state.insufficientAllocations = data;
        },
        GET_ALL_INVESTMENT_PORTFOLIOS(state, data) {
            state.investments.allPortfolios = data;
        },
        SAVE_INVESTMENT_PORTFOLIO(state, data) {
            state.investments.portfolio = data;
        },
        SET_INVESTMENTS(state, data) {

        },
        SET_DISABLE_NAVIGATION(state, status) {
            state.disableNavigation = status
        },
        SET_DISABLE_LINKS(state, status) {
            state.disableLinks = status
        },
        SET_INVESTMENTS_CHOICE(state, choice) {
            state.investments.currentPath = choice;
        },
        SET_STADION_GRAPHS(state, data) {
            state.RP.stadion.graph = data.graph;
        },
        SET_STADION_DISCLAIMER(state, data) {
            state.RP.stadion.disclaimer = data;
        }

    },
    actions: {
        getData({state, commit}) {
            return data.getData()
                .then(function (response) {
                    commit("SET_DATA", response.data);
                })
                .catch((error) => {
                    console.log(error)
                });
        },
        saveRetirementNeeds({state, commit}) {
            return data.saveRetirementNeeds({
                ...state.retirementneeds,
                ...state.results,
                common_planBalance:state.common.planBalance,
                common_dateOfBirth:state.common.dateOfBirth
            })
                .then(function (response) {
                    commit("GET_RETIREMENT_NEEDS", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        getRetirementNeeds({commit}) {
            return data.getRetirementNeeds()
                .then(function (response) {
                    commit("GET_RETIREMENT_NEEDS", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        saveBeneficiaries({state, commit}, form) {
            return data.saveBeneficiaries(form)
                .then(function (response) {
                    commit("SAVE_BENEFICIARIES", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        getBeneficiaries({commit}) {
            return data.getUiData()
                .then(function (response) {
                    commit("GET_BENEFICIARIES", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        deleteBeneficiaries({state, commit}, form) {
            return data.deleteBeneficiaries(form)
                .then(function (response) {
                    commit("DELETE_BENEFICIARIES", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        getResultsData({ commit })  {
            return data.getResultsData()
                .then(function(response) {
                    commit("GET_RN_RESULTS", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        getRiskProfile({commit}, params) {
            return data.getRiskProfile(params)
                .then(function (response) {
                    commit("GET_RISK_PROFILE", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        getContributions({state, commit}, params) {
            return data.getContributions(params)
                .then(function(response) {
                    commit("SET_CONTRIBUTIONS", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        calculateRiskProfileScore({state, commit}) {
            return data.getRiskScore({answers: state.RP.pointArray, stadionAccepted: state.RP.stadionAccepted })
                .then(function (response) {
                    commit("SET_RISK_SCORE", response.data);
                })
                .catch((error) => {
                    console.log(error)
                });
        },
        getStadionGraphs({state, commit}) {
            return data.getStadionGraphs()
                .then(function (response) {
                    commit("SET_STADION_GRAPHS", response.data);
                })
                .catch((error) => {
                    console.log(error)
                });
        },
        stadionRetireExpress({state, commit}) {
            return data.stadionRetireExpress()
                .then(function (response) {
                })
                .catch((error) => {
                    console.log(error)
                });
        },
        getStadionDisclaimerText({state, commit}) {
            return data.getStadionDisclaimerText()
                .then(function (response) {
                    commit("SET_STADION_DISCLAIMER", response.data);
                })
                .catch((error) => {
                    console.log(error)
                });
        },
        saveContributions({ commit, state }, params) {
            let saveData = new Object();
            saveData['request'] = JSON.stringify(params);
            saveData['autosOptOut'] = state.optOutOfAutosConfirm || state.optRadios !== 'radio-1';
            return data.saveContributions(saveData);
        },
        saveAutoIncrease({state}) {
            let autoIncreaseData = state.common.autos.autoIncreaseData;
            return data.saveAutoIncrease({
                'autoIncreaseFrequency': autoIncreaseData.frequency,
                'autoIncreaseCap': autoIncreaseData.increaseCap,
                'autoIncreasePercentLimit': autoIncreaseData.percentLimit,
                'autoIncreasePre': autoIncreaseData.pre,
                'autoIncreaseRoth': autoIncreaseData.roth,
                'autoIncreasePost': autoIncreaseData.post,
                'autoIncreaseStartDate': autoIncreaseData.startDate,
                'autoIncreaseOptIn': autoIncreaseData.autoIncreaseOptIn
            });
        },
        getInvestmentSections({commit}) {
            return data.getInvestmentSections()
                .then(function (response) {
                    commit("GET_INVESTMENT_SECTIONS", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        getInvestmentFunds({commit}) {
            return data.getInvestmentFunds()
                .then(function (response) {
                    commit("GET_INVESTMENT_FUNDS", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        getAllInvestmentPortfolios({commit}) {
            return data.getAllInvestmentPortfolios()
                .then(function (response) {
                    commit("GET_ALL_INVESTMENT_PORTFOLIOS", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        getInvestmentPortfolioByName({state, commit}) {
            return data.getInvestmentPortfolioByName(state.investments.selectedPortfolio)
                .then(function (response) {
                    commit("SAVE_INVESTMENT_PORTFOLIO", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        setInvestments({state, commit}) {
            var request = {funds: state.investments.allocationsParsed};
            if (state.investments.currentPath === "/investments/risk")
                request['portfolioid'] = state.investments.portfolio.id;
            return data.setInvestments(state.investments.currentPath, request)
                .then(function (response) {
                    commit("SET_INVESTMENTS", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        finishInvestments({state, commit}) {
            return data.finishInvestments({realignment: state.investments.realignment})
                .then(function (response) {
                    commit("SET_INVESTMENTS", response.data);
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
        setPath({state, commit}, pathType) {
            if (state.common.locale === "en")
                state.common.localeId = 1;
            else
                state.common.localeId = 2;
            return data.getPath(state.module.planid,pathType,state.common.locale).then(function(response) {
                return data.setPath(response.data.id).then(() => {
                    commit("SET_PATH", response.data);
                })
            });
        },
        changeLocale({state, commit},locale)
        {
            return data.changeLocale(locale).then(function(response) {
            });
        },
        saveRollover({commit},{type, value}) {
            return data.saveRollover(type, value)
                .then(function (response) {
                })
                .catch((error) => {
                    console.log(error.statusText)
                });
        },
    }
})
