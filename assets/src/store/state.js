import navLinks from '../data/navLinks'
import footerLinks from '../data/footerLinks'

export default {
  navLinks,
  footerLinks,
  links: [...navLinks, ...footerLinks]
}
