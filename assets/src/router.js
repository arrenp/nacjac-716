import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'
import RetirementNeeds from './views/RetirementNeeds.vue'
import MyProfile from './views/MyProfile.vue'
import RiskProfile from './views/RiskProfile.vue'
import Investments from './views/Investments.vue'
import Contributions from './views/Contributions.vue'
import PlanBasics from './views/PlanBasics.vue'
import Beneficiaries from './views/Beneficiaries.vue'
import PathChoice from './views/PathChoice.vue'
import Welcome from './components/Global/Welcome.vue'
import Library from './views/Library.vue'
import RolloverView from './views/Rollover.vue'
import Goodbye from './views/Goodbye.vue'

//Retirement Needs Components
import SocialSecurityPension from './components/RetirementNeeds/SocialSecurityPension.vue'
import VideoWarning from './components/RetirementNeeds/VideoWarning.vue'
import WelcomeVideo from './components/RetirementNeeds/WelcomeVideo.vue'
import PersonalInfo1 from './components/RetirementNeeds/PersonalInfo1.vue'
import PersonalInfo2 from './components/RetirementNeeds/PersonalInfo2.vue'
import PersonalInfo3 from './components/RetirementNeeds/PersonalInfo3.vue'
import ReplacementIncome from './components/RetirementNeeds/ReplacementIncome.vue'
import Results from './components/RetirementNeeds/Results.vue'
import IncomeInfo from './components/RetirementNeeds/IncomeInfo.vue'
import YearsBeforeRetirement from './components/RetirementNeeds/YearsBeforeRetirement.vue'
import RepIncome from "./components/RetirementNeeds/RepIncome.vue";
import Assets from './components/RetirementNeeds/Assets'
import ReplacementIncome2 from "./components/RetirementNeeds/ReplacementIncome2";
import RNEndingVideo from './components/RetirementNeeds/RNEndingVideo.vue'
import RetirementAge from './components/RetirementNeeds/RetirementAge.vue'


//Risk Profile Modules
import RPCongratulations from './components/RiskProfile/RPCongratulations.vue'
import RPWelcomeVideo from './components/RiskProfile/RPWelcomeVideo.vue'
import RPWelcome from './components/RiskProfile/RPWelcome.vue'
import YourRiskProfile from './components/RiskProfile/YourRiskProfile.vue'
import RPQuestionComponent from './components/RiskProfile/RPQuestionComponent.vue'
import RTQKinetik from './components/RiskProfile/RTQKinetik.vue'
import RTQStadionRetireExpress from './components/RiskProfile/RTQStadionRetireExpress.vue'
import RTQStadion1 from './components/RiskProfile/RTQStadion1.vue'
import RTQStadionAdvice from './components/RiskProfile/RTQStadionAdvice.vue'
import RTQStadionResults from './components/RiskProfile/RTQStadionResults.vue'
import RTQStadionTerms from './components/RiskProfile/RTQStadionTerms.vue'
import RPEndingVideo from './components/RiskProfile/RPEndingVideo.vue'

//Investments Modules
import InvestmentEndVideo from './components/Investments/InvestmentEndVideo.vue'
import InvestmentType from './components/Investments/InvestmentType.vue'
import InvestmentWelcome from './components/Investments/InvestmentWelcome.vue'
import InvestmentFunds from './components/Investments/InvestmentFunds.vue'
import InvestmentQDIA from './components/Investments/InvestmentQDIA.vue'
import InvestmentDefault from './components/Investments/InvestmentDefault.vue'
import RiskBasedPortfolios from './components/Investments/RiskBasedPortfolios.vue'
import RiskBasedInvestments from './components/Investments/RiskBasedInvestments.vue'
import RiskBasedFunds from './components/Investments/RiskBasedFunds.vue'
import TargetDateFund from './components/Investments/TargetDateFund.vue'
import CustomChoice from './components/Investments/CustomChoice.vue'
import CustomChoiceReview from './components/Investments/CustomChoiceReview.vue'
import Realignment from './components/Investments/Realignment.vue'

//Contributions Modules
import AutosACA from './components/Contributions/AutosACA.vue'
import AutosOptOutEnrollment from './components/Contributions/AutosOptOutEnrollment.vue'
import DEF from './components/Contributions/DEF.vue'
import DEFASI from './components/Contributions/DEFASI.vue'
import ContributionsWelcomeVideo from './components/Contributions/ContributionsWelcomeVideo.vue'
import ContributionsEndingVideo from './components/Contributions/ContributionsEndingVideo.vue'
import ContributionIntro from './components/Contributions/ContributionsIntro.vue'


// My profile
import Confirmation from './components/MyProfile/Confirmation.vue'
import OptOut from './components/MyProfile/OptOut.vue'
import SavingTransactions from './components/MyProfile/SavingTransactions.vue'
import EmailMyProfile from "./components/MyProfile/EmailMyProfile";
import MyProfileEndingVideos from "./components/MyProfile/MyProfileEndingVideos";

//Goodbye
import ThankYou from "./components/Goodbye/ThankYou";
import GoodbyeEndingVideo from "./components/Goodbye/GoodbyeEndingVideo";
import GoodbyeStartingVideo from "./components/Goodbye/GoodbyeStartingVideo";

//Rollover
import Rollover from "./components/Rollover/Rollover";
import RolloverWelcomeVideo from "./components/Rollover/RolloverWelcomeVideo";

//Beneficiaries Modules
import AddEditBeneficiary from './components/Beneficiaries/AddEditBeneficiary.vue'
import Express from "./components/Contributions/Express";
import AutosOptOutCombined from "./components/Contributions/AutosOptOutCombined";


Vue.use(VueRouter)

const Router = new VueRouter({
    mode: "history",
    base: "/SP",
    routes: [
        {
            path: '/',
            name: 'pathChoice',
            component: PathChoice
        },
        { path: '/pathChoice', name:"pathChoice",component: PathChoice  },
        { path: '/retirementNeeds', redirect: {path: '/RN'} },
        { path: '/risk', redirect: {path: '/riskprofile'} },
        { path: '/retirementNeeds', redirect: {path: '/RN'} },
        {
            path: '/planBasics',
            name: 'planBasics',
            component: PlanBasics
        },
        {
            path: '/RN',
            name: 'retirementNeeds',
            component: RetirementNeeds,
            children: [
                {
                    path: '',
                },
                {
                    path: 'PersonalInfo1',
                    name: 'PersonalInfo1',
                    component: PersonalInfo1,
                },
                {
                    path: 'PersonalInfo2',
                    name: 'PersonalInfo2',
                    component: PersonalInfo2,
                },
                {
                    path: 'PersonalInfo3',
                    name: 'PersonalInfo3',
                    component: PersonalInfo3,
                },
                {
                    path: 'WelcomeVideo',
                    name: 'WelcomeVideo',
                    component: WelcomeVideo,
                },
                {

                    path: 'Income',
                    name: 'Income',
                    component: IncomeInfo,
                },
                {
                    path: 'SSI_Pension',
                    name: 'SSI_Pension',
                    component: SocialSecurityPension,
                },
                {
                    path: 'VideoWarning',
                    name: 'VideoWarning',
                    component: VideoWarning,
                },
                {
                    path: 'Ret_Income',
                    name: 'Ret_Income',
                    component: ReplacementIncome,
                },
                {
                    path: 'Assets',
                    name: 'Assets',
                    component: Assets
                },
                {
                    path: 'YTR',
                    name: 'YTR',
                    component: YearsBeforeRetirement,
                },
                {
                    path: 'Inflation_Adj',
                    name: 'Inflation_Adj',
                    component: RepIncome
                },
                {
                    path: 'RN_Summ',
                    name: 'RN_Summ',
                    component: Results,
                },
                {
                    path: 'Rep_Income',
                    name: 'Rep_Income',
                    component: ReplacementIncome2,
                },
                {
                    path: 'RNEndingVideo',
                    name: 'RNEndingVideo',
                    component: RNEndingVideo,
                },
                {
                    path: 'RetirementAge',
                    name: 'RetirementAge',
                    component: RetirementAge,
                }
            ],
        },
        {
            path: '/riskprofile',
            name: 'risk',
            component: RiskProfile,
            children: [
                {
                    path: ''
                },
                {
                    path: 'Welcome',
                    name: 'Welcome',
                    component: RPWelcome,
                },
                {
                    path: 'RTQStadionRetireExpress',
                    name: 'RTQStadionRetireExpress',
                    component: RTQStadionRetireExpress,
                },
                {
                    path: 'RTQStadion1',
                    name: 'RTQStadion1',
                    component: RTQStadion1,
                },
                {
                    path: 'RTQStadionAdvice',
                    name: 'RTQStadionAdvice',
                    component: RTQStadionAdvice,
                },
                {
                    path: 'RTQStadionTerms',
                    name: 'RTQStadionTerms',
                    component: RTQStadionTerms,
                },
                {
                    path: 'RTQStadionResults',
                    name: 'RTQStadionResults',
                    component: RTQStadionResults,
                },
                {
                    path: 'RP/:id',
                    name: 'RP',
                    component: RPQuestionComponent,
                },
                {
                    path: 'RPCongratulations',
                    name: 'RPCongratulations',
                    component: RPCongratulations,
                },
                {
                    path: 'RPWelcomeVideo',
                    name: 'RPWelcomeVideo',
                    component: RPWelcomeVideo,
                },
                {
                    path: 'RPEndingVideo',
                    name: 'RPEndingVideo',
                    component: RPEndingVideo,
                },
                {
                    path: 'RTQKinetik',
                    name: 'RTQKinetik',
                    component: RTQKinetik,
                },
                {
                    path: 'Finish',
                    name: 'Finish',
                    component: YourRiskProfile,
                }
            ],
        },
        {
            path: '/investments',
            name: 'investments',
            component: Investments,
            children: [
                {
                    path: '',
                },
                {
                    path: 'Invest_Welcome',
                    name: 'Invest_Welcome',
                    component: InvestmentWelcome,
                },
                {
                    path: 'Invest_End_Video',
                    name: 'Invest_End_Video',
                    component: InvestmentEndVideo,
                },
                {
                    path: 'investmentSelector',
                    name: 'investmentSelector',
                    component: InvestmentType,
                },
                {
                    path: 'InvestmentQDIA',
                    name: 'InvestmentQDIA',
                    component: InvestmentQDIA,
                },
                {
                    path: 'InvestmentDefault',
                    name: 'InvestmentDefault',
                    component: InvestmentDefault,
                },
                {
                    path: 'InvestmentFunds',
                    name: 'InvestmentFunds',
                    component: InvestmentFunds,
                },
                {
                    path: 'RiskBasedPortfolios',
                    name: 'RiskBasedPortfolios',
                    component: RiskBasedPortfolios,
                },
                {
                    path: 'RiskBasedInvestments',
                    name: 'RiskBasedInvestments',
                    component: RiskBasedInvestments,
                },
                {
                    path: 'RiskBasedFunds',
                    name: 'RiskBasedFunds',
                    component: RiskBasedFunds,
                },
                {
                    path: 'TDF',
                    name: 'TDF',
                    component: TargetDateFund,
                },
                {
                    path: 'CC',
                    name: 'CC',
                    component: CustomChoice,
                },
                {
                    path: 'CChoice_Review',
                    name: 'CChoice_Review',
                    component: CustomChoiceReview,
                },
                {
                    path: 'Realignment',
                    name: 'Realignment',
                    component: Realignment,
                },
            ],
        },
        {
            path: '/myprofile',
            name: 'myprofile',
            component: MyProfile,
            children: [
                {
                    path: '',
                },
                {
                    path: 'My_Profile',
                    name: 'My_Profile',
                    component: Confirmation,
                },
                {
                    path: 'optout',
                    name: 'optout',
                    component: OptOut,
                },
                {
                    path: 'savingtransactions',
                    name: 'SavingTransactions',
                    component: SavingTransactions,
                },
                {
                    path: 'Email_My_Profile',
                    name: 'Email_My_Profile',
                    component: EmailMyProfile
                },
                {
                    path: 'MyProfileEndingVideos',
                    name: 'MyProfileEndingVideos',
                    component: MyProfileEndingVideos
                },
            ],
        },
        {
            path: '/contributions',
            name: 'contributions',
            component: Contributions,
            children: [
                {
                    path: '',
                    // redirect: function() {
                    //     if (store.state.common.autos.autoEnroll && (store.state.common.autos.autoEscalate || store.state.common.autos.defaultInvestment || store.state.common.autos.autoIncrease)) {
                    //         return { path: 'AutosACA' }
                    //     } else {
                    //         return { path: 'DEF' }
                    //     }
                    // }
                },
                {
                    path: 'ContributionIntro',
                    name: 'ContributionIntro',
                    component: ContributionIntro,
                },
                {
                    path: 'Express',
                    name: 'Express',
                    component: Express,
                },
                {
                    path: 'Contr',
                    name: 'Contr',
                    component: DEF,
                },
                {
                    path: 'DEFASI',
                    name: 'DEFASI',
                    component: DEFASI,
                },
                {
                    path: 'AutosACA',
                    name: 'AutosACA',
                    component: AutosOptOutCombined,
                },
                {
                    path: 'AutosOptOutEnrollment',
                    name: 'AutosOptOutEnrollment',
                    component: AutosOptOutEnrollment,
                },
                {
                    path: 'ContributionsWelcomeVideo',
                    name: 'ContributionsWelcomeVideo',
                    component: ContributionsWelcomeVideo,
                },
                {
                    path: 'ContributionsEndingVideo',
                    name: 'ContributionsEndingVideo',
                    component: ContributionsEndingVideo,
                },
            ],
        },
        {
            path: '/beneficiary_page',
            name: 'beneficiary_page',
            component: Beneficiaries,
            children: [
                {
                    path: ''
                },
                {
                    path: 'BEN_Info',
                    name: 'BEN_Info',
                    component: AddEditBeneficiary
                },
            ],
        },
        {
            path: '/rollover',
            name: 'rollover',
            component: RolloverView,
            children: [
                {
                    path: ''
                },
                {
                    path: 'RolloverWelcomeVideo',
                    name: 'RolloverWelcomeVideo',
                    component: RolloverWelcomeVideo
                },
                {
                    path: 'Rollover',
                    name: 'Rollover',
                    component: Rollover
                },
            ],
        },
        {
            path: '/goodbye',
            name: 'goodbye',
            component: Goodbye,
            children: [
                {
                    path: ''
                },
                {
                    path: 'GoodbyeStartingVideo',
                    name: 'GoodbyeStartingVideo',
                    component: GoodbyeStartingVideo
                },
                {
                    path: 'Goodbye',
                    name: 'Goodbye',
                    component: ThankYou
                },
                {
                    path: 'GoodbyeEndingVideo',
                    name: 'GoodbyeEndingVideo',
                    component: GoodbyeEndingVideo
                },
            ],
        },
        {
            path: '/library',
            name: 'library',
            component: Library,
        }
    ],
    scrollBehavior() {
        document.getElementById('app').scrollIntoView();
    }
})

function hasQueryParams(route) {
    return !!Object.keys(route.query).length
}

Router.beforeEach((to, from, next) => {
    if(!hasQueryParams(to) && hasQueryParams(from)){
        if (to.path === from.path) {
            return false;
        }
        next({path: to.path, query: from.query});
    } else {
        next();
    }
});

export default Router;
