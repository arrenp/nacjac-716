<?php

namespace Utilities\UploaderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use classes\classBundle\Entity\accountsUsersPlans;
use Utilities\UploaderBundle\Controller\cdnUploader;
require_once('cloudfiles.php');

class LogosController extends Controller
{

    public function uploadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $planid));
        $filenames = array();
        $filenames[0] = "providerLogoImage";
        $filenames[1] = "providerLogoEmailImage";
        $filenames[2] = "sponsorLogoImage";
        $filenames[3] = "bannerImage";
        $filenames[4] = "sponsorLogoEmailImage";
        $filenameCount = count($filenames);
        $responseString = "";
        //textbox processing
        $requestVariables = $request->request->all();
        $planArray = [];
        foreach ($requestVariables as $key => $value)
        {
            $field = str_replace("SRC", "", $key);

            if ($field == "participantLoginWebsiteAddress" && $request->request->get("adviserplan","" == "true"))
            {
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersPlans');
                $adviserplan = $repository->findOneBy(array('id' => $request->request->get("id")));
                $adviserplan->$field = $value;
            }
            else if (!in_array($key,$filenames))
            {
                $planArray[$field] = $value;
            }  
            
        }

        if (count($_FILES) > 0)
        {
            $cdnuploader = new cdnUploader();
            $container = $cdnuploader->containerByHostName($request->server->get('HTTP_HOST'));
            $filenameuploaded = "";
            for ($i = 0; $i < $filenameCount; $i++)
            {
                if (isset($_FILES[$filenames[$i]]))
                {
                    $uploadedFile = $_FILES[$filenames[$i]];      
                    $imageProperties = @getimagesize($uploadedFile["tmp_name"]);
                    if ($imageProperties)
                    {
                        $filename = $uploadedFile['name'];                    
                        $filenameuploaded = $filenames[$i];
                        $this->resizeLogo($uploadedFile["tmp_name"],$request->request->get($filenames[$i]."Width"),$request->request->get($filenames[$i]."Height"));                        
                        $filename = "0client" . $userid . "_plan" . $planid . "_" .time(). $filename;                  
                        $theurl = $cdnuploader->uploadFile($container,$uploadedFile['tmp_name'],$filename);
                        $planArray[$filenameuploaded] = $theurl;
                        $responseString = $responseString . " *" . $filenameuploaded . '*' . $theurl . "*" . $filenameuploaded . '* ';    
                    }
                }
            }
        }
        $this->get("PlanService")->update($currentplan, $planArray);
        $em->flush();

        return new response("<!-- " . $responseString . ' --><script>window.location="' . $request->request->get("returnpage") . '?success=true"</script>');
    }

    function resizeLogo($image,$width,$height)
    {
        list($imagewidth, $imageheight, $imageType) = getimagesize($image);
        $imageType = image_type_to_mime_type($imageType);
        $newImageWidth = ceil($width);
        $newImageHeight = ceil($height);
        $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);

        switch($imageType)
        {
            case "image/gif":
                $source = imagecreatefromgif($image);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                $source = imagecreatefromjpeg($image);
                break;
            case "image/png":
            case "image/x-png":
                $source = imagecreatefrompng($image);
                break;
        }

        if ( ($imageType == "image/gif") || ($imageType == "image/png") )
        {
            $transindex = imagecolortransparent($source);

            if($transindex >= 0)
            {
                $transcol = imagecolorsforindex($source, $transindex);
                $transindex = imagecolorallocatealpha($newImage, $transcol['red'], $transcol['green'], $transcol['blue'], 127);
                imagefill($newImage, 0, 0, $transindex);
                imagecolortransparent($newImage, $transindex);
            }

            elseif ($imageType == "image/png" || $imageType == "image/x-png")
            {
                imagealphablending($newImage, false);
                $color = imagecolorallocatealpha($newImage, 0, 0, 0, 127);
                imagefill($newImage, 0, 0, $color);
                imagesavealpha($newImage, true);
            }
        }

        imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$imagewidth,$imageheight);

        switch($imageType)
        {
            case "image/gif":
                imagegif($newImage,$image);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                imagejpeg($newImage,$image,90);
                break;
            case "image/png":
            case "image/x-png":
                imagepng($newImage,$image);
                break;
        }

        chmod($image, 0777);
        return $image;
    }
}
