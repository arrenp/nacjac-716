<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shared\ContactBundle\Classes\contact;
use Sessions\AdminBundle\Classes\adminsession;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Shared\General\GeneralMethods;

class AssociateController extends Controller
{

    public $userid;
    public $planid;
    public $modules;

    public function indexAction(Request $request)
    {
        $this->init();
        $general = $this->get('GeneralMethods');
        $session = $this->get('adminsession');
        $userid = $this->userid;
        $planid = $this->planid;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $currentmember = $repository->findOneBy(array('id' => $userid));
        $minscore = $currentmember->investmentsMinScore;
        $maxscore = $currentmember->investmentsMaxScore;

        if ($minscore == 0 && $maxscore == 0)
        {
            $minscore = 6;
            $maxscore = 54;
        }


        $connectionType = $currentmember->connectionType;
        $associate = $general->plansInvestmentsAvailable($currentmember);




        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $planid));

        $riskType = "RBP";
        if (trim($currentplan->riskType) != "") $riskType = trim($currentplan->riskType);
        $riskcounter = 0;
        if ($request->query->get("type") != null)
        {
            $riskType = $request->query->get("type");
        }

        if ($currentmember->recordkeeperPortfoliosAvailable)
        {
            $riskTypes[$riskcounter] = new \stdClass();
            $riskTypes[$riskcounter]->title = "Risk Based Portfolios";
            $riskTypes[$riskcounter]->value = "RBP";
            $riskcounter++;
        }

        if ($currentmember->recordkeeperRiskBasedFundsAvailable)
        {
            $riskTypes[$riskcounter] = new \stdClass();
            $riskTypes[$riskcounter]->title = "Risk Based Funds";
            $riskTypes[$riskcounter]->value = "RBF";
            $riskcounter++;
        }


        if ($riskcounter < 1) $riskTypes = "";

        if ($riskTypes != "") for ($i = 0; $i < count($riskTypes); $i++) if ($riskTypes[$i]->value == $riskType) $riskTypes[$i]->checked = "checked";
                else $riskTypes[$i]->checked = "";

        //$riskTypes = "";

        if ($currentplan->investmentsAutoScoring == 0) $displayClassName = "displayScores";
        else $displayClassName = "hideScores";

        if ($riskType == "RBP") $investmentsTable = "plansPortfolios";
        if ($riskType == "RBF") $investmentsTable = "plansRiskBasedFunds";

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $investmentsTable);
        $nonassociatedInvestments = $repository->findBy(array('planid' => $planid, 'profile' => 'ALL'));
        $conservativeInvestments = $repository->findBy(array('planid' => $planid, 'profile' => '0'), array("minscore" => "ASC"));
        $moderatelyConservativeInvestments = $repository->findBy(array('planid' => $planid, 'profile' => '1'), array("minscore" => "ASC"));
        $moderateInvestments = $repository->findBy(array('planid' => $planid, 'profile' => '2'), array("minscore" => "ASC"));
        $moderatelyAggressiveInvestments = $repository->findBy(array('planid' => $planid, 'profile' => '3'), array("minscore" => "ASC"));
        $aggressiveInvestments = $repository->findBy(array('planid' => $planid, 'profile' => '4'), array("minscore" => "ASC"));
        $totalInvestments = count($nonassociatedInvestments) + count($conservativeInvestments) + count($moderatelyConservativeInvestments) + count($moderateInvestments) + count($moderatelyAggressiveInvestments) + count($aggressiveInvestments);


        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansAssociate");

        if ($associate)
        {
            $recordkeeperInvestments = new Response("");
            if ($riskType == "RBP" && $connectionType == "Relius") $recordkeeperInvestments = $this->forward('RecordkeepersReliusBundle:Default:portfoliolist', array("planid" => $currentplan->activePlanid));
            if ($riskType == "RBF" && $connectionType == "Relius") $recordkeeperInvestments = $this->forward('RecordkeepersReliusBundle:Default:riskbasedfundslist', array("planid" => $currentplan->activePlanid));
            if ($riskType == "RBP" && $general->isSrt($currentmember)) $recordkeeperInvestments = $this->forward('RecordkeepersSRTBundle:Default:portfoliolist', array("planid" => $currentplan->activePlanid));
            if ($riskType == "RBF" && $general->isSrt($currentmember)) $recordkeeperInvestments = $this->forward('RecordkeepersSRTBundle:Default:riskbasedfundslist', array("plantypecd" => $currentplan->activePlantypecd));
            if ($riskType == "RBP" && $connectionType == "ExpertPlan") $recordkeeperInvestments = $this->forward('RecordkeepersExpertPlanBundle:Default:portfoliolist', array("planid" => $currentplan->activePlanid));
            $recordkeeperInvestments = $recordkeeperInvestments->getContent();
            $recordkeeperInvestments = explode("*seperator*", $recordkeeperInvestments);
            $this->validatePortfolios($nonassociatedInvestments, $recordkeeperInvestments);
            $this->validatePortfolios($conservativeInvestments, $recordkeeperInvestments);
            $this->validatePortfolios($moderatelyConservativeInvestments, $recordkeeperInvestments);
            $this->validatePortfolios($moderateInvestments, $recordkeeperInvestments);
            $this->validatePortfolios($moderatelyAggressiveInvestments, $recordkeeperInvestments);
            $this->validatePortfolios($aggressiveInvestments, $recordkeeperInvestments);
        }
        $general = $this->get('GeneralMethods');

        $addRiskBasedFunds = 1;
        if (in_array($connectionType, $general->recordkeeperRiskBasedFundsAvailable) || $riskType != "RBF") $addRiskBasedFunds = 0;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansModules');
        $modules = $repository->findOneBy(array('planid' => $this->planid));
        $investmentsGraph = $modules->investmentsGraph;

        return $this->render('PlansPlansBundle:Associate:index.html.twig', array('userid' => $userid, 'planid' => $planid, 'nonassociatedInvestments' => $nonassociatedInvestments, 'conservativeInvestments' => $conservativeInvestments,
                    "moderatelyConservativeInvestments" => $moderatelyConservativeInvestments, "moderateInvestments" => $moderateInvestments,
                    "moderatelyAggressiveInvestments" => $moderatelyAggressiveInvestments, "aggressiveInvestments" => $aggressiveInvestments,
                    "displayClassName" => $displayClassName, "connectionType" => $connectionType, "associate" => $associate, "currentplan" => $currentplan, "riskType" => $riskType,
                    "riskTypes" => $riskTypes, "riskcounter" => $riskcounter, "minscore" => $minscore, "maxscore" => $maxscore, "writeable" => $writeable, "modules" => $this->modules, "totalInvestments" => $totalInvestments, "riskBasedInvestmentsOff" => $this->riskBasedInvestmentsOff(), "addRiskBasedFunds" => $addRiskBasedFunds,"session" => $session,"investmentsGraph" => $investmentsGraph));
    }

    public function validatePortfolios(&$investments, $recordkeeperInvestments)
    {
        foreach ($investments as $investment)
        {
            foreach ($recordkeeperInvestments as $recordkeeperInvestment)
            {
                $investment->valid = false;
                if (trim($investment->name) == trim($recordkeeperInvestment))
                {
                    $investment->valid = true;
                    break;
                }
            }
        }
    }

    public function riskBasedInvestmentsOff()
    {
        $riskBasedTypes = array();
        $startname = "riskProfile";
        $this->riskBasedInvestmentsOffAddInvestment($riskBasedTypes, $startname . "Conservative", "Conservative");
        $this->riskBasedInvestmentsOffAddInvestment($riskBasedTypes, $startname . "ModeratelyConservative", "Moderately Conservative");
        $this->riskBasedInvestmentsOffAddInvestment($riskBasedTypes, $startname . "Moderate", "Moderate");
        $this->riskBasedInvestmentsOffAddInvestment($riskBasedTypes, $startname . "ModeratelyAggressive", "Moderately Aggressive");
        $this->riskBasedInvestmentsOffAddInvestment($riskBasedTypes, $startname . "Aggressive", "Aggressive");
        return $riskBasedTypes;
    }

    public function riskBasedInvestmentsOffAddInvestment(&$riskBasedTypes, $name, $description)
    {
        $riskBasedTypes[$name] = new \stdClass();
        $riskBasedTypes[$name]->name = $name;
        $riskBasedTypes[$name]->description = $description;
        $riskBasedTypes[$name]->checked = $this->modules->$name;
    }

    public function saveInvestmentsAction()
    {
        $this->init();

        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $this->planid));
        $currentplan->investmentsAutoScoring = $request->request->get("autoscoring", "1");
        $em->flush();
        $riskType = $request->request->get("type");

        if ($riskType == "RBP") $investmentsTable = "plansPortfolios";
        if ($riskType == "RBF") $investmentsTable = "plansRiskBasedFunds";




        if ($request->request->get("inactiveInvestments", "") != "")
        {
            $inactiveInvestments = explode(",", $request->request->get("inactiveInvestments", ""));

            foreach ($inactiveInvestments as $investment)
            {
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $investmentsTable);
                $currentInvestment = $repository->findOneBy(array('id' => $investment));
                $currentInvestment->profile = "ALL";
                $currentInvestment->minscore = 0;
                $currentInvestment->maxscore = 0;
                $em->flush();
            }
        }

        if ($request->request->get("activeInvestments", "") != "")
        {
            $activeInvestments = explode("|", $request->request->get("activeInvestments", ""));

            foreach ($activeInvestments as $activeInvestment)
            {
                $investment = explode(",", $activeInvestment);
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $investmentsTable);
                $currentInvestment = $repository->findOneBy(array('id' => $investment[0]));
                $currentInvestment->profile = $investment[3];
                $currentInvestment->minscore = $investment[1];
                $currentInvestment->maxscore = $investment[2];
                $em->flush();
                ;
            }
        }
        if ($request->request->get("invalidInvestments", "") != "")
        {
            $invalidInvestments = explode(",", $request->request->get("invalidInvestments", ""));
            foreach ($invalidInvestments as $invalidInvestment)
            {
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $investmentsTable);
                $investment = $repository->findOneBy(array('id' => $invalidInvestment));
                $em->remove($investment);
            }
        }
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansModules');
        $modules = $repository->findOneBy(array('planid' => $this->planid));
        $modules->riskProfileConservative = $request->request->get("riskProfileConservative");
        $modules->riskProfileModeratelyConservative = $request->request->get("riskProfileModeratelyConservative");
        $modules->riskProfileModerate = $request->request->get("riskProfileModerate");
        $modules->riskProfileModeratelyAggressive = $request->request->get("riskProfileModeratelyAggressive");
        $modules->riskProfileAggressive = $request->request->get("riskProfileAggressive");
        $em->flush();
        return new Response("saved");
    }

    public function init()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Plans");
        $session->set("currentpage", "PlansAssociate");
        $this->userid = $session->userid;
        $this->planid = $session->planid;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansModules');
        $this->modules = $repository->findOneBy(array('planid' => $this->planid));

        $general = $this->get('GeneralMethods');
        $this->modules->riskProfileConservative = $general->integerToChecked($this->modules->riskProfileConservative);
        $this->modules->riskProfileModeratelyConservative = $general->integerToChecked($this->modules->riskProfileModeratelyConservative);
        $this->modules->riskProfileModerate = $general->integerToChecked($this->modules->riskProfileModerate);
        $this->modules->riskProfileModeratelyAggressive = $general->integerToChecked($this->modules->riskProfileModeratelyAggressive);
        $this->modules->riskProfileAggressive = $general->integerToChecked($this->modules->riskProfileAggressive);
    }

}
