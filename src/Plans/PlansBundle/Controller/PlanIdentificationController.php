<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use Symfony\Component\HttpFoundation\JsonResponse;
class PlanIdentificationController extends Controller
{

    public function indexAction()
    {
        $session = $this->get('adminsession');
        $general = $this->get('GeneralMethods');
        $session->set("section", "Plans");
        $session->set("currentpage", "PlansPlanIdentification");
        $userid = $session->userid;
        $smartplanid = $session->planid;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $currentmember = $repository->findOneBy(array('id' => $userid));

        $connectionType = $currentmember->connectionType;
        $associate = $general->plansAssociateAvailable($currentmember);


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPlan');
        $defaultplan = $repository->findOneBy(array('userid' => $userid));
        $defaultProviderLogoImage = $defaultplan->providerLogoImage;
        $defaultSponsorLogoImage = $defaultplan->sponsorLogoImage;
        $defaultBannerImage = $defaultplan->bannerImage;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $smartplanid));
        $planname = $currentplan->name;
        $planid = $currentplan->planid;
        $appVersion = $currentplan->appVersion;

        $redirectwebsiteaddress = $currentplan->redirectWebsiteAddress;
        $providerLogoImage = $currentplan->providerLogoImage;
        $sponsorLogoImage = $currentplan->sponsorLogoImage;

        $bannerImage = $currentplan->bannerImage;
        $bannerLinkUrl = $currentplan->bannerLinkUrl;
        $initialPlan = $currentplan->initialPlan;
        $generalfunctions = $this->get('generalfunctions');
        $recordkeeperPlansOptions = $generalfunctions->search("recordkeeperPlans", "userid", $userid, "plans", "PlansPlansBundle:Branding:Search/recordkeeperplans.html.twig", "name,planid,plantypecd");

        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansPlanIdentification");

        $isVwiseAdmin = ($session->masterRoleType == "vwise_admin" ? 1 : 0);
        return $this->render('PlansPlansBundle:PlanIdentification:index.html.twig', array(
            "currentplan" => $currentplan, "userid" => $userid, "planid" => $planid, "planname" => $planname, 
            "smartplanid" => $smartplanid, "redirectwebsiteaddress" => $redirectwebsiteaddress,
            "providerLogoImage" => $providerLogoImage, "sponsorLogoImage" => $sponsorLogoImage, 
            "bannerImage" => $bannerImage, "bannerLinkUrl" => $bannerLinkUrl,
            "defaultProviderLogoImage" => $defaultProviderLogoImage, "defaultSponsorLogoImage" => $defaultSponsorLogoImage, 
            "defaultBannerImage" => $defaultBannerImage, "recordkeeperPlansOptions" => $recordkeeperPlansOptions,
            "connectionType" => $connectionType, "associate" => $associate, "writeable" => $writeable,
            "session" => $session, "currentmember" => $currentmember, "isVwiseAdmin" => $isVwiseAdmin, "initialPlan" => $initialPlan,
            "appVersion" => $appVersion
        ));
    }

    public function educateSelectPlanAction(Request $request)
    {
        $session = $this->get('adminsession');
        $em = $this->getDoctrine()->getManager();
        $userid = $session->userid;
        $smartplanid = $session->planid;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $smartplanid));


        $planid = $request->request->get("planid");

        $plantypecd = $request->request->get("plantypecd");



        $currentplan->activePlanid = $planid;
        $currentplan->activePlantypecd = $plantypecd;
        if ($request->request->has('name')) {
            $currentplan->name = $request->request->get("name");
        }
        $currentplan->redirectWebsiteAddress = $request->request->get("redirectWebsiteAddress");
        $currentplan->initialPlan = $request->request->get('initialplan', '');
        $currentplan->appVersion = $request->request->get('spe2', '');
        $findPlan = $this->checkPlanExists($request, $userid,$planid,true);



        if ($findPlan != null) 
        {
            if ($findPlan->id != $currentplan->id && $findPlan->deleted == 0)
            {
                return new Response("false");
            }
        }

        $currentplan->planid = $planid;
        $em->flush();

        return new Response("true");
    }
    public function checkPlanExists(Request $request,$userid,$planid,$checkCurrent)
	{       
		$session = $request->getSession();
		$type = $session->get("type") === "smartenroll" ? "smartenroll" : "smartplan";
		$em = $this->getDoctrine()->getManager();
		$queryString = "
			SELECT 
				plan 
			FROM 
				classesclassBundle:plans plan 
			WHERE 
				UPPER(plan.planid) = :planid and plan.userid = :userid and plan.deleted = :deleted and plan.type = :type
		";
		if ($checkCurrent)
		{
			$queryString  = $queryString." and plan.id != :smartplanid";
		}
		/* @var $query \Doctrine\ORM\Query */
		$query = $em->createQuery($queryString);
		$query->setParameter('planid', strtoupper($planid));
		$query->setParameter('userid', $userid);
		$query->setParameter('deleted', 0);
		$query->setParameter('type', $type);
		if ($checkCurrent)
		{
			$query->setParameter('smartplanid', $session->get("planid"));
		}
		$findPlan = $query->getOneOrNullResult();
		return $findPlan;
	}
    public function checkPlanExistsAction(Request $request)
    {
        $findPlan = $this->checkPlanExists($request, $request->request->get("userid"),$request->request->get("planid"),false);
        if ($findPlan == null) {
			return new Response("true");
		}
        return new Response("false");
    }
    
	public function getPlanExistsAction(Request $request) {
		$plan = $this->checkPlanExists($request, $request->request->get("userid"),$request->request->get("planid"),false);
		if ($plan !== null) {
			$return = array('plan' => array('id'=>$plan->id,'types'=>$plan->getTypes()));
		}
		else {
			$return = array('plan' => null);
		}	
		return new JsonResponse($return);
	}

	public function updatePlanTypesAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
		$toggle = $request->get("action") === "disable" ? 0 : 1;

		/* @var $plan plans */
		$plan = $repository->find($request->get("planid"));
		switch ($request->get("type")) {
			case "smartenroll":
				$plan->smartEnrollAllowed = $toggle;
				break;
			case "smartplan":
				$plan->smartPlanAllowed = $toggle;
				break;
			case "powerview":
				$plan->powerviewAllowed = $toggle;
				break;
			case "irio":
				$plan->irioAllowed = $toggle;
				break;
		}
		$em->flush();
		if ($request->get("redirect")) {
			$request->getSession()->getFlashBag()->set("success", "The plan's types have been updated");
		}
		return new Response("success");
	}

    public function getRecordkeeperTypeAction()
    {
        $session = $this->get('adminsession');
        $userid = $session->userid;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $currentmember = $repository->findOneBy(array('id' => $userid));
        return new Response($currentmember->connectionType);
    }
    
    public function savePlanAction(Request $request) {
        $session = $this->get('adminsession');
        $em = $this->getDoctrine()->getManager();
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $session->planid));
        $currentplan->enrollmentRedirectOn = $request->request->get("enrollmentRedirectOn", 0);
        $currentplan->enrollmentRedirectAddress = $request->request->get("enrollmentRedirectAddress");

        $em->flush();
        return new Response("success");
    }

}
