<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;

class PersonalInformationController extends Controller
{
    
	public function indexAction() 
        {
            $this->get("session")->set("section", "Plans");
            $this->get("session")->set("currentpage", "PIPWidget");
            return $this->render("index.html.twig");
	}  
        public function render($page,$params = [])
        {
            return parent::render("PlansPlansBundle:PIPWidget:".$page,$params);
        }
        public function locationListAction(Request $request)
        {
            $table = "plansLocation";
            $queryBuilder = $this->get("QueryBuilderDataTables")->queryBuilder($table);
            $queryBuilder->andWhere("a.planid = :planid");
            $queryBuilder->setParameters(["planid" => $this->get("session")->get("planid")]);
            $data = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder($table,["a.id","a.location","a.guid"],$request->request->all(),$queryBuilder);
            $locations = $data['rows'];
            $response = $data['response'];
            $data = [];
            foreach ($locations as $location)
            {
                $data[] =[
                    $location->id,
                    $location->location,
                    $location->guid,
                    $this->render("configureButtonsLocation.html.twig",["location" => $location])->getContent()
                ];
            }
            $response['data'] = $data;
            return new JsonResponse($response);
        }
        public function locationSections($entity = null)
        {
            $sections =  
            [
                [
                    "field" => "location"
                ],
                [
                    "field" => "guid"
                ]
            ];
            foreach ($sections as &$section)
            {
                $section['description'] = $section['field'];
                $section['value'] = null;
            }
            if ($entity != null)
            {
                foreach ($sections as &$section)
                {
                    $section['value'] = $entity->{$section['field']};
                }
            }
            return $sections;
        }
        public function addLocationAction(Request $request)
        {
            $params['sections'] = $this->locationSections();
            $params['savePath'] =  "_plans_personalInformation_addLocation_saved";
            return $this->render("add.html.twig",$params);
        }
        public function addLocationSavedAction(Request $request)
        {
            $params = $request->request->all();
            $params['planid'] = $this->get("session")->get("planid");
            $params['userid'] = $this->get("session")->get("userid");
            $this->get("PlansLocationService")->add($params);
            return new Response("");
        }
        public function editLocationAction(Request $request)
        {
            $params['sections'] = $this->locationSections($this->get("PlansLocationService")->findOneBy(["id" => $request->query->get("id")]));
            $params['savePath'] =  "_plans_personalInformation_editLocation_saved";
            $params['id'] = $request->query->get("id");
            return $this->render("edit.html.twig",$params);
        }
        public function editLocationSavedAction(Request $request)
        {
            $this->get("PlansLocationService")->save($this->get("PlansLocationService")->findOneBy(["id" => $request->request->get("id")]),$request->request->all());
            return new Response("");
        }
        public function deleteLocationAction(Request $request)
        {
            $params['sections'] = $this->locationSections($this->get("PlansLocationService")->findOneBy(["id" => $request->query->get("id")]));
            $params['savePath'] =  "_plans_personalInformation_deleteLocation_saved";
            $params['id'] = $request->query->get("id");
            return $this->render("delete.html.twig",$params);
        }
        public function deleteLocationSavedAction(Request $request)
        {
            $this->get("PlansLocationService")->delete($this->get("PlansLocationService")->findOneBy(["id" => $request->request->get("id")]));
            return new Response("");
        }
        public function advisorListAction(Request $request)
        {
            $table = "plansAdvisor";
            $queryBuilder = $this->get("QueryBuilderDataTables")->queryBuilder($table);
            $queryBuilder->andWhere("a.planid = :planid");
            $queryBuilder->setParameters(["planid" => $this->get("session")->get("planid")]);
            $data = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder($table,["a.id","a.firstName","a.lastName"],$request->request->all(),$queryBuilder);
            $advisors = $data['rows'];
            $response = $data['response'];
            $data = [];
            foreach ($advisors as $advisor)
            {
                $data[] =[
                    $advisor->id,
                    $advisor->firstName,
                    $advisor->lastName,
                    $this->render("configureButtonsAdvisor.html.twig",["advisor" => $advisor])->getContent()
                ];
            }
            $response['data'] = $data;
            return new JsonResponse($response);
        }
        public function advisorSections($entity = null)
        {
            $sections =  
            [
                [
                    "field" => "firstName"
                ],
                [
                    "field" => "lastName"
                ]
            ];
            foreach ($sections as &$section)
            {
                $section['description'] = $section['field'];
                $section['value'] = null;
            }
            if ($entity != null)
            {
                foreach ($sections as &$section)
                {
                    $section['value'] = $entity->{$section['field']};
                }
            }
            return $sections;
        }
        public function addAdvisorAction(Request $request)
        {
            $params['sections'] = $this->advisorSections();
            $params['savePath'] =  "_plans_personalInformation_addAdvisor_saved";
            return $this->render("add.html.twig",$params);
        }   
        public function addAdvisorSavedAction(Request $request)
        {
            $params = $request->request->all();
            $params['planid'] = $this->get("session")->get("planid");
            $params['userid'] = $this->get("session")->get("userid");
            $this->get("PlansAdvisorService")->add($params);
            return new Response("");
        }
        public function editAdvisorAction(Request $request)
        {
            $params['sections'] = $this->advisorSections($this->get("PlansAdvisorService")->findOneBy(["id" => $request->query->get("id")]));
            $params['savePath'] =  "_plans_personalInformation_editAdvisor_saved";
            $params['id'] = $request->query->get("id");
            return $this->render("edit.html.twig",$params);
        }
        public function editAdvisorSavedAction(Request $request)
        {
            $this->get("PlansAdvisorService")->save($this->get("PlansAdvisorService")->findOneBy(["id" => $request->request->get("id")]),$request->request->all());
            return new Response("");
        }
        public function deleteAdvisorAction(Request $request)
        {
            $params['sections'] = $this->advisorSections($this->get("PlansAdvisorService")->findOneBy(["id" => $request->query->get("id")]));
            $params['savePath'] =  "_plans_personalInformation_deleteAdvisor_saved";
            $params['id'] = $request->query->get("id");
            return $this->render("delete.html.twig",$params);
        }
        public function deleteAdvisorSavedAction(Request $request)
        {
            $this->get("PlansAdvisorService")->delete($this->get("PlansAdvisorService")->findOneBy(["id" => $request->request->get("id")]));
            return new Response("");
        }
}
