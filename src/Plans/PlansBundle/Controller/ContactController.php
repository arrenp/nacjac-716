<?php

namespace Plans\PlansBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shared\ContactBundle\Classes\contact;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
class ContactController extends Controller
{
	public $contact;
	public $userid;
	public $planid;
    public function indexAction()
    {
    	$this->init();
		$contact = $this->contact->indexAction();
		$defaultContact = $this->contact->defaultContact();
		$userid = $this->userid;
		$planid = $this->planid;
		$permissions = $this->get('rolesPermissions');
		$writeable = $permissions->writeable("PlansContact");
		return $this->render('PlansPlansBundle:Contact:index.html.twig', array('userid' => $userid, 'planid' => $planid, 'contact' => $contact,'defaultContact' => $defaultContact,"writeable" => $writeable));
    }
    public function init()
    {
		$session = $this->get('adminsession');
		$session->set("section","Plans");
		$session->set("currentpage","PlansContact");
		$userid = $session->userid;
		$planid = $session->planid;
		$em = $this->getDoctrine();
		$this->userid = $userid;
		$this->planid = $planid;
		$this->contact = new contact($em,"plans",$userid,$planid);
    }
}
