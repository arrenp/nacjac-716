<?php

namespace Plans\PlansBundle\Controller;

use classes\classBundle\Entity\PlansAdviceNotificationRecipients;
use classes\classBundle\Entity\PlanRiskProfileQuestionnaire;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\RecordkeepersBundle\Classes\recordKeeper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdviceController extends Controller
{

    public $userid;
    public $planid;

    public function indexAction()
    {

        $this->init();
        $session = $this->get('adminsession');
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansAdvice");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $this->planid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $currentuser = $repository->findOneBy(array('id' => $this->userid));

        $adviceStatus = $currentplan->adviceStatus;
        if ($adviceStatus == 0) $adviceStatus = "off";
        if ($adviceStatus == 1) $adviceStatus = "on";
        if ($adviceStatus == 2) $adviceStatus = "pending";

        if ($adviceStatus == "off") $adviceChange = "on";
        else $adviceChange = "off";

        $generalfunctions = $this->get('generalfunctions');

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:recordkeeperAdviceFunds');
        $recordkeeperAdviceFunds = $repository->findBy(array('planid' => $this->planid), array('ticker' => 'ASC'));
        $totalActiveFunds = 0;

        foreach ($recordkeeperAdviceFunds as $fund)
        {
            if ($fund->status == 1)
            {
                $fund->checked = "checked";
                $totalActiveFunds++;
            }
            else $fund->checked = "";
        }

        return $this->render('PlansPlansBundle:Advice:index.html.twig', array('currentplan' => $currentplan, 'currentuser' => $currentuser, "adviceStatus" => $adviceStatus, "adviceChange" => $adviceChange, "recordkeeperAdviceFunds" => $recordkeeperAdviceFunds, "writeable" => $writeable, "totalActiveFunds" => $totalActiveFunds,"session" => $session));
    }

    public function submitAdviceFundsAction(Request $request)
    {
        $this->init();

        $kinetikService = $this->get("kinetik.plan");
        $entityManager = $this->getDoctrine()->getManager();
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $this->planid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:recordkeeperAdviceFunds');

        $kinetikPlan = $kinetikService->getPlan("{$currentplan->partnerid}*sep*{$currentplan->planid}");
        $kinetikPlanExists = false;
        if ($kinetikPlan) {
            $kinetikPlanExists = true;
        }
        
        $currentFunds = $repository->findBy(array("planid" => $this->planid));
        $funds = array();
        foreach ($currentFunds as $fund) {
            $fund->status = 0;
        }
        
        $adviceFundIdArray = array_keys(array_filter($request->get("adviceFunds")));
        $selectedFunds = $repository->findBy(array('id' => $adviceFundIdArray, "planid" => $this->planid));
        $availableFunds = [];
        foreach($selectedFunds as $fund) {
            if ($fund->presence === 'NEW') {
                $fund->presence = 'CURRENT';
            }
            if ($fund->presence === 'OLD') {
                $entityManager->remove($fund);
                continue;
            }
            $availableFunds[] = [
                'name' => $fund->name,
                'ticker' => $fund->ticker,
                'nasdaqSymbol' => null
            ];
            $funds[] = array('name' => $fund->name, "ticker" => $fund->ticker);
            $fund->status = 1;
        }

        if ($kinetikPlanExists) {
            $response = $kinetikService->updatePlan("{$currentplan->partnerid}*sep*{$currentplan->planid}", $currentplan->name, $funds);
        } else {
            $response = $kinetikService->addPlan("{$currentplan->partnerid}*sep*{$currentplan->planid}", $currentplan->name, $funds);
        }
        
        if ($response !== false) {
            $currentplan->adviceDate = new \DateTime("now");
            $currentplan->adviceStatus = $response['active'] ? 1 : 2;
            $currentplan->advice = "";
            $currentplan->adviceCompare = "";
            $entityManager->flush();
            return new Response("saved");
        }
        else {
            return new Response("error");
        }
    }

    public function init()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Plans");
        $session->set("currentpage", "PlansAdvice");
        $userid = $session->userid;
        $planid = $session->planid;
        $this->userid = $userid;
        $this->planid = $planid;
    }
    
    public function notificationRecipientsAction() {
        $this->init();
        return $this->render('PlansPlansBundle:Advice:notificationRecipientsModal.html.twig', ['userid' => $this->userid, 'planid' => $this->planid]);
    }
    
    public function addNotificationRecipientAction(Request $request) {
        $this->init();
        $em = $this->getDoctrine()->getManager();
		$repository = $this->getDoctrine()->getRepository("classesclassBundle:PlansAdviceNotificationRecipients");
		$recipient = $repository->findOneBy(['email' => trim($request->get("email")),"planid" => $this->planid]);
		if (is_null($recipient)) {
			$recipient = new PlansAdviceNotificationRecipients();
            $recipient->firstName = trim($request->get("firstName"));
            $recipient->lastName = trim($request->get("lastName"));
			$recipient->email = trim($request->get("email"));
			$recipient->userid = $this->userid;
            $recipient->planid = $this->planid;
			$em->persist($recipient);
			$em->flush();
		}
		return new Response("success");
    }
    
    public function deleteNotificationRecipientAction(Request $request) {
        $this->init();
        $em = $this->getDoctrine()->getManager();
		$recipient = $this->getDoctrine()->getRepository("classesclassBundle:PlansAdviceNotificationRecipients")->find($request->get("id"));
		$em->remove($recipient);
		$em->flush();
		return new Response("success");
    }
    
    public function updateNotificationRecipientAction(Request $request) {
        $this->init();
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:PlansAdviceNotificationRecipients");
        $email = $request->get("email");
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return new Response("invalidemail");
        }
        
        $recipient = $repository->find($request->get("id"));
        $recipient->firstName = $request->get("firstName");
        $recipient->lastName = $request->get("lastName");
        $recipient->email = $email;
        $entityManager->flush();
        return new Response("success");
    }
    
    public function updateAdviceAction(Request $request) {
        $this->init();
        
        $entityManager = $this->getDoctrine()->getManager();
        
        $planid = $request->request->get('planid', '');
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:recordkeeperAdviceFunds');
        $fundsDeleted = $repository->findBy(array('planid' => $this->planid));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $this->planid));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:PlanRiskProfileQuestionnaire');

        foreach ($fundsDeleted as $fund)
        {
            $this->getDoctrine()->getManager()->remove($fund);
            $this->getDoctrine()->getManager()->flush();

        }
        if ($currentplan->adviceStatus == 0)
        {
            $service = $this->get("PlanRiskProfileQuestionnaireService");
            $service->add(array('planid' => $currentplan->id, 'userid' => $currentplan->userid, 'riskProfileQuestionnaireId' =>
                $this->getDoctrine()->getRepository('classesclassBundle:RiskProfileQuestionnaire')->findOneBy(array('name' => 'smart401k'))->id, 'isKinetik' => 1));

            $this->get("AdviceService")->populateAndTagFunds($this->userid, $planid);
            $currentplan->adviceStatus = 2;
            $currentplan->advice = "";
            $currentplan->adviceCompare = "";
        }
        else
        {
            $row = $repository->findOneBy(array('planid' => $currentplan->id, 'userid' => $currentplan->userid));
            if (!empty($row))
                $entityManager->remove($row);

            $currentplan->adviceStatus = 0;
            $currentplan->advice = "";
            $currentplan->adviceCompare = "";
        }
        $entityManager->flush();
        return new Response("saved");
    }
    
    public function refreshAdviceFundsAction(Request $request) {
        $this->init();
        $this->get("AdviceService")->populateAndTagFunds($this->userid, $request->request->get('planid', ''));
        return new Response("saved");
    }

}
