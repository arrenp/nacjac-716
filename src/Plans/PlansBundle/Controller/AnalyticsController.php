<?php

namespace Plans\PlansBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shared\ContactBundle\Classes\contact;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Plans\PlansBundle\Controller\ga\GoogleAnalyticsAPI;
class AnalyticsController extends Controller
{

    public function visitorsOverviewAction()
    {
    	$this->init();
    	$session = $this->get('adminsession');
    	$session->set("currentpage","PlansAnalyticsVisitorsOverview");
		$startDate = $this->startDate();
		$endDate = $this->endDate();
    	$customVarValue2= $this->customVarValue2();
    	$visits = $this->getVisits("visits","ga:date");
	 	$xAxis = $this->getXAxis($visits);                
        $series = $this->getSeries($visits);
        $subtitle = "VISITS";
        $graphTitle = "Overview for ".$customVarValue2;
    	return $this->render("PlansPlansBundle:Analytics:visitorsoverview.html.twig",array("visits" => $visits,"startDate" => $startDate, "endDate" => $endDate,"customVarValue2" => $customVarValue2,"xAxis" => $xAxis,"series" => $series,"subtitle" => $subtitle,"graphTitle" => $graphTitle));
    }

    public function pagesAction()
    {
    	$this->init();
    	$session = $this->get('adminsession');
    	$session->set("currentpage","PlansAnalyticsPages");
		$startDate = $this->startDate();
		$endDate = $this->endDate();
    	$customVarValue2= $this->customVarValue2();
    	$visits = $this->getVisits("pageviews","ga:date");
	 	$xAxis = $this->getXAxis($visits);
        $series = $this->getSeries($visits);
        $subtitle = "Page Views";
        $graphTitle = "Overview for ".$customVarValue2;
    	return $this->render("PlansPlansBundle:Analytics:pages.html.twig",array("visits" => $visits,"startDate" => $startDate, "endDate" => $endDate,"customVarValue2" => $customVarValue2,"xAxis" => $xAxis,"series" => $series,"subtitle" => $subtitle,"graphTitle" => $graphTitle));
    }


    public function citiesAction()
    {
    	$this->init();
    	$session = $this->get('adminsession');
    	$session->set("currentpage","PlansAnalyticsCities");    	
    	$request = Request::createFromGlobals();
		$request->getPathInfo();

		if($request->query->get("gaMetrics") != null)
		{

			switch ($request->query->get("gaMetrics")) 
			{
				case "ga:sessions":
					$gaMetrics 		= "sessions";
					$subtitle	= "Sessions";
					break;
				case "ga:visits":
					$gaMetrics 		= "visits";
					$subtitle	= "Visits";
					break;
				case "ga:pageviews":
					$gaMetrics 		= "pageviews";
					$subtitle = "Page Views";
					break;
			}
		} 
		else 
		{
			$gaMetrics 	= "sessions";
			$subtitle	= "Sessions";
		}

		
		$startDate = $this->startDate();
		$endDate = $this->endDate();
    	$customVarValue2= $this->customVarValue2();
    	$visits = $this->getVisits($gaMetrics,"ga:city,ga:latitude,ga:longitude");
	 	$xAxis = $this->getXAxisCities($visits);
        $series = $this->getSeriesCities($visits);
        $badPlan = true;
        if (isset($visits['rows']))
        {
        	$count_rows = count($visits['rows']);
        	$badPlan = false;

    	}
    	else
    	$count_rows = 0;
		$dataArray = "";
		for ($i = 0; $i < $count_rows; $i++)
	 	{
			$dataArray .= "[".$visits['rows'][$i][1].",".$visits['rows'][$i][2].",'".addslashes($visits['rows'][$i][0])."',".$visits['rows'][$i][3]."],";
		}
		$dataArray = substr($dataArray,0,-1);


       	$graphTitle = $subtitle." by Cities for ".$customVarValue2;
    	return $this->render("PlansPlansBundle:Analytics:cities.html.twig",array("visits" => $visits,"startDate" => $startDate, "endDate" => $endDate,"customVarValue2" => $customVarValue2,"xAxis" => $xAxis,"series" => $series,"subtitle" => $subtitle,"dataArray" => $dataArray,"graphTitle" => $graphTitle,"gaMetrics" => $gaMetrics,"badPlan" => $badPlan));
    }
    public function init()
    {
    	$session = $this->get('adminsession');
    	$session->set("section","Plans");

    }
    public function getXAxisCities($visits)
    {
    	$xAxis = "";
    	if (isset($visits['rows']))
    	{
	    	$count_rows = count($visits['rows']);
		    $xAxis = "categories: [";
		    for ($i = 0; $i < $count_rows; $i++)
	     	{                	         	
	    		$xAxis .= "'".addslashes($visits['rows'][$i][0])."'".",";
		    }            
		    $xAxis = substr($xAxis,0,-1);
		   	$xAxis .= "]\n";
	   	}
	    return $xAxis;
    }
    public function getSeriesCities($visits)
    {
    	$series = "";
    	if (isset($visits['rows']))
    	{
		    $count_rows = count($visits['rows']);
		    $series = "data: [";
		    for ($i = 0; $i < $count_rows; $i++)
	     	{
		    	$series .= $visits['rows'][$i][3].",";
		    }            
		    $series = substr($series,0,-1);
		    $series .= "]\n";
		}
	    return $series;
    }


    public function getXAxis($visits)
    {
    	$count_rows = count($visits['rows']);
	 	$xAxis = "categories: [";
        for ($i = 0; $i < $count_rows; $i++) 
        {
        	$datetime = $visits['rows'][$i][0];
        	$yyyy = substr($datetime,0,4);
        	$mm = substr($datetime,4,2);
        	$monthName = date("M", mktime(0, 0, 0, floatval($mm), 10));                    	
        	$dd = substr($datetime,6,2);
        	$dt_formatted = $monthName."-".$dd;                   	         	
        	$xAxis .= '"'.$dt_formatted.'"'.",";
        }   
        if ($xAxis != "categories: [")
        {
            $xAxis = substr($xAxis,0,-1);
        }
        $xAxis .= "]\n";
        return $xAxis;
    }

    public function getSeries($visits)
    {
    	$count_rows = count($visits['rows']);
        $series = "data: [";
        for ($i = 0; $i < $count_rows; $i++) {
        	$series .= $visits['rows'][$i][1].",";
        }       
        if ($series != "data: [")
        {
            $series = substr($series,0,-1);
        }
        $series .= "]\n";
        return $series;
    }

    public function getVisits($searchVariable,$dimensions)
    {
    	$customVarValue2= $this->customVarValue2();
		$ga = new GoogleAnalyticsAPI('service');
		$ga->auth->setClientId('891041475744-ttakqc55rr0d0k2so2likptb554bfck7.apps.googleusercontent.com');
		$ga->auth->setEmail('891041475744-ttakqc55rr0d0k2so2likptb554bfck7@developer.gserviceaccount.com');
		
    	$ga->auth->setPrivateKey('../src/Plans/PlansBundle/Controller/ga/spe_analytics.p12');
    	$auth = $ga->auth->getAccessToken();

    	if ($auth['http_code'] == 200)
	 	{
			$accessToken = $auth['access_token'];
			$tokenExpires = $auth['expires_in'];
			$tokenCreated = time();
		} 
		else 
		{
			echo "Session Expired.";
		}

    	// Set the accessToken and Account-Id
		$ga->setAccessToken($accessToken);
		$ga->setAccountId('ga:68222810');

		$startDate = $this->startDate();
		$endDate = $this->endDate();

		$params = array(
				'metrics'		=> 'ga:'.$searchVariable,
				'dimensions' 	=> 'ga:customVarValue2',
				'dimensions'	=> $dimensions,
				'max-results' 	=> 500,	
				'start-date' 	=> $startDate,
				'end-date'		=> $endDate,
				'filters'		=> 	'ga:customVarValue2=='.$customVarValue2
		);
		
		
		$visits = $ga->query($params);
		return $visits;

    }

    public function customVarValue2()
    {
    	$session = $this->get('adminsession');
    	$repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
		$plan = $repository->findOneBy(array('id' => $session->planid));
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
		$member = $repository->findOneBy(array('id' => $session->userid));
		$planid = $plan->planid;

		$partnerid = $member->partnerid;
		return  ucfirst($planid."_".$partnerid);
    }

    public function startDate()
    {
    	$request = Request::createFromGlobals();
		$request->getPathInfo();
		if($request->query->get("sdt") != null)
		{
			$startTime 	= $request->query->get("sdt");
		}
		else 
		{
			$startTime = date('Y-m-d',strtotime('-30 days'));
		}

		return $startTime;
    }

    public function endDate()
    {
    	$request = Request::createFromGlobals();
		$request->getPathInfo();
		$today = $this->todayDate();
		if($request->query->get("edt") != null)
		{
			$endTime 	= $request->query->get("edt");
		}
		else
		{
			$endTime = $today['year']."-".sprintf("%02s", $today['mon'])."-".sprintf("%02s",$today['mday']);
		}
		return $endTime;
    }
    public function todayDate()
    {

    	$today = getdate();
    	return $today;
    }


}



?>