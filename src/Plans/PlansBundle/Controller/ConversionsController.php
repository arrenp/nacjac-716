<?php

namespace Plans\PlansBundle\Controller;

use classes\classBundle\Entity\accountsUsers;
use classes\classBundle\Entity\plans;
use classes\classBundle\Entity\TransitionPeriodOverChanges;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utilities\UploaderBundle\Controller\cdnUploader;

class ConversionsController extends Controller {
    
    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section","Plans");
        $session->set("currentpage","PlansConversions");
        
        $repository = $this->getDoctrine()->getRepository(plans::class);
        $currentplan = $repository->find($session->planid);
        
        $repository = $this->getDoctrine()->getRepository(TransitionPeriodOverChanges::class);
        $query = $repository->createQueryBuilder('t')
            ->select('t as change, au.email as email')
            ->leftJoin('classesclassBundle:accountsUsers', 'au', 'WITH', 'au.id = t.accountsUsersId')
            ->where('t.planid = :planid')
            ->setParameter('planid', $session->planid)
            ->orderBy('t.id', 'DESC')
            ->getQuery();
        $transitionPeriodOverChanges = $query->getResult();
                
        return $this->render('PlansPlansBundle:Conversions:index.html.twig', array('currentplan' => $currentplan, 'transitionPeriodOverChanges' => $transitionPeriodOverChanges));
    }
    
    public function saveAction(Request $request) {
        $session = $this->get('adminsession');
        $repository = $this->getDoctrine()->getRepository(plans::class);
        $currentplan = $repository->find($session->planid);
        
        $currentplan->supportContactFirstName = $request->get("supportContactFirstName");
        $currentplan->supportContactLastName = $request->get("supportContactLastName");
        $currentplan->supportContactEmail = $request->get("supportContactEmail");
        $currentplan->customerServiceNumber = $request->get("customerServiceNumber");
        
        foreach (['lastDayForLoans', 'freezeTransactionsDate', 'takeoverDate', 'freezeEndDate'] as $dateField) {
            $currentplan->$dateField = null;
            if (!empty($request->get($dateField))) {
                $currentplan->$dateField = new \DateTime($request->get($dateField));
            }
        }
        
        $soxNoticeFile = $request->files->get("soxNoticeFile");
        if (!is_null($soxNoticeFile)) {
            $cdnuploader = new cdnUploader();
            $container = $cdnuploader->containerByHostName($request->server->get('HTTP_HOST'));
            $filename = $soxNoticeFile->getClientOriginalName();
            $filename = "SOX/0client" . $session->userid . "_plan" . $session->planid . "_" .time() . "_" . $filename;   
            $theurl = $cdnuploader->uploadFile($container, $soxNoticeFile->getRealPath(), $filename);
            $currentplan->soxNoticeURL = $theurl;
        }
        
        $entityManager = $this->getDoctrine()->getManager();
        $transitionPeriodOver = (bool) $request->get("transitionPeriodOver");
        if ($currentplan->transitionPeriodOver !== $transitionPeriodOver) {
            $transitionPeriodOverChange = new TransitionPeriodOverChanges();
            $transitionPeriodOverChange->planid = $currentplan->id;
            $transitionPeriodOverChange->accountsUsersId = $session->masterid;
            $transitionPeriodOverChange->changedTo = $transitionPeriodOver;
            $entityManager->persist($transitionPeriodOverChange);
            $currentplan->transitionPeriodOver = $transitionPeriodOver;
        }
        $entityManager->flush();
        
        $this->addFlash('success', 'Your changes were saved!');
        
        return new Response("done");
    }
    
}
