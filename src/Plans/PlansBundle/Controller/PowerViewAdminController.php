<?php
namespace Plans\PlansBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Entity\plansRkp;
use classes\classBundle\Entity\plansRkpEmail;
use classes\classBundle\Entity\plansRkpCampaignDates;
use classes\classBundle\Entity\plansWidget;
use classes\classBundle\Entity\plansWidgetEmail;
use classes\classBundle\Entity\plansWidgetCampaignDates;
use classes\classBundle\Entity\CTAList;
class PowerViewAdminController extends Controller
{
    public function indexAction()
    {       
        $this->init(); 
        $data = $this->getData();
        $list = $this->getCampaigns("powerview");       
        return $this->render('PlansPlansBundle:PowerView:index.html.twig', array("data" => $data,"list" => $list,"type" => "powerview"));
    }
    
    public function getData()
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $this->account = $repository->findOneBy(array('id' => $this->session->userid)); 
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $this->plan = $repository->findOneBy(array('id' => $this->session->planid)); 
        $this->rkpParams = array('recordkeeperPlanid' => $this->plan->planid,"partnerid" => $this->account->partnerid);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansRkp');
        $this->plansRkp = $repository->findOneBy($this->rkpParams);  
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansRkpEmail');
        $this->plansRkpEmail = $repository->findBy($this->rkpParams);        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansRkpCampaignDates');
        $this->plansRkpCampaignDates = $repository->findBy($this->rkpParams); 
        if ($this->plansRkp == null)
        $this->plansRkp = $this->setClassValues ("plansRkp");
        if ($this->plansRkpEmail == null)
        $this->plansRkpEmail[0] = $this->setClassValues ("plansRkpEmail");
        if ($this->plansRkpCampaignDates == null)
        $this->plansRkpCampaignDates[0] = $this->setClassValues ("plansRkpCampaignDates");        
        return array("account" => $this->account, "plan" => $this->plan, "plansRkp" => $this->plansRkp,
            "plansRkpEmail" => $this->plansRkpEmail, "plansRkpCampaignDates" => $this->plansRkpCampaignDates);       
    }
    
    public function setClassValues($class)
    {
        if ($class == "plansRkp")
        $object = new plansRkp();
        if ($class == "plansRkpEmail")
        $object = new plansRkpEmail();
        if ($class == "plansRkpCampaignDates")
        $object = new plansRkpCampaignDates();      
        $object->partnerid = $this->account->partnerid;
        $object->recordkeeperPlanid = $this->plan->planid;
        
        return $object;
    }
           
    public function getCampaigns($type)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:CTA');       
        $ctalist= $repository->findBy(array("type" => $type)); 
        $list = array();
        foreach ($ctalist as $cta)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:CTAAccountList');
            $ctaParams = array("ctaid" => $cta->id,"userid" => $this->account->id);  
            $powerview = null;
            $ctaAccount = $repository->findOneBy($ctaParams);    
            if ($ctaAccount != null)
            {
                $ctaAccount->description = $cta->campaignDescription;
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansWidget');       
                $widget= $repository->findOneBy(array("type" => $type,"planid" => $this->plan->id,"userid" => $this->account->id)); 
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:CTAList');
                $ctaPlansParams =array("ctaid" => $cta->id,"planid" => $this->plan->id,"userid" => $this->account->id);
                $ctaPlans = $repository->findOneBy($ctaPlansParams);
                if ($ctaPlans != null || $widget == null)
                $ctaAccount->checked = "checked";
                else
                $ctaAccount->checked = "";
                $list[] = $ctaAccount;               
            }
        }     
        return $list;
    }
    
    public function saveAction(Request $request)
    {
        $this->init();
        $this->getData();
        $type = $request->request->get("type");
        $ctaids = $request->request->get("ctaids");
        $session = $this->get('adminsession');
        $findby = array("userid" => $session->userid,"planid" => $session->planid);      
        $em = $this->getDoctrine()->getManager();
        $insert = false;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansWidget');       
        $plansWidget= $repository->findOneBy($findby);
        $notIn = array("id","partnerid","recordkeeperPlanid");
        if ($plansWidget == null)
        {
            $insert = true;
            $plansWidget = new plansWidget();
            $plansWidget->type = $type;
        }
        foreach ($this->plansRkp as $key => $value)
        {
            if (!in_array($key,$notIn))
            $plansWidget->$key = $value;  
        }
        $plansWidget->planid = $this->plan->id;
        $plansWidget->userid = $this->account->id;
        if ($insert)        
        $em->persist($plansWidget);        
        
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:CTAList');       
        $ctaList= $repository->findBy($findby); 
        foreach ($ctaList as $cta)
        $em->remove($cta);  
        
        if ($ctaids != "")
        {
            $ctaidArray = explode(",",$ctaids);
            foreach ($ctaidArray as $ctaid)
            {
                $cta = new CTAList();
                $cta->userid = $this->account->id;
                $cta->planid = $this->plan->id;
                $cta->ctaid = $ctaid;
                $em->persist($cta);
            }
        }
               
        $em->flush();
        return new Response("Saved");
    }
    public function getEmailListAction()
    {
        $widgets = $this->get("WidgetsRecordkeeper")->getWidgetBasedOffLoggedInAccount();
        $widgets->getLoggedInSid();
        $emailList = $widgets->getPlanEmailList();  
        $jsonEmailList = array();
        $counter = 0;
        foreach ($emailList as $email)
        {
            $counter2 = 0;
            $jsonEmailList['data'][$counter][$counter2++] = $email->ParticipantID;
            $jsonEmailList['data'][$counter][$counter2++] = $email->FirstName;
            $jsonEmailList['data'][$counter][$counter2++] = $email->LastName;
            $jsonEmailList['data'][$counter][$counter2++] = $email->StatusCode;
            $jsonEmailList['data'][$counter][$counter2++] = $email->Email;
            $jsonEmailList['data'][$counter][$counter2++] = $email->MobilePhoneNumber;
            $counter++;            
        }
        return new Response(json_encode($jsonEmailList));
    }
    public function exportToCsvAction()
    {
        $widgets = $this->get("WidgetsRecordkeeper")->getWidgetBasedOffLoggedInAccount();
        $widgets->getLoggedInSid();
        $emailList = $widgets->getPlanEmailList();
        $csv = array();
        $csvString = "";
        $output = fopen('php://output', 'w');
        foreach ($emailList as $email)
        {
            $counter2 = 0;
            $csv[$counter2++] = $email->ParticipantID;
            $csv[$counter2++] = $email->FirstName;
            $csv[$counter2++] = $email->LastName;
            $csv[$counter2++] = $email->StatusCode;
            $csv[$counter2++] = $email->Email;
            $csv[$counter2++] = $email->MobilePhoneNumber;
            fputcsv($output, $csv);
        }       
        return new Response("");
    }
    public function emailListAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Plans");
        $session->set("currentpage","PlansPowerViewEmailList");
        return $this->render('PlansPlansBundle:PowerView:emaillist.html.twig');       
    }

    public function init()
    {         
        $session = $this->get('adminsession');
        $session->set("section","Plans");
        $session->set("currentpage","PlansPowerView");
        $this->session = $session;         
    }
    //irio stuffers
    public function irioAction()
    {
        $this->irioInit();
        $data = $this->getData();       
        return $this->render('PlansPlansBundle:Irio:index.html.twig', array("data" => $data));
    }
    public function irioInit()
    {       
        $session = $this->get('adminsession');
        $session->set("section","Plans");
        $session->set("currentpage","PlansIrio");
        $this->session = $session;  
    }
}
