<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use classes\classBundle\Entity\plansModules;
use classes\classBundle\Entity\plansLibraryHotTopics;
use classes\classBundle\Entity\plansMessaging;
use classes\classBundle\Entity\plansDocuments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Classes\plansMethods;
class AdviserHomeController extends Controller
{

    public function indexAction()
    {
    	$session = $this->get('adminsession');
    	$session->set("section","Plans");
    	$session->set("currentpage","adviserhome");

        return $this->render('PlansPlansBundle:AdviserHome:index.html.twig');
    }

}
