<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use Shared\RecordkeepersBundle\Entity\recordKeeper;
use classes\classBundle\Entity\plansOutreach;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Shared\OutreachBundle\Classes\outreach;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Entity\accountsUsersPlans;
use Shared\General\GeneralMethods;
class OutreachController extends Controller
{

    public $userid;
    public $planid;
    public $adviserid;

    public function indexAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $success = false;
        $outreach = new outreach($this->getDoctrine(), $this->userid, $this->adviserid);
        $categories = $outreach->templates("plans");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPlan');
        $defaultPlan = $repository->findOneBy(array('userid' => $this->userid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $this->planid));
        $em = $this->getDoctrine()->getManager();
        $logoImage = $this->providerLogoAction($currentplan->id)->getContent();
        $sponsorImage = $this->sponsorLogoAction($currentplan->id)->getContent();
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansEmail");
        $adviserurl = "";
        if ($this->adviserid != null) {
            $adviserurl = "&adviserid=" . $this->adviserid;
        }
        $currentplan->participantLoginWebsiteAddress = $this->participantLoginWebsiteAddress($this->planid, $this->adviserid, 0);


        $general = $this->get('GeneralMethods');
        $session = $this->get('adminsession');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $member = $repository->findOneBy(array('id' => $session->userid));
        $showParticipantDetails = false;

        if ($general->isEducate($member) || $general->isSrt($member)) {
            $showParticipantDetails = true;
        }
       
        $adviserplan = null;
        if ($this->roleType == "adviser")
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersPlans');
            $adviserplan = $repository->findOneBy(array('planid' => $this->planid, 'accountsUsersId' => $this->adviserid));

            if ($adviserplan == null)
            {
                $connection = $this->get('doctrine.dbal.default_connection');
                $connection->executeQuery("INSERT INTO accountsUsersPlans(id,userid,planid,accountsUsersId) VALUES (NULL,'".$this->userid."','".$this->planid."','".$this->adviserid."')");
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersPlans');
                $adviserplan = $repository->findOneBy(array('planid' => $this->planid, 'accountsUsersId' => $this->adviserid));                
            }
        }

        $outreach = $permissions->readable('PlansOutreachHR', "masterRoles");
        $outreachwrite = $permissions->writeable('PlansOutreachHR', "masterRoles");

        if ($request->query->get("success") != null) $success = true;
        
        $images = array();
        
        $this->addImage($images, "Alternate Provider Logo (left)", "providerLogoEmailImage",$logoImage);
        $this->addImage($images, "Alternate Sponsor Logo (right)", "sponsorLogoEmailImage",$sponsorImage);

        return $this->render('SharedOutreachBundle:Default:index.html.twig', array('showParticipantDetails' => $showParticipantDetails, 'outreach' => $outreach, 'outreachwrite' => $outreachwrite, 'defaultPlan' => $defaultPlan, 'currentplan' => $currentplan, 'categories' => $categories, "logoImage" => $logoImage, "writeable" => $writeable, "adviserurl" => $adviserurl, "adviserplan" => $adviserplan, "success" => $success,"images" => $images));
    }

    public function saveHRAction(Request $request) {
        $this->init();
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $this->planid));
        $planArray = [
            'HRFirst' => $request->request->get('HRFirst'),
            'HRLast' => $request->request->get('HRLast'),
            'HREmail' => trim($request->request->get('HREmail')),
            'participantDetails' => $request->request->has('participantDetails') ? 1 : 0
        ];
        $this->get("PlanService")->update($plan, $planArray);
        $em->persist($plan);
        $em->flush();

        return new Response('succes');
    }

    public function addImage(&$images, $description, $section,$url)
    {
        if ($url == "https://admin.smartplanenterprise.com/images/alpha.png")
        {
            $url = "";//admin crashes on dev,qa if url is https://admin.smartplanenterprise.com/images/alpha.png but not local or prod
        }
        $i = count($images);
        $images[$i] = new \stdClass();
        $images[$i]->description = $description;
        $images[$i]->section = $section;
        $images[$i]->url = $url;
        $images[$i]->width = 0;
        $images[$i]->height = 0;
        $imageProperties = @getimagesize($images[$i]->url);
        if ($imageProperties)
        {
            $images[$i]->width = $imageProperties[0];
            $images[$i]->height = $imageProperties[1];
            $images[$i]->showDisplay = "none";
        }
        else
        $images[$i]->showDisplay = "none";        
    }
    public function updateYoutubePlayerAction()
    {
        $this->init();
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $this->planid));
        if ($currentplan->youtubePlayer == 0) $currentplan->youtubePlayer = 1;
        else $currentplan->youtubePlayer = 0;
        $em->flush();
        return new Response("saved");
    }
    

    public function setLogos(&$leftImage, &$rightImage, $planid, &$currentplan,$campaignid = null)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $planid));
        $leftImage = $currentplan->providerLogoImage;
        
        if ($currentplan->providerLogoEmailImage != "") 
        $leftImage = $currentplan->providerLogoEmailImage;
        if ($currentplan->sponsorLogoImage != "") 
        $rightImage =  $currentplan->sponsorLogoImage;
        if ($currentplan->sponsorLogoEmailImage != "") 
        $rightImage = $currentplan->sponsorLogoEmailImage;        
        if ($leftImage == null)
        $leftImage = "https://admin.smartplanenterprise.com/images/alpha.png";
        if ($rightImage == null)
        $rightImage = "https://admin.smartplanenterprise.com/images/alpha.png";        
    }
    public function providerLogoAction($planid,$campaignid = null)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $planid));
        $logos = json_decode($this->setLogosUrlAction($currentplan, $campaignid)->getContent());
        return new Response($logos->leftImage);
    }
    public function sponsorLogoAction($planid,$campaignid = null)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $planid));
        $logos = json_decode($this->setLogosUrlAction($currentplan, $campaignid)->getContent());
        return new Response($logos->rightImage);
    }
    public function providerLogoStyledAction($planid,$campaignid = null)
    {
        return new Response($this->styleLogo($this->providerLogoAction($planid, $campaignid)->getContent()));
        
    }
    public function sponsorLogoStyledAction($planid,$campaignid = null)
    {
        return new Response($this->styleLogo($this->sponsorLogoActionn($planid, $campaignid)->getContent()));
    }    

    public function setLogosUrlAction($currentplan,$campaignid = null)
    {
        $leftImage = "";
        $rightImage = "";
        $planid = $currentplan->id;
        $this->setLogos($leftImage, $rightImage, $planid, $currentplan,$campaignid);
        $logos = new \stdClass();
        $logos->leftImage = $leftImage;
        $logos->rightImage = $rightImage;
        return new Response(json_encode($logos));       
    }


    function styleLogo($url)
    {
        return '<img src = "'.$url.'" style = "max-width:250px" />';
        
    }

    public function templatesAction($name, $category)
    {
        if (function_exists('newrelic_disable_autorum')) newrelic_disable_autorum();
        $category = strtolower($category);
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $planid = $request->query->get("planid", "");
        $adviserid = $request->query->get("adviserid", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentPlan = $repository->findOneBy(array('id' => $planid));
        
        if ($adviserid == null) $adviserid = 0;

        $templateid = $request->query->get("templateid", "");


        $leftImage = null;
        $rightImage = null;
        $currentplan = null;
        $this->setLogos($leftImage, $rightImage, $planid, $currentplan);
        $leftImage = $this->styleLogo($leftImage);
        $rightImage = $this->styleLogo($rightImage);


        $link = $this->participantLoginWebsiteAddress($planid, $adviserid, $templateid);
        $secondLink = "";
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachTemplates');
        $currentTemplate = $repository->findOneBy(array('id' => $templateid));



        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVideoLibrary');
        $currentVideo = $repository->findOneBy(array('filename' => $currentTemplate->video));

        //if ($currentTemplate->linkPage == "gateway") $link = "https://gateway.smartplanenterprise.com/?planId=" . $planid . "&video=" . $currentVideo->youtubeid . "&id=" . $templateid;
        if ($currentTemplate->linkPage == "gateway")    
        //$link = "<vwise_videopage3/>&mp4=".$currentVideo->mp4url;
        $link = $this->generateUrl('_manage_videoLandingPage_preview',array("id" => 3, "name" => "Gateway","userid" => $currentPlan->userid,"smartplanid" => $currentPlan->id,"mp4" => $currentVideo->mp4url),true);

        if ($currentTemplate->linkPage == "videopage" || $currentTemplate->linkPage == "moovideopage")
        $link = $this->generateUrl('_plans_outreach_videoplayer', array(), true) . "?planid=" . $planid . "&templateid=" . $templateid . "&webmurl=" . $currentVideo->webmurl . "&mp4url=" . $currentVideo->mp4url . "&linkPage=" . $currentTemplate->linkPage;

        //$link = "<vwise_videopage999/>&mp4=".$currentVideo->mp4url;
        
        /*
        if ($currentTemplate->linkPage == "moovideopage")
        $link = "<vwise_videopage1000/>&mp4=".$currentVideo->mp4url;
         * 
         */
        //$link = "https://vwise.com/_outreach/gateway/SPE_SponsorLanding.php?planId=".$planid."&templateid=".$templateid."&video=".$currentVideo->youtubeid;
        //$link = "https://<vwise_domain/>/admin/plans/outreach/videoplayer/?planid=<vwise_planid/>&templateid=".$currentTemplate->id."&webmurl=".$currentVideo->webmurl."&mp4url=".$currentVideo->mp4url."&linkPage=".$currentTemplate->linkPage;



        $sections = array("dial", "matchanalysis", "planSpecificInteractiveAndEasyToUse", "highlightsDiscretionaryEmployerMatch", "maximizesSavings", "interactive", "matchingAddedToInvestorProfile", "anytimeAnywhere", "comingVerySoon", "setUpAssistance", "poweredByVwise");

        foreach ($request->query as $key => $value)
        {
            if ($secondLink == "") $secondLink = "matchingb?" . $key . "=" . $value;
            else $secondLink = $secondLink . "&" . $key . "=" . $value;
        }

        $arguements = array('currentplan' => $currentplan, 'leftImage' => $leftImage, 'rightImage' => $rightImage, "link" => $link, "sections" => $sections, "secondLink" => $secondLink);



        return $this->render('PlansPlansBundle:Outreach:Templates/' . $category . '/' . $name . '.html.twig', $arguements);
    }

    private function participantLoginWebsiteAddress($planid, $adviserid, $templateid)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $planid));
        $link = $currentplan->participantLoginWebsiteAddress;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersPlans');
        $adviserplan = $repository->findOneBy(array('planid' => $planid, 'accountsUsersId' => $adviserid));
        
        
        
        if ($adviserplan != null && $adviserplan->participantLoginWebsiteAddress != null) $link = $adviserplan->participantLoginWebsiteAddress;



        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansOutreach');

        $outreachvariable = $repository->findOneBy(array('planid' => $planid, 'templateid' => $templateid, 'adviserid' => $adviserid));

        if ($outreachvariable == null) $outreachvariable = $repository->findOneBy(array('planid' => $planid, 'templateid' => $templateid, 'adviserid' => 0));

        if ($outreachvariable != null && $outreachvariable->id != null) $link = $outreachvariable->participantLoginWebsiteAddress;


        return $link;
    }

    public function participantLoginWebsiteAddressAction($planid, $adviserid, $templateid)
    {
        return new Response($this->participantLoginWebsiteAddress($planid, $adviserid, $templateid));
    }

    public function videoplayerAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $planid = $request->query->get("planid", "");
        $adviserid = $request->query->get("adviserid", "");
        $videopage = $request->query->get("linkPage", "");
        $webmurl = $request->query->get("webmurl", "");
        $mp4url = $request->query->get("mp4url", "");
        $templateid = $request->query->get("templateid", "");
        $link = $this->participantLoginWebsiteAddress($planid, $adviserid, $templateid);
        $leftImage = null;
        $rightImage = null;
        $currentplan = null;

        $this->setLogos($leftImage, $rightImage, $planid, $currentplan);
        $leftImage = $this->styleLogo($leftImage);
        $rightImage = $this->styleLogo($rightImage);
        $template = "PlansPlansBundle:Outreach:Videoplayer/index.html.twig";
        if ($videopage == "moovideopage") $template = "PlansPlansBundle:Outreach:Videoplayer/moo.html.twig";
        return $this->render($template, array("leftImage" => $leftImage, "rightImage" => $rightImage, "link" => $link, "webmurl" => $webmurl, "mp4url" => $mp4url));
    }

    public function videoOnlyAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $video = $request->query->get("video", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVideoLibrary');
        $currentVideo = $repository->findOneBy(array('filename' => $video));
        return $this->render('PlansPlansBundle:Outreach:Videoplayer/videoonly.html.twig', array("mp4url" => $currentVideo->mp4url, "webmurl" => $currentVideo->webmurl));
    }

    public function videoOnlyWithoutLibraryAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        return $this->render('PlansPlansBundle:Outreach:Videoplayer/videoonly.html.twig', array("mp4url" => $request->query->get("mp4", ""), "webmurl" => $request->query->get("webm", "")));
    }

    public function editAction()
    {
        try
        {
            $this->init();
            $em = $this->getDoctrine()->getManager();
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
            $currentplan = $repository->findOneBy(array('id' => $this->planid));
            $request = Request::createFromGlobals();
            $request->getPathInfo();
            $templateid = $request->request->get("id", "");
            $participantLoginWebsiteAddress = $request->request->get("participantloginwebsiteaddress", "");

            $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansOutreach');


            $outreachvariable = $repository->findOneBy(array('planid' => $this->planid, 'userid' => $this->userid, 'templateid' => $templateid, 'adviserid' => $this->adviserid));
            if ($outreachvariable == null)
            {
                $outreachvariable = new plansOutreach();
                $outreachvariable->planid = $this->planid;
                $outreachvariable->userid = $this->userid;
                if ($this->adviserid != null) $outreachvariable->adviserid = $this->adviserid;
                $outreachvariable->templateid = $templateid;
                $outreachvariable->participantLoginWebsiteAddress = $participantLoginWebsiteAddress;
                $em->persist($outreachvariable);
                $em->flush();
            }
            else
            {
                $outreachvariable->participantLoginWebsiteAddress = $participantLoginWebsiteAddress;

                $em->flush();
            }

            return new Response("saved");
        }
        catch (Exception $e)
        {
            return new Response("Error occured!: " . $e->getMessage());
        }
    }

    public function init()
    {
        $session = $this->get('adminsession');
        $this->userid = $session->userid;
        $this->planid = $session->planid;
        $this->roleType = $session->roleType;
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        if ($request->query->get("planid") != "") $this->planid = $request->query->get("planid");
        $session->set("section", "Plans");
        $session->set("currentpage", "PlansEmail");
        if ($session->roleType == "adviser") $this->adviserid = $session->clientid;
        else $this->adviserid = 0;
    }

}
