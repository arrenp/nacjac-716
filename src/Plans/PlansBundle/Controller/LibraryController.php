<?php

namespace Plans\PlansBundle\Controller;

use classes\classBundle\Entity\plans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\DocumentsBundle\Classes\documents;
use \Spe\AppBundle\Services\PlanService;

use Shared\LibraryBundle\Controller\LibraryController as Controller;

class LibraryController extends Controller
{
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $session = $this->get('adminsession');
        $this->tablename = "plans";
        $this->userid = $session->userid;
        $this->planid = $session->planid;
        $this->findBy = array('planid' => $session->planid);
        $session->set("section","Plans");
        $session->set("currentpage","PlansLibrary");
        $permissions = $this->get('rolesPermissions');
        $this->writeable = $permissions->writeable("PlansLibrary");
        $this->path = 'PlansPlansBundle:';

    }
    

}
