<?php

namespace Plans\PlansBundle\Controller;

use classes\classBundle\Entity\PlansInvestmentsConfiguration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use classes\classBundle\Entity\plansModules;
use classes\classBundle\Entity\plansLibraryHotTopics;
use classes\classBundle\Entity\plansDocuments;
use classes\classBundle\Entity\plansFunds;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansPortfolioFunds;
use classes\classBundle\Entity\plansRiskBasedFunds;
use classes\classBundle\Entity\accountsUsersPlans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Classes\plansMethods;
use classes\classBundle\Entity\PlansMessages;
use classes\classBundle\Entity\plansLibraryUsers;
use classes\classBundle\Entity\PlansInvestmentsLinkYearsToRetirementAndScore;
use classes\classBundle\Entity\PlansInvestmentsConfigurationColors;
use classes\classBundle\Entity\PlansInvestmentsConfigurationFundsGroupValuesColors;
use classes\classBundle\Entity\PlansInvestmentsScore;
use classes\classBundle\Entity\PlansInvestmentsYearsToRetirement;
use classes\classBundle\Entity\PlansInvestmentsConfigurationDefaultFundsGroupValuesColors;

use Shared\General\AddPlanHelperFunctions;

class PlansHomeController extends Controller
{
    private $planHelperFunctions;

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->planHelperFunctions = new AddPlanHelperFunctions($this->getDoctrine()->getManager());

    }

    public function indexAction(Request $request)
    {
        $session = $this->get('adminsession');
        $session->set("section", "Plans");
        $session->set("currentpage", "PlansHome");
        $generalfunctions = $this->get('generalfunctions');
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansHome");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array('id' => $session->clientid));
        $type = $request->query->get("type");
        $session->set("type",$type);
        return $this->render('PlansPlansBundle:PlansHome:index.html.twig', array("session" => $session, "writeable" => $writeable, "roleType" => $session->roleType,"user" => $user,"type" => $type));
    }
    
    public function addPlanPromptAction()
    {
        $session = $this->get('adminsession');
        $general = $this->get('GeneralMethods');
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansHome");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array('id' => $session->clientid));
        $type = $session->type;
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $currentaccount = $repository->findOneBy(array('id' => $session->userid));
		$associate = $general->plansAssociateAvailable($currentaccount);
		$dummyplan = new plans;
        return $this->render('PlansPlansBundle:PlansHome:addplanprompt.html.twig', array("session" => $session, "writeable" => $writeable, "roleType" => $session->roleType,"user" => $user,"type" => $type,"currentaccount"=>$currentaccount, 'plan' => $dummyplan, 'associate' => $associate));
    }

    public function addPlanAction(Request $request)
    {
        $session = $this->get('adminsession');
        $general = $this->get('GeneralMethods');
        $em = $this->getDoctrine()->getManager();
        $userid = $session->userid;
        $planid = $request->request->get("displayedplanid");
        $name = $request->request->get("name");

        $addedPlanId = $general->addPlan($em, $userid, $planid, $name, $session->clientid, $session->type);
        $session->set("planid", $addedPlanId);

        $repository = $em->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $userid));
		if ($request->request->get("doAssociate", "0")) {
			if (strpos($account->connectionType, "SRT") === 0) {
				return $this->forward('RecordkeepersSRTBundle:Default:selectPlan');
			}
			if ($account->connectionType === "Relius" ) {
				return $this->forward('RecordkeepersReliusBundle:Default:selectPlan');
			}
			if ($account->connectionType === "ExpertPlan" ) {
				return $this->forward('RecordkeepersExpertPlanBundle:Default:selectPlan');
			}
			if ($account->connectionType === "Envisage") {
				return $this->forward('RecordkeepersEnvisageBundle:Default:selectPlan');
			}
		}

        return new Response("true");
    }

    public function addAccountsUsersPlan($clientid,$planid)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $user= $repository->findOneBy(array('id' => $clientid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan= $repository->findOneBy(array('id' => $planid));        
        if (!$user->allPlans)
        {
            $plan = new accountsUsersPlans();
            $plan->userid = $user->userid;
            $plan->plan= $currentplan;
            $plan->accountsUsersId = $user->id;
            $em->persist($plan);
            $em->flush();
        }
    } 
    public function copyPlanModalAction(Request $request)
    {
        $session = $this->get('adminsession');
        $general = $this->get('GeneralMethods');
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansHome");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan= $repository->findOneBy(array('id' => $request->query->get("id")));
        $types = $this->get("PlanService")->currentTypes();
        $types = array_diff_key($types, array_flip(["powerview","irio"])); // BSD-1237: hide powerview and irio from clients
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $currentaccount = $repository->findOneBy(array('id' => $session->userid));
		$associate = $general->plansAssociateAvailable($currentaccount);
        return $this->render('PlansPlansBundle:PlansHome:copyplanmodal.html.twig', compact("types","plan","session","associate","writeable","currentaccount"));    
    }
    public function copyPlanAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $planid = $request->request->get("planid");
        $userid = $request->request->get("userid", "");
        $copyinvestments = $request->request->get("copyinvestments", "notset");
        $overwritePlanId = $request->request->get("overwritePlanId");
        
        $useplanid = true;
        if ($userid == "")
        {
            $userid = $session->userid;
            $useplanid = false;
        }
        $em = $this->getDoctrine()->getManager();
        $methods = $this->get('GeneralMethods');

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $userid));
        $copyPortfolios = 0;
        if ($copyinvestments =="notset")
        {
            if (in_array($account->connectionType, $methods->recordkeeperPortfoliosAvailable) ) {
                $copyPortfolios = 1;              
            }
        }
        else
        {
            $copyPortfolios = (int)$copyinvestments;
        }

        $copyRiskBasedFunds = 0;
        if ($copyinvestments =="notset")
        {
            if (in_array($account->connectionType, $methods->recordkeeperRiskBasedFundsAvailable)) {
                $copyRiskBasedFunds = 1;
            }
        }
        else
        {
            $copyRiskBasedFunds =(int)$copyinvestments;
        }
        


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $originalPlan = $repository->findOneBy(array('id' => $planid));
        $type = $request->request->get("type", $originalPlan->type);
        $planExists = $repository->findOneBy(array('planid' => $originalPlan->planid,"partnerid" => $originalPlan->partnerid,"deleted" => 0, "type" => $type));
        if ($overwritePlanId !== null) {
            $plantobeadded = $repository->findOneBy(['id' => $overwritePlanId]);
            $types = $plantobeadded->getTypes();
            $types[] = $type;
            $this->planHelperFunctions->addPlanSegment($plantobeadded, $originalPlan, ["id", "planid", "activePlanid", "activePlantypecd","name"]);
            $plantobeadded->addTypes($types);
            $plantobeadded->userid = $userid;
            $plantobeadded->modifiedDate = new \DateTime("NOW");
            $useplanid = false;
            $planExists = null;
            
            $deleteFilter = ['planid' => $overwritePlanId];  
            
            $this->deleteTableData("plansModules", $deleteFilter);
            $this->deleteTableData("plansLibraryHotTopics", $deleteFilter);
            $this->deleteTableData("PlansMessages", $deleteFilter);
            $this->deleteTableData("plansDocuments", $deleteFilter);
            $this->deleteTableData("plansRiskBasedFunds", $deleteFilter);
            
            $this->deleteTableData("PlansInvestmentsConfiguration", $deleteFilter);
            $this->deleteTableData("PlansInvestmentsConfigurationDefaultFundsGroupValuesColors", $deleteFilter);
            $this->deleteTableData("PlansInvestmentsConfigurationColors", $deleteFilter);
            
            $this->deleteTableData("PlansInvestmentsScore", $deleteFilter);
            $this->deleteTableData("PlansInvestmentsConfigurationFundsGroupValuesColors", $deleteFilter);
            $this->deleteTableData("PlansInvestmentsYearsToRetirement", $deleteFilter);
            $this->deleteTableData("PlansInvestmentsLinkYearsToRetirementAndScore", $deleteFilter);
            
            $this->deleteTableData("plansLibraryUsers", $deleteFilter);
            $this->deleteTableData("plansFunds", $deleteFilter);
            $this->deleteTableData("plansPortfolios", $deleteFilter);
            $this->deleteTableData("plansPortfolioFunds", $deleteFilter);
            $this->deleteTableData("PlansAutoEnroll", $deleteFilter);
            $this->deleteTableData("PlansAutoEscalate", $deleteFilter);
            $this->deleteTableData("PlansAutoIncrease", $deleteFilter);
        }
        else {
            $plantobeadded = new plans();
            $saveplanid = $plantobeadded->planid;
            $this->planHelperFunctions->addPlanSegment($plantobeadded, $originalPlan);
            $plantobeadded->userid = $userid;
            if ($planExists != null) {
                $plantobeadded->planid = $saveplanid;
            }
            $plantobeadded->createdDate = new \DateTime("NOW");
            $plantobeadded->modifiedDate = new \DateTime("NOW");
            $plantobeadded->type = $type;
        }
        if (!$useplanid)
        {    
            if ($planExists != null) {
                $plantobeadded->name = $plantobeadded->name . " copy"; 
            }
        }
        else
        {
            $plantobeadded->planid = $originalPlan->planid;
            $plantobeadded->partnerid = $account->partnerid;
            $plantobeadded->name = $originalPlan->name;
        }
        if ($request->request->get("subplanid","") != "")
        {
            $plantobeadded->planid = $originalPlan->planid."-".$request->request->get("subplanid","");
            $plantobeadded->masterplanid = $originalPlan->planid;
            $plantobeadded->name = $request->request->get("subplanname","");
            $plantobeadded->subplan = 1;
        }


        $em->persist($plantobeadded);
        $em->flush(); //need to do this early to get newest id to copy to other tables
        $this->planHelperFunctions->copyTableData("PlanAppColors", "PlanAppColors", "planid", $planid, $plantobeadded->id, $userid);
        $this->planHelperFunctions->copyTableData("plansModules", "plansModules", "planid", $planid, $plantobeadded->id, $userid);
        $this->planHelperFunctions->copyTableData("plansLibraryHotTopics", "plansLibraryHotTopics", "planid", $planid, $plantobeadded->id, $userid);
        $this->planHelperFunctions->copyTableData("PlansMessages", "PlansMessages", "planid", $planid, $plantobeadded->id, $userid);
        $this->planHelperFunctions->copyTableData("plansDocuments", "plansDocuments", "planid", $planid, $plantobeadded->id, $userid);
        if ($copyRiskBasedFunds) {
            $this->planHelperFunctions->copyTableData("plansRiskBasedFunds", "plansRiskBasedFunds", "planid", $planid, $plantobeadded->id, $userid);
        }
        $this->planHelperFunctions->copyTableData('PlansInvestmentsConfiguration', 'PlansInvestmentsConfiguration', 'planid', $planid, $plantobeadded->id, $userid);
        $this->planHelperFunctions->copyTableData('PlansInvestmentsConfigurationDefaultFundsGroupValuesColors', 'PlansInvestmentsConfigurationDefaultFundsGroupValuesColors', 'planid', $planid, $plantobeadded->id, $userid);
        $this->planHelperFunctions->copyTableData('PlansInvestmentsConfigurationColors', 'PlansInvestmentsConfigurationColors', 'planid', $planid, $plantobeadded->id, $userid);
        $this->planHelperFunctions->copyTableDataException($userid, $plantobeadded->id, "Plans", array('planid' => $planid) );
        $this->planHelperFunctions->copyTableData('PlansAutoEnroll', "PlansAutoEnroll", "planid", $planid, $plantobeadded->id, $userid);
        $this->planHelperFunctions->copyTableData('PlansAutoEscalate', "PlansAutoEscalate", "planid", $planid, $plantobeadded->id, $userid);
        $this->planHelperFunctions->copyTableData('PlansAutoIncrease', "PlansAutoIncrease", "planid", $planid, $plantobeadded->id, $userid);

        // copy plansLibraryUser logic
        $repo = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryUsers');
        $result = $repo->findBy(array('planid' => $planid));
        foreach($result as $row) {
            $newUser = new plansLibraryUsers();
            $newUser->planid = $plantobeadded->id;
            $newUser->videoid = $row->videoid;
            $em->persist($newUser);
        }
        $em->flush();



        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        $funds = $repository->findBy(array('planid' => $planid));

        if ($copyPortfolios)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansPortfolios');
            $portfolios = $repository->findBy(array('planid' => $planid));
        }

        $fundsArray = array();
        $portfoliosArray = array();

        foreach ($funds as &$fund)
        {
            $fundsArray[$fund->id] = $fund;
        }
        if ($copyPortfolios) foreach ($portfolios as &$portfolio)
            {
                $portfoliosArray[$portfolio->id] = $portfolio;
            }

        if ($copyPortfolios)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansPortfolioFunds');
            $portfolioFunds = $repository->findBy(array('planid' => $planid));
            foreach ($portfolioFunds as $portfolioFund)
            {
                if (isset($fundsArray[$portfolioFund->fundid]) && !isset($fundsArray[$portfolioFund->fundid]->newid))
                {
                    $newFund = new plansFunds();
                    $this->planHelperFunctions->addPlanSegment($newFund, $fundsArray[$portfolioFund->fundid]);
                    $newFund->planid = $plantobeadded->id;
                    $newFund->userid = $userid;
                    $em->persist($newFund);
                    $em->flush();
                    $fundsArray[$portfolioFund->fundid]->newid = $newFund->id;
                }
                if (isset($portfoliosArray[$portfolioFund->portfolioid]) && !isset($portfoliosArray[$portfolioFund->portfolioid]->newid))
                {
                    $newPortfolio = new plansPortfolios();
                    $this->planHelperFunctions->addPlanSegment($newPortfolio, $portfoliosArray[$portfolioFund->portfolioid]);
                    $newPortfolio->planid = $plantobeadded->id;
                    $newPortfolio->userid = $userid;
                    $em->persist($newPortfolio);
                    $em->flush();
                    $portfoliosArray[$portfolioFund->portfolioid]->newid = $newPortfolio->id;
                }
                if (isset($fundsArray[$portfolioFund->fundid]->newid) && isset($portfoliosArray[$portfolioFund->portfolioid]->newid))
                {
                    $newPortfolioFund = new plansPortfolioFunds();
                    $this->planHelperFunctions->addPlanSegment($newPortfolioFund, $portfolioFund);
                    $newPortfolioFund->fundid = $fundsArray[$portfolioFund->fundid]->newid;
                    $newPortfolioFund->portfolioid = $portfoliosArray[$portfolioFund->portfolioid]->newid;
                    $newPortfolioFund->planid = $plantobeadded->id;
                    $newPortfolioFund->userid = $userid;
                    $em->persist($newPortfolioFund);
                    $em->flush();
                }
            }
        }

        foreach ($fundsArray as &$fund)
        {
            if (!isset($fund->newid))
            {
                $newFund = new plansFunds();
                $this->planHelperFunctions->addPlanSegment($newFund, $fund);
                $newFund->planid = $plantobeadded->id;
                $newFund->userid = $userid;
                $em->persist($newFund);
            }
        }
        $em->flush();
        if ($copyPortfolios)
        {
            foreach ($portfoliosArray as &$portfolio)
            {
                if (!isset($portfolio->newid))
                {
                    $newPortfolio = new plansPortfolios();
                    $newPortfolio->userid = $userid;
                    $this->planHelperFunctions->addPlanSegment($newPortfolio, $portfolio);
                    $newPortfolio->planid = $plantobeadded->id;
                    $em->persist($newPortfolio);
                }
            }
            $em->flush();
        }

        $this->addAccountsUsersPlan($session->clientid,$plantobeadded->id);
        $session->set("planid", $plantobeadded->id);
        return new Response("Plan Copied");
    }

    public function deletePlanAction(Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $planid = $request->request->get("planid");
        $currentplanid = $session->get("planid");
        $currentplandeleted = false;
        if ($currentplanid == $planid)
        {
            $currentplandeleted = true;
            $session->remove("planid");
        }




        /* hard delete */
        //$plansMethods = new plansMethods($this->container);
        //$plansMethods->deleteplan($planid);
        /* end hard delete */
        /* soft delete */
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $deletedPlan = $repository->findOneBy(array('id' => $planid));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $searchedPlan = $repository->findOneBy(array('planid' => $deletedPlan->planid, 'userid' => $deletedPlan->userid), array("deleted" => "DESC"));

        if ($searchedPlan == null) $deleteIndex = 1;
        else $deleteIndex = $searchedPlan->deleted + 1;


        $deletedPlan->deletedDate = new \DateTime("NOW");
        $deletedPlan->deleted = $deleteIndex;
        $em->flush();
        /* end soft delete */
        if ($currentplandeleted) return new Response("Current plan deleted");
        else return new Response("Plan deleted");
    }

    public function setPlanAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $planid = $request->request->get("planid");
        $session = $request->getSession();
        $userid = $session->get("userid");
        $session->set("planid", $planid);
        $planid = $session->get("planid");
        $session->save();

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array("id" => $planid));
        $currentplan->lastModified = time();
        $currentplan->modifiedDate = new \DateTime("NOW");
        $em->flush();

        $this->planHelperFunctions->fillInMissingPlanTables($userid, $planid);
        return new Response("Plan set");
    }

    public function displayPlanAction($planid, $writeable, $isadviser,$page = "planlist")
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array("id" => $planid));
        if ($page == "planlist")
        $template = "PlansPlansBundle:PlansHome:Search/displayplan.html.twig";
        if ($page == "subplanlist")
        $template = "PlansPlansBundle:PlansHome:Search/displaysubplan.html.twig";

        return $this->render($template, array("plan" => $plan, "writeable" => $writeable, "isadviser" => $isadviser));
    }

    public function deletePlanConfirmAction()
    {
        $session = $this->get('adminsession');
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array("id" => $request->query->get("id")));
        return $this->render('PlansPlansBundle:PlansHome:deleteplanconfirm.html.twig',array('plan' => $plan, 'session' => $session, 'types' => $plan->getTypes()));
    }

	public function searchDatatablesAction() {
		$em = $this->getDoctrine()->getManager();
		$connection = $em->getConnection();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $request = $request->request->all();
        $start = $request["start"];
        $maxresults = $request["length"];
        $types = explode(",", $request['types']);
        $template = $request['template'];
		$session = $this->get('adminsession');

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array('id' => $session->clientid));
			
		$searchstring = " WHERE a.userid = :userid AND a.deleted = 0 ";
		$parameters = [':userid' => $session->userid];
		
		if (!in_array($session->masterRoleType, ["vwise_admin","admin_master"])) {
			$searchstring .= " AND (a.isHomePlan = 0  OR (a.isHomePlan = 1 AND a.id IN (SELECT b.planid FROM HomePlanAccess b WHERE b.accountsUsersId=:accountsUsersId )) ) ";
			$parameters[':accountsUsersId'] = $session->clientid;
		}

		if (!$user->allPlans) {
			$searchstring .= " AND a.id IN (SELECT c.planid FROM accountsUsersPlans c WHERE c.accountsUsersId=:accountsUsersId) ";
			$parameters[':accountsUsersId'] = $session->clientid;
		}

		if ($session->type == "smartenroll") {
			$searchstring .= " AND a.smartEnrollAllowed = 1 ";
		}
		if ($session->type == "smartplan") {
			$searchstring .= " AND a.smartPlanAllowed = 1 ";
		}
		if ($session->type == "powerview") {
			$searchstring .= " AND a.powerviewAllowed = 1 ";
		}
		if ($session->type == "irio") {
			$searchstring .= " AND a.irioAllowed = 1 ";
		}

		if ($request['page'] == "subplanlist") {
			$searchstring .= " AND a.subplan = 0 ";
		}

		$queryCountString = "SELECT COUNT(a.id) FROM plans a $searchstring";
		$statement = $connection->prepare($queryCountString);
        $statement->execute($parameters);
        $recordsTotal = $statement->fetchColumn();

		if (isset($request['search']) && $request['search']['value'] != '')
		{
			$searchstring .= " AND ( a.id LIKE :searchvalue OR a.name LIKE :searchvalue OR a.planid LIKE :searchvalue OR a.createdDate LIKE :searchvalue) ";
			$parameters[':searchvalue'] = '%' . $request['search']['value'] . '%';
		}

		$orderByArr = [];
		foreach ($request["order"] as $val) {
			$orderByArr[] = "{$types[$request["columns"][$val["column"]]["data"]]} {$val["dir"]}";
        }
		$orderByArr[] = "a.id ASC";
		$orderByString = " ORDER BY " . implode(", ", $orderByArr);


		$queryFilteredCountString = "SELECT COUNT(a.id) FROM plans a $searchstring";
		$statement = $connection->prepare($queryFilteredCountString);
		$statement->execute($parameters);
		$recordsFiltered = $statement->fetchColumn();

		$queryString = "SELECT a.* FROM plans a $searchstring $orderByString LIMIT $start, $maxresults";
		$statement = $connection->prepare($queryString);
		$statement->execute($parameters);
		$items = $statement->fetchAll();

        $jsonheader = '{
		
		"recordsTotal": ' . $recordsTotal . ',
		"recordsFiltered": ' . $recordsFiltered . ',
		"data": [';

        $params = array('items' => $items, 'request' => $request);

        $generalMethods = $this->get('GeneralMethods');
        $generalMethods->datatablesFilterData($items);
        $jsoncontent = $this->render($template, $params);
        $jsoncontent = $jsoncontent->getContent();
        $generalMethods->datatablesFilterJson($jsoncontent);
        $jsonfooter = ']
		}';
		$response = new Response($jsonheader . $jsoncontent . $jsonfooter);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
    
    private function deleteTableData($table, $findby) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $table);
        $entities = $repository->findBy($findby);
        foreach ($entities as $entity) {
            $entityManager->remove($entity);
        }
        $entityManager->flush();
    }

    public function plansExportToCsvAction()
    {
        $session = $this->get('adminsession');
        $type = $session->type;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $querywhere = array('userid' => $session->userid, 'deleted' => 0);
        switch ($type) {
            case "smartplan": $querywhere['smartPlanAllowed'] = 1;
                break;
            case "smartenroll": $querywhere['smartEnrollAllowed'] = 1;
                break;
            case "irio": $querywhere['irioAllowed'] = 1;
                break;
            case "powerview": $querywhere['powerviewAllowed'] = 1;
                break;
            default: $querywhere['smartPlanAllowed'] = 1;
                break;
        }
        $plans = $repository->findBy($querywhere, array('id' => "DESC"));
        $fp  = tmpfile();

        fputcsv($fp, explode(",","Plan Name,Plan ID,Date Created"));
        foreach ($plans as $plan)
        {
            fputcsv($fp, array($plan->name,$plan->planid,$plan->createdDate->format('Y-m-d')));
        }

        $response = new Response(file_get_contents(stream_get_meta_data($fp)['uri']));
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="UserList.csv"');
        return $response;
    }

}
