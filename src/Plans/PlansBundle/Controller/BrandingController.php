<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;

class BrandingController extends Controller
{

    private $userid;
    private $planid;
    private $plan;

    public function indexAction()
    {
        $this->init();
        $images = array();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansBranding");
        $this->addImage($images, "Provider Logo (left)", "providerLogoImage");
        $this->addImage($images, "Sponsor Logo (right)", "sponsorLogoImage");
        $this->addImage($images, "Banner Image (center)", "bannerImage");
        
        $success = $request->query->get("success");
        return $this->render('PlansPlansBundle:Branding:index.html.twig', array('images' => $images, 'bannerLinkUrl' => $this->plan->bannerLinkUrl, "success" => $success, "writeable" => $writeable));
    }

    public function addImage(&$images, $description, $section)
    {
        $i = count($images);
        $images[$i] = new \stdClass();
        $images[$i]->description = $description;
        $images[$i]->section = $section;
        $images[$i]->url = $this->plan->$section;
        $images[$i]->width = 0;
        $images[$i]->height = 0;
        $imageProperties = @getimagesize($images[$i]->url);
        if ($imageProperties)
        {
            $images[$i]->width = $imageProperties[0];
            $images[$i]->height = $imageProperties[1];
            $images[$i]->showDisplay = "none";
        }
        else
        $images[$i]->showDisplay = "none";
    }

    public function init()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Plans");
        $session->set("currentpage", "PlansBranding");
        $this->userid = $session->userid;
        $this->planid = $session->planid;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $this->plan = $repository->findOneBy(array('id' => $this->planid));
    }

    public function uploadlogostestAction()
    {
        return new Response("test");
    }

}
