<?php
namespace Plans\PlansBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plansMedia;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
class MediaController extends Controller
{
    private $userid;
    private $planid;
    private $connection;
    
    public function indexAction()
    {
        $this->init();
        $mediaList = $this->getMediaList();        
        $media = $this->getCurrentMedia();
        return $this->render('PlansPlansBundle:Media:index.html.twig',array("mediaList" => $mediaList, 'media' => $media));
    }
    public function init()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Plans");
        $session->set("currentpage", "PlansMedia");
        $this->connection = $this->get('doctrine.dbal.default_connection');
        $this->userid = $session->userid;
        $this->planid = $session->planid;
    }
    public function saveMediaAction(Request $request)
    {
        $this->init();
        $media = $this->getCurrentMedia();
        $em = $this->getDoctrine()->getManager();
        $req = $request->request->all();
        foreach ($req as $key => $value)
        {
			if ($key == 'enddate_sc') {
				$value = new \DateTime($value);
			}
            $media->$key = $value;
        }
        if (isset($media->added))
        $em->persist($media);
        $em->flush();

        return new Response("");
    }
    public function getMediaList()
    {
        $media = $this->getCurrentMedia();
        $mediaList = array();
        $mediaList['welcome']['description'] = "Welcome";
        $mediaList['goodbye']['description'] = "GoodBye"; 
        
        foreach ($mediaList as  $key => $value)
        {
            $sql = "SELECT * FROM media WHERE smartplanSection = '".$key."'";
            $mediaList[$key]['list'] = $this->connection->fetchAll($sql);
            foreach ($mediaList[$key]['list'] as &$list)
            {
				$key2 = $key . '_sc';
                if ($media->$key == $list['source'])
                $list['checked'] = 'checked';
                else
                $list['checked'] = '';
                if ($media->$key2 == $list['source'])
                $list['checked_sc'] = 'checked';
                else
                $list['checked_sc'] = '';


                $sql = "SELECT * FROM media_localization WHERE media_id = ".$list['id'];
                $list['languages'] = $this->connection->fetchAll($sql);   
            }                        
        }
        return $mediaList;
    } 
    public function getCurrentMedia()
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansMedia');
        $media = $repository->findOneBy(array('planid' => $this->planid)); 
        if ($media === null)
        {
            $media = new plansMedia();
            $media->userid = (int)$this->userid;
            $media->planid = (int)$this->planid;
            $media->added = 1;
        }
        return $media;
    }
}
