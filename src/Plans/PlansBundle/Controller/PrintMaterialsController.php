<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Sessions\AdminBundle\Classes\adminsession;
class PrintMaterialsController extends Controller
{
    public function indexAction()
    {
    $generalfunctions = $this->get('generalfunctions');
  	$session = $this->get('adminsession');
  	$session->set("section","Plans");
  	$session->set("currentpage","PlansPrintPromotion");
    $userid = $session->userid;
 
    if ($session->roleType != "adviser")
    {
      $findby= "a.userid,^a.adviserid";
      $findbyvalues = $userid.",0";
    }

    if ($session->roleType == "adviser")
    {
      $adviserid = $session->clientid;
      $findby= "a.userid,|a.adviserid";
      $findbyvalues = $userid.",".$adviserid;
    }
    
    return $this->render('PlansPlansBundle:PrintMaterials:index.html.twig', array( "userid" => $userid,"findby" => $findby,"findbyvalues" => $findbyvalues));
    }

    public function searchAction()
    {
      $generalfunctions = $this->get('generalfunctions');
      $options = $generalfunctions->searchDataTablesOptions("printMaterials","userid",$userid,"documents","SettingsSettingsBundle:PrintMaterials:Search/printmaterials.html.twig");

      var_dump($options);
      return new Response("");
    }
}
