<?php

namespace Plans\PlansBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\OutreachBundle\Classes\outreach;
use classes\classBundle\Entity\sponsorConnectRequests;
use classes\classBundle\Entity\launchUsers;
use classes\classBundle\Entity\launchToolRecipients;
use classes\classBundle\Classes\mailEngine;
use classes\classBundle\MappingTypes\EncryptedType;
class SponsorConnectController extends Controller
{
    public $fromSections = array();
    public $toSections = array();
    public function indexAction(Request $request)
    {
        $this->init();
        $this->sections();
        $session = $this->get('adminsession');
        $session->set("currentpage", "plansNavSponsorConnect");
        $sponsorRequest = $this->getSponsorRequest();
        $template = "PlansPlansBundle:SponsorConnect:index.html.twig";
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $this->planid));
        $return = array("fromSections" => $this->fromSections,"toSections" => $this->toSections,"sponsorRequest" => $sponsorRequest,"plan" => $plan); 
        return $this->render($template,$return);
    }
    public function init($request = null)
    {
        $session = $this->get('adminsession');
        $session->set("section", "Plans");
        
        $this->userid = $session->userid;
        $this->planid = $session->planid;
        $this->roleType = $session->roleType;
    }
    public function sections($id = null)
    {
        $this->addSection($this->fromSections,"fromFirstName","First Name");  
        $this->addSection($this->fromSections,"fromLastName","Last Name"); 
        $this->addSection($this->fromSections,"fromEmailAddress","Email Address"); 
        $this->addSection($this->fromSections,"fromPhone","Phone");
        $this->addSection($this->toSections,"toFirstName","First Name");  
        $this->addSection($this->toSections,"toLastName","Last Name"); 
        $this->addSection($this->toSections,"toEmailAddress","Email Address"); 
        $this->addSection($this->toSections,"toPhone","Phone");
        $sponsorConnectRequests = $this->getSponsorRequest($id);
        if (isset($sponsorConnectRequests->id))
        {
            $this->copySection($this->fromSections,$sponsorConnectRequests);
            $this->copySection($this->toSections,$sponsorConnectRequests);
        }    
    }
    public function addSection(&$section,$name,$description)
    {
        $section[$name] =array();
        $section[$name]['name'] = $name;
        $section[$name]['description'] = $description;
        $section[$name]['value'] = "";
    }
    public function copySection(&$sections,$sponsorConnectRequests)
    {
        foreach ($sections as  $key => $value)
        $sections[$key]['value'] = $sponsorConnectRequests->$key;
    }
    public function createSponsorRequestAction(Request $request)
    {
        $this->init();
        $add = 0;
        $sponsorRequest = $this->getSponsorRequest();
        $em = $this->getDoctrine()->getManager();
        $request = $request->request->all();
        if (!isset($sponsorRequest->id))
        {
            $add = 1;
            $sponsorConnectRequests = new sponsorConnectRequests();
        }
        else
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:sponsorConnectRequests');
            $sponsorConnectRequests= $repository->findOneBy(array('id' => $sponsorRequest->id));
        }
        foreach ($request as $key => $value)
        {
            if (in_array($key, ['fromEmailAddress','toEmailAddress'])) {
                $value = trim($value);
            }
            $sponsorConnectRequests->$key = $value; 
        }
        $sponsorConnectRequests->userid = $this->userid;
        $sponsorConnectRequests->planid = $this->planid;
        $sponsorConnectRequests->requestDate = new \DateTime("now");
        $sponsorConnectRequests->scheduleDate = new \DateTime("now");
        $sponsorConnectRequests->requestStatus = 'done';
        if ($add) {
            $em->persist($sponsorConnectRequests);
        }

        $launchUsers = new launchUsers();
        $launchUsers->partnerid = $this->userid;
        $launchUsers->planid = $this->planid;
        $launchUsers->firstName = $request['toFirstName'];
        $launchUsers->lastName = $request['toLastName'];
        $launchUsers->telephone = $request['toPhone'];
        $launchUsers->urlPass = launchUsers::getRandSalt();
        $launchUsers->uploadDate = null;
        $launchUsers->email = trim($request['toEmailAddress']);
        $em->persist($launchUsers);

        $em->flush();
        $this->sponsorConnectEmailNotification($sponsorConnectRequests);
        return new Response("success");
    }

    public function sponsorConnectEmailNotification($sponsorRequest)
    {
        $session = $this->get('adminsession');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account= $repository->findOneBy(array('id' => $session->userid));
        $communicationSpecialistsIds =   explode(",",$account->communicationSpecialist); 
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan= $repository->findOneBy(array('id' => $this->planid)); 
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $template= $repository->findOneBy(array('id' => $sponsorRequest->templateid));         
        $emails = array();
        foreach ($communicationSpecialistsIds as $communicationSpecialistsId)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:communicationSpecialist');
            $vwise= $repository->findOneBy(array('id' => $communicationSpecialistsId)); 
            $emails[] = $vwise->emailAddress;
        }        
        foreach ($emails as $email)
        {           
            $contents = "Plan: ".$plan->name." - ".$plan->planid."<br/><br/>";
            if ($sponsorRequest->requestStatus == "request")
            {
                $subject = "New Sponsor Request";
                $contents = $contents."Template Title: ".$template->name."<br/><br/>";
                $contents = $contents."To: ".$sponsorRequest->toEmailAddress."<br/><br/>";
                $contents = $contents."From Email: ".$sponsorRequest->fromEmailAddress."<br/><br/>";
                $contents = $contents."From Name: ".$sponsorRequest->fromFirstName." ".$sponsorRequest->fromLastName;
            }
            if ($sponsorRequest->requestStatus == "cancelled")
            {
                $subject = "Sponsor Request Cancelled";
                $contents = $contents."Sponsor Request has been cancelled";
            }
            $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom('info@smartplanenterprise.com')
            ->setTo($email)
            ->setBody(
            $contents,'text/html');
            $this->get('mailer')->send($message); 
        }
    }
    
    public function getSponsorRequest($id = null)
    {
        $this->init();
        $entityManager = $this->getDoctrine()->getManager();
        if ($id == null) {
            $query = "SELECT s FROM classesclassBundle:sponsorConnectRequests s WHERE s.planid = " . (int) $this->planid . " AND s.userid = " . (int) $this->userid . " ORDER BY s.id DESC";
        }
        else {
            $query = "SELECT s FROM classesclassBundle:sponsorConnectRequests s WHERE s.id = " . (int) $id;
        }
        $sponsorConnectRequestsArray = $entityManager->createQuery($query)->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $sponsorConnectRequests = null;

        if ($sponsorConnectRequestsArray != null)
        {
            $sponsorConnectRequests = new \stdClass();
            foreach ($sponsorConnectRequestsArray as $key => $value)
            {
                $sponsorConnectRequests->$key = $value;
            }
        }
        if ($sponsorConnectRequests != null)
        {    
            $sponsorConnectRequests->formattedDate = "";

            if ($sponsorConnectRequests->requestStatus == "request" || $sponsorConnectRequests->requestStatus == "in progress" || $sponsorConnectRequests->requestStatus == "done")
            {
                $sponsorConnectRequests->formattedDate = "Date Submitted:";
                $sponsorConnectRequests->datehtml = 'Send Date <span class ="glyphicon glyphicon-calendar"></span>';
            }
            if ($sponsorConnectRequests->requestStatus == "cancelled")
            {
                $sponsorConnectRequests->formattedDate = "Date Cancelled:";  
                $sponsorConnectRequests->datehtml = 'Schedule Date <span class ="glyphicon glyphicon-calendar"></span>';
            }
            if ($sponsorConnectRequests->requestStatus == "pending")
            {
                $sponsorConnectRequests->formattedDate = "";  
                $sponsorConnectRequests->datehtml = 'Schedule Date <span class ="glyphicon glyphicon-calendar"></span>';
                $sponsorConnectRequests->subject = "Important Information from your Provider About Your Retirement Plan";
            }
            else 
            {
                $sponsorConnectRequests->formattedDate = $sponsorConnectRequests->formattedDate." ".date("h:iA | m/d/Y",strtotime($sponsorConnectRequests->requestDate));
                $sponsorConnectRequests->formattedDate = str_replace("|","on",$sponsorConnectRequests->formattedDate);
            }
            if ($sponsorConnectRequests->scheduleDate != "0000-00-00 00:00:00")
            {
                $explodeScheduleDate = explode(" ",$sponsorConnectRequests->scheduleDate);
                $sponsorConnectRequests->date = $explodeScheduleDate[0];
                $sponsorConnectRequests->time = $explodeScheduleDate[1];
            }
            else
            {
                $sponsorConnectRequests->date = "";
                $sponsorConnectRequests->time = "";
            }
        }
        else
        {
            $sponsorConnectRequests = new \stdClass();
            $sponsorConnectRequests->formattedDate = "";
            $sponsorConnectRequests->date = "";
            $sponsorConnectRequests->time = "";
            $sponsorConnectRequests->requestStatus = "";
            $sponsorConnectRequests->templateid = -1;
            $sponsorConnectRequests->requestDate = "";
            $sponsorConnectRequests->planid = $this->planid;
            $sponsorConnectRequests->notifiyEmailAddress= "";
            $sponsorConnectRequests->datehtml = 'Schedule Date <span class ="glyphicon glyphicon-calendar"></span>';
            $sponsorConnectRequests->subject = "Important Information from your Provider About Your Retirement Plan";
        }
        if ($sponsorConnectRequests->notifiyEmailAddress != "")
        {
            $sponsorConnectRequests->notifiyEmailAddressChecked = "checked";
            $sponsorConnectRequests->readonly = "";
        }
        else
        {
            $sponsorConnectRequests->notifiyEmailAddressChecked= "";
            $sponsorConnectRequests->readonly = "readonly";
        }


        return $sponsorConnectRequests;
    }
    public function getSponsorRequestAction()
    {
        $this->init();         
        $sponsorConnectRequests = $this->getSponsorRequest();
        return new Response(json_encode($sponsorConnectRequests));
    }
    public function cancelSponsorRequestAction(Request $request)
    {
        if ($request->request->get("id","") == "")
        $sponsorRequest = $this->getSponsorRequest();
        
        else
        {
            $sponsorRequest = new \stdClass();
            $sponsorRequest->id = $request->request->get("id","");
        }
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:sponsorConnectRequests');
        $sponsorConnectRequests= $repository->findOneBy(array('id' => $sponsorRequest->id));
        $sponsorConnectRequests->requestStatus = 'cancelled';
        $sponsorConnectRequests->requestDate = new \DateTime("now");
        $return = 1;
        
        if ($sponsorConnectRequests->campaignid != 0)
        {
            $mailEngine = new mailEngine("mailgunEngine",$this);
            $params = array("id" => $sponsorConnectRequests->campaignid);
            $return = $mailEngine->cancelCampaign($params);
            if ($return == -1)
            $sponsorConnectRequests->campaignid = 0;
        }
        $em->flush();
        $this->sponsorConnectEmailNotification($sponsorConnectRequests);
        return new Response($return);
    }
    public function sponsorListAction()
    {
        $this->init();
        $session = $this->get('adminsession');
        $session->set("currentpage", "plansNavSponsorConnectList");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan= $repository->findOneBy(array('id' => $this->planid));
        $sponsorUploadBy = $this->sponsorUploadedBy();
        return $this->render('PlansPlansBundle:SponsorConnect:sponsorlist.html.twig',array("plan" => $plan,"sponsorUploadBy" => $sponsorUploadBy));
    }
    public function sponsorListExportToCsvAction()
    {
        $this->init();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:launchToolRecipients');
        $users= $repository->findBy(array('planid' => $this->planid));
        $fp  = tmpfile();
        $csv = array();
        $csv[] = explode(",","First Name,Last Name,Email Address,Enrolled");
        foreach ($users as $user)
        {
            $csv[] = array($user->firstName,$user->lastName,$user->email,$user->enrolled);
        }
        foreach ($csv as $row)
        {
            fputcsv($fp, $row);
        } 
        $response = new Response(file_get_contents(stream_get_meta_data($fp)['uri']));
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="UserList.csv"');
        return $response;
    }
    public function sponsorUploadedBy()
    {
        $this->init();
        $response ="";
        $sql = "SELECT AES_DECRYPT(firstName, UNHEX('".EncryptedType::KEY."')) as firstName,AES_DECRYPT(lastName, UNHEX('".EncryptedType::KEY."')) as lastName FROM launchUsers WHERE smartplanid =".$this->planid." ORDER BY id desc";
        $connection = $this->get('doctrine.dbal.default_connection');
        $user = $connection->executeQuery($sql)->fetch();
        if ($user != null )
        {
            if($user['firstName'] != null)
            $response = $response.$user['firstName'];
            if ($user['lastName'] != null)
            {
                if ($response != "")
                $response = $response." ";
                $response = $response.$user['lastName'];   
            }
        }
        return $response;
    }
    public function otherPropertiesAction(Request $request)
    {
        $this->sections($request->query->get("id"));
        $template = "PlansPlansBundle:SponsorConnect:viewproperties.html.twig";
        $return = array("fromSections" => $this->fromSections,"toSections" => $this->toSections);
        return $this->render($template,$return);
    }
    public function scheduleSponsorConnectAction(Request $request)
    {
        $template = "PortalPortalBundle:Default:schedulesponsorconnect.html.twig";
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:sponsorConnectRequests');
        $sponsorConnect= $repository->findOneBy(array('id' => $request->query->get("id")));
        $title =  $sponsorConnect->subject;
        $params = array();
        $placeholderTag = "Sponsor Request ".time();
        $params['templateid'] = $sponsorConnect->templateid;
        $params['planid'] = $sponsorConnect->planid;
        $params['mailEngine'] = "mailgunEngine";
        $params['userid'] = $sponsorConnect->userid;
        $params['title'] = $title;
        $params['subject'] = $title;
        $params['description'] = $placeholderTag;
        $params['fromName'] = $sponsorConnect->fromFirstName." ".$sponsorConnect->fromLastName;
        $params['fromEmail'] = $sponsorConnect->fromEmailAddress;
        $querystring = "";
        foreach($params as $key => $value)
        {
            if ($querystring != "")
            $querystring = $querystring."&";
            $querystring = $querystring.$key."=".$value;
        }
        $explodeDate = explode(" ",$sponsorConnect->scheduleDate->format("Y-m-d H:i:s"));
        $sponsorConnect->date = $explodeDate[0];
        $sponsorConnect->time = $explodeDate[1];
        $sponsorConnect->datehtml = "";
        $return = array("params" => $params,"querystring" => $querystring,"sponsorConnect" => $sponsorConnect);
        
        return $this->render($template,$return);        
    } 
    public function scheduleSponsorConnectSavedAction(Request $request)
    {
        $scheduleTime = new \DateTime($request->request->get("time"));
        if ($request->request->get("sendnow") == "true")
        $scheduleTime = new \DateTime('now');
        $currentTime =  new \DateTime('now');
        if ($currentTime > $scheduleTime)
        return new Response("bad date");
        $em = $this->getDoctrine()->getManager();       
        $connection = $this->get('doctrine.dbal.default_connection');
        $sponsorconnectid = $request->request->get("sponsorconnectid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:sponsorConnectRequests');
        $sponsorConnect= $repository->findOneBy(array('id' => $sponsorconnectid));
        $sponsorConnect->scheduleDate = $scheduleTime;
        $em->flush();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan= $repository->findOneBy(array('id' => $sponsorConnect->planid));        
        $result = true;
        while ($result != null)
        {
            $uniqid = uniqid();
            $sql = "SELECT id FROM launchUsers WHERE urlPass = '".$uniqid."'";
            $result = $connection->executeQuery($sql)->fetch(); 
        }
        $insertArray = array();
        $insertArray['urlPass'] =  $uniqid;
        $insertArray['firstName'] =  $sponsorConnect->toFirstName;
        $insertArray['lastName'] =  $sponsorConnect->toLastName;
        $insertArray['email'] =  $sponsorConnect->toEmailAddress;
        $insertArray['partnerid'] = $sponsorConnect->userid;
        $insertArray['smartplanid'] = $sponsorConnect->planid;
        $insertArray['planID'] = $plan->planid;
        $insertFields = "";
        $insertValues = "";
        foreach($insertArray as $key => $value)
        {
            if ($insertFields != "")
            {
                $insertFields = $insertFields.",";
                $insertValues = $insertValues.",";

            }
            $insertFields = $insertFields.$key;
            $insertValues = $insertValues."'".$value."'";
        }
                                
        $mailEngineName = "mailgunEngine";               
        $mailEngine = new mailEngine($mailEngineName,$this);
        $params =  $request->request->all();
        $params['userid'] = $plan->userid;
        $params['textBody'] ="text body";
        $params['html'] = file_get_contents($this->generateUrl('_manage_outreach_new_preview', array("id" => $request->request->get("templateid"),"planid" => $plan->id,"userid" => $plan->userid), true));
        $params['html'] = str_replace("<vwise_sponsor_connect_link/>",$this->container->getParameter('sponsorConnect')."?uc=".$uniqid."#landing",$params['html']);
        $campaignid = $mailEngine->createCampaign($params);                 
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:campaigns');
        $campaign= $repository->findOneBy(array('id' => $campaignid));               

        $counter = 0;
        $params['emails'] = array();
        $params['campaignid'] = $campaign->id;
        $params['emails'][$counter]['emailAddress'] = $sponsorConnect->toEmailAddress;
        $params['emails'][$counter]['firstName'] = $sponsorConnect->toFirstName;
        $params['emails'][$counter]['lastName'] = $sponsorConnect->toLastName;

        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        $mailEngine->createList($params);

        $params = array();
        if ($request->request->get("sendnow") != "true")
        $params['scheduledTime'] = $sponsorConnect->scheduleDate->format("Y-m-d H:i:s");
        $params["id"] = $campaign->id;
        $sponsorConnect->campaignid = $campaign->id;
        $em->flush();
        $date = $mailEngine->sendCampaign($params);
        

        if ($date == "0000-00-00 00:00:00")
        return new Response("Failed to schedule campaign");

        if ($request->request->get("sendnow") != "true")
        $sponsorConnect->requestStatus = "in progress";
        else
        $sponsorConnect->requestStatus = "done";
        $em->flush();


        $sql = "INSERT INTO launchUsers (".$insertFields.") VALUES (".$insertValues.")";
        $connection->executeQuery($sql); 

        $sql = "SELECT id FROM launchUsers WHERE urlPass='".$insertArray['urlPass']."'";
        $launchUser = $connection->executeQuery($sql)->fetch(); 
        if ($launchUser == null)
        return new Response("Failed to add launch user");

        return new Response("Success");       
        

    }
    public function cancelSponsorRequestCommSpecialistAction(Request $request)
    {
        
        return $this->render('PortalPortalBundle:Default:cancelsponsorconnect.html.twig',array("request" => $request->query->all()));
    }
    public function uploadEmailListAction(Request $request)
    {
        $this->init();
        $pattern = "/[^A-Za-z0-9_!@#$%^&*().-]/";
        $em = $this->getDoctrine()->getManager();       
        $csv = array_map('str_getcsv', file($request->files->get("csvFile")->getRealPath()));
        $mappingHash = array("firstName","lastName","email","enrolled");
        if (trim($csv[0][0]) == "First Name")
        unset($csv[0]);
        $response = new Response("");
        if (is_array($csv) && count($csv) > 0)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:launchToolRecipients');
            foreach ($csv as $row)
            {
                if (count(array_filter($row)) == 0) {
                    continue;
                }
                $rowHash = [];
                foreach($row as $key => $field)
                {
                    $rowHash[$mappingHash[$key]] = preg_replace($pattern, "", $field);
                }
                $recipient = $repository->findOneBy(['email' => $rowHash['email'], 'planid' => $this->planid]);
                if (is_null($recipient)) {
                    $recipient = new launchToolRecipients();
                }
                else {
                    $recipient->contactStatus = 'UPDATED';
                    $recipient->lastModified = time();
                }
                foreach ($rowHash as $key => $field) {
                    $recipient->$key = $field;
                }
                $recipient->partnerid = $this->userid;
                $recipient->planid = $this->planid;     
                $em->persist($recipient);
            }

            $em->flush();
            $this->uploadEmailNotification();
            return $response;
        }
        $response->setStatusCode(500);
        return $response;
    }
    
    private function uploadEmailNotification() {
        $session = $this->get('adminsession');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account= $repository->findOneBy(array('id' => $session->userid));
        $communicationSpecialistsIds = explode(",",$account->communicationSpecialist); 
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:communicationSpecialist');
        $communicationSpecialists = $repository->findBy(array('id' => $communicationSpecialistsIds));
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $currentuser = $repository->findOneBy(array('id' => $session->clientid));
        if ($currentuser == null) {
            $currentuser = $repository->findOneBy(array('id' => $session->adviserid));
        }
     
        foreach ($communicationSpecialists as $communicationSpecialist)
        {           
            $subject = "{$currentuser->firstname} {$currentuser->lastname} uploaded a client list in SmartConnect.";

            $contents = "<h3>The following account uploaded a client list in SmartConnect.</h3>";
            $contents .= "<b>First Name:</b> {$currentuser->firstname}<br/><br/>";
            $contents .= "<b>Last Name:</b> {$currentuser->lastname}<br/><br/>";
            $contents .= "<b>Partner ID:</b> {$session->userid}<br/><br/>";
            $contents .= "<b>Smartplan ID:</b> {$session->planid}<br/><br/>";

            $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom('info@smartplanenterprise.com')
            ->setTo($communicationSpecialist->emailAddress)
            ->setBody($contents,'text/html');
            $this->get('mailer')->send($message); 
        }
    }
    
}