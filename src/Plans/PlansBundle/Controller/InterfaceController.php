<?php

namespace Plans\PlansBundle\Controller;

use classes\classBundle\Entity\abstractclasses\AppForkContent;
use classes\classBundle\Entity\PlanAppForkContent;
use classes\classBundle\Services\Entity\PlanAppForkContentService;
use classes\classBundle\Services\Style\ForkContentService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use Permissions\RolesBundle\Classes\rolesPermissions;
class InterfaceController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansInterface");	
        $session->set("section","Plans");
        $session->set("currentpage","PlansInterface");
        $planid = $session->planid;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $interface = $repository->findOneBy(array('id' => $planid));
        $colors = $this->get("PlanAppColorsService")->findOneBy(array('planid' => $planid));
        $hexList = $this->get("StyleService")->hexColorValues($colors);
    	return $this->render('Shared/Interface/index.html.twig',array(
    	    'id' => $planid,
            "colors" => $this->get("StyleService")->getColors($interface),
            "layouts" => $this->get("StyleService")->getLayouts($interface),
            "writeable" => $writeable,
            'savePath' => '_plans_interface_save',
            'hexList' => $hexList,
            'plan' => $interface
        ));
    }
    public function saveAction(Request $request)
    {
        $this->get("StyleService")->saveStyle($request->request->all(),$this->get("PlansService"));
        return new Response("");
    }
    public function saveHexAction(Request $request)
    {
        $params = $request->request->all();
        $params['userid'] = $this->get("session")->get("userid");
        $params['planid'] = $this->get("session")->get("planid");
        $this->get("StyleService")->saveHex($this->get("PlanAppColorsService"),$params,["planid" => $this->get("session")->get("planid")]);
    }
}
