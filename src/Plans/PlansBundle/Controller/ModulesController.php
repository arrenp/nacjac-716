<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;

class ModulesController extends Controller
{
    protected function getOptions($field, $options)
    {
        $array = array();
        foreach ($options as $label => $value)
        {
            $current = new \stdClass();
            $current->description = $label;
            $current->value = $value;
            $array[] = $current;
        }
        $this->calculateSelected($array, $field);
        return $array;
    }
    public function indexAction()
    {
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansModules");
        $session = $this->get('adminsession');
        $userid = $session->userid;
        $planid = $session->planid;
        $session->set("section", "Plans");
        $session->set("currentpage", "PlansModules");
        $this->general = $this->get('GeneralMethods');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $this->currentaccount = $repository->findOneBy(array('id' => $session->userid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansModules');
        $this->currentplan = $repository->findOneBy(array('planid' => $planid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $planid)); 
        $this->currentplan = (object) array_merge((array) $this->currentplan, (array) $plan);
        $this->connection = $this->get('doctrine.dbal.default_connection');       
        $this->currentplan->socialSecurityMultiplier = $this->currentplan->socialSecurityMultiplier * 100.0;
        $sync = 0;
        if (count($this->get("PlanService")->syncModulesPlans()) > 0)
        {
            $sync = 1;
        }
        return $this->render('PlansPlansBundle:Modules:index.html.twig', array("userid" => $userid, "planid" => $planid, "writeable" => $writeable, "section1" => $this->section1(), "section2" => $this->section2(), "section3" => $this->section3(),"section4" => $this->section4(), "plan" => $this->currentplan,"session" => $session,"sync" => $sync));
    }
    public function addsection(&$section, $data, $description, $name)
    {
        $i = count($section);
        $section[$i] = new \stdClass();
        $section[$i]->data = $data;
        $section[$i]->description = $description;
        $section[$i]->name = $name;
        $section[$i]->showoption = false;
        if (count($data) > 1)
        {
            $section[$i]->showoption = true;
            foreach ($data as $module)
            {
                if ($module->value == $this->currentplan->$name) $section[$i]->showoption = false;
            }
        }
    }
    public function riskBasedQuestionnaire()
    {
        $riskBasedQuestionnaire = array();
        $this->general->addSTDClassToArray($riskBasedQuestionnaire, 2);
        $riskBasedQuestionnaire[0]->description = "Off";
        $riskBasedQuestionnaire[1]->description = "On";
        $riskBasedQuestionnaire[0]->value = "0";
        $riskBasedQuestionnaire[1]->value = "1";
        $this->calculateSelected($riskBasedQuestionnaire, "riskBasedQuestionnaire");
        return $riskBasedQuestionnaire;
    }
    public function languageDropdownArray()
    {
        $languageDropdownArray = array();
        $this->general->addSTDClassToArray($languageDropdownArray, 2);

        $languageDropdownArray[0]->description = "Enabled";
        $languageDropdownArray[1]->description = "Disabled";

        $languageDropdownArray[0]->value = 1;
        $languageDropdownArray[1]->value = 0; 
        $this->calculateSelected($languageDropdownArray,"languageDropdown");
        return $languageDropdownArray;
    }
    public function language()
    {
        $accountLanguages = explode("|", $this->currentaccount->languagesAvailable);
        $accountLanguagesOriginal = array();
        foreach ($accountLanguages as $lang)
        {
            $lang = explode("_", $lang);
            $accountLanguagesOriginal[] = trim(end($lang));
        }
        $languages = $this->connection->fetchAll('SELECT * FROM languages WHERE name != "" group by name');
        $language = array();
        $path = "../src/Spe/AppBundle/Resources/translations/";
        $translator = $this->get("translator");
        foreach ($languages as $lang)
        {
            if ($translator->hasLocale($lang["name"]) && in_array($lang["name"], $accountLanguagesOriginal))
            {
                $language[] = new \stdClass();
                $language[count($language) - 1]->description = $lang["description"];
                $language[count($language) - 1]->value = $lang["name"];
            }
        }        
        $this->calculateSelected($language, "language");
        return $language;
    }
    public function minimumContribution()
    {
        $minimumContribution = $this->_minimumContribution();
        $this->calculateSelected($minimumContribution, "minimumContribution");
        return $minimumContribution;
    }
    public function minimumContributionRoth()
    {
        $minimumContribution = $this->_minimumContribution();
        $this->calculateSelected($minimumContribution, "minimumContributionRoth");
        return $minimumContribution;
    }
    public function minimumContributionPostTax()
    {
        $minimumContribution = $this->_minimumContribution();
        $this->calculateSelected($minimumContribution, "minimumContributionPostTax");
        return $minimumContribution;
    }
    public function maximumContribution()
    {
        $maximumContribution = $this->_maximumContribution();
        $this->calculateSelected($maximumContribution, "maximumContribution");
        return $maximumContribution;
    }
    public function maximumContributionRoth()
    {
        $maximumContribution = $this->_maximumContribution();
        $this->calculateSelected($maximumContribution, "maximumContributionRoth");
        return $maximumContribution;
    }
    public function maximumContributionPostTax()
    {
        $maximumContribution = $this->_maximumContribution();
        $this->calculateSelected($maximumContribution, "maximumContributionPostTax");
        return $maximumContribution;
    }
    public function _minimumContribution()
    {
        $minimumContribution = array();
        for ($i = 0; $i <= 5; $i++)
        {
            $minimumContribution[$i] = new \stdClass();
            $minimumContribution[$i]->description = $i . "%";
            $minimumContribution[$i]->value = $i;
        }
        return $minimumContribution;
    }
    public function _maximumContribution()
    {
        $maximumContribution = array();
        for ($i = 100; $i >=0; $i -= 5)
        {
            $percentage = new \stdClass();
            $percentage->description = $i . "%";
            $percentage->value = $i;
            $maximumContribution[] = $percentage;
        }
        return $maximumContribution;
    }
    public function defaultRetirementAge()
    {
        $retirementAge = $this->_defaultRetirementAge();
        $this->calculateSelected($retirementAge, "defaultRetirementAge");
        return $retirementAge;
    }
    public function _defaultRetirementAge()
    {
        $retirementAge = array();
        for ($i = 0; $i <= 100; $i++)
        {
            $retirementAge[$i] = new \stdClass();
            $retirementAge[$i]->description = $i;
            $retirementAge[$i]->value = $i;
        }
        return $retirementAge;
    }
    public function section1()
    {
        $session = $this->get('adminsession');
        $planid = $session->planid;
        $section1 = array();
        if (!$this->general->isOmni($this->currentaccount) && !$this->general->isGuardian($this->currentaccount) && !($this->currentaccount->connectionType == "Relius")) 
        {
            $this->addsection($section1, $this->deferralType(), "Deferral Type", "deferralType");
        }
        if($this->currentaccount->connectionType == "Educate")
        {
            $this->addsection($section1, $this->postTax(), "Post Tax", "postTax");
        }
        if ($this->currentaccount->connectionType != "Relius" && !in_array($this->currentaccount->connectionType,array("Omniamrts","Omniinspty","Omniinspty2","Omnimoa")) && !$this->general->isSrt($this->currentaccount))
        {    
            $this->addsection($section1, $this->beneficiaries(), "Beneficiaries", "beneficiaries");
            $this->addsection($section1, $this->beneficiariesAutoAdd(), "Beneficiary Auto Add", "beneficiaryAutoAdd");
        }
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $planid));
        if($plan->appVersion == 1) {
            $this->addsection($section1, $this->enrollment(), "Enrollment", "enrollment");
        }
        $this->addsection($section1, $this->vesting(), "Vesting", "vesting");
        if ($this->currentaccount->connectionType == "Relius" || $this->currentaccount->connectionType == "RetRev" || $this->currentaccount->connectionType == "Educate") 
        {
            $this->addsection($section1, $this->deferralFormat(), "Deferral Format", "deferralFormat");
        }
        if (!$this->general->isIntegrated($this->currentaccount))
        {
            $this->addsection($section1, $this->minimumContribution(), "Traditional Minimum Contribution", "minimumContribution");
            $this->addsection($section1, $this->maximumContribution(), "Traditional Maximum Contribution", "maximumContribution");
            $this->addsection($section1, $this->minimumContributionRoth(), "Roth Minimum Contribution", "minimumContributionRoth");
            $this->addsection($section1, $this->maximumContributionRoth(), "Roth Maximum Contribution", "maximumContributionRoth");
            $this->addsection($section1, $this->minimumContributionPostTax(), "Post Tax Minimum Contribution", "minimumContributionPostTax");
            $this->addsection($section1, $this->maximumContributionPostTax(), "Post Tax Maximum Contribution", "maximumContributionPostTax");
        }
        if ($this->currentaccount->language) 
        {
            $this->addsection($section1, $this->language(), "Language", "language");
            $this->addsection($section1,$this->languageDropdownArray(),"User Language Selector","languageDropdown");        
        }
        if ($this->currentaccount->retirementNeedsVersionOnOff) 
        {
            $this->addsection($section1, $this->retirementNeedsVersion(), "Retirement Needs Version", "retirementNeedsVersion");
        }
        if ($this->currentplan->retirementNeedsVersion == "by age")
        {
            $this->addnumbersection($section1, $this->currentplan->lifeExpectancy, "Life Expectancy", "lifeExpectancy", array("max" => 100));
            $this->addnumbersection($section1, $this->currentplan->retirementAge, "Retirement Age", "retirementAge", array("max" => 100));
        }
        if ($this->currentaccount->flexPlanAvailable)
        {
            $this->addsection($section1,$this->flexPlan(),"FlexPath","flexPlan");         
            $this->addnumbersection($section1, $this->currentplan->inflation, "FlexPath - Inflation", "inflation");
            $this->addnumbersection($section1, $this->currentplan->return, "FlexPath - Return", "return");    
        }
        if ($this->currentplan->type =="smartenroll" || $this->currentaccount->connectionType == "Educate")
        {
            $this->addsection($section1, $this->ECOMMModal(), "ECOMM Modal", "ECOMMModal");
            $this->addsection($section1, $this->ECOMMModalChecked(), "ECOMM Modal Checked", "ECOMMModalChecked"); 
        }
        if($this->currentaccount->connectionType == "Educate")
        {
            $this->addsection($section1, $this->combinedContributionAmount(), "Combined Contribution Amount", "combinedContributionAmount");
        }
        if($this->currentaccount->rolloverWorkflowEnabled || $this->currentaccount->connectionType=="Omniamrts" || $this->currentaccount->connectionType=="Omnimoa")
        {
            $this->addsection($section1, $this->rolloverWorkflow(), "Rollover Workflow", "rolloverWorkflow");
        }
        if ($this->currentaccount->trustedContactEnabled)
        {
            $this->addsection($section1, $this->trustedContactOn(), "Trusted Contact", "trustedContactOn");
        }
        $this->addsection($section1, $this->defaultRetirementAge(), "Default Retirement Age", "defaultRetirementAge");
        if ($this->currentplan->appVersion === 2)
        {
            $this->addsection($section1, $this->getOptions('autoPlayAudio', ['On' => 1, 'Off' => 0]), "Auto Play Audio", "autoPlayAudio");
        }
        return $section1;
    }
    public function section2()
    {
        $investmentSelectorRiskBasedPortfolio = $investmentSelectorTargetMaturityFund = $investmentSelectorCustomChoice = array();
        $this->addCheckboxType($investmentSelectorRiskBasedPortfolio, "investmentSelectorRiskBasedPortfolio", "Risk Based Portfolio");
        $this->addCheckboxType($investmentSelectorTargetMaturityFund, "investmentSelectorTargetMaturityFund", "Target Maturity Fund");
        $this->addCheckboxType($investmentSelectorCustomChoice, "investmentSelectorCustomChoice", "Custom Choice");
        $section2 = array();
        if ($this->currentplan->riskBasedQuestionnaire == 0)
        {
            $investmentSelectorRiskBasedPortfolio->disabled = true;
        }
        $this->addsection($section2, $investmentSelectorRiskBasedPortfolio, "Risk Based Investment Strategy", "investmentSelectorRiskBasedPortfolio");
        $this->addsection($section2, $investmentSelectorTargetMaturityFund, "Target Maturity Date", "investmentSelectorTargetMaturityFund");
        $this->addsection($section2, $investmentSelectorCustomChoice, "Custom Choice", "investmentSelectorCustomChoice");   
        return $section2;
    }
    public function section3()
    {
        $section3 = array();
        $catchup = $loans = $matchingContributions = $compounding = array();
        $this->addCheckboxType($catchup, "catchup", "");
        $this->addCheckboxType($loans, "loans", "");
        $this->addCheckboxType($matchingContributions, "matchingContributions", "");
        $this->addCheckboxType($compounding, "compounding", "");
        if($this->currentaccount->connectionType != "Relius")
        {
            $this->addsection($section3, $catchup, "Catchup", "catchup");
        }
        if (!$this->general->isGuardian($this->currentaccount) && $this->currentaccount->connectionType != "Relius")
        {
        $this->addsection($section3, $loans, "Loans", "loans");
        }
        $this->addsection($section3, $matchingContributions, "Matching Contributions", "matchingContributions");
        if($this->currentaccount->connectionType == "Educate")
        {
            $this->addsection($section3, $compounding, "Compounding", "compounding");
        }
        return $section3;
    }
    public function section4()
    {
        $section4 = array();
        $this->addsection($section4, $this->riskBasedQuestionnaire(), "Risk Based Questionnaire", "riskBasedQuestionnaire");
        return $section4;
    }
    public function combinedContributionAmount()
    {
        $combinedContributionAmount = array();
        for ($i = 0; $i <= 100; $i++)
        {
            $combinedContributionAmount[$i] = new \stdClass();
            $combinedContributionAmount[$i]->description = $i;
            $combinedContributionAmount[$i]->value = $i;
        }
        $this->calculateSelected($combinedContributionAmount, "combinedContributionAmount");
        return $combinedContributionAmount;
    }
    public function rolloverWorkflow()
    {
        $rolloverWorkflow = array();
        $this->general->addSTDClassToArray($rolloverWorkflow, 2);
        $rolloverWorkflow[0]->description = "No";
        $rolloverWorkflow[1]->description = "Yes";

        $rolloverWorkflow[0]->value = 0;
        $rolloverWorkflow[1]->value = 1;

        $this->calculateSelected($rolloverWorkflow, "rolloverWorkflow");
        return $rolloverWorkflow;
    }
    public function trustedContactOn() {
        $trustedContactOn = array();
        $this->general->addSTDClassToArray($trustedContactOn, 2);
        $trustedContactOn[0]->description = "No";
        $trustedContactOn[1]->description = "Yes";
        $trustedContactOn[0]->value = 0;
        $trustedContactOn[1]->value = 1;
        $this->calculateSelected($trustedContactOn, "trustedContactOn");
        return $trustedContactOn;
    }
    public function ECOMMModal()
    {
        $ECOMMModal = array();
        $this->general->addSTDClassToArray($ECOMMModal, 2);
        $ECOMMModal[0]->description = "Off";
        $ECOMMModal[1]->description = "On";
        $ECOMMModal[0]->value = "0";
        $ECOMMModal[1]->value = "1";
        $this->calculateSelected($ECOMMModal, "ECOMMModal");     
        return $ECOMMModal;
    }
    public function ECOMMModalChecked()
    {
        $ECOMMModalChecked = array();
        $this->general->addSTDClassToArray($ECOMMModalChecked, 2);
        $ECOMMModalChecked[0]->description = "Off";
        $ECOMMModalChecked[1]->description = "On";
        $ECOMMModalChecked[0]->value = "0";
        $ECOMMModalChecked[1]->value = "1";
        $this->calculateSelected($ECOMMModalChecked, "ECOMMModalChecked"); 
        return $ECOMMModalChecked;
    }
    public function flexPlan()
    {
        $flexPlanArray = array();
        $this->general->addSTDClassToArray($flexPlanArray, 2);

        $flexPlanArray[0]->description = "Enabled";
        $flexPlanArray[1]->description = "Disabled";

        $flexPlanArray[0]->value = 1;
        $flexPlanArray[1]->value = 0; 

        $this->calculateSelected($flexPlanArray,"flexPlan");
        return $flexPlanArray;
    }
    public function retirementNeedsVersion()
    {
        $retirementNeedsVersion = array();
        $this->general->addSTDClassToArray($retirementNeedsVersion, 2);
        $retirementNeedsVersion[0]->description = "Standard";
        $retirementNeedsVersion[1]->description = "By Age";
        $retirementNeedsVersion[0]->value = "standard";
        $retirementNeedsVersion[1]->value = "by age";
        $this->calculateSelected($retirementNeedsVersion, "retirementNeedsVersion");
        return $retirementNeedsVersion;
    }
    public function deferralFormat()
    {
        $deferralFormat = array();
        $this->general->addSTDClassToArray($deferralFormat, 3);
        $deferralFormat[0]->description = "Percent / Dollar - (user option)";
        $deferralFormat[1]->description = "Percent Only";
        $deferralFormat[2]->description = "Dollar Only";

        $deferralFormat[0]->value = "option";
        $deferralFormat[1]->value = "percent";
        $deferralFormat[2]->value = "dollar";

        $this->calculateSelected($deferralFormat, "deferralFormat");
        return $deferralFormat;
    }
    public function vesting()
    {
        $vesting = array();
        $this->general->addSTDClassToArray($vesting, 5);

        $vesting[0]->description = "None";
        $vesting[1]->description = "Generic";
        $vesting[2]->description = "3-Year";
        $vesting[3]->description = "5-Year";
        $vesting[4]->description = "6-Year";

        $vesting[0]->value = "none";
        $vesting[1]->value = "vest_gen";
        $vesting[2]->value = "vest_3yr";
        $vesting[3]->value = "vest_5yr";
        $vesting[4]->value = "vest_6yr";

        $this->calculateSelected($vesting, "vesting");
        return $vesting;
    }
    public function enrollment()
    {
        $enrollment = array();
        $this->general->addSTDClassToArray($enrollment, 2);
        $enrollment[0]->description = "Employee Enroll";
        $enrollment[1]->description = "Automatic Enroll";

        $enrollment[0]->value = "emp_enroll";
        $enrollment[1]->value = "auto_enroll";

        $this->calculateSelected($enrollment, "enrollment");
        return $enrollment;
    }
    public function beneficiaries()
    {
        $beneficiaries = array();
        $this->general->addSTDClassToArray($beneficiaries, 3);
        $beneficiaries[0]->description = "Mandatory";
        $beneficiaries[1]->description = "Optional";
        $beneficiaries[2]->description = "Off";

        $beneficiaries[0]->value = "on_mandatory";
        $beneficiaries[1]->value = "on_optional";
        $beneficiaries[2]->value = "off";

        $this->calculateSelected($beneficiaries, "beneficiaries");
        return $beneficiaries;
    }
    public function beneficiariesAutoAdd()
    {        
        $beneficiariesAutoAdd = array();
        $this->general->addSTDClassToArray($beneficiariesAutoAdd, 2);
        $beneficiariesAutoAdd[0]->description = "Off";
        $beneficiariesAutoAdd[1]->description = "On";

        $beneficiariesAutoAdd[0]->value = 0;
        $beneficiariesAutoAdd[1]->value = 1;

        $this->calculateSelected($beneficiariesAutoAdd, "beneficiaryAutoAdd");
        return $beneficiariesAutoAdd;        
    }
    public function postTax()
    {
        $postTax = array();
        $this->general->addSTDClassToArray($postTax, 2);
        $postTax[0]->description = "Yes";
        $postTax[1]->description = "No";

        $postTax[0]->value = "1";
        $postTax[1]->value = "0";

        $this->calculateSelected($postTax, "postTax");
        return $postTax;
    }
    public function deferralType()
    {
        $deferralType = array();
        $this->general->addSTDClassToArray($deferralType, 3);
        $deferralType[0]->description = "Traditional";
        $deferralType[1]->description = "Roth";
        $deferralType[2]->description = "Traditional+Roth";

        $deferralType[0]->value = "traditional";
        $deferralType[1]->value = "roth";
        $deferralType[2]->value = "traditional+roth";

        $this->calculateSelected($deferralType, "deferralType");
        return $deferralType;
    }


    public function addnumbersection(&$section, $data, $description, $name, $options = null)
    {
        $i = count($section);
        $section[$i] = new \stdClass();
        $section[$i]->data = $data;
        $section[$i]->description = $description;
        $section[$i]->name = $name;
        $section[$i]->type = "number";
        if (isset($options['max'])) $section[$i]->max = $options['max'];
    }

    public function calculateSelected(&$data, $field, $selectType = "selected")
    {
        foreach ($data as $module)
        {
            if ($module->value == $this->currentplan->$field) $module->selected = $selectType;
            else $module->selected = "";
        }
    }

    public function calculateChecked($number)
    {
        if ($number == 1) return "checked";
        else return "";
    }

    public function calculateDisplay($number)
    {
        if ($number == 1) return "block";
        else return "none";
    }

    public function addCheckboxType(&$data, $field, $savevalue)
    {
        $data = new \stdClass();
        if (is_string($this->currentplan->$field))
        {
            if ($this->currentplan->$field == "yes") $this->currentplan->$field = 1;
            else $this->currentplan->$field = 0;
        }
        $data->checked = $this->calculateChecked($this->currentplan->$field);
        $data->display = $this->calculateDisplay($this->currentplan->$field);
        $data->savevalue = $savevalue;
    }

    public function updateAction(Request $request)//generic update method
    {
        $response = $this->update($request);
        $syncstring = $request->request->get('syncstring', '');
        if ($syncstring != "")
        $syncArray = explode(",",$syncstring);
        if (isset($syncArray))
        foreach ($syncArray as $planid)
        $response = $this->update($request,$planid);
        return $response;
    }
    public function update($request,$planid = null)
    {
        try
        {
            $arr = $request->request->all();
            $session = $this->get('adminsession');
            if ($planid == null)
            $planid = $session->planid;

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('classesclassBundle:plansModules');
            $modules = $repository->findOneBy(array("planid" => $planid));
            
            $repository = $em->getRepository('classesclassBundle:plans');
            $plans = $repository->findOneBy(array("id" => $planid));               
            $plansSections = array("smartEnroll");
            $appModuleRepo = $em->getRepository('classesclassBundle:AppModule');
            $appModules = $appModuleRepo->findAll();
            $appModuleArray = array();
            foreach ($appModules as $module) {
                $appModuleArray[] = $module->module;
            }
            $repository = $em->getRepository('classesclassBundle:AppVideo');
            $appVideosArray = array();
            $appVideos = $repository->findBy(array('pathId' => $plans->standardPathId));
            foreach ($appVideos as $video) {
                if (!empty($video->module))
                    $appVideosArray[$video->module][] = $video->moduleSelection;
            }
            $appModuleSelectionRepo = $em->getRepository('classesclassBundle:AppModuleSelection');
            foreach ($arr as $key => $value)
            {
                if ($key != "table" && $key != "findby")
                {
                    if (in_array($key,$plansSections))
                    $plans->$key = $value;
                    else if (!in_array($key,array("userid","planid"))) {
                        if ($plans->appVersion === 2 && in_array($key, $appModuleArray) && !in_array($value, $appVideosArray[$key])) {
                            if(!empty($appModuleSelectionRepo->findOneBy(array('moduleid' => $appModuleRepo->findOneBy(array('module' => $key))->id, 'selection' => $value ))))
                                return new Response($key);

                        }
                        $modules->$key = $value;
                    }
                }
            }
            $booleanSections = array("compounding");
            foreach ($booleanSections as $section)
            {
                if (isset($arr[$section]))
                {
                    $modules->$section =  $arr[$section] == "yes";
                }
            }
            $modules->socialSecurityMultiplier = $modules->socialSecurityMultiplier/100.0;
            $em->flush();

            if (!$request->request->get("investmentSelectorRiskBasedPortfolio"))
            {
                $repository = $em->getRepository('classesclassBundle:plansPortfolios');
                $portfolios = $repository->findBy(array("planid" => $planid));
                $repository = $em->getRepository('classesclassBundle:plansRiskBasedFunds');
                $riskbasedfunds = $repository->findBy(array("planid" => $planid));

                foreach ($portfolios as $portfolio)
                {
                    $portfolio->profile = "ALL";
                    $portfolio->minscore = "";
                    $portfolio->maxscore = "";
                }
                foreach ($riskbasedfunds as $riskbasedfund)
                {
                    $riskbasedfund->profile = "ALL";
                    $riskbasedfund->minscore = "";
                    $riskbasedfund->maxscore = "";
                }
                $em->flush();
            }
            
            return new Response("saved");
        }
        catch (Exception $e)
        {
            return new Response("Error occured!: " . $e->getMessage());
        }        
        
    }   
    public function syncModalAction()
    {
        $plans = $this->get("PlanService")->syncModulesPlans();
        return $this->render('PlansPlansBundle:Modules:syncmodal.html.twig',array("plans" => $plans));
    }
}
