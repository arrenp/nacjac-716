<?php

namespace Plans\PlansBundle\Controller;

use Sessions\AdminBundle\Classes\adminsession;

use classes\classBundle\Entity\PlansInvestmentsScore;
use classes\classBundle\Entity\PlansInvestmentsYearsToRetirement;
use classes\classBundle\Entity\PlansInvestmentsConfiguration;
use classes\classBundle\Entity\PlansInvestmentsConfigurationColors;
use classes\classBundle\Entity\PlansInvestmentsLinkYearsToRetirementAndScore;
use classes\classBundle\Entity\PlansInvestmentsConfigurationFundsGroupValuesColors;
use classes\classBundle\Entity\PlansInvestmentsConfigurationDefaultFundsGroupValuesColors;
use classes\classBundle\Entity\plansPortfolios;

use Shared\InvestorProfileBundle\Controller\InvestorProfileController as Controller;
class InvestorProfileController extends Controller
{

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $session = $this->get('adminsession');
        $this->tablename = "Plans";
        $this->userid = $session->userid;
        $this->planid = $session->planid;
        $this->findBy = array('planid' => $session->planid);
        $session->set("section","Plans");
        $session->set("currentpage","PlansInvestorProfile");
        $this->twigParameters = array(
            'path' => 'plans',
            'controller' => 'PlansPlansBundle:InvestorProfile:');

    }

    public function getWhereClause()
    {
        return 'p.planid = :findby';
    }

    public function getParameterClause()
    {
        return $this->planid;
    }

    public function getInvestmentsScoreEntity()
    {
        return new PlansInvestmentsScore();
    }

    public function getInvestmentsConfigurationEntity()
    {
        return new PlansInvestmentsConfiguration();
    }

    public function getInvestmentsYearsToRetirementEntity()
    {
        return new PlansInvestmentsYearsToRetirement();
    }

    public function getInvestmentsConfigurationColorsEntity()
    {
        return new PlansInvestmentsConfigurationColors();
    }

    public function getInvestmentsLinkYearsToRetirementAndScoreEntity()
    {
        return new PlansInvestmentsLinkYearsToRetirementAndScore();
    }

    public function getInvestmentsConfigurationFundsGroupValuesColors()
    {
        return new PlansInvestmentsConfigurationFundsGroupValuesColors();
    }

    public function getInvestmentsConfigurationDefaultFundsGroupValuesColors()
    {
        return new PlansInvestmentsConfigurationDefaultFundsGroupValuesColors();
    }

    public function getinvestmentsScoreAssignColorActionRepository()
    {
        return 'classesclassBundle:plans';
    }

    public function getinvestmentsScoreAssignColorActionFindBy() {
        return array('id' => $this->planid);
    }

    public function getPortfolioEntity()
    {
        return new plansPortfolios();
    }
}
