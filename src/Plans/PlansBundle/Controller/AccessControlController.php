<?php

namespace Plans\PlansBundle\Controller;

use classes\classBundle\Entity\HomePlanAccess;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccessControlController extends Controller {
	
	public function indexAction() {
		$session = $this->get('adminsession');
        $session->set("section","Plans");
        $session->set("currentpage","PlansAccessControl");
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $session->planid)); 
		return $this->render('PlansPlansBundle:AccessControl:index.html.twig', array('session'=>$session, 'plan' => $plan));
	}
	
	public function saveUserAction(Request $request) {
		$repository = $this->getDoctrine()->getRepository("classesclassBundle:HomePlanAccess");
		$assoc = $repository->findOneBy(array('planid' => $request->get("planid"), 'accountsUsersId'=>$request->get("id")));
		$em = $this->getDoctrine()->getManager();
		
		if ($request->get("checked")) {
			if ($assoc === null) {
				$assoc = new HomePlanAccess;
				$assoc->planid = $request->get("planid");
				$assoc->accountsUsersId = $request->get("id");
				$em->persist($assoc);
			}
		}
		else {
			if ($assoc !== null) {
				$em->remove($assoc);
			}
		}
		$em->flush();
		return new Response("success");
	}
	
	public function checkUserExistsAction($planid, $accountsUsersId) {
		$repository = $this->getDoctrine()->getRepository("classesclassBundle:HomePlanAccess");
		$assoc = $repository->findOneBy(array('planid' => $planid, 'accountsUsersId'=>$accountsUsersId));
		if ($assoc === null) {
			return new Response("");
		}
		return new Response("checked");
	}
	
	public function setHomePlanAction(Request $request) {
		$repository = $this->getDoctrine()->getRepository("classesclassBundle:plans");
		$plan = $repository->find($request->get("planid"));
		$plan->isHomePlan = $request->get("isHomePlan", 0);
		$this->getDoctrine()->getManager()->flush();
		return new Response("success");
	}
	
}

