<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shared\ContactBundle\Classes\contact;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
class DeployController extends Controller
{

    public function indexAction()
    {
    	$session = $this->get('adminsession');
    	$general = $this->get('GeneralMethods');
    	$session->set("section","Plans");
    	$session->set("currentpage","PlansDeploy");
    	$repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
		$plan = $repository->findOneBy(array('id' => $session->planid));
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
		$member = $repository->findOneBy(array('id' => $session->userid));

		$deploymentUrl = $session->appurl."?id=".$plan->planid."&partner_id=".$member->partnerid;
        if ($plan->type != "smartplan") {
            $deploymentUrl = $deploymentUrl . "&type=" . $plan->type;
        }
		$showDeploymentUrl = 1;
		if ($general->isIntegrated($member)) {
            $showDeploymentUrl = 0;
        }

        $irioUrl = $session->appurl . "widgets/?sid=test" . "&planid=" . "LIN" . "&partner_id=" . "L14_SRT_DEV"
            . "&type=irio" . "&testing=srt2-widget-test";

        $powerviewUrl = $session->appurl . "widgets/?sid=test" . "&planid=" . "LIN" . "&partner_id=" . "L14_SRT_DEV"
            . "&type=powerview" . "&testing=srt2-widget-test";

        $vwisemember = 0;
        $irioAllowed = 0;
        $powerviewAllowed = 0;
        if ($general->isVwiseMember($session)) {
            $vwisemember = 1;
            if ($general->isSrt($member)) {
                $irioAllowed = $plan->irioAllowed;
                $powerviewAllowed = $plan->powerviewAllowed;
            }
        }

		$permissions= $this->get('rolesPermissions');
		$writeable = $permissions->writeable("PlansDeploy");


		return $this->render('PlansPlansBundle:Deploy:index.html.twig', array('plan' => $plan,'member' => $member, "deploymentUrl" => $deploymentUrl,"showDeploymentUrl" => $showDeploymentUrl,"writeable" => $writeable, "irioUrl" => $irioUrl, "powerviewUrl" => $powerviewUrl, "powerviewAllowed" => $powerviewAllowed, "irioAllowed" => $irioAllowed, "vwisemember" => $vwisemember));
    }

    public function toggleAction(Request $request) {
        $session = $this->get('adminsession');
        $em = $this->getDoctrine()->getManager();
        $state = $request->request->get('state');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $session->planid));
        if ($state == 'true') {
            $plan->profileNotificationShowEmail = 1;
        } else {
            $plan->profileNotificationShowEmail = 0;
        }
        $em->persist($plan);
        $em->flush();

        return new Response('saved');
    }

}
