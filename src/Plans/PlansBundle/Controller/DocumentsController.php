<?php
namespace Plans\PlansBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Shared\DocumentsBundle\Classes\documents;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
class DocumentsController extends Controller
{
	public $documents;
	public $templatepath;
    public function indexAction()
    {
	    $this->init();
        $permissions = $this->get('rolesPermissions');
    	$writeable = $permissions->writeable("PlansDocuments");
	    return $this->render($this->templatepath.'index.html.twig', array("documents" => $this->documents,"writeable" => $writeable));
    }
    public function editDocumentAction($id)
    {
        $this->init();
        $document = $this->documents->getDocument($id);
        return $this->render($this->templatepath."Search/editdocument.html.twig",array("document" => $document));
    }

    public function deleteDocumentAction($id)
    {
        $this->init();
        $document = $this->documents->getDocument($id);
        return $this->render($this->templatepath."Search/deletedocument.html.twig",array("document" => $document));
    }  
    public function init()
    {
        $session = $this->get('adminsession');
        $session->set("section","Plans");
        $session->set("currentpage","PlansDocuments");
        $userid = $session->userid;
        $planid = $session->planid;
        $search = $this->get('generalfunctions');
        $permissions = $this->get('rolesPermissions');
    	$writeable = $permissions->writeable("PlansDocuments");
        $this->documents = new documents($this->getDoctrine(),"plans",$search,$userid,$writeable,$planid);
        $this->documents->indexAction();
        $this->templatepath = "PlansPlansBundle:Documents:";
    }
}