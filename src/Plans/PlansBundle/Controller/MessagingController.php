<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Shared\MessagingBundle\Classes\Messaging;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Entity\PlansMessages;
use classes\classBundle\Entity\DefaultMessages;
class MessagingController extends Controller
{
    public function indexAction()
    {
        $messaging = $this->get("Messaging");
        $messages = $messaging->messages("plans");
        $session = $this->get('adminsession');
        $session->set("section","Plans");
        $session->set("currentpage","PlansMessaging");
        $userid = $session->userid;
        $planid = $session->planid;
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansMessaging");        
        return $this->render('SharedMessagingBundle:Default:index.html.twig', array( "userid" => $userid,"messages" => $messages, "writeable" => $writeable,"planid" => $planid,"defaultMessages" => true ));

    }
    public function defaultMessagesAction()
    {
        $response = new Response();        
        $messaging = $this->get("Messaging");
        $messages2 = $messaging->messages("settings");  
        $defaultMessages = array();
        foreach ($messages2 as $message) 
        {
            foreach ($message['data'] as $row)
            {     
                foreach ($row['records'] as $record)
                {
                    $defaultMessages['values'][] = [
                        "value" => $record->value,
                        "id" => $row['section']->name."_".$record->language->name
                    ];
                    $defaultMessages['actives'][] = [
                        "active" => $record->active,
                        "id" => $row['section']->name."Active"
                    ];
                }
            }
        }
        $response->setContent(json_encode($defaultMessages));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    public function saveAction() {
        $request = Request::createFromGlobals();
        $planMessaging = $this->get("Messaging");
        $messages = $planMessaging->messages("plans");
        $em = $this->getDoctrine()->getManager();
        
        $planid = (int) $request->request->get('planid');
        $userid = (int) $request->request->get('userid');
        foreach ($messages as $message) 
        {
            foreach ($message['data'] as $row)
            {     
                foreach ($row['records'] as $record)
                {
                    $findBy = array("userid" => $userid);
                    if (!$planid)
                    {
                        $table =  "DefaultMessages";
                    }
                    else
                    {
                        $table = "PlansMessages";
                        $findBy["planid"] = $planid;
                    }
                    $findBy = array_merge($findBy,array("messagesPanesId"=>$row['section']->id, "language" => $record->language->name));
                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:'.$table);
                    $currMessage = $repository->findOneBy($findBy);
                    if ($currMessage == null)
                    {
                        $entity = "classes\\classBundle\\Entity\\".$table;
                        $currMessage = new $entity;
                        $currMessage->userid = $userid;
                        $currMessage->planid = $planid;
                        $currMessage->language = $record->language->name;
                        $currMessage->messagesPanesId = $row['section']->id;
                    }
                    $currMessage->message = $request->request->get($row['section']->name."_".$record->language->name);
                    $currMessage->isActive =  $request->request->get($row['section']->name."Active") == "on";
                    $em->persist($currMessage);
                }
            }     
        }
        $em->flush();
        return new Response("saved");
    }

}
