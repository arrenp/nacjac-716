<?php

namespace Plans\PlansBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;

class ProfilesController extends Controller
{

    public function indexAction()
    {
        $generalfunctions = $this->get('generalfunctions'); 
        $session =  $this->get('adminsession');
        $session->set("section","Plans");
        $currentpage = "PlansProfiles";
        $session->set("currentpage",$currentpage);
        $profilesClassVar = $this->get('profiles.service');
        $sections = $profilesClassVar->sections();
        //$connection = $this->get('doctrine.dbal.default_connection');
        //$querystring = "SELECT DISTINCT(profiles.id), profiles.id as profiles_id, profiles.planid as profiles_planid, profiles.planName as profiles_planName, profiles.currentBalance as profiles_currentBalance, profiles.reportDate as profiles_reportDate, profiles.profileId as profiles_profileId,AES_DECRYPT(participants.firstName, UNHEX('5E6D217336')) as participants_firstName, AES_DECRYPT(participants.lastName, UNHEX('5E6D217336')) as participants_lastName,profilesContributions.pre as profilesContributions_pre, profilesContributions.roth as profilesContributions_roth, profilesContributions.catchup as profilesContributions_catchup, profilesContributions.preCurrent as profilesContributions_preCurrent, profilesContributions.rothCurrent as profilesContributions_rothCurrent,profilesRetirementNeeds.otherAssets as profilesRetirementNeeds_otherAssets, profilesRetirementNeeds.assetTypes as profilesRetirementNeeds_assetTypes, profilesRetirementNeeds.recommendedMonthlyPlanContribution as profilesRetirementNeeds_recommendedMonthlyPlanContribution,profilesInvestments.pname as profilesInvestments_pname FROM profiles profiles LEFT JOIN  participants participants ON (profiles.participantid=participants.id  ) LEFT JOIN  profilesContributions profilesContributions ON (profiles.id=profilesContributions.profileid  ) LEFT JOIN  profilesRetirementNeeds profilesRetirementNeeds ON (profiles.id=profilesRetirementNeeds.profileid  ) LEFT JOIN  profilesInvestments profilesInvestments ON (profiles.id=profilesInvestments.profileid  ) WHERE profiles.id > 0   ORDER BY profiles_id DESC LIMIT 0,100";
        //$items = $connection->fetchAll($querystring);
        //var_dump($items);
        return $this->render('PlansPlansBundle:Profiles:index.html.twig',array("sections" => $sections, "session" => $session,"currentpage" => $currentpage));
    }

    public function searchMoreInfoAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $profilesClassVar = $this->get('profiles.service');
        $profile = $profilesClassVar->returnProfile($request->query->get("id"));
        $profileLink = $this->getBaseProfileLink($profile['profileId']);

        return $this->render('PlansPlansBundle:Profiles:searchmoreinfo.html.twig',array("profile" => $profile,"profileLink" => $profileLink ));
    }
    public function profileRowAction($id)
    {
        $profilesClassVar = $this->get('profiles.service');
        $profile = $profilesClassVar->returnProfile($id);
        return $this->render('PlansPlansBundle:Profiles:Search/profilerow.html.twig', array("profile" => $profile ) ); 
    }
    public function getBaseProfileLink($profileId)
    {
        return $this->getParameter("appurl")."profile/report/".$profileId."?fromAdmin=1";
    }
    public function getBaseProfileLinkAction($id)
    {
        return new Response($this->getBaseProfileLink($id));
    }
}