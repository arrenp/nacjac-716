<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shared\General\GeneralMethods;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Sessions\AdminBundle\Classes\adminsession;
class vadsController extends Controller
{
    public function indexAction()
    {
        $methods = $this->get('GeneralMethods');
        $connection = $this->get('doctrine.dbal.default_connection');
        $session = $this->get('adminsession');
        $session->set("section","Plans");
        $session->set("currentpage","PlansVads");
        $sql = "SELECT * FROM vAdsMediaAuth LEFT JOIN media ON media.id = vAdsMediaAuth.mediaid WHERE userid = '".$session->userid."'";        
        $vads = $connection->fetchAll($sql);        
        
        foreach ($vads as &$vad)
        {
            $vad['new'] = '<script type="text/javascript" id="vads" src="http://devops.vwise.net/vadFinal/vad.js?sid='.$session->userid.'&vid='.$session->planid.'&mediaid='.$vad['mediaid'].'"></script>';
            $vad['old'] = '<script type="text/javascript" src="https://smartplanenterprise.com/_vAds/vAds.php?vId='.$session->planid.'&sid='.$session->userid.'&mediaid='.$vad['mediaid'].'"></script>';
            $vad['videolink'] = $this->generateUrl('_plans_outreach_videoplayer_videoonly_WithoutLibrary') . "?mp4=" . $vad['mp4url'] . "&webm=" . $vad['webmurl'];
        }
        $stats = $this->forward('ManageManageBundle:vAds:getStats',array(), array("planid" => $session->planid, "userid" => $session->userid))->getContent();
        
        return $this->render('PlansPlansBundle:Vads:index.html.twig',array("vads" => $vads,"stats" => $stats,"session" => $session,"httpmethod" => $methods->httpmethod()));
    }
}
