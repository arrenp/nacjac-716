<?php

namespace Plans\PlansBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use classes\classBundle\Entity\defaultPortfolioFunds;
use classes\classBundle\Entity\defaultFunds;
use classes\classBundle\Entity\defaultPortfolios;
use classes\classBundle\Entity\plansPortfolioFunds;
use classes\classBundle\Entity\plansFunds;
use classes\classBundle\Entity\plansPortfolios;
use Shared\InvestmentsBundle\Classes\investments;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;

class InvestmentsController extends Controller
{
    private $investments;
    private $templatePath;

    public function indexAction()
    {
        $this->init();
        $this->investments->indexAction();
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansInvestments");
        $qdiaReadable = $permissions->readable("PlansQdia");
        return $this->render($this->templatePath . 'index.html.twig', array('investments' => $this->investments, "writeable" => $writeable, "qdiaReadable" => $qdiaReadable));
    }

    public function fundsAction()
    {
        $this->init();
        $this->investments->indexAction();
        $permissions = $this->get('rolesPermissions');
        $session = $this->get('adminsession');
        $session->set("currentpage", "PlansInvestmentsFunds");
        $writeable = $permissions->writeable("PlansInvestmentsFunds");
        $qdiaReadable = $permissions->readable("PlansQdia");
        return $this->render($this->templatePath . 'funds.html.twig', array('investments' => $this->investments, "writeable" => $writeable, "qdiaReadable" => $qdiaReadable));
    }

    public function portfoliosAction()
    {
        $this->init();
        $this->investments->indexAction();
        $permissions = $this->get('rolesPermissions');
        $session = $this->get('adminsession');
        $session->set("currentpage", "PlansInvestmentsPortfolios");
        $writeable = $permissions->writeable("PlansInvestmentsPortfolios");
        $qdiaReadable = $permissions->readable("PlansQdia");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansModules');
        $modules = $repository->findOneBy(array('planid' => $session->planid));
        $investmentsGraph = $modules->investmentsGraph;
        return $this->render($this->templatePath . 'portfolios.html.twig', array('investments' => $this->investments, "writeable" => $writeable, "qdiaReadable" => $qdiaReadable, "investmentsGraph" => $investmentsGraph, 'controller' => 'plans'));

    }

    public function fundreorderAction()
    {
        $this->init();
        $this->investments->fundreorderAction();
        return $this->render($this->templatePath . 'reorderfund.html.twig', array('investments' => $this->investments));
    }

    public function portfolioreorderAction()
    {
        $this->init();
        $this->investments->portfolioreorderAction();
        return $this->render($this->templatePath . 'reorderportfolio.html.twig', array('investments' => $this->investments));
    }

    function portfoliosetfundsAction()
    {
        $this->init();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $portfolioid = $request->query->get('id');
        $allfunds = "";
        $portfoliofunds = "";
        $this->investments->portfoliosetfundsAction($portfolioid);
        $allfunds = $this->investments->allfunds;
        $portfoliofunds = $this->investments->portfoliofunds;
        return $this->render($this->templatePath . 'setportfoliofunds.html.twig', array('allfunds' => $allfunds, 'portfoliofunds' => $portfoliofunds, 'userid' => $userid, 'portfolioid' => $portfolioid));
    }

    public function fundreordersaveAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $itemstring = $request->request->get('ids', '');
        $searchtype = $request->request->get('searchtype', '');
        $response = $this->investments->reorderInvestments($itemstring, $searchtype, "fund");
        return new Response($response);
    }

    public function portfolioreordersaveAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $itemstring = $request->request->get('ids', '');
        $searchtype = $request->request->get('searchtype', '');
        $response = $this->investments->reorderInvestments($itemstring, $searchtype, "portfolio");
        return new Response($response);
    }

    public function fundDeleteAction($id)
    {
        $this->init();
        $fund = $this->investments->getFund($id);
        return $this->render($this->templatePath . "Search/deletefund.html.twig", array("fund" => $fund));
    }

    public function fundDeletedAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->request->get("id", "");
        $response = $this->investments->deleteFund($id);
        return new Response($response);
    }

    public function fundaddreorderAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $name = $request->request->get("name", "");
        $link = $request->request->get("link", "");
        $ticker = $request->request->get("ticker", "");
        $fundid = $request->request->get("fundid", "");
        $response = $this->investments->saveAndReorderFunds($name, $link, $ticker, $fundid, $request->request->get("groupValueId", 0));
        return new Response($response);
    }

    public function portfolioaddreorderAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $name = $request->request->get("name", "");
        $response = $this->investments->saveAndReorderPortfolios($name, $request->request->get("customid"));
        return new Response($response);
    }

    public function portfolioDeleteAction($id)
    {
        $this->init();
        $portfolio = $this->investments->getPortfolio($id);
        return $this->render($this->templatePath . "Search/deleteportfolio.html.twig", array("portfolio" => $portfolio));
    }

    public function portfolioDeletedAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $portfolioid = $request->request->get("id");
        $response = $this->investments->deletePortfolio($portfolioid);
        return new Response($response);
    }

    public function portfolioShowFundsAction(Request $request)
    {

        $this->init();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $portfolioid = $request->query->get('portfolioid');
        $this->investments->portfoliosetfundsAction($portfolioid);
        $allfunds = $this->investments->allfunds;
        $portfoliofunds = $this->investments->portfoliofunds;
        return $this->render($this->templatePath . 'setportfoliofunds.html.twig', array('allfunds' => $allfunds, 'portfoliofunds' => $portfoliofunds, 'userid' => $userid, 'portfolioid' => $portfolioid, 'writeable' => false));

    }

    function portfoliosetfundssaveAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $portfolioid = $request->request->get('portfolioid','');
        $itemstring = $request->request->get('ids','');
        $valuestring = $request->request->get('values','');
        $itemstring = explode(",", $itemstring);
        $valuestring = explode(",", $valuestring);
        $theRealItemString = array();
        $theRealValueString = array();

        foreach($itemstring as $item) {
            if ($item != "") {
                $theRealItemString[] = $item;
            }
        }
        foreach($valuestring as $item) {
            if ($item != "") {
                $theRealValueString[] = $item;
            }
        }
        $theRealItemString = implode(",", $theRealItemString);
        $theRealValueString = implode(",", $theRealValueString);

        $response = $this->investments->savePortfolioFunds($portfolioid,$theRealItemString,$theRealValueString);

        return new Response($response);
    }

    public function editFundAction($id)
    {
        $this->init();
        $fund = $this->investments->getFund($id);
        return $this->render($this->templatePath . "Search/editfund.html.twig", array("fund" => $fund, "investments" => $this->investments));
    }

    function addDefaultFundsAction()
    {
        $this->init();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");

        return $this->render($this->templatePath . 'defaultfunds.html.twig', array('userid' => $userid, 'planid' => $planid));
    }

    function loadDefaultFundsAction()
    {
        $this->init();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultFunds');
        $funds = $repository->findBy(array('userid' => $userid));

        $enabledFunds = array();
        $i = 0;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        foreach ($funds as $fund) {
            $searchfundsName = $repository->findBy(array('planid' => $planid, 'name' => $fund->name));
            $searchfundsTicker = $repository->findBy(array('planid' => $planid, 'ticker' => $fund->ticker));

            if (!count($searchfundsName) || !count($searchfundsTicker)) {
                $enabledFunds[$i]->id = $fund->id;
                $enabledFunds[$i]->name = $fund->name;
                $enabledFunds[$i]->ticker = $fund->ticker;
                $enabledFunds[$i]->link = $fund->link;
                $i++;
            }
        }
        return $this->render($this->templatePath . 'loaddefaultfunds.html.twig', array('userid' => $userid, 'planid' => $planid, 'enabledFunds' => $enabledFunds, 'count' => $i));
    }

    function addDefaultPortfoliosAction()
    {
        $this->init();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");


        return $this->render($this->templatePath . 'defaultportfolios.html.twig');
    }

    function loadDefaultPortfoliosAction()
    {
        $this->init();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPortfolios');
        $portfolios = $repository->findBy(array('userid' => $userid));

        $enabledPortfolios = array();
        $i = 0;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansPortfolios');
        foreach ($portfolios as $portfolio) {
            $searchportfoliosName = $repository->findBy(array('planid' => $planid, 'name' => $portfolio->name));

            if (!count($searchportfoliosName)) {
                $enabledPortfolios[$i]->id = $portfolio->id;
                $enabledPortfolios[$i]->name = $portfolio->name;
                $i++;
            }
        }
        return $this->render($this->templatePath . 'loaddefaultportfolios.html.twig', array('userid' => $userid, 'planid' => $planid, 'enabledPortfolios' => $enabledPortfolios, 'count' => $i));
    }

    function addDefaultFundToPlansAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $fundid = $request->request->get('id', '');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultFunds');
        $fund = $repository->findOneBy(array('id' => $fundid));
        $name = $fund->name;
        $link = $fund->link;
        $fundId = $fund->fundId;
        $ticker = $fund->ticker;
        $response = $this->investments->saveAndReorderFunds($name, $link, $ticker, $fundId, $fund->groupValueId);
        return new Response($response);
    }

    function addDefaultPortfolioToPlansAction()
    {
        $this->init();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $portfolioid = $request->request->get('id', '');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPortfolioFunds');
        $defaultPortfolioFunds = $repository->findBy(array('portfolioid' => $portfolioid));

        $defaultRepository = $this->getDoctrine()->getRepository('classesclassBundle:defaultFunds');
        $plansRepository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        foreach ($defaultPortfolioFunds as $defaultPortfolioFund) {
            $defaultFund = $defaultRepository->findOneBy(array('id' => $defaultPortfolioFund->fundid));

            $searchFundName = $plansRepository->findOneBy(array('name' => $defaultFund->name, 'planid' => $planid));
            $searchFundTicker = $plansRepository->findOneBy(array('ticker' => $defaultFund->ticker));

            if (!count($searchFundName) || !count($searchFundTicker)) {
                $fund = new plansFunds();
                $fund->userid = $userid;
                $fund->planid = $planid;
                $fund->name = $defaultFund->name;
                $fund->ticker = $defaultFund->ticker;
                $fund->link = $defaultFund->link;
                $em->persist($fund);
                $em->flush();

            }

        }
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPortfolios');
        $portfolio = $repository->findOneBy(array('id' => $portfolioid));
        $name = $portfolio->name;
        $portfolioid = $this->investments->saveAndReorderPortfolios($name);

        foreach ($defaultPortfolioFunds as $defaultPortfolioFund) {
            $defaultFund = $defaultRepository->findOneBy(array('id' => $defaultPortfolioFund->fundid));

            $searchFund = $plansRepository->findOneBy(array('name' => $defaultFund->name, 'ticker' => $defaultFund->ticker, 'planid' => $planid));

            $plansPortfolioFund = new plansPortfolioFunds();
            $plansPortfolioFund->fundid = $searchFund->id;
            $plansPortfolioFund->portfolioid = $portfolioid;
            $plansPortfolioFund->userid = $userid;
            $plansPortfolioFund->planid = $planid;
            $plansPortfolioFund->orderid = $defaultPortfolioFund->orderid;
            $plansPortfolioFund->percent = $defaultPortfolioFund->percent;

            $em->persist($plansPortfolioFund);
            $em->flush();
        }

        $response = "saved";
        return new Response($response);
    }

    public function qdiaAction()
    {
        $this->init();
        $session = $this->get('adminsession');
        $session->set("currentpage", "PlansQdia");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        $searchFund = $repository->findOneBy(array('planid' => $this->investments->planid, 'qdiaDefault' => 1));
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansQdia");


        if ($searchFund == null) {
            $search = $this->get('generalfunctions');
            if ($writeable)
                $fundlist = $search->search("plansFunds", "planid", $this->investments->planid, "funds", $this->templatePath . "Search/qdiawriteable.html.twig", "name,ticker", "fundlist", "inline-block", "classesclassBundle:Search:search.html.twig", "orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
            else
                $fundlist = $search->search("plansFunds", "planid", $this->investments->planid, "funds", $this->templatePath . "Search/qdiareadonly.html.twig", "name,ticker", "fundlist", "inline-block", "classesclassBundle:Search:search.html.twig", "orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
            return $this->render($this->templatePath . 'qdia.html.twig', array("fundlist" => $fundlist));
        } else {
            $qdiaFund = $this->findQdiaFund($searchFund->name);
            $paths = $this->get("PathsService")->getPlanPathsApp($this->investments->planid);
            return $this->render($this->templatePath . 'qdiaselected.html.twig', array(
                "searchFund" => $searchFund, "qdiaFund" => $qdiaFund, "writeable" => $writeable, "paths" => $paths
            ));
        }
    }


    public function qdiaFindInfoAction($fundname)
    {
        $this->init();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:qdiaFunds');
        $qdiaFund = $this->findQdiaFund($fundname);
        return $this->render($this->templatePath . 'Search/qdiafindinfo.html.twig', array("qdiaFund" => $qdiaFund));
    }


    public function qdiaSelectFundAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $fundid = $request->query->get('id', '');


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        $planFunds = $repository->findBy(array('planid' => $this->investments->planid, 'qdiaDefault' => 1));

        $this->qdiaClearFunds($planFunds);


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        $selectedFund = $repository->findOneBy(array('id' => $fundid));


        $qdiaFund = $this->findQdiaFund($selectedFund->name);

        $selectedFund->qdiaDefault = 1;
        if ($qdiaFund != null) {
            $selectedFund->qdiaVideo = $qdiaFund->video;
            $selectedFund->qdiaUrl = $qdiaFund->url;

        }
        $em->flush();

        return $this->redirect($this->generateUrl('_plans_investments_qdia'));

    }

    public function qdiaUnselectFundAction()
    {
        $this->init();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        $planFunds = $repository->findBy(array('planid' => $this->investments->planid, 'qdiaDefault' => 1));
        $this->qdiaClearFunds($planFunds);
        return $this->redirect($this->generateUrl('_plans_investments_qdia'));
    }

    public function qdiaSaveFundAction(Request $request)
    {
        $this->init();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        $entityManager = $this->getDoctrine()->getManager();
        $fund = $repository->findOneBy(array('planid' => $this->investments->planid, 'qdiaDefault' => 1));
        $fund->pathId = $request->get("pathId") !== 'default' ? $request->get("pathId") : null;
        $entityManager->flush();
        return $this->redirectToRoute('_plans_investments_qdia');

    }

    public function qdiaClearFunds($funds)
    {
        $em = $this->getDoctrine()->getManager();

        foreach ($funds as $fund) {
            $fund->qdiaDefault = 0;
            $fund->qdiaVideo = "";
            $fund->qdiaUrl = "";
            $fund->pathId = null;

        }
        $em->flush();
    }

    public function findQdiaFund($name)
    {
        $this->init();

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:qdiaFunds');
        $qdiaFund = $repository->findOneBy(array('name' => $name, 'planid' => $this->investments->planid));

        if ($qdiaFund == null)
            $qdiaFund = $repository->findOneBy(array('name' => $name, 'userid' => $this->investments->userid));

        if ($qdiaFund == null)
            $qdiaFund = $repository->findOneBy(array('name' => $name));

        return $qdiaFund;
    }

    public function saveGroupIdAction(Request $request)
    {
        $this->init();
        $this->investments->saveGroupId($request->request->get("groupid"));
        return new Response("");
    }

    public function init()//would use in constructor but doesn't seem possible
    {
        $session = $this->get('adminsession');
        $session->set("section", "Plans");
        //$session->set("currentpage","PlansInvestments");
        $userid = $session->userid;
        $planid = $session->planid;
        $em = $this->getDoctrine();
        $search = $this->get('generalfunctions');
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansInvestments");
        $this->investments = new investments($em, "plans", $search, $userid, $writeable, $planid, $this->container);
        $this->templatePath = 'PlansPlansBundle:Investments:';
    }

    public function riskBasedFundsAction()
    {
        $this->init();
        $session = $this->get('adminsession');
        $session->set("currentpage", "PlansRiskBasedFunds");
        return $this->render($this->templatePath . "riskbasedfunds.html.twig", array('investments' => $this->investments));
    }

    public function riskBasedFundsAddAction()
    {
        $this->init();
        return $this->render("SharedInvestmentsBundle:Default:addriskbasedfunds.html.twig", array('investments' => $this->investments, "label" => "_plans_investments_riskBasedFunds_add_saved"));
    }

    public function riskBasedFundsAddSavedAction(Request $request)
    {
        $this->init();
        $this->investments->addRiskBasedFund($request->request->all());
        return new Response("");
    }

    public function riskBasedFundsEditAction(Request $request)
    {
        $this->init();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansRiskBasedFunds');
        $riskBasedFund = $repository->findOneBy(array("id" => $request->query->get("id")));
        return $this->render("SharedInvestmentsBundle:Default:editriskbasedfunds.html.twig", array('investments' => $this->investments, "label" => "_plans_investments_riskBasedFunds_edit_saved", "riskBasedFund" => $riskBasedFund));
    }

    public function riskBasedFundsEditSavedAction(Request $request)
    {
        $this->init();
        $this->investments->editRiskBasedFund($request->request->all());
        return new Response("");
    }

    public function riskBasedFundsDeleteAction(Request $request)
    {
        $this->init();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansRiskBasedFunds');
        $riskBasedFund = $repository->findOneBy(array("id" => $request->query->get("id")));
        return $this->render("SharedInvestmentsBundle:Default:deleteriskbasedfunds.html.twig", array('investments' => $this->investments, "label" => "_plans_investments_riskBasedFunds_delete_saved", "riskBasedFund" => $riskBasedFund));
    }

    public function riskBasedFundsDeleteSavedAction(Request $request)
    {
        $this->init();
        $this->investments->deleteRiskBasedFund($request->request->all());
        return new Response("");
    }
    
    private function getRequest() {
        return $this->get('request_stack')->getCurrentRequest();
    }
}