<?php

namespace Cron\CronBundle\Command;

use classes\classBundle\Entity\plansRkp;
use classes\classBundle\Entity\plansRkpCampaignDates;
use classes\classBundle\Entity\plansRkpEmail;
use Cron\CronBundle\Utils\MailGun;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use classes\classBundle\MappingTypes\EncryptedType;
use Shared\General\GeneralMethods;
use Shared\RecordkeepersBundle\Classes\recordKeeper;

class CreateRkpDataCommand extends ContainerAwareCommand {
    
    protected $run = false;
    protected $send = false;
    protected $sendplan = false;
    protected $sendemail = false;
    protected $result = array();
    protected $nextSevenDaysArray = array();
    protected $batchPosition = 0;
    protected $recordKeeper;
    
    /** @var InputInterface */
    protected $input;
    
    /** @var OutputInterface */
    protected $output;

    /** @var EntityManager */
    protected $entityManager;
    
    public $session_id;
    public $table = "accountsRecordkeepersSrt";
    
    
    protected function configure()
    {
        $this
            ->setName('app:CreateRkpData')
            ->setDescription('Syncs the plansRkp tables and sends scheduled emails')
            ->setHelp('Syncs the plansRkp tables and sends scheduled emails')
            ->addOption("r", null, InputOption::VALUE_REQUIRED, "Set to 'yes' to sync database")
            ->addOption("s", null, InputOption::VALUE_REQUIRED, "Set to 'yes' to send emails")
            ->addOption("p", null, InputOption::VALUE_REQUIRED, "Force emails for this plan id")
            ->addOption("e", null, InputOption::VALUE_REQUIRED, "Force emails for this email address")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager = $this->getContainer()->get("doctrine")->getManager();
        $this->recordKeeper = new Recordkeeper($this->entityManager);

        $this->run = $input->getOption("r") === "yes";
        $this->send = $input->getOption("s") === "yes";
        $this->sendplan = ($input->getOption("p") !== null) ? $input->getOption("p") : false;
        $this->sendemail = ($input->getOption("e") !== null) ? $input->getOption("e") : false;
        
        $this->input = $input;
        $this->output = $output;


        $this->nextSevenDaysArray = $this->createNextSevenDaysArray();

        $connection = $this->entityManager->getConnection();
        if ($this->run) {
            $connection->query("DELETE FROM plansRkp WHERE id > 0");
            $connection->query("DELETE FROM plansRkpEmail WHERE id > 0");
            $connection->query("DELETE FROM plansRkpCampaignDates WHERE id > 0");
        }

        $sql = "SELECT *,AES_DECRYPT(userName, UNHEX('".EncryptedType::KEY."')) as userName,AES_DECRYPT(password, UNHEX('".EncryptedType::KEY."')) as password FROM ".$this->table." LEFT JOIN accounts on accounts.id = ".$this->table.".userid WHERE powerviewEnabled = 1 or irioEnabled = 1";
        $data = $connection->fetchAll($sql);

        foreach ($data as $account) {
            $this->executeForAccount($account);
        }
    }
    
    protected function persistEntity($entity) {
        if ($this->run) {
            $this->entityManager->persist($entity);
            $this->batchPosition = $this->batchPosition + 1;
            
            if ($this->batchPosition % 20 === 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
        }
    }
    
    protected function createNextSevenDaysArray() {
        $nextSevenDaysArray = array();
        for($i = 0; $i < 8; $i++) {
            $date = date("m/d", strtotime("+$i days"));
            $nextSevenDaysArray[$date] = true;
        }
        return $nextSevenDaysArray;
    }
    
    public function accountUrl($account)
    {
        $url = $account['url'];
        if ($account['recordkeeperVersion'] < 3)
        {
            $url = $url."/SDDAv2";
        }
        return $url;
    }
    public function sid($account)
    {
        $url = 'https://' . $this->accountUrl($account)."/pas.asmx/GetOperatorSessionID";
        $fields = array("userName" => $account['userName'],"password" => $account['password'], "userType" => $account['type'], "siteToken" => $account['siteToken']);
        $ch = curl_init();
        $fieldString = "";
        foreach ($fields as $key => $value)
        {
            if ($fieldString != "")
            $fieldString = $fieldString."&";
            $fieldString = $fieldString.$key."=".$value;
        }
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldString);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        $result = curl_exec($ch);
        if ($result) 
        {
            try
            {
                $xml = new \DOMDocument();
                $xml->preserveWhiteSpace = false;
                $xml->formatOutput = true;
                $xml->loadXML($result);
                $this->session_id = $xml->textContent;
                curl_close($ch);  
                return true;                
            } 
            catch (\Exception $ex) 
            {
                
            }
        }
        return false;
    }
    
    public function call($apiCall,$account,$parameters = array())
    {
        $url = 'https://' . $this->accountUrl($account) . '/pas.asmx/' . $apiCall . '?sessionId=' . $this->session_id . ($parameters ? '&' . http_build_query($parameters): '');
        try 
        {
            $this->recordKeeper->writeXmlLog('unknown', null, null, $url, null, $account['id'], 'send', $apiCall, "createRkpCommand", 'createRkpCommand');
            $rawXml = file_get_contents($url, FALSE);
            if ($rawXml) 
            {
                $xml = new \SimpleXMLElement($rawXml);
                $this->recordKeeper->writeXmlLog('unknown', null, null, $rawXml, null, $account['id'], 'receive', $apiCall, "createRkpCommand", 'createRkpCommand');
                return $xml;
            }
        }
        catch (\Exception $e)
        {
            /* TODO: Error Handling */
        }
        return FALSE;
    }
    
    public function executeForAccount($account) {
        $general = $this->getContainer()->get("GeneralMethods");

        $connection = $this->entityManager->getConnection();
        $this->output->writeln($account['partnerid']);
        if (!$this->sid($account))
        {
            $this->output->writeln("Sid Call Failed");
            $to = 'cgillespie@vwise.com';
            $subject = '!!Powerview/IRIO: Sid Failed!!';
            $message = $account['partnerid'];
            $headers = 'From: system@vwise.com' . "\r\n" .
                    'Reply-To: system@vwise.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
            mail($to, $subject, $message, $headers);
            return false;
        }
        $apiCall = 'getVwisePlanDetails';
        $data = $this->call($apiCall, $account); 
        
        if ($data) {
            $powerViewUrl = $data->PowerViewUrl;
            $participantLoginUrl = $data->ParticipantLoginUrl;
            $vWiseSSOUrl = preg_replace('/\?.*/', '', $data->vWiseSSOUrl);

            foreach ($data->vWisePlanList->vWisePlanDetailDTO as $plan) {
                $queue = [];
                $planId = (string) $plan->PlanCode;

                /* Check if plan is in database */
                $sql2 = "SELECT count(*) AS cnt FROM plans WHERE partnerid = '{$account['partnerid']}' AND planid = '$planId' AND deleted = 0";
                $this->output->writeln('****************************************************');
                $this->output->writeln($sql2);
                $this->output->writeln('****************************************************');
                
                $count = (int) $connection->fetchColumn($sql2);
                if ($count == 0) {
                    $type = array();
                    if ($plan->PowerViewEnabled == 'true') {
                        $type[] = 'powerview';
                    }
                    if ($plan->IRIOEnabled == 'true') {
                        $type[] = 'irio';
                    }
                    $addedplanid = $general->addPlan($this->entityManager, $account['id'], $plan->PlanCode, $plan->PlanName, null, $type);
                    $general->logAddPlan($this->entityManager, $addedplanid);
                }
                /* End check */

                $LogoCoBrandURL = (string) $plan->LogoCoBrandURL;
                //$HeaderDarkBgColor = (string)$plan->HeaderDarkBgColor ? (string)$plan->HeaderDarkBgColor : 'white';
                $HeaderDarkBgColor = 'white';
                $HeaderLightBgColor = (string) $plan->HeaderLightBgColor ? (string) $plan->HeaderLightBgColor : 'white';
                $HeaderBorderColor = (string) $plan->HeaderBorderColor;
                    
                $plansRkp = new plansRkp();
                $plansRkp->partnerid = $account['partnerid'];
                $plansRkp->recordkeeperPlanid = (string) $plan->PlanCode;
                $plansRkp->hrFirstName = (string) $plan->ContactFirstName;
                $plansRkp->hrLastName = (string) $plan->ContactLastName;
                $plansRkp->hrEmail = (string) $plan->ContactEmail;
                $plansRkp->participantLoginUrl = $participantLoginUrl;
                $plansRkp->vWiseSSOUrl = $vWiseSSOUrl;
                $plansRkp->powerViewUrl = $powerViewUrl;
                if (isset($plan->RetirementAge)) {
                    $plansRkp->retirementAge = (string) $plan->RetirementAge;
                }
                if (isset($plan->LogoBrandURL)) {
                    $plansRkp->logo = (string) $plan->LogoBrandURL;
                }
                if (isset($plan->DeferralFormLocation)) {
                    $plansRkp->deferralFormLocation = (string) $plan->DeferralFormLocation;
                }
                if (isset($plan->ContactPhone)) {
                    $plansRkp->hrContactPhone = (string) $plan->ContactPhone;
                }
                if (isset($plan->PowerViewEnabled) && (string) $plan->PowerViewEnabled == "true") {
                    $plansRkp->powerViewEnabled = 1;
                }
                if (isset($plan->IRIOEnabled) && (string) $plan->IRIOEnabled == "true") {
                    $plansRkp->irioEnabled = 1;
                }
                if (isset($plan->EmailAddressFrom)) {
                    $plansRkp->emailAddressFrom = (string) $plan->EmailAddressFrom;
                }
                if (isset($plan->PlanName)) {
                    $plansRkp->planName = (string) $plan->PlanName;
                }
                $plansRkp->smartPlanEnabled = (string) $plan->PlanCode == 'NC1' ? 0 : 1;
                $this->persistEntity($plansRkp);
                
                $this->output->writeln('==============================================');
                $this->output->writeln('Partner ID:');
                $this->output->writeln("\t" . $account['partnerid']);
                $this->output->writeln('Plans:');
                $this->output->writeln("\t" . $plan->PlanCode);
                $this->output->writeln('----------------------------------------------');
                $this->output->writeln('Emails:');

                $this->result[$account['partnerid']][] = (string) $plan->PlanCode;

                $campaignDatesOutput = "";
                $datecounter = 1;
                $frequencyString = "CampaignFrequency";

                $frequency = $frequencyString . $datecounter;
                $isScheduledForToday = false;
                $isScheduledWithinSevenDays = false;
                while (isset($plan->$frequency)) {
                    
                    $plansRkpCampaignDates = new plansRkpCampaignDates;
                    $plansRkpCampaignDates->partnerid = $account['partnerid'];
                    $plansRkpCampaignDates->recordkeeperPlanid = (string) $plan->PlanCode;
                    $plansRkpCampaignDates->scheduled = (string) $plan->$frequency;
                    $this->persistEntity($plansRkpCampaignDates);
                    
                    if (date('m/d') == (string) $plan->$frequency) {
                        $isScheduledForToday = true;
                    } 
                    if (isset($this->nextSevenDaysArray[(string) $plan->$frequency])) {
                        $isScheduledWithinSevenDays = true;
                    }

                    $campaignDatesOutput .= "\t" . (string) $plan->$frequency . "\n";
                    
                    $datecounter++;
                    $frequency = $frequencyString . $datecounter;
                }
                
                if ($isScheduledWithinSevenDays) {
                    $apiCall = 'getVwiseParticipantDetails';
                    $participantsCall = $this->call($apiCall, $account, ['planID' => $planId]);
                    $participants = array();
                    if (isset($participantsCall->vWiseParticipantList->vWiseParticipantDetailDTO)) {
                        $participants = $participantsCall->vWiseParticipantList->vWiseParticipantDetailDTO;
                    }
                    
                    foreach ($participants as $participant) {
                        if ((string) $participant->Email != "") {
                            $plansRkpEmail = new plansRkpEmail;
                            $plansRkpEmail->partnerid = $account['partnerid'];
                            $plansRkpEmail->recordkeeperPlanid = (string) $plan->PlanCode;
                            $plansRkpEmail->email = (string) $participant->Email;
                            $plansRkpEmail->firstName = (string) $participant->FirstName;
                            $plansRkpEmail->lastName = (string) $participant->LastName;
                            $this->persistEntity($plansRkpEmail);

                            /* Temporary code to send emails out */
                            //if (($account['partnerid'] == 'RPG_SRT_PROD' || $account['partnerid'] == 'TST_SRT_PROD' || $account['partnerid'] == 'SR3_SRT_DEV' || $account['partnerid'] == 'L14_SRT_DEV' || $account['partnerid'] == 'INT_SRT_DEV') && (string)$participant->Email != '') {
                            $from = (string) $plan->EmailAddressFrom;
                            $email = (string) $participant->Email;
                            $subject = 'Check out your progress in your ' . (string) $plan->PlanName;
                            if (isset($plan->PowerViewEnabled) && (string) $plan->PowerViewEnabled == "true") {
                                $queue[] = [
                                    'from' => $from,
                                    'email' => $email,
                                    'subject' => $subject,
                                    'params' => [
                                        'name' => (string) $participant->FirstName,
                                        'loginUrl' => (string) $data->PowerViewUrl,
                                        'forgotUrl' => (string) $data->ParticipantLoginUrl,
                                        'logoUrl' => (string) $plan->LogoBrandURL,
                                        'LogoCoBrandURL' => $LogoCoBrandURL,
                                        'HeaderDarkBgColor' => $HeaderDarkBgColor,
                                        'HeaderLightBgColor' => $HeaderLightBgColor,
                                        'HeaderBorderColor' => $HeaderBorderColor,
                                    ],
                                ];
                            }
                            $this->output->writeln("\t" . $email);
                            /* End */
                        }
                    }
                }
                
                $this->output->writeln('----------------------------------------------');
                $this->output->writeln('Dates:');
                $this->output->write($campaignDatesOutput);

                if (($this->send && $isScheduledForToday) || $this->sendplan == (string) $plan->PlanCode || $this->sendemail !== FALSE) {
                    $mg = new MailGun();
                    foreach ($queue as $q) {
                        if ($this->sendemail === false || $this->sendemail == $q['email']) {
                            $mg->tag = 'POWERVIEW_' . $account['partnerid'] . '_' . $plan->PlanCode;
                            $mg->send($q['from'], $q['email'], $q['subject'], $q['params']);
                            if ($this->sendemail !== FALSE) {
                                exit;
                            }
                        }
                    }
                    if ($this->sendplan !== FALSE) {
                        exit; /* Exit out after plan override is set and email is sent */
                    }
                }
                $this->output->write("==============================================\n\n");
            }
            
            if ($this->run) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
        }
    }
}
