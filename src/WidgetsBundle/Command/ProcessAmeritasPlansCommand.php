<?php

namespace WidgetsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Shared\General\SFTPConnection;
use Symfony\Component\Console\Input\InputArgument;
use classes\classBundle\Entity\plans;
use Plans\PlansBundle\Controller\PlansHomeController;

class ProcessAmeritasPlansCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:ProcessAmeritasPlans')
            ->setDescription('Processes plans for Ameritas')
            ->addOption('accountId', null, InputOption::VALUE_REQUIRED, 'Enter the account ID to run the report on.')
            ->addOption('filename', null, InputOption::VALUE_REQUIRED, 'TESTING parameter. Enter the filename to run command on.')
            ->addOption('startDate', null, InputOption::VALUE_REQUIRED, 'Date of the plans file to look for. If the start date is today or in the future, use current date minus one day');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->postToSlack('ameritas irio plans', "job starting");
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logincredentials = $em->getRepository('classesclassBundle:AmeritasFTP')->findAll();
        $host = $logincredentials[0]->ip;
        $user = $logincredentials[0]->user;
        $pass = $logincredentials[0]->pass;

        $accountId = $input->getOption('accountId');
        if (empty($accountId)) {
            throw new \Exception("Please enter an AccountID");
        }

        $columnsSet = false;
        $columns = array();
        $counter = 0;
        $map = array();

        $sftp = new SFTPConnection($host);
        $sftp->login($user, $pass);

        $filename = "SFPLANFL-DELTA-" . date('Ymd', strtotime('-1 day', strtotime('now')));

        if(!empty($input->getOption('filename'))) {
            $filename = $input->getOption('filename');
        } else if (!empty($input->getOption('startDate'))) {
            $time = strtotime($input->getOption('startDate'));
            if (strtotime('-1 day', time()) <= $time) { // future date
                $time = date('Ymd', strtotime('-1 day', strtotime('now')));
            } else { // past date
                $time = date('Ymd', $time);
            }

            $filename = "SFPLANFL-DELTA-" . $time;
        }

        $handle = $sftp->openFile("/data/prod/outbound/$filename.TXT");

        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $line = str_replace('"', '', $line);
                if (!$columnsSet) {
                    $columns = explode(',', $line);
                    $columnsSet = true;
                    continue;
                } else {
                    $i = 0;
                    $rowContent = explode(',', $line);
                    foreach ($columns as $column) {
                        if (!isset($map[$column])) {
                            $map[$column] = array();
                        }
                        $map[$column][$counter] = $rowContent[$i++];
                    }
                    $plan = $em->getRepository('classesclassBundle:plans')->findOneBy(["planid" => $map['PLN-KEY'][$counter], 'userid' => $accountId]);

                    if ($map['PLN-STATUS'][$counter] == 'ACTIVE') {
                        if (!$plan || ($plan != null && $plan->deleted == 1)) {
                            $this->createNewPlan($accountId, $map, $counter, $em);
                        }
                    } else if ($map['PLN-STATUS'][$counter] == 'TERMINATED') {
                        if ($plan) {
                            $plan->deleted = 1;
                            $plan->deletedDate = new \DateTime("NOW");
                        }

                    }
                    $counter++;
                }
                if ($counter % 100 == 0) {
                    $em->flush();
                }

            }
            $em->flush();

            $this->postToSlack('ameritas irio plans', "job completed");

            fclose($handle);
        } else {
            $this->postToSlack('ameritas irio plans', "something went wrong :(");
            throw new \Exception("Something wrong");
        }

    }

    protected function createNewPlan($accountId, $map, $counter, $em) {

        $plan = new PlansHomeController();
        $plantobeadded = new plans();
        $repository = $em->getRepository('classesclassBundle:defaultPlan');
        $defaultPlan = $repository->findOneBy(array('userid' => $accountId));

        $repository = $em->getRepository('classesclassBundle:accounts');
        $accounts = $repository->findOneBy(array('id' => $accountId));


        $plan->addPlan($plantobeadded, $accountId, $em, $defaultPlan);
        $plantobeadded->userid = $accountId;
        $plantobeadded->partnerid = $accounts->partnerid;
        $plantobeadded->planid = $map['PLN-KEY'][$counter];
        $plantobeadded->activePlanid = $map['PLN-KEY'][$counter];
        $plantobeadded->name = $map['PLN-PLAN-NAME'][$counter];
        $plantobeadded->company = $map['PLN-COMPANY-NAME'][$counter];
        $plantobeadded->phone = $map['PLN-CONTACT-PHONE'][$counter];
        $plantobeadded->phone2 = '';
        $plantobeadded->address = $map['PLN-CONTACT-ADDR1'][$counter];
        $plantobeadded->address2 = $map['PLN-CONTACT-ADDR2'][$counter];
        $plantobeadded->city = $map['PLN-CONTACT-CITY'][$counter];
        $plantobeadded->state = $map['PLN-CONTACT-STATE'][$counter];
        $plantobeadded->zip = $map['PLN-CONTACT-ZIP'][$counter];
        $plantobeadded->email = $map['PLN-CONTACT-EMAIL'][$counter];
        $plantobeadded->smartPlanAllowed = 0;
        $plantobeadded->irioAllowed = 1;
        $plantobeadded->deleted = 0;

        $em->persist($plantobeadded);
        $this->postToSlack('ameritas irio plans', "adding plan - user id: {$accountId} partner id: {$accounts->partnerid} plan id: {$map['PLN-KEY'][$counter]}");

    }

    private function postToSlack($tag, $message) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_URL, "https://hooks.slack.com/services/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");
        $encodedData = json_encode(['text' => "{$tag}: {$message}"]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Content-Length:' . strlen($encodedData)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        return $data;
    }
}