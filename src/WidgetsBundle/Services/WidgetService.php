<?php
namespace WidgetsBundle\Services;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\CTAStat;
use RollBar;
class WidgetService
{
    var $container;
	var $em;
	var $connection;
	var $session;

    public function __construct(containerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get("doctrine");
        $this->connection = $this->container->get("doctrine.dbal.default_connection");
		$this->session = $this->container->get('session');
    }

	public function isMobile() {
		$tmp = Request::createFromGlobals();
		$mobile = FALSE;

		if($tmp->query->get("mobile") || preg_match('/(alcatel|amoi|avantgo|blackberry|benq|cell|cricket|docomo|elaine|htc|iemobile|iphone|ipaq|ipod|j2me|java|midp|mini|mmp|motorola|nec-|nokia|palm|panasonic|philips|phone|playbook|sagem|sharp|sie-|silk|smartphone|sony|symbian|t-mobile|telus|up\.browser|up\.link|vodafone|wap|webos|wireless|xda|xoom|zte|android|ipad|mobi)/i', $tmp->headers->get('User-Agent'))) {
			$mobile = TRUE;
		}

		return $mobile;
	}

    public function setLang() {
        $request = $this->session->get("REQUEST");
        $lang = in_array($request['lang'], array('en', 'es')) ? $request['lang'] : 'en';
        $translator = $this->container->get('translator');
        $translator->setLocale($request['lang']);
        
        if (!empty($request['partner_id'])) {
            $accountId = $this->connection->fetchColumn("SELECT id FROM accounts WHERE partnerid = :partner_id", ['partner_id' => $request['partner_id']]);
            if ($accountId !== false) {
                $translator->setAccountId($accountId);
            }
        }
        return $lang;
    }
        
    public function getCrashUserData(){
        
		$session = $this->container->get('session');
		$data    = $session->get('DATA');        
        $rkp = $session->get('rkp');
        
        $strip = array('address', 'city', 'region', 'postalCode', 'maritalStatus', 
                       'gender', 'dateOfBirth', 'age',
                       'email', 'phone');
        
        foreach($strip as $clean){
            if (isset($data[$clean]))
            unset($data[$clean]);
        }
        
        return [
			'DATA' => $data,
			'RKP' => $rkp,
		];
    }        
	
    public function sendErrorNotification()
    {
        \Rollbar::init(array('access_token' => '9421d89825884d84b14e42a9d4a82c70',"included_errno" => 0));
        $message =  json_encode($this->getCrashUserData());
        \Rollbar::report_message('Widget Error',
        \Level::INFO,array("session" => $message));
    }  
    public function getRKPData()
    {
        $this->container->get('spe.app.rkp')->getRKPData();
    }
}
