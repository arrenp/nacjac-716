<?php
namespace WidgetsBundle\Services;
use Symfony\Component\DependencyInjection\ContainerInterface;
class mailService
{
    public function __construct(containerInterface $container){
        $this->container = $container;
    }
    function sendEmail($params){    
        if (!isset($params['from']))
        $params['from'] = "info@smartplanenterprise.com";
        $message = \Swift_Message::newInstance()
        ->setSubject($params['subject'])
        ->setFrom($params['from'])
        ->setSender($params['from'])
        ->setTo($params['email'])
        ->setBody(
        $params['html'],'text/html');
        return $this->container->get('mailer')->send($message); 
    }
    function sendText($params)
    {
        include getcwd()."/../src/classes/classBundle/Vendor/twilio/Services/Twilio.php";
        if (!isset($params['from']))
        $params['from'] = "9493045209";
        $AccountSid = "AC31a4576916bec87a2f7e3fc74bb9c3cb";
        $AuthToken  = "e92df1dd30cbc3dc1d5dc85fc2445b68";
        $client = new \Services_Twilio($AccountSid, $AuthToken);
        $message = $client->account->messages->create(array(
            "From" => $params['from'],
            "To" => $params['to'],
            "Body" => $params['text'],
        )); 
        return $message;
    }
}


