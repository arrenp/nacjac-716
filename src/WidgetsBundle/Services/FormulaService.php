<?php

namespace WidgetsBundle\Services;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FormulaService
{
	var $interest_rate;
	var $precision;
        var $container;
	var $em;
	var $connection;
	var $session;
	var $payroll = [
		'ANNUALLY' => 1,
		'MONTHLY' => 12,
		'SEMI-MONTHLY' => 24,
		'BI-WEEKLY' => 26,
		'WEEKLY' => 52,
	];

        public function __construct(containerInterface $container)
	{
		$this->interest_rate = 0.07;
		$this->precision = 10;

        $this->container = $container;
        $this->em = $this->container->get("doctrine");
        $this->connection = $this->container->get("doctrine.dbal.default_connection");
		$this->session = $this->container->get('session');
	}

	public function convertPercentToFlatDollar($salary, $percent, $frequency) {
        $flatDollar = round(bcmul($salary, bcdiv($percent, 100.0, $this->precision), $this->precision));
        return $flatDollar;
	}

	public function convertFlatDollarToPercent($salary, $dollar, $frequency,$mul = true) {
        $percent = bcdiv($dollar, $salary, $this->precision);
        if ($mul) {
            $percent = bcmul($percent, 100.0, $this->precision);
        }
        return $percent;
	}

    /**
     * Adapted from _projectedMonthlyRetirement.
     * This function takes absolute $deferralRate instead of relative $increase
     * @param type $data
     * @param type $deferralRate
     * @return float
     */
    public function getProjectedMonthlyIncome($data, $deferralRate) {
        $newData = $this->_calculateNew($data);
        if ($data['partDeferralSetting'] == "dollar")
        {
            $deferralRate =  $deferralRate * $this->payroll[$data['salaryFrequency']];
        }
        $projectedAnnualDepositGrowth = $this->_projectedAnnualDepositGrowth($newData,$deferralRate);
        $projectedAccountBalance_ = $newData['projectedCurrentBalanceSum'] + $projectedAnnualDepositGrowth;
        $inflationIncreased = bcpow(1 + $newData['inflationDiscount'],$newData['ageDiff'],$newData['precision']);
        $discountedValueOfFutureBalance = bcdiv($projectedAccountBalance_,$inflationIncreased,$newData['precision']);
        return $discountedValueOfFutureBalance/$newData['aprUsed'];
    }

	private function _getProjectedCurrentBalance($b, $n, $retire = 65) 
	{
            if ($n >= $retire) {
                return $b;
            }
            return bcmul($b, bcpow(1 + $this->interest_rate, ($retire - $n), $this->precision), $this->precision);
	}

	private function _getProjectedAnnualDepositGrowth($d, $n, $retire = 65) 
	{
		if ($n >= $retire) {
			return 0;
		}

		return bcmul($d, bcdiv((bcpow(1 + $this->interest_rate, ($retire - $n), $this->precision) - 1), $this->interest_rate, $this->precision), $this->precision);
	}

	private function _setProfile(&$data) {
        $rkp = $this->session->get('rkp');
	$data2 = $this->session->get("DATA");	
		$data['planName'] = isset($rkp['plan_data']->planName) ? $rkp['plan_data']->planName : '';
		$data['partDeferralSetting'] = isset($rkp['plan_data']->partDeferralSetting) ? $rkp['plan_data']->partDeferralSetting : 'percent';
		$data['firstName'] = isset($rkp['plan_data']->firstName) ? $rkp['plan_data']->firstName : null;
		$data['lastName'] = isset($rkp['plan_data']->lastName) ? $rkp['plan_data']->lastName : null;
		$data['address'] = isset($rkp['plan_data']->address) ? $rkp['plan_data']->address : null;
		$data['city'] = isset($rkp['plan_data']->city) ? $rkp['plan_data']->city : null;
		$data['region'] = isset($rkp['plan_data']->region) ? $rkp['plan_data']->region : null;
		$data['postalCode'] = isset($rkp['plan_data']->postalCode) ? $rkp['plan_data']->postalCode : null;
		$data['maritalStatus'] = isset($rkp['plan_data']->maritalStatus) ? $rkp['plan_data']->maritalStatus : null;
		$data['gender'] = isset($rkp['plan_data']->gender) ? $rkp['plan_data']->gender : null;
		//$data['ssn'] = isset($rkp['plan_data']->partSSN) ? $rkp['plan_data']->partSSN : null;
		$data['asOfDate'] = isset($rkp['plan_data']->asOfDate) && $rkp['plan_data']->asOfDate ? date('m/d/Y', strtotime($rkp['plan_data']->asOfDate)) : null;
		$data['dateOfBirth'] = isset($rkp['plan_data']->dateOfBirth) && $rkp['plan_data']->dateOfBirth ? date('m/d/Y', strtotime($rkp['plan_data']->dateOfBirth)) : null;
		$data['age'] = isset($rkp['plan_data']->dateOfBirth) && $rkp['plan_data']->dateOfBirth != null ? (new \DateTime($rkp['plan_data']->dateOfBirth))->diff(new \DateTime('now'))->y : 65;
        $data['email'] = isset($rkp['plan_data']->email) ? $rkp['plan_data']->email : null;
		$data['phone'] = isset($rkp['plan_data']->phone) ? $rkp['plan_data']->phone : null;
		$data['partStatus'] = isset($rkp['plan_data']->partStatus) ? $rkp['plan_data']->partStatus : null;
		$data['postContributions'] = isset($rkp['plan_data']->planAllowsDeferralsWidget) && $rkp['plan_data']->planAllowsDeferralsWidget == 1 ? 1 : 0;
		$data['balance'] = isset($rkp['plan_data']->totalBalance) ? $rkp['plan_data']->totalBalance : 0;
                $data['preCurrent'] = isset($rkp['plan_data']->preCurrent) ? $rkp['plan_data']->preCurrent : 0;                
                $data['rothCurrent'] = isset($rkp['plan_data']->rothCurrent) ? $rkp['plan_data']->rothCurrent : 0;
		$data['postCurrent'] = isset($rkp['plan_data']->postCurrent) ? $rkp['plan_data']->postCurrent : 0;
		$data['maxDeferral'] = isset($rkp['plan_data']->preMax) ? $rkp['plan_data']->preMax : 0;
                $data['maxDeferralPercent'] = isset($rkp['plan_data']->maxDeferralPercent) ? $rkp['plan_data']->maxDeferralPercent : 0;
                $data['maxDeferralDollar'] = isset($rkp['plan_data']->maxDeferralAmount) ? $rkp['plan_data']->maxDeferralAmount : 0;
		$data['hasRoth'] = isset($rkp['plan_data']->rkpExtras->hasroth) ? $rkp['plan_data']->rkpExtras->hasroth : 0;
		$data['matchInformation'] = isset($rkp['plan_data']->matchinformation) && !empty($rkp['plan_data']->matchinformation) ? $rkp['plan_data']->matchinformation : FALSE;
		$data['currentYTDPre'] = isset($rkp['plan_data']->contributionbysource) && isset($rkp['plan_data']->contributionbysource->pre) ? $rkp['plan_data']->contributionbysource->pre : 0;
		$data['currentYTDPost'] = isset($rkp['plan_data']->contributionbysource) && isset($rkp['plan_data']->contributionbysource->post) ? $rkp['plan_data']->contributionbysource->post : 0;
		$data['currentYTDMatch'] = isset($rkp['plan_data']->contributionbysource) && isset($rkp['plan_data']->contributionbysource->match) ? $rkp['plan_data']->contributionbysource->match : 0;

		$data['currentYTDTotal'] = $data['currentYTDPre'] + $data['currentYTDPost'];
		//$data['currentYTDTotal'] = $data['currentYTDPre'];
		$data['currentDeferral'] = $data['preCurrent'] + $data['rothCurrent'];   
		$data['currentDeferralFlatDollar'] = 0;
		//$data['currentDeferral'] = $data['preCurrent'];   
		$data['complete'] = 0;
		$data['textTemplate'] = 1;
                if ($this->session->get("DATA") == null)
                {
                    $data['confirmationCode'] = '';
                }
                else
                {
                    $data['confirmationCode'] = $this->session->get("DATA")['confirmationCode'];
                }
		$data['emailTemplate'] = "1101_PWV_EMAIL";
        $data['user_id'] = isset($rkp['plan_data']->user_id) ? $rkp['plan_data']->user_id : null;
		$data['success'] = TRUE;
		$data['message'] = '';
	}

    private function _setPlans(&$data) {
        $plansRkpSession = $this->session->get('plansRkp');
        $rkp = $this->session->get('rkp');
        $request = $this->session->get("REQUEST");

        $repository = $this->em->getRepository('classesclassBundle:plansRkp');
        $plansRkp = $repository->findOneBy(array('partnerid' => $request['partner_id'], 'recordkeeperPlanid' => $request['planid']),array('id' => 'DESC'));

        $repository = $this->em->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $rkp['plan_id']));

        $limits = $this->connection->executeQuery('SELECT * FROM serverConfig402Limits')->fetch();

        $data['partner_id'] = $request['partner_id'];
        $data['planid'] = $request['planid'];
        $data['retireAge'] = isset($plansRkp->retirementAge) && $plansRkp->retirementAge > 0 ? $plansRkp->retirementAge : 65;
        $data['retireAgeOriginal'] = $data['retireAge'];
        if (!empty($plansRkpSession->retirementAge))
        {
            $data['retireAge'] = $plansRkpSession->retirementAge;
        }
        $data['redirectUrl'] = isset($plansRkp->vWiseSSOUrl) ? $plansRkp->vWiseSSOUrl : '';
        $data['loginUrl'] = isset($plansRkp->participantLoginUrl) ? $plansRkp->participantLoginUrl : '';
        $data['logoUrl'] = isset($plansRkp->logo) ? $plansRkp->logo : '';
        $data['powerviewUrl'] = isset($plansRkp->powerViewUrl) ? $plansRkp->powerViewUrl : '';
        $data['powerViewEnabled'] = isset($plansRkp->powerViewEnabled) ? $plansRkp->powerViewEnabled : FALSE;
        $data['irioEnabled'] = isset($plansRkp->irioEnabled) ? $plansRkp->irioEnabled : FALSE;
        $data['smartPlanEnabled'] = isset($plansRkp->smartPlanEnabled) ? $plansRkp->smartPlanEnabled : FALSE;
        $data['cta'] = strtolower($this->session->get("cta"));
        $data['sid'] = $request['sid'];
        $data['uniqid'] = $rkp['participant_uid'];
        $data['retired'] = ($data['age'] >= $data['retireAge']) ? TRUE : FALSE;
        $data['currentplan'] = $currentplan;
        $data['limit402g'] = $limits['limit402g'];
        $data['lang'] = $request['lang'] ? $request['lang'] : 'en';
    }

	private function _setAPR(&$data) {
		$repository = $this->em->getRepository('classesclassBundle:Apr');
		$apr = $repository->findOneBy(array('age' => $data['retireAge'], 'year' => date('Y')));

		$data['apr'] = isset($apr->apr) ? $apr->apr : 0;
		$data['apr_join'] = isset($apr->apr_join) ? $apr->apr_join : 0;
	}

	private function _setInputData(&$data) {
        $tmp = $this->session->get('DATA');

		$data['salary'] = isset($tmp['salary']) ? $tmp['salary'] : 0;
		$data['salaryFrequency'] = isset($tmp['salaryFrequency']) ? $tmp['salaryFrequency'] : 0;
		$data['retireAge'] = isset($tmp['salaryRetireAge']) && $tmp['salaryRetireAge'] > 0 ? $tmp['salaryRetireAge'] : $data['retireAge'];
		$data['selectedDeferral'] = isset($tmp['selectedDeferral']) ? $tmp['selectedDeferral'] : 0;
		$data['otherBalance'] = isset($tmp['otherBalance']) ? $tmp['otherBalance'] : 0;

		$data['spouseBalance'] = isset($tmp['spouseBalance']) ? $tmp['spouseBalance'] : 0;
		$data['spouseIncome'] = isset($tmp['spouseIncome']) ? $tmp['spouseIncome'] : 0;
		$data['spouseAge'] = isset($tmp['spouseAge']) ? $tmp['spouseAge'] : 0;

		/* Update Balance */
		$data['balance'] += $data['otherBalance'];
                
		/* Update currentDeferral and maxDeferral (after salary updated) */
		if ($data['partDeferralSetting'] == 'dollar' && $data['salary']) {
			$data['currentDeferralFlatDollar'] = $data['currentDeferral'];
			$data['currentDeferral'] = $this->convertFlatDollarToPercent($data['salary'], $data['currentDeferral'], $data['salaryFrequency']);

            $data['maxDeferralFlatDollar'] = $data['maxDeferral'];
            $data['maxDeferral'] = $this->convertFlatDollarToPercent($data['salary'], $data['maxDeferral'], $data['salaryFrequency']);
		}
        $data['rothNew'] = isset($tmp['rothNew']) ? $tmp['rothNew'] : 0;
        $data['pretaxNew'] = isset($tmp['pretaxNew']) ? $tmp['pretaxNew'] : 0;
        $data['overrideDeferral'] = isset($tmp['overrideDeferral']) ? $tmp['overrideDeferral'] : 0;
	}

	private function _calcDeposit(&$data) {
		$data['deposit'] = ($data['salary']) * ($data['currentDeferral'] / 100);
		$data['depositOne'] = ($data['salary']) * (($data['currentDeferral'] + 1) / 100);
		$data['depositTwo'] = ($data['salary']) * (($data['currentDeferral'] + 2) / 100);
		$data['depositThree'] = ($data['salary']) * (($data['currentDeferral'] + 3) / 100);
	}

	public function _calcMatch(&$data) {
		/* Calculate matching */
		$data['maxMatch'] = 0;
		$data['matchSelf'] = 0;
		$data['matchEmployer'] = 0;
		$data['maxMatchDollar'] = 0;
		$data['compensationTotal'] = 0;
		$data['payFrequency'] = 0;

		if ($data['matchInformation']) {
			$data['maxMatchDollar'] = $data['matchInformation']->maxmatchdollar;
			$data['compensationTotal'] = $data['matchInformation']->compensationtotals * 4;
			$data['payFrequency'] = $data['matchInformation']->cycle;
			if (!empty($data['matchInformation']->matchdetails)) {

				$matchDollar = 0;
				$prevStep = 0;
				$matchStep = 0;

				for($i = 1; $i <= 5; $i++) {
					$key = 'matchstep' . $i;
					$step = isset($data['matchInformation']->matchdetails->$key) ? $data['matchInformation']->matchdetails->$key : 0;
					$key = 'matchrate' . $i;
					$rate = isset($data['matchInformation']->matchdetails->$key) ? $data['matchInformation']->matchdetails->$key : 0;
					$data['maxMatch'] = $step > 0 ? $step : $data['maxMatch'];

					if ($step > 0 && $rate > 0) {

						$matchStep = min($data['currentDeferral'], $step);
						$data['matchEmployer'] += ($data['salary']) * (($step - $prevStep) / 100) * ($rate / 100);
						if ($matchStep >= $prevStep) {
							$matchDollar += ($data['salary']) * (($matchStep - $prevStep) / 100) * ($rate / 100);
						}
						$prevStep = $step;
					}
				}

				$matchDollar = $data['maxMatchDollar'] > 0 ? min($data['maxMatchDollar'], round($matchDollar)) : round($matchDollar);
				$data['matchEmployer'] = $data['maxMatchDollar'] > 0 ? min($data['maxMatchDollar'], $data['matchEmployer']) : $data['matchEmployer'];
				$data['matchSelf'] = ($data['salary']) * ($data['maxMatch'] / 100);
				$data['deposit'] += $matchDollar;
				$data['depositOne'] += $matchDollar;
				$data['depositTwo'] += $matchDollar;
				$data['depositThree'] += $matchDollar;
			}
		}
	}

	private function _calcProjected(&$data) {
		$data['projectedCurrentBalance'] = $this->_getProjectedCurrentBalance($data['balance'], $data['age'], $data['retireAge']);
		$data['projectedCurrentBalanceSpouse'] = $this->_getProjectedCurrentBalance($data['spouseBalance'], $data['age'], $data['retireAge']);
		$data['projectedCurrentBalance'] += $data['projectedCurrentBalanceSpouse'];

		$apr = $data['projectedCurrentBalanceSpouse'] > 0 ? $data['apr_join'] : $data['apr'];
		$data['projectedAnnualDeposit'] = $this->_getProjectedAnnualDepositGrowth($data['deposit'], $data['age'], $data['retireAge']);
		$data['projectedIncome'] = ($data['projectedCurrentBalance'] + $data['projectedAnnualDeposit']);
		$data['projectedMonthlyIncome'] = ($data['projectedCurrentBalance'] + $data['projectedAnnualDeposit']) / ($apr);

		/* 1% increase */
		//$data['projectedAnnualDepositOne'] = $this->_getProjectedAnnualDepositGrowth($data['deposit'] * 1.01, $data['age'], $data['retireAge']);
		$data['projectedAnnualDepositOne'] = $this->_getProjectedAnnualDepositGrowth($data['depositOne'], $data['age'], $data['retireAge']);
		$data['projectedMonthlyIncomeOne'] = ($data['projectedCurrentBalance'] + $data['projectedAnnualDepositOne']) / ($apr);

		/* 2% increase */
		//$data['projectedAnnualDepositTwo'] = $this->_getProjectedAnnualDepositGrowth($data['deposit'] * 1.02, $data['age'], $data['retireAge']);
		$data['projectedAnnualDepositTwo'] = $this->_getProjectedAnnualDepositGrowth($data['depositTwo'], $data['age'], $data['retireAge']);
		$data['projectedMonthlyIncomeTwo'] = ($data['projectedCurrentBalance'] + $data['projectedAnnualDepositTwo']) / ($apr);

		/* 3% increase */
		//$data['projectedAnnualDepositThree'] = $this->_getProjectedAnnualDepositGrowth($data['deposit'] * 1.03, $data['age'], $data['retireAge']);
		$data['projectedAnnualDepositThree'] = $this->_getProjectedAnnualDepositGrowth($data['depositThree'], $data['age'], $data['retireAge']);
		$data['projectedMonthlyIncomeThree'] = ($data['projectedCurrentBalance'] + $data['projectedAnnualDepositThree']) / ($apr);

		$data['depositMax'] = ($data['salary']) * ($data['maxMatch'] / 100); /* Your contribution */
		$data['depositMax'] += ($data['salary']) * ($data['maxMatch'] / 100); /* Company matching */

		$data['projectedAnnualDepositMax'] = $this->_getProjectedAnnualDepositGrowth($data['depositMax'], $data['age'], $data['retireAge']);
		$data['projectedIncomeMax'] = ($data['projectedCurrentBalance'] + $data['projectedAnnualDepositMax']);
		$data['projectedMonthlyIncomeMax'] = ($data['projectedCurrentBalance'] + $data['projectedAnnualDepositMax']) / ($apr);

		$data['selectedAmount'] = $data['selectedDeferral'] > 0 ? ($data['selectedDeferral'] == 1 ? $data['projectedMonthlyIncomeOne'] : ($data['selectedDeferral'] == 2 ? $data['projectedMonthlyIncomeTwo'] : $data['projectedMonthlyIncomeThree'])) : 0;
		$data['diffDeferral'] = $data['maxMatch'] - $data['currentDeferral'];
        $data['newDeferral'] = ($data['selectedDeferral'] > 0) ? ($data['currentDeferral'] + $data['selectedDeferral']) : $data['maxMatch'];
        if (!empty($data['overrideDeferral'])) {
            $data['newDeferral'] = $data['partDeferralSetting'] == 'dollar' ? $this->convertFlatDollarToPercent($data['salary'], $data['overrideDeferral'], $data['salaryFrequency']) : $data['overrideDeferral'];
        }
	}

	private function _calcOther(&$data) {
		$data['overLimit402g'] = ($data['currentYTDTotal'] >= $data['limit402g']) ? TRUE : FALSE;
		//$data['frontLoaded'] = ($data['deposit'] >= $data['currentYTDMatch']) ? TRUE : FALSE;
		$data['frontLoaded'] = FALSE;
                for ($i = 1; $i <= 3;$i++)
                {
                    $this->_calcIncreaseShow($data, $i);
                }
                $data['maxOut'] = !$data['showIncreased'];
	}
        
        private function _calcIncreaseShow(&$data,$increase)
        {
            if ($data['partDeferralSetting'] == 'dollar')
            {
                $this->_calcFlatDollars($data);
                $deferralRate = $data['currentDeferralFlatDollar'] + ($data['salary'] * $increase/100);
                $deferralMax = $data['maxDeferralFlatDollar'];
            }
            else
            {
                $deferralRate = $data['currentDeferral'] + $increase;
                $deferralMax = $data['maxDeferral'];
            }
            $data['showIncreaseDeferralRate'.$increase] = $deferralRate;
            $data['showIncreaseMaxRate'.$increase] = $deferralMax;
            $data['showIncrease'.$increase]= !($deferralRate > $deferralMax);
            $data['showIncreased'] =!empty($data['showIncreased']) || $data['showIncrease'.$increase];
        }
	private function _calcFlatDollars(&$data) {
            $data['maxMatchFlatDollar'] = 0;
            $data['diffDeferralFlatDollar'] = 0;
            $data['newDeferralFlatDollar'] = 0;
            $data['selectedDeferralFlatDollar'] = 0;

            if ($data['partDeferralSetting'] == 'dollar') {
                $data['maxMatchFlatDollar'] = $this->convertPercentToFlatDollar($data['salary'], $data['maxMatch'], $data['salaryFrequency']);
                $data['diffDeferralFlatDollar'] = $this->convertPercentToFlatDollar($data['salary'], $data['diffDeferral'], $data['salaryFrequency']);
                $data['selectedDeferralFlatDollar'] = intval(round($this->convertPercentToFlatDollar($data['salary'], $data['selectedDeferral'], $data['salaryFrequency'])/$this->payroll[$data['salaryFrequency']]));
                $data['newDeferralFlatDollar'] = empty($data['overrideDeferral']) ? $data['selectedDeferralFlatDollar'] + $data['currentDeferralFlatDollar'] : $data['overrideDeferral'];                
            }
	}


	public function getData($checkDiff = 0) {
        $rkp = $this->session->get('rkp');
		$data = [
			'success' => FALSE,
			'message' => isset($rkp) ? $rkp['error'] : 'Error!',
		];

        if (isset($rkp['plan_data']) && !is_null($rkp['plan_data']) && isset($rkp['plan_data']->dateOfBirth)) {
			$this->_setProfile($data);
			$this->_setPlans($data);
			$this->_setAPR($data);
			$this->_setInputData($data);

			$this->_calcDeposit($data);
			$this->_calcMatch($data);
			$this->_calcProjected($data);

			$this->_calcOther($data);
            $data['new'] = $this->_calculateNew($data);

            $data['projectedIncome'] = $data['new']['discountedValueOfFutureBalance_0'];
            $data['projectedMonthlyIncome'] = $data['new']['projectedMonthlyRetirement_0'];
            $data['projectedMonthlyIncomeOne'] = $data['new']['projectedMonthlyRetirement_0.01'];
            $data['projectedMonthlyIncomeTwo'] = $data['new']['projectedMonthlyRetirement_0.02'];
            $data['projectedMonthlyIncomeThree'] = $data['new']['projectedMonthlyRetirement_0.03'];
            if ($data['selectedDeferral'] != 0) {
                $data['selectedAmount'] = $data['new']['projectedMonthlyRetirement_0.0'.$data['selectedDeferral']];
            }
            $data['projectedIncomeMax'] = $data['new']['discountedValueOfFutureBalance_'.$data['diffDeferral']/100];
            $data['projectedMonthlyIncomeMax'] = $data['new']['projectedMonthlyRetirement_'.$data['diffDeferral']/100];
			$this->_calcFlatDollars($data);
            
            $sessionData = $data;

			$numbers = array(
                'balance','projectedCurrentBalance','projectedIncome','projectedIncomeMax','projectedMonthlyIncome',
                'projectedMonthlyIncomeOne','projectedMonthlyIncomeTwo','projectedMonthlyIncomeThree','projectedMonthlyIncomeMax',
                'currentYTDPre','currentYTDMatch','currentYTDPost','currentYTDTotal','salary', 'selectedAmount',
                'matchSelf', 'matchEmployer', 'maxMatchFlatDollar', 'diffDeferralFlatDollar', 'newDeferralFlatDollar',
                'currentDeferralFlatDollar', 'maxDeferralFlatDollar'
            );

			foreach ($numbers as $key) {
				$data[$key . 'Raw'] = $data[$key];
                $sessionData[$key . 'Raw'] = $data[$key];
                if ($key != "salary")
                {
                    $data[$key] = number_format($data[$key]);
                }
			}
            $this->session->set("DATA",$sessionData);
		}
		if ($data['diffDeferral'] <= 0 && $data['cta'] == "powerview" && $checkDiff) {
			$data['success'] = FALSE;
			$data['message'] = 'Error! Deferring less than current amount';
		}
		return $data;
	}
        //new calculations
        function _projectedAnnualDepositGrowth($data,$deferralRate)
        {
            $salaryTimesDeferralRate = bcmul($data['salary'],$deferralRate,$data['precision']) + $data['matchTotal'];
            $interestIncreased = bcpow($data['interest'] + 1,$data['ageDiff'],$data['precision']);
            $depositGrowthIncreased = bcpow($data['depositGrowth'] + 1,$data['ageDiff'],$data['precision']);
            $interestMinusDepositGrowth = $data['interest'] - $data['depositGrowth'];
            $interestLeftOver = $interestIncreased  - $depositGrowthIncreased;
            $salaryTimesDeferralRateTimesInterestLeftOver = bcmul($salaryTimesDeferralRate,$interestLeftOver,$data['precision']);
            return bcdiv($salaryTimesDeferralRateTimesInterestLeftOver,$interestMinusDepositGrowth,$data['precision']);
        }
        function _increaseDepositGrowth($data,$number)
        {
            $depositAmountUpdated = bcmul($data['currentDepositAmount'],$number,$data['precision']);
            $interestUpdated = bcpow($data['interest'] + 1,$data['ageDiff']-1,$data['precision']);
            $top = bcmul($depositAmountUpdated,$interestUpdated,$data['precision']);
            return bcdiv($top,$data['interest'],$data['precision']);
        }
        function _matchAmountTier1($data)
        {
            $min = min(array($data['deferralRate'],$data['matchTier1Percent']));
            $return = bcmul($min,$data['salary'],$data['precision']);
            $return = bcmul($return,$data['matchTier1Cap'],$data['precision']);
            return $return;
        }
        function _matchAmountTier2($data)
        {
            $min = min(array($data['deferralRate'],$data['matchTier2Percent']))-$data['matchTier1Percent'];
            $return = bcmul($min,$data['salary'],$data['precision']);
            $return = bcmul($return,$data['matchTier2Cap'],$data['precision']);
            if ($return <= 0) {
                $return = 0;
            }
            return $return;
        }
        function _discountedValueOfFutureBalance($data, $increase)
        {
            $inflationIncreased = bcpow(1 + $data['inflationDiscount'],$data['ageDiff'],$data['precision']);
            return bcdiv($data['projectedAccountBalance_'.$increase],$inflationIncreased,$data['precision']);
        }
        function _aprUsed($data)
        {
            if ($data['projectedCurrentBalanceAtRetirementAgeSpouse'] > 0)
            return $data['jointApr'];
            return $data['apr'];
        }
        function _projectedMonthlyRetirement(&$data,$increase = 0)
        {
            $defRate = $data['deferralRate'];
            if ($data['partDeferralSetting'] == "dollar")
            {
                $defRate =  $defRate * $this->payroll[$data['salaryFrequency']];
            }
            $data['projectedAnnualDepositGrowth_'.$increase] = $this->_projectedAnnualDepositGrowth($data,$defRate + $increase);
            $data['projectedAccountBalance_'.$increase] = $data['projectedCurrentBalanceSum'] + $data['projectedAnnualDepositGrowth_'.$increase];
            $data['discountedValueOfFutureBalance_'.$increase] = $this->_discountedValueOfFutureBalance($data,$increase);
            $data['projectedMonthlyRetirement_'.$increase] = $data['discountedValueOfFutureBalance_'.$increase]/$data['aprUsed'];
        }      
        function _calculateNew($data2)
        {
            $data['salaryFrequency'] = $data2['salaryFrequency'];
            $data['partDeferralSetting'] = $data2['partDeferralSetting'];
            $data['precision'] = $this->precision;
            $data['currentAge'] = (int)$data2['age'];
            $data['retirementAge'] = (int)$data2['retireAge'];
            $data['rothContribution'] = (float)$data2['rothCurrent'] / 100.0;
            $data['preContribution'] = (float)$data2['preCurrent'] / 100.0;
            if ($data2['partDeferralSetting'] == 'dollar')
            {
                $data['preContribution']  = $this->convertFlatDollarToPercent((float)$data2['salary'], (float)$data2['preCurrent'], $data2['salaryFrequency'],false);
                $data['rothContribution']  = $this->convertFlatDollarToPercent((float)$data2['salary'], (float)$data2['rothCurrent'], $data2['salaryFrequency'],false);
            }
            $data['spouseBalance'] = (float)$data2['spouseBalance'];
            $data['spouseIncome'] = (float)$data2['spouseIncome'];
            $data['spouseAge'] = $data['currentAge']; // spouse age matches enrollee age
            $data['salary'] = (float)$data2['salary'];
            $data['currentBalance'] = (float)$data2['balance'];
            $data['interest'] = 0.07;
            $data['otherRetirementIncome'] = (float)$data2['otherBalance'];
            $data['depositGrowth'] = 0.03;
            $data['maxMatchAmount'] = 0;
            $data['matchTier1Percent'] = 0;
            $data['matchTier1Cap'] = 0;
            $data['matchTier2Percent'] = 0;
            $data['matchTier2Cap'] = 0;
            
            if (isset($data2['matchInformation']->maxmatchdollar)) {
                $data['maxMatchAmount'] = (float)$data2['matchInformation']->maxmatchdollar;
            }
            if (isset($data2['matchInformation']->matchdetails))
            {                
                $matchDetails = $data2['matchInformation']->matchdetails;
                if ($data['salary'] == 0) {
                    $data['salary'] = (float)$data2['matchInformation']->compensationtotals * 4;
                }
                $data['matchTier1Percent'] = (float)$matchDetails->matchstep1 / 100.0;
                $data['matchTier1Cap'] = (float)$matchDetails->matchrate1 / 100.0;
                $data['matchTier2Percent'] = (float)$matchDetails->matchstep2 / 100.0;
                $data['matchTier2Cap'] = (float)$matchDetails->matchrate2 / 100.0;
            }
                        
            $data['inflationDiscount'] = 0.03;
            $data['apr'] = $data2['apr'];
            $data['jointApr'] = $data2['apr_join'];
            $data['retirementAge'] = $data['retirementAge'] + 0.5;
            $data['currentAgePlusOne'] = $data['currentAge'] + 1.0;
            $data['deferralRate'] = $data['rothContribution']  + $data['preContribution'];
            $data['matchAmountTier1'] = $this->_matchAmountTier1($data);
            $data['matchAmountTier2'] = $this->_matchAmountTier2($data);
            $data['matchTotal'] = min($data['matchAmountTier1'] + $data['matchAmountTier2'], $data['maxMatchAmount']);
            $data['currentBalanceTotal'] = $data['currentBalance'];
            $data['ageDiff'] = $data['retirementAge'] - $data['currentAgePlusOne'];
            $data['currentDepositAmount'] = bcmul($data['salary'], $data['deferralRate'], $data['precision']) + $data['matchTotal'];
            $data['projectedCurrentBalanceAtRetirementAge'] = bcmul($data['currentBalanceTotal'], bcpow(1 + $data['interest'], $data['ageDiff'], $data['precision']), $data['precision']);
            $data['projectedCurrentBalanceAtRetirementAgeSpouse_Right'] = (bcpow(1 + $data['interest'], $data['retirementAge'] - $data['spouseAge'],$data['precision']));
            $data['projectedCurrentBalanceAtRetirementAgeSpouse'] = bcmul($data['spouseBalance'], $data['projectedCurrentBalanceAtRetirementAgeSpouse_Right'], $data['precision']);
            $data['projectedCurrentBalanceSum'] = $data['projectedCurrentBalanceAtRetirementAge'] + $data['projectedCurrentBalanceAtRetirementAgeSpouse'];
            $data['aprUsed'] = $this->_aprUsed($data);
            $this->_projectedMonthlyRetirement($data);
            $this->_projectedMonthlyRetirement($data,0.01);
            $this->_projectedMonthlyRetirement($data,0.02);
            $this->_projectedMonthlyRetirement($data,0.03);
            $this->_projectedMonthlyRetirement($data,$data2['diffDeferral'] / 100.0);
            return $data;
        }        
}
