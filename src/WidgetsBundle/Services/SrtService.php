<?php

namespace WidgetsBundle\Services;
use Symfony\Component\HttpFoundation\Session\Session;

class SrtService
{
	var $api_url;
	var $session_id;

    public function __construct(Session $session)
	{
		$this->api_url = 'https://lab14srt.dev.schwab.com/SDDAv2/pas.asmx';
        $param = $session->get('REQUEST');
		$this->session_id = $param['sid'];
	}

	private function _callLegacy($apiCall, $params = '') 
	{
        $url = $this->api_url . '/ExecuteLegacyAPICall?sessionID=' . $this->session_id . '&apiCall=' . $apiCall . '&parameters=' . $params;

		try 
		{
			$rawXml = file_get_contents($url, FALSE);

			if ($rawXml) 
			{
				$pattern = '/(&lt;boolean)(&gt;)(\w+)(&lt;\/boolean&gt;)/ie';
                $replacement = '"$1 value=\"" . strtolower("$3") . "\"$2$4"';
                $processed = preg_replace($pattern, $replacement, $rawXml);

                $data = wddx_deserialize($processed);

                return $data;
			}
		}
		catch (\Exception $e)
		{
			/* TODO: Error Handling */
		}

		return FALSE;

	}

	private function _call($apiCall, $params = '') 
	{
		$url = $this->api_url . '/' . $apiCall . '?sessionId=' . $this->session_id;

		try 
		{
			$rawXml = file_get_contents($url, FALSE);

			if ($rawXml) 
			{
                $xml = new \SimpleXMLElement($rawXml);

				return $xml;
			}
		}
		catch (\Exception $e)
		{
			/* TODO: Error Handling */
		}

		return FALSE;
	}

	public function getDeferral() 
	{
		$result = array();
		$apiCall = 'srtGetDeferralCurrent';
        $params = '_defpcttype=0%26_catchupind=0';
		$data = $this->_callLegacy($apiCall, $params);

		if ($data !== FALSE) {
			$result['defsource'] = array();
			$result['defsourcedesc'] = array();
			$result['defcurrdefval'] = array();
			$result['defpendingdefval'] = array();
			$result['defnewdefval'] = array();
			$result['defmindefval'] = array();
			$result['defmaxdefval'] = array();
			$result['defsourcetype'] = array();
			$result['defcatchupcurrdefval'] = array();
			$result['defcatchuppnddefval'] = array();
			$result['defsourcevruid'] = array();

			foreach ($data as $varName => $var) {
				if ($varName == "return_data") {
					foreach ($var as $recordset) {
						foreach ($recordset as $fieldName => $field) {
							foreach ($field as $string) {
								switch ($fieldName) {
									case "source": $result['defsource'][] = $string; break;
									case "sourcedesc": $result['defsourcedesc'][] = $string; break;
									case "currdefval": $result['defcurrdefval'][] = $string; break;
									case "pendingdefval": $result['defpendingdefval'][] = $string; break;
									case "newdefval": $result['defnewdefval'][] = $string; break;
									case "mindefval": $result['defmindefval'][] = $string; break;
									case "maxdefval": $result['defmaxdefval'][] = $string; break;
									case "sourcetype": $result['defsourcetype'][] = $string; break;
									case "catchupcurrdefval": $result['defcatchupcurrdefval'][] = $string; break;
									case "catchuppnddefval": $result['defcatchuppnddefval'][] = $string; break;
									case "sourcevruid": $result['defsourcevruid'][] = $string; break;
								}
							}
						}
					}
				}
			}
		}

		return $result;
	}

	public function getDeferralOptions() 
	{
		$result = array();
		$apiCall = 'srtGetDeferralOptions';

		$data = $this->_callLegacy($apiCall);

		if ($data !== FALSE) {
			$result['deferralsetting'] = '';
			$result['catchupdeferralsetting'] = '';
			$result['allowcatchupdeferralcontrib'] = '';
			$result['blockdeferralupdate'] = '';
			$result['participant50yearsold'] = '';

			foreach ($data as $varName => $var) {
				if ($varName == 'return_data') {
					foreach ($var as $recordset) {
						foreach ($recordset as $fieldName => $field) {
							foreach ($field as $string) {
								switch ($fieldName) {
									case 'deferralsetting': $result['deferralsetting'] = $string; break;
									case 'catchupdeferralsetting': $result['catchupdeferralsetting'] = $string; break;
									case 'allowcatchupdeferralcontrib': $result['allowcatchupdeferralcontrib'] = $string; break;
									case 'participant50yearsold': $result['participant50yearsold'] = $string; break;
									case 'blockdeferralupdate': $result['blockdeferralupdate'] = $string; break;
								}
							}
						}
					}
				}
			}
		}

		return $result;
	}

	public function getPlan() 
	{
		$result = array();
		$apiCall = 'srtGetPartSynoptic';

		$data = $this->_callLegacy($apiCall);

		if ($data !== FALSE) {
			foreach ($data as $varName => $var) {
				if ($varName == 'return_data') {
					foreach ($var as $recordset) {
						foreach ($recordset as $fieldName => $field) {
							foreach ($field as $string) {
								$result[$fieldName] = $string;
							}
						}
					}
				}
			}
		}

		return $result;
	}

	public function getComp() 
	{
		$result = array();
		$apiCall = 'getCompensationAndMatchProvisions';

		$data = $this->_call($apiCall);

		if ($data !== FALSE) {
			foreach ($data as $varName => $var) {
				if ($varName == 'CompensationHoursTracking') {
					foreach ($var as $fieldName => $string) {
						$result[$fieldName] = (string)$string;
					}
				}
			}
		}

		return $result;
	}
}
