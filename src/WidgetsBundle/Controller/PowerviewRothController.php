<?php

namespace WidgetsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class PowerviewRothController extends Controller {
    protected function init() {
        $this->directory = $this->get('WidgetService')->isMobile() ? 'mobile' : 'desktop';
        $this->get('WidgetService')->setLang();
    }
    
    public function indexAction($request) {
        $this->init();
        $data = $this->get('session')->get("DATA");
        $hasMatching = $data['maxMatch'] > 0;
        return $this->render("WidgetsBundle:PowerViewRoth/{$this->directory}:index.html.twig", array('data' => $data, 'hasMatching' => $hasMatching));
    }
    
    public function increaseContributionAction() {
        $this->init();
        $data = $this->get('session')->get("DATA");
        return $this->render("WidgetsBundle:PowerViewRoth/{$this->directory}:increaseContribution.html.twig", array('data' => $data));
    }
    
    public function selectContributionAction() {
        $this->init();
        $data = $this->get('session')->get("DATA");
        return $this->render("WidgetsBundle:PowerViewRoth/{$this->directory}:selectContribution.html.twig", array('data' => $data));
    }
    
    public function allocateContributionAction() {
        $this->init();
        $data = $this->get('session')->get("DATA");
        return $this->render("WidgetsBundle:PowerViewRoth/{$this->directory}:allocateContribution.html.twig", array('data' => $data));
    }
    
    public function allocateConfirmAction() {
        $this->init();
        $data = $this->get('session')->get("DATA");
        return $this->render("WidgetsBundle:PowerViewRoth/{$this->directory}:allocateConfirm.html.twig", array('data' => $data));
    }
    
    public function confirmationConnectedAction() {
        $this->init();
        $data = $this->get('session')->get("DATA");
        return $this->render("WidgetsBundle:PowerViewRoth/{$this->directory}:confirmationConnected.html.twig", array('data' => $data));
    }
    
    public function confirmationCtaAction() {
        $this->init();
        $data = $this->get('session')->get("DATA");
        return $this->render("WidgetsBundle:PowerViewRoth/{$this->directory}:confirmationCta.html.twig", array('data' => $data));
    }
    
    public function confirmationThankYouAction() {
        $this->init();
        return $this->render("WidgetsBundle:PowerViewRoth/{$this->directory}:confirmationThankYou.html.twig");
    }
    
    
}
