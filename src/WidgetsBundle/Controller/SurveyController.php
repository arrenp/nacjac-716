<?php
namespace WidgetsBundle\Controller;

use classes\classBundle\Entity\surveyEmployerGroups;
use classes\classBundle\Entity\SurveyEmployerPlans;
use classes\classBundle\Entity\surveyEmployers;
use classes\classBundle\Entity\surveyResults;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class SurveyController extends Controller {
    

    
    public function indexAction($request, Request $realRequest){
        
        $this->get('translator')->setLocale($request['lang']);
        
        $em              = $this->getDoctrine()->getManager();
        $session         = $realRequest->getSession();
        $session->set("partner_id",$request['partner_id']);
        $partId          = $session->get('partner_id');
        
        $employersGroupsQuery = $em->createQuery('SELECT p FROM classesclassBundle:surveyEmployerGroups p INDEX BY p.id WHERE p.partnerId = :partnerId ORDER BY p.displayOrder ASC');
        $employersGroupsQuery->setParameter('partnerId', $partId);
        $employersGroups = $employersGroupsQuery->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        $parents = array_filter($employersGroups, function($var) { return empty($var['parentId']); });
        $children = array_diff_key($employersGroups, $parents);
        
        foreach ($parents as &$parent) {
            $parent['children'] = [];
        }
        
        foreach ($children as $child) {
            $parentId = $child['parentId'];
            if (isset($parents[$parentId])) {
                $parents[$parentId]['children'][] = $child;
            }
        } 
        
        return $this->render('WidgetsBundle:Survey:index.html.twig', array('employersGroups' => $parents, 'partner' => $partId));
    }
    
    function getTypesAction(Request $request){
        $surveyEmployerId = $request->get('surveyEmployerId');
        
        $repository = $this->getDoctrine()->getRepository(SurveyEmployerPlans::class);
        $qb = $repository->createQueryBuilder('p');
        $qb->select('p.type, p.planid');
        $qb->where('p.surveyEmployerId = :surveyEmployerId');
        $qb->setParameter('surveyEmployerId', $surveyEmployerId);
        
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return new JsonResponse($data);
    }
    
    public function getEmployerGroupAction(Request $request) {
        $groupRepo = $this->getDoctrine()->getRepository(surveyEmployerGroups::class); 
        
        $qb = $groupRepo->createQueryBuilder('p')->where('p.id = :id')->setParameter('id', $request->get('id'));
        $group = $qb->getQuery()->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        if (!is_null($group)) {
            $employerRepo = $this->getDoctrine()->getRepository(surveyEmployers::class); 
            $qb = $employerRepo->createQueryBuilder('p');
            $qb->where('p.employerGroupsTableId = :employerGroupsTableId');
            $qb->setParameter('employerGroupsTableId', $request->get('id'));
            $qb->orderBy('p.employerName', 'ASC');
            $group['surveyEmployers'] = $qb->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        }
        return new JsonResponse($group);
    }
    
    function hashArray(&$object,$fieldname)
    {
        foreach ($object as $key => $value)
        {
           if (is_int($key))
           {
            $object[$value[$fieldname]] = $value;
            unset ($object[$key]); 
           }
        }        
    }    
    
    function saveResultsAction(Request $request){   
        $session = $request->getSession();
        $em                    = $this->getDoctrine()->getManager();
        $newaccount            = new surveyResults();
        $newaccount->partnerId = $session->get('partner_id');
        $newaccount->planId    = $request->request->get("plan"); 
        $newaccount->results   = $request->request->get("json");
        $newaccount->dateStamp = new \DateTime('now');
        $newaccount->userIp    = $request->server->get('REMOTE_ADDR');
        
        
        $partId    = $request->request->get("partner"); 
        $planId    = $request->request->get("plan");        
        
        $em->persist($newaccount);
        $em->flush();
        
        $redirect = $this->container->getParameter('app_domain');
        $appurl = $this->container->getParameter('appurl');
        return new response($appurl.'?id='.$planId.'&partner_id='.$partId."&type=smartenroll");
        
        
    }
    
    
}