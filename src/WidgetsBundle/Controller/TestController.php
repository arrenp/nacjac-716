<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller
{
    public function irioAction(Request $request)
    {
        return $this->render('WidgetsBundle:Test:irio.html.twig');
    }

    public function powerviewAction(Request $request)
    {
        return $this->render('WidgetsBundle:Test:powerview.html.twig');
    }

    public function surveyAction(Request $request)
    {
        return $this->render('WidgetsBundle:Test:survey.html.twig');
    }    
    
    public function nationalsurveyAction(Request $request)
    {
        return $this->render('WidgetsBundle:Test:national-survey.html.twig');
    }

    public function libraryAction(Request $request)
    {
        return $this->render('WidgetsBundle:Test:library.html.twig');
    }    
    public function multiAction(Request $request)
    {
        return $this->render('WidgetsBundle:Test:multi.html.twig');
    }      
}
