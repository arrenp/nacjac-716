<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ApiBundle\Controller\IrioController as IrioController2;
class IrioController extends IrioController2
{
    public function indexAction($request)
    {
		$tmp = Request::createFromGlobals();

		$session = $this->get('session');
		$this->get('WidgetService')->setLang();
		if (!$session->get('DATA')) {
			return $this->render('WidgetsBundle:Default:loading.html.twig', array('session' => $session));
		} else {
			$tpl = [
				'data' => $session->get('DATA'),
                               ];
                        $rkp = $session->get('rkp');                        
                        $tpl['jsSessionId'] =$this->sid;
                        $this->saveSessionToTable();
			return $this->render('WidgetsBundle:Irio:srt/index.html.twig', $tpl);
		}
    }
}


