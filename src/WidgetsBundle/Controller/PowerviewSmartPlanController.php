<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PowerviewSmartPlanController extends Controller
{
    public function indexAction($request)
    {
		$session = $this->get('session');
		$lang = $this->get('WidgetService')->setLang();
		$mobile = $this->get('WidgetService')->isMobile();
	
		$tpl = [
			'data' => $session->get('DATA'),
			'lang' => $lang,
		];

		if ($mobile) {
			return $this->render('WidgetsBundle:PowerViewSmartPlan:mobile.html.twig', $tpl);
		}
		else {
			return $this->render('WidgetsBundle:PowerViewSmartPlanNew:index.html.twig', $tpl);
		}
    }
}
