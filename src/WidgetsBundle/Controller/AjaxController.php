<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\CTAStat;
use classes\classBundle\Classes\mailgunEngine;
use ApiBundle\Controller\IrioController;
use classes\classBundle\Entity\profilesContributions;
class AjaxController extends IrioController
{
    public function initAction()
    {
        $this->get('WidgetService')->getRKPData();
        return $this->calculateAction();
    }

    public function init2Action()
    {
        return $this->calculateAction();
    }

	public function validateContributionAction(Request $request) {
		$session = $this->get('session');
		$error = [];
		$status = FALSE;

		$fields = [
			'other_balance' => 'Other retirement account balance', 
			'spouse_balance' => "Spouse's retirement account balance", 
			'spouse_income' => "Spouse's annual income",
			'spouse_age' => "Spouse's Age",
		];

		foreach($fields as $key => $value) {
			$$key = preg_replace('/\D+/', '', $request->request->get($key, 0));

			if ($$key) {
				if (!is_numeric($$key) || $$key <= 0) {
					$error[] = $value . ' is not valid';
				}
			}
		}

		if (!$error) {
			$tmp = $session->get('DATA');
			$status = TRUE;

			$tmp['selectedDeferral'] = $request->request->get('contribution', '');
			$tmp['otherBalance'] = $other_balance;
			$tmp['spouseBalance'] = $spouse_balance;
			$tmp['spouseIncome'] = $spouse_income;
			$tmp['spouseAge'] = $spouse_age;
                        foreach ($tmp as &$currentTemp)
                        {
                            if (is_string($currentTemp))
                            {
                                $currentTemp = strip_tags($currentTemp);
                            }
                        }

			$session->set('DATA', $tmp);
                        $this->saveSessionToTable();
			return $this->calculateAction();
		}
		else {
			$data = [
				'success' => $status,
				'message' => implode("\n", $error),
			];

			return new response(json_encode($data));
		}
	}

	public function saveRedirectAction(Request $request) 
	{
		$session = $this->get('session');
                $request = $session->get("REQUEST");

		$tmp = $session->get('DATA');
		$tmp['complete'] = TRUE;
		$session->set("DATA",$tmp);
                if (strtolower($session->get("cta")) != "powerview")
                {
                    $this->saveStatsAction();
                }
                $this->saveSessionToTable();
		$this->saveStatsAction();

		$data = [
                        'success' => TRUE,
                        'message' => '',
		];
                
        return new response(json_encode($data));
	}

	public function allowSessionAction() {
		$session = $this->get('session');
		//$session->start();
		return $this->render('WidgetsBundle:Default:cookies.html.twig');
	}

	public function saveMatchAction(Request $request) 
	{
		$session = $this->get('session');
		$tmp = $this->get('FormulaService')->getData();
                $rkp = $session->get('rkp');

		$pretax = $request->request->get('pretax', null);
		$roth = $request->request->get('roth', null);

                if ($tmp['postContributions'] && empty($session->get("REQUEST")['testing'])) {
			$success = TRUE;
			$postPct = $tmp['postCurrent'];
			$maxMatch = $tmp['partDeferralSetting'] == 'dollar' ? $tmp['maxMatchFlatDollar'] : $tmp['maxMatch'];
			$maxValue = $tmp['selectedDeferral'] + $tmp['currentDeferral'];
			$maxValue = $tmp['partDeferralSetting'] == 'dollar' ? $tmp['newDeferralFlatDollarRaw'] : $maxValue;

			if ($tmp['hasRoth'] && $tmp['preCurrent'] > 0 && $tmp['rothCurrent'] > 0) {
                $common = $session->get('common');
                $common['roth'] = true;
                $session->set("common",$common);

				if ($tmp['selectedDeferral'] > 0) { // IRIO
					if ($pretax + $roth != $maxValue) {
						$success = FALSE;
						$data = [
							'message' => 'Invalid values',
							'success' => FALSE,
						];
					}
					else {
						$contributionPct = $pretax;
						$rothPct = $roth;
					}
				}
				else { // PowerView
					if ($pretax + $roth != $maxMatch) {
						$success = FALSE;
						$data = [
							'message' => 'Invalid values',
							'success' => FALSE,
						];
					}
					else {
						$contributionPct = $pretax;
						$rothPct = $roth;
					}
				}
			}
			else {
				if ($tmp['selectedDeferral'] > 0) { // IRIO
					if ($tmp['hasRoth']) {
						$contributionPct = $tmp['preCurrent'];
						$rothPct = $maxValue;
					}
					else {
						$contributionPct = $maxValue;
						$rothPct = $tmp['rothCurrent'];
					}
				}
				else { // PowerView
					if ($tmp['hasRoth']) {
						$contributionPct = $tmp['preCurrent'];
						$rothPct = $maxMatch;
					}
					else {
						$contributionPct = $maxMatch;
						$rothPct = $tmp['rothCurrent'];
					}
				}
			}

			if ($success) {
				if ($rkp['plan_data']->partDeferralSetting == 'dollar') {
					$c = array(
						'contribution_mode' => 'DEFERDLR',
						'C_PreTaxContributionPct' => null,
						'C_PreTaxContributionValue' => $contributionPct,
						'C_RothContributionPct' => null,
						'C_RothContributionValue' => $rothPct,
						'C_PostTaxContributionPct' => null,
						'C_PostTaxContributionValue' => $postPct,
					);
				}
				else {
					$c = array(
						'contribution_mode' => 'DEFERPCT',
						'C_PreTaxContributionPct' => $contributionPct,
						'C_PreTaxContributionValue' => null,
						'C_RothContributionPct' => $rothPct,
						'C_RothContributionValue' => null,
						'C_PostTaxContributionPct' => $postPct,
						'C_PostTaxContributionValue' => null,
					);
				}
                                
                                $c['C_ACAoptOut'] = $rkp['plan_data']->partACAStatus == "1";
                                $acaPass = true;
                                $skipAca = false;
				$session->set('contributions', $c);
                                if ($rkp['plan_data']->partACAStatus == "1" && empty($session->get("widgetHeader")))
                                {
                                    $acaRequest = json_decode($this->get('spe.app.rkp')->postACAOptOut());
                                }
                                else
                                {
                                    $skipAca = true;
                                }
                                if ($skipAca || $acaRequest->success)
                                {
                                    if (empty($session->get("widgetHeader")))
                                    {
                                        $data = $this->get('spe.app.rkp')->postContributions();
                                    }
                                    else
                                    {
                                        $data = $this->get('spe.app.rkp')->postContributionsWidget();
                                    }
                                    $dataObject = json_decode($data);
                                }
                                else
                                {
                                    $acaPass = false;
                                }
				if (isset($dataObject->success) && $acaPass)
				{
					$dataArray['complete'] = $dataObject->success;
					if ($dataObject->success)
					$dataArray['confirmationCode'] =$dataObject->confirmation;
                                        else
                                        $this->get("WidgetService")->sendErrorNotification();
				}
				else
                                {
				$dataArray['complete'] = 0;
                                $this->get("WidgetService")->sendErrorNotification();
                                }
			
				$dataArray['maxMatch'] = $contributionPct;
				
				$dataArray = array_merge((array)$session->get('DATA'),$dataArray);
				$session->set("DATA",$dataArray);
                                $this->saveSessionToTable();
				$data = array_merge((array)$dataObject, $dataArray);
			}
			else {
				$dataArray['success'] = FALSE;
				$dataArray['complete'] = 0;
			
				$dataArray['maxMatch'] = $contributionPct;
				$dataArray = array_merge((array)$session->get('DATA'),$dataArray);
				$session->set("DATA",$dataArray);
				$data = array_merge((array)$dataObject, $dataArray);
			}
		}
		else {
            $tmp['success'] = $tmp['postContributions'];
            $tmp['complete'] = 1;
            $tmp['confirmationCode'] = $tmp['confirmation'] =  "test test test";                    
            $session->set("DATA",$tmp);
            $this->saveSessionToTable();
            $data = $tmp;
		}
        
        $data['successOrPostContributionsOff'] = $data['success']  || !$data['postContributions'];
		$this->saveStatsAction();
		return new response(json_encode($data));
	}

    public function calculateAction($checkDiff = 0)
    {
		$data = $this->get('FormulaService')->getData($checkDiff);
                if (!$data['success'])
                $this->get("WidgetService")->sendErrorNotification();

	    return new response(json_encode($data));


		/*
		$request = Request::createFromGlobals();
		$callback = $request->query->get('callback');

		if ($callback) { 
			$response = new JsonResponse($data, 200, array());
			$response->setCallback($callback);

			return $response;
		}
		else {
			return new response(json_encode($data));
		}
		*/
    }

	public function closeSessionAction() {
		$session = $this->get('session');
		$session->set('DATA', '');

		$data = [
			'success' => TRUE,
			'message' => '',
		];

        return new response(json_encode($data));
	}

    public function saveSalaryAction(Request $request)
    {
        $session = $this->get('session');
        $tmp = $session->get("DATA");
		$salary = preg_replace('/\D+/', '', $request->request->get('salary', 0));
		$salaryFrequency = $request->request->get('salary-frequency', '');
		$salaryRetireAge = $request->request->get('salary-retireage', '');

        $lang = $this->get("WidgetService")->setLang();
        $translator = $this->get('translator');
        $rkp = $session->get('rkp');
        $reloadIrioHtml = false;
        if (!empty($session->get("contributionAmountChoice")))
        {
            $contributionType = $request->request->get("contributionType");
            $deferral = str_replace(",","",$request->request->get("deferral",''));
            if (!is_numeric($deferral) || (float) $deferral < 0  )
            {
                $data = [
                        'success' => FALSE,
                        'message' => "deferral_amount",
                ];            
                return new response(json_encode($data));
            }    
            else
            {
                $reloadIrioHtml = $contributionType !== $rkp['plan_data']->partDeferralSetting;
                $deferralField = $session->get("deferralOptionType")."Current";
                $rkp['plan_data']->preCurrent = "";
                $rkp['plan_data']->$deferralField = (float) $deferral;
                $rkp['plan_data']->partDeferralSetting = $contributionType;
                $rkp['plan_data']->preMax = $tmp['maxDeferral'.ucfirst($contributionType)];
                $tmp['partDeferralSetting'] = $contributionType;
                $session->set("rkp",$rkp);
            }
        }
        if (!is_numeric($salary) || $salary <= 0) {

			$data = [
				'success' => FALSE,
				'message' => $translator->getMessages($lang)['widget']['enter_valid_number']
			];

			return new response(json_encode($data));
		} else if (!in_array($salaryFrequency, ['ANNUALLY', 'MONTHLY', 'SEMI-MONTHLY', 'BI-WEEKLY', 'WEEKLY'])) {

            $data = [
                'success' => FALSE,
                'message' => $translator->getMessages($lang)['widget']['enter_valid_frequency']
            ];

            return new response(json_encode($data));
        }
		else {
            if (!empty($session->get('header')))
            {
                $tmp['salary'] = $salary;
                $tmp['salaryFrequency'] = $salaryFrequency;
                $tmp['salaryRetireAge'] = $salaryRetireAge;
                $session->set("DATA", $tmp);
                $this->saveSessionToTable();
                $data = $this->get('FormulaService')->getData();
                $data['viewOnly'] = $session->get('header')->get('viewOnly') == 'yes';
                $data['irioHtml'] = $this->render('WidgetsBundle:Irio:irio.js.twig')->getContent().$this->render('WidgetsBundle:Irio:iriobody.html.twig',["data" => $data])->getContent();
                $data['reloadIrioHtml'] = $reloadIrioHtml;
                return new response(json_encode($data));
            }
            else{            
            $tmp['salary'] = $salary;
            $tmp['salaryFrequency'] = $salaryFrequency;
            $tmp['salaryRetireAge'] = $salaryRetireAge;
            $plansRkp = $this->get("session")->get('plansRkp');
            if (!empty($tmp['salaryRetireAge']))
            {
                if ($plansRkp == null)
                {
                    $plansRkp = new \stdClass();
                }
                $plansRkp->retirementAge = $salaryRetireAge;
                $session->set("plansRkp",$plansRkp);
            }
            $session->set("DATA", $tmp);
            return $this->calculateAction(1);
            }
		}
        //return new Response(json_encode($session->get('DATA')));
    }

    public function saveStatsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $methods = $this->get('GeneralMethods');
        $session = $this->get('session');    
        $rkp = $session->get('rkp');
        $request = $session->get("REQUEST");
        $data = $session->get('DATA');
        if (isset($data['currentplan']))
        $currentplan = $data['currentplan'];
        if (isset($currentplan))
        {
            $savedData = new CTAStat();        
            $savedData->sessionid = isset($request['sid']) ? $request['sid']: "";
            $savedData->userid = $currentplan->userid;
            $savedData->planid = $currentplan->id;
            $savedData->ipaddress = $methods->get_client_ip();
            $savedData->uniqid = isset($rkp['participant_uid']) ? $rkp['participant_uid']: "";
            $savedData->firstName = (isset($rkp['plan_data']) && $rkp['plan_data']->firstName != null) ? $rkp['plan_data']->firstName: "";
            $savedData->lastName = (isset($rkp['plan_data']) && $rkp['plan_data']->lastName != null) ? $rkp['plan_data']->lastName: "";
            $savedData->email = (isset($rkp['plan_data']) && $rkp['plan_data']->email != null) ? $rkp['plan_data']->email: "";
            $savedData->age = isset($data['age']) ? $data['age']: 0;
            $savedData->salary = isset($data['salary']) ? $data['salary']: 0;
            $savedData->predeferral = isset($data['currentDeferral']) ? $data['currentDeferral']: 0;
            $savedData->curdeferral = isset($data['newDeferral']) ? $data['newDeferral']: 0;
            $savedData->employerMatch = isset($data['currentYTDMatch']) ? $data['currentYTDMatch']: 0;
            $savedData->maxMatch = isset($data['maxMatch']) ? $data['maxMatch']: 0;
            $savedData->postContributions = isset($data['postContributions']) ? $data['postContributions']: 0;
            if (isset($data['currentDeferral']) && $data['currentDeferral'] > 0)
            $savedData->deferral = 1;
            else
            $savedData->deferral = 0; 
            $savedData->selectedDeferral = 0;
            if (isset($data['spouseBalance']))
            $savedData->totalSpouseAccountBalance = $data['spouseBalance'];
            $savedData->requestData = "";
            $savedData->responseData = "";
            $savedData->createDate = new \dateTime();
            $savedData->modifiedDate = new \dateTime();
            $savedData->complete = isset($data['complete']) ? $data['complete']: 0;
            if ($session->get("cta") != null)
            $savedData->cta = $session->get("cta");
            else
            $savedData->cta = "Undefined";
            if (isset($data['otherBalance']))
            $savedData->otherRetirementAccountBalance = $data['otherBalance'];
            if (isset($data['spouseIncome']))
            $savedData->totalSpouseAnnualIncome = $data['spouseIncome'];
            if (isset($data['spouseAge']))
            $savedData->spouseAge = $data['spouseAge'];
            if (isset($data['diffDeferral']))
            $savedData->diffDeferral = $data['diffDeferral'];
            if (isset($data['balance']))
            $savedData->balance = $data['balanceRaw'];     
            if (isset($data['projectedMonthlyIncome']))
            $savedData->projectedMonthlyIncome = $data['projectedMonthlyIncomeRaw'];  
            if (isset($data['projectedIncome']))
            $savedData->projectedIncome = $data['projectedIncomeRaw']; 
            
            $savedData->json = json_encode($session->all());
            
            $em->persist($savedData);
            $em->flush();  
        }
    }

    
    public function pingAction() {
        $response = $this->get('spe.app.rkp')->ping();
        return new Response (json_encode(json_decode($response,true)));
    }
    
    public function emailTemplateAction(Request $request)
    {
        $this->get("WidgetService")->setLang();
        $name = $request->request->get("name","");
        $toText = $request->request->get("text","");
        $toEmail = $request->request->get("email","");
        $session = $this->get('session');
        $rkp = $session->get('rkp');
        $data = $this->get('FormulaService')->getData();
        if ($data['partDeferralSetting'] == "dollar")
        {
            $data['currentDeferral'] = "$".$data['currentDeferralFlatDollar'];
            $data['newDeferral'] = "$".$data['newDeferralFlatDollar'];
        }
        else
        {
            $data['currentDeferral'] = $data['currentDeferral']."%";
            $data['newDeferral'] = $data['newDeferral']."%";
        }
		$request_session = $session->get("REQUEST");
		$error = [];
		$status = FALSE;

        if ($toText && !preg_match('/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/', $toText)) {
			$error[] = 'Phone number is not valid';
		}
        if ($toEmail && !filter_var($toEmail, FILTER_VALIDATE_EMAIL)) {
			$error[] = 'E-mail is not valid';
		}

		if (!isset($rkp['plan_id'])) {
			$error[] = 'Plan is not valid';
		}

		if (!$error) {
                        if (isset($data['currentplan']))
			$currentplan = $data['currentplan'];
                        else
                        $currentplan = null;

			$repository = $this->getDoctrine()->getRepository('classesclassBundle:plansRkp');
			/* TODO: Change partner_id and planid to userid and planid */
			$plansRkp = $repository->findOneBy(array('partnerid' => $request_session['partner_id'], 'recordkeeperPlanid' => $request_session['planid']),array('id' => 'DESC'));
			//$plansRkp = $repository->findOneBy(array('userid' => $currentplan->userid, 'planid' => $rkp['plan_id']),array('id' => 'DESC'));
            
            $accountRepository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
            $account = $accountRepository->findOneBy(['partnerid' => $request_session['partner_id']]);
            $fromName = trim($account->irioFromEmailName);
            
            $subject = $data['planName'];
            $fromEmail = 'info@smartplanenterprise.com';
            if (!empty($fromName)) {
                $fromEmail = $fromName . ' <' . $fromEmail . '>';
            }
            $isHR = $isForm = false;                       
            if ($plansRkp != null)
            {
                if (!empty($plansRkp->planName))
                {
                    $subject = $plansRkp->planName;
                }
                if (!empty($plansRkp->emailAddressFrom))
                {
                    $fromEmail = $plansRkp->emailAddressFrom;     
                }     
                $isHR = !empty($plansRkp->hrEmail);
                $isForm = !empty($plansRkp->deferralFormLocation);
            }
			if ($currentplan != null) {
                                $suffix = !empty($session->get('header')) ? "_AMERITAS": "";
				if ($data['postContributions']) {
					/* Plan allows for on line deferrals */
					$html =  $this->render('WidgetsBundle:Templates:1101_PWV_EMAIL'.$suffix.'.html.twig',array("plan" => $currentplan,"rkp" => $rkp,"data" => $data))->getContent();  
					$text =  $this->render('WidgetsBundle:Templates:1101_PWV_TEXT.txt.twig',array("data" => $data))->getContent();  
				}
				else {
					if (!$isHR && !$isForm) {
						/* Plan doesn’t allow for on-line contribution changes – no HR contact, no form */
						$html =  $this->render('WidgetsBundle:Templates:1102_PWV_EMAIL'.$suffix.'.html.twig',array("plan" => $currentplan,"rkp" => $rkp,"data" => $data, 'plansRkp' => $plansRkp))->getContent();  
						$text =  $this->render('WidgetsBundle:Templates:1102_PWV_TEXT.txt.twig',array('plansRkp' => $plansRkp,"data" => $data))->getContent();  
					}
					elseif ($isHR && !$isForm) {
						/* Plan doesn’t allow for on-line contribution changes –HR contact, no form */
						$html =  $this->render('WidgetsBundle:Templates:1103_PWV_EMAIL.html.twig',array("plan" => $currentplan,"rkp" => $rkp,"data" => $data, 'plansRkp' => $plansRkp))->getContent();  
						$text =  $this->render('WidgetsBundle:Templates:1103_PWV_TEXT.txt.twig',array('plansRkp' => $plansRkp,"data" => $data))->getContent();  
					}
					elseif (!$isHR && $isForm) {
						/* Plan doesn’t allow for on-line contribution changes – no HR contact, have form */
						$html =  $this->render('WidgetsBundle:Templates:1104_PWV_EMAIL.html.twig',array("plan" => $currentplan,"rkp" => $rkp,"data" => $data, 'plansRkp' => $plansRkp))->getContent();  
						$text =  $this->render('WidgetsBundle:Templates:1104_PWV_TEXT.txt.twig',array('plansRkp' => $plansRkp,"data" => $data))->getContent();  
					}
					elseif ($isHR && $isForm) {
						/* Plan doesn’t allow for on-line contribution changes – HR contact, have form */
						$html =  $this->render('WidgetsBundle:Templates:1105_PWV_EMAIL.html.twig',array("plan" => $currentplan,"rkp" => $rkp,"data" => $data, 'plansRkp' => $plansRkp))->getContent();  
						$text =  $this->render('WidgetsBundle:Templates:1105_PWV_TEXT.txt.twig',array('plansRkp' => $plansRkp,"data" => $data))->getContent();  
					}
				}
			}

			if (isset($html) &&  $toEmail != "" && $data['emailTemplate'])
			{
				$params['subject'] = $subject;
				$params['to'] = $toEmail;
				$params['html'] = $html;
				$params['from'] = $fromEmail;
                                $currentplan = $data['currentplan'];
                                $params['o:tag'] = array($data['cta'],$data['cta']."_userid_".$currentplan->userid,$data['cta']."_planid_".$currentplan->planid);  
                                $email = new mailgunEngine();
                                $email->sendEmail($params);
			}
			
			if (isset($text) && $toText != "")
			{
				$params['to'] = preg_replace('/\D+/', '', $toText);
				$params['text'] = $text;
				unset($params['from']);

				$this->get("mailService")->sendText($params);
			}

			$status = TRUE;
		}

		$data = [
			'success' => $status,
			'message' => implode("\n", $error),
		];

		return new response(json_encode($data));
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $request = Request::createFromGlobals();       
        if (!empty($request->get("sid")) && empty($this->get('session')->get("DATA")))
        {
            $this->sid = $request->get("sid");
            $this->readSessionFromTable();
        }
    }
    
    public function saveNewDeferralAction(Request $request) {
        $session = $this->get('session');
        $tmp = $session->get('DATA');

        $tmp['overrideDeferral'] = $request->request->get('overrideDeferral', 0);

        $session->set('DATA', $tmp);

        return $this->calculateAction();
    }
    
    public function saveContributionAllocationAction(Request $request) {
        $session = $this->get('session');
        $tmp = $session->get('DATA');

        $tmp['pretaxNew'] = $request->request->get('pretax', 0);
        $tmp['rothNew'] = $request->request->get('roth', 0);

        $session->set('DATA', $tmp);

        return $this->calculateAction();
    }
    
    public function submitContributionAllocationAction(Request $request) {
        $session = $this->get('session');
        $tmp = $session->get('DATA'); 
        $rkp = $session->get('rkp');
        $deferrals = (array) $rkp['plan_data']->deferrals;
        
        $pretax = $request->get("pretax", 0);
        $roth = $request->get("roth", 0);
        
        
        $newDeferral = $tmp['overrideDeferral'];
        if ($tmp['postContributions'] && empty($session->get("REQUEST")['testing'])) {
            $success = TRUE;
            
            if ($pretax > $rkp['plan_data']->preMax || $roth > $rkp['plan_data']->rothMax) {
                $success = FALSE;
                $data = ['message' => 'Over','success' => FALSE];
            }
            
            if ($pretax + $roth != $newDeferral) {
                $success = FALSE;
                $data = ['message' => 'Not Equal','success' => FALSE];
            }
            
            if ($success) { 
                $common = $session->get('common');
                if (isset($deferrals[1])) {
                    $common['roth'] = true;
                }
                $session->set("common",$common);
                
                if ($rkp['plan_data']->partDeferralSetting == 'dollar') {
                    $c = array(
                        'contribution_mode' => 'DEFERDLR',
                        'C_PreTaxContributionPct' => null,
                        'C_PreTaxContributionValue' => $pretax,
                        'C_RothContributionPct' => null,
                        'C_RothContributionValue' => $roth,
                        'C_PostTaxContributionPct' => null,
                        'C_PostTaxContributionValue' => $tmp['postCurrent'],
                    );
                }
                else {
                    $c = array(
                        'contribution_mode' => 'DEFERPCT',
                        'C_PreTaxContributionPct' => $pretax,
                        'C_PreTaxContributionValue' => null,
                        'C_RothContributionPct' => $roth,
                        'C_RothContributionValue' => null,
                        'C_PostTaxContributionPct' => $tmp['postCurrent'],
                        'C_PostTaxContributionValue' => null,
                    );
                }

                $c['C_ACAoptOut'] = 1;
                
                $acaPass = true;
                $skipAca = false;
                $session->set('contributions', $c);
                if ($rkp['plan_data']->partACAStatus == "1")
                {
                    $acaRequest = json_decode($this->get('spe.app.rkp')->postACAOptOut());
                }
                else
                {
                    $skipAca = true;
                }
                

                if ($skipAca || $acaRequest->success)
                {
                    $data = $this->get('spe.app.rkp')->postContributions();
                    $dataObject = json_decode($data); 
                }
                else
                {
                    $acaPass = false;
                }	

                if (isset($dataObject->success) && $acaPass)
                {
                    $dataArray['complete'] = $dataObject->success;
                    if ($dataObject->success) {
                        $dataArray['confirmationCode'] =$dataObject->confirmation;
                    }
                    else {
                        $this->get("WidgetService")->sendErrorNotification();
                    }
                    
                }
                else
                {
                    $dataArray['complete'] = 0;
                    $this->get("WidgetService")->sendErrorNotification();
                }

                $dataArray = array_merge((array)$session->get('DATA'),$dataArray);
                $session->set("DATA",$dataArray);
                $data = array_merge((array)$dataObject, $dataArray);
            }
            else {
                $dataArray['success'] = FALSE;
                $dataArray['complete'] = 0;
                
                $dataArray = array_merge((array)$session->get('DATA'),$dataArray);
                $session->set("DATA",$dataArray);
                $data = array_merge((array)$dataObject, $dataArray);                
            }
        }
        else {
            $tmp['success'] = $tmp['postContributions'];
            $tmp['complete'] = 1;
            $tmp['confirmationCode'] = $tmp['confirmation'] =  "test test test";                    
            $session->set("DATA",$tmp);
            $data = $tmp;
        }
        $data['successOrPostContributionsOff'] = $data['success']  || !$data['postContributions'];
        $this->saveStatsAction();
        return new response(json_encode($data));
    }
    
    public function getProjectedMonthlyIncomeAction(Request $request) {
        $session = $this->get('session');
        $data = $session->get('DATA');
        
        if ($data['partDeferralSetting'] == 'dollar') {
            $deferralRate = $this->get("FormulaService")->convertFlatDollarToPercent($data['salary'],$request->get("contribution"),$data['salaryFrequency'],false);
        }
        else {
            $deferralRate = $request->get("contribution") / 100;
        }
        
        $projectedMonthlyIncome = $this->get("FormulaService")->getProjectedMonthlyIncome($data, $deferralRate);
        return new Response($projectedMonthlyIncome);
    }

    public function saveRolloverWidgetOptionAction(Request $request) {

        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('id' => $id));

        if ($request->request->has("emailMe")) {
            $profiles->emailMe = $request->request->get("emailMe");
            $profiles->callMe = null;
            $profiles->callUs = null;
        }
        else if ($request->request->has("callMe")) {
            $profiles->callMe = $request->request->get("callMe");
            $profiles->emailMe = null;
            $profiles->callUs = null;
        }
        else {
            $profiles->callUs = 1;
            $profiles->emailMe = null;
            $profiles->callMe = null;
        }
        $em->flush();
        return new Response(json_encode(array('status' => 1)));
    }
}
