<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\surveyResults;
use Symfony\Component\HttpFoundation\Session\Session;
use classes\classBundle\Entity\videoLibStats;

class VideolibraryController extends Controller {
    
    public function indexAction(Request $request) {
        
        $session = $request->getSession();

        $partnerid = $request['partner_id'];
        $planidcode = $request['planid'];

        $session->set("partner_id",$partnerid);
        $videos = $this->getVideos($partnerid, $planidcode);
        
        return $this->render('WidgetsBundle:Videolibrary:index.html.twig', array('videos' => $videos, 'request' => $request));
    }
    
    public function getVideos($partnerid, $planidcode) {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $planid = $repository->findOneBy(array('partnerid' => $partnerid, 'planid' => $planidcode))->id;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryUsers');
        $libraryArray = $repository->findBy(array('planid' => $planid));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');
        $repositoryDirectories = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');

        $mainList = array();
        foreach ($libraryArray as $row) {
            $thisvideo = $repository->findOneBy(array('id' => $row->videoid));
            $thisvideo->directory = $repositoryDirectories->findOneBy(array('id' => $thisvideo->directoryid))->directory;
            $mainList[] = $thisvideo;
        }
        
        return $mainList;
    }
    
    public function insertAction (Request $vars) {
        
        $partnerID = $vars->request->get('partnerID');
        $videoPlayed = $vars->request->get('videoPlayed');
        $date      = new \DateTime("now");

        $em = $this->getDoctrine()->getManager();
        $newStat = new videoLibStats();
        $newStat->partnerID = $partnerID;
        $newStat->videoPlayed = $videoPlayed;
        $newStat->date = $date;
        $em->persist($newStat);
        $em->flush();

        return new Response('logged');

    }    

}

