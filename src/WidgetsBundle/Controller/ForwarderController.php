<?php
namespace WidgetsBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ApiBundle\Controller\IrioController;
class ForwarderController extends IrioController
{
    public function indexAction(Request $request)
    {
        $request = $request->query->all();        
        $session = $this->get('session'); 
        $type = trim(ucfirst($request['type']));
        $plan_id = trim($request['planid']);
        $partner_id = trim($request['partner_id']);
        $sid = trim($request['sid']);
		$request_session = $session->get("REQUEST");
                
		if (!isset($request['lang'])) {
			$request['lang'] = 'en';
		}                 
                $request_session['lang'] = $request['lang'];
		/* Additional error handling for type */
		if (!in_array($request['type'], array('powerview', 'irio', 'survey', 'nationalsurvey', 'videolibrary'))) {
			$request['type'] = 'powerview';
			return $this->redirect('/widgets/?' . http_build_query($request), 301);
		}
                
		$session = $this->get('session');
		$session->set('api_url',$this->container->getParameter( 'api_url' ));
		$session->set('REQUEST', $request);
        $this->get('WidgetService')->setLang();

		//echo json_encode($request);exit;
                $session->remove("plansRkp");

		if (in_array($type, ['Irio', 'Powerview'])) {
			if (!$session->get('DATA') || ($request != $request_session) || $session->get('DATA')['complete'] == TRUE) {
				$session->set('DATA', '');
				return $this->render('WidgetsBundle:Default:loading.html.twig');
			} 
			$rkp = $session->get('rkp');

			if (!isset($rkp['participant_uid'])) {
				return $this->render('WidgetsBundle:Default:loading.html.twig');
			}
			
			$em = $this->getDoctrine()->getManager();
			
			$repository = $em->getRepository('classesclassBundle:CTAStat');
			//$cta = $repository->findOneBy(array('uniqid' => $rkp['participant_uid']),array('id' => 'DESC'));
			$query = $em->createQuery('SELECT c.cta, c.complete FROM classesclassBundle:CTAStat c WHERE c.uniqid = :uniqid AND c.cta != :cta ORDER BY c.id DESC')
				->setParameter('uniqid', $rkp['participant_uid'])
				->setParameter('cta', 'Irio')
				->setMaxResults(1);
			$cta = $query->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
			$data = $session->get('DATA');

			$companyMatches = false;     
			$retired = false;
			$maxOut = false;
			$frontLoaded = false;
			$overLimit402g = false;
            $disallowedStatus = !in_array($data['partStatus'], array(751, 752));

			if ($data['maxMatch'] > 0 && ($data['currentDeferral'] <  $data['maxMatch'] ) ) {
                $companyMatches = true;
            }

			if ($data['age'] >= $data['retireAge']) {
                $retired = true;
            }
				
			if ($data['currentDeferral'] >= $data['maxDeferral']) {
                $maxOut = true;
            }

			if ($data['currentYTDTotal'] >= $data['limit402g']) {
                $overLimit402g = TRUE;
            }

			/*
			if ($data['deposit'] >= $data['currentYTDMatch']) 
				$frontLoaded = TRUE;
			*/

			if($type == 'Powerview') 
			{      
				if ($cta != null && $cta['complete'] == 1) {
                    if ($cta['cta'] == "Powerview") {
                        $type = "PowerviewSmartPlan";  
                    }
                    else if ($cta['cta'] == "PowerviewSmartPlan") {
                        $type = "PowerviewRedirect";
                    }
                    else if ($cta['cta'] == "PowerviewRedirect") {
                        $type = "PowerviewRoth";
                    }
                    else if ($cta['cta'] == "PowerviewRoth") {
                        $type = "PowerviewOffTheBench";
                    }
                    else if ($cta['cta'] == "PowerviewOffTheBench") {
                        $type = "Powerview";
                    }
				}

                $deferrals = (array) $rkp['plan_data']->deferrals;
                if ($type == 'PowerviewRoth' && ($maxOut || !isset($deferrals[1]) || $data['rothCurrent'] > 0 || $disallowedStatus)) {
                    $type = "PowerviewOffTheBench";
                }
                $enrollment = isset($rkp['plan_data']->rkpExtras->enrollment) && $rkp['plan_data']->rkpExtras->enrollment;
                if ($type == 'PowerviewOffTheBench' && ($maxOut || $enrollment || $data['preCurrent'] > 0 || $data['rothCurrent'] > 0 || $data['postCurrent'] > 0 || $disallowedStatus)) {
                    $type = "Powerview";
                } 
                if ($type == 'Powerview' && (!$companyMatches || $retired || $maxOut || $frontLoaded || $overLimit402g || $disallowedStatus || $data['partDeferralSetting'] == 'dollar')) {
                    $type = "PowerviewSmartPlan";  
                }
                if ($type == 'PowerviewSmartPlan' && (!$data['smartPlanEnabled'] || !in_array($data['partStatus'], array(751, 752, 753, 754, 755, 757, 758, 831)))) {
                    $type = "PowerviewRedirect";  
                }
			} 
			else if ($type == 'Irio') {
				
			}
		}

		if ($type == 'Survey') {
			/* TODO: Add error handling */
			
		}
                
		if ($type == 'videolibrary') {
			/* TODO: Add error handling */
		}                

		if (isset($request['override'])) {
			$type = $request['override'];
		}

        $session->set("cta",$type);
        $this->saveSessionToTable();
        return $this->forward("WidgetsBundle:".$type.":index", array("request" => $request)); 
    }
}
