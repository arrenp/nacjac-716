<?php

namespace Kinetik\KinetikBundle\Services;

class KinetikParticipantService extends KinetikBaseService {

    // profileId should only be populated only when customer has accepted advice

    protected function buildData($partnerParticipantId, $firstName, $lastName, $birthDate, $partnerPlanId, $profileId,
                                 $contributionPercent, $employerContributionPercent, $salary, $risk, $holdings) {
        $data = [
            "partnerParticipantId" => $partnerParticipantId,
            "firstName" => $firstName,
            "lastName" => $lastName,
            "birthDate" => $birthDate,
            "partnerPlanId" => $partnerPlanId,
            "profileId" => $profileId,
            "contributionPercent" => $contributionPercent,
            "employerContributionPercent" => $employerContributionPercent,
            "salary" => $salary,
            "risk" => $risk,
            "holdings" => $holdings,
        ];
        return $data;
    }

    public function addParticipant($partnerParticipantId, $firstName, $lastName, $birthDate, $partnerPlanId, $profileId,
                                   $contributionPercent, $employerContributionPercent, $salary, $risk, $holdings = null)
    {
        $data = $this->buildData($partnerParticipantId, $firstName, $lastName, $birthDate, $partnerPlanId, $profileId,
            $contributionPercent, $employerContributionPercent, $salary, $risk, $holdings);

        $response = $this->doCall("participant", "ADD", $data);
        return $response;
    }

    public function getParticipant($partnerParticipantId)
    {

        $response = $this->doCall("participant/{$partnerParticipantId}/portfolio", "GET");
        return $response;
    }

    public function updateParticipant($participantId, $data) { // dead 404
        $response = $this->doCall("participant/{$participantId}", "UPDATE", $data);
        return $response;
    }
    
}
