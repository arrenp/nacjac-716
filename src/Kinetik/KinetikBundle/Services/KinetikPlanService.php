<?php

namespace Kinetik\KinetikBundle\Services;

class KinetikPlanService extends KinetikBaseService {

    protected function buildData($partnerPlanId, $planName, array $funds) {
        $data = [
            "partnerPlanId" => $partnerPlanId,
            "planName" => $planName,
            "active" => true,
            "funds" => $funds,
        ];
        return $data;
    }

    public function getPlan($partnerPlanId)
    {
        $response = $this->doCall("plan/{$partnerPlanId}", "GET");
        return $response;
    }

    public function addPlan($partnerPlanId, $planName, array $funds)
    {
        $data = $this->buildData($partnerPlanId, $planName, $funds);

        $response = $this->doCall("plan", "ADD", $data);
        return $response;
    }

    public function updatePlan($partnerPlanId, $planName, array $funds) {

        $data = $this->buildData($partnerPlanId, $planName, $funds);

        $response = $this->doCall("plan/{$partnerPlanId}", "UPDATE", $data);
        return $response;
    }
    
}
