<?php

namespace Templates\TemplatesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TemplatesTemplatesBundle:Default:index.html.twig', array('name' => $name));
    }
}
