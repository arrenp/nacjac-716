<?php

namespace Templates\TemplatesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class ManageTemplateController extends Controller
{
    public function indexAction()
    {
        return $this->render('TemplatesTemplatesBundle:Default:index.html.twig');
    }

 public function headerAction(Request $request, $pagename)
    {
		$session = $request->getSession();


		$repository = $this->getDoctrine()->getRepository('classesclassBundle:manageUsers');


		$currentuser = $repository->findOneBy(array('id'=>$session->get("manageid")));

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsCategories');
		$promoDocsCategories = $repository->findBy(array(),array("orderid" => "ASC"));


		$session->save();
        return $this->render('TemplatesTemplatesBundle:Manage:header.html.twig',array('currentuser' => $currentuser,'pagename' => $pagename,'promoDocsCategories' => $promoDocsCategories));
    }

 public function footerAction()
    {
        return $this->render('TemplatesTemplatesBundle:Default:footer.html.twig');
    }

 public function lightboxheaderAction()
    {

        return $this->render('TemplatesTemplatesBundle:Default:lightboxheader.html.twig');
    }

}
