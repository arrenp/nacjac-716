<?php

namespace Templates\TemplatesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Permissions\AdvisersBundle\Classes\adviser;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use classes\classBundle\Entity\reportingSubscriptions;
use Shared\General\GeneralMethods;

class AdminTemplateController extends Controller
{

    public $sessionLength = 490000;

    public function indexAction()
    {
        return $this->render('TemplatesTemplatesBundle:Default:index.html.twig');
    }

    public function changeSessionExpiry($lifetime)
    {
        $cookie = session_get_cookie_params();
        setcookie(session_name(), session_id(), time() + $lifetime, $cookie['path'], $cookie['domain'], $cookie['secure'], $cookie['httponly']);
    }

    public function headerAction($pagename)
    {
        $this->changeSessionExpiry($this->sessionLength);
        $session = $this->get('adminsession');

        $currentTime = time();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $currentuser = $repository->findOneBy(array('id' => $session->clientid));
        if ($currentuser == null) $currentuser = $repository->findOneBy(array('id' => $session->adviserid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $session->planid));
        if ($currentplan != null && $session->adminPlanType != $currentplan->type)
        {
            $session->set("adminPlanType",$currentplan->type);
            $session->session->save();
        }
        $name = $currentuser->firstname . ' ' . $currentuser->lastname;
        $permissions = $this->get('rolesPermissions');

        $sectionPermissions = $permissions->permissions();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $currentaccount = $repository->findOneBy(array('id' => $session->userid));

        $manageNavAccounts = array();
        $manageNavAnalytics = array();
        $manageNavOutreach = array();
        $manageNavSales = array();
        $manageNavOther = array();
        $manageNavManagePermissions = array();

        $plansNavPlanFeatures = array();
        $plansNavOutreach = array();
        $plansNavAnalytics = array();
        $plansNavSponsorConnect = array();
        $plansNavPowerView = array();
        $plansNavIrio = array();
        
        $settingsNav = array();
        $settingsNavDefaults = array();
        $settingsNavTools = array();


        $statsClientServices = array();
        $statsArray = array();








        //stats
        $this->addSection($statsClientServices, "Plan Comparison", $this->generateUrl('_adminstats_index'), "StatsPlanComparison");
        $this->addSection($statsClientServices, "Support Tickets", $this->generateUrl('_adminstats_jira'), "StatsTickets");
        $this->addSection($statsClientServices, "Fiduciary Report", $this->generateUrl('_adminstats_FiduciaryReport'), "StatsFiduciaryReport");
        $this->addSection($statsClientServices, "Conversion", $this->generateUrl('_manage_reports_conversionreport'), "ReportsConversionReport");
        $this->addSection($statsClientServices, "User Activity", $this->generateUrl('_manage_reports_useractivity'), "ReportsUserActivityReport");
        $this->addSection($statsClientServices, "Performance", $this->generateUrl('_settings_performance'), "ReportsPerformanceReport");
        $this->addSection($statsClientServices, "Powerview Performance", $this->generateUrl('_manage_report_powerviewPerformance'), "ManageReportsPowerViewPerformance");
        $this->addSection($statsClientServices, "Powerview User Activity", $this->generateUrl('_manage_reports_powerviewUserActivity'), "ManageReportsPowerViewUserActivity");
        $this->addSection($statsClientServices, "Irio Performance", $this->generateUrl('_manage_report_irioPerformance'), "ManageReportsIrioPerformance");
        $this->addSection($statsClientServices, "Irio User Activity", $this->generateUrl('_manage_reports_irioUserActivity'), "ManageReportsIrioUserActivity");
        $this->addSection($statsClientServices, "Plans Added Report", $this->generateUrl('_manage_reports_plansAddedReport'), "ManageReportsPlansAdded");
        $this->addSection($statsClientServices, "Email Results By Contact", $this->generateUrl('_manage_reports_hubSpotEmailResults'), "ManageReportsEmailResults");
        $statsArray = $statsClientServices;



        //manage nav
        $this->addSection($manageNavAccounts, "Accounts List", $this->generateUrl('_manage_accounts'), "ManageAccounts");
        $this->addSection($manageNavAccounts, "Roles", $this->generateUrl('_manage_roles'), "ManageRoles");
        $this->addSection($manageNavAccounts, "Api", $this->generateUrl('_manage_api'), "ManageApi");
        $this->addSection($manageNavAccounts, "Communication Specialists", $this->generateUrl('_manage_groups',array("group" => "communicationSpecialist")), "ManageGroupsCommunicationSpecialist");
        $this->addSection($manageNavAccounts, "Account Managers",  $this->generateUrl('_manage_groups',array("group" => "accountManagers")), "ManageGroupsAccountManagers");
        $this->addSection($manageNavAccounts, "Smart Connect", $this->generateUrl('_manage_SponsorConnect'), "ManageSponsorConnect");
        

        $this->addSection($manageNavOutreach, "Plans Templates", $this->generateUrl('_manage_outreach'), "ManageOutreachPlans");
        $this->addSection($manageNavOutreach, "Vwise Templates", $this->generateUrl('_manage_outreach_vwise'), "ManageOutreachVwise");

        $this->addSection($manageNavAnalytics, "Profiles", $this->generateUrl('_manage_profiles'), "ManageAnalyticsProfiles");

        $this->addSection($manageNavOther, "Portal", $this->generateUrl('_manage_portal'), "ManagePortal");
        $this->addSection($manageNavOther, "Promo Docs", $this->generateUrl('_manage_promo_docs_manage'), "ManagePromoDocs");
        $this->addSection($manageNavOther, "Manage Media", $this->generateUrl('_manage_media'), "ManageMedia");
        $this->addSection($manageNavOther, "Export Database", $this->generateUrl('_manage_export_database'), "ExportDatabase");
        $this->addSection($manageNavOther, "Lincoln Keys", $this->generateUrl('_manage_LNCLNKeys'), "ManageLNCLNKeys");
        $this->addSection($manageNavOther, "Crons", $this->generateUrl('_manage_cron'), "ManageCron");
        $this->addSection($manageNavOther, "Library Videos", $this->generateUrl('_manage_plans_library'), "PlansLibraryVideos");
        $this->addSection($manageNavOther, "Funds Groups", $this->generateUrl('_manage_funds_groups'), "ManageFundGroups");
        $this->addSection($manageNavOther, "Translations", $this->generateUrl('_manage_translations_default'), "ManageTranslations");
        $this->addSection($manageNavOther, "Messaging Panes", $this->generateUrl('_manage_plans_messagingPanes'), "ManageMessagingPanes");
        $this->addSection($manageNavOther, "API", $this->generateUrl('_manage_API_ttl'), "ManageApiUI");

        $this->addSection($manageNavOther, "Risk Profile Creator", $this->generateUrl('_manage_risk_profile_creator'), "ManageRiskProfileCreator");

        $this->addSection($manageNavOther, "API Log", $this->generateUrl('_manage_API_log_ui'), "ManageApiUI");
        $this->addSection($manageNavOther, "Audit Log", $this->generateUrl('_manage_audit'), "AuditLog");
        $this->addSection($manageNavOther, "XML Log", $this->generateUrl('_manage_xml_raw_ui'), "ManageXmlRawUi");
        $this->addSection($manageNavOther, "AccountsRecordkeepersSrt UI", $this->generateUrl('_manage_accountsRecordkeepersSrt'), "ManageAccountsRecordkeepersSrtUI");
        $this->addSection($manageNavOther, "Kitchen Sink", $this->generateUrl('_manage_kitchenSink'), "ManageKitchenSink");
        $this->addSection($manageNavOther, "Translation Queue", $this->generateUrl('_manage_translationQueue'), "ManageTranslationQueue");
        $this->addSection($manageNavOther, "Survey", $this->generateUrl('_manage_survey'), "ManageSurvey");
        $this->addSection($manageNavOther, "Ameritas FTP Credentials", $this->generateUrl('_manage_ameritasftp'), "ManageAmeritasFTP");

        $this->addSection($manageNavSales, "View Promo Docs", $this->generateUrl('_manage_promo_docs_list_categories'), "ManageSalesPromoDocs");

        $manageArray = array();
        $manageArray = array_merge((array) $manageArray, (array) $manageNavAccounts, (array) $manageNavOutreach, $manageNavAnalytics, $manageNavOther, $manageNavSales);

        //settings nav
        //$this->addSection($settingsNavDefaults,"Investments",$this->generateUrl('_settings_investments'),"SettingsInvestments");
        $this->addSection($settingsNavDefaults, "Portfolios", $this->generateUrl('_settings_investments_portfolios'), "SettingsInvestmentsPortfolios");
        $this->addSection($settingsNavDefaults, "Funds", $this->generateUrl('_settings_investments_funds'), "SettingsInvestmentsFunds");
        $this->addSection($settingsNavDefaults, "Documents", $this->generateUrl('_settings_documents'), "SettingsDocuments");
        $this->addSection($settingsNavDefaults, "Contact", $this->generateUrl('_settings_contact'), "SettingsContact");
        $this->addSection($settingsNavDefaults, "Messaging Panes", $this->generateUrl('_settings_messaging'), "SettingsMessaging");
        $this->addSection($settingsNavDefaults, "Library", $this->generateUrl('_settings_library'), "SettingsLibrary");
        $this->addSection($settingsNavDefaults, "Investor Profile", $this->generateUrl('_settings_investor_profile'), "SettingsInvestorProfile");
        $this->addSection($settingsNavDefaults, "Autos", $this->generateUrl('_settings_autos'), "SettingsAutos");
        $this->addSection($settingsNavDefaults, "Interface", $this->generateUrl('_settings_interface'), "SettingsInterface");
        $this->addSection($settingsNavTools, "vAds", $this->generateUrl('_settings_vads'), "SettingsVads");
        $this->addSection($settingsNavTools, "Promotional Materials", $this->generateUrl('_settings_print_materials'), "SettingsPrintMaterials");
        $this->addSection($settingsNavTools, "Profiles", $this->generateUrl('_settings_profiles'), "SettingsAccountLevelReporting");
        $this->addSection($settingsNavTools, "Outreach", $this->generateUrl('_settings_outreach'), "SettingsEmail");
        $this->addSection($settingsNavTools, "Deployment Urls", $this->generateUrl('_settings_deploy'), "SettingsDeploymentUrls");
        //plans navs
        $this->addSection($plansNavPlanFeatures, "Plan Identification", $this->generateUrl('_plans_plan_identification'), "PlansPlanIdentification");
        $this->addSection($plansNavPlanFeatures, "Branding", $this->generateUrl('_plans_branding'), "PlansBranding");
        $this->addSection($plansNavPlanFeatures, "Modules", $this->generateUrl('_plans_modules'), "PlansModules");
        //$this->addSection($plansNavPlanFeatures,"Investments",$this->generateUrl('_plans_investments'),"PlansInvestments");
        $this->addSection($plansNavPlanFeatures, "Funds", $this->generateUrl('_plans_investments_funds'), "PlansInvestmentsFunds");
        $this->addSection($plansNavPlanFeatures, "Portfolios", $this->generateUrl('_plans_investments_portfolios'), "PlansInvestmentsPortfolios");
        $this->addSection($plansNavPlanFeatures, "Risk Based Funds", $this->generateUrl('_plans_investments_riskBasedFunds'), "PlansRiskBasedFunds");
        $this->addSection($plansNavPlanFeatures, "Risk Profile Configuration", $this->generateUrl('_plans_associate'), "PlansAssociate");
        $this->addSection($plansNavPlanFeatures, "Contact", $this->generateUrl('_plans_contact'), "PlansContact");
        $this->addSection($plansNavPlanFeatures, "Messaging Panes", $this->generateUrl('_plans_messaging'), "PlansMessaging");
        $this->addSection($plansNavPlanFeatures, "Interface", $this->generateUrl('_plans_interface'), "PlansInterface");
        $this->addSection($plansNavPlanFeatures, "Library", $this->generateUrl('_plans_library'), "PlansLibrary");
        $this->addSection($plansNavPlanFeatures, "Plan Deployment", $this->generateUrl('_plans_deploy'), "PlansDeploy");
        $this->addSection($plansNavPlanFeatures, "Advice", $this->generateUrl('_plans_advice'), "PlansAdvice");
        $this->addSection($plansNavPlanFeatures, "QDIA", $this->generateUrl('_plans_investments_qdia'), "PlansQdia");
        $this->addSection($plansNavPlanFeatures, "Media", $this->generateUrl('_plans_media'), "PlansMedia");
        $this->addSection($plansNavPlanFeatures, "Investor Profile", $this->generateUrl('_plans_investor_profile'), "PlansInvestorProfile");
        $this->addSection($plansNavPlanFeatures, "Access Control", $this->generateUrl('_plans_accessControl'), "PlansAccessControl");
        $this->addSection($plansNavPlanFeatures, "Autos", $this->generateUrl('_plans_autos'), "PlansAutos");
        $this->addSection($plansNavPlanFeatures, "Personal Information Control", $this->generateUrl('_plans_personalInformation'), "PIPWidget");
        $this->addSection($plansNavOutreach, "Outreach", $this->generateUrl('_plans_outreach'), "PlansEmail");
        $this->addSection($plansNavOutreach, "Video Ads", $this->generateUrl('_plans_vads'), "PlansVads");
        $this->addSection($plansNavOutreach, "Promotional Materials", $this->generateUrl('_plans_print_materials'), "PlansPrintPromotion");
        $this->addSection($plansNavOutreach, "Conversions", $this->generateUrl('_plans_conversions'), "PlansConversions");

        $this->addSection($plansNavAnalytics, "Profiles", $this->generateUrl('_plans_profiles'), "PlansProfiles");
        $this->addSection($plansNavAnalytics, "Analytics Cities", $this->generateUrl('_plans_analytics_cities'), "PlansAnalyticsCities");
        $this->addSection($plansNavAnalytics, "Analytics Pages", $this->generateUrl('_plans_analytics_pages'), "PlansAnalyticsPages");
        $this->addSection($plansNavAnalytics, "Analytics Visitors Overview", $this->generateUrl('_plans_analytics_visitors_overview'), "PlansAnalyticsVisitorsOverview");
        
        $this->addSection($plansNavSponsorConnect, "Sponsor Invites", $this->generateUrl('_plans_sponsor_connect'), "plansNavSponsorConnect");
        $this->addSection($plansNavSponsorConnect, "Employee Email List", $this->generateUrl('_plans_sponsor_connect_sponsorList'), "plansNavSponsorConnectList");

        $this->addSection($plansNavPowerView, "Communication Detail", $this->generateUrl('_plans_powerview_admin'), 'PlansPowerView');
        $this->addSection($plansNavIrio, "Irio", $this->generateUrl('_plans_powerview_irio'), 'PlansIrio');
        $this->addSection($plansNavPowerView, "Employee Email List", $this->generateUrl('_plans_widgets_emailList'), 'PlansPowerViewEmailList');



        $profilesClassVar = $this->get('profiles.service');
        $profileSections = $profilesClassVar->sections();
        $section = $session->section;
        $currentpage = $session->currentpage;
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        //if ($request->query->get("debugkey") == "2011")
        //if ($session->debugbar == 0 || !in_array($session->roleType,array("vwise","vwise_admin")))
        //$this->container->get('profiler')->disable();
        //$this->container->get('profiler')->enable();
        //if ($this->container->has('profiler'))
         //print_r($this->container->get('profiler'));

        
       
        //echo $this->container->get('kernel')->getBundle('AccountAccountBundle')->getNamespace();
        $maxsessiontime = $this->sessionLength - 10;


        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersLogins');
        $accountUsersLogin = $repository->findOneBy(array('sessionid' => $session->sessionid));
        $accountUsersLogin->lastActivity = $currentTime;
        //$em->flush();
        $openchat = $session->openchat;
        //$timeremaining= 1440;
        $session->set("openchat", 0);

        
        $types = $this->get("PlanService")->currentTypes();
        
        $unreadMessages = $this->get("AccountsUsersSystemMessagesService")->getCountOfUnreadMessages($currentuser->getId());
        
        return $this->render('TemplatesTemplatesBundle:Default:header.html.twig', array('name' => $name, 'pagename' => $pagename, 'currentuser' => $currentuser, 'currentplan' => $currentplan, "permissions" => $sectionPermissions, "profileSections" => $profileSections, "section" => $section, "currentpage" => $currentpage, "session" => $session, "plansNavPlanFeatures" => $plansNavPlanFeatures, "plansNavOutreach" => $plansNavOutreach, "plansNavAnalytics" => $plansNavAnalytics, "settingsNavDefaults" => $settingsNavDefaults, "settingsNavTools" => $settingsNavTools, "manageNavAccounts" => $manageNavAccounts, "manageNavOutreach" => $manageNavOutreach, "manageNavAnalytics" => $manageNavAnalytics, "manageNavOther" => $manageNavOther, "statsClientServices" => $statsClientServices, "newclientid" => $session->newclientid, "manageArray" => $manageArray, "manageNavSales" => $manageNavSales, "maxsessiontime" => $maxsessiontime, "statsArray" => $statsArray,  "currentTime" => $currentTime, "openchat" => $openchat,"currentaccount" => $currentaccount,
            "plansNavSponsorConnect" => $plansNavSponsorConnect,"plansNavPowerView" =>$plansNavPowerView,"plansNavIrio" =>$plansNavIrio,"types" => $types,"unreadMessages" => $unreadMessages));
    }

    

    public function sessionTimeRemainingAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $accountsUsersId = $request->request->get("accountsUsersId", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersLogins');
        $currentuser = $repository->findOneBy(array('accountsUsersId' => $accountsUsersId));
        return new response($currentuser->lastActivity . "|" . time());
    }

    public function renewsessionAction()
    {
        $this->changeSessionExpiry($this->sessionLength);
        return new Response("");
    }

    public function removeExpiredLoginAction()
    {
        $session = $this->get('adminsession');
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersLogins');
        $login = $repository->findOneBy(array('sessionid' => $session->sessionid));
        if ($login != null)
        {
            $em->remove($login);
            $em->flush();
        }


        return new Response("");
    }

    public function addSection(&$nav, $description, $link, $page)
    {
        $permissions = $this->get('rolesPermissions');
        $nav[] = new \stdClass();
        $i = count($nav) - 1;
        $nav[$i]->description = $description;
        $nav[$i]->link = $link;
        $nav[$i]->page = $page;
        $nav[$i]->visible = $permissions->readable($nav[$i]->page);
    }

    public function footerAction()
    {
        $session = $this->get('adminsession');
        $section = $session->section;
        $currentpage = $session->currentpage;
        $year = date("Y");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $currentuser = $repository->findOneBy(array('id' => $session->clientid));
        return $this->render('TemplatesTemplatesBundle:Default:footer.html.twig', array("section" => $section, "currentpage" => $currentpage, "year" => $year, "currentuser" => $currentuser));
    }

    public function lightboxheaderAction()
    {
        $currentTime = time();
        $maxsessiontime = 9999;
        $session = $this->get('adminsession');

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $currentuser = $repository->findOneBy(array('id' => $session->clientid));

        return $this->render('TemplatesTemplatesBundle:Default:lightboxheader.html.twig', array("currentTime" => $currentTime, "maxsessiontime" => $maxsessiontime, "currentuser" => $currentuser, "name" => "", "openchat" => ""));
    }

    public function lightboxfooterAction()
    {

        return $this->render('TemplatesTemplatesBundle:Default:lightboxfooter.html.twig');
    }

    public function subscribePanelAction($section)
    {
        $methods = $this->get('GeneralMethods');
        $sections = array();
        $session = $this->get('adminsession');
        $methods->addSection($sections, "", "");
        $methods->addSection($sections, "daily", "Daily");
        $methods->addSection($sections, "weekly", "Weekly");
        $methods->addSection($sections, "monthly", "Monthly");
        $methods->addSection($sections, "quarterly", "Quarterly");
        $methods->addSection($sections, "yearly", "Yearly");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:reportingSubscriptions');
        $subscription = $repository->findOneBy(array('accountsUsersId' => $session->clientid, "name" => $section));
        foreach ($sections as $section1)
        {
            if ($subscription != null && $subscription->howoften == $section1->field) $section1->selected = "selected";
            else $section1->selected = "";
        }
        return $this->render('TemplatesTemplatesBundle:Default:subscribe.html.twig', array("sections" => $sections, "section" => $section));
    }

    public function subscribePanelSavedAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:reportingSubscriptions');
        $subscription = $repository->findOneBy(array('accountsUsersId' => $session->clientid, "name" => $request->request->get("name")));
        $wasnull = 0;
        if ($subscription == null)
        {
            $subscription = new reportingSubscriptions();
            $subscription->accountsUsersId = $session->clientid;
            $subscription->name = $request->request->get("name");
            $wasnull = 1;
        }
        $subscription->howoften = $request->request->get("howoften");
        switch ($subscription->name)
        {
            case "VideoLibrary":
                $subscription->path = "/admin/account/MediaWidget/emailSubscription/";
                break;
        }
        if ($wasnull)
        {
            $em->persist($subscription);
        }
        else if ($request->request->get("howoften") == "")
        {
            $em->remove($subscription);
        }
        $em->flush();
        return new Response("");
    }
    
    
    public function setDebugBarAction()
    {   
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
      
        
        $session->set("debugbar",$request->request->get("debugbar"));
        return new Response("");
    }
    public function setCsvSessionAction(Request $request)
    {
        $this->get("session")->set("csvFilename",$request->request->get("filename"));
        $this->get("session")->set("csvContents",$request->request->get("contents"));
        return new Response("");
    }
    public function generateCsvAction(Request $request)
    {        
        $response = new Response($this->get("session")->get("csvContents"));
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$this->get("session")->get("csvFilename").'"');
        return $response;
    }
}
