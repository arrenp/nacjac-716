<?php
// src/AppBundle/Profiler/SuperAdminMatcher.php
namespace Templates\TemplatesBundle\Profiler;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class SuperAdminMatcher implements RequestMatcherInterface
{
    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function matches(Request $request)
    {
        if ($request->getSession()->get("debugbar",0) == 1 && in_array($request->getSession()->get("roleType",0),array("vwise","vwise_admin"))  )
        {
            $request->getSession()->set("debugbar",1);
            return true;
        }
        else
        {
            $request->getSession()->set("debugbar",0);
            return false;
        }
        
    }
}