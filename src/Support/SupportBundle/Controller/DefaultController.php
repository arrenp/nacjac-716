<?php

namespace Support\SupportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
use Support\SupportBundle\Controller\jira\Jira;

class DefaultController extends Controller
{

    public $userid;
    public $clientid;
    public $curlHeader;
    public $curlFooter;
    public $config;
    public $currentuser;

    public function indexAction(Request $request)
    {
        $this->init($request);
        return $this->render('SupportSupportBundle:Default:index.html.twig', array('currentuser' => $this->currentuser));
    }

    public function jiraAction(Request $request)
    {
        $this->init($request);
        return $this->render('SupportSupportBundle:Default:jira.html.twig', array('currentuser' => $this->currentuser));
    }

    public function loadTicketsAction(Request $request)
    {
        $this->init($request);
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $status = $request->query->get("status", "");
        
        $statusOrg = $status;
        
        $statusArray = $this->statusArray($statusOrg);
        
        $status = "";
        
        foreach ($statusArray as $statusElement)
        {
            if ($status != "")
            $status = $status." or ";
            $status = $status."status=".$statusElement;
        }
        

        $clientstring = '&clientuserid ~ \"' . $this->currentuser->id . '\"';
        if ($request->query->get("loadall", "") == "true") $clientstring = '';
        $command = $this->curlHeader.' -X POST '.$this->curlFooter.' --data \'{"jql":"project = AST' . $clientstring . '&('.$status.') ORDER BY created DESC","startAt":0,"fields":["summary","description","status","created","issue","customfield_10100"]}\' "https://' . $this->config['host'] . '/rest/api/2/search"';
        
        $tickets = $this->createTicketVariable($command);
        foreach ($tickets as &$ticket)
        $this->returnStatus($statusOrg,$ticket->status->name );
        
        return $this->render('SupportSupportBundle:Default:loadtickets.html.twig', array('currentuser' => $this->currentuser, 'tickets' => $tickets,"showuserid" =>$request->query->get("showuserid",0) ));
    }
    
    function statusArray($statusOrg)
    {
        if ($statusOrg == "Open")
        $statusArray = $this->openStatus();
        
        if ($statusOrg == "Done")
        $statusArray = $this->closedStatus();
        
        return $statusArray;
    }
    function openStatus()
    {
        return array("Open","in_development_ticket");
    }
    function closedStatus()
    {
        return array("Done");
    }
    
    function returnStatus($statusOrg,&$status)
    {
        if ($statusOrg == "Done")
        $status = "Closed";
        else if ( $status != "Open" )
        $status =  "In Progress";        
    }

    public function createTicketVariable($command)
    {
        $command_exec = json_decode(exec($command));
        //var_dump($command_exec);
        $issuesJson = $command_exec->issues;
        $tickets = array();
        $counter = 0;
        foreach ($issuesJson as $issueJson)
        {
            $tickets[$counter] = $issueJson->fields;
            $explode = explode("T", $tickets[$counter]->created);
            $tickets[$counter]->created = $explode[0];
            $tickets[$counter]->id = $issueJson->id;
            $counter++;
        }
        return $tickets;
    }

    public function addTicketAction(Request $request)
    {
        $this->init($request);
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $subject = str_replace('"','\"',$request->request->get("subject", ""));
        $subject = str_replace("'","’",$subject);
        $message = $request->request->get("message", "");
        $message = str_replace("\n","\\n",$message);
        $message = str_replace("\t","\\t",$message);
        $message = htmlspecialchars($message,ENT_QUOTES,"UTF-8");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $this->currentuser->userid));
        $accountManager = $account->accountManager;
        $managerArray = array();
        if ($accountManager != "") {
            $accManagers = explode(',', $accountManager);
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountManagers');
            foreach ($accManagers as $manager) {
                $managerArray[] = $repository->findOneBy(array('id' => $manager))->emailAddress;

            }
        }
        $managerEmailString = implode(",", $managerArray);
               
        $command = $this->curlHeader . '-X POST --data \'{"fields": {"project":{ "key": "AST"},"summary": "' . $subject . '","description": "' . $message . '","issuetype": {"name": "Task"},"customfield_10600": "' . $this->currentuser->userid . '","customfield_10601": "' . $managerEmailString . '","customfield_10100" : "' . $this->currentuser->id . '","customfield_10101" :"' . $this->currentuser->getEmail() . '" }}\'' . $this->curlFooter . 'https://' . $this->config['host'] . '/rest/api/2/issue/';
        $ticket = json_decode(exec($command));
        $url = "";
        $emailContents = '<a href = "https://'.$this->config['host'].'/browse/'.$ticket->key.'">View Ticket</a>';
        $message = \Swift_Message::newInstance()
        ->setSubject("Support Ticket Created ".$ticket->key)
        ->setFrom('info@smartplanenterprise.com')
        ->setTo("cgillespie@vwise.com")
            
        ->setBody(
        $emailContents,'text/html');
        //render email template and send email
        $this->get('mailer')->send($message);
        $message->setTo("dgorsline@vwise.com");
        $this->get('mailer')->send($message);
        $message->setTo("calber@vwise.com");
        $this->get('mailer')->send($message);

        return new Response($message);
    }

    public function viewTicketAction(Request $request)
    {
        $this->init($request);
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $command = $this->curlHeader.' -X POST '.$this->curlFooter.' --data \'{"jql":"project = AST AND id=' . $id . '","startAt":0,"fields":["summary","description","status","created","issue"]}\' "https://' . $this->config['host'] . '/rest/api/2/search"';
        $ticket = $this->createTicketVariable($command);
        $ticket = $ticket[0];
        $ticket->description = htmlspecialchars_decode($ticket->description,ENT_QUOTES);
        if ($ticket->status->name != "Open")
        {
            if (in_array($ticket->status->name,$this->openStatus()))
            $statusOrg = "Open";
            if (in_array($ticket->status->name,$this->closedStatus()))
            $statusOrg = "Done";
            $this->returnStatus($statusOrg, $ticket->status->name);
        }
                
        return $this->render('SupportSupportBundle:Default:viewticket.html.twig', array('currentuser' => $this->currentuser, 'ticket' => $ticket));
    }

    public function init(Request $request)
    {
        $session = $this->get('adminsession');
        $session->set("section", "Support");
        $session = $request->getSession();
        $this->userid = $session->get("userid");
        $this->clientid = $session->get("clientid");


        $this->config = array();
        //if ($request->server->get('HTTP_HOST') == "symfony-admin.smartplanenterprise.com") $this->config['host'] = "localhost:8080";
        //else if ($request->server->get('HTTP_HOST') == "127.0.0.1") $this->config['host'] = "jira.smartplanenterprise.com:8080";
        //else $this->config['host'] = "172.24.16.203:8080";
        $this->config['host'] = "vwise-inc.atlassian.net";
        $this->config['username'] = "ops";
        $this->config['password'] = "g0GZAPuLcy6a";



        $this->curlHeader = ' curl -D- -g -u '.$this->config['username'].':'.$this->config['password'].'  ';
        $this->curlFooter = ' -H "Content-Type: application/json" ';

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $this->currentuser = $repository->findOneBy(array('id' => $this->clientid));
    }

    public function sendEmailAction()
    {
        try
        {
            $request = Request::createFromGlobals();
            $request->getPathInfo();
            $email = $request->request->get('email', '');
            $themessage = $request->request->get('message', '');
            $user = $request->request->get('user', '');
            $title = $request->request->get('title', '');

            $tousmessage = '<div style = "color:#173B0B">Title: ' . $title . '<br/>Below is a message from user ' . $user . '  </div><hr/>
			' . $themessage . '<hr/><br/>Contact back<br/>Email: <a href = "mailto:' . $email . '">' . $email . '</a>';
            $toclientmessage = 'Thank you for contacting support@smartplanenterprise.com, below is a copy of your email: <br/><br/>' . $themessage;


            $message = \Swift_Message::newInstance()
                    ->setSubject('Smartplan Admin Support | request needed from ' . $user)
                    ->setFrom("support@smartplanenterprise.com")
                    ->setTo(array('support@smartplanenterprise.com' => 'Smartplan Support', 'nrobertson@vwise.com' => 'Nick'))
                    ->setBody(
                    $tousmessage, 'text/html'
            );

            $this->get('mailer')->send($message);


            $message = \Swift_Message::newInstance()
                    ->setSubject('Smartplan Admin Support | Email sent')
                    ->setFrom("support@smartplanenterprise.com")
                    ->setTo($email)
                    ->setBody(
                    $toclientmessage, 'text/html'
            );
            $this->get('mailer')->send($message);
            return new Response("sent");
        }
        catch (Exception $e)
        {
            return new Response("Error occured!: " . $e->getMessage());
        }
    }

    public function monitoringToolAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Stats");
        return $this->render('SupportSupportBundle:Default:monitoringtool.html.twig');
    }

    public function getMonitoringToolAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $startDate = $request->request->get('startDate');
        $endDate = $request->request->get('endDate');
        $sql = "select statData from monitoringTool where date between '$startDate' and '$endDate'";
        $connection = $this->get('doctrine.dbal.default_connection');
        $items = $connection->fetchAll($sql);
        $statData = '';
        foreach ($items as $item)
        {
            foreach ($item as $key => $val2)
            {
                if ($key == "statData")
                {
                    $statData .= $val2 . ',';
                }
            }
        }
        $statData = rtrim($statData, ',');
        return new Response($statData);
    }

    public function dashboardMonitoringAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Stats");        
        
        
        $sql = "select * from dashboardMonitoringTool limit 1";
        $connection = $this->get('doctrine.dbal.default_connection');
        $items = $connection->fetchAll($sql);
        foreach ($items as $item)
        {
            foreach ($item as $key => $val2)
            {
                if ($key == "srt_status")
                {
                    $srt_status = $val2;
                }
                if ($key == "srt_date")
                {
                    $srt_date = $val2;
                }
                if ($key == "retrev_status")
                {
                    $retrev_status = $val2;
                }
                if ($key == "retrev_date")
                {
                    $retrev_date = $val2;
                }
                if ($key == "dst_status")
                {
                    $dst_status = $val2;
                }
                if ($key == "dst_date")
                {
                    $dst_date = $val2;
                }
                if ($key == "envisage_status")
                {
                    $envisage_status = $val2;
                }
                if ($key == "envisage_date")
                {
                    $envisage_date = $val2;
                }
                if ($key == "relius_status")
                {
                    $relius_status = $val2;
                }
                if ($key == "relius_date")
                {
                    $relius_date = $val2;
                }
            }
        }
        return $this->render('SupportSupportBundle:Default:dashboardMonitoring.html.twig', array('srt_status' => $srt_status, 'srt_date' => $srt_date, 'retrev_status' => $retrev_status, 'retrev_date' => $retrev_date, 'dst_status' => $dst_status, 'dst_date' => $dst_date, 'envisage_status' => $envisage_status, 'envisage_date' => $envisage_date, 'relius_status' => $relius_status, 'relius_date' => $relius_date));
    }

}
