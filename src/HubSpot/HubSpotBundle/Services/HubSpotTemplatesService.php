<?php

namespace HubSpot\HubSpotBundle\Services;

class HubSpotTemplatesService extends HubSpotBaseService {
	
	/**
	 * @param array $data Template data
	 * @return array|bool
	 */
	public function createTemplate(array $data) {
		$response = $this->doCall("/content/api/v2/templates", "POST", $data);
		return $response;
	}
	
	/**
	 * @param array $options Options and filters
	 * @return array|bool
	 */
	public function listTemplates(array $options = array()) {
		$response = $this->doCall("/content/api/v2/templates", "GET", null, $options);
		return $response;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @param array $data Template data
	 * @return array|bool
	 */
	public function updateTemplate($templateId, array $data = array()) {
		$response = $this->doCall("/content/api/v2/templates/$templateId", "PUT", $data);
		return $response;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @return bool
	 */
	public function deleteTemplate($templateId) {
		$response = $this->doCall("/content/api/v2/templates/$templateId", "DELETE");
		return $response !== false;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @return array|bool 
	 */
	public function getTemplateById($templateId) {
		$response = $this->doCall("/content/api/v2/templates/$templateId");
		return $response;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @param array $data Template data
	 * @return array|bool
	 */
	public function updateAutoSaveBuffer($templateId, array $data = array()) {
		$response = $this->doCall("/content/api/v2/templates/$templateId/buffer", "PUT", $data);
		return $response;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @return array|bool
	 */
	public function getAutoSaveBuffer($templateId) {
		$response = $this->doCall("/content/api/v2/templates/$templateId/buffer");
		return $response;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @return bool
	 */
	public function hasBufferedChanges($templateId) {
		$response = $this->doCall("/content/api/v2/templates/$templateId/has-buffered-changes");
		return $response !== false ? $response['has_changes'] : false;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @return array|bool
	 */
	public function pushBufferLive($templateId) {
		$response = $this->doCall("/content/api/v2/templates/$templateId/push-buffer-live", "POST");
		return $response;
	}
	
	/**
	 * NOTE: The API documentation says to use POST but this appears to be incorrect. It works with PUT.
	 * @param int $templateId Template ID
	 * @return array|bool 
	 */
	public function restoreDeleted($templateId) {
		$response = $this->doCall("/content/api/v2/templates/$templateId/restore-deleted", "PUT");
		return $response;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @return array|bool
	 */
	public function listPreviousVersions($templateId) {
		$response = $this->doCall("/content/api/v2/templates/$templateId/versions");
		return $response;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @param int $versionId Version ID
	 * @return array|bool
	 */
	public function getPreviousVersion($templateId, $versionId) {
		$response = $this->doCall("/content/api/v2/templates/$templateId/versions/$versionId");
		return $response;
	}
	
	/**
	 * @param int $templateId Template ID
	 * @param int $versionId Version ID
	 * @return bool
	 */
	public function restorePreviousVersion($templateId, $versionId) {
		$response = $this->doCall("/content/api/v2/templates/$templateId/versions/restore", "POST", array('version_id' => $versionId));
		return $response !== false;
	}
}
