<?php

namespace HubSpot\HubSpotBundle\Services;

class HubSpotWorkflowService extends HubSpotBaseService
{

    public function getWorkflows() {
        $response = $this->doCall("/automation/v3/workflows");
        return $response !== false ? $response['workflows'] : false;
    }

    public function getWorkflow($workflowId) {
        $response = $this->doCall("/automation/v3/workflows/{$workflowId}");
        return $response !== false ? $response : false;
    }
    
    public function getCurrentEnrollment($vid) {
        $response = $this->doCall("/automation/v2/workflows/enrollments/contacts/$vid");
        return $response !== false ? $response : [];
    }
}
