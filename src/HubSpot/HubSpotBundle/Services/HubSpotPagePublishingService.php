<?php

namespace HubSpot\HubSpotBundle\Services;

class HubSpotPagePublishingService extends HubSpotBaseService {
	
	/**
	 * @param array $data Page data
	 * @return array|bool
	 */
	public function createPage(array $data) {
		$response = $this->doCall("/content/api/v2/pages", "POST", $data);
		return $response;
	}
	
	/**
	 * @param array $options Options and filters
	 * @return array|bool
	 */
	public function listPages(array $options = array()) {
		$response = $this->doCall("/content/api/v2/pages", "GET", null, $options);
		return $response;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @param array $data Page data
	 * @return array|bool
	 */
	public function updatePage($pageId, array $data = array()) {
		$response = $this->doCall("/content/api/v2/pages/$pageId", "PUT", $data);
		return $response;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @return bool
	 */
	public function deletePage($pageId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId", "DELETE");
		return $response !== false;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @return array|bool
	 */
	public function getPageById($pageId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId");
		return $response;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @param array $data Page data
	 * @return array|bool
	 */
	public function updateAutoSaveBuffer($pageId, array $data = array()) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/buffer", "PUT", $data);
		return $response;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @return array|bool
	 */
	public function getAutoSaveBuffer($pageId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/buffer");
		return $response;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @return array|bool The cloned page data as an array or false on failure
	 */
	public function clonePage($pageId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/clone", "POST");
		return $response;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @return bool
	 */
	public function hasBufferedChanges($pageId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/has-buffered-changes");
		return $response !== false ? $response['has_changes'] : false;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @param array $data Format: ['action' => ACTION] where ACTION is one of "push-buffer-live","schedule-publish","cancel-publish"
	 * @return bool
	 */
	public function publishAction($pageId, array $data) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/publish-action", "POST", $data);
		return $response !== false;
	}
	
	/**
	 * @param int $pageId Page ID 
	 * @return array|bool The live page data as an array or false on failure
	 */
	public function pushBufferLive($pageId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/push-buffer-live", "POST");
		return $response;
	}
	
	/**
	 * NOTE: The API documentation says to use POST but this appears to be incorrect. It works with PUT.
	 * @param int $pageId Page ID
	 * @return array|bool The restored page data as an array or false on failure
	 */
	public function restoreDeleted($pageId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/restore-deleted", "PUT");
		return $response;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @return bool
	 */
	public function validateBuffer($pageId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/validate-buffer", "POST");
		return $response !== false ? $response['succeeded'] : false;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @return array|bool
	 */
	public function listPreviousVersions($pageId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/versions");
		return $response;
	}
	
	/**
	 * @param int $pageId Page ID
	 * @param int $versionId Version ID
	 * @return bool
	 */
	public function restorePreviousVersion($pageId, $versionId) {
		$response = $this->doCall("/content/api/v2/pages/$pageId/versions/restore", "POST", array('version_id' => $versionId));
		return $response !== false;
	}
}
