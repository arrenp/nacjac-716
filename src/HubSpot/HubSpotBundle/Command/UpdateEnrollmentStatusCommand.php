<?php

namespace HubSpot\HubSpotBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateEnrollmentStatusCommand extends ContainerAwareCommand {
    
    protected function configure()
    {
        $this
            ->setName('app:UpdateEnrollmentStatusCommand')
            ->setDescription('Sets enrolled column of launchToolRecipients to "y" if they completed smartenroll')
            ->setHelp('Sets enrolled column of launchToolRecipients to "y" if they completed smartenroll')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $entityManager \Doctrine\ORM\EntityManager */
        $entityManager = $this->getContainer()->get("doctrine")->getManager();
        
        /* @var $hubSpotService \HubSpot\HubSpotBundle\Services\HubSpotContactsService */
        $hubSpotService = $this->getContainer()->get('hub_spot.contacts');
        $generalService = $this->getContainer()->get("general.methods");
        $generalService->postToSlack("info", $this->getName(), "Job start");
        
        $query = $entityManager->createQuery("
            SELECT r
            FROM classesclassBundle:launchToolRecipients r
            JOIN classesclassBundle:profiles p WITH p.smartEnrollPartId = r.smartEnrollPartId
            WHERE r.smartEnrollPartId != '' AND p.reportDate > '0000-00-00 00:00:00' AND (r.enrolled IS NULL OR LOWER(r.enrolled) != 'y')
            GROUP BY r.id
        ");
        $results = $query->getResult();
        $counter = 0;
        foreach ($results as $launchToolRecipient) {
            $launchToolRecipient->enrolled = 'Y';
            $counter++;
            
            $email = trim($launchToolRecipient->email);
            if (($email == '') || ($email == 'Email')) {
                continue;
            }
            $properties = ['status_code' => 1, 'status_code_description' => 'Enrolled'];
            $hubSpotService->addToBatch($email, $properties);
        }
        $entityManager->flush();
        $hubSpotService->flushBatch();
        $outputString = "Updated $counter rows";
        $output->writeln($outputString);
        $generalService->postToSlack("info", $this->getName(), $outputString);
        if (!empty($hubSpotService->getLastError()))
        {
            $generalService->postToSlack("error", $this->getName(), $hubSpotService->getLastError());
            $invalidEmails = $hubSpotService->getInvalidEmails();
            if (!empty($invalidEmails))
            {
                $generalService->postToSlack("error", "Failed Emails ",implode(",",$invalidEmails ));
            }
        }
        $generalService->postToSlack("info", $this->getName(), "Job End");
    }
    
}
