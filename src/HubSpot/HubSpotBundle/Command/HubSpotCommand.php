<?php

namespace HubSpot\HubSpotBundle\Command;

use classes\classBundle\Entity\participants;
use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use HubSpot\HubSpotBundle\Services\HubSpotContactsService;
use classes\classBundle\Services\Widgets\WidgetsSrt;
use Doctrine\ORM\EntityManager;
use classes\classBundle\Entity\EmailCampaignEmployees;
use classes\classBundle\Entity\accountsRecordkeepersSrt;
use classes\classBundle\Entity\plans;

class HubSpotCommand extends ContainerAwareCommand
{

    /**
     * @var HubSpotContactsService
     */
    private $hubSpotService;

    /**
     * @var WidgetsSrt;
     */
    private $widgetSrtService;

    /**
     * @var EntityManager
     */
    private $entityManager;
    
    private $invalidEmails;
    
    private $emailPropertiesMap;

    private $emailToParticipantId;

    private $emailToPlanId;

    /**
     * @param OutputInterface $output
     * @param $participants
     * @param $emailCampaignEmployees
     * @param $plan
     * @param $accountRecord
     * @param $isSmartPlan
     * @param $isSmartEnroll
     * @param $providerLogo
     * @param $sponsorLogo
     * @param $smartPlanLogin
     * @param $smartEnrollLogin
     * @param $HRFirst
     * @param $HRLast
     * @param $HREmail
     */
    public function processParticipants(OutputInterface $output, $participants, $emailCampaignEmployees, $planCode, $accountRecord, $isSmartPlan, $isSmartEnroll, $providerLogo, $sponsorLogo, $smartPlanLogin, $smartEnrollLogin, $HRFirst, $HRLast, $HREmail)
    {
        $eligibleStatusCode = $accountRecord->eligibleStatus;

        if ($participants) {
            if (property_exists($participants, 'vWiseParticipantList')) {
                if (property_exists($participants->vWiseParticipantList, 'vWiseParticipantDetailDTO')) {
                    $output->writeln("participant count:" . count($participants->vWiseParticipantList->vWiseParticipantDetailDTO));
                    if (!is_array($participants->vWiseParticipantList->vWiseParticipantDetailDTO)) {
                        $participants->vWiseParticipantList->vWiseParticipantDetailDTO = array($participants->vWiseParticipantList->vWiseParticipantDetailDTO);
                    }
                    foreach ($participants->vWiseParticipantList->vWiseParticipantDetailDTO as $participantDTO) {
                        if (property_exists($participantDTO, 'Email')) {
                            $participant = $emailCampaignEmployees->findOneBy(array('participantId' => $participantDTO->ParticipantID));
                            if ($participant) {
                                $participant->updateFromXml($participantDTO, $eligibleStatusCode);
                            } else {
                                $participant = new EmailCampaignEmployees();
                                $participant->loadFromXml($participantDTO, $eligibleStatusCode);
                            }
                            if ($participant->getEmail()) {
                                $participant->setPlanId($planCode);
                                $participant->setPartnerId($accountRecord->partnerid);
                                $participant->setSmartPlan($isSmartPlan);
                                $participant->setSmartEnroll($isSmartEnroll);
                                $participant->setProviderLogo($providerLogo);
                                $participant->setSponsorLogo($sponsorLogo);
                                $participant->setSmartPlanLogin($smartPlanLogin);
                                $participant->setSmartEnrollLogin($smartEnrollLogin);
                                $participant->setHRFirst($HRFirst);
                                $participant->setHRLast($HRLast);
                                $participant->setHREmail($HREmail);
                                $this->entityManager->persist($participant);
                            } else {
                                continue;
                            }
                            $email = $participant->getEmail();
                            $properties = $participant->getPropertyArray();
                            $this->emailPropertiesMap[$email] = $properties;
                            $this->emailToParticipantId[$email] = $participantDTO->ParticipantID;
                            $this->emailToPlanId[$email] = $planCode;
                            $result = $this->hubSpotService->addToBatch($email, $properties);
                            $this->processResponse($result);
                        }
                    }
                }
            }
        } else {
            $output->writeln('no participants');
        }
    }

    protected function configure()
    {
        $this
            ->setName('app:hubspot')
            ->setDescription('Migrates enrollees to HubSpot.')
            ->setHelp('Use this command to mirgrate enrolless to HubSpot')
            ->addOption('accountId', null, InputOption::VALUE_REQUIRED, 'Account id of enrollees')
            ->addOption('file', null, InputOption::VALUE_REQUIRED, 'file of contacts')
            ->addOption('list', null, InputOption::VALUE_NONE, 'list all contacts')
            ->addOption('delete', null, InputOption::VALUE_REQUIRED, 'delete contact')
            ->addOption('deleteall', null, InputOption::VALUE_NONE, 'delete all contacts')
            ->addOption('count', null, InputOption::VALUE_NONE, 'get count of contacts')
            ->addOption('plan', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'plan id');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $gm = $this->getContainer()->get('general.methods');
        $gm->postToSlack("info", "HubSpot Command", "Starting HubSpot Command", "default");
        $this->rkp = $this->getContainer()->get('spe.app.rkp');
        $this->hubSpotService = $this->getContainer()->get('hub_spot.contacts');
        $this->widgetSrtService = $this->getContainer()->get('WidgetsSrt');
        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
        $this->emailPropertiesMap = [];
        $this->invalidEmails = [];
        $this->hubSpotService->clearInvalidEmails();

        // do not log sql queries to save memory
        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);

        $emailCampaignEmployees = $this->entityManager->getRepository('classesclassBundle:EmailCampaignEmployees');
        // need to find out what correct parameters are for Pentegra

        $accountId = $input->getOption('accountId');
        $file = $input->getOption('file');

        $displayList = $input->getOption('list');
        $deleteVid = $input->getOption('delete');
        $deleteAll = $input->getOption('deleteall');
        $getCount = $input->getOption('count');
        $planIds = $input->getOption('plan');

        $accountsRecordkeepersSrt = $this->entityManager->getRepository('classesclassBundle:accountsRecordkeepersSrt');
        $account = $accountsRecordkeepersSrt->findOneBy(array('userid' => $accountId));
        if (!$account) {
            $gm->postToSlack("error", "HubSpot Command", "account {$accountId} not found in accountsRecordkeepersSrt", "default");
            $output->writeln("account {$accountId} not found in accountsRecordkeepersSrt");
            return;
        }

        $accounts = $this->entityManager->getRepository('classesclassBundle:accounts');
        $accountRecord = $accounts->findOneBy(array('id' => $accountId));
        if (!$accountRecord) {
            $gm->postToSlack("error", "HubSpot Command", "account {$accountId} on found in accounts", "default");
            $output->writeln("account {$accountId} on found in accounts");
            return;
        }

        if ($getCount) {
            $output->writeln("Count of Contact List");
            $result = array('has-more' => true);
            $count = 0;
            $vidOffset = 0;
            $output->writeln('');
            while ($result['has-more']) {
                $output->write('.');
                $result = $this->hubSpotService->getAllContacts(array('vidOffset' => $vidOffset, 'count' => 100));
                $vidOffset = $result['vid-offset'];
                $count += count($result['contacts']);
            }
            $output->writeln('');
            $output->writeln("Count:" . $count);
        } else if ($deleteVid) {
            $contact = $this->hubSpotService->getContactById($deleteVid);
            $employee = $emailCampaignEmployees->findOneBy(array('email' => $contact['properties']['email']['value']));
            if ($employee) {
                $this->entityManager->remove($employee);
                $this->entityManager->flush();
            }
            $result = $this->hubSpotService->deleteContact($deleteVid);
            $output->writeln("deleting $deleteVid:" . ($result ? "Success" : "Failure"));
        } else if ($deleteAll) {
            $employees = $emailCampaignEmployees->findAll();
            foreach ($employees as $employee) {
                $this->entityManager->remove($employee);
            }
            $this->entityManager->flush();
            $result = array('has-more' => true);
            $count = 0;
            $vidOffset = 0;
            while ($result['has-more']) {
                $result = $this->hubSpotService->getAllContacts(array('vidOffset' => $vidOffset, 'count' => 100));
                $vidOffset = $result['vid-offset'];
                foreach ($result['contacts'] as $contact) {
                    $this->hubSpotService->deleteContact($contact['vid']);
                }
                $count += count($result['contacts']);
            }
            $output->writeln("Deleting all Contacts:" . ($result ? "Success" : "Failure"));
        } else if ($displayList) {
            $output->writeln("Contact List");
            $result = array('has-more' => true);
            $count = 0;
            $vidOffset = 0;
            while ($result['has-more']) {
                $result = $this->hubSpotService->getAllContacts(array('vidOffset' => $vidOffset, 'count' => 100));
                $vidOffset = $result['vid-offset'];
                foreach ($result['contacts'] as $contact) {
                    $vid = $contact['vid'];
                    $firstName = array_key_exists('firstname', $contact['properties']) ? $contact['properties']['firstname']['value'] : '';
                    $lastName = array_key_exists('lastname', $contact['properties']) ? $contact['properties']['lastname']['value'] : '';
                    $output->writeln("{$vid}\t{$firstName}\t{$lastName}");
                }
                $count += count($result['contacts']);
            }
            $output->writeln("Count:" . $count);
        } else if (($accountId != null) && ($file != null)) {
            // reading from file assign to account id
            $output->writeln("Adding Contacts from file");

            // plans can be an array, but for file import, needs to be only one plan
            // assume we're using the first plan listed
            $planId = $planIds[0];

            $isSmartPlan = 'N';
            $isSmartEnroll = 'N';
            $smartPlanLogin = null;
            $smartEnrollLogin = null;
            $providerLogo = null;
            $sponsorLogo = null;

            $vWisePlans = $this->entityManager->getRepository('classesclassBundle:plans');
            $output->writeln($planId);
            $vWisePlan = $vWisePlans->findBy(array('planid' => $planId, 'userid' => $accountId));
            if (!$vWisePlan) {
                $output->writeln('plan not found');
                return;
            } else {
                foreach ($vWisePlan as $vwp) {
                    $output->writeln($vwp->type);
                    if ($vwp->type == "smartplan") {
                        $isSmartPlan = 'Y';
                        $smartPlanLogin = $vwp->participantLoginWebsiteAddress;
                    }
                    if ($vwp->type == "smartenroll") {
                        $isSmartEnroll = 'Y';
                        $smartEnrollLogin = $vwp->participantLoginWebsiteAddress;
                    }
                    $providerLogo = $vwp->providerLogoEmailImage;
                    $sponsorLogo = $vwp->sponsorLogoEmailImage;
                    $HRFirst = $vwp->HRFirst;
                    $HRLast = $vwp->HRLast;
                    $HREmail = $vwp->HREmail;
                }
            }

            $rawXml = file_get_contents($file, FALSE);
            $xml = simplexml_load_string($rawXml);
            $participants = json_decode(json_encode($xml));

            $this->processParticipants($output, $participants, $emailCampaignEmployees, $planId, $accountRecord, $isSmartPlan, $isSmartEnroll, $providerLogo, $sponsorLogo, $smartPlanLogin, $smartEnrollLogin, $HRFirst, $HRLast, $HREmail);
            $this->entityManager->flush();
            $this->entityManager->clear();
            $result = $this->hubSpotService->flushBatch();
            $this->processResponse($result);

        } else if (($accountId != null) && ($file == null)) {
            try {
                $gm->postToSlack("info", "HubSpot Command", "Starting 'Updating Contacts via data call'", "default");
                $output->writeln("Updating Contacts via data call");
                // reading from database
                // go get a session id

                $accountSettings = array(
                    'userName' => $account->userName,
                    'password' => $account->password,
                    'type' => $account->type,
                    'siteToken' => $account->siteToken,
                    'url' => $account->url,
                    'recordkeeperVersion' => $accountRecord->recordkeeperVersion,
                    'connectionType' => $accountRecord->connectionType,
                    'id' => $accountRecord->id
                );
                $this->widgetSrtService->sid($accountSettings);

                $subPlans = array();
                $vWisePlans = $this->entityManager->getRepository('classesclassBundle:plans');
                if (count($planIds) > 0) {
                    $subPlans = $planIds;
                } else {
                    $statement = $vWisePlans->createQueryBuilder('plans')
                        ->select('plans.planid')
                        ->distinct()
                        ->where('plans.userid = :userid')
                        ->andWhere('plans.participantDetails = :participantDetails')
                        ->andWhere('plans.deleted = :deleted')
                        ->setParameter('userid', $accountId)
                        ->setParameter('participantDetails', 1)
                        ->setParameter('deleted', 0)
                        ->getQuery();
                    $planIdsInTheWrongFormat = $statement->getResult();

                    foreach ($planIdsInTheWrongFormat as $planId) {
                        $subPlans[] = $planId['planid'];
                    }
                }

                foreach ($subPlans as $plan) {
                    /**
                     * @var Plans
                     */
                    $vWisePlan = null;
                    $isSmartPlan = 'N';
                    $isSmartEnroll = 'N';
                    $smartPlanLogin = null;
                    $smartEnrollLogin = null;
                    $providerLogo = null;
                    $sponsorLogo = null;
                    $HRFirst = null;
                    $HRLast = null;
                    $HREmail = null;

                    $output->writeln($plan);

                    $vWisePlan = $vWisePlans->findBy(array('planid' => $plan, 'userid' => $accountId));
                    if (!$vWisePlan) {
                        $gm->postToSlack("error", "HubSpot Command", "Plan not found: {$plan}", "default");
                        $output->writeln('plan not found');
                        continue;
                    } else {
                        foreach ($vWisePlan as $vwp) {
                            $output->writeln($vwp->type);
                            if ($vwp->type == "smartplan") {
                                $isSmartPlan = 'Y';
                                $smartPlanLogin = $vwp->participantLoginWebsiteAddress;
                            }
                            if ($vwp->type == "smartenroll") {
                                $isSmartEnroll = 'Y';
                                $smartEnrollLogin = $vwp->participantLoginWebsiteAddress;
                            }
                            $providerLogo = $vwp->providerLogoEmailImage;
                            $sponsorLogo = $vwp->sponsorLogoEmailImage;
                            $HRFirst = $vwp->HRFirst;
                            $HRLast = $vwp->HRLast;
                            $HREmail = $vwp->HREmail;
                            $smartplanid = $vwp->id;
                        }
                    }

                    // $plan->PlanCode maps to plan.planid and plan.userid = userid
                    $participants = $this->widgetSrtService->getParticipantDetails($plan,$accountId,$smartplanid);

                    $this->processParticipants($output, $participants, $emailCampaignEmployees, $plan, $accountRecord, $isSmartPlan, $isSmartEnroll, $providerLogo, $sponsorLogo, $smartPlanLogin, $smartEnrollLogin, $HRFirst, $HRLast, $HREmail);
                    $this->entityManager->flush();
                    $this->entityManager->clear();
                }
                $result = $this->hubSpotService->flushBatch();
                $this->processResponse($result);

            } catch (\Exception $e) {
                $gm->postToSlack("error", "HubSpot Command", "\nException:\n" . $e->getMessage() . "\n" . $e->getFile() . ":" . $e->getLine(), "default");
                $output->writeln("\nException:\n" . $e->getMessage() . "\n" . $e->getFile() . ":" . $e->getLine());
                $this->entityManager->flush();
                $result = $this->hubSpotService->flushBatch();
                $this->processResponse($result);
            }
            $gm->postToSlack("info", "HubSpot Command", "Finished 'Updating Contacts via data call'", "default");
        }
        
        if (count($this->invalidEmails) > 0) {
            $alertRecipientsRepository = $this->entityManager->getRepository('classesclassBundle:HubSpotAlertRecipients');
            $alertRecipients = $alertRecipientsRepository->findAll();
            $to = [];
            foreach ($alertRecipients as $alertRecipient) {
                $to[] = $alertRecipient->emailAddress;
            }
            $contents = "Account ID: $accountId<br>";
            $contents .= "Company Name: {$accountRecord->company}<br><br>";
            $contents .= "The following invalid e-mail addresses have been encountered in the HubSpot import: <br><br>";
            foreach ($this->invalidEmails as $invalidE) {
                $contents .= "{$invalidE}, Participant ID: {$this->emailToParticipantId[$invalidE]}, Plan Id: {$this->emailToPlanId[$invalidE]}<br>";
            }
            $message = \Swift_Message::newInstance()
            ->setSubject('Invalid Email Addresses in HubSpot Import')
            ->setFrom('info@smartplanenterprise.com')
            ->setBcc($to)
            ->setBody(
            $contents,'text/html');
            $this->getContainer()->get('mailer')->send($message); 
        }
    }

    public function valueOrNull($object, $field) {
        if (property_exists($object, $field)) {
            if (is_object($object->$field) && (get_class($object->$field) == \stdClass::class)) {
                return null;
            } else {
                return trim($object->$field);
            }
        } else {
            return null;
        }
    }

    protected function processResponse($result) {
        if ($result === HubSpotContactsService::BATCH_ERROR || $result === HubSpotContactsService::BATCH_FLUSHED) {
            $invalidEmails = $this->hubSpotService->getInvalidEmails(); 
            if (count($invalidEmails) > 0) {
                $validEmails = array_diff_key($this->emailPropertiesMap, array_flip($invalidEmails));
                $resubmit = $this->hubSpotService->replaceGroup($validEmails);
                if (!$resubmit) {
                    $invalidEmails = array_merge($invalidEmails, $validEmails);
                }
            }
            $this->invalidEmails = array_merge($this->invalidEmails, $invalidEmails);
            $this->hubSpotService->clearInvalidEmails();
            $this->emailPropertiesMap = [];
        }
    }

}