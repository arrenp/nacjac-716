<?php

namespace HubSpot\HubSpotBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use HubSpot\HubSpotBundle\Services\HubSpotContactsService;
use HubSpot\HubSpotBundle\Services\HubSpotEmailService;
use HubSpot\HubSpotBundle\Services\HubSpotWorkflowService;
use Shared\General\SFTPConnection;

class HubSpotAuditReportCommand extends ContainerAwareCommand
{

    var $outputPath = '/root/csv/pentegra/';

    protected function configure()
    {
        $this
            ->setName('app:hubspotaudit')
            ->setDescription('Generates HubSpot audit report')
            ->setHelp('Use this command to generate the HubSpot audit report')
            ->addOption('startDate', null, InputOption::VALUE_REQUIRED, 'Start Date')
            ->addOption('endDate', null, InputOption::VALUE_REQUIRED, 'End Date')
            ->addOption('skipUpload', null, InputOption::VALUE_NONE, 'If present, skip upload')
            ->addOption('workflowId', null, InputOption::VALUE_REQUIRED, 'workflowId')
            ->addOption('outputPath', null, InputOption::VALUE_REQUIRED, 'outputPath')
            ->addOption('startingfilename', null, InputOption::VALUE_REQUIRED, 'startingfilename')
            ->addOption('vwiseSftpDir', null, InputOption::VALUE_REQUIRED, 'optional vwise directory to upload file to');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $generalService \Shared\GeneralBundle\Services\GeneralService */
        $generalService = $this->getContainer()->get("general.methods");
        $generalService->postToSlack("info", $this->getName(), "Starting");
        
        /**
         * @var HubSpotContactsService
         */
        $hubSpotService = $this->getContainer()->get('hub_spot.contacts');

        /**
         * @var HubSpotEmailService
         */
        $hubSpotEmailService = $this->getContainer()->get('hub_spot.emails');

        /**
         * @var HubSpotWorkflowService
         */
        $hubSpotWorkflowService = $this->getContainer()->get('hub_spot.workflows');

        // get workflowId from command line parameters
        $workflowId = $input->getOption('workflowId');
        $generalService->postToSlack("info", $this->getName(), "workflowId: $workflowId");

        // check if workflowId provided is valid
        $validWorkflowId = $this->checkValidWorkflowId($workflowId);

        if (!$validWorkflowId) {
            $output->writeln("workflowId {$workflowId} not found");
            $generalService->postToSlack("error", $this->getName(), "workflowId {$workflowId} not found");
            return;
        }

        $workflow = $hubSpotWorkflowService->getWorkflow($workflowId);
        
        // get first email campaign id in workflow
        if (!array_key_exists('actions', $workflow)) {
            $output->writeln("no actions on workflow {$workflow['name']} {$workflowId}");
            $generalService->postToSlack("error", $this->getName(), "no actions on workflow {$workflow['name']} {$workflowId}");
            return;
        }
        $emailCampaignId = null;
        $actions = $workflow['actions'];
        foreach ($actions as $action) {
            if ($action['type'] == 'EMAIL') {
                if (!array_key_exists('emailCampaignId', $action)) {
                    $output->writeln("no email campaign id on email action");
                    $generalService->postToSlack("error", $this->getName(), "no email campaign id on email action");
                    return;
                }
                $emailCampaignId = $action['emailCampaignId'];
                break;
            }
        }

        $endDate = $input->getOption('endDate');
        if (!$endDate) {
            $endDate = date('m/d/Y');
        }
        $emailSendDate = $endDate;
        $endDate .= ' 08:00:00 GMT-0700';
        $generalService->postToSlack("info", $this->getName(), "End date: $endDate");

        $startDate = $input->getOption('startDate');
        if (!$startDate) {
            $startDate = date('m/d/Y', strtotime($endDate . ' - 1 days'));
        }
        $startDate .= ' 08:00:00 GMT-0700';
        $generalService->postToSlack("info", $this->getName(), "Start date: $startDate");

        $endTimestamp = strtotime($endDate) * 1000;
        $startTimestamp = strtotime($startDate) * 1000;
        $events = $hubSpotEmailService->getEmailEvents(null, $emailCampaignId, null, 'SENT', $startTimestamp, $endTimestamp);
        $bounceEvents = $hubSpotEmailService->getEmailEvents(null, $emailCampaignId, null, 'BOUNCE', $startTimestamp, $endTimestamp);
        $droppedEvents = $hubSpotEmailService->getEmailEvents(null, $emailCampaignId, null, 'DROPPED', $startTimestamp, $endTimestamp);

        $emails = [];
        $bounces = [];
        $drops = [];
        $dropDates = [];
        foreach ($events as $event) {
            $email = $event['recipient'];
            $emails[] = $email;
        }

        // just in case we get a bounce that does not have a send today
        foreach ($bounceEvents as $bounceEvent) {
            $email = $bounceEvent['recipient'];
            if (!in_array($email, $emails)) {
                $emails[] = $email;
            }
        }

        // just in case we get a drop that does not have a send today
        foreach ($droppedEvents as $droppedEvent) {
            $email = $droppedEvent['recipient'];
            if (!in_array($email, $emails)) {
                $emails[] = $email;
            }
        }

        // all relevant emails are added,
        // set the bounces and drops for everyone
        foreach ($emails as $email) {
            $bounces[$email] = 0;
            $drops[$email] = 0;
        }

        foreach ($bounceEvents as $bounceEvent) {
            $email = $bounceEvent['recipient'];
            $bounces[$email] += 1;
        }

        foreach ($droppedEvents as $droppedEvent) {
            $email = $droppedEvent['recipient'];
            $drops[$email] += 1;

            $tempDate = new \DateTime();
            $tempDate->setTimestamp($droppedEvent['created'] / 1000);
            $tempDate->setTimezone(new \DateTimeZone('UTC'));
            $tempDate = $tempDate->format('Y-m-d');

            $dropDates[$email] = $tempDate;
        }

        $members = [];
        // HubSpot can only handle requests for 100 records at a time
        // but googling php array chunk pays off sometimes
        $emailChunks = array_chunk($emails, 100);
        foreach ($emailChunks as $emailChunk) {
            $query = array(
                'formSubmissionMode' => 'none',
                'property' => array(
                    'email',
                    'firstname',
                    'lastname',
                    'participant_id',
                    'plan_id',
                    'partner_id',
                    'status_code',
                    'hubspot_test_last_email_send_date'
                )
            );
            $chunkMembers = $hubSpotService->getGroupByEmail($emailChunk, $query);
            $members = array_merge($members, $chunkMembers);
        }

        $path = './';
        if ($input->getOption('outputPath'))
        {
            $this->outputPath = $input->getOption('outputPath');
        }
        if (file_exists($this->outputPath)) {
            $path = $this->outputPath;
        }
        $filenameDate = date('Ymd', strtotime($emailSendDate));
        $startingfilename = "Pentegra";
        if ($input->getOption('startingfilename'))
        {
            $startingfilename = $input->getOption('startingfilename');
        }
        $filename = "{$startingfilename}_{$filenameDate}.csv";
        $generalService->postToSlack("info", $this->getName(), "Output filename: {$path}{$filename}");
        $file = fopen($path . $filename, 'w');
        fputcsv($file, array(
            'Email',
            'First Name',
            'Last Name',
            'Participant ID',
            'Emails Bounced',
            'Emails Dropped',
            'First email send date',
            'Plan Id',
            'Partner Id',
            'Status Code'
        ));
        
        $emailsProcessed = [];
        if (count($members) > 0) {
            foreach ($members as $member) {
                $properties = $member['properties'];
                $email = $this->getValue('email', $properties);
                $sendDate = $emailSendDate;
                if (array_key_exists($email, $dropDates)) {
                    $sendDate = $dropDates[$email];
                }
                $bounce = $bounces[$email] > 0 ? 1 : 0;

                if ($bounce) {
                    $sendDate = new \DateTime();
                    $sendDate->setTimestamp($this->getValue('hubspot_test_last_email_send_date', $properties) / 1000);
                    $sendDate->setTimezone(new \DateTimeZone('UTC'));
                    $sendDate = $sendDate->format('Y-m-d');
                }

                $drop = $drops[$email] > 0 ? 1 : 0;
                fputcsv($file, array(
                    $email,
                    $this->getValue('firstname', $properties),
                    $this->getValue('lastname', $properties),
                    $this->getValue('participant_id', $properties),
                    $bounce,
                    $drop,
                    $sendDate,
                    $this->getValue('plan_id', $properties),
                    $this->getValue('partner_id', $properties),
                    $this->getValue('status_code', $properties)
                ));
                $emailsProcessed[] = $email;
            }
        }
        fclose($file);
        $generalService->postToSlack("info", $this->getName(), "Emails processed: " . implode(",", $emailsProcessed));

        $skipUpload = $input->getOption('skipUpload');
        if (!$skipUpload) {
            try {
                $generalService->postToSlack("info", $this->getName(), "Starting upload");
                $vwiseSftpDir = $input->getOption('vwiseSftpDir');
                if (!empty($vwiseSftpDir)) {
                    $output->writeln("uploading to vwise sftp");
                    $remotePath = rtrim($vwiseSftpDir,'/') . '/';
                    $vwiseSftpUsername = $this->getContainer()->getParameter('vwise_sftp_username');
                    $vwiseSftpPassword = $this->getContainer()->getParameter('vwise_sftp_password');
                    $vwiseSftpHost = $this->getContainer()->getParameter('vwise_sftp_host');
                    $vwiseSftpPort = (int) $this->getContainer()->getParameter('vwise_sftp_port');
                    $sftp = new SFTPConnection($vwiseSftpHost, $vwiseSftpPort);
                    $sftp->login($vwiseSftpUsername, $vwiseSftpPassword);
                    $sftp->uploadFile($path . $filename, $remotePath . $filename);
                }
                else {
                    $output->writeln('uploading');
                    $username = $this->getContainer()->getParameter('pentegraUser');
                    $password = $this->getContainer()->getParameter('pentegraPassword');
                    $sftp = new SFTPConnection('sftp.pentegra.com');
                    $sftp->login($username, $password);
                    $sftp->uploadFile($path . $filename, $filename);
                }
                $generalService->postToSlack("info", $this->getName(), "Upload succeeded");
            } catch (\Exception $ex) {
                $generalService->postToSlack("error", $this->getName(), "Upload failed");
                throw $ex;
            }
        }
        $generalService->postToSlack("info", $this->getName(), "Finished");
    }
    
    public function getValue($name, $properties) {
        $value = null;
        if (array_key_exists($name, $properties) && array_key_exists('value', $properties[$name])) {
            $value = $properties[$name]['value'];
        }
        return $value;
    }

    private function checkValidWorkflowId($workflowId) {
        $hubSpotWorkflowService = $this->getContainer()->get('hub_spot.workflows');
        $workflows = $hubSpotWorkflowService->getWorkflows();

        foreach ($workflows as $workflow) {
            if ($workflow['id'] == $workflowId) {
                return true;
            }
        }
        return false;
    }

}