<?php

namespace HubSpot\HubSpotBundle\Command;

use classes\classBundle\Entity\launchToolRecipients;
use classes\classBundle\Entity\plans;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use HubSpot\HubSpotBundle\Services\HubSpotContactsService;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HubSpotDataMigrationCommand extends ContainerAwareCommand {
    
    /** @var HubSpotContactsService */
    private $hubSpotService;
    
    private $invalidEmails = [];
    
    private $invalidPlans = [];
    
    private $emailPropertiesMap = [];

    /** @var EntityRepository */
    private $enrolleeRepository;
    
    /** @var EntityManager */
    private $entityManager;
    
    /** @var Logger */
    private $logger;
    
    protected function configure()
    {
        $this
            ->setName('app:HubSpotDataMigration')
            ->setDescription('Sends all enrollees to HubSpot that are updated and not in an excluded plan')
            ->setHelp('Sends all enrollees to HubSpot that are updated and not in an excluded plan')
            ->addArgument('limit', InputArgument::OPTIONAL, "Minimum number of records to process")
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager = $this->getContainer()->get("doctrine")->getManager();
        $this->hubSpotService = $this->getContainer()->get('hub_spot.contacts');
        $this->logger = $this->getContainer()->get("logger");
        $limit = $input->getArgument("limit");
        
        $this->enrolleeRepository = $this->entityManager->getRepository('classesclassBundle:launchToolRecipients');
        $plansRepository = $this->entityManager->getRepository('classesclassBundle:plans');
        
        /* @var $statement QueryBuilder */
        $statement = $this->enrolleeRepository->createQueryBuilder('r')
            ->select('r.id')
            ->innerJoin('classesclassBundle:plans', 'p', 'WITH', 'p.id = r.planid')
            ->innerJoin('classesclassBundle:accounts', 'a', 'WITH', 'a.id = p.userid')
            ->distinct()
            ->where("r.contactStatus IN ('NEW', 'UPDATED')")
            ->andWhere('a.isExcludedFromHubSpot = 0')
            ->andWhere('p.isExcludedFromHubSpot = 0')
            ->andWhere('p.deleted = 0')
            ->setMaxResults($limit)
            ->getQuery();
        $enrolleeIds = array_column($statement->getResult(), 'id');
        
        if (count($enrolleeIds) === 0) {
            return;
        }
        
        $this->logger->info("hubspot process started");

        foreach ($enrolleeIds as $enrolleeId) {
            /* @var $enrollee launchToolRecipients */
            $enrollee = $this->enrolleeRepository->find($enrolleeId);
            /* @var $plan plans */
            $plan = $plansRepository->find($enrollee->planid);
            
            $hasError = false;
            
            if (empty($plan->participantLoginWebsiteAddress)) {
                $this->addPlanError($plan->partnerid, $plan->planid, "No Website");
                $hasError = true;
            }
            
            if (empty($plan->HRFirst) || empty($plan->HRLast) || empty($plan->HREmail)) {
                $this->addPlanError($plan->partnerid, $plan->planid, "No HR");
                $hasError = true;
            }
            
            if (false === ($properties = $this->createArray($plan, $enrollee))) {
                $this->addEmailError($plan->partnerid, $plan->planid, "No Email - {$enrollee->firstName}, {$enrollee->lastName}");
                $hasError = true;
            }
            
            if ($hasError) {
                $enrollee->contactStatus = "ERROR";
                continue;
            }

            $this->emailPropertiesMap[$properties['email']] = $properties;
            $result = $this->hubSpotService->addToBatch($properties['email'], $properties);
            $this->processResponse($result);   
        }
        
        $this->logger->info("hubspot process finished");
        
        $this->entityManager->flush();
        $result = $this->hubSpotService->flushBatch();
        $this->processResponse($result);
        
        if (count($this->invalidEmails) > 0) {
            $parameters = ["invalidEmails" => $this->invalidEmails];
            $body = $this->getContainer()->get("templating")->render("HubSpotHubSpotBundle:Email:invalidEmailList.html.twig", $parameters);
            $message = \Swift_Message::newInstance()
                ->setSubject("bad emails")
                ->setFrom("support@smartplanenterprise.com")
                ->setTo($this->getContainer()->getParameter("hubspot_error_email"))
                ->setBody($body, 'text/html');
            $this->getContainer()->get('mailer')->send($message);
        }
        
        if (count($this->invalidPlans) > 0) {
            $parameters = ["invalidPlans" => $this->invalidPlans];
            $body = $this->getContainer()->get("templating")->render("HubSpotHubSpotBundle:Email:invalidPlanList.html.twig", $parameters);
            $message = \Swift_Message::newInstance()
                ->setSubject("missing hubspot info")
                ->setFrom("support@smartplanenterprise.com")
                ->setTo($this->getContainer()->getParameter("hubspot_error_email"))
                ->setBody($body, 'text/html');
            $this->getContainer()->get('mailer')->send($message);
        }

    }
    
    private function createArray(plans $plan, launchToolRecipients $enrollee) {
        $email = trim($enrollee->email);
        $properties = [];
        if ($email == 'Email' || $email == '') {
            return false;
        }
        
        $properties['participant_id'] = $enrollee->id;
        $properties['plan_id'] = $plan->planid;
        $properties['partner_id'] = $plan->partnerid;
        $properties['firstname'] = $enrollee->firstName;
        $properties['lastname'] = $enrollee->lastName;
        $statusDescription = '';
        $enrolled = strtolower(trim($enrollee->enrolled));
        if (in_array($enrolled, ['y', 'yes'])) {
            $statusCode = 1;
            $statusDescription = 'Enrolled';
        }
        else if (in_array($enrolled, ['n', 'no'])) {
            $statusCode = 0;
            $statusDescription = 'Not Enrolled';
        }
        if (isset($statusCode)) {
            $properties['status_code'] = $statusCode;
        }
        $properties['status_code_description'] = $statusDescription;
        $properties['mobilephone'] = '';
        $properties['email'] = $email;
        $properties['first_eligible_status_date'] = '';
        $properties['last_eligible_status_date'] = '';
        $properties['status_code_at_enrollment'] = '';
        $properties['provider_logo'] = !empty($plan->providerLogoEmailImage) ? $plan->providerLogoEmailImage : $plan->providerLogoImage;
        $properties['sponsor_logo'] = !empty($plan->sponsorLogoEmailImage) ? $plan->sponsorLogoEmailImage : $plan->sponsorLogoImage;
        $properties['smart_plan'] = ($plan->smartPlanAllowed ? 'Y' : 'N');
        $properties['smart_enroll'] = ($plan->smartEnrollAllowed ? 'Y' : 'N');
        $properties['smart_plan_participant_login_website_address'] = ($plan->smartPlanAllowed ? $plan->participantLoginWebsiteAddress : '');
        $partLoginWebsiteAddress = $plan->participantLoginWebsiteAddress;
        if (!empty($partLoginWebsiteAddress)) {
            $partLoginWebsiteAddress .= (strrpos($partLoginWebsiteAddress, "?") !== false) ? "&" : "?";
            $partLoginWebsiteAddress .= "smartenrollpartid=" . $enrollee->smartEnrollPartId;
        }
        $properties['smart_enroll_participant_login_website_address'] = ($plan->smartEnrollAllowed ? $partLoginWebsiteAddress : '');
        $properties['hr_contact_first_name'] = $plan->HRFirst;
        $properties['hr_contact_last_name'] = $plan->HRLast;
        $properties['hr_contact_email'] = $plan->HREmail;
        $properties['support_contact_first_name'] = $plan->supportContactFirstName;
        $properties['support_contact_last_name'] = $plan->supportContactLastName;
        $properties['contact_email'] = $plan->supportContactEmail;
        $properties['contact_phone'] = $plan->customerServiceNumber;
        if (!is_null($plan->lastDayForLoans)) {
            $properties['last_day_for_loans'] = $plan->lastDayForLoans->format('m/d/Y');
        }
        if (!is_null($plan->freezeTransactionsDate)) {
            $properties['freeze_transactions_date'] = $plan->freezeTransactionsDate->format('m/d/Y');
        }
        if (!is_null($plan->takeoverDate)) {
            $properties['takeover_date'] = $plan->takeoverDate->format('m/d/Y');
        }
        if (!is_null($plan->freezeEndDate)) {
            $properties['freeze_end_date'] = $plan->freezeEndDate->format('m/d/Y');
        }
        $properties['sox_notice_url'] = $plan->soxNoticeURL;
        $properties['transition_period_over'] = $plan->transitionPeriodOver ? 'Yes' : 'No';
        $properties['plan_name'] = $plan->name;
        $properties['update_time'] = $enrollee->lastModified;
        
        return $properties;
    }

    private function processResponse($result) {
        if ($result !== HubSpotContactsService::BATCH_ENQUEUED) {
            
            $invalidEmails = $this->hubSpotService->getInvalidEmails(); 
            $invalidEmailsHashArray = array_flip($invalidEmails);
            
            if (count($invalidEmailsHashArray) > 0) {
                // Extract valid emails and try to resend to hubspot
                $validEmails = array_diff_key($this->emailPropertiesMap, $invalidEmailsHashArray);
                if (!$this->hubSpotService->replaceGroup($validEmails)) {
                    $invalidEmailsHashArray = array_fill_keys(array_keys($this->emailPropertiesMap), 1);
                }
            }
            else if ($result === HubSpotContactsService::BATCH_ERROR) {
                $invalidEmailsHashArray = array_fill_keys(array_keys($this->emailPropertiesMap), 1);
            }
            
            
            foreach (array_keys($this->emailPropertiesMap) as $email) {
                if (isset($invalidEmailsHashArray[$email])) {
                    $status = "ERROR";
                    $this->addEmailError($this->emailPropertiesMap[$email]['partner_id'], $this->emailPropertiesMap[$email]['plan_id'], $email);
                }
                else {
                    $status = "N/A";
                }
                if (isset($this->emailPropertiesMap[$email])) {
                    $enrollee = $this->enrolleeRepository->find($this->emailPropertiesMap[$email]['participant_id']);
                    $enrollee->contactStatus = $status;
                }
            }

            $this->entityManager->flush();
            $this->hubSpotService->clearInvalidEmails();
            $this->emailPropertiesMap = [];
        }
    }
    
    private function addPlanError($partnerid, $planid, $type) {
        $this->invalidPlans[$partnerid][$planid][$type] = true;
    }
    
    private function addEmailError($partnerid, $planid, $name) {
        $this->invalidEmails[$partnerid][$planid][] = $name;
    }
}
