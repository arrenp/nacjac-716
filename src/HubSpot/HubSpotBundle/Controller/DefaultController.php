<?php

namespace HubSpot\HubSpotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('HubSpotHubSpotBundle:Default:index.html.twig', array('name' => $name));
    }
}
