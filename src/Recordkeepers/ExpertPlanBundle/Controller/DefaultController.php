<?php

namespace Recordkeepers\ExpertPlanBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use classes\classBundle\Entity\recordkeeperPlans;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansRiskBasedFunds;
use Shared\RecordkeepersBundle\Classes\recordKeeper;
class DefaultController extends Controller
{
	public function indexAction()
	{
		$plans= $this->planlist();
		$request = Request::createFromGlobals();
		$planid = $request->query->get('planid','');
		$portfolios = $this->portfoliolist($planid);
		$riskBasedFunds = "";
		$em = $this->getDoctrine();
		$recordKeeper = new recordKeeper($em);
		return new Response($recordKeeper->test($plans,$portfolios,$riskBasedFunds));
	}

	public function updatePortfoliosAction()
	{
		$request = Request::createFromGlobals();
		$planid = $request->request->get('planid','');
		$em = $this->getDoctrine();
		$portfolioList= $this->portfoliolist($planid);
		$session = $this->getRequest()->getSession();
		$userid = $session->get("userid");
		$planid = $session->get("planid");
		$riskType = $request->request->get('riskType','');
		$addPortfolios = new recordKeeper($em);
		$addPortfolios->updateplan($userid,"planid",$request->request->get('planid',''),$planid);
		$addPortfolios->updateRiskType($userid,$planid,$riskType);
		return new Response($addPortfolios->updatePortfolios($portfolioList,$userid,$planid));

	}
	public function selectPlanAction()
	{
		$em = $this->getDoctrine();
		$request = Request::createFromGlobals();
		$session = $this->getRequest()->getSession();
		$userid = $session->get("userid");
		$planid = $session->get("planid");
		$currentplan = new recordKeeper($em);
		return $currentplan->updateplan($userid,"planid",$request->request->get('planid',''),$planid, $request->request->get('spe2', ''), $request->request->get('initialplan', ''));
	}

	public function updatePlansAction()
	{
		$em = $this->getDoctrine();
		$planlist= $this->planlist();
		$session = $this->getRequest()->getSession();
		$userid = $session->get("userid");
		$addplans = new recordKeeper($em);
		return new Response($addplans->updateplans($planlist,$userid));
	}

	private function portfolioList($planid)
	{
		$rawXml = $this->portfoliosXML($planid);
		$xml = new \SimpleXMLElement($rawXml);
		$portfolios = array();
		$i = 0;
		foreach($xml as $Portfolio)
		$portfolios[$i++] = $Portfolio->Name;
		return $portfolios;

	}
	public function portfoliolistAction($planid)
	{
		$em = $this->getDoctrine();
		$request = Request::createFromGlobals();
		$recordkeeper = new recordKeeper($em);
		$portfolios = $this->portfoliolist($planid);
		$portfoliosString = $recordkeeper->investmentString($portfolios);
		return new Response($portfoliosString);
	}	

	private function planlist()
	{
		$rawXml = $this->plansXML();
		$response = $rawXml;
		$rawXml = str_replace("�","'",$rawXml);

		$rawXml = str_replace("&", "&amp;", $rawXml);
		$xml = new \SimpleXMLElement($rawXml);

		$PLAG_IDI = array();
		$pla_idi = array();
		$pla_code_cid = array();
		$PlanName = array();

		$i=0;
		foreach ($xml as $plan)
		{
			$pList[$i]['PLANID'] = trim($plan->Id);
			$pList[$i]['PLANNAM'] = trim($plan->Name);
			$pList[$i]['PLANTYPECD'] = "";
			$i++;
		}

		return $pList;
	}
	private function portfoliosXML($planid)
	{

		$url = $this->url();
		$url = "https://$url/VWise.mvc?action=GetPortfolioData&epPlan=$planid";
		$rawXml = file_get_contents($url);
		return str_replace("&","and",trim($rawXml));

	}
	private function plansXML()
	{
		$dbuser = $this->user();
		$url = $this->url();
		$plansurl = "https://$url/VWise.mvc?action=GetPlansData&userid=$dbuser";
		$rawXml = file_get_contents($plansurl);
		$rawXml = trim($rawXml);
		return $rawXml;
	}
	private function url()
	{
		$session = $this->getRequest()->getSession();
		$userid = $session->get("userid");

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
		$currentmember = $repository->findOneBy(array('id' => $userid));
		$url = $currentmember->recordkeeperUrl;

		return $url;

	}
	private function user()
	{
		$session = $this->getRequest()->getSession();
		$userid = $session->get("userid");

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
		$currentmember = $repository->findOneBy(array('id' => $userid));
		$user = $currentmember->recordkeeperUser;

		return $user;

	}
        
    private function getRequest() {
        return $this->get('request_stack')->getCurrentRequest();
    }

}
