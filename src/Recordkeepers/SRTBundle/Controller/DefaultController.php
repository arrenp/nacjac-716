<?php

namespace Recordkeepers\SRTBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use classes\classBundle\Entity\recordkeeperPlans;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansRiskBasedFunds;
use Shared\RecordkeepersBundle\Classes\recordKeeper;
use classes\classBundle\Entity\accounts;

error_reporting(0);

class DefaultController extends Controller
{

    public function indexAction()
    {
        $request = Request::createFromGlobals();
        $planid = $request->query->get('planid', '');
        $plantypecd = $request->query->get('plantypecd', '');
        $plans = $this->planlist();
        $portfolios = $this->portfoliolist($planid);
        $riskBasedFunds = $this->riskBasedFundslist($plantypecd);
        $em = $this->getDoctrine();
        $recordKeeper = new recordKeeper($em);
        return new Response($recordKeeper->test($plans, $portfolios, $riskBasedFunds));
    }

    public function updatePlansAction()
    {
        $em = $this->getDoctrine();
        $planlist = $this->planlist();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $addplans = new recordKeeper($em);
        return new Response($addplans->updateplans($planlist, $userid));
    }

    public function selectPlanAction()
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $currentplan = new recordKeeper($em);
        return $currentplan->updateplan($userid, "plantypecd", $request->request->get('plantypecd', ''), $planid, $request->request->get('spe2', ''), $request->request->get('initialplan', ''));
    }

    public function updatePortfoliosAction()
    {
        $request = Request::createFromGlobals();
        $planid = $request->request->get('planid', '');
        $em = $this->getDoctrine();
        $portfolioList = $this->portfoliolist($planid);
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $riskType = $request->request->get('riskType', '');
        $addPortfolios = new recordKeeper($em);
        $addPortfolios->updateplan($userid, "plantypecd", $request->request->get('plantypecd', ''), $planid);
        $addPortfolios->updateRiskType($userid, $planid, $riskType);
        return new Response($addPortfolios->updatePortfolios($portfolioList, $userid, $planid));
    }

    public function updateRiskBasedFundsAction()
    {
        $request = Request::createFromGlobals();
        $plantypecd = $request->request->get('plantypecd', '');
        $em = $this->getDoctrine();
        $riskBasedFundsList = $this->riskBasedFundslist($plantypecd);
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $riskType = $request->request->get('riskType', '');
        $addRiskedBasedFunds = new recordKeeper($em);
        $addRiskedBasedFunds->updateplan($userid, "plantypecd", $request->request->get('plantypecd', ''), $planid);
        $addRiskedBasedFunds->updateRiskType($userid, $planid, $riskType);
        return new Response($addRiskedBasedFunds->updateRiskBasedFunds($riskBasedFundsList, $userid, $planid));
    }

    public function updateAdviceAction()
    {

        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $planid = $request->request->get('planid', '');
        $adviceList = $this->advicelist($planid);
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $recordkeeper = new recordKeeper($em);
        $recordkeeper->toggleAdvice($adviceList, $userid, $planid);
        return new Response("saved");
    }

    public function adviceListAction()
    {
        $request = Request::createFromGlobals();
        $planid = $request->query->get('planid', '');
        $em = $this->getDoctrine();
        $adviceList = $this->advicelist($planid);
        return $this->render('SharedRecordkeepersBundle:Advice:index.html.twig', array("adviceList" => $adviceList));
    }

    private function advicelist($planid)
    {
        return $this->get("SRTServiceAdmin")->advicelist($planid);
    }

    private function portfoliolist($planid)
    {

        $portfolioXML = $this->portfoliodataXML($planid);
        $rawXml = str_replace(":", "", $portfolioXML);
        $rawXml = str_replace("boolean", "string", $rawXml);
        $rawXml = str_replace("dateTime", "string", $rawXml);
        $wddx = new \SimpleXMLElement($rawXml);
        $wddx = utf8_encode($wddx);
        $data = wddx_deserialize($wddx);

        $portNames = array();
        foreach ($data as $var)
        {
            // Names
            foreach ($var as $name => $field)
            {
                if ($name == "porf_na")
                {
                    foreach ($field as $value)
                    {
                        $portNames[] = $value;
                    }
                }
            }
        }

        // remove duplicates
        $portNames = array_unique($portNames);
        $portNames = array_merge($portNames);



        return $portNames;
    }

    public function portfoliolistAction($planid)
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $recordkeeper = new recordKeeper($em);
        $portfolios = $this->portfoliolist($planid);
        $portfoliosString = $recordkeeper->investmentString($portfolios);
        return new Response($portfoliosString);
    }

    public function riskBasedFundslistAction($plantypecd)
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $recordkeeper = new recordKeeper($em);
        $riskBasedFunds = $this->riskBasedFundslist($plantypecd);
        $riskBasedFundsString = $recordkeeper->investmentString($riskBasedFunds);
        return new Response($riskBasedFundsString);
    }

    private function planlist()
    {
        $rawXml = $this->plansXML();
        //echo $rawXml;
        $wddx = new \SimpleXMLElement($rawXml);
        $wddx = utf8_encode($wddx);
        $data = wddx_deserialize($wddx);

        $PLAG_IDI = array();
        $pla_idi = array();
        $pla_code_cid = array();
        $PlanName = array();
        $namecounter = 0;
        $idcounter = 0;
        $typecdcounter = 0;

        foreach ($data as $var)
        {
            foreach ($var as $name => $field)
            {

                foreach ($field as $value)
                {
                    switch ($name)
                    {

                        case 'pla_idi':
                            $pList[$idcounter++]['PLANID'] = $value;
                            break;

                        case 'pla_code_cid':
                            $pList[$typecdcounter++]['PLANTYPECD'] = $value;
                            break;

                        case 'PlanName':
                            $pList[$namecounter++]['PLANNAM'] = $value;
                            break;
                    }
                }
                $i++;
            }
        }
        $counter = $idcounter;
        if ($counter < $typecdcounter) $counter = $typecdcounter;
        if ($counter < $namecounter) $counter = $namecounter;

        for ($i = 0; $i < $counter; $i++) {
            if (trim($pList[$i]['PLANNAM']) != "") {
                $filteredPlist[] = $pList[$i];
            }
        }

        return $filteredPlist;
    }

    private function riskBasedFundslist($plantypecd)
    {
        $rawXml = $this->riskBasedFundsXML($plantypecd);
        $rawXml = explode("<ipm_na>", $rawXml);

        $count = count($rawXml);
        $j = 0;


        for ($i = 1; $i < $count; $i++)
        {
            $rawXml[$i] = explode("</ipm_na>", $rawXml[$i]);
            $rawXml[$i][0] = trim($rawXml[$i][0]);

            $ipms[$j] = "" . htmlspecialchars_decode($rawXml[$i][0]) . "";
            $j++;
            ////echo $rawXml[$i][0].'<br/>';
        }
        $ipms = array_unique($ipms);


        $ipms = array_merge($ipms);

        return $ipms;
    }

    private function plansXML()
    {
        $sid = $this->sid();
        //$Query = "06d40298-ba01-4925-9422-493e3227d21a";        
        $queryId = $this->getQueryId("plansQuery");
        $parameters = "";
        $recordkeeperUrl = $this->getRecordkeeperUrl();
        $url = "https://$recordkeeperUrl/SRTDB_tk.asmx/ExecuteQueryWDDX?sessionID=$sid&sqlID=$queryId&parameters=$parameters";
        $rawXml = file_get_contents($url);
        if (trim($rawXml) == "")
        return $sid;
        return $rawXml;
    }
    public function plansXMLAction()
    {
        //header('Content-type: text/xml');
        //$wddx = new \SimpleXMLElement($this->plansXML());
        //$wddx = utf8_encode($wddx);
        //$data = wddx_deserialize($wddx);
        
        echo "<pre>";
        echo '<code style = "color:black">';
        echo $this->plansXML();
        echo "</code>";
        echo "</pre>";
        
        
        return new Response("");
    }
    public function portfoliosXMLAction(Request $request)
    {
        
        $planid = $request->query->get("planid");
         
        return new Response($this->portfoliodataXML($planid));
    }
    public function riskBasedFundsXMLAction(Request $request)
    {
        echo '<pre >';
        echo '<code style = "color:black">';
        $plantypecd = $request->query->get("plantypecd");
        echo htmlentities($this->riskBasedFundsXML($plantypecd));
        echo "</code>";
        echo "</pre>";
        return new Response("");
    }    

    private function riskBasedFundsXML($plantypecd)
    {
        
        $sid = $this->sid();
        //$Query = "e0cc6ad8-9160-4565-8473-25188ef3c324";
        $queryId = $this->getQueryId("riskBasedFundsQuery");
        $parameters = "'$plantypecd'";
        $recordkeeperUrl = $this->getRecordkeeperUrl();
        $url = "https://$recordkeeperUrl/SRTDB_tk.asmx/ExecuteQuery?sessionID=$sid&sqlID=$queryId&parameters=$parameters";
        $rawXml = file_get_contents($url);
        return $rawXml;
    }

    private function portfoliodataXML($planid)
    {
        $sid = $this->sid();
        //$Query = "33a61d6e-698c-46a5-b958-83ea457f0dba";
        $queryId = $this->getQueryId("portfolioQuery");
        $parameters = "'$planid','$planid'";
        $recordkeeperUrl = $this->getRecordkeeperUrl();
        $url = "https://$recordkeeperUrl/SRTDB_tk.asmx/ExecuteQueryWDDX?sessionID=$sid&sqlID=$queryId&parameters=$parameters";
        $rawXml = file_get_contents($url);
        return $rawXml;
    }

    private function advicedataXML($planid)
    {
        return $this->get("SRTServiceAdmin")->advicedataXML($planid);
    }
    
    public function advicedataXMLAction(Request $request)
    {
        //header('Content-type: text/xml');
        $planid = $request->query->get("planid");
        echo "<pre>";
        echo '<code style = "color:black">';
        echo $this->advicedataXML($planid);
        echo "</code>";
        echo "</pre>";
        return new Response("");
    }

    private function sid()
    {
        return $this->get("SRTServiceAdmin")->sid();
    }
    
    private function getQueryId($type) {
        return $this->get("SRTServiceAdmin")->getQueryId($type);
    }
    
    private function getRecordkeeperUrl() {
        return $this->get("SRTServiceAdmin")->getRecordkeeperUrl();
    }
    
    public function updatePlanIdsAction()
    {
        $session = $this->getRequest()->getSession();
        if ($session->get("masterRoleType") == "vwise_admin")
        {
            $em = $this->getDoctrine();
            $this->updatePlansAction();
            $recordKeeper = new recordKeeper($em);
            $recordKeeper->updatePlanIds($session->get("userid"), $this->container->get('doctrine.dbal.default_connection'),'plantypecd');
        }
        return new Response("");
    }
    
    private function getRequest() {
        return $this->get('request_stack')->getCurrentRequest();
    }

}
