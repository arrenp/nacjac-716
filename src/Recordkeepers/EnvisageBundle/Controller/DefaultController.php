<?php

namespace Recordkeepers\EnvisageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use classes\classBundle\Entity\recordkeeperPlans;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansRiskBasedFunds;
use Shared\RecordkeepersBundle\Classes\recordKeeper;

class DefaultController extends Controller
{

    public function indexAction()
    {
        $plans = $this->planlist();
        $request = Request::createFromGlobals();
        $portfolios = "";
        $riskBasedFunds = "";
        $em = $this->getDoctrine();
        $recordKeeper = new recordKeeper($em);
        return new Response($recordKeeper->test($plans, $portfolios, $riskBasedFunds));
    }

    public function updatePlansAction()
    {
        $em = $this->getDoctrine();
        $planlist = $this->planlist();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $addplans = new recordKeeper($em);
        return new Response($addplans->updateplans($planlist, $userid));
    }

    public function selectPlanAction()
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $currentplan = new recordKeeper($em);
        return $currentplan->updateplan($userid, "plantypecd", $request->request->get('plantypecd', ''), $planid, $request->request->get('initialplan', ''));
    }

    private function planlist()
    {
        $rawXml = $this->plansXML();
        $array1 = explode("<Plan>", $rawXml);
        $pCount = 0;
        $beenthrough = false;

        for ($i = 0; $i < count($array1); $i++)
        {
            $array1[$i] = str_replace('<Id>', '', $array1[$i]);
            $array1[$i] = explode('<Name>', $array1[$i]);


            if (isset($array1[$i][0]))
            {
                $pla_code_cid[$i - 1] = $array1[$i][0];
                $pCount++;
            }

            if (isset($array1[$i][1]))
            {
                $PlanName[$i - 1] = $array1[$i][1];
            }

            $beenthrough = false;

            if ($i >= 0) $pla_idi[$i - 1] = "";
        }
        $count = count($PlanName);
        for ($i = 0; $i < $count; $i++)
        {
            $pList[$i]['PLANNAM'] = trim($PlanName[$i]);
            $pList[$i]['PLANTYPECD'] = trim($pla_code_cid[$i]);
            $pList[$i]['PLANID'] = trim($pla_idi[$i]);
            //$pList[$i]['PLANID'] = trim($pla_code_cid[$i]);;
            //$pList[$i]['PLANTYPECD'] = "";
        }
        return $pList;
    }

    private function plansXMLPreFilter()
    {
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $currentmember = $repository->findOneBy(array('id' => $userid));

        $url = $currentmember->recordkeeperUrl;
        $dbuser = $currentmember->recordkeeperUser;
        $dbpass = $currentmember->recordkeeperPass;
        $url = $url . "?type=4&uid=" . $dbuser . "|" . $dbpass . "";
        $rawXml = file_get_contents($url);
        return $rawXml;
    }

    private function plansXML()
    {
        $rawXml = $this->plansXMLPreFilter();
        $rawXml = str_replace('<Plans xmlns="http://com/envisagesystems/vWise/plans">', '', $rawXml);
        $rawXml = str_replace('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '', $rawXml);
        $rawXml = str_replace('</Id>', '', $rawXml);
        $rawXml = str_replace('</Name>', '', $rawXml);
        $rawXml = str_replace('</Plan>', '', $rawXml);
        $rawXml = str_replace('<Plan/>', '', $rawXml);
        $rawXml = str_replace('</Plans>', '', $rawXml);
        return $rawXml;
    }

    public function plansXMLAction()
    {


        $xml = $this->plansXMLPreFilter();
        $Soap = new \SimpleXMLElement($xml);
        echo '<pre >';
        echo '<code style = "color:black">';
        echo htmlentities($Soap->asXML());
        echo "</code>";
        echo "</pre>";
        return new Response("");
    }
    
    public function updatePlanIdsAction()
    {
        $session = $this->getRequest()->getSession();
        if ($session->get("masterRoleType") == "vwise_admin")
        {
            $em = $this->getDoctrine();
            $this->updatePlansAction();
            $recordKeeper = new recordKeeper($em);
            $recordKeeper->updatePlanIds($session->get("userid"), $this->container->get('doctrine.dbal.default_connection'),'plantypecd');
        }
        return new Response("");
    }    
    
    private function getRequest() {
        return $this->get('request_stack')->getCurrentRequest();
    }
}
