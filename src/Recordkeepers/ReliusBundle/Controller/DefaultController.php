<?php

namespace Recordkeepers\ReliusBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use classes\classBundle\Entity\recordkeeperPlans;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansRiskBasedFunds;
use classes\classBundle\Entity\recordkeeperAdviceFunds;
use Shared\RecordkeepersBundle\Classes\recordKeeper;

error_reporting(0);

class DefaultController extends Controller
{

    public function indexAction()
    {

        $request = Request::createFromGlobals();
        $planid = $request->query->get('planid', '');
        $plans = $this->planlist();
        $portfolios = $this->portfoliolist($planid);
        $riskBasedFunds = $this->riskBasedFundslist($planid);
        $em = $this->getDoctrine();
        $recordKeeper = new recordKeeper($em);
        return new Response($recordKeeper->test($plans, $portfolios, $riskBasedFunds));
    }

    public function updatePortfoliosAction()
    {
        $request = Request::createFromGlobals();
        $planid = $request->request->get('planid', '');
        $em = $this->getDoctrine();
        $portfolioList = $this->portfoliolist($planid);
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $riskType = $request->request->get('riskType', '');
        $addPortfolios = new recordKeeper($em);
        $addPortfolios->updateplan($userid, "planid", $request->request->get('planid', ''), $planid);
        $addPortfolios->updateRiskType($userid, $planid, $riskType);
        return new Response($addPortfolios->updatePortfolios($portfolioList, $userid, $planid));
    }

    public function updateAdviceAction()
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $planid = $request->request->get('planid', '');
        $adviceList = $this->advicelist($planid);
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $recordkeeper = new recordKeeper($em);
        $recordkeeper->toggleAdvice($adviceList, $userid, $planid);
        return new Response("saved");
    }

    public function adviceListAction()
    {
        $request = Request::createFromGlobals();
        $planid = $request->query->get('planid', '');
        $em = $this->getDoctrine();
        $adviceList = $this->advicelist($planid);
        return $this->render('SharedRecordkeepersBundle:Advice:index.html.twig', array("adviceList" => $adviceList));
    }

    public function updateRiskBasedFundsAction()
    {
        $request = Request::createFromGlobals();
        $planid = $request->request->get('planid', '');
        $em = $this->getDoctrine();
        $riskBasedFundsList = $this->riskBasedFundslist($planid);
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $riskType = $request->request->get('riskType', '');
        $addRiskedBasedFunds = new recordKeeper($em);
        $addRiskedBasedFunds->updateplan($userid, "planid", $request->request->get('planid', ''), $planid);
        $addRiskedBasedFunds->updateRiskType($userid, $planid, $riskType);
        return new Response($addRiskedBasedFunds->updateRiskBasedFunds($riskBasedFundsList, $userid, $planid));
    }

    public function selectPlanAction()
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $planid = $session->get("planid");
        $currentplan = new recordKeeper($em);
        return $currentplan->updateplan($userid, "planid", $request->request->get('planid', ''), $planid, $request->request->get('spe2', ''), $request->request->get('initialplan', ''));
    }

    public function updatePlansAction()
    {
        $em = $this->getDoctrine();
        $planlist = $this->planlist();
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        $addplans = new recordKeeper($em);
        return new Response($addplans->updateplans($planlist, $userid));
    }

    private function advicelist($planid)
    {
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        return $this->get("ReliusService")->advicelist($planid, $userid);
    }

    public function portfoliolist($planid)
    {
        $Soap = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $this->plandataXML($planid));
        $Soap = new \SimpleXMLElement($Soap);
        $PlanInvestProd = $Soap->soapBody->GetPlanDataResponse->a_objPlanDataDS->diffgrdiffgram->PlanData->PlanInvProd;
        foreach ($PlanInvestProd as $data)
        {
            $da = (array) $data;
            foreach ($da as $name => $value)
            {
                if (isset($name) && isset($value) && $name == "DESCRIPTION" && trim($value) != "")
                {
                    $portfolios[$i++] = $value;
                }
            }
        }
        return $portfolios;
    }

    public function portfoliolistAction($planid)
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $recordkeeper = new recordKeeper($em);
        $portfolios = $this->portfoliolist($planid);
        $portfoliosString = $recordkeeper->investmentString($portfolios);
        return new Response($portfoliosString);
    }

    public function riskBasedFundslistAction($planid)
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $recordkeeper = new recordKeeper($em);
        $riskBasedFunds = $this->riskBasedFundslist($planid);
        $riskBasedFundsString = $recordkeeper->investmentString($riskBasedFunds);
        return new Response($riskBasedFundsString);
    }

    private function riskBasedFundslist($planid)
    {
        $Soap = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $this->plandataXML($planid));
        $Soap = new \SimpleXMLElement($Soap);
        $PlanInvestProd = $Soap->soapBody->GetPlanDataResponse->a_objPlanDataDS->diffgrdiffgram->PlanData->PlanInvest;
        foreach ($PlanInvestProd as $data)
        {
            $da = (array) $data;
            foreach ($da as $name => $value)
            {
                if (isset($name) && isset($value) && $name == "DESCRIPTION" && trim($value) != "")
                {
                    $riskBasedFunds[$i++] = $value;
                }
            }
        }
        return $riskBasedFunds;
    }
    
    public function portfoliosXMLAction(Request $request)
    {
        //header('Content-type: text/xml');
        $planid = $request->query->get("planid");
        $Soap = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $this->plandataXML($planid));
        $Soap = new \SimpleXMLElement($Soap);
        
        $PlanInvestProd = $Soap->soapBody->GetPlanDataResponse->a_objPlanDataDS->diffgrdiffgram->PlanData->PlanInvProd;

        foreach ($PlanInvestProd as $data)
        {
            $da = (array) $data;
            echo "<pre>";
            var_dump($data);
            echo "</pre>";
            
        }
        
        return new Response("");
    }
    public function riskBasedFundsXMLAction(Request $request)
    {
        //header('Content-type: text/xml');
        $planid = $request->query->get("planid");
        $Soap = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $this->plandataXML($planid));
        $Soap = new \SimpleXMLElement($Soap);
        
        $PlanInvestProd = $Soap->soapBody->GetPlanDataResponse->a_objPlanDataDS->diffgrdiffgram->PlanData->PlanInvest;

        foreach ($PlanInvestProd as $data)
        {
            $da = (array) $data;
            echo "<pre>";
            var_dump($data);
            echo "</pre>";
            
        }
        
        return new Response("");
    }    
    public function plansXMLAction()
    {
        $Soap = $this->plansXML();
        $Soap = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $Soap);
        $Soap = new \SimpleXMLElement($Soap);
        echo '<pre >';
        echo '<code style = "color:black">';
        echo htmlentities($Soap->asXML());
        echo "</code>";
        echo "</pre>";
        return new Response("");
    }
    

    private function planlist()
    {
        $Soap = $this->plansXML();
        $Soap = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $Soap);


        $Soap = new \SimpleXMLElement($Soap);

        $PlanData = $Soap->asXML();

        //echo $PlanData.'<br/>';
        $PlanData2 = $PlanData;
        $PlanData = str_replace("<PLANNAM>", "<SEPERATOR>", $PlanData);
        $PlanData = str_replace("<PLANID>", "<SEPERATOR>", $PlanData);
        $PlanData = str_replace("<PLANTYPECD>", "<SEPERATOR>", $PlanData);



        $PlanData = explode("<SEPERATOR>", $PlanData);
        $count = count($PlanData);


        $j = 0;
        $z = 0;

        for ($i = 1; $i < $count; $i++)
        {
            if ($j == 1)
            {
                $PlanData[$i] = explode("</PLANNAM>", $PlanData[$i]);
                $pList[$z]['PLANNAM'] = $PlanData[$i][0];
                $j = 2;
            }
            else if ($j == 2)
            {
                $PlanData[$i] = explode("</PLANTYPECD>", $PlanData[$i]);
                $pList[$z]['PLANTYPECD'] = $PlanData[$i][0];
                $z++;
                $j = 0;
            }
            else if ($j == 0)
            {
                $PlanData[$i] = explode("</PLANID>", $PlanData[$i]);
                $pList[$z]['PLANID'] = $PlanData[$i][0];
                $j = 1;
            }
        }
        $pCount = count($pList);
        if (isset($pList[0])) $pCount = count($pList);
        else $pCount = 0;
        return $pList;
    }

    private function plansXML()
    {
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        return $this->get("ReliusService")->plansXML($userid);
    }

    private function plandataXML($planid)
    {
        $session = $this->getRequest()->getSession();
        $userid = $session->get("userid");
        return $this->get("ReliusService")->plandataXML($planid, $userid);
    }

    public function test()
    {

        echo "cat";
    }
    
    public function updatePlanIdsAction()
    {
        $session = $this->getRequest()->getSession();
        if ($session->get("masterRoleType") == "vwise_admin")
        {
            $em = $this->getDoctrine();
            $this->updatePlansAction();
            $recordKeeper = new recordKeeper($em);
            $recordKeeper->updatePlanIds($session->get("userid"), $this->container->get('doctrine.dbal.default_connection'));
        }
        return new Response("");
    }
    
    private function getRequest() {
        return $this->get('request_stack')->getCurrentRequest();
    }

}
