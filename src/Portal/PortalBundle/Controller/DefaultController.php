<?php

namespace Portal\PortalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Classes\mailEngine;
use classes\classBundle\Entity\plansWidgetCampaigns;
class DefaultController extends Controller
{

    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Dashboard");
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:portalslider');
        //find if user name and password exists in members table
        $slides = $repository->findBy(array(), array('orderid' => 'ASC'));
        $slidescount = count($slides);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:portalnews');
        $happenings = $repository->findBy(array(), array('orderid' => 'DESC'));
        $userid = $session->userid;

        $connection = $this->get('doctrine.dbal.default_connection');
        $latestPlans = $connection->fetchAll("SELECT * FROM plans  WHERE userid = " . $userid . " AND deleted = '0' ORDER by id DESC LIMIT 5");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $lastModifiedPlan = $repository->findOneBy(array("userid" => $userid), array('modifiedDate' => 'DESC'), array('deleted' => '0'));
        $roleType = $session->roleType;

        return $this->render('PortalPortalBundle:Default:index.html.twig', array('slides' => $slides, 'slidescount' => $slidescount, 'happenings' => $happenings, "latestPlans" => $latestPlans, "lastModifiedPlan" => $lastModifiedPlan, "roleType" => $roleType));
    }

    public function widgetCreateCampaignAction(Request $request)
    {
        return $this->render('PortalPortalBundle:Default:widget/createcampaign.html.twig',array("request" => $request->query->all()));
    }
    public function widgetCreateCampaignSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();         
        $repository= $this->getDoctrine()->getRepository('classesclassBundle:campaignsCategories');
        $campaignCategory = $repository->findOneBy(array("name" => "Targeted Campaign"));        
        $repository= $this->getDoctrine()->getRepository('classesclassBundle:outreachNewCategories');
        $category = $repository->findOneBy(array("name" => "PowerView"));
        $repository= $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $template = $repository->findOneBy(array("name" => "PowerView","categoryid" => $category->id));        

        $repository= $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array("id" => $request->request->get("id"))); 
        
        $repository= $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array("id" => $plan->userid)); 
        
        $plansWidgetCampaigns = new plansWidgetCampaigns();
       
        $params = array("partnerid" => $plan->partnerid,"recordkeeperPlanid" => $plan->planid);
        $repository= $this->getDoctrine()->getRepository('classesclassBundle:plansRkp');
        $plansRkp = $repository->findOneBy($params);
       
        $mailEngine = new mailEngine("mailgunEngine",$this);
        $mailEngineParams = array();
        $mailEngineParams['communicationSpecialist'] = $account->communicationSpecialist;
        $mailEngineParams['userid'] = $plan->userid;
        $mailEngineParams['planid'] = $plan->id;
        $mailEngineParams['textBody'] ="";
        $mailEngineParams['templateid'] =$template->id;
        $mailEngineParams['categoryid'] =  $campaignCategory->id;
        $mailEngineParams['html'] = file_get_contents($this->generateUrl('_manage_outreach_new_preview', array("id" => $template->id,"planid" => $plan->id,"userid" => $plan->userid), true));
        $mailEngineParams['html'] = str_replace("<vwise_powerviewurl/>",$plansRkp->powerViewUrl,$mailEngineParams['html']);
        $mailEngineParams['html'] = str_replace("<vwise_powerviewloginurl/>",$plansRkp->participantLoginUrl,$mailEngineParams['html']);
        $mailEngineParams['description'] ="";
        $mailEngineParams['fromEmail'] = $plansRkp->emailAddressFrom;
        $mailEngineParams['fromName'] = $plansRkp->hrFirstName." ".$plansRkp->hrLastName;
        $title = "Widget Campaign ".$plan->id;
        $mailEngineParams['subject'] = $title;
        $mailEngineParams['title'] = $title;

        $plansWidgetCampaigns->campaignid = $mailEngine->createCampaign($mailEngineParams);
        $plansWidgetCampaigns->userid = $plan->userid;
        $plansWidgetCampaigns->planid = $plan->id;
        $plansWidgetCampaigns->type = "powerview";
        
        $emailParams['campaignid'] = $plansWidgetCampaigns->campaignid;
        $emailParams['emails'] = array();
        $emailCounter = 0;
        
        $widgets = $this->get("WidgetsRecordkeeper")->getWidgetBasedOffLoggedInAccount();
        $widgets->getLoggedInSid();
        $emailList = $widgets->getPlanEmailList();      
        foreach ($emailList  as $email)
        {
            $emailParams['emails'][$emailCounter]['emailAddress'] = $email->Email;
            $emailParams['emails'][$emailCounter]['firstName'] = $email->FirstName;
            $emailParams['emails'][$emailCounter]['lastName'] = $email->LastName;
            $emailCounter++;
        }
        $mailEngine->createList($emailParams);
        $em->persist($plansWidgetCampaigns);
        $em->flush();
        return new Response($plansWidgetCampaigns->campaignid);
    }
}
