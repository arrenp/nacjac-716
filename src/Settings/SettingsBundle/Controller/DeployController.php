<?php

namespace Settings\SettingsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shared\ContactBundle\Classes\contact;
use Permissions\AdvisersBundle\Classes\adviser;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Component\HttpFoundation\Request;
class DeployController extends Controller
{

    public function indexAction()
    {
		$session = $this->get('adminsession');
		$session->set("section","Settings");
		$session->set("currentpage","SettingsDeploymentUrls");
		return $this->render('SettingsSettingsBundle:Deploy:index.html.twig');
	}
   
	public function deploymentUrlsCsvAction()
	{
		$this->init();
		$session = $this->get('adminsession');
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
		$currentmember = $repository->findOneBy(array('id'=>$this->userid));

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
		$plans = $repository->findBy(array('userid'=>$this->userid));

		$response = "Name,Planid,Url";
		$response = $response."\n";
		foreach ($plans as $plan)
		{
			$deploymenturl = $session->appurl."?id=".$plan->planid."&partner_id=".$currentmember->partnerid;
			$response = $response.$plan->name.','.$plan->planid.','.$deploymenturl;
			$response = $response."\n";
		}
		$response = new Response($response);
		$response->headers->set('Content-Type', 'text/csv');
    	$response->headers->set('Content-Disposition', 'attachment; filename="deployment-list.csv"');
		return $response;
	}
    public function init(Request $request)
    {
		$session = $request->getSession();
		$userid = $session->get("userid");
		$session->set("currentpage","SettingsDeploymentUrls");
		$this->userid = $userid;
    }
}
