<?php

namespace Settings\SettingsBundle\Controller;

use classes\classBundle\Entity\AccountAppForkContent;
use classes\classBundle\Services\Style\ForkContentService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InterfaceController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansInterface");
        $session->set("section","Settings");
        $session->set("currentpage","SettingsInterface");
        //$session->set("currentpage","PlansInterface");
        $userid = $session->userid;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPlan');
        $interface = $repository->findOneBy(array('userid' => $userid));
        $colors = $this->get("AccountAppColorsService")->findOneBy(array('userid' => $userid));
        $hexList = $this->get("StyleService")->hexColorValues($colors);
    	return $this->render('Shared/Interface/index.html.twig',array(
    	    'id' => $interface->id,
            "colors" => $this->get("StyleService")->getColors($interface),
            "layouts" => $this->get("StyleService")->getLayouts($interface),
            "writeable" => $writeable,
            'savePath' => '_settings_interface_save',
            'hexList' => $hexList
        ));
    }
    public function saveAction(Request $request)
    {
        $this->get("StyleService")->saveStyle($request->request->all(),$this->get("DefaultPlanService"));
        return new Response("");
    }

    public function saveHexAction(Request $request)
    {
        $params = $request->request->all();
        $params['userid'] = $this->get("session")->get("userid");
        $this->get("StyleService")->saveHex($this->get("AccountAppColorsService"),$params,["userid" => $this->get("session")->get("userid")]);
    }
}