<?php

namespace Settings\SettingsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
class ProfilesController extends Controller
{

    public function indexAction()
    {
        $generalfunctions = $this->get('generalfunctions'); 
        $session =  $this->get('adminsession');
		$session->set("section","Settings");
        $currentpage = "SettingsAccountLevelReporting";
        $session->set("currentpage",$currentpage);
        $profilesClassVar = $this->get('profiles.service');
        $sections = $profilesClassVar->sections();
        $profilelist = $generalfunctions->search("profiles","profiles.userid",$session->userid,"profiles","PlansPlansBundle:Profiles:Search/profiles.html.twig","profiles.id","profiles","inline-block","classesclassBundle:Search:searchprofiles.html.twig","profiles.id DESC,profiles.id", "Newest-Oldest,Oldest-Newest","","profileList");
        return $this->render('SettingsSettingsBundle:Profiles:index.html.twig',array("profilelist" => $profilelist,"sections" => $sections,"session" => $session,"currentpage" => $currentpage));
    }
}