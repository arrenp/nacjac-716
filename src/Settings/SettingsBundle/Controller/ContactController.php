<?php

namespace Settings\SettingsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shared\ContactBundle\Classes\contact;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
class ContactController extends Controller
{
	public $contact;
	public $userid;
    public function indexAction()
    {
    	$this->init();
		$contact = $this->contact->indexAction();
		$userid = $this->userid;
		$permissions = $this->get('rolesPermissions');
		$writeable = $permissions->writeable("SettingsContact");
		//return new Response("test");
		return $this->render('SettingsSettingsBundle:Contact:index.html.twig', array('userid' => $userid, 'contact' => $contact,'writeable' => $writeable));
    }

    public function init()
    {
		$session = $this->get('adminsession');
		$session->set("section","Settings");
		$session->set("currentpage","SettingsContact");
		$userid = $session->userid;
		$em = $this->getDoctrine();
		$this->userid = $userid;
		$this->contact = new contact($em,"settings",$userid);
    }
    public function planListAction(Request $request)
    {
        $this->init();
        $queryBuilder  = $this->get("QueryBuilderDataTables")->queryBuilder("plans");              
        $queryBuilder->where("a.deleted = :deleted");
        $queryBuilder->andWhere("a.userid = :userid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPlan');
        $defaultPlan = $repository->findOneBy(array('userid' => $this->get("session")->get("userid"))); 
        $contact = $this->contact->setContact($defaultPlan);
        $sqls = [];
        foreach ($contact as $key => $value)
        {
            $sqls[] = "a.".$key." != :".$key;
        }
        $queryBuilder->andWhere(implode(" OR ",$sqls));
        foreach ($contact as $key => $value)
        {
            $queryBuilder->setParameter($key,$value['value']);
        }

        $queryBuilder->setParameter("deleted",0);
        $queryBuilder->setParameter("userid",$this->get("session")->get("userid"));
        $result = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder("plans",["a.id","a.planid"],$request->request->all(),$queryBuilder);
        $data = [];
        foreach ($result['rows'] as $plan)
        {
            $row = [
                $plan->id,
                $plan->planid,
                $this->render('SettingsSettingsBundle:Contact:planListButtons.html.twig',["plan" => $plan])->getContent()
            ];
            $data[] = $row;
        }      
        $response = $result['response'];
        $response['data'] = $data;       
        return new JsonResponse($response);        
    } 
    public function syncAction(Request $request)
    {
        return $this->render('SettingsSettingsBundle:Contact:sync.html.twig');
    }
    public function syncSavedAction(Request $request)
    {
        $this->init();
        $this->contact->syncContacts($this->get("session")->get("userid"),$this->get("database_connection"));
        return new Response("");
    }
    public function syncPlanAction(Request $request)
    {
        return $this->render('SettingsSettingsBundle:Contact:syncPlan.html.twig',["id" => $request->query->get("id")]);
    }
    public function syncPlanSavedAction(Request $request)
    {
        $this->init();
        $this->contact->syncContactPlan($this->get("session")->get("userid"), $request->request->get("id"));
        return new Response("");
    }
}
