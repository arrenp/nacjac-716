<?php
namespace Settings\SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Shared\OutreachBundle\Classes\outreach;
use classes\classBundle\Entity\outreachCategoriesDeny;
use classes\classBundle\Entity\outreachTemplatesDeny;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
class OutreachController extends Controller
{
	public function indexAction()
	{
		$this->init();
		$permissions = $this->get('rolesPermissions');
    	$writeable = $permissions->writeable("SettingsEmail");
		$generalfunctions = $this->get('generalfunctions');
		$adviserlist = $generalfunctions->search("accountsUsers","roleType,userid","adviser,".$this->userid,"advisers","SettingsSettingsBundle:Outreach:Search/adviserlist.html.twig","firstname,lastname","advisers","inline-block","classesclassBundle:Search:search.html.twig","firstname ASC,firstname DESC,lastname ASC,lastname DESC", "Firstname A-Z,Firstname Z-A,Lastname A-Z,Lastname Z-A");
		return $this->render('SettingsSettingsBundle:Outreach:index.html.twig',array("userid" => $this->userid,'writeable' => $writeable));
	}
    public function setPermissionsAction()
    {
    	$this->init();
    	$request = Request::createFromGlobals();
		$request->getPathInfo();
		$adviserid = $request->query->get("adviserid",0);
    	$outreach = new outreach($this->getDoctrine(),$this->userid,$adviserid);
    	$categories = $outreach->templates("settings");
    	$permissions = $this->get('rolesPermissions');
    	$writeable = $permissions->writeable("SettingsEmail");
    	return $this->render('SettingsSettingsBundle:Outreach:setpermissions.html.twig',array('categories' => $categories,'writeable' => $writeable,"adviserid" => $adviserid));
    }
	public function saveTemplatePermissionsAction()
	{
		$this->init();
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$adviserid = $request->request->get("adviserid",0);

		echo $adviserid;
		$categoriesIdString = $request->request->get("categories","");
		$templatesIdString = $request->request->get("templates","");
		$categoriesIdArray = array();
		$templatesIdArray = array();
		$em = $this->getDoctrine()->getManager();


		$repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachCategoriesDeny');
		$categoryListDeny = $repository->findBy(array("userid" => $this->userid,"adviserid" => $adviserid) );;

		foreach ($categoryListDeny as $categoryDeny)
		{
		$em->remove($categoryDeny);
		$em->flush();
		}

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachTemplatesDeny');
		$templateListDeny = $repository->findBy(array("userid" => $this->userid,"adviserid" => $adviserid) );;

		foreach ($templateListDeny as $templateDeny)
		{
		$em->remove($templateDeny);
		$em->flush();
		}
		if ($categoriesIdString != "")
		$categoriesIdArray = explode(",",$categoriesIdString);


		foreach ($categoriesIdArray as $categoryid)
		{
		$outreachCategoriesDeny = new outreachCategoriesDeny();
		$outreachCategoriesDeny->userid = $this->userid;
		$outreachCategoriesDeny->categoryid = $categoryid;
		$outreachCategoriesDeny->adviserid = $adviserid;
		$em->persist($outreachCategoriesDeny);
		$em->flush();
		}

		if ($templatesIdString != "")
		$templatesIdArray = explode(",",$templatesIdString);

		foreach ($templatesIdArray as $templateid)
		{
		$outreachTemplatesDeny = new outreachTemplatesDeny();
		$outreachTemplatesDeny->userid = $this->userid;
		$outreachTemplatesDeny->templateid = $templateid;
		$outreachTemplatesDeny->adviserid= $adviserid;
		$em->persist($outreachTemplatesDeny);
		$em->flush();
		}



		return new Response("saved");

	}
	public function init()
	{
		$session = $this->get('adminsession');
		$session->set("section","Settings");
		$session->set("currentpage","SettingsEmail");
		$this->userid = $session->userid;
		
	}

}
