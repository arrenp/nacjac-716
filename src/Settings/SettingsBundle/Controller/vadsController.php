<?php

namespace Settings\SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;

class vadsController extends Controller
{
    public function indexAction()
    {

  	$session = $this->get('adminsession');
	$session->set("section","Settings");
	$session->set("currentpage","SettingsVads");
	$userid = $session->userid;


	$repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPlan');
	$plan = $repository->findOneBy(array('userid' => $userid));

	$vadfrequency = $plan->vadFrequency;

	$days = floor($vadfrequency/1440);

	$minutes = $vadfrequency - ($days * 1440);

	$minutes = floor($minutes/60);

	$checkedon = "";
	$checkedperiod = "";
	if ($vadfrequency <= 0)
	$checkedon = "checked";
	else
	$checkedperiod = "checked";

	$vadsUrl = "https://smartplanenterprise.com/_vAds/vAds.php?sid=".$userid;

	$vadsIframeUrl = "https://smartplanenterprise.com/_vAds/vAdsIframe.php?sid=".$userid;

	$permissions = $this->get('rolesPermissions');

	$writeable = $permissions->writeable("SettingsVads");

    return $this->render('SettingsSettingsBundle:Vads:index.html.twig', array( "userid" => $userid, 'vadfrequency' => $vadfrequency,'days' => $days,'minutes' => $minutes, 'checkedon' => $checkedon, 'checkedperiod' => $checkedperiod,"vadsUrl" => $vadsUrl, "vadsIframeUrl" => $vadsIframeUrl,"writeable" => $writeable));
    }
}
