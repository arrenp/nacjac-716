<?php
namespace Settings\SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sessions\AdminBundle\Classes\adminsession;
ini_set("memory_limit","2000M");
set_time_limit(0);

class PerformanceController extends Controller{
    
    private $performance;
    private $templatePath;
    private $db = "unset";
    
    public function indexAction(){
        
        $this->init();
        $end = date("Y-m-d", strtotime("+1 days"));
        $start   = date('Y-m-d', strtotime("-30 days"));
        return $this->render('SettingsSettingsBundle:Performance:index.html.twig', array('startdate' => $start,'enddate'   => $end));
    }
    
    public function vWiseReport($audience, $account, $plan, $fix, $var2){
        
        $req = Request::createFromGlobals();
        $req->getPathInfo();
        $params['level'] = $req->query->get('level');
        $params['acct'] = $account;
        $params['plan'] =  $plan;
        $params['var2'] = $var2;
        $params['audience'] = "provider";
        $params['prov'] =$req->query->get('prov');
        $params['start'] = $req->query->get('start');
        $params['end'] = $req->query->get('end');
        return $this->get("PerformanceReportService")->vWiseReport($params);
    }
    
    public function getAcctsAction(){
        
        $req = Request::createFromGlobals();
        $req->getPathInfo();
        
        $level  = $req->request->get('level');        
        
        $accountSql = 'SELECT id, partnerid, company FROM accounts WHERE reportable = 1 order by company';
        switch($level){
            case 'vWise':
            case 'provider':
                $rows = $this->get('doctrine.dbal.default_connection')->fetchAll($accountSql);
                foreach ($rows as $data){
                    $accounts[] = '<option value="'.$data['id'].'" data="'.$data['id'].'" data-smtplan="'.$data['partnerid'].'">'.$data['id'].'-'.$data['company'].'</option>';
                }
        
                break;
            case 'advisor':
                $rows = $this->get('doctrine.dbal.default_connection')->fetchAll('SELECT accountsUsers.id,accountsUsers.userid,accountsUsers.firstname, accountsUsers.lastname,accountsUsers.email,accounts.partnerid FROM accountsUsers LEFT join accounts ON accounts.id = accountsUsers.userid WHERE accountsUsers.roleType = "adviser"  AND accounts.id is not null AND accounts.visible = 1');
                foreach($rows as $data){
                    $accounts[] = '<option value="'.$data['id'].'" data="'.$data['userid'].'" data-smtplan="'.$data['partnerid'].'">'.$data['userid'].'-'.$data['id'].'-'.$data['firstname'].' '.$data['lastname'].'</option>';
                }
        
                                  
                break;
            default:
                break;
        }
        
        return new JsonResponse($accounts);
        
    }
    
    public function getPlansAction(){
        
        $req = Request::createFromGlobals();
        $req->getPathInfo();
        
        $id  = $req->request->get('acctId');
        $level  = $req->request->get('level');
        

        switch($level){
            case 'vWise':
                
                $rows = $this->get('doctrine.dbal.default_connection')->fetchAll("SELECT id as smartplanid,name,planid, partnerid FROM plans WHERE testing = '0' AND userid = '$id' AND deleted = '0' order by name");

                foreach($rows as $data){
                    $accounts[] = '<option value="'.$data['smartplanid'].'" data="'.$data['planid'].'">'.$data['planid'].'-'.$data['name'].'</option>';
                }

                $acctList = implode(',', $accounts);
                break;
            case 'provider':
                $rows = $this->get('doctrine.dbal.default_connection')->fetchAll("SELECT id as smartplanid,name,planid FROM plans WHERE testing = '0' AND userid = '$id' AND deleted = '0' order by name");

                foreach($rows as $data){
                    $accounts[] = '<option value="'.$data['smartplanid'].'" data="'.$data['planid'].'">'.$data['planid'].'-'.$data['name'].'</option>';
                }

                $acctList = implode(',', $accounts);                
                break;
            case 'advisor':
                
                $advisersPlansArray =  "SELECT accountsUsersPlans.planid as planid FROM accountsUsersPlans WHERE accountsUsersPlans.accountsUsersId = '$id'";  
                $advisersPlans = "";
                $query  = $this->get('doctrine.dbal.default_connection')->fetchAll($advisersPlansArray);
                $counter = 0;
                
                    foreach ($query as $advisersPlan)
                    {
                        if ($counter > 0)
                        $advisersPlans = $advisersPlans.",";
                        $advisersPlans   = $advisersPlans.$advisersPlan['planid'];
                        $counter++;
                    }

                $plans  = $advisersPlans;
                $rows = $this->get('doctrine.dbal.default_connection')->fetchAll("SELECT id as smartplanid,name,planid, partnerid FROM plans WHERE testing = '0' AND id IN($plans) AND deleted = '0'");

                foreach($rows as $data){
                    $accounts[] = '<option value="'.$data['smartplanid'].'" data="'.$data['planid'].'">'.$data['planid'].'-'.$data['name'].'</option>';
                }                
                
                $acctList = implode(',', $accounts);                 
                break;
            default:
                break;
        }
        return new Response($acctList); 
    }    
    
    public function pageCallAction(){
        $data = $this->getReportData();
        $data['isPdf'] = false;
        return $this->render('SettingsSettingsBundle:Performance:viewreport.html.twig', $data);
    }

    public function generatePdfAction() {
        $data = $this->getReportData();
        $data['isPdf'] = true;
        $html = $this->renderView('SettingsSettingsBundle:Performance:viewreport.html.twig', $data);

        $filename = sprintf($data["provider_name"] . " " . $data["plan_name"] . ' %s.pdf', date('Y-m-d'));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            ]
        );


    }

    public function getReportAction() {
        $data = $this->getReportData();
        return new Response($this->get("PerformanceReportService")->reportHtml($data));
    }
    
    public function init(){
         
           $session = $this->get('adminsession');
           $session->set("section","Stats");
           $session->set("currentpage","Performance Report");
           
    }

    /**
     * @return array|string
     */
    public function getReportData()
    {
        $req = Request::createFromGlobals();
        $req->getPathInfo();

        $audience = $req->query->get('audience');
        $account = $req->query->get('acct');
        $plan = $req->query->get('plan');
        $fix = $req->query->get('pop');
        $var2 = $req->query->get('var2');

        switch ($audience) {
            case 'provider':
                $data = $this->vWiseReport($audience, $account, $plan, $fix, $var2);
                break;
            case 'advisor':
                $data = $this->vWiseReport($audience, $account, $plan, $fix, $var2);
                break;
            case 'sponsor':
                $data = $this->sponsorReport($audience, $account, $plan, $fix, $var2);
                break;
        }
        return $data;
    }

}
