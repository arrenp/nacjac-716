<?php

namespace Settings\SettingsBundle\Controller;

use Sessions\AdminBundle\Classes\adminsession;

use classes\classBundle\Entity\DefaultInvestmentsScore;
use classes\classBundle\Entity\DefaultInvestmentsYearsToRetirement;
use classes\classBundle\Entity\DefaultInvestmentsConfiguration;
use classes\classBundle\Entity\DefaultInvestmentsConfigurationColors;
use classes\classBundle\Entity\DefaultInvestmentsLinkYearsToRetirementAndScore;
use classes\classBundle\Entity\DefaultInvestmentsConfigurationFundsGroupValuesColors;
use classes\classBundle\Entity\DefaultInvestmentsConfigurationDefaultFundsGroupValuesColors;
use classes\classBundle\Entity\defaultPortfolios;

use Shared\InvestorProfileBundle\Controller\InvestorProfileController as Controller;
class InvestorProfileController extends Controller
{

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $session = $this->get('adminsession');
        $this->tablename = "Default";
        $this->userid = $session->userid;
        $this->findBy = array('userid' => $session->userid);
        $session->set("section","Settings");
        $session->set("currentpage","SettingsInvestorProfile");
        $this->twigParameters = array(
            'path' => 'settings',
            'controller' => 'SettingsSettingsBundle:InvestorProfile:');

    }

    public function getWhereClause()
    {
        return 'p.userid = :findby';
    }

    public function getParameterClause()
    {
        return $this->userid;
    }

    public function getInvestmentsScoreEntity()
    {
        return new DefaultInvestmentsScore();
    }

    public function getInvestmentsConfigurationEntity()
    {
        return new DefaultInvestmentsConfiguration();
    }

    public function getInvestmentsYearsToRetirementEntity()
    {
        return new DefaultInvestmentsYearsToRetirement();
    }

    public function getInvestmentsConfigurationColorsEntity()
    {
        return new DefaultInvestmentsConfigurationColors();
    }

    public function getInvestmentsLinkYearsToRetirementAndScoreEntity()
    {
        return new DefaultInvestmentsLinkYearsToRetirementAndScore();
    }

    public function getInvestmentsConfigurationFundsGroupValuesColors()
    {
        return new DefaultInvestmentsConfigurationFundsGroupValuesColors();
    }

    public function getInvestmentsConfigurationDefaultFundsGroupValuesColors()
    {
        return new DefaultInvestmentsConfigurationDefaultFundsGroupValuesColors();
    }

    public function getinvestmentsScoreAssignColorActionRepository()
    {
        return 'classesclassBundle:defaultPlan';
    }

    public function getinvestmentsScoreAssignColorActionFindBy() {
        return array('userid' => $this->userid);
    }

    public function getPortfolioEntity()
    {
        return new defaultPortfolios();
    }
}
