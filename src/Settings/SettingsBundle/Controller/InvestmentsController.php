<?php

namespace Settings\SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use classes\classBundle\Entity\defaultPortfolioFunds;
use classes\classBundle\Entity\defaultFunds;
use classes\classBundle\Entity\defaultPortfolios;
use Shared\InvestmentsBundle\Classes\investments;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;

class InvestmentsController extends Controller
{
    private $investments;
    private $templatePath;

    public function indexAction()
    {
        $this->init();
        $session = $this->get('adminsession');
        $session->set("section", "Settings");
        $session->set("currentpage", "SettingsInvestments");
        $this->investments->indexAction();
        $adviser = $this->get('rolesPermissions');
        $writeable = $adviser->writeable("SettingsInvestments");
        return $this->render($this->templatePath . 'index.html.twig', array('investments' => $this->investments, "writeable" => $writeable));
    }

    public function fundsAction()
    {
        $this->init();
        $session = $this->get('adminsession');
        $session->set("section", "Settings");
        $session->set("currentpage", "SettingsInvestmentsFunds");
        $this->investments->indexAction();
        $adviser = $this->get('rolesPermissions');
        $writeable = $adviser->writeable("SettingsInvestmentsFunds");
        return $this->render($this->templatePath . 'funds.html.twig', array('investments' => $this->investments, "writeable" => $writeable));

    }

    public function portfoliosAction()
    {
        $this->init();
        $session = $this->get('adminsession');
        $session->set("section", "Settings");
        $session->set("currentpage", "SettingsInvestmentsPortfolios");
        $this->investments->indexAction();
        $adviser = $this->get('rolesPermissions');
        $writeable = $adviser->writeable("SettingsInvestmentsFunds");
        return $this->render($this->templatePath . 'portfolios.html.twig', array('investments' => $this->investments, "writeable" => $writeable, 'controller' => 'settings'));
    }

    public function fundreorderAction()
    {
        $this->init();
        $session = $this->get('adminsession');
        $session->set("section", "Settings");
        $session->set("currentpage", "SettingsInvestmentsFunds");
        $this->investments->fundreorderAction();
        return $this->render($this->templatePath . 'reorderfund.html.twig', array('investments' => $this->investments));
    }

    public function portfolioreorderAction()
    {
        $this->init();
        $this->investments->portfolioreorderAction();
        return $this->render($this->templatePath . 'reorderportfolio.html.twig', array('investments' => $this->investments));
    }

    function portfoliosetfundsAction()
    {
        $this->init();
        $session = $this->get('adminsession');
        $userid = $session->userid;
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $portfolioid = $request->query->get('id');
        $allfunds = "";
        $portfoliofunds = "";
        $this->investments->portfoliosetfundsAction($portfolioid);
        $allfunds = $this->investments->allfunds;
        $portfoliofunds = $this->investments->portfoliofunds;
        return $this->render($this->templatePath . 'setportfoliofunds.html.twig', array('allfunds' => $allfunds, 'portfoliofunds' => $portfoliofunds, 'userid' => $userid, 'portfolioid' => $portfolioid));
    }

    public function fundreordersaveAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $itemstring = $request->request->get('ids', '');
        $searchtype = $request->request->get('searchtype', '');
        $response = $this->investments->reorderInvestments($itemstring, $searchtype, "fund");
        return new Response($response);
    }

    public function portfolioreordersaveAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $itemstring = $request->request->get('ids', '');
        $searchtype = $request->request->get('searchtype', '');
        $response = $this->investments->reorderInvestments($itemstring, $searchtype, "portfolio");
        return new Response($response);
    }

    public function fundDeleteAction($id)
    {
        $this->init();
        $fund = $this->investments->getFund($id);
        return $this->render($this->templatePath . "Search/deletefund.html.twig", array("fund" => $fund));
    }

    public function fundDeletedAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->request->get("id", "");
        $response = $this->investments->deleteFund($id);
        return new Response($response);
    }

    public function fundaddreorderAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $name = $request->request->get("name", "");
        $link = $request->request->get("link", "");
        $ticker = $request->request->get("ticker", "");
        $fundid = $request->request->get("fundid", "");
        $response = $this->investments->saveAndReorderFunds($name, $link, $ticker, $fundid, $request->request->get("groupValueId", 0));
        return new Response($response);
    }

    public function portfolioaddreorderAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $name = $request->request->get("name", "");
        $response = $this->investments->saveAndReorderPortfolios($name, $request->request->get("customid"));
        return new Response($response);
    }

    public function portfolioDeleteAction($id)
    {
        $this->init();
        $portfolio = $this->investments->getPortfolio($id);
        return $this->render($this->templatePath . "Search/deleteportfolio.html.twig", array("portfolio" => $portfolio));
    }

    public function portfolioDeletedAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $portfolioid = $request->request->get("id");
        $response = $this->investments->deletePortfolio($portfolioid);
        return new Response($response);
    }


    public function portfolioShowFundsAction(Request $request)
    {
        $this->init();
        $session = $this->get('adminsession');
        $userid = $session->userid;
        $portfolioid = $request->query->get('portfolioid');
        $this->investments->portfoliosetfundsAction($portfolioid);
        $allfunds = $this->investments->allfunds;
        $portfoliofunds = $this->investments->portfoliofunds;
        return $this->render($this->templatePath . 'setportfoliofunds.html.twig', array('allfunds' => $allfunds, 'portfoliofunds' => $portfoliofunds, 'userid' => $userid, 'portfolioid' => $portfolioid, 'writeable' => false));

    }

    public function portfoliosetfundssaveAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $portfolioid = $request->request->get('portfolioid', '');
        $itemstring = $request->request->get('ids', '');
        $valuestring = $request->request->get('values', '');
        $response = $this->investments->savePortfolioFunds($portfolioid, $itemstring, $valuestring);
        return new Response($response);
    }

    public function editFundAction($id)
    {
        $this->init();
        $fund = $this->investments->getFund($id);
        return $this->render($this->templatePath . "Search/editfund.html.twig", array("fund" => $fund, "investments" => $this->investments));
    }

    public function saveGroupIdAction(Request $request)
    {
        $this->init();
        $this->investments->saveGroupId($request->request->get("groupid"));
        return new Response("");
    }

    public function init()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Settings");
        $session->set("currentpage", "SettingsInvestments");
        $userid = $session->userid;
        $em = $this->getDoctrine();
        $search = $this->get('generalfunctions');
        $adviser = $this->get('rolesPermissions');
        $writeable = $adviser->writeable("SettingsInvestments");


        $this->investments = new investments($em, "settings", $search, $userid, $writeable, 0, $this->container);
        $this->templatePath = 'SettingsSettingsBundle:Investments:';
    }
}
