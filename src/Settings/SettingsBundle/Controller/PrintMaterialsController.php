<?php

namespace Settings\SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
class PrintMaterialsController extends Controller
{
    public function indexAction()
    {
	$generalfunctions = $this->get('generalfunctions');
  	$session = $this->get('adminsession');
	$session->set("section","Settings");
	$session->set("currentpage","SettingsPrintMaterials");
	$userid = $session->userid;


    if ($session->roleType != "adviser")
    {
      $findby= "a.userid,^a.adviserid";
      $findbyvalues = $userid.",0";
    }

    if ($session->roleType == "adviser")
    {
      $adviserid = $session->clientid;
      $findby= "a.userid,|a.adviserid";
      $findbyvalues = $userid.",".$adviserid;
    }

    return $this->render('SettingsSettingsBundle:PrintMaterials:index.html.twig', array("options" => $options,"findby" => $findby,"findbyvalues" => $findbyvalues));
    }
}
