<?php

namespace Settings\SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sessions\AdminBundle\Classes\adminsession;
class DefaultController extends Controller
{
    public function indexAction()
    {
    	$session = $this->get('adminsession');
    	$session->set("section","Settings");
    	$session->set("currentpage","SettingsHome");
        return $this->render('SettingsSettingsBundle:Default:index.html.twig');
    }
}
