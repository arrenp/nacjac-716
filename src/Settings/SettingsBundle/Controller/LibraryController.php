<?php

namespace Settings\SettingsBundle\Controller;

use classes\classBundle\Entity\plans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\DocumentsBundle\Classes\documents;
use \Spe\AppBundle\Services\PlanService;
use Shared\LibraryBundle\Controller\LibraryController as Controller;

class LibraryController extends Controller
{

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $session = $this->get('adminsession');
        $this->tablename = "Default";
        $this->userid = $session->userid;
        $this->findBy = array('userid' => $this->userid);
        $session->set("section","Settings");
        $session->set("currentpage","SettingsLibrary");
        $permissions = $this->get('rolesPermissions');
        $this->writeable = $permissions->writeable("SettingsLibrary");
        $this->path = 'SettingsSettingsBundle:';

    }
    

}
