<?php

namespace Settings\SettingsBundle\Controller;

use classes\classBundle\Entity\PlansAutoEnroll;
use classes\classBundle\Entity\PlansAutoEscalate;
use classes\classBundle\Entity\PlansAutoIncrease;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AutosController extends Controller {
    
    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section","Settings");
        $session->set("currentpage","SettingsAutos");
        
        /* @var $autoService \classes\classBundle\Services\Autos\AutosService */
        $autoService = $this->get("AutosService");
        $autoService->set(["userid" => $session->userid]);
        
        $autoEnroll = $autoService->getData('AutoEnroll');
        $autoEnroll = is_null($autoEnroll) ? new PlansAutoEnroll() : $autoEnroll;

        $autoEscalate = $autoService->getData('AutoEscalate');
        $autoEscalate = is_null($autoEscalate) ? new PlansAutoEscalate() : $autoEscalate;
        
        $autoIncrease = $autoService->getData('AutoIncrease');
        $autoIncrease = is_null($autoIncrease) ? new PlansAutoIncrease() : $autoIncrease;
        
        return $this->render('Shared/Autos/index.html.twig', ['autoEnroll' => $autoEnroll, 'autoEscalate' => $autoEscalate, 'autoIncrease' => $autoIncrease]);
    }
    
    public function saveAction(Request $request) {
        $session = $this->get('adminsession');
        /* @var $autoService \classes\classBundle\Services\Autos\AutosService */
        $autoService = $this->get("AutosService");
        $autoService->set(['userid' => $session->userid]);
        
        $autoService->addData('AutoEnroll', $request->get('autoEnroll'));
        $autoService->addData('AutoEscalate', $request->get('autoEscalate'));
        $autoService->addData('AutoIncrease', $request->get('autoIncrease'));
        
        return new Response("success");
    }
    
}
