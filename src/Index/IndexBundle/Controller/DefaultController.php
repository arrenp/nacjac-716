<?php

namespace Index\IndexBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use classes\classBundle\Entity\defaultPlan;
use classes\classBundle\Entity\defaultModules;
use classes\classBundle\Entity\defaultLibraryHotTopics;
use classes\classBundle\Entity\accountsUsersLogins;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;


class DefaultController extends BaseController
{
    
    public function generateAuthenticateSessionIdAction(Request $request)
    {
        $session = $request->getSession();
        return new Response($session->get("_csrf/authenticate"));
    }

    public function setMigrationPasswordAction(Request $request)
    {
        $em = $this->container->get("doctrine")->getManager();
        $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:accountsUsers');
        $params = array('password' => '', 'email' => $request->request->get("email"));

        //var_dump($params);
        $user = $repository->findOneBy($params);
        if ($user)
        {
            $connection = $this->container->get('doctrine.dbal.default_connection');
            $querystring = "SELECT  AES_DECRYPT(migrationPassword, '5E6D217336') as migrationPasswordDecrypted  FROM accountsUsers WHERE id = " . $user->id . ' LIMIT 1';
            $accountUserRaw = $connection->fetchAll($querystring);
            $user->eraseCredentials();
            $user->setPlainPassword($accountUserRaw[0]['migrationPasswordDecrypted']);
            $this->container->get('fos_user.user_manager')->updateUser($user, false);
            $em->flush();
        }
        return new Response("");
    }

    public function renderLogin(array $data)
    {
//        var_dump($session->get("_csrf/authenticate"));exit;

        /*
          $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:accountsUsers');
          $users = $repository->findBy(array('password' => ''));
          $connection = $this->container->get('doctrine.dbal.default_connection');

          foreach ($users as $user)
          {
          $querystring = "SELECT  AES_DECRYPT(migrationPassword, '5E6D217336') as migrationPasswordDecrypted  FROM accountsUsers WHERE id = ".$user->id.' LIMIT 1';
          $accountUserRaw = $connection->fetchAll($querystring);
          $password = $accountUserRaw[0]['migrationPasswordDecrypted'];
          echo $password.'<br/><br/>';
          //$user->setPassword($password);
          }
         */
        $session = $this->get("session");
        $session->remove("_security.main.target_path");
        $session->save();
        $template = sprintf('IndexIndexBundle:Default:login.html.twig');
        $hostName = 'www.' . $_SERVER['HTTP_HOST'];
        
        $response = new Response();
        $response->headers->set("Content-Security-Policy", "frame-ancestors 'self'");
        return $this->container->get('templating')->renderResponse($template, array('hostName' => $hostName, "sessionid" => $session->get("_csrf/authenticate")), $response);
    }

    public function returnClientId()
    {
        $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();
//        $id = 0;
//        $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:accountsUsers');
//        $currentUser = $repository->findOneBy(array('email' => $email));
        if ($currentUser != null) $id = $currentUser->id;
        return $id;
    }

    public function setSessionVariablesAction(Request $request)
    {
        $session = $request->getSession();
        $em = $this->container->get("doctrine")->getManager();
        if ($session->get("newclientid", "") == "") $clientid = $this->returnClientId();
        else $clientid = $session->get("newclientid", "");
        $email = (string) $this->container->get('security.token_storage')->getToken()->getUser();
        if ($clientid > 0)
        {

            $session->set("clientid", $clientid);
            $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:accountsUsers');
            $currentUser = $repository->findOneBy(array('id' => $session->get("clientid", "0")));
            
            if ($currentUser->loginAttempts >= 5)
            {
                return $this->passwordAttemptsExceeded($currentUser);
            }
            
            $currentDay = new \dateTime("now");
            if ((int)$currentUser->passwordExpirationDate->diff($currentDay)->format("%R%a") > 0 )
            return new Response("3");

            if ($session->get("masterid", "") == "")
            {
                $session->set("masterid", $clientid);
                $session->set("masterRoleType",$currentUser->roleType);
                $session->set("openchat", 1);
                $accountsUsersLogins = new accountsUsersLogins();
                $accountsUsersLogins->accountsUsersId = $currentUser->id;
                $accountsUsersLogins->sessionid = $session->get("_csrf/authenticate", "");

                $accountsUsersLogins->loginTime = date_timestamp_get($currentUser->getLastLogin());
                $currentUser->loginAttempts = 0;
                $em->persist($accountsUsersLogins);
                $em->flush();
            }

            $session->set("userid", $currentUser->userid);
            $session->set("roleType", $currentUser->roleType);
            $session->set("clientid", $currentUser->id);
            $session->set("currentpage", "");
            $session->set("section", "");
            $session->set("debugbar", 0);

            $session->set('appurl', $this->container->getParameter('appurl'));

            $this->createTableData("defaultPlan", $currentUser->userid);
            $this->createTableData("defaultModules", $currentUser->userid);
            $this->createTableData("defaultLibraryHotTopics", $currentUser->userid);

            if ($session->get("masterid", "") == $session->get("newclientid", "")) $session->remove("newclientid");
            if ($session->get("planid") != null) $session->remove("planid");
            $session->save();

            return new Response("1");
        }
        else 
        {
            $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:accountsUsers');
            $currentUser = $repository->findOneBy(array('email' => $request->request->get("_username")));    
            if ($currentUser != null)
            {
                $currentUser->loginAttempts++;
                if ($currentUser->loginAttempts <= 5)
                $em->flush();
                if ($currentUser->loginAttempts >= 5)
                {
                    return $this->passwordAttemptsExceeded($currentUser);            
                }
            }
            return new Response("0");
        }
    }

    private function createTableData($table, $userid)
    {
        $em = $this->container->get("doctrine")->getManager();

        $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:' . $table);
        $currentdata = $repository->findBy(array("userid" => $userid));

        if (count($currentdata) > 0)//only copies data if no rows are returned
            return false;

        if ($table == "defaultPlan") $addeddata = new defaultPlan();
        if ($table == "defaultModules") $addeddata = new defaultModules();
        if ($table == "defaultLibraryHotTopics") $addeddata = new defaultLibraryHotTopics();

        $addeddata->userid = $userid;
        $em->persist($addeddata);
        $em->flush();

        return true;
    }
    public function checkPasswordAction(Request $request)
    {
        $form = $request->request->get("fos_user_resetting_form");
        $password = $form['plainPassword']['first'];
        $token = $request->request->get("token");
        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);
        $user->setPlainPassword($password);
        $response = new JsonResponse(array("errors" => $user->errors));
        return $response;
    }
    public function sendEmailCheckAction(Request $request)
    {
        $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array('email' => $request->request->get("username")));
        if ($user == null || $user->loginAttempts < 5)
        {
            return new Response("");
        }
        else
        {
            return $this->passwordAttemptsExceeded($user);
        }
    }
    public function passwordAttemptsExceeded($user)
    {
        return $this->container->get('templating')->renderResponse("IndexIndexBundle:Default:passwordattempts.html.twig",array("accountManagers" => $user->getAccountsGroups("accountManagers")));
    }
}

?>
