<?php

namespace Index\IndexBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class RedirectController extends Controller
{
  	public function redirectAction()
  	{
  		return $this->redirect($this->generateUrl('_index'),301);
  	}
}
?>