<?php

namespace Shared\General;

use classes\classBundle\Entity\plansModules;
use classes\classBundle\Entity\plansLibraryHotTopics;
use classes\classBundle\Entity\plansDocuments;
use classes\classBundle\Entity\plansFunds;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansPortfolioFunds;
use classes\classBundle\Entity\plansRiskBasedFunds;
use classes\classBundle\Entity\accountsUsersPlans;
use classes\classBundle\Entity\PlansMessages;
use classes\classBundle\Entity\plansLibraryUsers;
use classes\classBundle\Entity\PlansInvestmentsLinkYearsToRetirementAndScore;
use classes\classBundle\Entity\PlansInvestmentsConfigurationColors;
use classes\classBundle\Entity\PlansInvestmentsConfigurationFundsGroupValuesColors;
use classes\classBundle\Entity\PlansInvestmentsScore;
use classes\classBundle\Entity\PlansInvestmentsYearsToRetirement;
use classes\classBundle\Entity\PlansInvestmentsConfigurationDefaultFundsGroupValuesColors;
use classes\classBundle\Entity\PlansInvestmentsConfiguration;

class AddPlanHelperFunctions
{
    private $em;

    public function __construct($em) {
        $this->em = $em;
    }

    public function addAccountsUsersPlan($clientid,$planid)
    {
        if (empty($clientid)) {
            return;
        }
        $repository = $this->em->getRepository('classesclassBundle:accountsUsers');
        $user= $repository->findOneBy(array('id' => $clientid));
        $repository = $this->em->getRepository('classesclassBundle:plans');
        $currentplan= $repository->findOneBy(array('id' => $planid));
        if (!$user->allPlans)
        {
            $plan = new accountsUsersPlans();
            $plan->userid = $user->userid;
            $plan->plan= $currentplan;
            $plan->accountsUsersId = $user->id;
            $this->em->persist($plan);
            $this->em->flush();
        }
    }

    public function addPlanHelper(&$plantobeadded, $defaultPlan,$type)
    {
        $this->addPlanSegment($plantobeadded, $defaultPlan);
        $plantobeadded->createdDate = new \DateTime("NOW");
        $plantobeadded->modifiedDate = new \DateTime("NOW");
        if ($type == "smartplan" || $type == "smartenroll") {
            $plantobeadded->type = $type;
        } else if (is_array($type)) { // CreateRkpData command is the only case where $type would be an array
            $plantobeadded->type = "smartplan";
            $plantobeadded->smartPlanAllowed = 0;
            $plantobeadded->irioAllowed = 0;
            $plantobeadded->powerviewAllowed = 0;
            $plantobeadded->smartEnrollAllowed = 0;

            $repository = $this->em->getRepository('classesclassBundle:plansRkp');
            $rkp = $repository->findOneBy(array('recordkeeperPlanid' => $plantobeadded->planid));
            if (in_array('irio', $type)) {
                $plantobeadded->irioAllowed = 1;
                if ($rkp) {
                    $rkp->irioEnabled = 1;
                }
            }
            if (in_array('powerview', $type)) {
                $plantobeadded->powerviewAllowed = 1;
                if ($rkp) {
                    $rkp->powerViewEnabled = 1;
                }
            }

            if ($rkp) {
                $this->em->persist($rkp);
            }
        } else {
            $smartplanAllowed = $plantobeadded->smartPlanAllowed;
            $plantobeadded->type = "smartplan";
            if ($type == "irio") {
                $plantobeadded->irioAllowed = 1;
            }
            if ($type == "powerview") {
                $plantobeadded->powerviewAllowed = 1;
            }
            $plantobeadded->smartPlanAllowed = $smartplanAllowed;
        }
    }

    public function copyTableData($table, $othertable, $findby, $findbyvalue, $idvalue, $userid = null)
    {
        $repository = $this->em->getRepository('classesclassBundle:' . $table);
        $currentdata = $repository->findBy(array("planid" => $idvalue));

        if (count($currentdata) > 0) {//only copies data if no rows are returned
            return false;
        }

        $repository = $this->em->getRepository('classesclassBundle:' . $othertable);
        $otherdatas = $repository->findBy(array($findby => $findbyvalue));
        
        $classPath = "classes\classBundle\Entity\\$table";
        foreach ($otherdatas as $otherdata)
        {
            $addeddata = new $classPath();

            $this->addPlanSegment($addeddata, $otherdata);
            $addeddata->planid = $idvalue;
            if ($userid != null) {
                $addeddata->userid = $userid;
            }

            $this->em->persist($addeddata);
            $this->em->flush();
        }
        return true;
    }

    public function addPlanSegment(&$object, $copiedFrom, array $exclude = [])
    {
        $class_vars = get_class_vars(get_class($object));
        foreach ($class_vars as $key => $value)
        {
            if (isset($copiedFrom->$key) && !in_array($key, $exclude)) {
                $object->$key = $copiedFrom->$key;
            }
        }
    }

    public function fillInMissingPlanTables($userid, $planid)
    {
        $this->copyTableData("plansModules", "defaultModules", "userid", $userid, $planid);
        $this->copyTableData("plansLibraryHotTopics", "defaultLibraryHotTopics", "userid", $userid, $planid);
        $this->copyTableData("PlansMessages", "DefaultMessages", "userid", $userid, $planid);
        $this->copyTableData("plansLibraryUsers", "DefaultLibraryUsers", "userid", $userid, $planid);
        $this->copyTableData("PlansAutoEnroll", "DefaultAutoEnroll", "userid", $userid, $planid);
        $this->copyTableData("PlansAutoEnroll", "DefaultAutoEnroll", "userid", $userid, $planid);
        $this->copyTableData("PlansAutoEscalate", "DefaultAutoEscalate", "userid", $userid, $planid);
        $this->copyTableData("PlansAutoIncrease", "DefaultAutoIncrease", "userid", $userid, $planid);
        $this->copyTableData("PlanAppColors", "AccountAppColors", "userid", $userid, $planid);
    }

    /* copyTableDataException
     * params: $userid, $planid
     *
     * Because PlansInvestmentsConfigurationFundsGroupValuesColors and PlansInvestmentsLinkYearsToRetirementAndScore rely on ids
     * that are specific to one planid, we can't do a straight copy of Default->Plans tables. copyTableDataException creates PlansInvestmentsScore
     * and PlansInvestmentsYearsToRetirement with data from the defaults tables while temporarily storing the id of the created rows.
     * This function will then use those ids to link PlansInvestmentsConfigurationFundsGroupValuesColors (scoreMinId) and
     * PlansInvestmentsLinkYearsToRetirementAndScore (InvestmentsYearsToRetirementId, InvestmentsScoreId)
     * with the appropriate id corresponding to planid.
     *
     * Rows created in tables:  PlansInvestmentsScore
     *                          PlansInvestmentsConfigurationFundsGroupValuesColors
     *                          PlansInvestmentsYearsToRetirement
     *                          PlansInvestmentsLinkYearsToRetirementAndScore
     *
     * return: none
     */
    public function copyTableDataException($userid, $planid, $tablename = "Default", $findby) {
        $minToId = array();

        $defaultInvestmentsScoreRepo = $this->em->getRepository('classesclassBundle:' . $tablename . 'InvestmentsScore');
        $defaultScore = $defaultInvestmentsScoreRepo->findBy($findby);
        foreach ($defaultScore as $ds) {
            $insert = new PlansInvestmentsScore();
            $insert->planid = $planid;
            $insert->userid = $userid;
            $insert->min = $ds->min;
            $insert->label = $ds->label;
            $this->em->persist($insert);
            $this->em->flush();

            $minToId[$ds->min] = $insert->id;
        }

        $repo = $this->em->getRepository('classesclassBundle:' . $tablename . 'InvestmentsConfigurationFundsGroupValuesColors');
        $defaultConfig = $repo->findBy($findby);
        foreach ($defaultConfig as $dc) {
            $insert = new PlansInvestmentsConfigurationFundsGroupValuesColors();
            $insert->planid = $planid;
            $insert->userid = $userid;
            $insert->min = $dc->min;
            $insert->color = $dc->color;
            $insert->hoverColor = $dc->hoverColor;
            $insert->fundGroupValueId = $dc->fundGroupValueId;
            $insert->isCustom = $dc->isCustom;

            $defaultMin = $defaultInvestmentsScoreRepo->findOneBy(array('id' => $dc->scoreMinId))->min;

            $insert->scoreMinId = $minToId[$defaultMin];
            $this->em->persist($insert);

        }
        $this->em->flush();

        $yearsToRetirementRepo = $this->em->getRepository('classesclassBundle:' . $tablename . 'InvestmentsYearsToRetirement');
        $defaultYears = $yearsToRetirementRepo->findBy($findby);
        $yearsToId = array();
        foreach ($defaultYears as $dy) {
            $insert = new PlansInvestmentsYearsToRetirement();
            $insert->planid = $planid;
            $insert->userid = $userid;
            $insert->min = $dy->min;
            $this->em->persist($insert);
            $this->em->flush();

            $yearsToId[$dy->min] = $insert->id;
        }

        $repo = $this->em->getRepository('classesclassBundle:' . $tablename . 'InvestmentsLinkYearsToRetirementAndScore');
        $defaultLink = $repo->findBy($findby);
        foreach ($defaultLink as $dl) {
            $insert = new PlansInvestmentsLinkYearsToRetirementAndScore();
            $insert->planid = $planid;
            $insert->userid = $userid;
            $insert->type = $dl->type;

            $defaultMin = $defaultInvestmentsScoreRepo->findOneBy(array('id' => $dl->InvestmentsScoreId))->min;
            $insert->InvestmentsScoreId = $minToId[$defaultMin];

            $defaultYear = $yearsToRetirementRepo->findOneBy(array('id' => $dl->InvestmentsYearsToRetirementId))->min;
            $insert->InvestmentsYearsToRetirementId = $yearsToId[$defaultYear];

            $this->em->persist($insert);
        }
        $this->em->flush();

    }

}