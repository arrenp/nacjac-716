<?php

namespace Shared\General;

use classes\classBundle\Entity\AddedPlansFromCommand;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Entity\plans;

class GeneralMethods
{

    public $controller;
    public $recordkeeperPortfoliosAvailable = array("Relius", "SRT", "ExpertPlan","Educate","SRT2");
    public $recordkeeperRiskBasedFundsAvailable = array("Relius", "SRT","Educate","SRT2","Alerus");

    private $em;

    public function removeTableData($table, $findby, $findbyvalue)
    {
        $em = $this->controller->get("doctrine")->getManager();
        $repository = $this->controller->get("doctrine")->getRepository('classesclassBundle:' . $table);
        $objects = $repository->findBy(array($findby => $findbyvalue));
        $counter = 0;
        foreach ($objects as $object)
        {
            $em->remove($object);
            if ($counter == 100)
            {
                $em->flush();
                $counter = 0;
            }
            $counter++;
        }
        $em->flush();
    }

    public function addSTDClassToArray(&$array, $length)
    {
        for ($i = 0; $i < $length; $i++) $array[$i] = new \stdClass();
    }

    public function integerToChecked($integer)
    {

        if ($integer == 1) return "checked";
        else return "";
    }

    public function datatablesFilterData(&$items)
    {
        foreach ($items as &$item)
        {
            foreach ($item as &$value)
            {

                if (is_string($value))
                {
                    $value = str_replace("\\", "", $value);
                    $value = str_replace("&amp;", "&", $value);
                    //$value = str_replace("/","",$value);
                }
            }
        }
    }

    public function datatablesFilterJson(&$jsoncontent)
    {
        $jsoncontent = str_replace(" ", "^//blankspace//^", $jsoncontent);
        $jsoncontent = filter_var($jsoncontent, FILTER_SANITIZE_URL);
        $jsoncontent = str_replace("^//blankspace//^", " ", $jsoncontent);
        $jsoncontent = str_replace("\&quot;", "&quot;", $jsoncontent);
        $jsoncontent = str_replace("\'", "'", $jsoncontent);
        $jsoncontent = str_replace("\'", "'", $jsoncontent);
        $jsoncontent  = str_replace('\\"',"-replaceme-",$jsoncontent);
        $jsoncontent = str_replace('\\','\\\\',$jsoncontent);
        $jsoncontent  = str_replace("-replaceme-",'\\"',$jsoncontent);
    }

    public function __construct($controller)
    {
        $this->controller = $controller;
    }

    public function addSection(&$sections, $field, $description)
    {
        $sections[$field] = new \stdClass();
        $sections[$field]->field = $field;
        $sections[$field]->description = $description;
    }

    public function isIntegrated($account)
    {
        $integratedTypes = array("Relius", "SRT", "Envisage", "ExpertPlan", "RetRev", "DST", "Alerus","Omniinspty","Omniinspty2","RetRev2", "Omniamrts","SRT2", "Omnimoa");
        if (in_array($account->connectionType, $integratedTypes)) {
            return 1;
        }
        return 0;
    }
    public function isOmni($account)
    {
        $comparsion = strpos(strtolower($account->connectionType),"omni");
        if ($comparsion !== false && $comparsion == 0) {
            return true;
        }
        return false;        
    }
    public function isSrt($account)
    {
        $comparsion = strpos(strtolower($account->connectionType),"srt");
        if ($comparsion !== false && $comparsion == 0) {
            return true;
        }
        return false;  
    }
    public function isEducate($account)
    {
        $comparsion = strpos(strtolower($account->connectionType),"educate");
        if ($comparsion !== false && $comparsion == 0) {
            return true;
        }
        return false;
    }
    public function isVwiseMember($session)
    {
        if (strpos($session->masterRoleType, "vwise_") > -1) {
            return true;
        }
        return false;
    }

    public function plansAssociateAvailable($account)
    {
        if ($account->connectionType != "Relius" && !$this->isSrt($account) && $account->connectionType != "ExpertPlan" && $account->connectionType != "Envisage")
        return 0;
        else
        return 1;
    }
    public function plansInvestmentsAvailable($account)
    {
        if ($account->connectionType != "Relius" && !$this->isSrt($account) && $account->connectionType != "ExpertPlan")
        return 0;
        else
        return 1;               
    }

    function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR')) $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED')) $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR')) $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED')) $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR')) $ipaddress = getenv('REMOTE_ADDR');
        else $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    function generateGraphsObject(&$graphs)
    {
        foreach ($graphs as &$graph)
        {
            $graph['json'] = str_replace("\n","", $graph['json'] );
            $graph['options'] = json_decode($graph['json'],true);
            $graph['stringify'] = json_encode($graph['options'],false);
        }
    }    
    function httpmethod()
    {
        if ($_SERVER['HTTP_HOST'] == "127.0.0.1")
        return "http://";
        return  "https://";        
    }
    function calculateAge($bithdayDate)
    {
        $date = new \DateTime($bithdayDate);
        $now = new \DateTime();
        $interval = $now->diff($date);
        return $interval->y;       
    }
    
	function isGuardian($account)
    {
        return ($account->connectionType == "Guardian");
    }
    
	function addDaysToString($string,$days=1)
    {
        if ($days > 0)
        $daystring = "+".$days;
        $daystring = $daystring." day";
        $string = new \DateTime($string);
        $string->modify($daystring);
        return $string->format('Y-m-d');
    }

    function isAssocArray($array) {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }

    function addPlan($em, $userid, $planid = null, $name = null, $clientid = null, $type = "smartplan")
    {
        $this->em = $em;
        $addPlanHelper = new AddPlanHelperFunctions($em);

        $repository = $this->em->getRepository('classesclassBundle:defaultPlan');
        $defaultPlan = $repository->findOneBy(array('userid' => $userid));
        $repository = $this->em->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $userid));
        $repository = $this->em->getRepository('classesclassBundle:defaultModules');
        $module = $repository->findOneBy(array('userid' => $userid));

        $plantobeadded = new plans();
        $plantobeadded->partnerid = $account->partnerid;

        if (!empty($planid))
        {
            $plantobeadded->planid = $planid;
        }
        if (!empty($name))
        {
            $plantobeadded->name = $name;
        }
        $addPlanHelper->addPlanHelper($plantobeadded, $defaultPlan,$type);

        $this->em->persist($plantobeadded);
        $this->em->flush(); //need to do this early to get newest id to copy to other tables
        $addPlanHelper->fillInMissingPlanTables($userid, $plantobeadded->id);
        $addPlanHelper->copyTableData('PlansInvestmentsConfiguration', 'DefaultInvestmentsConfiguration', 'userid', $userid, $plantobeadded->id);
        $addPlanHelper->copyTableData('PlansInvestmentsConfigurationDefaultFundsGroupValuesColors', 'DefaultInvestmentsConfigurationDefaultFundsGroupValuesColors', 'userid', $userid, $plantobeadded->id);
        $addPlanHelper->copyTableData('PlansInvestmentsConfigurationColors', 'DefaultInvestmentsConfigurationColors', 'userid', $userid, $plantobeadded->id);
        $addPlanHelper->copyTableDataException($userid, $plantobeadded->id, "Default", array('userid' => $userid)); // used to compute values for PlansInvestmentsScore and PlansInvestmentsConfigurationFundsGroupValuesColors
        $addPlanHelper->copyTableData("plansDocuments", "defaultDocuments", "userid", $userid, $plantobeadded->id);
        $addPlanHelper->addAccountsUsersPlan($clientid,$plantobeadded->id);
        if ($module->investmentsGraph)
        {
            $addPlanHelper->copyTableData("plansPortfolios", "defaultPortfolios", "userid", $userid, $plantobeadded->id);
        }

        return $plantobeadded->id;
    }

    function logAddPlan($em, $planid, $command = "createRkpData")
    {
        $this->em = $em;
        $logplan = new AddedPlansFromCommand();
        $logplan->planid = $planid;
        $logplan->command = $command;
        $this->em->persist($logplan);
        $this->em->flush();
    }

}
