<?php


namespace Shared\GeneralBundle\Controller;


use classes\classBundle\Entity\abstractclasses\AppForkContent;
use classes\classBundle\Entity\AccountAppForkContent;
use classes\classBundle\Entity\PlanAppForkContent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ForkContentController extends Controller
{
    public function indexAction(Request $request, $mode = 'account')
    {
        $locale = $request->get("locale", "en");
        $forkContent = $this->getForkContent($mode);
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansInterface");
        return $this->render('Shared/ForkContent/index.html.twig', [
            'forkContent' => $forkContent,
            'localeContent' => $forkContent->getContent($locale),
            'locale' => $locale,
            'writeable' => $writeable,
            'mode' => $mode
        ]);
    }

    public function saveAction(Request $request, $mode = 'account')
    {
        $session = $this->get('adminsession');
        $locale = $request->get("locale");
        $entityManager = $this->getDoctrine()->getManager();
        /** @var $forkContent AppForkContent */
        $forkContent = $this->getForkContent($mode);
        $forkContent->userid = $session->userid;
        if ($mode === 'plan')
        {
            $forkContent->planid = $session->planid;
        }
        $entityManager->persist($forkContent);
        $forkContent->setContent($request->get("localeContent"), $locale);
        $forkContent->copy($request->get("forkContent"));
        if (property_exists($forkContent, 'published'))
        {
            $forkContent->published = $request->get("published");
        }
        $entityManager->flush();
        return new Response("");
    }

    public function previewAction($mode = 'account')
    {
        return $this->render('Shared/ForkContent/preview.html.twig');
    }

    protected function getForkContent($mode)
    {
        $table = $mode === 'account' ? AccountAppForkContent::class : PlanAppForkContent::class;
        $repository = $this->getDoctrine()->getRepository($table);
        $params = [];
        if ($mode === 'plan')
        {
            $params['planid'] = $this->get("adminsession")->planid;
        }
        if ($mode === 'account')
        {
            $params['userid'] = $this->get("adminsession")->userid;
        }
        $forkContent = $repository->findOneBy($params);
        if (is_null($forkContent))
        {
            $forkContent = new $table;
        }
        return $forkContent;
    }
}