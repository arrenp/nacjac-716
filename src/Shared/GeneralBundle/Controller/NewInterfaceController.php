<?php


namespace Shared\GeneralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewInterfaceController extends Controller
{
    public function saveAction(Request $request, $mode = 'account')
    {
        $params = $request->request->all();
        $params['userid'] = $this->get("session")->get("userid");
        if ($mode === 'plan')
        {
            $params['planid'] = $this->get("session")->get("planid");
            $this->get("StyleService")->saveHex($this->get("PlanAppColorsService"),$params,["planid" => $this->get("session")->get("planid")]);
        }
        if ($mode === 'account')
        {
            $this->get("StyleService")->saveHex($this->get("AccountAppColorsService"),$params,["userid" => $this->get("session")->get("userid")]);
        }
        return new Response("");
    }
}