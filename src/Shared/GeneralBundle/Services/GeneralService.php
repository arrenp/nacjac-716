<?php

namespace Shared\GeneralBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

class GeneralService {

    private $container;
    
    public function __construct(containerInterface $container) {
        $this->container = $container;
    }

    function postToSlack($severity, $appname, $message, $format = "default")
    {
        $severity = strtolower($severity);
        switch ($severity) {
            case "info": $url = $this->container->getParameter('batch_job_output');
                break;
            case "error": $url = $this->container->getParameter('batch_job_error');
                break;
            default: $url = $this->container->getParameter('batch_job_output');
                break;
        }

        $encodedData = json_encode(['text' => "{$appname}: {$message}"]);
        if ($format === "code") {
            $encodedData = json_encode(['text' => "{$appname}: `{$message}`"]);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Content-Length:' . strlen($encodedData)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
    }
    
}
