<?php

namespace Shared\LibraryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\DocumentsBundle\Classes\documents;
use \Spe\AppBundle\Services\PlanService;

abstract class LibraryController extends Controller
{
    protected $tablename;
    protected $findBy;
    protected $twigParameters;
    protected $planid;
    protected $userid;
    protected $writeable;
    protected $path;

    public function indexAction()
    {
        $this->init();
        $mainDirectory = "https://c06cc6997b3d297a6674-653de9dab23a7201285f2586065c1865.ssl.cf1.rackcdn.com/video/library/";
        $m4v = "m4v/";
        $webmv = "webmv/";
        $mp4 = ".mp4";
        $webm = ".webm";

        $library = $this->get('spe.app.plan')->getLibraryData();

        $libraryFiles = array();
        foreach ($library as $lib) {
            $file = array(
                'title' => $lib['title'],
                'value' => $lib['value'],
                'mp4' => $mainDirectory . $lib['directory'] . "/" . $m4v . $lib['video'] . $mp4,
                'webm' => $mainDirectory . $lib['directory'] . "/" . $webmv . $lib['video'] . $webm
            );
            $libraryFiles[$lib['type']][] = $file;
        }

        $library = array();

        // get displaynames from plansLibraryType
        $repo = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $libTypeTable = $repo->findAll();
        $typeMap = array();
        foreach($libTypeTable as $libType) {
            $typeMap[$libType->type] = $libType->displayname;
        }

        foreach ($libraryFiles as $key => $libFiles) {
            $data = $this->createLibrarySection($libFiles);

            $library[$key] = new \stdClass();
            $library[$key]->description = $typeMap[$key];
            $library[$key]->data =  $data;
            $library[$key]->table =  $this->tablename . "LibraryUsers";
        }

        $libraryRowLength = 12/count($library);

        return $this->render($this->path . 'Library:index.html.twig',array(
            'userid' => $this->userid,
            'planid' => $this->planid,
            "writeable" => $this->writeable,
            "documents" => $this->documents,
            "library" => $library,
            "libraryRowLength" => $libraryRowLength));
    }

    private function createLibrarySection($files)
    {

        $librarySection = array();

        $usersRepo = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->tablename . 'LibraryUsers');
        $videosRepo = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');

        $videosList = $videosRepo->findAll();
        $plansList = $usersRepo->findBy($this->findBy);

        $planCheckedVideos = array();
        $videosArray = array();

        // preprocess. create an array of videos the planid has checked
        foreach($videosList as $item) {
            $videosArray[$item->id] = array("name" => $item->name);
        }
        foreach($plansList as $item) {
            array_push($planCheckedVideos, $videosArray[$item->videoid]["name"]);
        }

        foreach ($files as $file) {

            $section = new \stdClass();
            $section->title = $file['title'];
            $section->value = $file['value'];
            $section->mp4 = $file['mp4'];
            $section->webm = $file['webm'];
            $value = $file['value'];
            if (in_array($value, $planCheckedVideos, true)) {
                $section->checked = "checked";
                $section->data = 1;
            }
            else {
                $section->checked = "";
                $section->data = 0;
            }
            $section->videourl = $this->generateUrl('_plans_outreach_videoplayer_videoonly_WithoutLibrary')."?mp4=".$section->mp4."&webm=".$section->webm;
            $librarySection[] = $section;
        }

        return $librarySection;
    }

    public function init()
    {
        $search = $this->get('generalfunctions');

        $this->documents = new documents($this->getDoctrine(),"plans",$search,$this->userid,$this->writeable,$this->planid);
        $this->documents->indexAction();
        $this->templatepath = "SettingsSettingsBundle:Documents:";
    }

}
