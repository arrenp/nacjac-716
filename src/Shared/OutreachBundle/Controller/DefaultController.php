<?php

namespace Shared\OutreachBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use classes\classBundle\Entity\plans;
use Shared\RecordkeepersBundle\Entity\recordKeeper;
use classes\classBundle\Entity\plansOutreach;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Shared\OutreachBundle\Classes\outreach;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Utilities\mailer\PHPMailer;
use Utilities\mailer\SMTP;
use Utilities\mailer\mailerInit;
class DefaultController extends Controller
{
	public $userid;
	public $planid;
	public $adviserid;

  	public function sendTestEmailAction()
	{
		$mail = new PHPMailer(true);
		$mail->IsSMTP();
		$mailerInit = new mailerInit($mail); 

		$this->init();

		$request = Request::createFromGlobals();

		// the URI being requested (e.g. /about) minus any query parameters
		$request->getPathInfo();

		// retrieve GET and POST variables respectively
		$templateid = $request->request->get("id","");
                echo $templateid . 'cuong';
		$emails = $request->request->get("emails","");

		$planid = $request->request->get("planid","");
		$adviserid = $request->request->get("adviserid","");

		if ($planid == "")
		$planid = $this->planid;


		$repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachTemplates');
		//check if email address exists
		$template= $repository->findOneBy(array("id" => $templateid) );;


		$adviserurl = "";
		if ($adviserid != null)
		$adviserurl = "&adviserid=".$adviserid;

			//if email exists, send email
			if (isset($template))
			{
			try
			{

			$templateUrl = $this->generateUrl('_plans_outreach_templates', array("category" => $template->category, "name" => $template->filename), true )."?planid=".$planid.$adviserurl."&templateid=".$templateid;
			$emailContents = file_get_contents($templateUrl);
			//pass in name, password, id and email address into email template

			$emails = explode(",",$emails);

			for ($i = 0; $i < count($emails); $i++)
			{
				try 
				{
				  $mail->AddAddress($emails[$i]);

				  echo "Message Sent OK<p></p>\n";
				} 
				catch (phpmailerException $e) 
				{
				  echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} 
				catch (Exception $e) 
				{
				  echo $e->getMessage(); //Boring error messages from anything else!
				}
			}
			$mail->Subject = $request->request->get("title","");
			$mail->MsgHTML($emailContents);
			$mail->Send();
			//send response to user that email has been sent
			return new Response("sent");
			}
			catch (Exception $e)
			{
			return new Response("Error occured!: ".$e->getMessage());
			}

			}
			else  //else, inform user email does not exist
			return new Response("template does not exist");
			


	}

	public function init()
	{
		$session = $this->get('adminsession');
		$this->userid = $session->userid;
		$this->planid = $session->planid;
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		if ($request->query->get("planid") != "")
		$this->planid = $request->query->get("planid");
		if ($session->roleType == "adviser")
		$this->adviserid = $session->clientid;
		else
		$this->adviserid = 0;
	}


}
