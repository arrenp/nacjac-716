<?php

namespace Shared\TranslationBundle;

use Shared\TranslationBundle\DependencyInjection\Compiler\TranslatorPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SharedTranslationBundle extends Bundle
{
    public function build(ContainerBuilder $container) {
        $container->addCompilerPass(new TranslatorPass());
    }
}
