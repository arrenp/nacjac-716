<?php

namespace Shared\TranslationBundle\Translation;

use Symfony\Bundle\FrameworkBundle\Translation\Translator as BaseTranslator;
use Symfony\Component\Translation\MessageCatalogue;

class Translator extends BaseTranslator {
    
    /** @var integer */
    private $accountId;
    
    protected function loadCatalogue($locale) {
        parent::loadCatalogue($locale);
        $this->loadDatabaseCatalogue($locale);
    }
    
    private function loadDatabaseCatalogue($locale) {

        $session = $this->container->get('session');
        if ($this->accountId === null) {
            $this->setAccountId($session->get("account")["id"]);
        }
        $planid = $session->get("module")['planid'];
        $fallbackAccountId = null;
        if ($this->accountId != null) {
            $repository = $this->container->get("doctrine")->getRepository("classesclassBundle:accounts");
            $account = $repository->find($this->accountId);
            $fallbackAccountId = $account->translationId;
        }

        $repository = $this->container->get("doctrine")->getRepository("classesclassBundle:languages");
        $languages = $repository->findAll();
        $languageMap = array();
        foreach ($languages as $language) {
            $languageMap[$language->id] = $language->name; 
        }
        
        $sql = "
            select
                l.name,
                t.domain,
                t.sourceKey,
                dt.`content`
            from
                DefaultTranslations dt
                left join languages l on l.id = dt.languageId
                left join TranslationSources t on t.id = dt.sourceId

            UNION

            SELECT 
                l.name,
                t.domain,
                t.sourceKey,
                at.`content`
            FROM 
                AccountsTranslations at 
                left join languages l on l.id = at.languageId
                left join TranslationSources t on t.id = at.sourceId
            WHERE 
                at.accountId = ?

            UNION
            
            SELECT 
                l.name,
                t.domain,
                t.sourceKey,
                at.`content`
            FROM 
                AccountsTranslations at 
                left join languages l on l.id = at.languageId
                left join TranslationSources t on t.id = at.sourceId
            WHERE 
                at.accountId = ?

            UNION
            
            SELECT 
                l.name,
                t.domain,
                t.sourceKey,
                pt.`content`
            FROM 
                PlansTranslations pt 
                left join languages l on l.id = pt.languageId
                left join TranslationSources t on t.id = pt.sourceId
            WHERE 
                pt.accountId = ?
                and pt.planId = ?
                
        ";
        $conn = $this->container->get("doctrine")->getConnection();
        $statement = $conn->prepare($sql);
        $statement->bindValue(1, $fallbackAccountId);
        $statement->bindValue(2, $this->accountId);
        $statement->bindValue(3, $this->accountId);
        $statement->bindValue(4, $planid);
        $statement->execute();
        $data = $statement->fetchAll();

        $messages = array();
        foreach ($data as $datum) {
            if (!array_key_exists($datum['name'], $messages)) {
                $messages[$datum['name']] = array();
            }
            if (!array_key_exists($datum['domain'], $messages[$datum['name']])) {
                $messages[$datum['name']][$datum['domain']] = array();
            }
            if (!array_key_exists($datum['sourceKey'], $messages[$datum['name']][$datum['domain']])) {
                $messages[$datum['name']][$datum['domain']][$datum['sourceKey']] = array();
            }
            $messages[$datum['name']][$datum['domain']][$datum['sourceKey']] = $datum['content'];
        }

        $messages[$locale] = isset($messages[$locale]) ? $messages[$locale] : array();
        $this->catalogues[$locale]->addCatalogue(new MessageCatalogue($locale, $messages[$locale]));
        
        $current = $this->catalogues[$locale];
        while($fallbackCatalogue = $current->getFallbackCatalogue()) {
            $fallbackLocale = $fallbackCatalogue->getLocale();
            $messages[$fallbackLocale] = isset($messages[$fallbackLocale]) ? $messages[$fallbackLocale] : array();
            $fallbackCatalogue->addCatalogue(new MessageCatalogue($fallbackLocale, $messages[$fallbackLocale] ));
            $current = $fallbackCatalogue;
        }
    }


    public function hasLocale($locale) {
        $catalogue = $this->getCatalogue($locale);
        return (!empty($catalogue->getDomains()));
    }
	
    public function setAccountId($accountId) {
        $this->accountId = $accountId;
    }

    public function getMessages() {
        $catalogue = $this->getCatalogue($this->getLocale());
        $messages = $catalogue->all();

        while ($catalogue = $catalogue->getFallbackCatalogue()) {
            $messages = array_replace_recursive($catalogue->all(), $messages);
        }
        return $messages;
    }

}
