<?php
namespace Shared\ProfilesBundle\Services;
use Symfony\Component\HttpFoundation\Request;

class ProfilesService
{
	public $profiles;
	public $lastid;
	public $fieldTypes;
	public $decryptionKey;
	public $sectionDescriptions;
	public $sectionNames;
	public $sectionTypes;
	public $sectionDatabases;
	public $conn;
	public $limit;
	public $validFields;

    public function __construct($conn, $lastid = -1)
    {
        $this->lastid = $lastid;
        $this->decryptionKey = '5E6D217336';
        $this->limit = 500;
        $this->fieldTypes = array();
        $this->conn = $conn;
    }

	public function addSearchField($description,$name,$type,$database,$additional)
	{
		$this->sectionDescriptions[] = $description;
		$this->sectionNames[] = $name;
		$this->sectionTypes[] = $type;
		$this->sectionDatabases[] = $database;
		$this->sectionAdditionals[] = $additional;
	}

	public function validFields()
	{
		$this->validFields = array();
		$profilesText = "profiles.";
		$participantsText = "participants.";
		$profilesRetirementNeedsText = "profilesRetirementNeeds.";
		$profilesContributionsText = "profilesContributions.";
		$profilesRiskProfileText = "profilesRiskProfile.";
		$profilesInvestmentsText = "profilesInvestments.";
		$profilesBeneficiariesText = "profilesBeneficiaries.";
		
		$profilesArray = ["id","currentBalance","participantid","planName","reportDate","planid","profileId"];		
		$participantsArray = ["profileid","firstName","lastName","email","employeeId"];
		$profilesRetirementNeedsArray = ["otherAssets","profileid","recommendedMonthlyPlanContribution","assetTypes"];
		$profilesContributionsArray = ["catchup","pre","profileid","roth","post","preCurrent","rothCurrent","mode","cmode","currentYearlyIncome"];
		$profilesRiskProfileArray = ["profileid"];	
		$profilesInvestmentsArray = ["profileid","type","pname"];
		$profilesBeneficiariesArray = ["profileid","firstName","lastName","gender","relationship","maritalStatus","ssn","percent","type"];
		
		/*			
		$profilesArray = ["id","currentBalance","planName","reportDate","planid","profileId"];		
		$participantsArray = ["firstName","lastName"];
		$profilesRetirementNeedsArray = ["otherAssets","recommendedMonthlyPlanContribution","assetTypes"];
		$profilesContributionsArray = ["catchup","pre","roth","preCurrent","rothCurrent"];
		$profilesRiskProfileArray = [];	
		$profilesInvestmentsArray = ["pname"];
		$profilesBeneficiariesArray = [];
		*/


		$this->addToValidFields($profilesText,$profilesArray);
		$this->addToValidFields($participantsText,$participantsArray);
		$this->addToValidFields($profilesContributionsText,$profilesContributionsArray);
		$this->addToValidFields($profilesRetirementNeedsText,$profilesRetirementNeedsArray);
		$this->addToValidFields($profilesRiskProfileText,$profilesRiskProfileArray);
		$this->addToValidFields($profilesInvestmentsText,$profilesInvestmentsArray);
		$this->addToValidFields($profilesBeneficiariesText,$profilesBeneficiariesArray);
	}
	public function addToValidFields($table,$fields)
	{
		foreach ($fields as $field)
		{
			$this->validFields[$table.$field] = 1;//use a hash table for faster searching
		}
	}


	public function sections()
	{
		$sections = array();
		//old
		$this->addSearchField("Current Balance","currentBalance","number","profiles","");
		$this->addSearchField("Outside Asset Amount","otherAssets","number","profilesRetirementNeeds","");
		$this->addSearchField("Catch Up Contributions","catchup","number","profilesContributions","");
		$this->addSearchField("Pre-Tax per paycheck","pre","number","profilesContributions","");
		$this->addSearchField("After-Tax (Roth) per paycheck","roth","number","profilesContributions","");
		$this->addSearchField('Recommended Monthly Plan Contribution',"recommendedMonthlyPlanContribution","number","profilesRetirementNeeds","");
		$this->addSearchField("Contributions Current","preCurrent","number","profilesContributions","");
		$this->addSearchField("Roth Contributions Current","rothCurrent","number","profilesContributions","");
		$this->addSearchField("Contribution %","pre_PLUS_roth","number","profilesContributions","AND profilesContributions.mode='PCT'");
		$this->addSearchField("Outside Asset Types","assetTypes","checkboxes","profilesRetirementNeeds","");
		$this->addSearchField("Investment Type","type","checkboxes","profilesInvestments","");
		$this->addSearchField("Report Date","reportDate","date","profiles","");
        $this->addSearchField("Miscellaneous","testing","checkboxes","plans","");


		$i = 0;
		foreach ($this->sectionDescriptions as $description)
		{
			$sections[$i] = new \stdClass();
			$sections[$i]->description = $description;
			$i++;
		}
		$i = 0;
		foreach ($this->sectionTypes as $type)
		{

			$sections[$i]->type = $type;
			$i++;
		}
		$i = 0;
		foreach ($this->sectionDatabases as $database)
		{

			$sections[$i]->database = $database;
			$i++;
		}
		$i = 0;
		foreach ($this->sectionAdditionals as $additional)
		{

			$sections[$i]->additional = $additional;
			$i++;
		}		
		$i = 0;

		foreach ($this->sectionNames as $name)
		{
			$sections[$i]->name = $name;
			if ($name == "assetTypes")
			{
				$sections[$i]->descriptions = array(0 => "Stocks Stock Mutual Funds", 1 => "Former Retirement Account",
				2 => "Individual Retirement Account (IRA)",3 => "Certificate of Deposit",4 => "Annuities", 5 => "Bonds Bonds Mutual Funds",
				6 => "Insurance", 7 => "Money Market Fund");
				$sections[$i]->values = array(0 => "Stocks Stock Mutual Funds", 1 => "Former Retirement Account",
				2 => "Individual Retirement Account",3 => "Certificate of Deposit",4 => "Annuities", 5 => "Bonds Bonds Mutual Funds",
				6 => "Insurance", 7 => "Money Market Fund");
			}

			if ($name == "type")
			{

				$sections[$i]->descriptions = array(0 => "Advice", 1 => "Custom",
				2 => "Default",3 => "Risk",4 => "Target");
				$sections[$i]->values = array(0 => "ADVICE", 1 => "CUSTOM",
				2 => "DEFAULT",3 => "RISK",4 => "TARGET");
			}
            if ($name == "testing")
            {
                $sections[$i]->descriptions = array(0 => "Hide Test Plan Profiles");
                $sections[$i]->values = array(0 => "0");
            }
			$i++;
		}

		return $sections;

	}
	public function search()
	{
		if ($this->lastid > -1)
		$profiles = $this->conn->fetchAll('SELECT * FROM profiles  WHERE id < '.$this->lastid.' ORDER BY id desc  LIMIT '.$this->limit);
		else
		$profiles = $this->conn->fetchAll('SELECT * FROM profiles ORDER BY id desc LIMIT '.$this->limit);

		foreach ($profiles as &$profile)
		{
			$this->returnProfileTables($profile);

			$this->filterProfileVariable($profile);

			$this->lastid = $profile['id'];			
		}
		return $profiles;
	}

	public function returnProfile($id)
	{
		$id = intval($id);//id must be interger
		$profiles = $this->conn->fetchAll('SELECT * FROM profiles WHERE id = '.$id.' LIMIT 1');
		$profile = $profiles[0];
		$this->returnProfileTables($profile);
		$this->filterProfileVariable($profile);	
		return $profile;
	}
	public function returnProfileTables(&$profile)
	{

		$participantQuery = 'SELECT '.$this->generateQueryFields("participants").' FROM participants WHERE id = "'.$profile['participantid'].'"';
		$profile['participant'] = $this->findOne($participantQuery);

		$beneficiariesQuery = 'SELECT '.$this->generateQueryFields("profilesBeneficiaries").' FROM profilesBeneficiaries WHERE profileid = "'.$profile['id'].'"';
		$profile['beneficiaries'] = $this->findAll($beneficiariesQuery);
		

		$contributionsQuery = 'SELECT '.$this->generateQueryFields("profilesContributions").' FROM profilesContributions WHERE profileid = "'.$profile['id'].'"';
		$profile['contributions'] = $this->findOne($contributionsQuery );

		$investmentsQuery = 'SELECT '.$this->generateQueryFields("profilesInvestments").' FROM profilesInvestments WHERE profileid = "'.$profile['id'].'"';
		$profile['investments'] = $this->findAll($investmentsQuery);	

		$retirementNeedsQuery = 'SELECT '.$this->generateQueryFields("profilesRetirementNeeds").' FROM profilesRetirementNeeds WHERE profileid = "'.$profile['id'].'"';
		$profile['retirementNeeds'] = $this->findOne($retirementNeedsQuery);

		$riskProfileQuery = 'SELECT '.$this->generateQueryFields("profilesRiskProfile").' FROM profilesRiskProfile WHERE profileid = "'.$profile['id'].'"';
		$profile['riskProfile'] = $this->findOne($riskProfileQuery);

	}

	private function filterProfileVariable(&$profile)
	{
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$profile['participantName'] = $profile['participant']['participants_firstName'].' '.$profile['participant']['participants_lastName'];
		$profile['contributions']['preRoth'] = $this->addFields($profile['contributions']['pre'],$profile['contributions']['roth']);
		$profile['contributions']['preRothPost'] = $this->addFields($profile['contributions']['pre'],$profile['contributions']['roth'], $profile['contributions']['post']);
		if (isset($profile['investments'][0]))
		{
			$profile['investmentType'] = $profile['investments'][0]['profilesInvestments_type'];
			$profile['investmentPname'] = $profile['investments'][0]['profilesInvestments_pname'];
		}
		$profile['retirementNeeds']['assetTypes'] = explode("^",$profile['retirementNeeds']['assetTypes']);
	}
	function addFields($field1,$field2,$field3 = null)
	{
		if ($field1 == null) {
            $field1 = 0;
        }
		if ($field2 == null) {
            $field2 = 0;
        }
		if ($field3 == null) {
		    $field3 = 0;
        }
		return $field1 + $field2 + $field3;

	}

	private function findOne($querystring)
	{
		$rows = $this->conn->fetchAll($querystring);
		if (count($rows) > 0)
		return $rows[0];
		return null;
	}
	private function findAll($querystring)
	{
		$rows = $this->conn->fetchAll($querystring);
		return $rows;
	}
	public function generateQueryFields($table,$allfields = 0,$encryption = 1)
	{
		$this->validFields();
		$fields = $this->fields($table);
		$encyptedFields = $this->encyptedFields($table);
		
		$counter = 0;
		$querystring = "";
		foreach ($fields as $field)
		{	
			if (isset($this->validFields[$table.".".$field]) || $allfields)
			{
                if ($counter > 0) {
                    $querystring = $querystring.", ";
                }
                if ($allfields == 0)
                {
                    $querystring = $querystring.$table.".".$field;
                    $querystring = $querystring.", ";
                }
				$querystring = $querystring.$table.".".$field." as ".$table."_".$field;
				$this->fieldTypes[$table.".".$field] = false;
				$counter++;
				
			}
		}
		
		foreach ($encyptedFields as $field)
		{
			if ((isset($this->validFields[$table.".".$field]) || $allfields) )
			{
                            if ($encryption)
                            {
				if ($counter > 0)
				$querystring = $querystring.", ";
				$querystring = $querystring."AES_DECRYPT(".$table.".".$field.", UNHEX('".$this->decryptionKey."')) as ".$table."_".$field;
				$this->fieldTypes[$table.".".$field] = true;
				$counter++;
                            }
                            else
                            {
                                if ($counter > 0)
				$querystring = $querystring.", ";
				$querystring = $querystring.$table.".".$field." as ".$table."_".$field; 
				$this->fieldTypes[$table.".".$field] = true;
				$counter++;                                
                            }
			}
		}


		return $querystring;
	}
	public function encyptedFields($table)
	{
		$encyptedArray = array();
		$columnsArrayQuery = "SHOW COLUMNS FROM ".$table;
		$columnsArrayResult = $this->conn->fetchAll($columnsArrayQuery);
		foreach ($columnsArrayResult as $row)
		{
			if (substr_count($row['Type'],"blob") > 0)
			$encyptedArray[] = $row['Field'];
		}
		return $encyptedArray;
	}

	public function fields($table)
	{
		$unencyptedArray = array();
		$columnsArrayQuery = "SHOW COLUMNS FROM ".$table;
		$columnsArrayResult = $this->conn->fetchAll($columnsArrayQuery);
		foreach ($columnsArrayResult as $row)
		{
			if (substr_count($row['Type'],"blob") == 0)
			$unencyptedArray[] = $row['Field'];
		}
		return $unencyptedArray;

	}
}

?>