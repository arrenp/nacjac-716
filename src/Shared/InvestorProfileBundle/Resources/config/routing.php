<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('shared_investor_profile_homepage', new Route('/hello/{name}', array(
    '_controller' => 'SharedInvestorProfileBundle:Default:index',
)));

return $collection;
