<?php

namespace Shared\MessagingBundle\Classes;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use classes\classBundle\Entity\PlansMessages;

class Messaging
{

    public $container;
    
    /**
     * 
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function messages($type)
    {
        $session = $this->container->get("session");
        $userid = $session->get("userid");
        $table = ($type == "plans") ? "PlansMessages" : "DefaultMessages";
        $repository = $this->container->get("doctrine")->getRepository("classesclassBundle:$table");
        $languages = $this->container->get("AccountsService")->getLanguages($userid);
        $sql = "
            SELECT
                p.*,
                g.name AS groupName
            FROM
                messagesPanes AS p
                LEFT JOIN messagesPanesGroups AS g ON g.id = p.messagesPanesGroupsId
            ORDER BY 
                g.displayOrder ASC,
                p.displayOrder ASC
        ";
        $em = $this->container->get("doctrine")->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $messages = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $groupName = $row['groupName'];
            $nextSection = new \stdClass();
            $nextSection->id = $row['id'];
            $nextSection->name = $row['name'];
            $nextSection->description = $row['description'];
            $messages[$groupName]['data'][$nextSection->id]['section'] = $nextSection;           
            foreach ($languages as $language)
            {                
                $nextMessage = new \stdClass();
                $findBy = array();
                $findBy['messagesPanesId'] = $row['id'];
                $findBy['userid'] = $userid;
                $findBy['language'] = $language->name;

                if ($type == "plans") {
                    $findBy['planid'] = $session->get("planid");
                }
                $currMessage = $repository->findOneBy($findBy);

                $nextMessage->value = !empty($currMessage->message) ? $currMessage->message : '';
                $nextMessage->active = !empty($currMessage->isActive) ? $currMessage->isActive : 0;
                $nextMessage->language = $language;
                
                if (!isset($messages[$groupName]['title']))
                {
                    $messages[$groupName]['title'] = $groupName;
                }

                $messages[$groupName]['data'][$nextSection->id]['records'][$language->name] = $nextMessage;
            }
        }
        return $messages;
    }
    
    public function messagesPanes($em) {
        $sql = "
            SELECT
                p.*,
                g.name AS groupName
            FROM
                messagesPanes AS p
                LEFT JOIN messagesPanesGroups AS g ON g.id = p.messagesPanesGroupsId
            ORDER BY 
                g.displayOrder ASC,
                p.displayOrder ASC
        ";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
}
