<?php

namespace Shared\ContactBundle\Classes;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


class contact
{
	public $em;
	public $type;//settings or plans
	public $planTable;
	public $findplanby;


	public function indexAction()
	{
            $repository =	$this->em->getRepository('classesclassBundle:'.$this->planTable);
            $plan = $repository->findOneBy($this->findplanby);
            return $this->setContact($plan);
	}

	public function defaultContact()
	{
            $repository =	$this->em->getRepository('classesclassBundle:defaultPlan');
            $plan = $repository->findOneBy(array("userid" => $this->userid));
            return $this->setContact($plan);
	}
        public function syncContacts($userid,$connection)
        {
            $repository= $this->em->getRepository('classesclassBundle:defaultPlan');
            $defaultPlan = $repository->findOneBy(array("userid" => $userid));
            $sqls = [];
            $contact = $this->setContact($defaultPlan);  
            $sqlArray = [];
            foreach ($contact as $key => $value)
            {
                $sqlArray[$key] = $value['value'];
            }        
            foreach ($sqlArray as $key => $value)
            {
                $sqls[] = $key." = :".$key;
                $params[":".$key] = $value;
            }
            $params[":userid"] = $userid;
            $sql = "UPDATE plans SET ".implode(",",$sqls)." where userid = :userid";
            $stmt = $connection->prepare($sql);
            $stmt->execute($params);
        }
        public function syncContactPlan($userid,$planid)
        {
            $repository =	$this->em->getRepository('classesclassBundle:defaultPlan');
            $defaultPlan = $repository->findOneBy(array("userid" => $userid));    
            $repository =	$this->em->getRepository('classesclassBundle:plans');
            $plan = $repository->findOneBy(array("id" => $planid));  
            $contact = $this->setContact($defaultPlan);
            foreach ($contact as $key => $value)
            {
                $plan->{$key} = $value['value'];
            }
            $this->em->getManager()->flush();
        }
        
        public function setContact($plan)
        {
            $contact['company']['value'] = $plan->company;
            $contact['phone']['value'] = $plan->phone;
            $contact['phone2']['value'] = $plan->phone2;
            $contact['address']['value'] = $plan->address;
            $contact['address2']['value'] = $plan->address2;
            $contact['city']['value'] = $plan->city;
            $contact['state']['value'] = $plan->state;
            $contact['zip']['value'] = $plan->zip;
            $contact['email']['value'] = $plan->email;
            $contact['company']['description'] = "Company: ";
            $contact['phone']['description'] = "Phone Number: ";
            $contact['phone2']['description'] = "Second Phone Number: ";
            $contact['address']['description'] = "Address: ";
            $contact['address2']['description'] = "Address 2: ";
            $contact['city']['description'] = "City: ";
            $contact['state']['description'] = "State: ";
            $contact['zip']['description'] = "Zip: ";
            $contact['email']['description'] = "Email: ";
            $contact['advisorFirst']['value'] = $plan->advisorFirst;
            $contact['advisorLast']['value'] = $plan->advisorLast;
            $contact['advisorEmail']['value'] = $plan->advisorEmail;
            $contact['advisorPhone']['value'] = $plan->advisorPhone;
            $contact['advisorFirst']['description'] = "Advisor First: ";
            $contact['advisorLast']['description'] = "Advisor Last: ";
            $contact['advisorEmail']['description'] = "Advisor Email: ";
            $contact['advisorPhone']['description'] = "Advisor Phone: ";
            return $contact;
        }

   	public function __construct($em,$type,$userid,$planid = 0)
	{
		$this->em = $em;
		$this->type = $type;
		$this->userid = $userid;
		$this->planid = $planid;
		if ($type == "settings")
		{
			$this->planTable = "defaultPlan";
			$this->findplanby =array("userid" => $this->userid);

		}

		if ($type == "plans")
		{

			$this->planTable = "plans";
			$this->findplanby =array("id" => $this->planid);

		}

	}

}
