<?php

namespace Shared\PromoDocsBundle\Classes;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Shared\RecordkeepersBundle\Entity\recordKeeper;


class promoDocs
{
	public $em;
   	public function __construct($em)
	{
		$this->em = $em;
	}

	public function promoDocsCategories()
	{
		$repository = $this->em->getRepository('classesclassBundle:managePromoDocsCategories');
		$promoDocsCategories= $repository->findBy(array(),array("orderid" => "ASC"));
		return $promoDocsCategories;

	}
	public function promoDocsSections($id)
	{

		$repository = $this->em->getRepository('classesclassBundle:managePromoDocsSections');
		$promoDocsSections = $repository->findBy(array("parentid" => $id),array("orderid" => "ASC"));


		foreach ($promoDocsSections as $section)
		{

			$repository = $this->em->getRepository('classesclassBundle:managePromoDocs');
			$promoDocs = $repository->findBy(array("parentid" => $section->id),array("orderid" => "ASC"));
			if (count($promoDocs) > 0)
			$section->docs = $promoDocs;

		}

		return $promoDocsSections;


	}

	public function deleteCategory($id)
	{
		$repository = $this->em->getRepository('classesclassBundle:managePromoDocsCategories');
		$category= $repository->findOneBy(array("id" => $id));

		$repository = $this->em->getRepository('classesclassBundle:managePromoDocsSections');
		$sections= $repository->findBy(array("parentid" => $id));

		foreach ($sections as $section)
		{

			$repository = $this->em->getRepository('classesclassBundle:managePromoDocs');
			$docs = $repository->findBy(array("parentid" => $section->id),array("orderid" => "ASC"));

			foreach ($docs as $doc)
			{
				$this->em->getManager()->remove($doc);
				$this->em->getManager()->flush();
			}

			$this->em->getManager()->remove($section);
			$this->em->getManager()->flush();

		}



		$this->em->getManager()->remove($category);
		$this->em->getManager()->flush();

	}

	public function deleteSection($id)
	{
		$repository = $this->em->getRepository('classesclassBundle:managePromoDocsSections');
		$section= $repository->findOneBy(array("id" => $id));
		echo $section->name.'<br/>';
		$repository = $this->em->getRepository('classesclassBundle:managePromoDocs');
		$docs = $repository->findBy(array("parentid" => $section->id),array("orderid" => "ASC"));

		foreach ($docs as $doc)
		{

			$this->em->getManager()->remove($doc);
			$this->em->getManager()->flush();
		}

		$this->em->getManager()->remove($section);
		$this->em->getManager()->flush();

	}

}
