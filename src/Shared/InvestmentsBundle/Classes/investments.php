<?php

namespace Shared\InvestmentsBundle\Classes;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use classes\classBundle\Entity\defaultFunds;
use classes\classBundle\Entity\defaultPortfolios;
use classes\classBundle\Entity\defaultPortfolioFunds;
use classes\classBundle\Entity\plansFunds;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansPortfolioFunds;
use Shared\RecordkeepersBundle\Entity\recordKeeper;
use classes\classBundle\Entity\recordkeeperPlans;
use classes\classBundle\Entity\defaultRiskBasedFunds;
use classes\classBundle\Entity\plansRiskBasedFunds;
class investments
{
	public $em;
	public $type;//settings or plans
	public $fundlist;//ajax search fundlist
	public $portfoliolist;//ajax search portfoliolist
	public $search;//ajax search object
	public $fundTable;//fund table based off type
	public $portfolioTable;//portfolio table based of type
	public $planTable;
	public $portfolioFundsTable;
	public $userid;//current userid
	public $planid;//current planid
	public $searchPath;//path of search templates
	public $allfunds;//funds with no score based off portfolio id
	public $portfoliofunds;//funds with score based off portfolio id
	public $findplanby;
	public $findinvestmentby;
	public $writeable;

	public function indexAction()
	{


		if ($this->type == "settings")
		{
			if ($this->writeable)
			{
				$this->fundlist =  $this->search->search($this->fundTable,"userid",$this->userid,"funds",$this->searchPath."fundlist.html.twig","name,ticker","fundlist","inline-block","classesclassBundle:Search:search.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
				$this->portfoliolist = $this->search->search($this->portfolioTable,"userid",$this->userid,"portfolios",$this->searchPath."portfoliolist.html.twig","name","portfoliolist","inline-block","classesclassBundle:Search:search.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
			}
			else
			{
			$this->fundlist =  $this->search->search($this->fundTable,"userid",$this->userid,"funds","SharedInvestmentsBundle:Default:search/fundlistreadonly.html.twig","name,ticker","fundlist","inline-block","classesclassBundle:Search:search.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
			$this->portfoliolist = $this->search->search($this->portfolioTable,"userid",$this->userid,"portfolios","SharedInvestmentsBundle:Default:search/portfoliolistreadonly.html.twig","name","portfoliolist","inline-block","classesclassBundle:Search:search.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
			}


		}

		if ($this->type == "plans")
		{
			if ($this->writeable)
			{
				$this->fundlist =  $this->search->search($this->fundTable,"planid",$this->planid,"funds",$this->searchPath."fundlist.html.twig","name,ticker","fundlist","inline-block","classesclassBundle:Search:search.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
				$this->portfoliolist = $this->search->search($this->portfolioTable,"planid",$this->planid,"portfolios",$this->searchPath."portfoliolist.html.twig","name","portfoliolist","inline-block","classesclassBundle:Search:search.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
			}
			else
			{
				$this->fundlist =  $this->search->search($this->fundTable,"planid",$this->planid,"funds","SharedInvestmentsBundle:Default:search/fundlistreadonly.html.twig","name,ticker","fundlist","inline-block","classesclassBundle:Search:search.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
				$this->portfoliolist = $this->search->search($this->portfolioTable,"planid",$this->planid,"portfolios","SharedInvestmentsBundle:Default:search/portfoliolistreadonly.html.twig","name","portfoliolist","inline-block","classesclassBundle:Search:search.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", "Application Order,Name A-Z, Name Z-A,Newest,Oldest");
			}
		}
                $this->setFundGroups();
	}
        public function setFundGroups()
        {
            $FundsGroupsService = $this->controller->get("FundsGroupsService");
            $FundsGroupsValuesService = $this->controller->get("FundsGroupsValuesService");
            $repository = $this->em->getRepository('classesclassBundle:'.$this->planTable);
            $plan = $repository->findOneBy($this->findplanby);
            $this->fundGroup = $FundsGroupsService->findOneBy(array("id" => $plan->fundGroupid));
            $this->fundsGroups = $FundsGroupsService->getList();
            if ($this->fundGroup != null)
            {
                $this->fundsGroupsValues = $FundsGroupsValuesService->getByGroupId($this->fundGroup->id);
            }
            else
            {
                $this->fundsGroupsValues = null;
            }            
        }

	public function fundreorderAction()
	{
		if ($this->type == "settings")
		{
			$this->fundlist =  $this->search->search($this->fundTable,"userid",$this->userid,"funds",$this->searchPath."reorderfundlist.html.twig","name,ticker","fundlist","none","classesclassBundle:Search:reorderinvestments.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", ",Name A-Z, Name Z-A,Newest,Oldest","1000");
		}
		if ($this->type == "plans")
		{
			$this->fundlist =  $this->search->search($this->fundTable,"planid",$this->planid,"funds",$this->searchPath."reorderfundlist.html.twig","name,ticker","fundlist","none","classesclassBundle:Search:reorderinvestments.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", ",Name A-Z, Name Z-A,Newest,Oldest","1000");
		}

	}

	public function portfolioreorderAction()
	{
		if ($this->type == "settings")
		{
		$this->portfoliolist = $this->search->search($this->portfolioTable,"userid",$this->userid,"portfolios",$this->searchPath."reorderportfoliolist.html.twig","name","portfoliolist","none","classesclassBundle:Search:reorderinvestments.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", ",Name A-Z, Name Z-A,Newest,Oldest","1000");
		}
		if ($this->type == "plans")
		{
		$this->portfoliolist = $this->search->search($this->portfolioTable,"planid",$this->planid,"portfolios",$this->searchPath."reorderportfoliolist.html.twig","name","portfoliolist","none","classesclassBundle:Search:reorderinvestments.html.twig","orderid ASC,name ASC,name DESC,id DESC,id ASC", ",Name A-Z, Name Z-A,Newest,Oldest","1000");
		}

	}

	public function portfoliosetfundsAction($portfolioid)
	{
		$repository = $this->em->getRepository('classesclassBundle:'.$this->fundTable);

		if ($this->type == "settings")
		$findFundsBy = array('userid' => $this->userid);
		else if ($this->type == "plans")
		$findFundsBy = array('userid' => $this->userid,"planid" => $this->planid);
		else
		return 0;

		$funds = $repository->findBy($findFundsBy,array('orderid' => 'ASC'));
		$this->allfunds = [];
		$this->portfoliofunds = [];
		$i = 0;
		$j = 0;
		foreach ($funds as $fund)
		{
			$repository = $this->em->getRepository('classesclassBundle:'.$this->portfolioFundsTable);
			$existancefund = $repository->findOneBy(array('fundid' => $fund->id,'userid'=> $this->userid,'portfolioid' => $portfolioid ));
			if (isset($existancefund))
			{
				$portfoliofundswithoutorder[$j] = $fund;
				$portfoliofundswithoutorder[$j]->percent = $existancefund->percent;
				$portfoliofundswithoutorder[$j]->orderid = $existancefund->orderid;
				$j++;
			}
			else
			$this->allfunds[$i++] = $fund;
		}
		if (isset($portfoliofundswithoutorder))
		for ($i = 0; $i < count($portfoliofundswithoutorder); $i++)
		$portfoliofunds2[$portfoliofundswithoutorder[$i]->orderid] = $portfoliofundswithoutorder[$i];
		$j = 0;
		if (isset($portfoliofunds2))
		for ($i = 0; $j < count($portfoliofunds2); $i++)
		if (isset($portfoliofunds2[$i]))
		$this->portfoliofunds[$j++] = $portfoliofunds2[$i];
	}
	public function reorderInvestments($itemstring,$searchtype,$investmentType)
	{
	if ($investmentType == "fund")
	$investmentTable = $this->fundTable;
	else if ($investmentType == "portfolio")
	$investmentTable = $this->portfolioTable;
	$investmentOrder = $investmentType."Order";
	try
	{
		$items = explode(",",$itemstring);
		$response = "saved";
		$repository = $this->em->getRepository('classesclassBundle:'.$this->planTable);
		$plan = $repository->findOneBy($this->findplanby);
		$plan->$investmentOrder = $searchtype;
		$this->em->getManager()->flush();
		for ($i = 0; $i < count($items); $i++)
		{
		$repository = $this->em->getRepository('classesclassBundle:'.$investmentTable);
		$fund = $repository->findOneBy(array('id' => $items[$i]));
		$fund->orderid = $i;;
		$this->em->getManager()->flush();
	}
	return "saved";
	}
	catch (Exception $e)
	{
		return "Exception occured";
	}

	}

	public function deleteFund($fundid)
	{
	$response = "The selected Fund  is currently being used by a Portfolio, cannot delete unless removed from the Portfolio";
	$repository = $this->em->getRepository('classesclassBundle:'.$this->portfolioFundsTable );
	$fund = $repository->findOneBy(array('userid' => $this->userid,'fundid' => $fundid));
		if (!isset($fund))
		{
		$response = "deleted";
		$repository = $this->em->getRepository('classesclassBundle:'.$this->fundTable);
		$fund = $repository->findOneBy(array('userid' => $this->userid,'id' => $fundid));
		$this->em->getManager()->remove($fund);
		$this->em->getManager()->flush();
		}
	return $response;
	}
	public function deletePortfolio($portfolioid)
	{
		try
		{
			$repository = $this->em->getRepository('classesclassBundle:'.$this->portfolioTable);
			$portfolio= $repository->findOneBy(array('id' => $portfolioid));
			$this->em->getManager()->remove($portfolio);
			$this->em->getManager()->flush();
			$repository = $this->em->getRepository('classesclassBundle:'.$this->portfolioFundsTable);
			$portfolioFunds= $repository->findBy(array('portfolioid' => $portfolioid));
				foreach ($portfolioFunds as $portfolioFund)
				{
				$this->em->getManager()->remove($portfolioFund);
				$this->em->getManager()->flush();
				}
				return "deleted";
		}
		catch (Exception $e)
		{
		return "Exception occured";
		}
	}



	public function saveAndReorderFunds($name,$link,$ticker,$fundid,$groupValueId)
	{
		if ($this->type == "settings")
		{
			$fund = new defaultFunds();
		}
		if ($this->type == "plans")
		{
			$fund = new plansFunds();
			$fund->planid = $this->planid;
		}

		$fund->name = trim($name);
		$fund->link = trim($link);
		$fund->ticker = trim($ticker);
		$fund->fundId = trim($fundid) == '' ? NULL : trim($fundid);
		$fund->userid = $this->userid;
        $fund->groupValueId = $groupValueId;
		$fund->orderid = 0;
		$fund->display = 1;

		$this->em->getManager()->persist($fund);
		$this->em->getManager()->flush();


		$repository = $this->em->getRepository('classesclassBundle:'.$this->planTable);


		$plan = $repository->findOneBy($this->findplanby);
		if ($plan->fundOrder != "")
		{
			$fundorder = explode(" ", $plan->fundOrder);//assume format of field space order ex planid DESC
			$fundorderfield = trim($fundorder[0]);
			$fundordervalue = trim($fundorder[1]);
			$repository = $this->em->getRepository('classesclassBundle:'.$this->fundTable);
			$funds= $repository->findBy($this->findinvestmentby,array($fundorderfield => $fundordervalue));
			$i = 0;
			foreach ($funds as $fund)
			{
				$fund->orderid = $i;
				$this->em->getManager()->flush();
				$i++;
			}
		}
		return "saved";

	}
	public function saveAndReorderPortfolios($name,$customid = "")
	{
		if ($this->type == "settings") 
                {
                    $portfolio = new defaultPortfolios();
                }
		if ($this->type == "plans")
		{
			$portfolio = new plansPortfolios();
			$portfolio->planid = $this->planid;
		}

		$portfolio->name = trim($name);
		$portfolio->userid = $this->userid;
		$portfolio->profile = "ALL";
                $portfolio->customid = trim($customid);


		$this->em->getManager()->persist($portfolio);
		$this->em->getManager()->flush();


		$repository = $this->em->getRepository('classesclassBundle:'.$this->planTable);
		$plan = $repository->findOneBy($this->findplanby);
		if ($plan->portfolioOrder != "")
		{
			$portfolioorder = explode(" ", $plan->portfolioOrder);//assume format of field space order ex planid DESC
			$portfolioorderfield = trim($portfolioorder[0]);
			$portfolioordervalue = trim($portfolioorder[1]);
			$repository = $this->em->getRepository('classesclassBundle:'.$this->portfolioTable);
			$portfolios= $repository->findBy($this->findinvestmentby,array($portfolioorderfield => $portfolioordervalue));
			$i = 0;
			foreach ($portfolios as $portfolio)
			{
				$portfolio->orderid = $i;
				$this->em->getManager()->flush();
				$i++;
			}
		}
		return $portfolio->id;
	}

	public function savePortfolioFunds($portfolioid,$itemstring,$valuestring)
	{
		try
		{
			$items = explode(",", $itemstring);
			$values = explode(",", $valuestring);
			$repository = $this->em->getRepository('classesclassBundle:'.$this->portfolioFundsTable);
			$portfoliofunds= $repository->findBy(array('portfolioid' => $portfolioid));
			foreach ($portfoliofunds as $portfoliofund)
			{
				$this->em->getManager()->remove($portfoliofund);
				$this->em->getManager()->flush();
			}
			for ($i = 0; $i < count($items); $i++)
			{
				if ($this->type == "settings")
				$link = new defaultPortfolioFunds();

				if ($this->type == "plans")
				{
				$link = new plansPortfolioFunds();
				$link->planid = $this->planid;
				}

				$link->userid = $this->userid;
				$link->portfolioid = $portfolioid;
				$link->fundid = $items[$i];
				$link->percent = $values[$i];
				$link->orderid = $i;
				$this->em->getManager()->persist($link);
				$this->em->getManager()->flush();
			}

			return "saved";
		}

		catch (Exception $e)
		{
			return "Exception caught!";
		}

	}
	public function getPortfolio($id)
	{
		$repository = $this->em->getRepository('classesclassBundle:'.$this->portfolioTable);
		$portfolio= $repository->findOneBy(array('id' => $id));
		return $portfolio;
	}
	public function getFundsForPortfolio($portfolioid)
	{
		$repository = $this->em->getRepository('classesclassBundle:'.$this->portfolioFundsTable);
		$portfolioFunds= $repository->findBy(array('portfolioid' => $portfolioid),array("orderid" =>"ASC"));
		$funds = array();
		foreach ($portfolioFunds as $portfolioFund)
		{
			$repository = $this->em->getRepository('classesclassBundle:'.$this->fundTable);
			$fund= $repository->findOneBy(array('id' => $portfolioFund->fundid));	
			if ($fund != null)
			$funds[] = $fund;			
		}
		return $funds;
	}
	public function getFund($id)
	{
		$repository = $this->em->getRepository('classesclassBundle:'.$this->fundTable);
		$fund= $repository->findOneBy(array('id' => $id));
                $this->setFundGroups();
		return $fund;
	}

   	public function __construct($em,$type,$search,$userid,$writeable,$planid = 0,$controller)
	{
		$this->em = $em;
		$this->type = $type;
		$this->search = $search;
		$this->userid = $userid;
		$this->planid = $planid;
		$this->writeable = $writeable;
                $this->controller = $controller;
		if ($type == "settings")
		{
			$this->fundTable = "defaultFunds";
			$this->portfolioTable = "defaultPortfolios";
			$this->portfolioFundsTable = "defaultPortfolioFunds";
			$this->searchPath = "SettingsSettingsBundle:Investments:Search/";
			$this->planTable = "defaultPlan";
			$this->findplanby =array("userid" => $this->userid);
			$this->findinvestmentby = array("userid" => $this->userid);
                        $this->riskBasedFundsTable = "defaultRiskBasedFunds";
                        
		}

		if ($type == "plans")
		{
			$this->fundTable = "plansFunds";
			$this->portfolioTable = "plansPortfolios";
			$this->portfolioFundsTable = "plansPortfolioFunds";
			$this->searchPath = "PlansPlansBundle:Investments:Search/";
			$this->planTable = "plans";
			$this->findplanby =array("id" => $this->planid);
			$this->findinvestmentby = array("planid" => $this->planid);
                        $this->riskBasedFundsTable = "plansRiskBasedFunds";
		}

	}
        
        public function saveGroupId($groupid)
        {
            $repository = $this->em->getRepository('classesclassBundle:'.$this->planTable);
            $plan = $repository->findOneBy($this->findplanby);
            $plan->fundGroupid = $groupid;
            $this->em->getManager()->flush();
        }
        public function addRiskBasedFund($params)
        {
            $class = "classes\\classBundle\\Entity\\".$this->riskBasedFundsTable;
            $riskBasedFund = new $class;
            foreach ($params as $key => $value)
            {
                $riskBasedFund->$key = $value;
            }
            $this->em->getManager()->persist($riskBasedFund);
            $this->em->getManager()->flush();
        }    
        public function editRiskBasedFund($params)
        {
            $repository = $this->em->getRepository('classesclassBundle:'.$this->riskBasedFundsTable );
            $riskBasedFund= $repository->findOneBy(array("id" =>$params['id']));
            foreach ($params as $key => $value)
            {
                $riskBasedFund->$key = $value;
            }
            $this->em->getManager()->flush();
        }     
        public function deleteRiskBasedFund($params)
        {
            $repository = $this->em->getRepository('classesclassBundle:'.$this->riskBasedFundsTable );
            $riskBasedFund= $repository->findOneBy(array("id" =>$params['id']));
            $this->em->getManager()->remove($riskBasedFund);
            $this->em->getManager()->flush();
        }
}
