<?php

namespace Shared\DocumentsBundle\Classes;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;



class documents
{

	public $userid;//current userid
	public $planid;//current planid
	public $search;
	public $options;
	public $table;
	public $findby;
	public $findbyvalue;
	public $documentlist;
	public $templatepath;
	public $templatename;
	public $em;

	public function indexAction()
	{
		$this->options = $this->search->search($this->table,$this->findby,$this->findbyvalue,"documents",$this->templatepath.$this->templatename);
	}

	public function getDocument($id)
	{
		$repository = $this->em->getRepository('classesclassBundle:'.$this->table);
		$document = $repository->findOneBy(array("id" => $id));
		return $document;
	}



   	public function __construct($em,$type,$search,$userid,$writeable,$planid = 0)
	{
		$this->em = $em;
		$this->userid = $userid;
		$this->planid = $planid;
		$this->search = $search;
		$this->writeable = $writeable;
		if ($type == "settings")
		{
			$this->table = "defaultDocuments";
			$this->findby = "a.userid";
			$this->findbyvalues = $this->userid;
			$this->templatepath = "SettingsSettingsBundle:Documents:Search/";
			$this->templatename = "documentlist.html.twig";
		}

		if ($type == "plans")
		{
			$this->table = "plansDocuments";
			$this->findby = "a.planid";
			$this->findbyvalues = $this->planid;
			$this->templatepath = "PlansPlansBundle:Documents:Search/";
			$this->templatename = "documentlist.html.twig";
		}
		if (!$writeable)
		{
		$this->templatepath = "SharedDocumentsBundle:Default:Search/";
		$this->templatename = "documentlistreadonly.html.twig";

		}

	}

}
