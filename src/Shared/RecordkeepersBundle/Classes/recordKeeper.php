<?php
namespace Shared\RecordkeepersBundle\Classes;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansRiskBasedFunds;
use classes\classBundle\Entity\recordkeeperPlans;
use classes\classBundle\Entity\recordkeeperAdviceFunds;
use Shared\RecordkeeperBundle\Classes\advice;
set_time_limit(0);
class recordKeeper
{
    private $em;


    public function test($plans,$portfolios,$riskBasedFunds)
    {

        $plandata = 1;
        $response = "<strong>Plans</strong><br/>";
        if (count($plans) > 0)
        {

            for ($i = 0; $i < count($plans); $i++)
            {
            $response = $response.$plans[$i]['PLANNAM'].'<br/>';
            $response = $response.$plans[$i]['PLANID'].'<br/>';
            $response = $response.$plans[$i]['PLANTYPECD'].'<br/><hr/>';

            }
        }
        else
        {
            $response = $response."plan update failed or no plans existant";
            $plandata = 0;
        }

        if ($plandata)
        {
            $response = $response."<strong>Portfolios</strong><br/>";

            $portfolios_count = count($portfolios);
            for ($i = 0; $i < $portfolios_count; $i++)
            $response = $response.$portfolios[$i].'<br/><hr/>';
            if ($portfolios_count == 0)
            $response = $response."call failed or no portfolios";

            $response = $response."<hr/><strong>Risk Based Funds</strong><br/>";


            $count = count($riskBasedFunds);
            for ($i = 0; $i < $count; $i++)
            $response = $response.$riskBasedFunds[$i].'<br/>';
            if ($count == 0)
            $response = $response."call failed or no risk based funds";
        }
        return $response;


    }
    public function adviceCall($xml,$planid,$userid) {
            $headers = array('Authorization:Smart401k=E0BBCAD4-42DD-45FD-8232-B1266C3C4DD2', 'Content-Type: text/xml');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://www2.smart401k.com/vWise/RequestAdviceService.ashx");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
            $this->writeXmlLog("none", "none", "none",$xml,$planid,$userid,'send');

            $output = curl_exec($ch);
            $info = curl_getinfo($ch);

            if($info['http_code'] != 200) {
                $output = curl_error($ch);
                $error = true;
            }

            $this->writeXmlLog("none", "none", "none",$output,$planid,$userid,'receive');
            curl_close($ch);

            if (isset($error)) {
                $output = false;
            }
            return $output;

    }

    public function writeXmlLog($profileUid, $xmlUid, $profileId, $xml,$planid,$userid,$action,$service = "advice",$connection = "admin", $application = null) {
        $conn = $this->em->getConnection();
        $sql = "insert into xmlRaw (profileUid, xmlUid, profileId, userid, planid, connection, action, data, service, application) values (:profileUid, :xmlUid, :profileId, :userid, :planid, :connection, :action, " . "AES_ENCRYPT(:data, UNHEX('5E6D217336'))" . ", :service, :application)";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("profileUid", $profileUid);
        $stmt->bindValue("xmlUid", $xmlUid);
        $stmt->bindValue("profileId", $profileId);
        $stmt->bindValue("userid", $userid);
        $stmt->bindValue("planid", $planid);
        $stmt->bindValue("connection", strtolower($connection));
        $stmt->bindValue("action", $action);
        $stmt->bindValue("data", $xml);
        $stmt->bindValue("service", $service);
        $stmt->bindValue("application", $application);
        $stmt->execute();
    }

    public function turnOffAdviceXML($planid)
    {
        $sendXML1='<?xml version="1.0"?>
        <SmartPlan>
            <Type>1</Type>
            <Action>2</Action>
            <Plan>
                <SmartPlanId>'.$planid.'</SmartPlanId>
            </Plan>
</SmartPlan>';
        return $sendXML1;

    }

    public function turnOnAdvice($planid,$userid,$adviceFundIdString)
    {

        $repository = $this->em->getRepository('classesclassBundle:plans');
        $currentplan= $repository->findOneBy(array('id' => $planid));
        $repository = $this->em->getRepository('classesclassBundle:accountsUsers');
        $currentuser= $repository->findOneBy(array('userid' => $userid));
        $repository = $this->em->getRepository('classesclassBundle:accounts');
        $currentaccount= $repository->findOneBy(array('id' => $userid));

        $repository = $this->em->getRepository('classesclassBundle:recordkeeperAdviceFunds');
        $currentfunds = $repository->findBy(array('planid' => $planid));
        foreach ($currentfunds as $fund)
        {
            $fund->status = 0;
            $this->em->getManager()->flush();
        }


        $sendXML1 = '<?xml version="1.0"?>
    <SmartPlan>
        <Type>1</Type>
        <Action>1</Action>
        <Plan>
            <SmartPlanId>'.$currentplan->id.'</SmartPlanId>
            <Employer>'.$currentplan->name.'</Employer><RecordKeeper>'.$currentaccount->connectionType.'</RecordKeeper>
            <Contact>
                    <FirstName>'.$currentuser->firstname.'</FirstName>
                                            <LastName>'.$currentuser->lastname.'</LastName>
                                            <Address1>'.$currentuser->address.'</Address1>
                                            <Address2>'.$currentuser->address2.'</Address2>
                                            <City>'.$currentuser->city.'</City>
                                            <State>'.$currentuser->state.'</State>
                                            <Zip>'.$currentuser->zip.'</Zip>
                                            <Email>'.$currentuser->getEmail().'</Email>
                        <Phone>'.$currentuser->phone.'</Phone>
          </Contact>


            ';

                $availableFunds = array();
                $availableFundsString = "<AvailableFunds>";
                $i = 0;
                $adviceFundIdArray = explode(",",$adviceFundIdString);
                        foreach ($adviceFundIdArray as $fundid)
                        {
                            $repository = $this->em->getRepository('classesclassBundle:recordkeeperAdviceFunds');
                            $recordkeeperAdviceFund = $repository->findOneBy(array('id' => $fundid,"planid" => $planid));
                            $recordkeeperAdviceFund->status = 1;
                            $this->em->getManager()->flush();
                            $availableFunds[$i] = new \stdClass();
                            $availableFunds[$i]->name = $recordkeeperAdviceFund->name;
                            $availableFunds[$i]->ticker = $recordkeeperAdviceFund->ticker;
                            $availableFunds[$i]->cusip = $recordkeeperAdviceFund->cusip;
                            $i++;

                        }

                        foreach ($availableFunds as $fund)
                        {
                            $availableFundsString = $availableFundsString.'
                            <Fund>
                            <Ticker>'.$fund->ticker.'</Ticker>
                            <Cusip>'.$fund->cusip.'</Cusip>
                            <Name>'.$fund->name.'</Name>
                            </Fund>';
                        }
                    $availableFundsString = $availableFundsString."</AvailableFunds>";

            $sendXML1 = $sendXML1.$availableFundsString."</Plan>
</SmartPlan>";
            $sendXML1 = str_replace("&","&amp;",$sendXML1);

            echo $sendXML1.'<br/>';

            $repository = $this->em->getRepository('classesclassBundle:plans');
            $currentplan = $repository->findOneBy(array("id" => $planid));
            $currentplan->adviceDate = new \DateTime("now");
            $currentplan->adviceStatus = 2;
            $currentplan->advice = "";
            $currentplan->adviceCompare = "";

            $this->em->getManager()->flush();

            return $this->adviceCall($sendXML1,$planid,$userid);
    }
    public function toggleAdvice($adviceList,$userid,$planid)
    {
        $repository = $this->em->getRepository('classesclassBundle:recordkeeperAdviceFunds');
        $fundsDeleted= $repository->findBy(array('planid' => $planid));

        $repository = $this->em->getRepository('classesclassBundle:plans');
        $currentplan= $repository->findOneBy(array('id' => $planid));


        foreach ($fundsDeleted as $fund)
        {
            $this->em->getManager()->remove($fund);
            $this->em->getManager()->flush();

        }
        if ($currentplan->adviceStatus == 0)
        {
            foreach ($adviceList as $fund)
            {


                $newAdviceFund = new recordkeeperAdviceFunds();
                $newAdviceFund->userid = $userid;
                $newAdviceFund->planid = $planid;
                $newAdviceFund->name = $fund->name;
                $newAdviceFund->ticker = $fund->ticker;
                $newAdviceFund->cusip = $fund->cusip;

                if ($newAdviceFund->name == null)
                $newAdviceFund->name = "";
                if ($newAdviceFund->ticker == null)
                $newAdviceFund->ticker = "";
                if ($newAdviceFund->cusip  == null)
                $newAdviceFund->cusip  = "";

                $repository = $this->em->getRepository('classesclassBundle:recordkeeperAdviceFunds');
                $searchFund =  $repository->findOneBy(array('planid' => $planid,"name" => $fund->name, "ticker" => $fund->ticker));


                if ($searchFund == null)
                {
                    $this->em->getManager()->persist($newAdviceFund);
                    $this->em->getManager()->flush();
                }
            }
            $currentplan->adviceStatus = 2;
            $currentplan->advice = "";
            $currentplan->adviceCompare = "";
            $this->em->getManager()->flush();
        }
        else
        {
            $currentplan->adviceStatus = 0;
            $currentplan->advice = "";
            $currentplan->adviceCompare = "";
            $this->em->getManager()->flush();
            $turnoffadviceXML =    $this->turnOffAdviceXML($planid);
            echo $this->adviceCall($turnoffadviceXML,$planid,$userid);

        }


    }
    public function updateplans($plans,$userid)
    {

        if (count($plans) > 0)
        {
            $response = "Updated Plans";
            $repository = $this->em->getRepository('classesclassBundle:recordkeeperPlans');
            $plans_deleted = $repository->findBy(array('userid' => $userid));
            $i = 0;
            foreach ($plans_deleted as &$plan)
            {
                $this->em->getManager()->remove($plan);
                if ($i % 100 == 0)
                $this->em->getManager()->flush();
                $i++;
            }
            $planCount = count($plans);
            for ($i = 0; $i < $planCount; $i++)
            {
                $recordkeeperPlan = new recordkeeperPlans();
                $recordkeeperPlan->name = $plans[$i]['PLANNAM'];
                $recordkeeperPlan->planid = $plans[$i]['PLANID'];
                $recordkeeperPlan->plantypecd = $plans[$i]['PLANTYPECD'];
                $recordkeeperPlan->userid = $userid;
                $this->em->getManager()->persist($recordkeeperPlan);
                if ($i % 100 == 0)
                $this->em->getManager()->flush();
            }
            $this->em->getManager()->flush();
        }
        else
        $response = "plan update failed or no plans existant";

        return $response;

    }
    public function updateplan($userid,$findidby,$findidbyvalue,$smartplanid, $spe2 = 1, $initialplan = null)
    {
        $success = false;
        try {
            $repository = $this->em->getRepository('classesclassBundle:recordkeeperPlans');
            $selectedPlan = $repository->findOneBy(array($findidby => $findidbyvalue, 'userid' => $userid));
            $repository = $this->em->getRepository('classesclassBundle:plans');
            $currentPlan = $repository->findOneBy(array('id' => $smartplanid));
            $currentPlan->name = str_replace("&amp;", "&", $selectedPlan->name);

            $currentPlan->activePlanid = $selectedPlan->planid;
            $currentPlan->activePlantypecd = $selectedPlan->plantypecd;

            $planid = $findidbyvalue;

            $repository = $this->em->getRepository('classesclassBundle:plans');
            $findPlan = $repository->findBy(array('planid' => $planid, 'userid' => $userid, 'deleted' => 0));
            $findCurrentPlan = $repository->findBy(array('planid' => $planid, 'userid' => $userid, 'id' => $smartplanid));

            if ($initialplan !== null) {
                $success = $this->updateInitialPlan($currentPlan, $initialplan);
            }
            if (count($findPlan) > 0 && count($findCurrentPlan) < 1) {
                if ($success) {
                    return new Response('partial');
                }
                return new Response("false");
            }
          
            $currentPlan->planid = $planid;
            $currentPlan->appVersion = $spe2;
            $this->em->getManager()->flush();
            return  new Response("true");
        }
        catch (Exception $e)
        {

            return new Response("exception");
        }

    }

    public function updateInitialPlan($currentPlan, $initialplan) {
        if ($currentPlan->initialPlan != $initialplan) {
            $currentPlan->initialPlan = $initialplan;
        }
        else {
            return false;
        }
        $this->em->getManager()->persist($currentPlan);
        $this->em->getManager()->flush();
        return true;
    }

    public function updatePortfolios($portfolios,$userid,$planid)
    {
        $response = "saved";
            try
            {
                $portfolios_count = count($portfolios);
                if ($portfolios_count > 0)
                {
                    $this->removeOldInvestments($planid);

                    foreach($portfolios as $portfolio)
                    {
                        $newportfolio = new plansPortfolios();
                        $newportfolio->userid = $userid;
                        $newportfolio->name = $portfolio;
                        $newportfolio->planid = $planid;
                        $newportfolio->profile = "ALL";
                        $this->em->getManager()->persist($newportfolio);
                        $this->em->getManager()->flush();

                    }

                }

                else
                {
                    $response = "no portfolios";
                }
            }

            catch (Exception $e)
            {
                $response = "Error occured!";
            }

        return($response);

    }
    public function updateRiskBasedFunds($riskBasedFunds,$userid,$planid)
    {

        $response = "saved";
        try
        {

            $riskBasedFunds_count = count($riskBasedFunds);
            if ($riskBasedFunds_count > 0)
            {
                $this->removeOldInvestments($planid);

                foreach ($riskBasedFunds as $riskBasedFund)
                {
                    $newriskBasedFund = new plansRiskBasedFunds();
                    $newriskBasedFund ->userid = $userid;
                    $newriskBasedFund ->name = $riskBasedFund;
                    $newriskBasedFund ->planid = $planid;
                    $newriskBasedFund ->profile = "ALL";
                    $this->em->getManager()->persist($newriskBasedFund);
                    $this->em->getManager()->flush();

                }

            }

            else
            {
                $response = "no Risk Based Funds";
            }
        }

        catch (Exception $e)
        {
            $response = "Error occured!";
        }

    return ($response);



    }

    public function removeOldInvestments($planid)
    {
    $this->removeOldPortfolios($planid);
    $this->removeOldRiskBasedFunds($planid);
    }
    public function removeOldPortfolios($planid)
    {
        $repository = $this->em->getRepository('classesclassBundle:plansPortfolios');
        $portfolios_deleted = $repository->findBy(array('planid' => $planid));

        foreach ($portfolios_deleted as $portfolio)
        {
            $this->em->getManager()->remove($portfolio);
            $this->em->getManager()->flush();
        }

    }
    public function removeOldRiskBasedFunds($planid)
    {
        $repository = $this->em->getRepository('classesclassBundle:plansRiskBasedFunds');
        $riskBasedFunds_deleted = $repository->findBy(array('planid' => $planid));

        foreach ($riskBasedFunds_deleted as $riskBasedFund)
        {
            $this->em->getManager()->remove($riskBasedFund);
            $this->em->getManager()->flush();
        }

    }

    public    function updateRiskType($userid,$planid,$riskType)
    {
    $repository = $this->em->getRepository('classesclassBundle:plans');
    $findPlan = $repository->findOneBy(array('id' => $planid,'userid' => $userid));
    $findPlan->riskType = $riskType;
    $this->em->getManager()->flush();
    }

    public function __construct($manager)
    {
    $this->em = $manager;

    }
    public function investmentString($investments)
    {
        $querystring = "";
        foreach ($investments as $investment)
        {
            $querystring = $querystring.$investment."*seperator*";
        }
        return $querystring;
    }

    public function updatePlanIds($userid,$connection,$planid = "planid")
    {
        $querystring = "UPDATE plans,recordkeeperPlans SET plans.activePlanid = recordkeeperPlans.planid,plans.activePlantypecd = recordkeeperPlans.plantypecd,plans.name = recordkeeperPlans.name WHERE plans.userid = ".$userid." AND recordkeeperPlans.userid = plans.userid AND plans.planid = recordkeeperPlans.".$planid." AND plans.activePlanid = '' and plans.activePlantypecd = '' and recordkeeperPlans.name != ''";
        $connection->executeQuery($querystring);
    }

}