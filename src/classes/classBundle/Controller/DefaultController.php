<?php

namespace classes\classBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use classes\classBundle\Entity\accounts;
use classes\classBundle\Entity\accountsUsers;
use classes\classBundle\Entity\defaultFunds;
use classes\classBundle\Entity\defaultPortfolios;
use classes\classBundle\Entity\defaultPortfolioFunds;
use classes\classBundle\Entity\plansFunds;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansPortfolioFunds;
use classes\classBundle\Entity\defaultDocuments;
use classes\classBundle\Entity\plansRiskBasedFunds;
use classes\classBundle\Entity\plansDocuments;
use classes\classBundle\Entity\portalnews;
use classes\classBundle\Entity\managePromoDocsCategories;
use classes\classBundle\Entity\managePromoDocsSections;
use classes\classBundle\Entity\managePromoDocs;
use classes\classBundle\Entity\manageUsers;
use classes\classBundle\Entity\APIAccounts;
use Symfony\Component\HttpFoundation\Session\Session;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\plansLibraryUsers;
use classes\classBundle\Entity\DefaultLibraryUsers;

error_reporting(0);

class DefaultController extends Controller
{

    public $searchQuery;
    public $searchQueryCount;

    public function updateAction()//generic update method
    {
        $trimArray = [
            'defaultPlan' => ['email', 'errorNotificationEmail', 'profileNotificationEmail', 'advisorEmail'],
            'plans' => ['email', 'errorNotificationEmail', 'profileNotificationEmail', 'advisorEmail']
        ];
        try
        {
            $request = Request::createFromGlobals();
            // the URI being requested (e.g. /about) minus any query parameters
            $request->getPathInfo();
            $table = $request->request->get('table', '');
            $findby = $request->request->get('findby', '');

            $arr = $request->request->all();
            $findbyid = $request->request->get($findby);

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('classesclassBundle:' . $table);
            //find if user name and password exists in members table
            $theuser = $repository->findOneBy(array($findby => $findbyid));
            while (list($key, $value) = each($arr))
            {
                if ($key != "table" && $key != "findby")
                {
                    $valid = true;
                    if ($table == "accountsUsers" && $key == "email")//exception 1 - users email 
                    {
                        $checkForExistingEmail = $repository->findByEmail($value);
                        if ($checkForExistingEmail != null) $valid = false;
                        else
                        {
                            $theuser->setEmail($arr['email']);
                            $theuser->setUsername($arr['email']);
                        }
                    }
                    else if ($valid) {
                        if ($value == '') {
                            $theuser->$key = NULL;
                        } else {
                            if (isset($trimArray[$table]) && in_array($key, (array) $trimArray[$table], true)) {
                                $value = trim($value);
                            } 
                            $theuser->$key = $value; // if valid copy
                        }

                    }
                }
            }
            $em->flush();

            //return $this->redirect($this->generateUrl('_account_index'));
            return new Response("saved");
        }
        catch (Exception $e)
        {
            return new Response("Error occured!: " . $e->getMessage());
        }
    }

    public function addAction()//almost generic add method
    {

        $request = Request::createFromGlobals();
        // the URI being requested (e.g. /about) minus any query parameters
        $request->getPathInfo();
        $table = $request->request->get('table', '');
        $em = $this->getDoctrine()->getManager();

        $arr = $request->request->all();


        if ($table == "advisers") $item = new advisers();

        if ($table == "defaultFunds") $item = new defaultFunds();

        if ($table == "defaultPortfolios") $item = new defaultPortfolios();

        if ($table == "defaultDocuments")
        {
            $item = new defaultDocuments();
            $item->dateAdded = new \DateTime("NOW");
        }

        if ($table == "plansFunds") $item = new plansFunds();

        if ($table == "plansRiskBasedFunds") $item = new plansRiskBasedFunds();

        if ($table == "plansDocuments")
        {
            $item = new plansDocuments();
            $item->dateAdded = new \DateTime("NOW");
        }

        if ($table == "APIAccounts") $item = new APIAccounts();

        /*
          if ($table == "members")
          {
          $item = new members();
          $item->subscribedate = new \DateTime("now");
          }
         */

        if ($table == "portalnews") $item = new portalnews();

        if ($table == "managePromoDocsCategories") $item = new managePromoDocsCategories();

        if ($table == "managePromoDocsSections") $item = new managePromoDocsSections();

        if ($table == "managePromoDocs") $item = new managePromoDocs();

        if ($table == "manageUsers") $item = new manageUsers();

        if ($table == "accountsUsers")
        {
            $item = new accountsUsers();
            $repository = $em->getRepository('classesclassBundle:accounts');
            $item->account = $repository->findOneBy(array("id" => $arr['userid']));
            if (isset($arr['email']))
            {
                $item->setEmail($arr['email']);
                $item->setUsername($arr['email']);
                $item->setEnabled(1);
            }
            if (isset($arr['password'])) $item->setPlainPassword($arr['password']);
        }
        while (list($key, $value) = each($arr))
        {

            if ($key != "table" && $key != "findby" && (($table == "accountsUsers" && $key != "email" && $key != "password" && $key != "enabled") || ($table != "accountsUsers") )) $item->$key = $value;
        }

        $em->persist($item);
        $em->flush();

        return new Response($item->id);
    }

    public function deleteAction()//generic delete method
    {

        try
        {
            $request = Request::createFromGlobals();
            // the URI being requested (e.g. /about) minus any query parameters
            $request->getPathInfo();
            $table = $request->request->get('table', '');
            $findby = $request->request->get('findby', '');

            $findbyid = $request->request->get($findby);

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('classesclassBundle:' . $table);

            $theuser = $repository->findOneBy(array($findby => $findbyid));
            if ($theuser != null)
            {
                $deletedid = $theuser->id;
                $em->remove($theuser);
                $em->flush();

                return new Response('object deleted id ' . $deletedid);
            }
            else
            {
                return new Response("Item does not exist");
            }
        }
        catch (Exception $e)
        {
            return new Response("Error occured!: " . $e->getMessage());
        }
    }

    public function refreshTableAction()
    {
        try {
            $videosRepo = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');
            $videos = $videosRepo->findAll();
            $videosMap = array();
            foreach($videos as $item) {
                $videosMap[$item->name] = $item->id;
            }

            $request = Request::createFromGlobals();
            // the URI being requested (e.g. /about) minus any query parameters
            $request->getPathInfo();
            $table = $request->request->get('table', '');
            $findby = $request->request->get('findby', '');
            $arr = $request->request->all();
            $findbyid = $request->request->get($findby);

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('classesclassBundle:' . $table);

            $toDelete = $repository->findBy(array($findby => $findbyid));
            foreach($toDelete as $item) {
                $em->remove($item);
            }
            $em->flush();

            while (list($key, $value) = each($arr))
            {
                if ($key != "table" && $key != "findby" && $value && $videosMap[$key])
                {
                    $fullclass = 'classes\\classBundle\\Entity\\'.$table;
                    $user = new $fullclass;

                    $user->$findby = $findbyid;
                    $user->videoid = $videosMap[$key];
                    $em->persist($user);
                }
            }
            $em->flush();

            return new Response('Table ' . $findbyid . ' refreshed.');

        } catch (Exception $e) {
            return new Response("Error occured!: " . $e->getMessage());
        }
    }


    function proper_escape($datastring)
    {
        # Strip slashes if data has already been escaped by magic quotes
        if (get_magic_quotes_gpc()):
            $datastring = stripslashes($datastring);
        endif;

        return $datastring;
    }

    public function filterprofilevar(&$variable)
    {
        foreach ($variable as &$value)
        {
            $value = str_replace("%20", "", $value);
            $value = str_replace("\n", "", $value);
            $value = str_replace(",", "¸", $value);
        }
    }

    public function searchCsvAction(Request $request)
    {
        $pagenumber = 1;
        $pagetype = $request->request->get('csvtype', "");
        $this->searchDatatablesVariable($request);
        $items = $this->items;
        foreach ($items as &$item)
        {
            foreach ($item as &$value)
            {
                $value = str_replace(",", "", $value);
            }
        }


        $profileClass = $this->get('profiles.service');
        if ($pagetype == "participantSelections")
        {
            header("Cache-Control: must-revalidate");
            header("Pragma: must-revalidate");
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename=participant-selections.csv');
            header("Content-type: application/vnd.ms-excel");
            header('Content-Transfer-Encoding: binary');
            $csvstring = 'Plan name during creation,Plan Id,Name - First,Name - Last,Date generated,Current Yearly Income,Current Balance,Pre-Tax per paycheck,Catch Up Contributions,After-Tax (Roth) per paycheck,Post-Tax per paycheck,Recommended Monthly Plan Contribution,contributionCurrent,rothContributionCurrent,Catchup Contributions Mode, Contributions Mode, Investment Type,Portfolio Name, Other Assets';
            $output = fopen('php://output', 'w');
            fputcsv($output,explode(",",$csvstring));
            foreach ($items as &$item)
            {
                $csvrow = 
                [
                    $item['planName'], $item['planid'],$item['participants_firstName'],$item['participants_lastName'],
                    $item['profiles_reportDate'] ,  $item['profilesContributions_currentYearlyIncome'],  $item['profiles_currentBalance'],
                    $item['profilesContributions_pre'],$item['profilesContributions_catchup'],$item['profilesContributions_roth'],
                    $item['profilesContributions_post'],$item['profilesRetirementNeeds_recommendedMonthlyPlanContribution'],
                    $item['profilesContributions_preCurrent'],$item['profilesContributions_rothCurrent'],$item['cmode'],$item['mode'],
                    $item['profilesInvestments_type'],$item['profilesInvestments_pname'],$item['profilesRetirementNeeds_otherAssets']                   
                ];    
                fputcsv($output, $csvrow);
            }                 
            fclose($output);
        }
        if ($pagetype == "participantEmails")
        {
            header("Cache-Control: must-revalidate");
            header("Pragma: must-revalidate");
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename=participant-emails.csv');
            header("Content-type: application/vnd.ms-excel");
            header('Content-Transfer-Encoding: binary');
            $csvstring = 'id,Planid,Email,Report Date,Employee ID';
            $output = fopen('php://output', 'w');
            fputcsv($output,explode(",",$csvstring));
            foreach ($items as &$item)
            {
                $csvrow =
                [
                    $item['profiles_id'],$item['planid'],$item['participants_email'],$item['profiles_reportDate'],$item['participants_employeeId']
                ];
                fputcsv($output, $csvrow);
            }
            fclose($output);
        }
        if ($pagetype == "participantBeneficiaries")
        {
            header("Cache-Control: must-revalidate");
            header("Pragma: must-revalidate");
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename=participant-beneficiaries.csv');
            header("Content-type: application/vnd.ms-excel");
            header('Content-Transfer-Encoding: binary');
            $csvstring = 'Plan name during creation,Plan Id,Participant Name - First,Participant Name - Last,Date generated, Beneficiary Type, Beneficiary First Name, Beneficiary Last Name, Beneficiary Gender, Beneficiary Relationship, Beneficiary Martial Status, Beneficiary Social Security Number, Beneficiary Percent';
            $output = fopen('php://output', 'w');
            fputcsv($output,explode(",",$csvstring));
            foreach ($items as &$item)
            {
                $profile = array();

                $profile = $profileClass->returnProfile($item["profiles_id"]);

                foreach ($profile['beneficiaries'] as $beneficiary)
                {
                    $csvrow = 
                    [
                        $profile['planName'],$profile['planid'],$profile['participant']['participants_firstName'],
                        $profile['participant']['participants_lastName'],$profile['reportDate'],$beneficiary['profilesBeneficiaries_type'],
                        $beneficiary['profilesBeneficiaries_firstName'],$beneficiary['profilesBeneficiaries_lastName'],$beneficiary['profilesBeneficiaries_gender'],
                        $beneficiary['profilesBeneficiaries_relationship'],$beneficiary['profilesBeneficiaries_maritalStatus'],
                        $beneficiary['profilesBeneficiaries_ssn'],$beneficiary['profilesBeneficiaries_percent']
                    ];
                    fputcsv($output, $csvrow);
                }
            }
        }
        exit;
        return new Response("");
    }

    public function searchDatatablesVariable(Request $requestObj)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $requestObj->request->all();
        $table = $request['table'];
        $start = $request["start"];
        $maxresults = $request["length"];
        $searchstring = "";
        $types = explode(",", $request['types']);
        $template = $request['template'];
        $findby = explode(",", $request['findby']);
        $findbyvalues = explode(",", $request['findbyvalues']);
        $customquery = $request['customquery'];
        $wherecounter = 0;
        $controller = $request['controller'];
        $session = $requestObj->getSession();
        $clientid = (int) $session->get("clientid");
        $repository = $this->getDoctrine()->getRepository(accountsUsers::class);
        $user = $repository->find($clientid);
        if ($customquery != "profileList")
        {
            if ($request['findby'] != "") if (count($findby) == count($findbyvalues))
                {
                    $searchstring = $searchstring . " ( ";
                    for ($i = 0; $i < count($findby); $i++)
                    {
                        if ($i > 0)
                        {
                            if (substr_count($findby[$i], "^") > 0) $searchstring = $searchstring . " AND ";
                            if (substr_count($findby[$i], "|") > 0) $searchstring = $searchstring . " OR ";
                            if (substr_count($findby[$i], "!") > 0)
                            {

                                $searchstring = $searchstring . " AND NOT ";
                            }
                            $findby[$i] = substr($findby[$i], 1, strlen($findby[$i]));
                        }
                        else if (substr_count($findby[$i], "!") > 0)
                        {
                            $searchstring = $searchstring . " NOT ";
                            $findby[$i] = substr($findby[$i], 1, strlen($findby[$i]));
                        }
                        $findbyvaluesPrint = "";
                        if ($findbyvalues[$i]{0} != "'")
                        $findbyvaluesPrint = "'";
                        $findbyvaluesPrint = $findbyvaluesPrint.$findbyvalues[$i];
                        if (!isset($findbyvalues[$i]{strlen($findbyvalues[$i])-2}) || $findbyvalues[$i]{strlen($findbyvalues[$i])-2} != "'")
                        $findbyvaluesPrint = $findbyvaluesPrint."'";
                        $searchstring = $searchstring . $findby[$i] . " = " . $findbyvaluesPrint;
                    }

                    $searchstring = $searchstring . " ) ";
                }
            if ($customquery == "videoLibStats")
            {

                if ($searchstring != "") $searchstring = $searchstring . " AND ";
                $searchstring = $searchstring . '  a.widgetKey in (' . $request['keys'] . ') ';
                if ($request['startDate'] != null && $request['startDate'] != null) $searchstring = $searchstring . ' AND a.date >= :videoLibrarydate and a.date <= :videoLibraryenddate ';
                else unset($customquery);
            }
            if ($searchstring != "" && $wherecounter == 0)
            {
                $searchstring = "WHERE " . $searchstring;
                $wherecounter++;
            }
            if ($customquery == "adviserPlanList")
            {
                $queryCountString = 'SELECT COUNT(a) FROM classesclassBundle:' . $table . ' a
	            JOIN a.plan b  ' . $searchstring;
            }
            else if ($customquery == "accountlist")
            {


                $queryCountString = 'SELECT COUNT(DISTINCT a) FROM classesclassBundle:' . $table . ' a LEFT
	            JOIN a.accountsUsersEnabledAccounts b LEFT JOIN a.accountsUsers c ' . $searchstring;
            }
            else if ($customquery == "accountlistfull")
            {


                $queryCountString = 'SELECT COUNT(DISTINCT a) FROM classesclassBundle:' . $table . ' a
	            LEFT JOIN a.accountsUsers b  ' . $searchstring;
            }
            else if ($customquery == "userlist")
            {
                $searchstring = $searchstring . " AND a.id != " . $clientid;
                $queryCountString = 'SELECT COUNT(a)
				FROM classesclassBundle:' . $table . ' a ' . $searchstring;
            }
            else
            {
                $queryCountString = 'SELECT COUNT(a)
				FROM classesclassBundle:' . $table . ' a ' . $searchstring;
            }


            if (count($types) > 0)
            {
                if (isset($request['search']) && $request['search']['value'] != '')
                {
                    if (trim($searchstring) != "") $searchstring = $searchstring . " AND ";

                    $searchstring = $searchstring . " ( ";

                    $searchcounter = 0;
                    foreach ($types as $type)
                    {
                        if ($searchcounter != 0) $searchstring = $searchstring . " OR ";

                        $searchstring = $searchstring . " " . $type . "  LIKE :searchvalue";

                        $searchcounter++;
                    }

                    $searchstring = $searchstring . " ) ";
                }
            }

            //echo $searchstring;


            if ($searchstring != "" && $wherecounter == 0)
            {
                $searchstring = "WHERE " . $searchstring;
                $wherecounter++;
            }

            if (isset($request['order']) && count($request['order']))
            {

                $orderby = $this->order($request, $types);
                //$searchstring = $searchstring." ".$orderby;
            }
            if ($customquery == "adviserPlanList")
            {
                $queryFilteredCountString = 'SELECT COUNT(a) FROM classesclassBundle:' . $table . ' a
	            JOIN a.plan b ' . $searchstring . ' ' . $orderby;

                $queryString = 'SELECT a, b FROM classesclassBundle:' . $table . ' a
	            JOIN a.plan b   ' . $searchstring . ' ' . $orderby;
            }
            else if ($customquery == "accountlist")
            {
                if (isset($request['search']) && $request['search']['value'] != '')
                {
                    $searchstring = str_replace("c.firstname  LIKE :searchvalue", "( (c.firstname  LIKE :searchvalue OR c.lastname  LIKE :searchvalue) AND c.roleType = 'admin_master'  )", $searchstring);
                }
                if ($orderby == "ORDER BY c.firstname ASC") $orderby = "ORDER BY c.firstname ASC, c.lastname ASC";
                if ($orderby == "ORDER BY c.firstname DESC") $orderby = "ORDER BY c.firstname DESC, c.lastname DESC";

                $queryFilteredCountString = 'SELECT COUNT(DISTINCT a)
				FROM classesclassBundle:' . $table . ' a LEFT JOIN a.accountsUsersEnabledAccounts b LEFT JOIN a.accountsUsers c ' . $searchstring . ' ' . $orderby;
                $queryString = 'SELECT a,b
				FROM classesclassBundle:' . $table . ' a LEFT JOIN a.accountsUsersEnabledAccounts b LEFT JOIN a.accountsUsers c ' . $searchstring . ' GROUP BY a.id ' . $orderby;
            }
            else if ($customquery == "accountlistfull")
            {
                if (isset($request['search']) && $request['search']['value'] != '')
                {
                    $searchstring = str_replace("b.firstname  LIKE :searchvalue", "( (b.firstname  LIKE :searchvalue OR b.lastname  LIKE :searchvalue) AND b.roleType = 'admin_master'  )", $searchstring);
                }
                if ($orderby == "ORDER BY b.firstname ASC") $orderby = "ORDER BY b.firstname ASC, b.lastname ASC";
                if ($orderby == "ORDER BY b.firstname DESC") $orderby = "ORDER BY b.firstname DESC, b.lastname DESC";


                $queryFilteredCountString = 'SELECT COUNT(DISTINCT a)
				FROM classesclassBundle:' . $table . ' a LEFT JOIN a.accountsUsers b ' . $searchstring . ' ' . $orderby;
                $queryString = 'SELECT a,b
				FROM classesclassBundle:' . $table . ' a LEFT JOIN a.accountsUsers b ' . $searchstring . ' GROUP BY a.id ' . $orderby;
            }
            else
            {

                $queryFilteredCountString = 'SELECT COUNT(a)
				FROM classesclassBundle:' . $table . ' a ' . $searchstring . ' ' . $orderby;
                $queryString = 'SELECT a
				FROM classesclassBundle:' . $table . ' a ' . $searchstring . ' ' . $orderby;
                //if ($searchstring != " ORDER BY a.id DESC")
                //echo $queryString;
            }

            $query = $em->createQuery
                    (
                    $queryCountString
            );

            if ($customquery == "videoLibStats")
            {
                $query->setParameter('videoLibrarydate', $request['startDate']);
                $query->setParameter('videoLibraryenddate', $request['endDate']);
            }


            $recordsTotal = $query->getSingleScalarResult();

            $query = $em->createQuery
                    (
                    $queryFilteredCountString
            );

            if (isset($request['search']) && $request['search']['value'] != '') $query->setParameter('searchvalue', '%' . $request['search']['value'] . '%');
            if ($customquery == "videoLibStats")
            {
                $query->setParameter('videoLibrarydate', $request['startDate']);
                $query->setParameter('videoLibraryenddate', $request['endDate']);
            }
            $recordsFiltered = $query->getSingleScalarResult();


            $query = $em->createQuery
                    (
                    $queryString
            );
            //echo $queryString;
            if (isset($request['search']) && $request['search']['value'] != '') $query->setParameter('searchvalue', '%' . $request['search']['value'] . '%');
            if ($customquery == "videoLibStats")
            {
                $query->setParameter('videoLibrarydate', $request['startDate']);
                $query->setParameter('videoLibraryenddate', $request['endDate']);
            }

            $query->setMaxResults($maxresults);
            $query->setFirstResult($start);
            //if ($start != 0)
            //echo $start;
            $this->items = $query->getResult();
        }
        if ($customquery == "profileList")
        {

            $recordsTotal = $maxresults;
            $recordsFiltered = $maxresults;
            $items = array();
            $connection = $this->get('doctrine.dbal.default_connection');
            $profiles = $this->get('profiles.service');






            $orderby = $this->order($request, $types);
            $orderby = str_replace(".", "_", $orderby);
            $baseString = "SELECT  " . $profiles->generateQueryFields("profiles").', plans.planid as planid ';

            $baseStringCount = "SELECT  COUNT(DISTINCT(profiles.id)) AS total ";
            $joinString = "";
            $joinTablesCounter = 0;
            $joinedTables = array();

            $this->addJoinedTable($joinedTables, "participants", "profiles.participantid", "participants.id");
            $this->addJoinedTable($joinedTables, "profilesContributions", "profiles.id", "profilesContributions.profileid");
            $this->addJoinedTable($joinedTables, "profilesRetirementNeeds", "profiles.id", "profilesRetirementNeeds.profileid");
            //$this->addJoinedTable($joinedTables,"profilesRiskProfile","profiles.id","profilesRiskProfile.profileid");
            $this->addJoinedTable($joinedTables, "profilesInvestments", "profiles.id", "profilesInvestments.profileid");
			$this->addJoinedTable($joinedTables, "plans", "profiles.planid", "plans.id");

            foreach ($joinedTables as $joinedTable)
            {
                $baseString = $baseString . "," . $profiles->generateQueryFields($joinedTable->table);
				$baseString = rtrim($baseString, ",");
            }
            $profileString = " FROM profiles profiles";
            $baseString = $baseString . $profileString;
            $baseStringCount = $baseStringCount . $profileString;

            foreach ($joinedTables as $joinedTable)
            {
                $joinString = $joinString . " LEFT JOIN   " . $joinedTable->table;
                $joinString = $joinString . " ON (";
                $joinString = $joinString . $joinedTable->profileKey . "=" . $joinedTable->joinedKey;
                $joinString = $joinString . "  )";
            }
            $searchstring = " AND deleted = 0 ";

            if (count($types) > 0)
            {
                if (isset($request['search']) && $request['search']['value'] != '')
                {

                    $searchstring = $searchstring . " AND ";

                    $searchstring = $searchstring . " ( ";

                    $searchcounter = 0;
                    foreach ($types as $type)
                    {

                        if ($searchcounter != 0) $searchstring = $searchstring . " OR ";
                        if (!$profiles->fieldTypes[$type]) $decryptionText = $type;
                        else $decryptionText = "AES_DECRYPT(" . $type . ", UNHEX('" . $profiles->decryptionKey . "'))";
                        $searchstring = $searchstring . " " . $decryptionText . "  LIKE '%" . $request['search']['value'] . "%'";
                        $searchcounter++;
                    }

                    $searchstring = $searchstring . " ) ";
                }
            }


            $profileSections = $profiles->sections();
            $request = Request::createFromGlobals();
            $request->getPathInfo();
            $generalMethods = $this->get('GeneralMethods');
            foreach ($profileSections as $section)
            {

                $searchstringcompare = $searchstring;
                $operationsvalue = str_replace("_PLUS_", "+" . $section->database . ".", $section->name);
                //$operationsvalue =  str_replace("_",".",$operationsvalue);
                $operationsvalue = $section->database . '.' . $operationsvalue;

                if ($section->type == "number")
                {


                    if (!is_numeric($request->request->get("equalto_" . $section->name . "_" . $section->database)))
                    {
                        if (is_numeric($request->request->get("lessthan_" . $section->name . "_" . $section->database)))
                        {
                            $comparevalue = floatval($request->request->get("lessthan_" . $section->name . "_" . $section->database));
                            $searchstring = $searchstring . ' AND  ' . $operationsvalue . ' <= ' . $comparevalue . " " . $section->additional;
                        }
                        if (is_numeric($request->request->get("greaterthan_" . $section->name . "_" . $section->database)))
                        {
                            $comparevalue = floatval($request->request->get("greaterthan_" . $section->name . "_" . $section->database));
                            $searchstring = $searchstring . ' AND  ' . $operationsvalue . ' >= ' . $comparevalue . " " . $section->additional;
                        }
                    }
                }
                if ($section->type == "date")
                {

                    if ($request->request->get("lessthan_" . $section->name . "_" . $section->database) != "")
                    {           
                            $endDate = $request->request->get("lessthan_" . $section->name . "_" . $section->database);
                            $endDate = $generalMethods->addDaysToString($endDate);                           
                            $searchstring = $searchstring . ' AND  ' . $operationsvalue . ' <= "' . $endDate . '"';
                    }
                    if ($request->request->get("greaterthan_" . $section->name . "_" . $section->database) != "") $searchstring = $searchstring . ' AND  ' . $operationsvalue . ' >= "' . $request->request->get("greaterthan_" . $section->name . "_" . $section->database) . '"';
                }
                if ($section->type == "checkboxes")
                {

                    if (trim($request->request->get("checkboxes_" . $section->name . "_" . $section->database)) != "")
                    {
                        $checkBoxes = $this->proper_escape($request->request->get("checkboxes_" . $section->name . "_" . $section->database));
                        $checkValues = explode(",", $checkBoxes);
                        $checkstring = "";
                        foreach ($checkValues as $checkValue)
                        {

                            if ($section->description == "Hide Test Plan Profiles") {
                                $checkstring = $checkstring . ' (' . $operationsvalue . "=" . $checkValue . ") ";
                                continue;
                            }
                            if ($checkstring != "") {
                                $checkstring = $checkstring . ' OR ';
                            }


                            $checkstring = $checkstring . '  ' . $operationsvalue . " LIKE '%" . $checkValue . "%'";
                        }
                        $checkstring = " AND (" . $checkstring . ")";
                        $searchstring = $searchstring . $checkstring;
                    }
                }
            }



            $profileString2 = " WHERE deleted=0 and profiles.id > 0 AND profiles.planid != 0 AND profiles.userid != 0 AND profilestatus = 1 AND reportDate is not null AND reportDate != '' AND reportDate != '0000-00-00 00:00:00'";
            if ($request->request->get("findby", "") != "") for ($i = 0; $i < count($findby); $i++) $profileString2 = $profileString2 . " AND " . $findby[$i] . "=" . $findbyvalues[$i];


			if (!$user->allPlans) {
				$profileString2 .= " AND plans.id IN (SELECT aup.planid FROM accountsUsersPlans aup WHERE aup.accountsUsersId=$clientid) ";
			}	
			
			if (!in_array($session->get("masterRoleType"), ["vwise_admin","admin_master"])) {
				$profileString2 .= " AND (plans.isHomePlan = 0  OR (plans.isHomePlan = 1 AND plans.id IN (SELECT hpa.planid FROM HomePlanAccess hpa WHERE hpa.accountsUsersId=$clientid))) ";
			}

            $baseString = $baseString . $joinString . $profileString2;
            $baseStringCount = $baseStringCount . $joinString . $profileString2;
            $baseStringCountFiltered = $baseStringCount . $searchstring;
            $querystring = $baseString . $searchstring;

            $groupby = "GROUP BY profiles.id";

            $querystring = $querystring . " " . $groupby . ' ' . $orderby . ' LIMIT ' . $start . ',' . $maxresults;

            $countquery = $connection->executeQuery($baseStringCount)->fetch();
            $countqueryFiltered = $connection->executeQuery($baseStringCountFiltered)->fetch();
            $recordsTotal = $countquery['total'];
            $recordsFiltered = $countqueryFiltered['total'];

            $this->items = $connection->fetchAll($querystring);
        }


        $jsonheader = '{
		
		"recordsTotal": ' . $recordsTotal . ',
		"recordsFiltered": ' . $recordsFiltered . ',
		"data": [';

        $params = array('items' => $this->items, 'request' => $request, 'controller' => $controller);
        if ($template == 'ManageManageBundle:PlansLibrary:Search/videolist.html.twig') { // case to deference an id to a string description for plansLibraryVideos
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
            $types = $repository->findAll();
            $typeMap = array();
            foreach ($types as $type) {
                $typeMap[$type->id] = $type->displayname;
            }
            $params['typeMap'] = $typeMap;
        } else if ($template == 'ManageManageBundle:Messaging:Search/messageslist.html.twig') { // case to deference an id to a string description for messagesPanes
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
            $groups = $repository->findAll();
            $groupMap = array();
            foreach ($groups as $group) {
                $groupMap[$group->id] = $group->name;
            }
            $params['groupMap'] = $groupMap;
        }
        $this->recordsFiltered = $recordsFiltered;
        $generalMethods = $this->get('GeneralMethods');
        $generalMethods->datatablesFilterData($this->items);
        $jsoncontent = $this->render($template, $params);
        $jsoncontent = $jsoncontent->getContent();
        $generalMethods->datatablesFilterJson($jsoncontent);
        $jsonfooter = ']
		}';
        return $jsonheader . $jsoncontent . $jsonfooter;
    }

    public function addJoinedTable(&$joinedTables, $table, $profileKey = null, $joinedKey = null)
    {
        $joinedTables[$table] = new \stdClass();
        $joinedTables[$table]->table = $table;
        $joinedTables[$table]->profileKey = $profileKey;
        $joinedTables[$table]->joinedKey = $joinedKey;
    }

    public function searchCsvCountAction(Request $request)
    {
        $this->searchDatatablesVariable($request);
        return new Response($this->recordsFiltered);
    }

    public function searchDatatablesAction(Request $request)
    {
        return new Response($this->searchDatatablesVariable($request));
    }

    public function order($request, $columns)
    {
        $order = '';

        if (isset($request['order']) && count($request['order']))
        {
            $orderBy = array();
            $dtColumns = $this->pluck($columns, 'dt');

            for ($i = 0, $ien = count($request['order']); $i < $ien; $i++)
            {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];

                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];

                if ($requestColumn['orderable'] == 'true')
                {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                            'ASC' :
                            'DESC';

                    $orderBy[] = '' . $columns[$requestColumn['data']] . ' ' . $dir;
                }
            }

            $order = 'ORDER BY ' . implode(', ', $orderBy);
        }

        return $order;
    }

    public function pluck($a, $prop)
    {
        $out = array();

        for ($i = 0, $len = count($a); $i < $len; $i++)
        {
            $out[] = $a[$i][$prop];
        }

        return $out;
    }

    public function searchVariables($maxresults = null, $pagenumber = null)
    {

        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        //post requests

        $table = $request->request->get('table', '');

        $findby = $request->request->get('findby', '');
        $findbyvalue = $request->request->get('findbyvalue', '');

        if ($findby != "false")
        {
            $findbys = explode(",", $findby);
            $findbysvalue = explode(",", $findbyvalue);
            $findbyVars = array();
            foreach ($findbys as $value)
            {
                $findbyVar = $value;
                $valueArray = explode(".", $value);
                if (count($valueArray) > 1) $findbyVar = $valueArray[1];
                $findbyVars[] = $findbyVar;
            }
        }

        $types = $request->request->get('types', '');
        $template = $request->request->get('template', '');
        $value = urldecode($request->request->get('value', ''));
        if ($pagenumber == null) $pagenumber = $request->request->get('pagenumber', '');
        if ($maxresults == null) $maxresults = $request->request->get('maxresults', '');
        $selectedletter = $request->request->get('selectedletter', '');
        $searchtype = $request->request->get('searchtype', '');
        $queryReference = $request->request->get("query", '');
        if ($queryReference == "") $searchField = "a.";
        else $searchField = "";


        if ($searchtype != "") $searchtypestring = "ORDER BY " . $searchField . $searchtype;



        $searchstring = "";
        $types = explode(",", $types);

        for ($i = 0; $i < count($types); $i++) if ($i == 0) $searchstring = '( ' . $searchField . $types[$i] . ' LIKE :searchvalue';
            else $searchstring = $searchstring . ' OR ' . $searchField . $types[$i] . ' LIKE :searchvalue';
        $searchstring = $searchstring . ' ) ';


        if ($selectedletter != "")
        {
            for ($i = 0; $i < count($types); $i++) if ($i == 0) $searchstring = $searchstring . ' AND ( ' . $searchField . $types[$i] . ' LIKE :selectedletter';
                else $searchstring = $searchstring . ' OR ' . $searchField . $types[$i] . ' LIKE :selectedletter';
            $searchstring = $searchstring . ' ) ';
        }

        if ($findby != "false")
        {
            for ($i = 0; $i < count($findbys); $i++)
            {
                $searchstring = $searchstring . ' AND ' . $searchField . $findbys[$i] . ' = :' . $findbyVars[$i];
            }
            //default search condition that has nothing to do with value searched, ex "id"
        }
        if ($queryReference == "")//one entity
        {
            $queryCountString = 'SELECT COUNT(a)
		 FROM classesclassBundle:' . $table . ' a
		 WHERE ' . $searchstring;
            $queryString = 'SELECT a
		 FROM classesclassBundle:' . $table . ' a
		 WHERE ' . $searchstring . ' ' . $searchtypestring;
        }

        if ($queryReference == "adviserPlanList")
        {
            $queryCountString = 'SELECT COUNT(a) FROM classesclassBundle:' . $table . ' a
            JOIN a.plan b WHERE ' . $searchstring;

            $queryString = 'SELECT a, b FROM classesclassBundle:' . $table . ' a
            JOIN a.plan b  WHERE ' . $searchstring . ' ' . $searchtypestring;
        }




        //other special cases go here
        $query = $em->createQuery
                (
                $queryCountString
        );
        $query->setParameter('searchvalue', '%' . $value . '%');
        if ($findby != "false") for ($i = 0; $i < count($findbys); $i++) $query->setParameter($findbyVars[$i], $findbysvalue[$i]);
        if ($selectedletter != "") $query->setParameter('selectedletter', $selectedletter . '%');
        if (isset($checkArray)) foreach ($checkArray as $checkKey => $checkValue) $query->setParameter($checkKey, '%' . $checkValue . '%');

        $count = $query->getSingleScalarResult();

        $query = $em->createQuery
                (
                $queryString
        );
        $query->setParameter('searchvalue', '%' . $value . '%');
        if ($findby != "false") for ($i = 0; $i < count($findbys); $i++) $query->setParameter($findbyVars[$i], $findbysvalue[$i]);
        if ($selectedletter != "") $query->setParameter('selectedletter', $selectedletter . '%');

        if (isset($checkArray)) foreach ($checkArray as $checkKey => $checkValue) $query->setParameter($checkKey, '%' . $checkValue . '%');

        $query->setMaxResults($maxresults);
        $query->setFirstResult(($pagenumber - 1) * $maxresults);

        $this->searchQuery = $query;
        $this->searchQueryCount = $count;
    }

    public function searchAction()//generic searching
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();

        $this->searchVariables();
        $items = $this->searchQuery->getResult();
        $page = $request->request->get('template', '');
        $responsetemplate = $this->render($page, array('items' => $items));
        if (count($items) > 0) $response = new Response($responsetemplate->getContent() . "<!--*totalrows*" . $this->searchQueryCount . "*totalrows*-->");
        else $response = new Response("Sorry, no results found<!--*totalrows*" . $this->searchQueryCount . "*totalrows*-->");
        return $response;
    }

    function searchTemplateAction($options)
    {

        return $this->render($options->searchtemplate, array("options" => $options));
    }

    public function search($table, $findby, $findbyvalue, $datatype = "items", $templatename = "fundlist.html.twig", $searchtypes = "name", $listname = "item", $searchoptionsdisplay = "inline-block", $searchtemplate = "classesclassBundle:Search:search.html.twig", $searchoptionsvaluestring = "", $seachoptionsdescriptionstring = "", $resultsperpage = "", $query = "")
    {
        $item->datatype = $datatype;
        $item->listname = $listname;
        $item->findby = $findby;
        $item->findbyvalue = $findbyvalue;
        $item->templatename = $templatename;
        $item->searchtemplate = $searchtemplate;
        $item->table = $table;
        $item->searchtypes = $searchtypes;
        if ($resultsperpage == "") $item->resultsperpage = array(10, 20, 50, 100, 200);
        else $item->resultsperpage = explode(",", $resultsperpage);

        $item->filterby = $this->alphalist();
        if ($searchoptionsvaluestring == "")
        {
            $item->searchoptions[0]->value = "id DESC";
            $item->searchoptions[0]->description = "Newest-Oldest";
            $item->searchoptions[1]->value = "id ASC";
            $item->searchoptions[1]->description = "Oldest-Newest";
            $item->searchoptions[2]->value = "name ASC";
            $item->searchoptions[2]->description = "A-Z";
            $item->searchoptions[3]->value = "name DESC";
            $item->searchoptions[3]->description = "Z-A";
        }
        else
        {
            $searchoptionsvalues = explode(",", $searchoptionsvaluestring);
            $seachoptionsdescriptions = explode(",", $seachoptionsdescriptionstring);
            for ($i = 0; $i < count($searchoptionsvalues); $i++)
            {
                $item->searchoptions[$i]->value = $searchoptionsvalues[$i];
                if ($seachoptionsdescriptionstring == "") $item->searchoptions[$i]->description = $searchoptionsvalues[$i];
                else $item->searchoptions[$i]->description = $seachoptionsdescriptions[$i];
            }
        }
        $item->searchoptionsdisplay = $searchoptionsdisplay;
        $item->query = $query;
        //new new table
        return $item;
    }
    public function createHash($len){
        $alphanumeric = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789';
        $alphanumericNum = strlen($alphanumeric) - 1;

        $hash = NULL;
        for($i = 1; $i <= $len; $i++){
            $pos = rand(0,$alphanumericNum);
            $hash .= substr($alphanumeric,$pos,1);
        }

        return $hash;
    }
    public function getUserId($session)
    {
        $userid = $session->get("userid");
        if (empty($userid)) {
            $userid = $session->get('plan')['userid'];
        }
        return $userid;
    }

    public function getPlanId($session)
    {
        $planid = $session->get("planid");
        if (empty($planid)) {
            $planid = $session->get('plan')['id'];
        }
        return $planid;
    }

    public function authfailuremssage()
    {
        return "not authorized";
    }

    public function alphalist()
    {
        $char = 'a';
        for ($i = 0; $i < 26; $i++) $charlist[$i] = $char++;
        return $charlist;
    }

    public function calculateDelta($enter, $exit) {

        if ($exit > $enter) {
            $changeDef = (($exit - $enter) / $enter) * 100;
        } else if ($enter > $exit) {
            $changeDef = (($enter - $exit) / $enter) * 100;
        } else {
            $changeDef = 0;
        }
        return $changeDef;
    }
}
