<?php
class mailigen 
{
    public $mailEngine;
    public $apiKey;
    public $emptyListId;
    public $api;
    public $type;

    public function __construct()
    {
    }
    public function init($mailEngine)
    {
        $this->mailEngine = $mailEngine;
        $this->apiKey = $this->mailEngine->apiKey;
        $this->emptyListId = $this->mailEngine->emptyListId;
        $this->type = $this->mailEngine->type;
        require_once 'mgapi/inc/MGAPI.class.php';
        $this->api = new MGAPI($this->apiKey);
    }
    public function createCampaign(&$params,&$campaign,&$html,&$text)
    {
        $campaign->title = $params['title'];
        $campaign->subject = $params['subject'];
        $campaign->fromEmail = $params['fromEmail'];
        $campaign->fromName = $params['fromName'];
        $campaign->html = $params['html'];
        $campaign->mailEngineCampaignid = 0;
    }
    public function updateCampaign(&$params,&$campaign)
    {
        $cid = $campaign->mailEngineCampaignid;
        $name = 'title';
        $value = $params['title'];
        $this->api->campaignUpdate($cid, $name, $value);
        $name1 = 'subject';
        $value1 = $params['subject'];
        $this->api->campaignUpdate($cid, $name1, $value1);
        $name2 = 'from_email';
        $value2 = $params['fromEmail'];
        $this->api->campaignUpdate($cid, $name2, $value2);
        $name3 = 'from_name';
        $value3 = $params['fromName'];
        $this->api->campaignUpdate($cid, $name3, $value3);
    }
    public function createList(&$params,&$contact,&$currentCampaign,&$campaignidString) 
    {
        $title = time();
        $options = array(
            'permission_reminder' => 'Write a short reminder about how the recipient joined your list.',
            'notify_to' => 'mbeaussart@vwise.com',
            'subscription_notify' => true,
            'unsubscription_notify' => true,
            'has_email_type_option' => true
        );
        $listId = $this->api->listCreate($title, $options);
        $id = $listId;
        $batch = array();
        $double_optin = true;
        $update_existing = false;
        foreach ($params['emails'] as $email) {
            $batch[] = array('EMAIL' => $email['emailAddress']);
        }
        $this->api->listBatchSubscribe($id, $batch, $double_optin, $update_existing);
        
        if($currentCampaign->mailEngineListid == 0) {
            $options1 = array(
                'list_id' => $id,
                'subject' => $currentCampaign->subject,
                'from_email' => $currentCampaign->fromEmail,
                'from_name' => $currentCampaign->fromName,
                'title' => $currentCampaign->title,
                'generate_text' => true
            );
            $currentCampaign->mailEngineListid = $id;
            $content1 = array(
                'html' => $currentCampaign->html
            );
            $retval1 = $this->api->campaignCreate($this->type, $options1, $content1); 
            $currentCampaign->mailEngineCampaignid = $retval1;
        } else {
            $this->api->campaignDelete($currentCampaign->mailEngineCampaignid);
            $this->api->listDelete($currentCampaign->mailEngineListid);
            $options2 = array(
                'list_id' => $id,
                'subject' => $currentCampaign->subject,
                'from_email' => $currentCampaign->fromEmail,
                'from_name' => $currentCampaign->fromName,
                'title' => $currentCampaign->title,
                'generate_text' => true
            );
            $currentCampaign->mailEngineListid = $id;
            $content2 = array(
                'html' => $currentCampaign->html
            );
            $retval2 = $this->api->campaignCreate($this->type, $options2, $content2); 
            $currentCampaign->mailEngineCampaignid = $retval2;        
        }
    }
    public function sendCampaign(&$params,&$campaign)
    {
        $cid = $campaign->mailEngineCampaignid;
        if (isset($params['scheduledTime'])) {
            $schedule_time = $params['scheduledTime'];//new DateTime($params['scheduledTime'], new DateTimeZone('America/Los_Angeles')); 
            $this->api->campaignSchedule($cid, $schedule_time);
            $sentDate = $params['scheduledTime'];
        } else {
            $sentDate = date("Y-m-d H:i:s");
            $this->api->campaignSendNow($cid);
        }
        return $sentDate;
    }
    public function getStats(&$campaign)
    {
        $delivered = 0;
        $unique_opens = 0;
        $unique_clicks = 0;
        $bounces = 0;
        $unsubscribes = 0;
        $cid = $campaign->mailEngineCampaignid; 
        $retval = $this->api->campaignStats($cid);
        foreach ($retval as $k => $v) {
            if($k == 'emails_sent') {
                $delivered = $v;
            }
            if($k == 'unique_opens') {
                $unique_opens = $v;
            }
            if($k == 'unique_clicks') {
                $unique_clicks = $v;
            }
            if($k == 'unsubscribes') {
                $unsubscribes = $v;
            }
            if($k == 'hard_bounces') {
                $bounces += $v;
            }
            if($k == 'soft_bounces') {
                $bounces += $v;
            }
            if($k == 'blocked_bounces') {
                $bounces += $v;
            }
            if($k == 'temporary_bounces') {
                $bounces += $v;
            }
            if($k == 'generic_bounces') {
                $bounces += $v;
            }
        }
        $emailStats = new \stdClass();
        $emailStats->opens = new \stdClass();
        $emailStats->clicks = new \stdClass();
        
        $emailStats->delivered = $delivered;
        $emailStats->opens->unique = $unique_opens;
        $emailStats->clicks->unique = $unique_clicks;
        $emailStats->bounces = $bounces;
        $emailStats->unsubscribes = $unsubscribes;

        return $emailStats;
    }
    public function viewUsers($params,&$campaign)
    {
        $users = array();
        $first_name = '';
        $last_name = '';
        $id = $campaign->mailEngineListid;
        $status = "subscribed";
        $start = 0;
        $limit = 500;
        $retval = $this->api->listMembers($id, $status, $start, $limit);
        foreach ($retval as $member) {
            $email = $member['email']; 
            $users[$email] = new \stdClass();
            $users[$email]->email = $email;
            $users[$email]->firstName = $first_name;
            $users[$email]->lastName = $last_name;
            if ($users[$email]->firstName instanceof \stdClass)
            $users[$email]->firstName = "";
            if ($users[$email]->lastName instanceof \stdClass)
            $users[$email]->lastName = "";
        }
        return $users;
    }    
    public function clearList($params,$currentCampaign,$users)
    {
        
    }
    public function cancelCampaign($params,$campaign)
    {
        $cid = $campaign->mailEngineCampaignid;
        $this->api->campaignUnschedule($cid);
    }
    public function previewTemplate($params,$campaign)
    {
        return $campaign->html;
    }
    public function previewProperites($params,$campaign)
    {
        $properties = new \stdClass();
        $properties->subject = $campaign->subject;
        $properties->fromName = $campaign->fromName;
        $properties->fromEmail = $campaign->fromEmail;
        return $properties;
    }
    public function curlCall($url, $data, $options = null, $method = 'POST')
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        
        if ($method == 'PUT') {
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        }
        if ($method == 'DELETE') {
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }
        if ($data != "")
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $buf = curl_exec($ch);
        curl_close($ch);
        return $buf;
    }     
}