<?php

namespace classes\classBundle\Classes;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\plans;
use classes\classBundle\Entity\plansModules;
use classes\classBundle\Entity\plansLibraryHotTopics;
use classes\classBundle\Entity\PlansMessages;
use classes\classBundle\Entity\plansDocuments;
class plansMethods
{
	public $controller;
	//we soft delete plans now, keep just in case we need to hard delete plans
	public function deleteplan($planid)
	{
		$general = new GeneralMethods($this->controller);
		foreach ($this->deleteSections() as $section)
		$general->removeTableData($section,"planid",$planid);
		$general->removeTableData("plans","id",$planid);
	}

    public function addPlan($plantobeadded,$userid,$em,$defaultPlan)
    {
    	
		$plantobeadded->name = "new plan";
		$this->addPlanSegment($plantobeadded,$defaultPlan);
		$plantobeadded->createdDate = new \DateTime("NOW");
		$plantobeadded->modifiedDate= new \DateTime("NOW");
    }

    public function fillInMissingPlanTables($userid,$planid,&$addeddata)
    {
    	$this->copyTableData("plansModules","defaultModules","userid",$userid,$planid,$addeddata);
	$this->copyTableData("plansLibraryHotTopics","defaultLibraryHotTopics","userid",$userid,$planid,$addeddata);
	$this->copyTableData("PlansMessages","DefaultMessages","userid",$userid,$planid,$addeddata);

    }
    private function addPlanSegment(&$object,$copiedFrom)
    {
		$class_vars = get_class_vars(get_class($object));
		foreach ($class_vars as $key => $value)
		{
		if (isset($copiedFrom->$key))
			$object->$key = $copiedFrom->$key;
		}
    }
    public function copyTableData($table,$othertable,$findby,$findbyvalue,$idvalue,&$addeddata)
    {
		$em = $this->controller->get("doctrine")->getManager();

		$repository = $this->controller->get("doctrine")->getRepository('classesclassBundle:'.$table);
		$currentdata = $repository->findBy(array("planid" => $idvalue));

		if (count($currentdata) > 0)//only copies data if no rows are returned
		return false;



		$repository = $this->controller->get("doctrine")->getRepository('classesclassBundle:'.$othertable);
		$otherdatas = $repository->findBy(array($findby => $findbyvalue));


		foreach ($otherdatas as $otherdata)
		{


			if ($table == "plansModules") {
                            $addeddata = new plansModules();
                        }	
			if ($table == "plansLibraryHotTopics") {
                            $addeddata = new plansLibraryHotTopics();
                        }	
			if ($table == "PlansMessages") {
                            $addeddata = new PlansMessages();
                        }	
			if ($table == "plansDocuments") {
                            $addeddata = new plansDocuments();
                        }	

			$this->addPlanSegment($addeddata,$otherdata);
			$addeddata->pensionEstimatorURL = NULL;     // need to explicitly null this column
			$addeddata->planid = $idvalue;
			
		}
		return true;
    }        

	private function deleteSections()
	{

		return array(
			"advisersPlans",
			"plansDocuments",
			"plansLibraryHotTopics",
			"plansModules",
			"plansOutreach",
			"plansPortfolioFunds",
			"plansPortfolios",
			"plansRiskBasedFunds",
			"plansFunds",
			"PlansMessages");

	}
	public function deleteplans($userid)
	{
		$general = new GeneralMethods($this->controller);
		foreach ($this->deleteSections() as $section)
		$general->removeTableData($section,"userid",$userid);
		$general->removeTableData("plans","userid",$userid);
	}




	public function __construct($controller)
	{
		$this->controller = $controller;
	}
}