<?php
class mailjet 
{
    public $mailEngine;
    public $apiKey;
    public $secret;
    public $defaultListId;
    public $headers;

    public function __construct()
    {
    }
    public function init($mailEngine)
    {
        $this->mailEngine = $mailEngine;
        $this->apiKey = $this->mailEngine->apiKey;
        $this->secret = $this->mailEngine->secret;
        $this->defaultListId = $this->mailEngine->defaultListId;
        $this->headers = array(
            'Content-Type:application/json'
        );
    }
    public function createCampaign(&$params,&$campaign,&$html,&$text)
    {
        $url = "https://api.mailjet.com/v3/REST/newsletter";
        $ID_CONTACTSLIST = (int)$this->defaultListId;
        $data = array();
        $data["Locale"] = "en_US";
        $data["Sender"] = $params['fromName'];
        $data["SenderEmail"] = $params['fromEmail'];
        $data["Subject"] = $params['subject'];
        $data["ContactsListID"] = "$ID_CONTACTSLIST";
        $data["Title"] = $params['title'];
        $campaign->fromEmail = $params['fromEmail'];
        $json = json_encode($data);
        $buf = $this->curlCall($url, $json, 'null', 'POST');
        $res = json_decode($buf);                                                                                                                                    
        foreach($res as $val) {                                                                                                                                  
            if(is_array($val)) {                                                                                                                                           
                foreach($val as $val1) {                                                                                                                                     
                    $campaign->mailEngineCampaignid = $val1->ID;
                    $newsletterId = $val1->ID;
                }                                                                                                                                                            
            }                                                                                                                                                              
        } 
        $data1 = array();
        $data1["Html-part"] = $html;
        $url1 = "https://api.mailjet.com/v3/REST/newsletter/".$newsletterId."/detailcontent";
        $json1 = json_encode($data1);
        $buf1 = $this->curlCall($url1, $json1, 'null', 'PUT');
    }
    public function updateCampaign(&$params,&$campaign)
    {
        $url = "https://api.mailjet.com/v3/REST/newsletter";
        $ID_CONTACTSLIST = (int)$campaign->mailEngineListid;
        $data = array();
        $data["Locale"] = "en_US";
        $data["Sender"] = $params['fromName'];
        $data["SenderEmail"] = $params['fromEmail'];
        $data["Subject"] = $params['subject'];
        $data["ContactsListID"] = "$ID_CONTACTSLIST";
        $data["Title"] = $params['title'];
        $campaign->fromEmail = $params['fromEmail'];
        $json = json_encode($data);
        $buf = $this->curlCall($url, $json, 'null', 'POST');
        $res = json_decode($buf);                                                                                                                                    
        foreach($res as $val) {                                                                                                                                  
            if(is_array($val)) {                                                                                                                                           
                foreach($val as $val1) {                                                                                                                                     
                    $campaign->mailEngineCampaignid = $val1->ID;
                    $newsletterId = $val1->ID;
                }                                                                                                                                                            
            }                                                                                                                                                              
        } 
        $data1 = array();
        $data1["Html-part"] = $params['html'];
        $url1 = "https://api.mailjet.com/v3/REST/newsletter/".$newsletterId."/detailcontent";
        $json1 = json_encode($data1);
        $buf1 = $this->curlCall($url1, $json1, 'null', 'PUT');
    }
    public function createList(&$params,&$contact,&$currentCampaign,&$campaignidString) 
    {
        $url = "https://api.mailjet.com/v3/REST/contactslist";
        $data = array();
        $data['Name'] = time();
        $json = json_encode($data);
        $buf = $this->curlCall($url, $json, 'null', 'POST');
        $res = json_decode($buf);                                                                                                                                    
        foreach($res as $val) {                                                                                                                                  
            if(is_array($val)) {                                                                                                                                           
                foreach($val as $val1) {                                                                                                                                     
                    $listId = $val1->ID;
                }                                                                                                                                                            
            }                                                                                                                                                              
        }
        $currentCampaign->mailEngineListid = $listId;
        $url1 = "https://api.mailjet.com/v3/REST/contact/managemanycontacts";                                                                                             
        $data1 = array();
        $data2 = array();
        $data1["ContactsLists"] = array(array("ListID" => $listId,  "action" => "addforce"));                                                                                         
        foreach ($params['emails'] as $email) {
            if(!empty($email['emailAddress'])) {
                $data2[] = array("email" => $email['emailAddress']);
            }
        }
        $data1["Contacts"] = $data2;
        $json1 = json_encode($data1);                                                                                                                                                                                                                                                                                      
        $buf1 = $this->curlCall($url1, $json1, 'null', 'POST');                                                                                                                    
        
        $url3 = "https://api.mailjet.com/v3/REST/newsletter";
        $ID_CONTACTSLIST = (int)$listId;
        $data3 = array();
        $data3["Locale"] = "en_US";
        $data3["Sender"] = $currentCampaign->fromName;
        $data3["SenderEmail"] = $currentCampaign->fromEmail;
        $data3["Subject"] = $currentCampaign->subject;
        $data3["ContactsListID"] = "$ID_CONTACTSLIST";
        $data3["Title"] = $currentCampaign->title;
        $json3 = json_encode($data3);
        $buf3 = $this->curlCall($url3, $json3, 'null', 'POST');
        $res3 = json_decode($buf3);                                                                                                                                    
        foreach($res3 as $val) {                                                                                                                                  
            if(is_array($val)) {                                                                                                                                           
                foreach($val as $val1) {                                                                                                                                     
                    $currentCampaign->mailEngineCampaignid = $val1->ID;
                    $newsletterId = $val1->ID;
                }                                                                                                                                                            
            }                                                                                                                                                              
        } 
        $data4 = array();
        $data4["Html-part"] = $currentCampaign->html;
        $url4 = "https://api.mailjet.com/v3/REST/newsletter/".$newsletterId."/detailcontent";
        $json4 = json_encode($data4);
        $buf4 = $this->curlCall($url4, $json4, 'null', 'PUT');
    }
    public function sendCampaign(&$params,&$campaign)
    {
        $url = "https://api.mailjet.com/v3/REST/newsletter/".$campaign->mailEngineCampaignid."/send";
        $json =  json_encode(new stdClass);
        $buf = $this->curlCall($url, $json, 'null', 'POST');
        $sentDate = date("Y-m-d H:i:s");
        return $sentDate;
        //print_r($buf);
    }
    public function getStats(&$campaign)
    {
        $delivered = 0;
        $unique_opens = 0;
        $unique_clicks = 0;
        $bounces = 0;
        $unsubscribes = 0;
        
        $url = "https://api.mailjet.com/v3/REST/campaignstatistics?NewsLetter=".$campaign->mailEngineCampaignid;                                                                                     
        $buf = $this->curlCall($url, '', 'null', 'GET');                                                                                                                        
        $res = json_decode($buf);                                                                                                                                        
        foreach($res as $val) {                                                                                                                                          
            if(is_array($val)) {                                                                                                                                           
                foreach($val as $val1) {                                                                                                                                     
                    $delivered = $val1->DeliveredCount;                                                                                                                                                                                                                                                                         
                    $unique_opens = $val1->OpenedCount;                                                                                                                                                                                                                                                                         
                    $unique_clicks = $val1->ClickedCount;                                                                                                                                                                                                                                                                       
                    $bounces = $val1->BouncedCount;                                                                                                                                                                                                                                                                             
                    $unsubscribes = $val1->UnsubscribedCount;                                                                                                                                                                                                                                                                   
                }                                                                                                                                                            
            }                                                                                                                                                              
        }        
          
        $emailStats = new \stdClass();
        $emailStats->opens = new \stdClass();
        $emailStats->clicks = new \stdClass();
        
        $emailStats->delivered = $delivered;
        $emailStats->opens->unique = $unique_opens;
        $emailStats->clicks->unique = $unique_clicks;
        $emailStats->bounces = $bounces;
        $emailStats->unsubscribes = $unsubscribes;

        return $emailStats;
    }
    public function viewUsers($params,&$campaign)
    {
        $users = array();
        $first_name = '';
        $last_name = '';
        $url = "https://api.mailjet.com/v3/REST/contact?ContactsList=" . $campaign->mailEngineListid;                                                                                             
        $buf = $this->curlCall($url, '', 'null', 'GET');                                                                                                                        
        $res = json_decode($buf);                                                                                                                                        
        foreach($res as $val) {                                                                                                                                          
            if(is_array($val)) {                                                                                                                                           
                foreach($val as $val1) {                                                                                                                                     
                    $email = $val1->Email; 
                    $users[$email] = new \stdClass();
                    $users[$email]->email = $email;
                    $users[$email]->firstName = $first_name;
                    $users[$email]->lastName = $last_name;
                    if ($users[$email]->firstName instanceof \stdClass)
                    $users[$email]->firstName = "";
                    if ($users[$email]->lastName instanceof \stdClass)
                    $users[$email]->lastName = "";                                                                                                                                  
                }                                                                                                                                                            
            }                                                                                                                                                              
        } 
        return $users;
    }    
    public function clearList($params,$currentCampaign,$users)
    {
    }
    public function cancelCampaign($params,$campaign)
    {

    }
    public function previewTemplate($params,$campaign)
    {
        return $campaign->html;
    }
    public function previewProperites($params,$campaign)
    {
        $properties = new \stdClass();
        $properties->subject = $campaign->subject;
        $properties->fromName = $campaign->fromName;
        $properties->fromEmail = $campaign->fromEmail;
        return $properties;
    }
    public function curlCall($url, $data, $options = null, $method = 'POST')
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, "$this->apiKey:$this->secret");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
        }       
        if ($method == 'PUT') {
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        }
        if ($method == 'DELETE') {
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }
        if ($data != "")
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $buf = curl_exec($ch);
        curl_close($ch);
        return $buf;
    }     
}