<?php
class iContact
{
    public $mailEngine;
    public function __construct()
    {
       
    }
    public function init($mailEngine)
    {
        $this->mailEngine = $mailEngine;
        $this->headers = array(
            'Accept: text/xml',
            'Content-Type: text/xml',
            'Api-Version: 2.0',
            'Api-AppId: ' . $this->mailEngine->key,
            'Api-Username: ' . $this->mailEngine->username,
            'Api-Password: ' . $this->mailEngine->password
        ); 
    }
    public function createCampaign(&$params,&$campaign,&$html,&$text)
    {
        $data = "<campaigns>
         <campaign>
             <name>" . $this->mailEngine->escapeXml($params['title']) . "</name>
             <description>" . $this->mailEngine->escapeXml($params['description']) . "</description>
             <fromEmail>" . $this->mailEngine->escapeXml($params['fromEmail']) . "</fromEmail>
             <fromName>" . $this->mailEngine->escapeXml($params['fromName']) . "</fromName>
             <forwardToFriend>3</forwardToFriend>
             <subscriptionManagement>0</subscriptionManagement>
             <useAccountAddress>1</useAccountAddress>
             <clickTrackMode>1</clickTrackMode>
             <archiveByDefault>1</archiveByDefault>
          </campaign>
        </campaigns>";

            $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->createCampaignPath, $data);

            $campaign->mailEngineCampaignid = simplexml_load_string($buf)->campaigns->campaign->campaignId;
            $data = "
        <messages>
                <message>
                        <campaignId>" . $campaign->mailEngineCampaignid . "</campaignId>
                        <subject>" . $this->mailEngine->escapeXml($params['subject']) . "</subject>
                        <messageType>normal</messageType>
                        <messageName>" . $this->mailEngine->escapeXml($params['title']) . "</messageName>
                        <htmlBody>" . $this->mailEngine->escapeXml($html) . "</htmlBody>
                        <textBody>" . $this->mailEngine->escapeXml($text) . "</textBody>
                </message>
        </messages>";

        $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->createTemplatePath, $data);

        $xml = simplexml_load_string($buf);
        $campaign->mailEngineTemplateid = $xml->messages->message->messageId;
    }
    public function updateCampaign(&$params,&$campaign)
    {
        $data = "
        <campaigns>
        <campaign><campaignId>" . $campaign->mailEngineCampaignid . "</campaignId>";
        if (isset($params['title']))
        {
            $data = $data . "<name>" . $this->mailEngine->escapeXml($params['title']) . "</name>";
        }
        if (isset($params['description']))
        {
            $data = $data . "<description>" . $this->mailEngine->escapeXml($params['description']) . "</description>";
        }
        if (isset($params['fromEmail']))
        {
            $data = $data . "<fromEmail>" . $this->mailEngine->escapeXml($params['fromEmail']) . "</fromEmail>";
        }
        if (isset($params['fromName']))
        {
            $data = $data . "<fromName>" . $this->mailEngine->escapeXml($params['fromName']) . "</fromName>";
        }
        $data = $data . "</campaign>
        </campaigns>";

        $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->updateCampaignPath, $data);
        //echo $buf;
        $data = "
        <messages>
                <message>
                        <messageId>" . $campaign->mailEngineTemplateid . "</messageId>  
        ";
        if (isset($params['subject']))
        {
            $data = $data . "<subject>" . $this->mailEngine->escapeXml($params['subject']) . "</subject>";
        }
        if (isset($params['title']))
        {
            $data = $data . "<messageName>" . $this->mailEngine->escapeXml($params['title']) . "</messageName>";
        }
        if (isset($params['html']))
        {
            $data = $data . "<htmlBody>" . $this->mailEngine->escapeXml($params['html']) . "</htmlBody>";
        }
        if (isset($params['textBody']))
        {
            $data = $data . "<textBody>" . $this->mailEngine->escapeXml($params['textBody']) . "</textBody>";
        }
        $data = $data . "</message></messages>";
        $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->updateTemplatePath, $data);        
        
    }
    public function createList(&$params,&$contact,&$currentCampaign,&$campaignidString)
    {
        
        if ($currentCampaign->mailEngineListid != "")
        {
            $ch = curl_init($this->mailEngine->url.$this->mailEngine->createSegmentPath."/".$currentCampaign->mailEngineListid);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            $buf = curl_exec($ch);
            curl_close($ch);   
        }
        
        $sql = "SELECT * FROM campaigns WHERE sentDate != '0000-00-00 00:00:00'";
        $campaigns = $this->mailEngine->dbal->fetchAll($sql);
        foreach ($campaigns as $campaign)
        {
            $ch = curl_init($this->mailEngine->url.$this->mailEngine->createSegmentPath."/".$campaign['mailEngineListid']);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            $buf = curl_exec($ch);
            curl_close($ch);
            //echo $buf;
        } 
         $listName = time();
         $data = "<lists>
            <list>
            <name>" . $listName . "</name>
            </list>
            </lists>";

        $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->createList, $data);
        $xml = simplexml_load_string($buf);
        $currentCampaign->mailEngineListid = $xml->lists->list->listId;
        $listId = $xml->lists->list->listId;
        
        $data = "<contacts>";
        foreach ($params['emails'] as $email)
        {
            $firstName = "";
            $lastName = "";
            if (isset($email['firstName'])) $firstName = $email['firstName'];
            if (isset($email['lastName'])) $lastName = $email['lastName'];
            $data = $data . "<contact><email>" . $email['emailAddress'] . "</email>
       <firstName>" . $firstName . "</firstName>
       <lastName>" . $lastName . "</lastName>
       <campaignid>" . $campaignidString . "</campaignid></contact>";
        }
        $data = $data . "</contacts>";
        //echo $data;
        //echo $data;
        $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->createListPath, $data);
        //echo $buf;

        $xml = simplexml_load_string($buf);
        $contactString = "";
        foreach ($xml->contacts->contact as $contact)
        {
            $contactString = $contactString . "<subscription> <contactId>" . $contact->contactId . "</contactId>
            <listId>" . $listId . "</listId>
            <status>normal</status></subscription>";
        }

        $data = "
        <subscriptions>

        " . $contactString . "

        </subscriptions>
    ";


        $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->createSubscriptionPath, $data);
       // echo "subscription".$buf."endsubscription";

        /*$data = "
    <segments>
        <segment>
            <name>" . $currentCampaign->title . " List</name>
            <description>Enter Description for: " . $currentCampaign->title . "</description>
            <listId>15683</listId>
        </segment>
    </segments>";

        $buf = $this->curlCall($this->mailEngine->url.$this->mailEngine->createSegmentPath, $data);

        //echo $buf;
        //echo $buf;
        $xml = simplexml_load_string($buf);
        $currentCampaign->mailEngineListid = $xml->segments->segment->segmentId;
        $data = "
    <criteria>
        <criterion>
            <fieldName>campaignid</fieldName>
            <operator>contains</operator>
            <values>
                <value>-" . $currentCampaign->mailEngineCampaignid . "-</value>
            </values>
        </criterion>   
    </criteria>
    ";
       // echo $this->mailEngine->url . $this->mailEngine->createSegmentPath . "/" . $xml->segments->segment->segmentId . "/" . $this->mailEngine->createCriteriaPath;
        $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->createSegmentPath . "/" . $xml->segments->segment->segmentId . "/" . $this->mailEngine->createCriteriaPath, $data);
       // echo $buf; */      
    }
    public function sendCampaign(&$params,&$campaign)
    {
            $scheduledTime = "";

            $data = "
            <sends>
                <send>
                        <messageId>" . $campaign->mailEngineTemplateid . "</messageId>
                            <includeListIds>". $campaign->mailEngineListid."</includeListIds>
                           ".$scheduledTime."
                </send>
            </sends>
                   ";
           
            $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->sendCampaignPath, $data);
            
            $xml = simplexml_load_string($buf);
            if (isset($xml->sends->send->scheduledTime))
            return 1;
            else
            return -1;       
    }
    public function curlCall($url, $data, $options = null)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        if ($data != "")
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $buf = curl_exec($ch);
        curl_close($ch);
        return $buf;
    }     
    public function getStats(&$campaign)
    {
        $buf = $this->curlCall($this->mailEngine->url.$this->mailEngine->templatePath."/".$campaign->mailEngineTemplateid."/".$this->mailEngine->stats, "");
        $stats = json_decode(json_encode((array) simplexml_load_string($buf)));
        $stats = $stats->statistics; 
        return $stats;
    }
    public function viewUsers($params,&$campaign)
    {
        /*$buf = $this->curlCall($this->mailEngine->url.$this->mailEngine->createSegmentPath."/".$campaign->mailEngineListid."/".$this->mailEngine->createCriteriaPath,"");
        $xml = json_decode(json_encode((array) simplexml_load_string($buf)));
        $campaignid = (int)$xml->criteria->criterion->values->value;
        if ("-".$campaign->mailEngineCampaignid."-" !=  $campaignid)
        return -1;
        $url = $this->mailEngine->url."contacts/?status=total&listId=15683&campaignid=*".$campaignid."*";
        $buf = $this->curlCall($url,"");
        $xml = json_decode(json_encode((array) simplexml_load_string($buf)));

        
        $buf = $this->curlCall($url."*&limit=".$xml->total,"");
        $xml = json_decode(json_encode((array) simplexml_load_string($buf)));
        $users = array();
        $counter = 0;
        
        if (count($xml->contacts->contact) != 1)
        $contacts = $xml->contacts->contact;
        else
        $contacts = $xml->contacts;*/
        $buf = $this->curlCall($this->mailEngine->url.'/'.$this->mailEngine->createListPath.'?status=total&listId=' . $campaign->mailEngineListid,"");
         $xml = json_decode(json_encode((array) simplexml_load_string($buf)));
        //print_r($xml);
        //$contacts = $xml->contacts;
        //print_r($contacts);
        $users = array();
        
        if (count($xml->contacts->contact) != 1)
        $contacts = $xml->contacts->contact;
        else
        $contacts = $xml->contacts;
        foreach ($contacts as $contact)
        {         
            if ($contact->status == "normal")
            {
                $users[$contact->email] = new \stdClass();
                $users[$contact->email]->email = $contact->email;
                $users[$contact->email]->firstName = $contact->firstName;
                $users[$contact->email]->lastName = $contact->lastName;
                if ($users[$contact->email]->firstName instanceof \stdClass)
                $users[$contact->email]->firstName = "";
                if ($users[$contact->email]->lastName instanceof \stdClass)
                $users[$contact->email]->lastName = "";
            }   
        }
           
        return $users;
    }    
    public function clearList($params,$currentCampaign,$users)
    {
        $url = $this->mailEngine->url."contacts/?status=total&listId=15683&campaignid=*-".$currentCampaign->mailEngineCampaignid."-*";
        $buf = $this->curlCall($url,"");
        $xml = json_decode(json_encode((array) simplexml_load_string($buf)));
        $buf = $this->curlCall($url."*&limit=".$xml->total,"");
        $xml = json_decode(json_encode((array) simplexml_load_string($buf)));
        $curlString = "<contacts>";
        if (isset($xml->contacts->contact))
        {
            if (count($xml->contacts->contact) != 1)
            $contacts = $xml->contacts->contact;
            else
            $contacts = $xml->contacts;
            foreach ($contacts as $contact)
            {
                if (isset($contact->campaignid))
                {
                    //echo "test";
                    $contact->campaignid = str_replace("-".$currentCampaign->mailEngineCampaignid."-","",$contact->campaignid);
                    $curlString = $curlString."<contact>    <contactId>".$contact->contactId."</contactId><campaignid>".$contact->campaignid."</campaignid></contact>";
                }
            }
            $curlString = $curlString."</contacts>";
            $this->curlCall($this->mailEngine->url."contacts/",$curlString); 
        }
    }
    public function cancelCampaign($params,$campaign)
    {
        if ($campaign->mailEngineSendid == "")
        return -1;
        $ch = curl_init($this->mailEngine->url.$this->mailEngine->sendCampaignPath."/".$campaign->mailEngineSendid);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_exec($ch);
        curl_close($ch);
        $buf = $this->curlCall($this->mailEngine->url.$this->mailEngine->sendCampaignPath."/".$campaign->mailEngineSendid, "");
        if (substr_count($buf,"<error>Not Found</error>") > 0)
        return 1;
        else
        return -1;
    }
    public function previewTemplate($params,$campaign)
    {
        $buf =  $this->curlCall($this->mailEngine->url . $this->mailEngine->createTemplatePath."/".$campaign->mailEngineTemplateid, "");
        $xml = simplexml_load_string($buf);
        return $xml->message->htmlBody;
    }
    public function previewProperites($params,$campaign)
    {
        $buf =  $this->curlCall($this->mailEngine->url . $this->mailEngine->createTemplatePath."/".$campaign->mailEngineTemplateid, "");
        $xml = json_decode(json_encode((array)simplexml_load_string($buf)));
        $properties = new \stdClass();
        $properties->subject = $xml->message->subject;
        $buf = $this->curlCall($this->mailEngine->url . $this->mailEngine->updateCampaignPath."/".$campaign->mailEngineCampaignid, "");
        $xml = json_decode(json_encode((array)simplexml_load_string($buf)));
        $properties->fromName = $xml->campaign->fromName;
        $properties->fromEmail = $xml->campaign->fromEmail;
        return $properties;
    }
    public function campaignStatus($params,$campaign)
    {
        $properties = new \stdClass();
        $properties->sentStatus = "";
        if ($campaign->mailEngineSendid != "")
        {
            $buf = $this->curlCall($this->mailEngine->url.$this->mailEngine->sendCampaignPath."/".$campaign->mailEngineSendid,"");
            $xml = json_decode(json_encode((array)simplexml_load_string($buf)));
            if (isset($xml->send))
            {
                if ($xml->send->status == "released")
                $properties->sentStatus = "sent";
                if ($xml->send->status == "pending")
                $properties->sentStatus = "pending";
            }
        }
        return $properties;
    }
}