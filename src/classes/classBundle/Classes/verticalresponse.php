<?php
class verticalresponse 
{
    public $mailEngine;
    public $clientID;
    public $clientSecret;
    public $accessToken;
    public $userID;

    public function __construct()
    {
    }
    public function init($mailEngine)
    {
        $this->mailEngine = $mailEngine;
        $this->clientID = $this->mailEngine->clientID;
        $this->clientSecret = $this->mailEngine->clientSecret;
        $this->accessToken = $this->mailEngine->accessToken;
        $this->userID = $this->mailEngine->userID;
        $this->headers = array(
            'Authorization: ' . $this->userID . ' ' . $this->accessToken,
            'Content-Type:application/json'
        );
    }
    public function createCampaign(&$params,&$campaign,&$html,&$text)
    {
        $data = array();
        $data['name'] = $params['title'];
        $data['subject'] = $params['subject'];
        $data['message'] = $html;
        $data['company'] = $params['fromName'];
        $data['from'] = $params['fromEmail'];
        $campaign->fromEmail = $params['fromEmail'];
        
        $json = json_encode($data);
        $url = "https://vrapi.verticalresponse.com/api/v1/messages/emails";
        $buf = $this->curlCall($url, $json, 'null', 'POST');
        $response = json_decode($buf);
        $arr = explode('/', $response->url);
        $campaign->mailEngineCampaignid = end($arr);
    }
    public function updateCampaign(&$params,&$campaign)
    {
        $data = array();
        $data['name'] = $params['title'];
        $data['subject'] = $params['subject'];
        $data['message'] = $params['html'];
        $data['company'] = $params['fromName'];
        $data['from'] = $params['fromEmail'];
        
        $json = json_encode($data);
        $url = "https://vrapi.verticalresponse.com/api/v1/messages/emails/" . $campaign->mailEngineCampaignid;
        $buf = $this->curlCall($url, $json, 'null', 'PUT');
        $response = json_decode($buf);
    }
    public function createList(&$params,&$contact,&$currentCampaign,&$campaignidString) {
        $url = "https://vrapi.verticalresponse.com/api/v1/lists";
        $data = array();     
        $data['name'] = time();       
        $json = json_encode($data);  
        $buf = $this->curlCall($url, $json, 'null', 'POST');      
        $response = json_decode($buf);       
        $arr = explode('/', $response->url);      
        $currentCampaign->mailEngineListid = end($arr);       
        $url1 = "https://vrapi.verticalresponse.com/api/v1/lists/" . end($arr) . "/contacts";
        $data1 = array();
        foreach ($params['emails'] as $email) {
            $data1['email'] = $email['emailAddress'];
            $json1 = json_encode($data1);
            $buf1 = $this->curlCall($url1, $json1, 'null', 'POST');       
            $response1 = json_decode($buf1);        
            //print_r($response1);
        }     
    }
    public function sendCampaign(&$params,&$campaign)
    {
        $url = "https://vrapi.verticalresponse.com/api/v1/messages/emails/" . $campaign->mailEngineCampaignid;
        $data = array();
        $schedule = false;
        $data['list_ids'] = array($campaign->mailEngineListid);
        if (isset($params['scheduledTime'])) {
            $date = new DateTime($params['scheduledTime'], new DateTimeZone('America/Los_Angeles')); 
            $date->modify('+475 minutes');
            $data['scheduled_at'] = $date->format('Y-m-d H:i:s');
            $dbDate = $date->format('Y-m-d H:i:s');
            $schedule = true;
        }
        $json = json_encode($data);
        $buf = $this->curlCall($url, $json, 'null', 'POST');
        $response = json_decode($buf);
        if($response->success->code == 200) { 
            $url1 = "https://vrapi.verticalresponse.com/api/v1/messages/emails/" . $campaign->mailEngineCampaignid . "?type=all";
            $buf1 = $this->curlCall($url1, '', 'null', 'GET');
            $response1 = json_decode($buf1);
            if($schedule === true) {
                $sentDate = $dbDate;
            } else {
                $sentDate = $response1->attributes->scheduled_at;
            }
        } else {
            $sentDate = '0000-00-00 00:00:00';
        }
        return $sentDate;
    }
    public function getStats(&$campaign)
    {
        $url = "https://vrapi/api/v1/messages/emails/" . $campaign->mailEngineCampaignid . "/stats";
        $buf = $this->curlCall($url, '', 'null', 'GET');
        $response = json_decode($buf);
        $emailStats = new \stdClass();
        $emailStats->opens = new \stdClass();
        $emailStats->clicks = new \stdClass();
        
        $emailStats->delivered = $response->attributes->total_contacts_sent;
        $emailStats->opens->unique = $response->attributes->opens;
        $emailStats->clicks->unique = $response->attributes->unique_clicks;
        $emailStats->bounces = $response->attributes->bounces;
        $emailStats->unsubscribes = $response->attributes->unsubscribes;

        return $emailStats;
    }
    public function viewUsers($params,&$campaign)
    {
        $users = array();
        $campaignidString = '';
        $email = '';
        $first_name = '';
        $last_name = '';
        $url = "https://vrapi.verticalresponse.com/api/v1/lists/".$campaign->mailEngineListid."/contacts?type=all";
        $buf = $this->curlCall($url, '', 'null', 'GET');
        $response = json_decode($buf);
        foreach($response as $res) {
            //print_r($res);
            if(is_array($res)) {
                for($i = 0; $i < count($res); $i++) {
                    foreach($res[$i] as $val) {
                        if(is_object($val)) {
                            foreach($val as $key => $val1) {
                                //echo $key;//print_r($val1);

                                if($key == 'email') {
                                    $email = $val1;
                                    //echo $email;
                                }
                                if($key == 'first_name') {
                                    $first_name = $val1;
                                    //echo $first_name;
                                }
                                if($key == 'last_name') {
                                    $last_name = $val1;
                                    //echo $last_name;
                                }
                            }
                        }
                    }

                    $users[$email] = new \stdClass();
                    $users[$email]->email = $email;
                    $users[$email]->firstName = $first_name;
                    $users[$email]->lastName = $last_name;
                    if ($users[$email]->firstName instanceof \stdClass)
                    $users[$email]->firstName = "";
                    if ($users[$email]->lastName instanceof \stdClass)
                    $users[$email]->lastName = "";
                        
                    
                }
            }    
        }
        return $users;
    }    
    public function clearList($params,$currentCampaign,$users)
    {
    }
    public function cancelCampaign($params,$campaign)
    {
        $url = "https://vrapi.verticalresponse.com/api/v1/messages/emails/".$campaign->mailEngineCampaignid."/unschedule";
        $buf = $this->curlCall($url, '', 'null', 'POST');
        $response = json_decode($buf);
    }
    public function previewTemplate($params,$campaign)
    {
        $url = "https://vrapi.verticalresponse.com/api/v1/messages/emails/".$campaign->mailEngineCampaignid."?type=basic";
        $buf = $this->curlCall($url, '', 'null', 'GET');
        $response = json_decode($buf);
        return $response->attributes->message;
    }
    public function previewProperites($params,$campaign)
    {
        $url = "https://vrapi.verticalresponse.com/api/v1/messages/emails/".$campaign->mailEngineCampaignid."?type=all";
        $buf = $this->curlCall($url, '', 'null', 'GET');
        $response = json_decode($buf);
        $properties = new \stdClass();
        $properties->subject = $response->attributes->subject;
        $properties->fromName = $response->attributes->name;
        $properties->fromEmail = $response->attributes->from;
        return $properties;
    }
    public function curlCall($url, $data, $options = null, $method = 'POST')
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        
        if ($method == 'PUT') {
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        }
        if ($method == 'DELETE') {
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }
        if ($data != "")
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $buf = curl_exec($ch);
        curl_close($ch);
        return $buf;
    }     
}