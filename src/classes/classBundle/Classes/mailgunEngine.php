<?php
namespace classes\classBundle\Classes;
use Mailgun\Mailgun;
class mailgunEngine
{
    public $mailEngine;
    public function __construct()
    {
        $this->mgClient = new Mailgun('key-2h7ql8j154r4m-nsf93h141lob8are57');
        $this->domain = "admin.smartplanenterprise.com";
    }
    public function init($mailEngine)
    {
        $this->mailEngine = $mailEngine;
    }
    public function createCampaign(&$params,&$campaign,&$html,&$text)
    {
    }
    public function updateCampaign(&$params,&$campaign)
    {       
    }
    public function createList(&$params,&$contact,&$currentCampaign,&$campaignidString)
    {
    
    }
    public function sendCampaign(&$params,&$campaign)
    {
      $users = $this->viewUsers($params,$campaign);
      $params['html'] = $campaign->html;
      $this->mailEngine->addCampaignId($params,$campaign);
      foreach ($users as $user)
      {
        $userHtml = $params['html'];
        $userHtml = str_replace("{FIRST_NAME}",$user['firstName'],$userHtml);
        $userHtml = str_replace("{LAST_NAME}",$user['lastName'],$userHtml);
        
        $from = $campaign->fromName." <".$campaign->fromEmail.">";
        # Make the call to the client.
        $result = $this->mgClient->sendMessage($this->domain,
          array('from'    => $from,
                'h:Sender' => $from,
                'to'      => $user['email'],
                'subject' => $campaign->subject,
                'html'    => $userHtml,
                'o:tag'   => array($this->mailEngine->calculateCampaignid($campaign)),
                'o:tracking' => true,
                'o:tracking-clicks' => true,
                'o:tracking-opens' => true
                ));        
      }
      return 1;
    }
    public function sendEmail($params)
    {
        $params['h:Sender'] = $params['from'];
        if (isset($params['o:tag']))
        {
            $params['o:tracking'] = true;
            $params['o:tracking-clicks'] = true;
            $params['o:tracking-opens'] = true;
        }
        return $result = $this->mgClient->sendMessage($this->domain,$params);                     
    }
    public function clearList($params,$currentCampaign,$users)
    { 
    }
    public function getStatsViaTagId($tagid,$extraOptions = array("duration" => "1m") )
    {
        $emailStats = new \stdClass();       
        $emailStats->delivered = 0;
        $emailStats->opens = new \stdClass();
        $emailStats->opens->unique = 0;
        $emailStats->clicks = new \stdClass();
        $emailStats->clicks->unique = 0;
        $emailStats->bounces = 0;
        $emailStats->unsubscribes = 0; 
        $emailStats->undelivered = 0;
        $emailStats->sent = 0;
        try
        {
            $options = array('event' => array('accepted', 'delivered', 'failed','opened','clicked','unsubscribed','complained','stored'));
            $options = array_merge($options,$extraOptions);
            $result = $this->mgClient->get($this->domain."/tags/".$tagid."/stats",$options);             
        } 
        catch (\Exception $ex) 
        {
            return $emailStats;
        }
        if (isset($result->http_response_body->stats[0]))
        {
            foreach ($result->http_response_body->stats as $stats)
            {
                $emailStats->sent =  $emailStats->sent + $stats->accepted->total;
                $emailStats->delivered = $emailStats->delivered + $stats->delivered->total;
                if (isset($stats->opened->unique))
                $emailStats->opens->unique = $emailStats->opens->unique + $stats->opened->unique;
                if (isset($stats->clicked->unique))
                $emailStats->clicks->unique = $emailStats->clicks->unique + $stats->clicked->unique;
                $emailStats->bounces = $emailStats->bounces + $stats->failed->permanent->bounce;
                $emailStats->unsubscribes = $emailStats->unsubscribes + $stats->unsubscribed->total;
                $emailStats->undelivered = $emailStats->undelivered + abs($stats->accepted->total-$stats->delivered->total);
            }
        }
        return $emailStats;        
    }
    public function getStats(&$campaign)
    {
        return $this->getStatsViaTagId($this->mailEngine->calculateCampaignid($campaign));
    }
    public function appendStats(&$emailStats,$emailStats2)
    {
        $emailStats->sent =  $emailStats->sent +  $emailStats2->sent;
        $emailStats->delivered = $emailStats->delivered + $emailStats2->delivered ;
        if (isset($emailStats2->opens->unique))
        $emailStats->opens->unique = $emailStats->opens->unique + $emailStats2->opens->unique;
        if (isset($emailStats2->clicks->unique))
        $emailStats->clicks->unique = $emailStats->clicks->unique + $emailStats2->clicks->unique;
        $emailStats->bounces = $emailStats->bounces + $emailStats2->bounces;
        $emailStats->unsubscribes = $emailStats->unsubscribes + $emailStats2->unsubscribes;
        $emailStats->undelivered = $emailStats->undelivered + $emailStats2->undelivered;
    }
    public function viewUsers($params,&$campaign)
    {
        $connection = $this->mailEngine->dbal;
        $sql = "SELECT emailAddress as email,firstName,lastName FROM campaignsLists WHERE campaigns LIKE '%-".$campaign->id."-%'";
        return $connection->fetchAll($sql);
    }    

    public function previewTemplate($params,$campaign)
    {
        return $campaign->html;
    }
    public function previewProperites($params,$campaign)
    {
        return $campaign;
    }
    public function campaignStatus($params,$campaign)
    {
        
    }
}