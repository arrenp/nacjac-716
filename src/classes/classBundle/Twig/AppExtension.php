<?php

namespace classes\classBundle\Twig;

use Twig_Extension;
use Twig_SimpleFunction;

class AppExtension extends Twig_Extension
{
  public function getFunctions()
  {
    return array(
      new Twig_SimpleFunction('fileGetContents', array($this, 'fileGetContents') ),
    );
  }

  public function fileGetContents($file)
  {
    return file_get_contents($file);
  }

  public function getName()
  {
    return 'app_extension';
  }
}
?>