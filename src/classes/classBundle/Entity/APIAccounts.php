<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class APIAccounts
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="userid", type="integer")
	 */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="apiKey", type="string", length=100)
	 */
	public $apiKey;	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="ipaddress", type="string", length=15)
	 */
	public $ipaddress;	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="active", type="smallint")
	 */
	public $active;	   
	/**
	 * @var string
	 *
	 * @ORM\Column(name="productid", type="integer")
	 */
	public $productid;


    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
