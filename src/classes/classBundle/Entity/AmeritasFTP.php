<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AmeritasFTP
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="ip", type="string", length=25)
	 */
	public $ip;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="user", type="encrypted", length=25)
	 */
	public $user;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="pass", type="encrypted", length=50)
	 */
	public $pass;

	public function setId($id) {
	   $this->id = $id;
    }

    public function setIp($ip) {
        $this->ip = $ip;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }


}
