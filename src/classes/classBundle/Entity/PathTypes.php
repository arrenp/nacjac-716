<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
 class PathTypes extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    public $type;  
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=50)
     */
    public $description;
}
