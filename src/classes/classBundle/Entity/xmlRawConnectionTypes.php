<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * xmlRawConnectionTypes
 *
 * @ORM\Table(name="xmlRawConnectionTypes")
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class xmlRawConnectionTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="connectionType", type="string", length=255)
     */
    public $connectionType;
}

