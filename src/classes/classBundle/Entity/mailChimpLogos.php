<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * mailChimpLogos
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class mailChimpLogos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
     /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;   
    /**
     * @var integer
     *
     * @ORM\Column(name="campaignID", type="string", length = 45)
     */
    public $campaignID;
    /**
     * @var string
     *
     * @ORM\Column(name="providerLogoImage", type="string", length=255)
     */
    public $providerLogoImage;    
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorLogoImage", type="string", length=255)
     */
    public $sponsorLogoImage;    
       
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($key != "id")
            $this->$key = "";
        }
    }

}
