<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class Messages {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="messagesPanesId", type="integer")
     */
    public $messagesPanesId;
    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text" ,length = 65535)
     */
    public $message;
    /**
     * @var integer
     *
     * @ORM\Column(name="isActive", type="smallint")
     */
    public $isActive;   
        /**
     * @var string
     *
     * @ORM\Column(name="language", type="string" ,length = 45)
     */
    public $language;
}
