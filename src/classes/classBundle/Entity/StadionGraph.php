<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class StadionGraph
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer",nullable=true)
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="pathId", type="integer")
     */
    public $pathId;
    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl", type="text")
     */
    public $imageUrl; 
    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl2", type="string", length=255)
     */
    public $imageUrl2;
    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl3", type="string", length=255)
     */
    public $imageUrl3;
    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl4", type="string", length=255)
     */
    public $imageUrl4;
    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl5", type="string", length=255)
     */
    public $imageUrl5;
    /**
     * @var string
     *
     * @ORM\Column(name="keyImageUrl1", type="string", length=255)
     */
    public $keyImageUrl1;
    /**
     * @var string
     *
     * @ORM\Column(name="keyImageUrl2", type="string", length=255)
     */
    public $keyImageUrl2;
    /**
     * @var string
     *
     * @ORM\Column(name="keyImageUrl3", type="string", length=255)
     */
    public $keyImageUrl3;
    /**
     * @var string
     *
     * @ORM\Column(name="investorImageUrl", type="text")
     */
    public $investorImageUrl;        
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text")
     */
    public $type;
}