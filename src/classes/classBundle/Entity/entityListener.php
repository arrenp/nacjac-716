<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Acl\Exception\Exception;

class entityListener
{
    /**
     * @var EntityManager;
     */
    private $em;

    public function __construct(containerInterface $container)
    {
        $this->container = $container;
        $this->session = $this->container->get('session');
        $this->em = $this->container->get("doctrine");
    }

    public function postPersist($object, LifecycleEventArgs $eventArgs)
    {
        if ($this->session->get("masterid")) {

            $objectManager = $eventArgs->getEntityManager();
            $unitOfWork = $objectManager->getUnitOfWork();
            $changeSet = $unitOfWork->getOriginalEntityData($eventArgs->getObject());

            $params = array(
                'time' => (new \DateTime())->format('Y-m-d H:i:s'),
                'record' => $changeSet,
                'type' => 'INSERT',
                'section' => $this->session->get("section",""),
                'page' => $this->session->get("currentpage",""),
                'accountsUsersId' => $this->session->get("masterid"),
                'tablename' => $this->getClass($object),
                'recordId' => $this->getRecordId($object)
            );
            $this->addToAuditLog($params);
        }
    }

    public function preUpdate($object, PreUpdateEventArgs $eventArgs)
    {
        if ($this->session->get("masterid")) {
            $params = array(
                'time' => (new \DateTime())->format('Y-m-d H:i:s'),
                'record' => $eventArgs->getEntityChangeSet(),
                'type' => 'UPDATE',
                'section' => $this->session->get("section",""),
                'page' => $this->session->get("currentpage",""),
                'accountsUsersId' => $this->session->get("masterid"),
                'tablename' => $this->getClass($object),
                'recordId' => $this->getRecordId($object)
            );
            $this->addToAuditLog($params);
        }
    }

    public function preRemove($object, LifecycleEventArgs $eventArgs)
    {
        if ($this->session->get("masterid")) {
            $objectManager = $eventArgs->getEntityManager();
            $unitOfWork = $objectManager->getUnitOfWork();
            $changeSet = $unitOfWork->getOriginalEntityData($eventArgs->getObject());

            $params = array(
                'time' => (new \DateTime())->format('Y-m-d H:i:s'),
                'record' => $changeSet,
                'type' => 'DELETE',
                'section' => $this->session->get("section",""),
                'page' => $this->session->get("currentpage",""),
                'accountsUsersId' => $this->session->get("masterid"),
                'tablename' => $this->getClass($object),
                'recordId' => $this->getRecordId($object)
            );
            $this->addToAuditLog($params);
        }
    }

    private function getClass($object)
    {
        $explodename = explode("\\",get_class($object));
        $name = $explodename[count($explodename)-1]; 
        $replace = array();
        $replace['Media'] = "media";
        $replace['MediaLocalization'] = "media_localization";
        if (isset($replace[$name])) {
            $name = $replace[$name];
        }
        return $name;
    }

    /**
     * @param $params
     */
    private function addToAuditLog($params)
    {
        try {

            $logStatement = $this->em->getConnection()->prepare("insert auditLog(accountsUsersId, tablename, recordId, record, type, section, page, time) values (:accountsUsersId, :tablename, :recordId, :record, :type, :section, :page, :time)");
            $params['record'] = $this->em->getConnection()->quote(json_encode($params['record']), \PDO::PARAM_STR);
            $logStatement->execute($params);

        } catch (Exception $e) {
            // ignore and carry on
        }
    }
    
    private function getRecordId($object) {
        $em = $this->em->getManager();
        $meta = $em->getClassMetaData(get_class($object));
        try {
            $identifier = $meta->getSingleIdentifierFieldName();
        } catch (Exception $e) {
            return null;
        }
        return $object->$identifier;
    }

}