<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * accountsRoles
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class accountsRoles
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsDashboard", type="string", length=255)
     */
    public $permissionsDashboard;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsAccount", type="string", length=255)
     */
    public $permissionsAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsHome", type="string", length=255)
     */
    public $permissionsSettingsHome;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsInvestments", type="string", length=255)
     */
    public $permissionsSettingsInvestments;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsInvestmentsFunds", type="string", length=255)
     */
    public $permissionsSettingsInvestmentsFunds;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsInvestmentsPortfolios", type="string", length=255)
     */
    public $permissionsSettingsInvestmentsPortfolios;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsDocuments", type="string", length=255)
     */
    public $permissionsSettingsDocuments;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsContact", type="string", length=255)
     */
    public $permissionsSettingsContact;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsMessaging", type="string", length=255)
     */
    public $permissionsSettingsMessaging;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsVads", type="string", length=255)
     */
    public $permissionsSettingsVads;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsPrintMaterials", type="string", length=255)
     */
    public $permissionsSettingsPrintMaterials;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsAccountLevelReporting", type="string", length=255)
     */
    public $permissionsSettingsAccountLevelReporting;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsEmail", type="string", length=255)
     */
    public $permissionsSettingsEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsDeploymentUrls", type="string", length=255)
     */
    public $permissionsSettingsDeploymentUrls;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSupport", type="string", length=255)
     */
    public $permissionsSupport;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansHome", type="string", length=255)
     */
    public $permissionsPlansHome;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansPlanIdentification", type="string", length=255)
     */
    public $permissionsPlansPlanIdentification;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansBranding", type="string", length=255)
     */
    public $permissionsPlansBranding;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansModules", type="string", length=255)
     */
    public $permissionsPlansModules;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansInvestments", type="string", length=255)
     */
    public $permissionsPlansInvestments;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansInvestmentsFunds", type="string", length=255)
     */
    public $permissionsPlansInvestmentsFunds;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansInvestmentsPortfolios", type="string", length=255)
     */
    public $permissionsPlansInvestmentsPortfolios;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansQdia", type="string", length=255)
     */
    public $permissionsPlansQdia;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansAssociate", type="string", length=255)
     */
    public $permissionsPlansAssociate;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansAdvice", type="string", length=255)
     */
    public $permissionsPlansAdvice;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansMessaging", type="string", length=255)
     */
    public $permissionsPlansMessaging;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansLibrary", type="string", length=255)
     */
    public $permissionsPlansLibrary;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsLibrary", type="string", length=255)
     */
    public $permissionsSettingsLibrary;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansDocuments", type="string", length=255)
     */
    public $permissionsPlansDocuments;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansContact", type="string", length=255)
     */
    public $permissionsPlansContact;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansDeploy", type="string", length=255)
     */
    public $permissionsPlansDeploy;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansEmail", type="string", length=255)
     */
    public $permissionsPlansEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansVads", type="string", length=255)
     */
    public $permissionsPlansVads;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansPrintPromotion", type="string", length=255)
     */
    public $permissionsPlansPrintPromotion;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansProfiles", type="string", length=255)
     */
    public $permissionsPlansProfiles;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansInterface", type="string", length=255)
     */
    public $permissionsPlansInterface;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansAnalyticsCities", type="string", length=255)
     */
    public $permissionsPlansAnalyticsCities;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansAnalyticsVisitorsOverview", type="string", length=255)
     */
    public $permissionsPlansAnalyticsVisitorsOverview;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansAnalyticsPages", type="string", length=255)
     */
    public $permissionsPlansAnalyticsPages;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageIndex", type="string", length=255)
     */
    public $permissionsManageIndex;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageAccounts", type="string", length=255)
     */
    public $permissionsManageAccounts;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageRoles", type="string", length=255)
     */
    public $permissionsManageRoles;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageApi", type="string", length=255)
     */
    public $permissionsManageApi;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageOutreachPlans", type="string", length=255)
     */
    public $permissionsManageOutreachPlans;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageOutreachVwise", type="string", length=255)
     */
    public $permissionsManageOutreachVwise;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageAnalyticsProfiles", type="string", length=255)
     */
    public $permissionsManageAnalyticsProfiles;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManagePortal", type="string", length=255)
     */
    public $permissionsManagePortal;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManagePromoDocs", type="string", length=255)
     */
    public $permissionsManagePromoDocs;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageSalesPromoDocs", type="string", length=255)
     */
    public $permissionsManageSalesPromoDocs;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsEmailCampaigns", type="string", length=255)
     */
    public $permissionsEmailCampaigns;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsStatsPlanComparison", type="string", length=255)
     */
    public $permissionsStatsPlanComparison;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsStatsTickets", type="string", length=255)
     */
    public $permissionsStatsTickets;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsStatsLaunchStat", type="string", length=255)
     */
    public $permissionsStatsLaunchStat;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsStatsFiduciaryReport", type="string", length=255)
     */
    public $permissionsStatsFiduciaryReport;



    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageMedia", type="string", length=50)
     */
    public $permissionsManageMedia;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsCreateCampaign", type="string", length=255)
     */
    public $permissionsCreateCampaign;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsExportDatabase", type="string", length=10)
     */
    public $permissionsExportDatabase;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageLNCLNKeys", type="string", length=50)
     */
    public $permissionsManageLNCLNKeys;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsAccountVideoLibrary", type="string", length=10)
     */
    public $permissionsAccountVideoLibrary;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageCron", type="string", length=10)
     */
    public $permissionsManageCron;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsReportsConversionReport", type="string", length=10)
     */
    public $permissionsReportsConversionReport;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsReportsUserActivityReport", type="string", length=10)
     */
    public $permissionsReportsUserActivityReport;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsReportsPerformanceReport", type="string", length=10)
     */
    public $permissionsReportsPerformanceReport;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageGroupsAccountManagers", type="string", length=10)
     */
    public $permissionsManageGroupsAccountManagers;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageGroupsCommunicationSpecialist", type="string", length=10)
     */
    public $permissionsManageGroupsCommunicationSpecialist;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansSponsorConnect", type="string", length=10)
     */
    public $permissionsplansNavSponsorConnect;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansSponsorConnectList", type="string", length=10)
     */
    public $permissionsplansNavSponsorConnectList;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageSponsorConnect", type="string", length=10)
     */
    public $permissionsManageSponsorConnect;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansMedia", type="string", length=10)
     */
    public $permissionsPlansMedia;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansPowerView", type="string", length=10)
     */
    public $permissionsPlansPowerView;
    
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansIrio", type="string", length=10)
     */
    public $permissionsPlansIrio;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansPowerViewEmailList", type="string", length=10)
     */
    public $permissionsPlansPowerViewEmailList; 
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageReportsPowerViewPerformance", type="string", length=10)
     */
    public $permissionsManageReportsPowerViewPerformance;    
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageReportsPowerViewUserActivity", type="string", length=10)
     */
    public $permissionsManageReportsPowerViewUserActivity;   
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageReportsIrioPerformance", type="string", length=10)
     */
    public $permissionsManageReportsIrioPerformance; 
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageReportsIrioUserActivity", type="string", length=10)
     */
    public $permissionsManageReportsIrioUserActivity;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansLibraryVideos", type="string", length=10)
     */
    public $permissionsPlansLibraryVideos;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageFundGroups", type="string", length=10)
     */
    public $permissionsManageFundGroups;    
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsAuditLog", type="string", length=10)
     */
    public $permissionsAuditLog;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansRiskBasedFunds", type="string", length=10)
     */
    public $permissionsPlansRiskBasedFunds;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansInvestorProfile", type="string", length=10)
     */
    public $permissionsPlansInvestorProfile;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsInvestorProfile", type="string", length=10)
     */
    public $permissionsSettingsInvestorProfile;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsAutos", type="string", length=10)
     */
    public $permissionsSettingsAutos;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageTranslations", type="string", length=10)
     */
    public $permissionsManageTranslations;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageMessagingPanes", type="string", length=10)
     */
    public $permissionsManageMessagingPanes;
	/**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansAccessControl", type="string", length=10)
     */
    public $permissionsPlansAccessControl;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageApiUI", type="string", length=10)
     */
    public $permissionsManageApiUI;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageSystemMessages", type="string", length=10)
     */
    public $permissionsManageSystemMessages;

    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageRiskProfileCreator", type="string", length=10)
     */
    public $permissionsManageRiskProfileCreator;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageXmlRawUi", type="string", length=10)
     */
    public $permissionsManageXmlRawUi;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageAccountsRecordkeepersSrtUI", type="string", length=10)
     */
    public $permissionsManageAccountsRecordkeepersSrtUI;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageAmeritasFTP", type="string", length=10)
     */
    public $permissionsManageAmeritasFTP;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageReportsPlansAdded", type="string", length=10)
     */
    public $permissionsManageReportsPlansAdded;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageKitchenSink", type="string", length=10)
     */
    public $permissionsManageKitchenSink;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageTranslationQueue", type="string", length=10)
     */
    public $permissionsManageTranslationQueue;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansOutreachHR", type="string", length=10)
     */
    public $permissionsPlansOutreachHR;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageHDMI", type="string", length=10)
     */
    public $permissionsManageHDMI;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageReportsEmailResults", type="string", length=10)
     */
    public $permissionsManageReportsEmailResults;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansConversions", type="string", length=255)
     */
    public $permissionsPlansConversions;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsPlansAutos", type="string", length=255)
     */
    public $permissionsPlansAutos;
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsManageSurvey", type="string", length=10)
     */
    public $permissionsManageSurvey;    
    /**
     * @var string
     *
     * @ORM\Column(name="permissionsSettingsInterface", type="string", length=10)
     */
    public $permissionsSettingsInterface;    
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($this->$key != "id") $this->$key = "";
        }
    }

}
