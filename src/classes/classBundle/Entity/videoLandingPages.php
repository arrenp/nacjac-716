<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class videoLandingPages
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="html", type="text")
     */
    public $html;

    
    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($key != "id") $this->$key = "";
        }
        $this->csvExport = new ArrayCollection();
    }

}
