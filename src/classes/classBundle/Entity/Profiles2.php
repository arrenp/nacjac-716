<?php

namespace classes\classBundle\Entity;

use classes\classBundle\Entity\Profiles2\AppData;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class Profiles2 extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var integer
     *
     * @ORM\Column(name="participantid", type="integer")
     */
    public $participantid;
    /**
     * @var string
     *
     * @ORM\Column(name="sessionid", type="string", length=255)
     */
    public $sessionid;
    /**
     * @var string
     *
     * @ORM\Column(name="cookie", type="string", length=255)
     */
    public $cookie;
    /**
    * @var string
    *
    * @ORM\Column(name="appData", type="encrypted")
    */
    public $appData;
    /**
    * @var boolean
    *
    * @ORM\Column(name="enrollmentStatus", type="boolean")
    */
    public $enrollmentStatus;
    /**
    * @var datetime
    *
    * @ORM\Column(name="createDate", type="datetime")
    */
    public $createDate;
    /**
    * @var datetime
    *
    * @ORM\Column(name="updateDate", type="datetime")
    */
    public $updateDate;
    private $appDataArray;
    public function setAppData(AppData $appData)
    {
        $this->appDataArray = $appData;
        $this->appData = $this->getAppDataJson();
    }
    public function loadJson($json)
    {
        $appData = new AppData();
        $appData->loadJson($json);
        $this->setAppData($appData);
    }
    public function getAppData()
    {
        return $this->appDataArray->getFullData();
    }
    public function getAppDataJson()
    {
        return $this->appDataArray->getFullDataJson();
    }
    public function getData()
    {
        $data = clone $this;
        $data->appData = $this->getAppData();
        unset($data->appDataArray);
        return $data;
    }
    public function getAppDataArray()
    {
        return $this->appDataArray;
    }
}
