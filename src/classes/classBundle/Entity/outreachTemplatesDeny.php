<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class outreachTemplatesDeny
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="userid", type="integer")
	 */
	public $userid;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="adviserid", type="integer")
	 */
	public $adviserid;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="templateid", type="integer")
	 */
	public $templateid;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
