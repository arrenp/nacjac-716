<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AccountsBatchFileNotificationEmails {
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
	public $userid;
	/**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
	public $email;
}
