<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ProfilesAdpGraphs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id; 
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;    
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="profileid", type="integer", nullable=true)
     */
    public $profileid;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    public $description;   
    /**
     * @var string
     *
     * @ORM\Column(name="total", type="float")
     */
    public $total;    
    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    public $color; 
    /**
     * @var string
     *
     * @ORM\Column(name="hoverColor", type="string", length=255)
     */
    public $hoverColor; 
}

