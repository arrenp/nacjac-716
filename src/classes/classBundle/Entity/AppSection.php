<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use classes\classBundle\Entity\BaseEntity;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AppSection extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="pathId", type="integer")
     */
    public $pathId;
    /**
     * @var integer
     *
     * @ORM\Column(name="appSectionListId", type="integer")
     */
    public $appSectionListId;
    /**
     * @var integer
     *
     * @ORM\Column(name="sort", type="integer")
     */
    public $sort;
    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="boolean")
     */
    public $active;
    /**
     * @ORM\OneToMany(targetEntity="AppStep", mappedBy="section", cascade={"persist","remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    public $steps;
    public function __construct()
    {
        $this->active = true;
        $this->steps = new ArrayCollection();
    }
}


