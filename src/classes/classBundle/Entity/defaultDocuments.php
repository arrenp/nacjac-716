<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class defaultDocuments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=100)
     */
    public $url;
    /**
     * @var integer
     *
     * @ORM\Column(name="dateAdded", type="datetime")
     */
    public $dateAdded;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
