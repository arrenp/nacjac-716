<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class Translations {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @ORM\Column(name="content", type="text", length = 65535)
     */
    public $content;
    /**
     * @ORM\Column(name="sourceId", type="integer")
     */
    public $sourceId;
    /**
     * @ORM\Column(name="languageId", type="integer")
     */
    public $languageId;
    
    /**
     * @ORM\ManyToOne(targetEntity="TranslationSources", inversedBy="defaultTranslations", fetch="EAGER")
     * @ORM\JoinColumn(name="sourceId", referencedColumnName="id")
     */
    public $translationSource;
}
