<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApiLog
 *
 * @ORM\Table(name="api_log")
 * @ORM\Entity
 */
class ApiLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="api_user_id", type="integer")
     */
    private $apiUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="api_id", type="integer")
     */
    private $apiId;

    /**
     * @var string
     *
     * @ORM\Column(name="headers", type="encrypted", length=1024)
     */
    private $headers;

    /**
     * @var string
     *
     * @ORM\Column(name="response", type="text")
     */
    private $response;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=45)
     */
    private $ip;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;
    /**
     * @var string
     *
     * @ORM\Column(name="application", type="string",length=100)
     */
    private $application;
    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string",length=100)
     */
    private $token;

    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set apiUserId
     *
     * @param integer $apiUserId
     *
     * @return ApiLog
     */
    public function setApiUserId($apiUserId)
    {
        $this->apiUserId = $apiUserId;

        return $this;
    }

    /**
     * Get apiUserId
     *
     * @return integer
     */
    public function getApiUserId()
    {
        return $this->apiUserId;
    }

    /**
     * Set apiId
     *
     * @param integer $apiId
     *
     * @return ApiLog
     */
    public function setApiId($apiId)
    {
        $this->apiId = $apiId;

        return $this;
    }

    /**
     * Get apiId
     *
     * @return integer
     */
    public function getApiId()
    {
        return $this->apiId;
    }

    /**
     * Set action
     *
     * @param string $headers
     *
     * @return ApiLog
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set payload
     *
     * @param string $response
     *
     * @return ApiLog
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get payload
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return ApiLog
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return ApiLog
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return ApiLog
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     *
     * @return ApiLog
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
    
    /**
     * Set application
     *
     * @param string $application
     *
     * 
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }

    /**
     * Get application
     *
     * @return string
     */
    public function getApplication()
    {
        return $this->application;
    }
    
    /**
     * Set token
     *
     * @param string $token
     *
     * 
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}

