<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansModules
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;

    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;

    /**
     * @var string
     *
     * @ORM\Column(name="deferralType", type="string", length=100)
     */
    public $deferralType;

    /**
     * @var string
     *
     * @ORM\Column(name="matchingContributions", type="string", length=100)
     */
    public $matchingContributions;

    /**
     * @var string
     *
     * @ORM\Column(name="catchup", type="string", length=100)
     */
    public $catchup;

    /**
     * @var string
     *
     * @ORM\Column(name="loans", type="string", length=100)
     */
    public $loans;

    /**
     * @var string
     *
     * @ORM\Column(name="enrollment", type="string", length=100)
     */
    public $enrollment;

    /**
     * @var string
     *
     * @ORM\Column(name="vesting", type="string", length=100)
     */
    public $vesting;

    /**
     * @var string
     *
     * @ORM\Column(name="beneficiaries", type="string", length=100)
     */
    public $beneficiaries;

    /**
     * @var string
     *
     * @ORM\Column(name="riskProfileConservative", type="smallint")
     */
    public $riskProfileConservative;

    /**
     * @var string
     *
     * @ORM\Column(name="riskProfileModeratelyConservative", type="smallint")
     */
    public $riskProfileModeratelyConservative;

    /**
     * @var string
     *
     * @ORM\Column(name="riskProfileModerate", type="smallint")
     */
    public $riskProfileModerate;

    /**
     * @var string
     *
     * @ORM\Column(name="riskProfileModeratelyAggressive", type="smallint")
     */
    public $riskProfileModeratelyAggressive;

    /**
     * @var string
     *
     * @ORM\Column(name="riskProfileAggressive", type="smallint")
     */
    public $riskProfileAggressive;

    /**
     * @var string
     *
     * @ORM\Column(name="deferralFormat", type="string", length=100)
     */
    public $deferralFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="investmentSelectorDefault", type="string", length=100)
     */
    public $investmentSelectorDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="investmentSelectorRiskBasedPortfolio", type="integer")
     */
    public $investmentSelectorRiskBasedPortfolio;

    /**
     * @var string
     *
     * @ORM\Column(name="investmentSelectorTargetMaturityFund", type="integer")
     */
    public $investmentSelectorTargetMaturityFund;

    /**
     * @var string
     *
     * @ORM\Column(name="investmentSelectorCustomChoice", type="integer")
     */
    public $investmentSelectorCustomChoice;

    /**
     * @var string
     *
     * @ORM\Column(name="investmentSelectorGlidePathFund", type="integer")
     */
    public $investmentSelectorGlidePathFund;

    /**
     * @var integer
     *
     * @ORM\Column(name="minimumContribution", type="integer")
     */
    public $minimumContribution;

    /**
     * @var integer
     *
     * @ORM\Column(name="maximumContribution", type="integer")
     */
    public $maximumContribution;

    /**
     * @var integer
     *
     * @ORM\Column(name="matchingPercent", type="integer")
     */
    public $matchingPercent;

    /**
     * @var integer
     *
     * @ORM\Column(name="matchingCap", type="integer")
     */
    public $matchingCap;

    /**
     * @var integer
     *
     * @ORM\Column(name="secondaryMatchingPercent", type="integer")
     */
    public $secondaryMatchingPercent;

    /**
     * @var integer
     *
     * @ORM\Column(name="secondaryMatchingCap", type="integer")
     */
    public $secondaryMatchingCap;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=100)
     */
    public $language;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifeExpectancy", type="integer")
     */
    public $lifeExpectancy;

    /**
     * @var integer
     *
     * @ORM\Column(name="retirementAge", type="integer")
     */
    public $retirementAge;

    /**
     * @var string
     *
     * @ORM\Column(name="retirementNeedsVersion", type="string", length = 100)
     */
    public $retirementNeedsVersion;

    /**
     * @var integer
     *
     * @ORM\Column(name="percentageOfIncomeFromPlan", type="integer")
     */
    public $percentageOfIncomeFromPlan;

    /**
     * @var string
     *
     * @ORM\Column(name="replacementIncomePercent", type="float",length = 255)
     */
    public $replacementIncomePercent;

    /**
     * @var float
     *
     * @ORM\Column(name="inflation", type="float", nullable=true)
     */
    public $inflation;

    /**
     * @var float
     *
     * @ORM\Column(name="`return", type="float",nullable=true)
     */
    public $return;

    /**
     * @var boolean
     *
     * @ORM\Column(name="flexPlan", type="boolean", options={"default"=false}, nullable=true)
     */
    public $flexPlan = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="riskProfile", type="boolean", options={"default"=true}, nullable=true)
     */
    public $riskProfile = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="spousalWaiverOn", type="boolean", options={"default"=false}, nullable=true)
     */
    public $spousalWaiverOn;

    /**
     * @var string
     *
     * @ORM\Column(name="spousalWaiverURL", type="string", length = 255, nullable=true)
     */
    public $spousalWaiverURL;
    /**
     * @var string
     *
     * @ORM\Column(name="thankyouAudio", type="smallint")
     */
    public $thankyouAudio;
    /**
     * @var string
     *
     * @ORM\Column(name="languageDropdown", type="smallint")
     */
    public $languageDropdown;

    /**
     * @var string
     *
     * @ORM\Column(name="customerServiceNumber", type="string", length = 255, nullable=true)
     */
    public $customerServiceNumber;
    /**
     * @var string
     *
     * @ORM\Column(name="ECOMMModal", type="smallint")
     */
    public $ECOMMModal;
    /**
     * @var string
     *
     * @ORM\Column(name="ECOMMModalChecked", type="smallint")
     */
    public $ECOMMModalChecked;

    /**
     * @var integer
     *
     * @ORM\Column(name="retirementNeedsFlow", type="integer")
     */
    public $retirementNeedsFlow;

    /**
     * @var string
     *
     * @ORM\Column(name="pensionEstimatorURL", type="string", length=255)
     */
    public $pensionEstimatorURL;

    /**
     * @var float
     *
     * @ORM\Column(name="socialSecurityMultiplier", type="float")
     */
    public $socialSecurityMultiplier;
    /**
     * @var string
     *
     * @ORM\Column(name="riskBasedQuestionnaire", type="smallint")
     */
    public $riskBasedQuestionnaire;
    /**
     * @var string
     *
     * @ORM\Column(name="investmentsGraph", type="smallint")
     */
    public $investmentsGraph;
    /**
     * @var string
     *
     * @ORM\Column(name="ATBlueprint", type="smallint")
     */
    public $ATBlueprint;
    /**
     * @var string
     *
     * @ORM\Column(name="postTax", type="smallint")
     */
    public $postTax;  
    /**
     * @var integer
     *
     * @ORM\Column(name="minimumContributionRoth", type="integer")
     */
    public $minimumContributionRoth;

    /**
     * @var integer
     *
     * @ORM\Column(name="maximumContributionRoth", type="integer")
     */
    public $maximumContributionRoth;
    /**
     * @var integer
     *
     * @ORM\Column(name="minimumContributionPostTax", type="integer")
     */
    public $minimumContributionPostTax;

    /**
     * @var integer
     *
     * @ORM\Column(name="maximumContributionPostTax", type="integer")
     */
    public $maximumContributionPostTax;
    /**
     * @var string
     *
     * @ORM\Column(name="combinedContributionAmount", type="integer")
     */
    public $combinedContributionAmount;
    /**
     * @var string
     *
     * @ORM\Column(name="compounding", type="smallint")
     */
    public $compounding;
    /**
     * @var string
     *
     * @ORM\Column(name="rolloverWorkflow", type="smallint")
     */
    public $rolloverWorkflow;
    /**
     * @var integer
     *
     * @ORM\Column(name="maximumAnnualIncome", type="integer")
     */
    public $maximumAnnualIncome;
    /**
     * @var integer
     *
     * @ORM\Column(name="trustedContactOn", type="smallint")
     */
    public $trustedContactOn;
    /**
     * @var integer
     *
     * @ORM\Column(name="beneficiaryAutoAdd", type="smallint")
     */
    public $beneficiaryAutoAdd;
    /**
     * @var integer
     *
     * @ORM\Column(name="defaultRetirementAge", type="smallint")
     */
    public $defaultRetirementAge;
    /**
     * @var string
     *
     * @ORM\Column(name="expressInvestmentSelectorDefault", type="string", length=100)
     */
    public $expressInvestmentSelectorDefault;
    /**
     * @var integer
     *
     * @ORM\Column(name="expressPretaxDefault", type="integer")
     */
    public $expressPretaxDefault;
    /**
     * @var boolean
     *
     * @ORM\Column(name="expressPretaxEditable", type="boolean")
     */
    public $expressPretaxEditable = true;
    /**
     * @var boolean
     *
     * @ORM\Column(name="autoPlayAudio", type="boolean")
     */
    public $autoPlayAudio;
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if (!in_array($key, [
                'flexPlan',
                'riskProfile',
                'return',
                'inflation',
                'spousalWaiverOn',
                'spousalWaiverURL',
                'expressPretaxEditable'
            ]))
            {
                $this->$key = "";
            }
        }
        $this->validationObject();
    }
    function validationObject()
    {
        $this->validationOptions = 
        [
            "deferralType" =>
            [
                "roth",
                "traditional",
                "traditional+roth"
            ],
            "matchingContributions" =>
            [
                "yes",
                "no"
            ],
            "catchup" =>
            [
                "yes",
                "no"                
            ],
            "loans" =>
            [
                "yes",
                "no"                   
            ],
            "enrollment" =>
            [
                "auto_enroll",
                "emp_enroll"
            ],
            "vesting" =>
            [
                "none",
                "vest_3yr",
                "vest_5yr",
                "vest_6yr",
                "vest_gen"
            ],
            "beneficiaries" =>
            [
                "off",
                "on_mandatory",
                "on_optional"
            ],
            "deferralFormat" =>
            [
                "dollar",
                "option",
                "percent"                
            ],
            "investmentSelectorDefault" =>
            [
                "Custom Choice",
                "Risk Based Portfolio",
                "Target Maturity Fund"
            ],
            "investmentSelectorRiskBasedPortfolio" =>
            [
                0,1
            ],
            "investmentSelectorCustomChoice" =>
            [
                0,1
            ],
            "retirementNeedsFlow" =>
            [
                1,2,3,4
            ],
            "investmentSelectorTargetMaturityFund" =>
            [
                0,1
            ],
            "languageDropdown" => 
            [
                0,1
            ],
            "postTax" => 
            [
                0,1
            ],
            "investmentsGraph" =>
            [
                0,1
            ],
            "beneficiaryAutoAdd" =>
            [
                0,1
            ]
        ];
        $this->validationInt = array(
            "matchingPercent",
            "matchingCap",
            "secondaryMatchingPercent",
            "secondaryMatchingCap",
            "socialSecurityMultiplier",
            "minimumContribution",
            "maximumContribution",
            "minimumContributionRoth",
            "maximumContributionRoth",
            "minimumContributionPostTax",
            "maximumContributionPostTax",
            "combinedContributionAmount",
        );
        $this->validationLength = array("pensionEstimatorURL");
        $this->validationRange = array(
            "matchingPercent" => array("min" => 0,"max" => 100),
            "matchingCap" => array("min" => 0,"max" => 100),
            "secondaryMatchingPercent" => array("min" => 0,"max" => 100),
            "secondaryMatchingCap" => array("min" => 0,"max" => 100),
            "socialSecurityMultiplier" => array("min" => 0,"max" => 100),
            "minimumContribution" => array("min" => 0,"max" => 5),
            "maximumContribution" => array("min" => 0,"max" => 100),
            "minimumContributionRoth" => array("min" => 0,"max" => 5),
            "maximumContributionRoth" => array("min" => 0,"max" => 100),
            "minimumContributionPostTax" => array("min" => 0,"max" => 5),
            "maximumContributionPostTax" => array("min" => 0,"max" => 100),
            "combinedContributionAmount" => array("min" => 0,"max" => 100),
        );
    }

}
