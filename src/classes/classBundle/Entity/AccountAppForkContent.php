<?php


namespace classes\classBundle\Entity;

use classes\classBundle\Entity\abstractclasses\AppForkContent;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AccountAppForkContent extends AppForkContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
}