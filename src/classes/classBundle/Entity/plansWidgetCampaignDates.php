<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\rkpCampaignDates;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansWidgetCampaignDates extends rkpCampaignDates
{  
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;      
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45)
     */
    public $type; 
}


