<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class profilesRiskProfileAnswers
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="profileid", type="integer")
     */
    public $profileid; 
    /**
     * @var string
     *
     * @ORM\Column(name="questionId", type="text")
     */
    public $questionId;
    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text" )
     */
    public $answer;      
    /**
     * @var string
     *
     * @ORM\Column(name="selected", type="smallint" )
     */
    public $selected;  
    /**
     * @var string
     *
     * @ORM\Column(name="points", type="integer" )
     */
    public $points;
    /**
     * @var string
     *
     * @ORM\Column(name="thpoints", type="integer" )
     */
    public $thpoints;
}

