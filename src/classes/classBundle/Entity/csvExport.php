<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class csvExport {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="timeStamp", type="string", length=255)
     */
    public $timeStamp;
    /**
     * @var string
     *
     * @ORM\Column(name="stats", type="string", length=255)
     */
    public $stats;

   public function __construct()//do not remove, will fail for doctrine add function
	 {

		$class_vars = get_class_vars(get_class($this));
			foreach ($class_vars as $key => $value)
			{
				if ($this->$key != "id")
				$this->$key = "";

			}
			$this->csvExport = new ArrayCollection();
	}

}
