<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class RiskProfileQuestionnaireQuestion extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="riskProfileQuestionnaireId", type="integer")
     */
    public $riskProfileQuestionnaireId;
    /**
     * @var integer
     *
     * @ORM\Column(name="displayOrder", type="integer")
     */
    public $displayOrder;
    /**
     * @var integer
     *
     * @ORM\Column(name="showPoints", type="smallint")
     */
    public $showPoints;  
    public function __construct()
    {
        $this->showPoints = 0;
    }
}

