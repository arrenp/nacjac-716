<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * messages
 *
 * @ORM\Table(name="participantsTransactions")
 * @ORM\Entity
 *  @ORM\EntityListeners({"entityListener"})
 */
class ParticipantsTransactions extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
     /**
     * @var text
     *
     * @ORM\Column(name="participantid", type="string",length=255)
     */
    public $participantid;   
    /**
     * @var text
     *
     * @ORM\Column(name="date", type="datetime",nullable=true)
     */
    public $date;
    /**
     * @var text
     *
     * @ORM\Column(name="result", type="smallint")
     */
    public $result;
    /**
     * @var text
     *
     * @ORM\Column(name="type", type="text")
     */
    public $type;
     /**
     * @var text
     *
     * @ORM\Column(name="sourceApplication", type="string",length=45)
     */
    public $sourceApplication;      
    public function __construct()
    {
        $this->date = new \DateTime("now");
    }
}
