<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AppVideoLocale extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="playListId", type="integer")
     */
    public $playListId;
    /**
     * @var integer
     *
     * @ORM\Column(name="pathId", type="integer")
     */
    public $pathId;
    /**
     * @var integer
     *
     * @ORM\Column(name="videoId", type="integer")
     */
    public $videoId;    
    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer")
     */
    public $languageId;
    /**
     * @var integer
     *
     * @ORM\Column(name="url", type="text")
     */
    public $url;    
    /**
     * @var integer
     *
     * @ORM\Column(name="subtitleUrl", type="text")
     */
    public $subtitleUrl; 
}

