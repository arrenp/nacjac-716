<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class profilesRiskProfile
{
	/**
     * @ORM\ManyToOne(targetEntity="profiles", inversedBy="riskProfile")
     * @ORM\JoinColumn(name="profileid", referencedColumnName="id")
     **/
	public $profile;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer",nullable=true)
     */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planid", type="integer",nullable=true)
	 */
    public $planid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="profileid", type="integer",nullable=true)
	 */
    public $profileid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="xml", type="string",length = 31,nullable=true)
	 */
    public $xml;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="ans", type="string",length = 127,nullable=true)
	 */
    public $ans;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="position", type="smallint",nullable=true)
	 */
    public $position;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string",length = 255,nullable=true)
     */
    public $name;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text",nullable=true,length = 65535)
	 */
    public $description;
    /**
     * @var string
     *
     * @ORM\Column(name="participantsId", type="integer")
     */
    public $participantsId;
    /**
     * @var integer
     *
     * @ORM\Column(name="stadionPathId", type="smallint", nullable=true)
     */
    public $stadionPathId;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
        $this->stadionPathId = null;
	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return profilesRiskProfile
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set planid
     *
     * @param integer $planid
     *
     * @return profilesRiskProfile
     */
    public function setPlanid($planid)
    {
        $this->planid = $planid;

        return $this;
    }

    /**
     * Get planid
     *
     * @return integer
     */
    public function getPlanid()
    {
        return $this->planid;
    }

    /**
     * Set profileid
     *
     * @param integer $profileid
     *
     * @return profilesRiskProfile
     */
    public function setProfileid($profileid)
    {
        $this->profileid = $profileid;

        return $this;
    }

    /**
     * Get profileid
     *
     * @return integer
     */
    public function getProfileid()
    {
        return $this->profileid;
    }

    /**
     * Set xml
     *
     * @param string $xml
     *
     * @return profilesRiskProfile
     */
    public function setXml($xml)
    {
        $this->xml = $xml;

        return $this;
    }

    /**
     * Get xml
     *
     * @return string
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * Set ans
     *
     * @param string $ans
     *
     * @return profilesRiskProfile
     */
    public function setAns($ans)
    {
        $this->ans = $ans;

        return $this;
    }

    /**
     * Get ans
     *
     * @return string
     */
    public function getAns()
    {
        return $this->ans;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return profilesRiskProfile
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return profilesRiskProfile
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set profile
     *
     * @param \classes\classBundle\Entity\profiles $profile
     *
     * @return profilesRiskProfile
     */
    public function setProfile(\classes\classBundle\Entity\profiles $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \classes\classBundle\Entity\profiles
     */
    public function getProfile()
    {
        return $this->profile;
    }
    

    /**
     * Set name
     *
     * @param string $name
     *
     * @return profilesRiskProfile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @return string
     */
    public function getParticipantsId()
    {
        return $this->participantsId;
    }

    /**
     * @param string $spousalWaiverForm
     */
    public function setParticipantsId($participantsId)
    {
        $this->participantsId = $participantsId;
    }    
    
    /**
     * @return int
     */
    public function getStadionPathId()
    {
        return $this->stadionPathId;
    }
    
    /**
     * @param int $stadionPathId
     */
    public function setStadionPathId($stadionPathId)
    {
        $this->stadionPathId = $stadionPathId;
    }  
}
