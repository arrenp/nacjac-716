<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class profilesRiskProfileQuestions
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="profileid", type="integer")
     */
    public $profileid; 
    /**
     * @var string
     *
     * @ORM\Column(name="languageId", type="integer")
     */
    public $languageId;
    /**
     * @var string
     *
     * @ORM\Column(name="question", type="text" )
     */
    public $question;      
    /**
     * @var string
     *
     * @ORM\Column(name="used", type="smallint" )
     */
    public $used;   
    /**
     * @var string
     *
     * @ORM\Column(name="number", type="integer" )
     */
    public $number;  
}
