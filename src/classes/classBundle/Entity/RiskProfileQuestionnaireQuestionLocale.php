<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class RiskProfileQuestionnaireQuestionLocale extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="riskProfileQuestionnaireQuestionId", type="integer")
     */
    public $riskProfileQuestionnaireQuestionId;
    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer")
     */
    public $languageId;
    /**
     * @var string
     *
     * @ORM\Column(name="question", type="text")
     */
    public $question;
    /**
     * @var string
     *
     * @ORM\Column(name="audio", type="string",length=45,nullable=true)
     */
    public $audio;
}


