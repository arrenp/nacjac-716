<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class videoLandingPagesStats
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
      /**
     * @var string
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
      /**
     * @var string
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;  
     /**
     * @var string
     *
     * @ORM\Column(name="campaignid", type="string", length=255)
     */
    public $campaignid;   
     /**
     * @var string
     *
     * @ORM\Column(name="emailid", type="string", length=255)
     */
    public $emailid;     

    /**
     * @var string
     *
     * @ORM\Column(name="videoLandingPagesId", type="integer")
     */
    public $videoLandingPagesId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="mp4", type="string", length=100)
     */
    public $mp4;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="watched", type="float")
     */
    public $watched;  
    
    /**
     * @var string
     *
     * @ORM\Column(name="length", type="float")
     */
    public $length;
    /**
     * @var string
     *
     * @ORM\Column(name="played", type="smallint")
     */
    public $played;  
    /**
     * @var string
     *
     * @ORM\Column(name="complete", type="smallint")
     */
    public $complete;    
     /**
     * @var string
     *
     * @ORM\Column(name="sessionid", type="string", length=100)
     */
    public $sessionid;   
    /**
     * @var string
     *
     * @ORM\Column(name="ipaddress", type="string", length=100)
     */
    public $ipaddress;
      /**
     * @var string
     *
     * @ORM\Column(name="weight", type="text")
     */
    public $weight;  
      /**
     * @var string
     *
     * @ORM\Column(name="mousePosition", type="text")
     */
    public $mousePosition; 
    
    
        
    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($key != "id") $this->$key = "";
        }
        $this->watched = 0;
        
    }
}
