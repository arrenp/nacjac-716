<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use classes\classBundle\Entity\abstractclasses\AppColors;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AccountAppColors extends AppColors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;   
}

