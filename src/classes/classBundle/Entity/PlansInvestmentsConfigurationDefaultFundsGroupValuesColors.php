<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsConfigurationDefaultFundsGroupValuesColors;
use Doctrine\ORM\Mapping as ORM;
/**
 * PlansInvestmentsConfigurationDefaultFundsGroupValuesColors
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansInvestmentsConfigurationDefaultFundsGroupValuesColors extends InvestmentsConfigurationDefaultFundsGroupValuesColors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
}
