<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class outreachVideoLibrary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="youtubeid", type="string",length = 250)
 	 */
 	 public $youtubeid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="mp4url", type="string",length = 1000)
	 */
	 public $mp4url;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="webmurl", type="string", length=1000)
	 */
     public $webmurl;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="filename", type="string", length=250)
	 */
     public $filename;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
