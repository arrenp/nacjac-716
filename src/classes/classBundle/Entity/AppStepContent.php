<?php

namespace classes\classBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AppStepContent
{
    const TYPE_AUDIO = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    public $name;
    /**
     * @var integer
     *
     * @ORM\Column(type="smallint")
     */
    public $type;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    public $englishContent;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    public $spanishContent;
    /**
     * @ORM\ManyToOne(targetEntity="AppStep", inversedBy="contents")
     * @ORM\JoinColumn(name="stepId", referencedColumnName="id", unique=true)
     */
    public $step;
}