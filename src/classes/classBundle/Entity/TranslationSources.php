<?php

namespace classes\classBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class TranslationSources {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @ORM\Column(name="sourceKey", type="string", length=100)
     */
    public $sourceKey;
    /**
     * @ORM\Column(name="domain", type="string", length=100)
     */
    public $domain;
    
    /**
     * @ORM\OneToMany(targetEntity="DefaultTranslations", mappedBy="translationSource")
     */
    public $defaultTranslations;
    /**
     * @ORM\OneToMany(targetEntity="AccountsTranslations", mappedBy="translationSource")
     */
    public $accountsTranslations;
    /**
     * @ORM\OneToMany(targetEntity="PlansTranslations", mappedBy="translationSource")
     */
    public $plansTranslations;

    
    public function __construct() {
        $this->defaultTranslations = new ArrayCollection();
        $this->accountsTranslations = new ArrayCollection();
    }
    
}
