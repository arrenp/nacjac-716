<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use classes\classBundle\Entity\abstractclasses\AutoIncrease;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultAutoIncrease extends AutoIncrease
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;    
}


