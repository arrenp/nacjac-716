<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansLocation extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length = 255)
     */
    public $location;
    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="string", length = 255)
     */
    public $guid;
    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }
}