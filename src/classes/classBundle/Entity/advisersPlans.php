<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class advisersPlans
{
    /** 
     * 
     * @ORM\ManyToOne(targetEntity="plans", inversedBy="adviserPlans")
     * @ORM\JoinColumn(name="planid", referencedColumnName="id")
     */
    public $plan;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
	 * @var integer
	 *
	 * @ORM\Column(name="userid", type="integer")
	 *
	 *
     */
    public $userid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="adviserid", type="integer")
	 *
	 *
     */
	public $adviserid;
    /**
	 * @var integer
	 *
	 * @ORM\Column(name="planid", type="integer")
	 *
	 *
     */
	public $planid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="participantLoginWebsiteAddress", type="string", length=255)
	 */
    public $participantLoginWebsiteAddress;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($key != "plan")
     		$this->$key = "";

   		}
	}
}
