<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class CTA
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    public $title;

    /**
     * @var string
     *
     * @ORM\Column(name="controllerName", type="string", length=255)
     */
    public $controllerName;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort", type="integer")
     */
    public $sort;
    /**
     * @var string
     *
     * @ORM\Column(name="campaignDescription", type="string", length=255)
     */
    public $campaignDescription;   
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45)
     */
    public $type;     
    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            $this->$key = "";
        }
    }

}
