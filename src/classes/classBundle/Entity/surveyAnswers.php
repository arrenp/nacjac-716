<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class surveyAnswers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
     /**
     * @var text
     *
     * @ORM\Column(name="partnerid", type="string", length = 255)
     */
    public $partnerid;   
    /** 
     * @var datetime
     * 
     * @ORM\Column(type="datetime", name="createDate") 
     */
    public $createDate;   
     /** 
     * @var datetime
     * 
     * @ORM\Column(type="datetime", name="modifiedDate") 
     */
    public $modifiedDate; 
    /**
     * @var text
     *
     * @ORM\Column(name="answer", type="string", length = 255)
     */
    public $answer; 
        /**
     * @var text
     *
     * @ORM\Column(name="questionid", type="integer")
     */
    public $questionid;   
    /**
     * @var text
     *
     * @ORM\Column(name="nextQuestionId", type="integer")
     */
    public $nextQuestionId;     
    /**
     * @var text
     *
     * @ORM\Column(name="active", type="smallint")
     */
    public $active; 
    /**
     * @var text
     *
     * @ORM\Column(name="sort", type="integer")
     */
    public $sort;      
}

