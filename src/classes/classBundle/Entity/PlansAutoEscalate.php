<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use classes\classBundle\Entity\abstractclasses\AutoEscalate;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansAutoEscalate extends AutoEscalate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;    
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;   
}


