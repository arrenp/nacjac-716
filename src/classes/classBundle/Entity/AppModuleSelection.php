<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
 class AppModuleSelection extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
     /**
      * @var integer
      *
      * @ORM\Column(name="moduleid", type="integer")
      */
     public $moduleid;
    /**
     * @var string
     *
     * @ORM\Column(name="selection", type="string", length=255)
     */
    public $selection;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    public $description;
}

