<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * participants
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\EntityListeners({"entityListener"})
 */
class participants
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     * @ORM\Column(name="firstName", type="encrypted")
     */
    public $firstName;

    /**
     * @var string
     * @ORM\Column(name="lastName", type="encrypted")
     */
    public $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="legalName", type="encrypted")
     */
    public $legalName;

    /**
     * @var string
     * @ORM\Column(name="gender", type="encrypted")
     */
    public $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="encrypted")
     */
    public $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length = 50)
     */
    public $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length = 50)
     */
    public $state;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length = 10)
     */
    public $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="maritalStatus", type="encrypted")
     */
    public $maritalStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="employmentDate", type="encrypted")
     */
    public $employmentDate;

    /**
     * @var string
     * @ORM\Column(name="birthDate", type="encrypted")
     */
    public $birthDate;

    /**
     * @var string
     * @ORM\Column(name="hireDate", type="encrypted")
     */
    public $hireDate;

    /**
     * @var string
     * @ORM\Column(name="marketingOptIn", type="smallint")
     */
    public $marketingOptIn;

    /**
     * @var string
     *
     * @ORM\Column(name="employeeId", type="encrypted")
     */
    public $employeeId;

    /**
     * @var string
     * @ORM\Column(name="email", type="encrypted")
     */
    public $email;
        /**
     * @var string
     * @ORM\Column(name="email2", type="encrypted")
     */
    public $email2;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string",length = 50)
     */
    public $phone;

    /**
     * @var \DateTime
     * @ORM\Column(name="enrollmentDate", type="datetime", nullable=true)
     */
    public $enrollmentDate;

    /**
     * @var string
     *
     * @ORM\Column(name="uniqid", type="string",length = 63, nullable=true)
     */
    public $uniqid;

    /**
     * @var string
     *
     * @ORM\Column(name="defaultParticipant", type="smallint")
     */
    public $defaultParticipant; //for profiles that do not have a participant
    /**
     * @var string
     *
     * @ORM\Column(name="connection", type="string", length = 15, nullable=true)
     */
    public $connection;

    /**
     * @var string
     *
     * @ORM\Column(name="testing", type="smallint")
     */
    public $testing;

    /**
     * @var string
     *
     * @ORM\Column(name="smartEnroll", type="smallint")
     */
    public $smartEnroll;
    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length = 50)
     */
    public $mobile;
    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length = 255)
     */
    public $location;
    /**
     * @var string
     *
     * @ORM\Column(name="preferredEmail", type="encrypted")
     */
    public $preferredEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="advisorFirstName", type="string", length = 255,nullable=true)
     */
    public $advisorFirstName;
    /**
     * @var string
     *
     * @ORM\Column(name="advisorLastName", type="string", length = 255,nullable=true)
     */
    public $advisorLastName;
    /**
     * @var string
     *
     * @ORM\Column(name="mailingAddressAddress", type="encrypted")
     */
    public $mailingAddressAddress;
    /**
     * @var string
     *
     * @ORM\Column(name="mailingAddressState", type="encrypted")
     */
    public $mailingAddressState;
    /**
     * @var string
     *
     * @ORM\Column(name="mailingAddressZip", type="encrypted")
     */
    public $mailingAddressZip;
    /**
     * @var string
     *
     * @ORM\Column(name="mailingAddressCity",type="encrypted")
     */
    public $mailingAddressCity;
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            $this->$key = "";
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return participants
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return participants
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set legalName
     *
     * @param string $legalName
     *
     * @return participants
     */
    public function setLegalName($legalName)
    {
        $this->legalName = $legalName;

        return $this;
    }

    /**
     * Get legalName
     *
     * @return string
     */
    public function getLegalName()
    {
        return $this->legalName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return participants
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return participants
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return participants
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return participants
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set maritalStatus
     *
     * @param string $maritalStatus
     *
     * @return participants
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get maritalStatus
     *
     * @return string
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set employmentDate
     *
     * @param string $employmentDate
     *
     * @return participants
     */
    public function setEmploymentDate($employmentDate)
    {
        $this->employmentDate = $employmentDate;

        return $this;
    }

    /**
     * Get employmentDate
     *
     * @return string
     */
    public function getEmploymentDate()
    {
        return $this->employmentDate;
    }

    /**
     * Set birthDate
     *
     * @param string $birthDate
     *
     * @return participants
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

	/**
     * Get birthDate
     *
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

	/**
     * Set hireDate
     *
     * @param string $hireDate
     *
     * @return participants
     */
    public function setHireDate($hireDate)
    {
        $this->hireDate = $hireDate;

        return $this;
    }

    /**
     * Get hireDate
     *
     * @return string
     */
    public function getHireDate()
    {
        return $this->hireDate;
    }

	/**
     * Set marketingOptIn
     *
     * @param string $marketingOptIn
     *
     * @return participants
     */
    public function setMarketingOptIn($marketingOptIn)
    {
        $this->marketingOptIn = $marketingOptIn;

        return $this;
    }

    /**
     * Get marketingOptIn
     *
     * @return string
     */
    public function getMarketingOptIn()
    {
        return $this->marketingOptIn;
    }
    /**
     * Set employeeId
     *
     * @param string $employeeId
     *
     * @return participants
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;

        return $this;
    }

    /**
     * Get employeeId
     *
     * @return string
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return participants
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return participants
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set defaultParticipant
     *
     * @param integer $defaultParticipant
     *
     * @return participants
     */
    public function setDefaultParticipant($defaultParticipant)
    {
        $this->defaultParticipant = $defaultParticipant;

        return $this;
    }

    /**
     * Get defaultParticipant
     *
     * @return integer
     */
    public function getDefaultParticipant()
    {
        return $this->defaultParticipant;
    }

    /**
     * Set testing
     *
     * @param integer $testing
     *
     * @return participants
     */
    public function setTesting($testing)
    {
        $this->testing = $testing;

        return $this;
    }

    /**
     * Get testing
     *
     * @return integer
     */
    public function getTesting()
    {
        return $this->testing;
    }

    /**
     * Set enrollmentDate
     *
     * @param \DateTime $enrollmentDate
     *
     * @return participants
     */
    public function setEnrollmentDate($enrollmentDate)
    {
        $this->enrollmentDate = $enrollmentDate;

        return $this;
    }

    /**
     * Get enrollmentDate
     *
     * @return \DateTime
     */
    public function getEnrollmentDate()
    {
        return $this->enrollmentDate;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps()
    {
        if ($this->getEnrollmentDate() == null)
        {
            $this->setEnrollmentDate(new \DateTime('now'));
        }
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return participants
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set connection
     *
     * @param string $connection
     *
     * @return participants
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;

        return $this;
    }

    /**
     * Get connection
     *
     * @return string
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Set uniqid
     *
     * @param string $uniqid
     *
     * @return participants
     */
    public function setUniqid($uniqid)
    {
        $this->uniqid = $uniqid;

        return $this;
    }

    /**
     * Get uniqid
     *
     * @return string
     */
    public function getUniqid()
    {
        return $this->uniqid;
    }
    
    public function getMobile()
    {
        return $this->mobile;
    }
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;  
    }
    public function getLocation()
    {
        return $this->location;
    }
    public function setLocation($location)
    {
        $this->location = $location;  
    }
    public function getPreferredEmail()
    {
        return $this->preferredEmail;       
    }
    public function setPreferredEmail($preferredEmail)
    {
        $this->preferredEmail = $preferredEmail;
    }
    public function setEmail2($email2)
    {
        $this->email2 = $email2;
    }
    public function getEmail2()
    {
        return $this->email2;
    }
    public function setSmartEnroll($smartEnroll)
    {
        $this->smartEnroll = $smartEnroll;
    }
    public function getSmartEnroll()
    {
        return $this->smartEnroll;
    }
    public function setAdvisorFirstName($firstName)
    {
        $this->advisorFirstName = $firstName;

        return $this;
    }
    public function setAdvisorLastName($firstName)
    {
        $this->advisorLastName = $firstName;

        return $this;
    }
}
