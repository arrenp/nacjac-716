<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class serverConfig402Limits
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="limit402g", type="float")
	 */
    public $limit402g;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="fiftyYears", type="float")
	 */
    public $fiftyYears;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastThreeYears", type="float")
	 */
    public $lastThreeYears;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="longTermEm", type="float")
	 */
    public $longTermEm;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
