<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class surveyEmployers{
    
    
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    
     /**
     * @var string
     *
     * @ORM\Column(name="employerName", type="string", length=255)
     */
    public $employerName;
    
     /**
     * @var string
     *
     * @ORM\Column(name="employerGroupsTableId", type="string", length=45)
     */
    public $employerGroupsTableId;
    
    /**
    * @var string
    *
    * @ORM\Column(name="county", type="string", length=100)
    */
   public $county;
   
    /**
    * @var string
    *
    * @ORM\Column(name="state", type="string", length=45)
    */
   public $state;   
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45)
     */
    public $type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="string", length=45)
     */
    public $planId;    
    
    public function __construct()//do not remove, will fail for doctrine add function
    {

        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($this->$key != "id") $this->$key = "";
        }
        
    }    
    
}

