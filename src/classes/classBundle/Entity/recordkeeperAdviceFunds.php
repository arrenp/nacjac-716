<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class recordkeeperAdviceFunds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="planid", type="integer")
	 */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string",length = 200)
     */
     public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="ticker", type="string",length = 200)
     */
    public $ticker;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="cusip", type="string",length = 200)
	 */
    public $cusip;
    /**
     * @var string
     *
     * @ORM\Column(name="status", type="integer")
     */
    public $status;
    /**
     * @var string
     *
     * @ORM\Column(name="presence", type="string", length = 200)
     */
    public $presence;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($key != "id") {
                $this->$key = "";
            }
   		}
        $this->presence = "CURRENT";
	}
}
