<?php

namespace classes\classBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class TranslationQueue extends BaseEntity
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @ORM\Column(name="sourceKey", type="string", length=100)
     */
    public $sourceKey;
    /**
     * @ORM\Column(name="domain", type="string", length=100)
     */
    public $domain;
    /**
     * @ORM\Column(name="language", type="string", length=10)
     */
    public $language;    
    /**
     * @ORM\Column(name="content", type="text", length = 65535)
     */
    public $content;    
    /**
     * @ORM\Column(name="active", type="smallint")
     */
    public $active;   
    /**
     * @var string
     *
     * @ORM\Column(name="partnerid", type="string", nullable=true, length = 255)
     */    
    public $partnerid;
}
