<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * xmlRawServices
 *
 * @ORM\Table(name="xmlRawServices")
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class xmlRawServices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="connectionId", type="integer")
     */
    public $connectionId;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=255)
     */
    public $service;
}

