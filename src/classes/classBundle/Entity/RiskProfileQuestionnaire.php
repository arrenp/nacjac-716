<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class RiskProfileQuestionnaire extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string" ,length = 100)
     */
    public $name; 

    /**
     * @var string
     *
     * @ORM\Column(name="scoringXml", type="text",nullable=true)
     */
    public $scoringXml;  
    /**
     * @var string
     *
     * @ORM\Column(name="customScoringType", type="string" ,length = 50,nullable=true)
     */
    public $customScoringType; 
}

