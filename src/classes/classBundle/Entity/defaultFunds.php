<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class defaultFunds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="ticker", type="string", length=100)
     */
    public $ticker;
    /**
     * @var string
     *
     * @ORM\Column(name="fundId", type="string", length=255, nullable=true)
     */
    public $fundId;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="link", type="string", length=1000)
	 */
    public $link;
    /**
     * @var integer
     *
     * @ORM\Column(name="orderid", type="integer")
     */
    public $orderid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="qdiaDefault", type="smallint")
	 */
	public $qdiaDefault;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="qdiaVideo", type="string", length = 200)
	 */
	public $qdiaVideo;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="qdiaUrl", type="string", length = 200)
	 */
    public $qdiaUrl;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="display", type="smallint")
     */
    public $display;
    /**
    * @var integer
    *
    * @ORM\Column(name="groupValueId", type="integer")
     */
    public $groupValueId;
    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
