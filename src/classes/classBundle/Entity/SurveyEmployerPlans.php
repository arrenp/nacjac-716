<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class SurveyEmployerPlans {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="surveyEmployerId", type="integer")
     */
    public $surveyEmployerId;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45, nullable=true)
     */
    public $type;
    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="string", length=45, nullable=true)
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="county", type="string", length=100, nullable=true)
     */
    public $county;
    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=45, nullable=true)
     */
    public $state;
    /**
     * @var string
     *
     * @ORM\Column(name="partnerid", type="string", length=100,nullable=true)
     */
     public $partnerid;

}
