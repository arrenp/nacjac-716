<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class RiskProfileQuestionnaireAnswer extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="riskProfileQuestionnaireQuestionId", type="integer")
     */
    public $riskProfileQuestionnaireQuestionId;
    /**
     * @var integer
     *
     * @ORM\Column(name="displayOrder", type="integer")
     */
    public $displayOrder;
    /**
     * @var string
     *
     * @ORM\Column(name="points", type="integer")
     */
    public $points;
    /**
     * @var string
     *
     * @ORM\Column(name="thpoints", type="integer")
     */
    public $thpoints;    
    public function __construct()
    {
        $this->points = 0;
        $this->thpoints = 0;
    }
}

