<?php

namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\rkp;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansRkp extends rkp
{   
         /**
     * @var text
     *
     * @ORM\Column(name="partnerid", type="string", length = 255)
     */
    public $partnerid;   
    /**
     * @var text
     *
     * @ORM\Column(name="recordkeeperPlanid", type="string", length = 255)
     */
    public $recordkeeperPlanid; 
}