<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * fundsGroups
 *
 * @ORM\Table(name="fundsGroupsValues")
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class FundsGroupsValues extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="groupid", type="integer")
     */
    public $groupid;
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string",length=255)
     */
    public $value;
    /**
     * @var string
     *
     * @ORM\Column(name="deleted", type="smallint")
     */
    public $deleted;
    /**
     * @var string
     *
     * @ORM\Column(name="orderid", type="integer")
     */
    public $orderid;        
    
    public function __construct()
    {  
        $this->deleted = 0;
        $this->orderid = time();
    }
}
