<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class emailMasterList {
     /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="planID", type="string", length=45)
     */
    public $partnerID;
    /**
     * @var string
     *
     * @ORM\Column(name="partnerID", type="string", length=45)
     */

    public $planID;
    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=45)
     */
    public $firstName;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastName", type="string", length=45)
	 */
    public $lastName;
    /**
     * @var string 
     *
     * @ORM\Column(name="email", type="string", length=45)
     */
    public $email;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="campaignID", type="string", length=45)
     */
    public $campaignID;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="category", type="string", length=45)
     */
    public $category;


   public function __construct()//do not remove, will fail for doctrine add function
	 {

		$class_vars = get_class_vars(get_class($this));
			foreach ($class_vars as $key => $value)
			{
				if ($this->$key != "id")
				$this->$key = "";

			}
			$this->emailMasterList = new ArrayCollection();
	}

}
