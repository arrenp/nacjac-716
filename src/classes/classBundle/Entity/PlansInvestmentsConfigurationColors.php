<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsConfigurationColors;
use Doctrine\ORM\Mapping as ORM;
/**
 * PlansInvestmentsConfigurationColors
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansInvestmentsConfigurationColors extends InvestmentsConfigurationColors
{ 
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
}

