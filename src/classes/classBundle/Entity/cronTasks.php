<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class cronTasks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    public $description;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=100)
     */
    public $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="minute", type="integer")
     */
    public $minute;    

    /**
     * @var string
     *
     * @ORM\Column(name="hour", type="integer")
     */
    public $hour; 
    /**
     * @var string
     *
     * @ORM\Column(name="day", type="integer")
     */
    public $day; 
    /**
     * @var string
     *
     * @ORM\Column(name="month", type="integer")
     */
    public $month; 
    /**
     * @var string
     *
     * @ORM\Column(name="dayOfTheWeek", type="integer")
     */
    public $dayOfTheWeek; 
    /**
     * @var string
     *
     * @ORM\Column(name="active", type="smallint")
     */
    public $active;

    public function __construct()//do not remove, will fail for doctrine add function
    {
        $this->active = 0;
    }
}
