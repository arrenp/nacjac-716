<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsConfigurationFundsGroupValuesColors;
use Doctrine\ORM\Mapping as ORM;
/**
 * PlansInvestmentsConfigurationFundsGroupValuesColors
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansInvestmentsConfigurationFundsGroupValuesColors extends InvestmentsConfigurationFundsGroupValuesColors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
}
