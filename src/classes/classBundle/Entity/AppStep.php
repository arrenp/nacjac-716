<?php


namespace classes\classBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AppStep
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    public $active;
    /**
     * @var integer
     *
     * @ORM\Column(name="appSectionId", type="integer")
     */
    public $appSectionId;
    /**
     * @var integer
     *
     * @ORM\Column(name="appStepListId", type="integer")
     */
    public $appStepListId;
    /**
     * @var integer
     *
     * @ORM\Column(name="sort", type="integer")
     */
    public $sort;
    /**
     * @ORM\ManyToOne(targetEntity="AppSection", inversedBy="steps")
     * @ORM\JoinColumn(name="appSectionId", referencedColumnName="id")
     */
    public $section;
    /**
     * @ORM\ManyToOne(targetEntity="AppStepList")
     * @ORM\JoinColumn(name="appStepListId", referencedColumnName="id")
     */
    public $step;
    /**
     * @ORM\OneToMany(targetEntity="AppStepContent", mappedBy="step", cascade={"persist","remove"})
     */
    public $contents;

    public function setAppStepList(AppStepList $appStepList) {
        $this->step = $appStepList;
        $this->sort = $appStepList->defaultSort;
    }
    public function __construct()
    {
        $this->active = true;
        $this->contents = new ArrayCollection();
    }
}