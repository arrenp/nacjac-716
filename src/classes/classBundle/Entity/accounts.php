<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class accounts
{
    
    const RECORDKEEPER_VERSION_BASE = 1;
    const RECORDKEEPER_VERSION_SRT_2 = 2;
    const RECORDKEEPER_VERSION_SRT_2_MIGRATION = 3;

    /**
     * @ORM\OneToMany(targetEntity="accountsRecordkeepersQueryIds", mappedBy="account")
     * 
     */
    public $accountsRecordkeepersQueryIds;
    /**
     * @ORM\OneToMany(targetEntity="accountsUsers", mappedBy="account")
     * 
     */
    public $accountsUsers;

    /**
     * @ORM\OneToMany(targetEntity="accountsUsersEnabledAccounts", mappedBy="account")
     * 
     */
    public $accountsUsersEnabledAccounts;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    public $company;

    /**
     * @var string
     *
     * @ORM\Column(name="companyWebAddress", type="string", length=255)
     */
    public $companyWebAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="partnerid", type="string", length=100)
     */
    public $partnerid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscribedate", type="date")
     */
    public $subscribedate;

    /**
     * @var string
     *
     * @ORM\Column(name="connectionType", type="string", length=100)
     */
    private $connectionType;

    /**
     * @var string
     *
     * @ORM\Column(name="recordkeeperTpaid", type="string", length=100)
     */
    public $recordkeeperTpaid;

    /**
     * @var string
     *
     * @ORM\Column(name="recordkeeperAdviceProviderCd", type="string", length=100)
     */
    public $recordkeeperAdviceProviderCd;

    /**
     * @var string
     *
     * @ORM\Column(name="recordkeeperUrl", type="string", length=200)
     */
    public $recordkeeperUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="recordkeeperUser", type="string", length=100)
     */
    public $recordkeeperUser;

    /**
     * @var string
     *
     * @ORM\Column(name="recordkeeperPass", type="string", length=100)
     */
    public $recordkeeperPass;

    /**
     * @var string
     *
     * @ORM\Column(name="recordkeeperPortfoliosAvailable", type="smallint")
     */
    public $recordkeeperPortfoliosAvailable;

    /**
     * @var string
     *
     * @ORM\Column(name="recordkeeperRiskBasedFundsAvailable", type="smallint")
     */
    public $recordkeeperRiskBasedFundsAvailable;

    /**
     * @var integer
     *
     * @ORM\Column(name="recordkeeperVersion", type="smallint")
     */
    public $recordkeeperVersion;
    /**
     * @var string
     *
     * @ORM\Column(name="investmentsMinScore", type="integer")
     */
    public $investmentsMinScore;

    /**
     * @var string
     *
     * @ORM\Column(name="investmentsMaxScore", type="integer")
     */
    public $investmentsMaxScore;

    /**
     * @var string
     *
     * @ORM\Column(name="ACAon", type="smallint")
     */
    public $ACAon;

    /**
     * @var string
     *
     * @ORM\Column(name="MSTClientID", type="string", length = 100)
     */
    public $MSTClientID;

    /**
     * @var string
     *
     * @ORM\Column(name="adviceAvailable", type="smallint")
     */
    public $adviceAvailable;

    /**
     * @var string
     *
     * @ORM\Column(name="qdiaAvailable", type="smallint")
     */
    public $qdiaAvailable;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="smallint")
     */
    public $language;

    /**
     * @var string
     *
     * @ORM\Column(name="languagesAvailable", type="string", length = 100)
     */
    public $languagesAvailable;

    /**
     * @var string
     *
     * @ORM\Column(name="languagePrefix", type="string", length = 45)
     */
    public $languagePrefix;

    /**
     * @var string
     *
     * @ORM\Column(name="dstEncryption", type="string", length = 45)
     */
    public $dstEncryption;

    /**
     * @var string
     *
     * @ORM\Column(name="retirementNeedsVersionOnOff", type="integer")
     */
    public $retirementNeedsVersionOnOff;

    /**
     * @var string
     *
     * @ORM\Column(name="LNCLNKey", type="string", length = 50)
     */
    public $LNCLNKey;

    /**
     * @var string
     *
     * @ORM\Column(name="educateParticipantId", type="integer")
     */
    public $educateParticipantId;

    /**
     * @var string
     *
     * @ORM\Column(name="visible", type="smallint")
     */
    public $visible;

    /**
     * @var string
     *
     * @ORM\Column(name="spAvailable", type="smallint")
     */
    public $spAvailable;
    
    /**
     * @var string
     *
     * @ORM\Column(name="flexPlanAvailable", type="smallint")
     */
    public $flexPlanAvailable;    
    /**
     * @var string
     *
     * @ORM\Column(name="communicationSpecialist", type="text")
     */
    public $communicationSpecialist;    
    /**
     * @var string
     *
     * @ORM\Column(name="accountManager", type="text")
     */
    public $accountManager;       
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorConnect", type="smallint")
     */
    public $sponsorConnect;
    /**
     * @var string
     *
     * @ORM\Column(name="smartEnrollEnabled", type="smallint")
     */
    public $smartEnrollEnabled;  
    /**
     * @var string
     *
     * @ORM\Column(name="beneficiaryType", type="string", length = 45)
     */
    public $beneficiaryType;   
    /**
     * @var string
     *
     * @ORM\Column(name="smartPlanEnabled", type="smallint")
     */
    public $smartPlanEnabled; 
    /**
     * @var string
     *
     * @ORM\Column(name="irioEnabled", type="smallint")
     */
    public $irioEnabled;
    /**
     * @var string
     *
     * @ORM\Column(name="powerviewEnabled", type="smallint")
     */
    public $powerviewEnabled;
    /**
     * @var string
     *
     * @ORM\Column(name="smartEnrollClass", type="string", length = 45)
     */
    public $smartEnrollClass;
    /**
     * @var string
     *
     * @ORM\Column(name="smartEnrollPath", type="string", length = 45,nullable=true)
     */
    private $smartEnrollPath; 
    /**
     * @var string
     *
     * @ORM\Column(name="personalInvestorProfile", type="smallint")
     */
    public $personalInvestorProfile;
    /**
     * @var string
     *
     * @ORM\Column(name="rolloverWorkflowEnabled", type="smallint")
     */
    public $rolloverWorkflowEnabled;
    /**
     * @var integer
     *
     * @ORM\Column(name="translationId", type="integer", nullable=true)
     */
    public $translationId;
    /**
     * @var integer
     *
     * @ORM\Column(name="eligibleStatus", type="integer", nullable=true)
     */
    public $eligibleStatus;
    /**
     * @var integer
     *
     * @ORM\Column(name="enrolledStatus", type="integer", nullable=true)
     */
    public $enrolledStatus;
    /**
     * @var integer
     *
     * @ORM\Column(name="reportable", type="integer", nullable=true)
     */
    public $reportable;
    /**
     * @var string
     *
     * @ORM\Column(name="connectionClass", type="string", length = 100,nullable=true)
     */
    private $connectionClass;
    /**
     * @var integer
     *
     * @ORM\Column(name="trustedContactEnabled", type="smallint")
     */
    public $trustedContactEnabled;
    /**
     * @var string
     *
     * @ORM\Column(name="trustedContactServiceUrl", type="string", length=200)
     */
    public $trustedContactServiceUrl;
    /**
     * @var integer
     *
     * @ORM\Column(name="enrollmentRedirectEnabled", type="smallint")
     */
    public $enrollmentRedirectEnabled;

    /**
     * @var string
     * 
     * @ORM\Column(name="irioFromEmailName", type="string", length=200)
     */
    public $irioFromEmailName;
    /**
     * @var integer
     *
     * @ORM\Column(name="stadionManagedAccountEnabled", type="smallint")
     */
    public $stadionManagedAccountEnabled;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    public $isExcludedFromHubSpot = false;
    /**
     * @var integer
     *
     * @ORM\Column(name="standardPathId", type="integer", nullable=true)
     */
    public $standardPathId;
    /**
     * @var integer
     *
     * @ORM\Column(name="smartenrollPathId", type="integer", nullable=true)
     */
    public $smartenrollPathId;
    /**
     * @var integer
     *
     * @ORM\Column(name="smartexpressPathId", type="integer", nullable=true)
     */
    public $smartexpressPathId;
     /*
     * @var string
     * 
     * @ORM\Column(name="investmentUpdateOption", type="string", length=50)
     */
    public $investmentUpdateOption;
    /**
     * @var integer
     *
     * @ORM\Column(name="beneficiaryAudioEnabled", type="smallint")
     */
    public $beneficiaryAudioEnabled;
    /**
     * @var integer
     *
     * @ORM\Column(name="PIPWidget", type="smallint")
     */
    public $PIPWidget;
    public function __set($name, $value)
    {
        if ($name == "smartEnrollPath")
        {
            if (trim($value) == "") {
                $this->smartEnrollPath = null;
            } else {
                $this->smartEnrollPath = trim($value);
            }
        }
        else if ($name == "connectionClass")
        {
            if (trim($value) == "" )
            {
                $value = null;
            }
            else
            {
                $value = trim($value);
            }
            $this->$name = $value;
        }
        else
        {
            $this->$name = $value;
        }
    }
    public function __get($name)
    {
        $session = new Session();
        if ($name == "connectionType" && $session->has("adminPlanType") && !($session->get("adminPlanType") == "smartplan" || $session->get("adminPlanType") == ""  )  && $session->has("section") && $session->get("section") == "Plans" )
        {
            return "Educate";
        }
        return $this->$name;
    }
    public function getConnectionType()//for twig
    {
        return $this->connectionType;
    }
    public function setConnectionType($connectionType)
    {
        $this->connectionType = $connectionType;
    }
    public function getLanguagesAvailable()
    {
        return explode("|",$this->languagesAvailable);
    }
    public function __construct()//do not remove, will fail for doctrine add function
    {

        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($this->$key != "id" && $this->$key != "translationId") $this->$key = "";

            if ($this->$key == "subscribedate") $this->$key = new \DateTime("now");
        }
        $this->visible = 1;
        $this->smartPlanEnabled = 1;
        $this->smartEnrollPath = null;
        $this->accountsUsers = new ArrayCollection();
        $this->accountsUsersEnabledAccounts = new ArrayCollection();
        $this->accountsRecordkeepersQueryIds = new ArrayCollection();
        $this->trustedContactEnabled = 0;
        $this->connectionClass = null;
    }
    public function getGroups($entityClass)
    {
        global $kernel;
        $groups = array();
        $accountManagerids = explode(",",$this->accountManager);
        $repository = $kernel->getContainer()->get("doctrine")->getRepository('classesclassBundle:'.$entityClass);
        foreach ($accountManagerids as $id)
        {
           $group = $repository->findOneBy(array('id' => $id));
           if ($group != null)
           {
                $groups[] = $repository->findOneBy(array('id' => $id));
           }
        }
        return $groups;
    }
}
