<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class qdiaFunds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
	 * @var integer
	 *
	 * @ORM\Column(name="planid", type="integer")
	 */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    public $name;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="video", type="string", length = 200)
	 */
	public $video;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="url", type="string", length = 200)
	 */
    public $url;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="youtubeid", type="string", length = 200)
	 */
    public $youtubeid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="videoname", type="string", length = 200)
	 */
    public $videoname;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
