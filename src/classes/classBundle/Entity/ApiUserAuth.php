<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApiUserAuth
 *
 * @ORM\Table(name="api_user_auth")
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ApiUserAuth
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="api_user_id", type="integer")
     */
    private $apiUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="api_id", type="integer")
     */
    private $apiId;

    /**
     * @var integer
     *
     * @ORM\Column(name="enabled", type="smallint")
     */
    private $enabled;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set apiUserId
     *
     * @param integer $apiUserId
     *
     * @return ApiUserAuth
     */
    public function setApiUserId($apiUserId)
    {
        $this->apiUserId = $apiUserId;

        return $this;
    }

    /**
     * Get apiUserId
     *
     * @return integer
     */
    public function getApiUserId()
    {
        return $this->apiUserId;
    }

    /**
     * Set apiId
     *
     * @param integer $apiId
     *
     * @return ApiUserAuth
     */
    public function setApiId($apiId)
    {
        $this->apiId = $apiId;

        return $this;
    }

    /**
     * Get apiId
     *
     * @return integer
     */
    public function getApiId()
    {
        return $this->apiId;
    }

    /**
     * Set enabled
     *
     * @param integer $enabled
     *
     * @return ApiUserAuth
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return integer
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}

