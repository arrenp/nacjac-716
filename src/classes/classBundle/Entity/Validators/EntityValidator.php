<?php
namespace classes\classBundle\Entity\Validators;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Services\BaseService;
use classes\classBundle\Entity\plans;
use classes\classBundle\Entity\plansModules;
use classes\classBundle\Entity\plansMessaging;
class EntityValidator extends BaseService
{
    public function validate($object,$details = array(),$header = array())
    {
        $errors = array();
        $doctrine = $this->container->get("doctrine");
        $em = $doctrine->getManager();
        $metadata = $em->getClassMetadata(get_class($object));
        $fieldMappings = $metadata->fieldMappings;
        $table = $metadata->getTableName();
        foreach ($object->validationLength as $field)
        {
            if ($object->$field != "*DO_NOT_VALIDATE*")
            {
                $length = $fieldMappings[$field]['length'];
                if (strlen($object->$field) > $length) {
                    $errors[] = array("table"=>$table,"fieldRaw" =>$field,"field" => $header[$field],"message" => "%field% is too long, limit is ".$length);
                }
            }
        }
        foreach ($object->validationInt as $field)
        {
            if ($object->$field != "*DO_NOT_VALIDATE*")
            {
                if (filter_var($object->$field, FILTER_VALIDATE_INT) === false) {
                    $errors[] = array("table"=>$table,"fieldRaw" =>$field,"field" => $header[$field],"message" => "%field% has to be integer value, \"".$object->$field."\" found");
                }
            }
        }
        foreach ($object->validationFloat as $field)
        {
            if ($object->$field != "*DO_NOT_VALIDATE*")
            {
                if (!is_numeric($object->$field)) {
                    $errors[] = array("table"=>$table,"fieldRaw" =>$field,"field" => $header[$field],"message" => "%field% has to be number,  \"".$object->$field."\" found");
                }    
            }
        }
        foreach ($object->validationOptions as $key => $options)
        {    
            if ($object->$key != "*DO_NOT_VALIDATE*")
            {
                if (is_int($options[0]))
                {
                    if (is_numeric($object->$key))
                    $object->$key = (int)$object->$key;
                    else
                    $failed = true;
                }

                if (!in_array($object->$key,$options)  || $failed)
                {
                    $errors[] = array("table"=>$table,"fieldRaw" =>$key,"field" => $header[$key],"message" => "%field% has to be one of the following values: '".implode("','",$options)."', value \"".$object->$key."\" found");
                }
                $failed = false;
            }
        }
        foreach ($object->validationRange as $key => $range) 
        {
            if ($object->$key != "*DO_NOT_VALIDATE*")
            {   
                $failed = false;
                $messages = array();
                if (isset($range['min'])) {
                    $messages[] = "greater than or equal to {$range['min']}";
                    if (!is_numeric($object->$key) || $object->$key < $range['min']) {
                        $failed = true;
                    }
                    
                }
                if (isset($range['max'])) {
                    $messages[] = "less than or equal to {$range['max']}";
                    if (!is_numeric($object->$key) || $object->$key > $range['max']) {
                        $failed = true;
                    }
                }
                
                if ($failed) {
                    $errors[] = array("table"=>$table,"fieldRaw" =>$key,"field" => $header[$key],"message" => "%field% has to be " . implode(" and ", $messages));
                }
            }
        }
        foreach ($object->validationDateTime as $field)
        {
            try 
            {
                if ($object->$field !== "*DO_NOT_VALIDATE*")
                {
                   new \DateTime($object->$field);
                }
            } catch (\Exception $e) 
            {
                $errors[] = array("table" => $table, "fieldRaw" => $field, "field" => $header[$field], "message" => "%field% is not in date format");
            }
        }
        if ($object->validationPaths)
        {
            $types = $this->container->get("PathsService")->getPathIdSections();
            foreach ($types as $field)
            {
                if ($object->$field != "*DO_NOT_VALIDATE*" && !empty($object->$field))
                {
                    $path = $this->container->get("PathsService")->getPath(["path" => $object->$field]);
                    if(empty($path))                        
                        $errors[] = array("table" => $table, "fieldRaw" => $field, "field" => $header[$field], "message" => "%field% path does not exist");                             
                    else
                    {
                        $pathAccount = $this->container->get("PathsService")->getPathAccount(["pathId" => $path->id]);                       
                        $pathType = substr($field, 0, -6);
                        $pathTypeEntity = $this->container->get("PathsService")->getPathType(["id" => $path->pathTypeId]);
                        if ((empty($pathAccount) && !$path->general)  ||  empty($pathTypeEntity) || $pathType != $pathTypeEntity->type)
                            $errors[] = array("table" => $table, "fieldRaw" => $field, "field" => $header[$field], "message" => "%field% account does not have access to path");                             
                    }
                }
            }
        }
        foreach ($errors as &$error) {
            $error['message'] = str_replace("%field%",$error['field'],$error['message']);
        }
        $detailsString = "";
        foreach ($details['params'] as $detail)
        {
            if ($detailsString != "") 
            {
                $detailsString = $detailsString.",";
            }
            $detailsString = $detailsString.$detail['description']."=".$detail['value'];
        }
        if ($detailsString != "") 
        {
            $detailsString = "Line ".$details['line'].": (".$detailsString.") ";
        }
        $error['message'] = $detailsString.$error['message'];
        return $errors;
    }    
}

