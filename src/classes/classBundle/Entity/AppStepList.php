<?php


namespace classes\classBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AppStepList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=50)
     */
    public $path;
    /**
     * @var integer
     *
     * @ORM\Column(name="appSectionListId", type="integer")
     */
    public $appSectionListId;
    /**
     * @ORM\ManyToOne(targetEntity="AppSectionList", inversedBy="steps")
     * @ORM\JoinColumn(name="appSectionListId", referencedColumnName="id")
     */
    public $section;
    /**
     * @ORM\OneToMany(targetEntity="DefaultContent", mappedBy="appStep")
     * @ORM\OrderBy({"contentTypeId" = "ASC"})
     */
    public $contents;
    /**
     * @var integer
     *
     * @ORM\Column(name="defaultSort", type="integer")
     */
    public $defaultSort;

    public function __construct()
    {
        $this->contents = new ArrayCollection();
    }
}