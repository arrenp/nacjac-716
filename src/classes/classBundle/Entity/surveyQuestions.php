<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class surveyQuestions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
     /**
     * @var text
     *
     * @ORM\Column(name="partnerid", type="string", length = 255)
     */
    public $partnerid;   
    /** 
     * @var datetime
     * 
     * @ORM\Column(type="datetime", name="createDate") 
     */
    public $createDate;   
     /** 
     * @var datetime
     * 
     * @ORM\Column(type="datetime", name="modifiedDate") 
     */
    public $modifiedDate;  
    /**
     * @var text
     *
     * @ORM\Column(name="question", type="string", length = 255)
     */
    public $question;
    /**
     * @var text
     *
     * @ORM\Column(name="isFirst", type="smallint")
     */
    public $isFirst;
    /**
     * @var text
     *
     * @ORM\Column(name="active", type="smallint")
     */
    public $active;    
    /**
     * @var text
     *
     * @ORM\Column(name="answerType", type="string",length=20)
     */
    public $answerType;    
}