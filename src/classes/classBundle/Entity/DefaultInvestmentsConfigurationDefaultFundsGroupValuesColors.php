<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsConfigurationDefaultFundsGroupValuesColors;
use Doctrine\ORM\Mapping as ORM;
/**
 * DefaultInvestmentsConfigurationDefaultFundsGroupValuesColors
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultInvestmentsConfigurationDefaultFundsGroupValuesColors extends InvestmentsConfigurationDefaultFundsGroupValuesColors
{
}
