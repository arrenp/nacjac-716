<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class tmdfDefault
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planid", type="integer")
	 */
    public $planid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="investmentSet", type="text", length = 65535)
	 */
    public $investmentSet;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="video", type="string", length = 100)
	 */
    public $video;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="qdiaUrl", type="string", length = 100)
	 */
    public $qdiaUrl;
    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
