<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
abstract class InvestmentsScore
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid; 
    /**
     * @var integer
     *
     * @ORM\Column(name="min", type="integer")
     */
    public $min;   
    /**
     * @var integer
     *
     * @ORM\Column(name="label", type="string",length = 100,nullable=true)
     */
    public $label;      
}

