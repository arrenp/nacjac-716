<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
abstract class InvestmentsConfigurationColors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;  
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
    * @var string
    *
    * @ORM\Column(name="leftColor", type="string", length=40)
     */
    public $leftColor;
    /**
    * @var string
    *
    * @ORM\Column(name="rightColor", type="string",length = 40)
     */
    public $rightColor;
    /**
     * @var string
     *
     * @ORM\Column(name="middleColor", type="string",length = 40)
     */
    public $middleColor;
}

