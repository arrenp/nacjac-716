<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
abstract class AutoEnroll
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */    
    public $enabled;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length = 255)
     */
    public $type;
    /**
     * @var boolean
     *
     * @ORM\Column(name="defaultFund", type="boolean")
     */    
    public $defaultFund;    
    /**
     * @var boolean
     *
     * @ORM\Column(name="defaultFundBasedOnAge", type="boolean")
     */    
    public $defaultFundBasedOnAge;       
    /**
     * @var string
     *
     * @ORM\Column(name="defaultFundName", type="string")
     */    
    public $defaultFundName;
    /**
     * @var string
     *
     * @ORM\Column(name="moneyHolderFundName", type="string")
     */
    public $moneyHolderFundName;
    /**
     * @var string
     *
     * @ORM\Column(name="moneyHolderFundTicker", type="string")
     */
    public $moneyHolderFundTicker;
    /**
     * @var integer
     *
     * @ORM\Column(name="daysInMoneyHolder", type="integer")
     */
    public $daysInMoneyHolder;
    /**
     * @var string
     *
     * @ORM\Column(name="defaultFundTicker", type="string")
     */    
    public $defaultFundTicker;   
    /**
     * @var string
     *
     * @ORM\Column(name="pre", type="float")
     */    
    public $pre;
    /**
     * @var string
     *
     * @ORM\Column(name="roth", type="float")
     */    
    public $roth; 
    /**
     * @var string
     *
     * @ORM\Column(name="post", type="float")
     */    
    public $post;       
    public function deferralPercent()
    {
        if ($this->type == "percent")
        {
            return $this->pre + $this->roth + $this->post;
        }
        return 0;
    }
    public function deferralDollar()
    {
        if ($this->type == "dollar")
        {
            return $this->pre + $this->roth + $this->post;
        }
        return 0;
    }
}

