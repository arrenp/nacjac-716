<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
abstract class rkp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var text
     *
     * @ORM\Column(name="hrFirstName", type="string", length = 255)
     */
    public $hrFirstName;
    /**
     * @var text
     *
     * @ORM\Column(name="hrLastName", type="string", length = 255)
     */
    public $hrLastName;    
    /**
     * @var text
     *
     * @ORM\Column(name="hrEmail", type="string", length = 255)
     */
    public $hrEmail;
    /**
     * @var text
     *
     * @ORM\Column(name="participantLoginUrl", type="string", length = 255)
     */
    public $participantLoginUrl;
    /**
     * @var text
     *
     * @ORM\Column(name="powerViewUrl", type="string", length = 255)
     */
    public $powerViewUrl;
    /**
     * @var text
     *
     * @ORM\Column(name="vWiseSSOUrl", type="string", length = 255)
     */
    public $vWiseSSOUrl;    
    /**
     * @var text
     *
     * @ORM\Column(name="retirementAge", type="integer")
     */
    public $retirementAge;
    /**
     * @var text
     *
     * @ORM\Column(name="logo", type="string", length = 255)
     */
    public $logo;      
	/**
     * @var text
     *
     * @ORM\Column(name="deferralFormLocation", type="string", length = 255)
     */
    public $deferralFormLocation;      
	/**
     * @var text
     *
     * @ORM\Column(name="hrContactPhone", type="string", length = 255)
     */
    public $hrContactPhone;      
	/**
     * @var boolean
     *
     * @ORM\Column(name="powerViewEnabled", type="boolean", nullable=false)
     */
    public $powerViewEnabled;      
	/**
     * @var text
     *
     * @ORM\Column(name="irioEnabled", type="boolean", nullable=false)
     */
    public $irioEnabled;      
	/**
     * @var text
     *
     * @ORM\Column(name="emailAddressFrom", type="string", length = 255)
     */
    public $emailAddressFrom;      
	/**
     * @var text
     *
     * @ORM\Column(name="planName", type="string", length = 255)
     */
    public $planName; 
	/**
     * @var boolean
     *
     * @ORM\Column(name="smartPlanEnabled", type="boolean", nullable=false)
     */
    public $smartPlanEnabled;      


    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
                
            $this->$key = "";
        }
    }    
    
}
