<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
use classes\classBundle\Entity\BaseEntity;
abstract class RiskProfileQuestionnaireSelected extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="riskProfileQuestionnaireId", type="integer")
     */
    public $riskProfileQuestionnaireId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="selected", type="boolean")
     */
    public $selected;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="isStadion", type="boolean")
     */
    public $isStadion;
    /**
     * @var integer
     *
     * @ORM\Column(name="isKinetik", type="boolean")
     */
    public $isKinetik;
    /**
     * @var integer
     *
     * @ORM\Column(name="smartenrollSelected", type="boolean")
     */
    public $smartenrollSelected;
    /**
     * @var integer
     *
     * @ORM\Column(name="smartexpressSelected", type="boolean")
     */
    public $smartexpressSelected;    
    public function __construct()
    {
        $this->selected = false;
        $this->isStadion = false;
        $this->isKinetik = false;
        $this->smartenrollSelected = false;
        $this->smartexpressSelected = false;
    }
}
