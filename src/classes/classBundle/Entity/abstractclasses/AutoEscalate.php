<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
abstract class AutoEscalate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;    
    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */    
    public $enabled;    
    /**
     * @var string
     *
     * @ORM\Column(name="fundName", type="string", length = 100)
     */    
    public $fundName;     
    /**
     * @var string
     *
     * @ORM\Column(name="ticker", type="string", length = 100)
     */    
    public $ticker;    
    /**
     * @var float
     *
     * @ORM\Column(name="percent", type="float")
     */    
    public $percent;
    /**
     * @var float
     *
     * @ORM\Column(name="pre", type="float")
     */
    public $pre;
    /**
     * @var float
     *
     * @ORM\Column(name="roth", type="float")
     */
    public $roth;
    /**
     * @var float
     *
     * @ORM\Column(name="post", type="float")
     */
    public $post;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     */
    public $startDate;
    
}

