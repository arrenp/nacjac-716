<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
class rkpEmail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var text
     *
     * @ORM\Column(name="email", type="encrypted", length = 255)
     */
    public $email;    
    /**
     * @var text
     *
     * @ORM\Column(name="firstName", type="encrypted", length = 255)
     */
    public $firstName;
        /**
     * @var text
     *
     * @ORM\Column(name="lastName", type="encrypted", length = 255)
     */
    public $lastName; 
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
                
            $this->$key = "";
        }
    }       
}
