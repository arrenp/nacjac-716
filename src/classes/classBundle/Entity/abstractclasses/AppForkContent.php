<?php


namespace classes\classBundle\Entity\abstractclasses;

use classes\classBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

abstract class AppForkContent extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    public $boxOneColor;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    public $boxTwoColor;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    public $buttonOneColor;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    public $buttonTwoColor;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500)
     */
    public $customHtml;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    public $customHtmlEnabled = false;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $englishContent;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $spanishContent;

    public static $languageFields = [
        'title',
        'box_1_title',
        'box_1_description',
        'box_2_title',
        'box_2_description',
        'button_1_text',
        'button_2_text'
    ];

    public function getContent($locale = 'en')
    {
        if ($locale === 'es')
        {
            $content = $this->spanishContent;
        }
        else
        {
            $content = $this->englishContent;
        }
        $data = json_decode($content, true);
        return array_merge(array_fill_keys(self::$languageFields, ''), (array)$data);
    }

    public function setContent($data, $locale = 'en')
    {
        $data = array_intersect_key($data, array_flip(self::$languageFields));
        if ($locale === 'es')
        {
            $this->spanishContent = json_encode($data, JSON_PRETTY_PRINT);
        }
        else
        {
            $this->englishContent = json_encode($data, JSON_PRETTY_PRINT);
        }
    }

}