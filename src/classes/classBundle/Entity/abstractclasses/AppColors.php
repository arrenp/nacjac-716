<?php

namespace classes\classBundle\Entity\abstractclasses;
use classes\classBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

abstract class AppColors extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="hex1", type="string",length =50)
     */
    public $hex1;  
    /**
     * @var string
     *
     * @ORM\Column(name="hex2", type="string",length =50)
     */
    public $hex2;    
    /**
     * @var string
     *
     * @ORM\Column(name="hex3", type="string",length =50)
     */
    public $hex3;     
    /**
     * @var string
     *
     * @ORM\Column(name="hex4", type="string",length =50)
     */
    public $hex4;  
}
