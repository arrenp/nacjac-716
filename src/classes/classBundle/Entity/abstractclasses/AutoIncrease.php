<?php
namespace classes\classBundle\Entity\abstractclasses;
use Doctrine\ORM\Mapping as ORM;
abstract class AutoIncrease
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;  
    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */    
    public $enabled;        
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="frequencyEnabled", type="boolean")
     */    
    public $frequencyEnabled;    
    /**
     * @var string
     *
     * @ORM\Column(name="frequency", type="string")
     */    
    public $frequency;    
    /**
     * @var float
     *
     * @ORM\Column(name="amountLimit", type="float")
     */    
    public $amountLimit;  
    /**
     * @var float
     *
     * @ORM\Column(name="percentLimit", type="float")
     */    
    public $percentLimit;  
    /**
     * @var float
     *
     * @ORM\Column(name="timeLimit", type="float")
     */    
    public $timeLimit;    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length = 100)
     */    
    public $type; 
    /**
     * @var smallint
     *
     * @ORM\Column(name="amountDecimalPlaces", type="smallint")
     */    
    public $amountDecimalPlaces; 
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     */    
    public $startDate;
    /**
     * @var string
     *
     * @ORM\Column(name="pre", type="float")
     */
    public $pre;
    /**
     * @var string
     *
     * @ORM\Column(name="roth", type="float")
     */
    public $roth;
    /**
     * @var string
     *
     * @ORM\Column(name="post", type="float")
     */
    public $post;
    /**
     * @var string
     *
     * @ORM\Column(name="increaseCap", type="float")
     */
    public $increaseCap;
}

