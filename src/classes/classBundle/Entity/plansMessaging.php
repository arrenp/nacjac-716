<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansMessaging
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
  	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="planid", type="integer")
	 */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="disclaimer", type="text",length = 65535)
     */
    public $disclaimer;
    /**
     * @var string
     *
     * @ORM\Column(name="matching", type="text",length = 65535)
     */
    public $matching;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="autoEnroll", type="text",length = 65535)
	 */
    public $autoEnroll;
    /**
     * @var integer
     *
     * @ORM\Column(name="loans", type="text",length = 65535)
     */
    public $loans;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="vesting", type="text",length = 65535)
	 */
    public $vesting;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="targetMonthlyContribution", type="text",length = 65535)
	 */
    public $targetMonthlyContribution;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="investmentSelector", type="text",length = 65535)
	 */
    public $investmentSelector;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="portfolioSelector", type="text",length = 65535)
	 */
    public $portfolioSelector;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customChoice", type="text",length = 65535)
	 */
    public $customChoice;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="deferrals", type="text",length = 65535)
	 */
    public $deferrals;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="investorProfile", type="text",length = 65535)
	 */
    public $investorProfile;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="investorProfileDisclosure", type="text",length = 65535)
	 */
    public $investorProfileDisclosure;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="confirmation", type="text",length = 65535)
	 */
    public $confirmation;
    /**
     * @var string
     *
     * @ORM\Column(name="disclaimerProfile", type="text" ,length = 65535)
     */
    public $disclaimerProfile;
    /**
     * @var string
     *
     * @ORM\Column(name="disclaimerActive", type="smallint")
     */
    public $disclaimerActive;
    /**
     * @var string
     *
     * @ORM\Column(name="matchingActive", type="smallint")
     */
    public $matchingActive;
    /**
     * @var string
     *
     * @ORM\Column(name="autoEnrollActive", type="smallint")
     */
    public $autoEnrollActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="loansActive", type="smallint")
     */
    public $loansActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="vestingActive", type="smallint")
     */
    public $vestingActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="targetMonthlyContributionActive", type="smallint")
     */
    public $targetMonthlyContributionActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="investmentSelectorActive", type="smallint")
     */
    public $investmentSelectorActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="portfolioSelectorActive", type="smallint")
     */
    public $portfolioSelectorActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="customChoiceActive", type="smallint" )
     */
    public $customChoiceActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="deferralsActive", type="smallint")
     */
    public $deferralsActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="investorProfileActive", type="smallint")
     */
    public $investorProfileActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="investorProfileDisclosureActive", type="smallint")
     */
    public $investorProfileDisclosureActive;
    /**
     * @var integer
     *
     * @ORM\Column(name="confirmationActive", type="smallint")
     */
    public $confirmationActive;
    /**
     * @var string
     *
     * @ORM\Column(name="disclaimerProfileActive", type="smallint")
     */
    public $disclaimerProfileActive;
     /**
     * @var string
     *
     * @ORM\Column(name="beneficiary", type="text" ,length = 65535)
     */
    public $beneficiary;     
    /**
     * @var string
     *
     * @ORM\Column(name="beneficiaryActive", type="smallint")
     */
    public $beneficiaryActive;
    /**
     * @var string
     *
     * @ORM\Column(name="thankyou", type="text" ,length = 65535)
     */
    public $thankyou;

    /**
     * @var string
     *
     * @ORM\Column(name="thankyouActive", type="smallint")
     */
    public $thankyouActive;

    /**
     * @var string
     *
     * @ORM\Column(name="customReview", type="text" ,length = 65535)
     */
    public $customReview;

    /**
     * @var string
     *
     * @ORM\Column(name="customReviewActive", type="smallint")
     */
    public $customReviewActive;


    /**
     * @var string
     *
     * @ORM\Column(name="targetDateReview", type="text" ,length = 65535)
     */
    public $targetDateReview;

    /**
     * @var string
     *
     * @ORM\Column(name="targetDateReviewActive", type="smallint")
     */
    public $targetDateReviewActive;


    /**
     * @var string
     *
     * @ORM\Column(name="riskBasedReview", type="text" ,length = 65535)
     */
    public $riskBasedReview;

    /**
     * @var string
     *
     * @ORM\Column(name="riskBasedReviewActive", type="smallint")
     */
    public $riskBasedReviewActive;
    
    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
                $this->validationObject();
	}
    function validationObject()
    {
        $class_vars = get_class_vars(get_class($this));
        $this->validationLength = array();
        $this->validationOptions = array();
        foreach ($class_vars as $key => $value)
        {
            if (!in_array($key,array("id","userid","planid")))
            {
                $compare = "Active";
                if(substr_compare( $key, $compare,-strlen($compare),strlen($compare)) == 0)
                $this->validationOptions[$key] = [0,1];
                else
                $this->validationLength[] = $key;
            }           
        }
    }

}
