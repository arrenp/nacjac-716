<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * fundsGroups
 *
 * @ORM\Table(name="fundsGroups")
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class FundsGroups extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=100)
     */
    public $tag;
    /**
     * @var string
     *
     * @ORM\Column(name="deleted", type="smallint")
     */
    public $deleted;   
    /**
     * @var string
     *
     * @ORM\Column(name="orderid", type="integer")
     */
    public $orderid;    
    
    public function __construct()
    {  
        $this->deleted = 0;
    }    
}
