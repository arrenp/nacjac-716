<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class profilesAutoIncrease {
    /**
     * @ORM\ManyToOne(targetEntity="profiles", inversedBy="autoIncrease")
     * @ORM\JoinColumn(name="profileid", referencedColumnName="id")
     **/
	public $profile;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer", nullable=true)
     */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planid", type="integer", nullable=true)
	 */
    public $planid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="profileid", type="integer", nullable=true)
	 */
    public $profileid;
    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", scale = 2, precision = 18, nullable=true)
     */
    public $amount; 
    /**
	 * @var string
	 *
	 * @ORM\Column(name="mode", type="string", length = 15, nullable=true)
	 */
    public $mode;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="source", type="string", length = 15, nullable=true)
	 */
    public $source;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="frequency", type="string", length = 15, nullable=true)
	 */
    public $frequency;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     */
    public $startDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime", nullable=true)
     */
    public $endDate;
    /**
	 * @var string
	 *
	 * @ORM\Column(name="occurrences", type="string", length = 15, nullable=true)
	 */
    public $occurrences;
    
}
