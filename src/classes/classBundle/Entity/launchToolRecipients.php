<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class launchToolRecipients
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="partnerid", type="string", length=45)
     */
    public $partnerid;
    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="encrypted", length=45)
     */
    public $firstName;  
    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="encrypted", length=45)
     */
    public $lastName;     
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="encrypted", length=45)
     */
    public $email;  
    /**
     * @var string
     *
     * @ORM\Column(name="uploadDate", type="datetime")
     */
    public $uploadDate;      
    /**
    * @var integer
    *
    * @ORM\Column(name="planid", type="integer")
    */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="recordkeeperPlanid", type="string", length=50)
     */
    public $recordkeeperPlanid;  
    /**
     * @var string
     *
     * @ORM\Column(name="enrolled", type="string", length=45)
     */
    public $enrolled;  
    /**
     * @var string
     *
     * @ORM\Column(name="smartEnrollPartId", type="string", length=25)
     */
    public $smartEnrollPartId;
    /**
     * @var string
     *
     * @ORM\Column(name="deleted", type="smallint")
     */
    public $deleted;
    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=10)
     */
    public $contactStatus;
    /**
     * @var integer
     * 
     * @ORM\Column(type="integer")
     */
    public $lastModified;

    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
             $this->$key = "";
             $this->uploadDate = new \dateTime('now');

        }
        $this->smartEnrollPartId = uniqid("", true);
        $this->deleted = 0;
        $this->contactStatus = "NEW";
        $this->lastModified = time();
    }
}