<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class planuploadHeaders 
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string",length=100)
     */
    public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="header", type="text")
     */
    public $header;
    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string",length=100)
     */
    public $category;
    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string",length=100)
     */
    public $action;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string",length=100)
     */
    public $type;     
    
}
