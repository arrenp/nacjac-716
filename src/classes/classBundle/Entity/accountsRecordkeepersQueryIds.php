<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * accountsRecordkeepersQueryIds
 *
 * @ORM\Table()
 * @ORM\Entity
 *  @ORM\EntityListeners({"entityListener"})
 */
class accountsRecordkeepersQueryIds {
    /**
     * @ORM\ManyToOne(targetEntity="accounts")
     * @ORM\JoinColumn(name="userid", referencedColumnName="id")
     */
    public $account;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=25)
     */
    public $type;
    /**
     * @var string
     *
     * @ORM\Column(name="queryid", type="string", length=100)
     */
    public $queryid;
}
