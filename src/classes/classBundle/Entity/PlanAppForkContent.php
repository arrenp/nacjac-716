<?php


namespace classes\classBundle\Entity;

use classes\classBundle\Entity\abstractclasses\AppForkContent;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlanAppForkContent extends AppForkContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    public $published = false;
}