<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plans extends BaseEntity
{
	
    /**
     * @ORM\OneToMany(targetEntity="accountsUsersPlans", mappedBy="plan")
     * 
     */
    public $plans;    

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
  /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
  public $userid;
  /**
   * @var string
   *
   * @ORM\Column(name="planid", type="string", length=50) 
  */
    public $planid;
    /**
     * @var integer
     *
     * @ORM\Column(name="appVersion", type="integer")
     */
    public $appVersion;
  /**
   * @var string
   *
   * @ORM\Column(name="partnerid", type="string", length=100)
   */
    public $partnerid;
  /**
   * @var string
   *
   * @ORM\Column(name="activePlanid", type="string", length=255)
   */
    public $activePlanid;
  /**
   * @var string
   *
   * @ORM\Column(name="activePlantypecd", type="string", length=255)
   */
    public $activePlantypecd;
    /**
     * @var integer
     *
     * @ORM\Column(name="lastModified", type="integer")
     */
    public $lastModified;    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    public $company;
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50)
     */
    public $phone;
    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=50)
     */
    public $phone2;
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    public $address;
    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255)
     */
    public $address2;
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    public $city;
    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255)
     */
    public $state;
    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=10)
     */
    public $zip;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    public $email;
    /**
     * @var string
     *
     * @ORM\Column(name="redirectWebsiteAddress", type="string", length=255)
     */
    public $redirectWebsiteAddress;
	/**
     * @var string
     *
     * @ORM\Column(name="timeoutWebsiteAddress", type="string", length=255)
     */
    public $timeoutWebsiteAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="participantLoginWebsiteAddress", type="string", length=255)
     */
    public $participantLoginWebsiteAddress;
    /**
     * @var string
     *
     * @ORM\Column(name="errorNotificationEmail", type="string", length=255)
     */
    public $errorNotificationEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="profileNotificationEmail", type="string", length=255)
     */
    public $profileNotificationEmail;
  /**
   * @var string
   *
   * @ORM\Column(name="youtubePlayer", type="integer")
   */
    public $youtubePlayer;
    /**
     * @var string
     *
     * @ORM\Column(name="defaultEmailAddress", type="string", length=255)
     */
    public $defaultEmailAddress;
    /**
     * @var string
     *
     * @ORM\Column(name="providerLogoImage", type="string", length=255)
     */
    public $providerLogoImage;
    /**
     * @var string
     *
     * @ORM\Column(name="providerLogoImageThumb", type="string", length=255)
     */
    public $providerLogoImageThumb;
    /**
     * @var string
     *
     * @ORM\Column(name="providerLogoEmailImage", type="string", length=255)
     */
    public $providerLogoEmailImage;
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorLogoEmailImage", type="string", length=255)
     */
    public $sponsorLogoEmailImage;    

    /**
     * @var string
     *
     * @ORM\Column(name="sponsorLogoImage", type="string", length=255)
     */
    public $sponsorLogoImage;
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorLogoImageThumb", type="string", length=255)
     */
    public $sponsorLogoImageThumb;
    /**
     * @var string
     *
     * @ORM\Column(name="bannerImage", type="string", length=255)
     */
    public $bannerImage;
    /**
     * @var string
     *
     * @ORM\Column(name="bannerLinkUrl", type="string", length=255)
     */
    public $bannerLinkUrl;
    /**
     * @var string
     *
     * @ORM\Column(name="interfaceColor", type="string", length=255)
     */
    public $interfaceColor;
    /**
     * @var string
     *
     * @ORM\Column(name="interfaceLayout", type="string", length=255)
     */
    public $interfaceLayout;
    /**
     * @var string
     *
     * @ORM\Column(name="advice", type="text",length = 65535)
     */
    public $advice;
    /**
     * @var string
     *
     * @ORM\Column(name="adviceStatus", type="smallint")
     */
    public $adviceStatus;
     /**
     * @var string
     *
     * @ORM\Column(name="adviceCompare", type="text",length = 65535)
     */
    public $adviceCompare;
  /**
   * @var string
   *
   * @ORM\Column(name="adviceDate", type="datetime")
   */
    public $adviceDate;
  /**
   * @var string
   *
   * @ORM\Column(name="riskType", type="string", length=10)
   */
    public $riskType;
  /**
   * @var string
   *
   * @ORM\Column(name="fundOrder", type="string", length=50)
   */
    public $fundOrder;
  /**
   * @var string
   *
   * @ORM\Column(name="portfolioOrder", type="string", length=50)
   */
    public $portfolioOrder;
  /**
   * @var string
   *
   * @ORM\Column(name="investmentsAutoScoring", type="smallint", length=50)
   */
    public $investmentsAutoScoring;
  /**
   * @var string
   *
   * @ORM\Column(name="vadFrequency", type="string", length=50)
   */
  public $vadFrequency;
  /**
   * @var string
   *
   * @ORM\Column(name="MSTClientSessionID", type="string", length=100)
   */
  public $MSTClientSessionID;
  /**
   * @var string
   *
   * @ORM\Column(name="fundLink", type="smallint")
   */
  public $fundLink;
  /**
   * @var string
   *
   * @ORM\Column(name="IRSCode", type="string",length = 20)
   */
  public $IRSCode;
  /**
   * @var string
   *
   * @ORM\Column(name="defaultInvestment", type="smallint")
   */
  public $defaultInvestment;
  /**
   * @var string
   *
   * @ORM\Column(name="deferralOff", type="smallint")
   */
  public $deferralOff;
  /**
   * @var string
   *
   * @ORM\Column(name="pipContent", type="smallint")
   */
  public $pipContent;
  /**
   * @var string
   *
   * @ORM\Column(name="pipPosition", type="smallint")
   */
  public $pipPosition;
  /**
   * @var string
   *
   * @ORM\Column(name="matchPercent", type="integer")
   */
  public $matchPercent;
  /**
   * @var string
   *
   * @ORM\Column(name="secondaryMatchCap", type="integer")
   */
  public $secondaryMatchCap;
  /**
   * @var string
   *
   * @ORM\Column(name="secondaryMatchPercent", type="integer")
   */
  public $secondaryMatchPercent;
  /**
   * @var string
   *
   * @ORM\Column(name="morningstar", type="integer")
   */
  public $morningstar;
  /**
   * @var string
   *
   * @ORM\Column(name="deleted", type="smallint")
   */
  public $deleted;
  /**
   * @var string
   *
   * @ORM\Column(name="createdDate", type="datetime")
   */
  public $createdDate;  
  /**
   * @var string
   *
   * @ORM\Column(name="modifiedDate", type="datetime")
   */
  public $modifiedDate;
  /**
   * @var string
   *
   * @ORM\Column(name="deletedDate", type="datetime")
   */
  public $deletedDate;  
    /**
     * @var string
     *
     * @ORM\Column(name="sections", type="string",length = 100)
     */
    public $sections;
    /**
     * @var string
     *
     * @ORM\Column(name="reportsStylesHeader", type="string",length = 20)
     */    
    public $reportsStylesHeader;
    /**
     * @var string
     *
     * @ORM\Column(name="reportsStylesFont", type="string",length = 20)
     */    
    public $reportsStylesFont;
    /**
     * @var string
     *
     * @ORM\Column(name="reportsInnerTable", type="string",length = 20)
     */    
    public $reportsInnerTable;
    /**
     * @var string
     *
     * @ORM\Column(name="reportsBackground", type="string",length = 20)
     */    
    public $reportsBackground;  
    /**
     * @var string
     *
     * @ORM\Column(name="subplan", type="smallint")
     */
    public $subplan; 
    /**
     * @var string
     *
     * @ORM\Column(name="masterplanid", type="string", length=50)
     */
    public $masterplanid;
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorConnect", type="smallint")
     */
    public $sponsorConnect; 
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorConnectTemplate", type="text")
     */    
    public $sponsorConnectTemplate;   
    /**
     * @var string
     *
     * @ORM\Column(name="investorProfileEmail", type="string", length = 255)
     */
    public $investorProfileEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length = 20)
     */
    private $type;
    /**
     * @var string
     *
     * @ORM\Column(name="advisorFirst", type="string", length = 100)
     */
    public $advisorFirst;
    /**
     * @var string
     *
     * @ORM\Column(name="advisorLast", type="string", length = 100)
     */
    public $advisorLast; 
    /**
     * @var string
     *
     * @ORM\Column(name="advisorEmail", type="string", length = 100)
     */
    public $advisorEmail; 
    /**
     * @var string
     *
     * @ORM\Column(name="advisorPhone", type="string", length = 100)
     */
    public $advisorPhone;
    /**
     * @var string
     *
     * @ORM\Column(name="smartEnrollAllowed", type="smallint")
     */
    public $smartEnrollAllowed;
     /**
     * @var string
     *
     * @ORM\Column(name="smartPlanAllowed", type="smallint")
     */
    public $smartPlanAllowed; 
    /**
     * @var string
     *
     * @ORM\Column(name="irioAllowed", type="smallint")
     */
    public $irioAllowed;
    /**
     * @var string
     *
     * @ORM\Column(name="powerviewAllowed", type="smallint")
     */
    public $powerviewAllowed;      
    /**
    * @var integer
    *
    * @ORM\Column(name="fundGroupid", type="integer")
     */
    public $fundGroupid;
    /**
     * @var integer
     *
     * @ORM\Column(name="profileNotificationShowEmail", type="integer")
     */
    public $profileNotificationShowEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="HRFirst", type="string", length = 255)
     */
    public $HRFirst;
    /**
     * @var string
     *
     * @ORM\Column(name="HRLast", type="string", length = 255)
     */
    public $HRLast;
    /**
     * @var string
     *
     * @ORM\Column(name="HREmail", type="string", length = 255)
     */
    public $HREmail;
    /**
     * @var string
     *
     * @ORM\Column(name="participantDetails", type="integer")
     */
    public $participantDetails;
	/**
     * @var integer
     *
     * @ORM\Column(name="isHomePlan", type="smallint")
     */
	public $isHomePlan;
    /**
     * @var integer
     *
     * @ORM\Column(name="enrollmentRedirectOn", type="smallint")
     */
    public $enrollmentRedirectOn;
    /**
     * @var string
     *
     * @ORM\Column(name="enrollmentRedirectAddress", type="string", length=255)
     */
    public $enrollmentRedirectAddress;
    
    /**
     * @var string
     *
     * @ORM\Column(name="supportContactFirstName", type="string", length=255)
     */
    public $supportContactFirstName;
    /**
     * @var string
     *
     * @ORM\Column(name="supportContactLastName", type="string", length=255)
     */
    public $supportContactLastName;
    /**
     * @var string
     *
     * @ORM\Column(name="supportContactEmail", type="string", length=255)
     */
    public $supportContactEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="customerServiceNumber", type="string", length=50)
     */
    public $customerServiceNumber;
    /**
    * @var string
    *
    * @ORM\Column(name="lastDayForLoans", type="datetime", nullable=true)
    */
    public $lastDayForLoans;
    /**
    * @var string
    *
    * @ORM\Column(name="freezeTransactionsDate", type="datetime", nullable=true)
    */
    public $freezeTransactionsDate;
    /**
    * @var string
    *
    * @ORM\Column(name="takeoverDate", type="datetime", nullable=true)
    */
    public $takeoverDate;
    /**
    * @var string
    *
    * @ORM\Column(name="freezeEndDate", type="datetime", nullable=true)
    */
    public $freezeEndDate;
    /**
     * @var string
     *
     * @ORM\Column(name="soxNoticeURL", type="string", length=255)
     */
    public $soxNoticeURL;     
    /**
     * @var bool
     * 
     * @ORM\Column(name="transitionPeriodOver", type="boolean")
     */
    public $transitionPeriodOver;
    /**
     * @var string
     *
     * @ORM\Column(name="initialPlan", type="boolean")
     */
    public $initialPlan;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    public $isExcludedFromHubSpot = false;
    /**
     * @var integer
     *
     * @ORM\Column(name="standardPathId", type="integer", nullable=true)
     */
    public $standardPathId;
    /**
     * @var integer
     *
     * @ORM\Column(name="smartenrollPathId", type="integer", nullable=true)
     */
    public $smartenrollPathId;
    /**
     * @var integer
     *
     * @ORM\Column(name="smartexpressPathId", type="integer", nullable=true)
     */
    public $smartexpressPathId;
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
              $this->$key = "";
              if ($key == "planid")
              {
                  $salt = uniqid(mt_rand());  
                  $this->$key = "|".substr(hash('sha1', $salt.rand(1,1000000)),0,12);
              }
        }
        $this->adviceDate = new \DateTime("0000-00-00");
        $this->lastDayForLoans = $this->freezeTransactionsDate = $this->takeoverDate = $this->freezeEndDate = null;
        $this->transitionPeriodOver = false;
        $this->name = "new plan";
        $this->plans = new ArrayCollection();
        $this->validationObject();
    }
  
    public function validationObject()
    {
      $this->validationDateTime = ["lastDayForLoans","freezeTransactionsDate","takeoverDate","freezeEndDate"];
      $this->validationLength = array("name","redirectWebsiteAddress","errorNotificationEmail"
      ,"profileNotificationEmail","timeoutWebsiteAddress","supportContactFirstName","supportContactLastName","supportContactEmail","customerServiceNumber","soxNoticeURL"); 
      $this->validationOptions = 
      [
          "interfaceColor" =>
          [
              "abgil",
              "ascensus",
              "blue",
              "burgundy",
              "charcoal",
              "gold",
              "green",
              "LNCLN",
              "red"
          ],
          "interfaceLayout" =>
          [
              "side",
              "top",
              "bottom"
          ],
          "riskType" =>
          [
              "RBF",
              "RBP"
          ],
          "fundLink" =>
          [
              0,1
          ],
          "IRSCode" =>
          [
              "401a",
              "401k",
              "403b",
              "457"
          ],
          "pipContent" =>
          [
              0,1,2,3,4,5
          ],
          "pipPosition" =>
          [
              0,1,2
          ],
          "sections" =>
          [
              "riskProfileAA",
              "riskProfileABGIL",
              "riskProfileAXA",
              "riskProfileAXA2013",
              "riskProfileAXA2014_Ibbotson",
              "riskProfileLNCLN7",
              "riskProfileLW",
              "riskProfileS401k",
              "riskProfileSPA6",
              "riskProfileSPE",
              "riskProfileSPE6",
              "riskProfileWIL6",
              "riskProfileADP"
          ],
          "sponsorConnect" =>
          [
              0,1
          ],
          "smartEnrollAllowed" =>
          [
              0,1
          ],
          "smartPlanAllowed" => 
          [
              0,1
          ],
          "irioAllowed" => 
          [
              0,1
          ],
          "powerviewAllowed" =>
          [
              0,1
          ],
          "enrollmentRedirectOn" =>
          [
              0,1
          ]
      ];
      $this->validationInt = array("vadFrequency");   
      $this->validationPaths = 1;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return plans
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set planid
     *
     * @param string $planid
     * @return plans
     */
    public function setPlanid($planid)
    {
        $this->planid = $planid;

        return $this;
    }

    /**
     * Get planid
     *
     * @return string 
     */
    public function getPlanid()
    {
        return $this->planid;
    }

    /**
     * Set activePlanid
     *
     * @param string $activePlanid
     * @return plans
     */
    public function setActivePlanid($activePlanid)
    {
        $this->activePlanid = $activePlanid;

        return $this;
    }

    /**
     * Get activePlanid
     *
     * @return string 
     */
    public function getActivePlanid()
    {
        return $this->activePlanid;
    }

    /**
     * Set activePlantypecd
     *
     * @param string $activePlantypecd
     * @return plans
     */
    public function setActivePlantypecd($activePlantypecd)
    {
        $this->activePlantypecd = $activePlantypecd;

        return $this;
    }

    /**
     * Get activePlantypecd
     *
     * @return string 
     */
    public function getActivePlantypecd()
    {
        return $this->activePlantypecd;
    }

    /**
     * Set lastModified
     *
     * @param integer $lastModified
     * @return plans
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    /**
     * Get lastModified
     *
     * @return integer 
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return plans
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return plans
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return plans
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     * @return plans
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string 
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return plans
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return plans
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return plans
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return plans
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return plans
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return plans
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set redirectWebsiteAddress
     *
     * @param string $redirectWebsiteAddress
     * @return plans
     */
    public function setRedirectWebsiteAddress($redirectWebsiteAddress)
    {
        $this->redirectWebsiteAddress = $redirectWebsiteAddress;

        return $this;
    }

    /**
     * Get redirectWebsiteAddress
     *
     * @return string 
     */
    public function getRedirectWebsiteAddress()
    {
        return $this->redirectWebsiteAddress;
    }

	/**
     * Set timeoutWebsiteAddress
     *
     * @param string $timeoutWebsiteAddress
     * @return plans
     */
    public function setTimeoutWebsiteAddress($timeoutWebsiteAddress)
    {
        $this->timeoutWebsiteAddress = $timeoutWebsiteAddress;

        return $this;
    }

    /**
     * Get timeoutWebsiteAddress
     *
     * @return string 
     */
    public function getTimeoutWebsiteAddress()
    {
        return $this->timeoutWebsiteAddress;
    }
    /**
     * Set participantLoginWebsiteAddress
     *
     * @param string $participantLoginWebsiteAddress
     * @return plans
     */
    public function setParticipantLoginWebsiteAddress($participantLoginWebsiteAddress)
    {
        $this->participantLoginWebsiteAddress = $participantLoginWebsiteAddress;

        return $this;
    }

    /**
     * Get participantLoginWebsiteAddress
     *
     * @return string 
     */
    public function getParticipantLoginWebsiteAddress()
    {
        return $this->participantLoginWebsiteAddress;
    }

    /**
     * Set errorNotificationEmail
     *
     * @param string $errorNotificationEmail
     * @return plans
     */
    public function setErrorNotificationEmail($errorNotificationEmail)
    {
        $this->errorNotificationEmail = $errorNotificationEmail;

        return $this;
    }

    /**
     * Get errorNotificationEmail
     *
     * @return string 
     */
    public function getErrorNotificationEmail()
    {
        return $this->errorNotificationEmail;
    }

    /**
     * Set profileNotificationEmail
     *
     * @param string $profileNotificationEmail
     * @return plans
     */
    public function setProfileNotificationEmail($profileNotificationEmail)
    {
        $this->profileNotificationEmail = $profileNotificationEmail;

        return $this;
    }

    /**
     * Get profileNotificationEmail
     *
     * @return string 
     */
    public function getProfileNotificationEmail()
    {
        return $this->profileNotificationEmail;
    }

    /**
     * Set youtubePlayer
     *
     * @param integer $youtubePlayer
     * @return plans
     */
    public function setYoutubePlayer($youtubePlayer)
    {
        $this->youtubePlayer = $youtubePlayer;

        return $this;
    }

    /**
     * Get youtubePlayer
     *
     * @return integer 
     */
    public function getYoutubePlayer()
    {
        return $this->youtubePlayer;
    }

    /**
     * Set defaultEmailAddress
     *
     * @param string $defaultEmailAddress
     * @return plans
     */
    public function setDefaultEmailAddress($defaultEmailAddress)
    {
        $this->defaultEmailAddress = $defaultEmailAddress;

        return $this;
    }

    /**
     * Get defaultEmailAddress
     *
     * @return string 
     */
    public function getDefaultEmailAddress()
    {
        return $this->defaultEmailAddress;
    }

    /**
     * Set providerLogoImage
     *
     * @param string $providerLogoImage
     * @return plans
     */
    public function setProviderLogoImage($providerLogoImage)
    {
        $this->providerLogoImage = $providerLogoImage;

        return $this;
    }

    /**
     * Get providerLogoImage
     *
     * @return string 
     */
    public function getProviderLogoImage()
    {
        return $this->providerLogoImage;
    }

    /**
     * Set providerLogoEmailImage
     *
     * @param string $providerLogoEmailImage
     * @return plans
     */
    public function setProviderLogoEmailImage($providerLogoEmailImage)
    {
        $this->providerLogoEmailImage = $providerLogoEmailImage;

        return $this;
    }

    /**
     * Get providerLogoEmailImage
     *
     * @return string 
     */
    public function getProviderLogoEmailImage()
    {
        return $this->providerLogoEmailImage;
    }

    /**
     * Set sponsorLogoImage
     *
     * @param string $sponsorLogoImage
     * @return plans
     */
    public function setSponsorLogoImage($sponsorLogoImage)
    {
        $this->sponsorLogoImage = $sponsorLogoImage;

        return $this;
    }

    /**
     * Get sponsorLogoImage
     *
     * @return string 
     */
    public function getSponsorLogoImage()
    {
        return $this->sponsorLogoImage;
    }

    /**
     * Set bannerImage
     *
     * @param string $bannerImage
     * @return plans
     */
    public function setBannerImage($bannerImage)
    {
        $this->bannerImage = $bannerImage;

        return $this;
    }

    /**
     * Get bannerImage
     *
     * @return string 
     */
    public function getBannerImage()
    {
        return $this->bannerImage;
    }

    /**
     * Set bannerLinkUrl
     *
     * @param string $bannerLinkUrl
     * @return plans
     */
    public function setBannerLinkUrl($bannerLinkUrl)
    {
        $this->bannerLinkUrl = $bannerLinkUrl;

        return $this;
    }

    /**
     * Get bannerLinkUrl
     *
     * @return string 
     */
    public function getBannerLinkUrl()
    {
        return $this->bannerLinkUrl;
    }

    /**
     * Set interfaceColor
     *
     * @param string $interfaceColor
     * @return plans
     */
    public function setInterfaceColor($interfaceColor)
    {
        $this->interfaceColor = $interfaceColor;

        return $this;
    }

    /**
     * Get interfaceColor
     *
     * @return string 
     */
    public function getInterfaceColor()
    {
        return $this->interfaceColor;
    }

    /**
     * Set interfaceLayout
     *
     * @param string $interfaceLayout
     * @return plans
     */
    public function setInterfaceLayout($interfaceLayout)
    {
        $this->interfaceLayout = $interfaceLayout;

        return $this;
    }

    /**
     * Get interfaceLayout
     *
     * @return string 
     */
    public function getInterfaceLayout()
    {
        return $this->interfaceLayout;
    }

    /**
     * Set advice
     *
     * @param string $advice
     * @return plans
     */
    public function setAdvice($advice)
    {
        $this->advice = $advice;

        return $this;
    }

    /**
     * Get advice
     *
     * @return string 
     */
    public function getAdvice()
    {
        return $this->advice;
    }

    /**
     * Set adviceStatus
     *
     * @param integer $adviceStatus
     * @return plans
     */
    public function setAdviceStatus($adviceStatus)
    {
        $this->adviceStatus = $adviceStatus;

        return $this;
    }

    /**
     * Get adviceStatus
     *
     * @return integer 
     */
    public function getAdviceStatus()
    {
        return $this->adviceStatus;
    }

    /**
     * Set adviceCompare
     *
     * @param string $adviceCompare
     * @return plans
     */
    public function setAdviceCompare($adviceCompare)
    {
        $this->adviceCompare = $adviceCompare;

        return $this;
    }

    /**
     * Get adviceCompare
     *
     * @return string 
     */
    public function getAdviceCompare()
    {
        return $this->adviceCompare;
    }

    /**
     * Set adviceDate
     *
     * @param \DateTime $adviceDate
     * @return plans
     */
    public function setAdviceDate($adviceDate)
    {
        $this->adviceDate = $adviceDate;

        return $this;
    }

    /**
     * Get adviceDate
     *
     * @return \DateTime 
     */
    public function getAdviceDate()
    {
        return $this->adviceDate;
    }

    /**
     * Set riskType
     *
     * @param string $riskType
     * @return plans
     */
    public function setRiskType($riskType)
    {
        $this->riskType = $riskType;

        return $this;
    }

    /**
     * Get riskType
     *
     * @return string 
     */
    public function getRiskType()
    {
        return $this->riskType;
    }

    /**
     * Set fundOrder
     *
     * @param string $fundOrder
     * @return plans
     */
    public function setFundOrder($fundOrder)
    {
        $this->fundOrder = $fundOrder;

        return $this;
    }

    /**
     * Get fundOrder
     *
     * @return string 
     */
    public function getFundOrder()
    {
        return $this->fundOrder;
    }

    /**
     * Set portfolioOrder
     *
     * @param string $portfolioOrder
     * @return plans
     */
    public function setPortfolioOrder($portfolioOrder)
    {
        $this->portfolioOrder = $portfolioOrder;

        return $this;
    }

    /**
     * Get portfolioOrder
     *
     * @return string 
     */
    public function getPortfolioOrder()
    {
        return $this->portfolioOrder;
    }

    /**
     * Set investmentsAutoScoring
     *
     * @param integer $investmentsAutoScoring
     * @return plans
     */
    public function setInvestmentsAutoScoring($investmentsAutoScoring)
    {
        $this->investmentsAutoScoring = $investmentsAutoScoring;

        return $this;
    }

    /**
     * Get investmentsAutoScoring
     *
     * @return integer 
     */
    public function getInvestmentsAutoScoring()
    {
        return $this->investmentsAutoScoring;
    }

    /**
     * Set vadFrequency
     *
     * @param string $vadFrequency
     * @return plans
     */
    public function setVadFrequency($vadFrequency)
    {
        $this->vadFrequency = $vadFrequency;

        return $this;
    }

    /**
     * Get vadFrequency
     *
     * @return string 
     */
    public function getVadFrequency()
    {
        return $this->vadFrequency;
    }

    /**
     * Set MSTClientSessionID
     *
     * @param string $mSTClientSessionID
     * @return plans
     */
    public function setMSTClientSessionID($mSTClientSessionID)
    {
        $this->MSTClientSessionID = $mSTClientSessionID;

        return $this;
    }

    /**
     * Get MSTClientSessionID
     *
     * @return string 
     */
    public function getMSTClientSessionID()
    {
        return $this->MSTClientSessionID;
    }

    /**
     * Set fundLink
     *
     * @param integer $fundLink
     * @return plans
     */
    public function setFundLink($fundLink)
    {
        $this->fundLink = $fundLink;

        return $this;
    }

    /**
     * Get fundLink
     *
     * @return integer 
     */
    public function getFundLink()
    {
        return $this->fundLink;
    }

    /**
     * Set IRSCode
     *
     * @param string $iRSCode
     * @return plans
     */
    public function setIRSCode($iRSCode)
    {
        $this->IRSCode = $iRSCode;

        return $this;
    }

    /**
     * Get IRSCode
     *
     * @return string 
     */
    public function getIRSCode()
    {
        return $this->IRSCode;
    }

    /**
     * Set defaultInvestment
     *
     * @param integer $defaultInvestment
     * @return plans
     */
    public function setDefaultInvestment($defaultInvestment)
    {
        $this->defaultInvestment = $defaultInvestment;

        return $this;
    }

    /**
     * Get defaultInvestment
     *
     * @return integer 
     */
    public function getDefaultInvestment()
    {
        return $this->defaultInvestment;
    }

    /**
     * Set deferralOff
     *
     * @param integer $deferralOff
     * @return plans
     */
    public function setDeferralOff($deferralOff)
    {
        $this->deferralOff = $deferralOff;

        return $this;
    }

    /**
     * Get deferralOff
     *
     * @return integer 
     */
    public function getDeferralOff()
    {
        return $this->deferralOff;
    }

    /**
     * Set pipContent
     *
     * @param integer $pipContent
     * @return plans
     */
    public function setPipContent($pipContent)
    {
        $this->pipContent = $pipContent;

        return $this;
    }

    /**
     * Get pipContent
     *
     * @return integer 
     */
    public function getPipContent()
    {
        return $this->pipContent;
    }

    /**
     * Set pipPosition
     *
     * @param integer $pipPosition
     * @return plans
     */
    public function setPipPosition($pipPosition)
    {
        $this->pipPosition = $pipPosition;

        return $this;
    }

    /**
     * Get pipPosition
     *
     * @return integer 
     */
    public function getPipPosition()
    {
        return $this->pipPosition;
    }

    /**
     * Set matchPercent
     *
     * @param integer $matchPercent
     * @return plans
     */
    public function setMatchPercent($matchPercent)
    {
        $this->matchPercent = $matchPercent;

        return $this;
    }

    /**
     * Get matchPercent
     *
     * @return integer 
     */
    public function getMatchPercent()
    {
        return $this->matchPercent;
    }

    /**
     * Set secondaryMatchCap
     *
     * @param integer $secondaryMatchCap
     * @return plans
     */
    public function setSecondaryMatchCap($secondaryMatchCap)
    {
        $this->secondaryMatchCap = $secondaryMatchCap;

        return $this;
    }

    /**
     * Get secondaryMatchCap
     *
     * @return integer 
     */
    public function getSecondaryMatchCap()
    {
        return $this->secondaryMatchCap;
    }

    /**
     * Set secondaryMatchPercent
     *
     * @param integer $secondaryMatchPercent
     * @return plans
     */
    public function setSecondaryMatchPercent($secondaryMatchPercent)
    {
        $this->secondaryMatchPercent = $secondaryMatchPercent;

        return $this;
    }

    /**
     * Get secondaryMatchPercent
     *
     * @return integer 
     */
    public function getSecondaryMatchPercent()
    {
        return $this->secondaryMatchPercent;
    }

    /**
     * Set morningstar
     *
     * @param integer $morningstar
     * @return plans
     */
    public function setMorningstar($morningstar)
    {
        $this->morningstar = $morningstar;

        return $this;
    }

    /**
     * Get morningstar
     *
     * @return integer 
     */
    public function getMorningstar()
    {
        return $this->morningstar;
    }
    public function __set($name, $value) 
    {
        $this->$name = $value;
        if ($name == "type")
        {
            $hashTypes = array();
            $hashTypes['smartplan'] = "smartPlanAllowed";
            $hashTypes['smartenroll'] = "smartEnrollAllowed";
            $hashTypes['irio'] = "irioAllowed";
            $hashTypes['powerview'] = "powerviewAllowed";
            $type = $hashTypes[$value];
            $this->$type= 1;
        }
    }
    public function __get($name)
    {
        return $this->$name;
    }
	
	public function getTypes() {
		$types = [];
		if ($this->smartEnrollAllowed) {
			$types[] = "smartenroll";
		}
		if ($this->smartPlanAllowed) {
			$types[] = "smartplan";
		}
		if ($this->powerviewAllowed) {
			$types[] = "powerview";
		}
		if ($this->irioAllowed) {
			$types[] = "irio";
		}
		return $types;
	}
    
    public function addTypes(array $types = []) {
        foreach ($types as $type) {
            switch ($type) {
                case "smartenroll":
                    $this->smartEnrollAllowed = 1; 
                break;
                case "smartplan":
                    $this->smartPlanAllowed = 1; 
                break;
                case "powerview":
                    $this->powerviewAllowed = 1; 
                break;
                case "irio":
                    $this->irioAllowed = 1; 
                break;
            }
        }
    }
}
