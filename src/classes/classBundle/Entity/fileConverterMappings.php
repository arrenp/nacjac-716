<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class fileConverterMappings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
     /**
     * @var string
     *
     * @ORM\Column(name="headerid", type="integer")
     */
    public $headerid;      
     /**
     * @var string
     *
     * @ORM\Column(name="fromValue", type="string", length=100)
     */
    public $from;  
     /**
     * @var string
     *
     * @ORM\Column(name="toValue", type="string", length=100)
     */
    public $to;    
}



