<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class reportingSubscriptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="accountsUsersId",type="integer")
     */
    public $accountsUsersId;    
     /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    public $name;
    
      /**
     * @var string
     *
     * @ORM\Column(name="howoften", type="string",length=100)
     */
    public $howoften;
  
    /**
     * @var string
     *
     * @ORM\Column(name="date", type="date")
     */
    public $date;    
    
      /**
     * @var string
     *
     * @ORM\Column(name="lastProcessed", type="date", nullable =true)
     */
    public $lastProcessed;
     /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length = 200)
     */
    public $path;       
    
    
    
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($key != "date" && $key != "lastProcessed")
            $this->$key = "";
        }
        $this->date = new \DateTime("now");
        $this->lastProcessed = new \DateTime("now");
        
    }

}
