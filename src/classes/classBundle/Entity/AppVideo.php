<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
 class AppVideo extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="playListId", type="integer")
     */
    public $playListId;
    /**
     * @var integer
     *
     * @ORM\Column(name="pathId", type="integer")
     */
    public $pathId;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    public $description;
    /**
     * @var string
     *
     * @ORM\Column(name="step", type="integer")
     */
    public $step;
     /**
      * @var string
      *
      * @ORM\Column(name="disclaimer", type="string", length=255)
      */
     public $disclaimer;
     /**
      * @var string
      *
      * @ORM\Column(name="module", type="string", length=255)
      */
     public $module;
     /**
      * @var string
      *
      * @ORM\Column(name="moduleSelection", type="string", length=255)
      */
     public $moduleSelection;
    
    public function __construct()
    {
        $this->step = time();
    }
}

