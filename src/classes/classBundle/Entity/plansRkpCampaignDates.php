<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\rkpCampaignDates;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansRkpCampaignDates extends rkpCampaignDates
{     
    /**
     * @var text
     *
     * @ORM\Column(name="partnerid", type="string", length = 255)
     */
    public $partnerid;   
    /**
     * @var text
     *
     * @ORM\Column(name="recordkeeperPlanid", type="string", length = 255)
     */
    public $recordkeeperPlanid; 
}


