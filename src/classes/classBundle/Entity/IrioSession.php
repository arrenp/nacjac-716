<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class IrioSession
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */       
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="sid", type="text", length=255)
     */
    public $sid;  
    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text")
     */
    public $data;      
    
}