<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class skinVideo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="description", type="string",length = 50)
     */
	public $description;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="folder", type="string",length = 25)
	 */
	public $folder;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="custom", type="smallint")
	 */
	public $custom;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customFolder", type="string",length = 25)
	 */
	public $customFolder;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customTransition", type="smallint")
	 */
	public $customTransition;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="transitionFolder", type="string",length = 50)
	 */
	public $transitionFolder;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}



}
