<?php
namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PathContent extends BaseEntity
{
    public static $operatorTypes = ['equ', 'add', 'sub'];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;    
    /**
     * @var integer
     *
     * @ORM\Column(name="defaultContentId", type="integer")
     */
    public $defaultContentId;  
    /**
     * @var integer
     *
     * @ORM\Column(name="pathId", type="integer")
     */
    public $pathId;  
    /**
     * @var string
     *
     * @ORM\Column(name="englishContent", type="string", length=255, nullable=true)
     */
    public $englishContent; 
    /**
     * @var string
     *
     * @ORM\Column(name="spanishContent", type="string", length=255, nullable=true)
     */
    public $spanishContent; 
    /**
     * @var integer
     *
     * @ORM\Column(name="enable", type="integer")
     */
    public $enable;
    /**
     * @var string
     *
     * @ORM\Column(name="rules", type="string", length=255, nullable=true)
     */
    protected $rules;

    /**
     * @return array
     */
    public function getRules()
    {
        return json_decode($this->rules, true);
    }

    /**
     * @param array $rules
     */
    public function setRules(array $rules = array()): void
    {
        $rules = array_replace_recursive(self::getDefaultRules(), $rules);
        $this->rules = json_encode($rules);
    }

    public static function getDefaultRules()
    {
        return [
            'operator' => 'equ',
            'arguments' => [
                0 => [
                    'type' => 'raw',
                    'value' => null
                ],
                1 => [
                    'type' => 'raw',
                    'value' => null
                ]
            ]
        ];
    }
}

