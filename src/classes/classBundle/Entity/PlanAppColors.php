<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use classes\classBundle\Entity\abstractclasses\AppColors;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlanAppColors extends AppColors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;   
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;  
}
