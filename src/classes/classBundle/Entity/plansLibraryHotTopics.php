<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansLibraryHotTopics
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
     /**
     * @var string
     *
     * @ORM\Column(name="rolloverLong", type="integer")
     */
    public $rolloverLong;
     /**
     * @var string
     *
     * @ORM\Column(name="dreams401k", type="integer")
     */
    public $dreams401k;
      /**
     * @var string
     *
     * @ORM\Column(name="withdraw", type="integer")
     */
    public $withdraw;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}
}
