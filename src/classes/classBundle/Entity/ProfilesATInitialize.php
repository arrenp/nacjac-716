<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ProfilesATInitialize
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="WsArchitectTask", type="encrypted")
     */
    public $wsArchitectTask;
    /**
     * @var string
     *
     * @ORM\Column(name="WsBlueprint", type="encrypted")
     */
    public $wsBlueprint;
    /**
     * @var string
     *
     * @ORM\Column(name="profileid", type="string")
     */
    public $profileid;
    /**
     * @var string
     *
     * @ORM\Column(name="WsPlan", type="encrypted")
     */
    public $wsPlan;
    /**
     * @var string
     *
     * @ORM\Column(name="WsIndividual", type="encrypted")
     */
    public $wsIndividual;
}

