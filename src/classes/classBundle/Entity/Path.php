<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
 class Path extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="pathTypeId", type="integer")
     */
    public $pathTypeId;
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=50)
     */
    public $path;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=50)
     */
    public $description;   
    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="integer")
     */
    public $createdBy;   
    /**
     * @var string
     *
     * @ORM\Column(name="updatedBy", type="integer")
     */
    public $updatedBy;
    /**
     * @var string
     *
     * @ORM\Column(name="createdDate", type="datetime")
     */
    public $createdDate;
    /**
     * @var string
     *
     * @ORM\Column(name="updatedDate", type="datetime")
     */
    public $updatedDate;
    /**
     * @var string
     *
     * @ORM\Column(name="active", type="smallint")
     */
    public $active;  
    /**
     * @var string
     *
     * @ORM\Column(name="general", type="smallint")
     */
    public $general;     
    public function __construct() 
    {
        $this->createdDate = $this->updatedDate =  new \DateTime('now');
        $this->active = 0;
        $this->general = 0;
    }   
}

