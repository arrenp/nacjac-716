<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class campaignsStats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="campaignid", type="integer")
     */
    public $campaignid;
    /**
     * @var string
     *
     * @ORM\Column(name="sends", type="integer")
     */
    public $sends;   
    /**
     * @var string
     *
     * @ORM\Column(name="uniqueOpens", type="integer")
     */
    public $uniqueOpens;       
    /**
     * @var string
     *
     * @ORM\Column(name="uniqueClicks", type="integer")
     */
    public $uniqueClicks;      
    /**
     * @var string
     *
     * @ORM\Column(name="bounces", type="integer")
     */
    public $bounces;    
    /**
     * @var string
     *
     * @ORM\Column(name="unsubscribes", type="integer")
     */
    public $unsubscribes; 
    
    public function __construct()
    {

        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            $this->$key = "";
        }
        
    }    
    
}