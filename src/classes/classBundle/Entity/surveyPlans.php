<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class surveyPlans
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
     /**
     * @var text
     *
     * @ORM\Column(name="partnerid", type="string", length = 255)
     */
    public $partnerid;   
    /**
     * @var text
     *
     * @ORM\Column(name="recordkeeperPlanid", type="string", length = 255)
     */
    public $recordkeeperPlanid;
    /** 
     * @var datetime
     * 
     * @ORM\Column(type="datetime", name="createDate") 
     */
    public $createDate;   
     /** 
     * @var datetime
     * 
     * @ORM\Column(type="datetime", name="modifiedDate") 
     */
    public $modifiedDate;       
    /**
     * @var text
     *
     * @ORM\Column(name="answerid", type="integer")
     */
    public $answerid;     
}