<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsConfigurationColors;
use Doctrine\ORM\Mapping as ORM;
/**
 * DefaultInvestmentsConfigurationColors
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultInvestmentsConfigurationColors extends InvestmentsConfigurationColors
{
         
}

