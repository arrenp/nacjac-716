<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class accountsUsersLogins
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="accountsUsersId", type="integer")
     */
    public $accountsUsersId;
    /**
     * @var integer
     *
     * @ORM\Column(name="sessionid", type="string")
     */
    public $sessionid;
    /**
     * @var integer
     *
     * @ORM\Column(name="loginTime", type="integer")
     */
    public $loginTime;
    /**
     * @var integer
     *
     * @ORM\Column(name="warning", type="integer")
     */
    public $warning;
    /**
     * @var integer
     *
     * @ORM\Column(name="numberOfChatSessions", type="integer")
     */
    public $numberOfChatSessions;
     /**
     * @var integer
     *
     * @ORM\Column(name="lastActivity", type="integer")
     */
    public $lastActivity;   
   


    public function __construct()
    {
      $class_vars = get_class_vars(get_class($this));
      foreach ($class_vars as $key => $value)
      {
        if ($this->$key != "id")
        $this->$key = "";
      }
      
  }

}
