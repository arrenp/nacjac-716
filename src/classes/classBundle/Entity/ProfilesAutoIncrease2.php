<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use classes\classBundle\Entity\abstractclasses\AutoIncrease;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ProfilesAutoIncrease2 extends AutoIncrease
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;    
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;   
    /**
     * @var string
     *
     * @ORM\Column(name="profileid", type="integer", nullable=true)
     */
    public $profileid;
}


