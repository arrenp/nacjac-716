<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class profilesRetirementNeeds
{
	/**
     * @ORM\ManyToOne(targetEntity="profiles", inversedBy="retirementNeeds")
     * @ORM\JoinColumn(name="profileid", referencedColumnName="id")
     **/
	public $profile;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer",nullable=true)
     */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planid", type="integer",nullable=true)
	 */
    public $planid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="profileid", type="integer",nullable=true)
	 */
    public $profileid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="currentYearlyIncome", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $currentYearlyIncome;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="estimatedRetirementIncome", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $estimatedRetirementIncome;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="estimatedSocialSecurityIncome", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $estimatedSocialSecurityIncome;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="replacementIncome", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $replacementIncome;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="yearsBeforeRetirement", type="integer", nullable=true)
	 */
    public $yearsBeforeRetirement;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="inflationAdjustedReplacementIncome", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $inflationAdjustedReplacementIncome;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="currentPlanBalance", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $currentPlanBalance;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="otherAssets", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $otherAssets;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="totalAssets", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $totalAssets;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="assetTypes", type="text", length = 65535,nullable=true)
	 */
    public $assetTypes;
   	/**
	 * @var string
	 *
	 * @ORM\Column(name="yearsLiveInRetirement", type="float", length = 255,nullable=true)
	 */
    public $yearsLiveInRetirement;
    /**
     * @var string
     *
     * @ORM\Column(name="retirementAge", type="float", length = 255,nullable=true)
     */
    public $retirementAge;
    /**
     * @var string
     *
     * @ORM\Column(name="lifeExpectancy", type="float", length = 255,nullable=true)
     */
    public $lifeExpectancy;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="annualIncomeNeededInRetirement", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $annualIncomeNeededInRetirement;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="estimatedSavingsAtRetirement", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $estimatedSavingsAtRetirement;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="recommendedMonthlyPlanContribution", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $recommendedMonthlyPlanContribution;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="percentageOfIncomeFromPlan", type="integer", nullable=true)
	 */
    public $percentageOfIncomeFromPlan;
    /**
     * @var string
     *
     * @ORM\Column(name="participantsId", type="integer")
     */
    public $participantsId;
    /**
     * @var string
     *
     * @ORM\Column(name="estimatedPensionIncome", type="decimal",  scale = 2, precision = 18,nullable=true)
     */
    public $estimatedPensionIncome;
    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return profilesRetirementNeeds
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set planid
     *
     * @param integer $planid
     *
     * @return profilesRetirementNeeds
     */
    public function setPlanid($planid)
    {
        $this->planid = $planid;

        return $this;
    }

    /**
     * Get planid
     *
     * @return integer
     */
    public function getPlanid()
    {
        return $this->planid;
    }

    /**
     * Set profileid
     *
     * @param integer $profileid
     *
     * @return profilesRetirementNeeds
     */
    public function setProfileid($profileid)
    {
        $this->profileid = $profileid;

        return $this;
    }

    /**
     * Get profileid
     *
     * @return integer
     */
    public function getProfileid()
    {
        return $this->profileid;
    }

    /**
     * Set currentYearlyIncome
     *
     * @param string $currentYearlyIncome
     *
     * @return profilesRetirementNeeds
     */
    public function setCurrentYearlyIncome($currentYearlyIncome)
    {
        $this->currentYearlyIncome = $currentYearlyIncome;

        return $this;
    }

    /**
     * Get currentYearlyIncome
     *
     * @return string
     */
    public function getCurrentYearlyIncome()
    {
        return $this->currentYearlyIncome;
    }

    /**
     * Set estimatedRetirementIncome
     *
     * @param string $estimatedRetirementIncome
     *
     * @return profilesRetirementNeeds
     */
    public function setEstimatedRetirementIncome($estimatedRetirementIncome)
    {
        $this->estimatedRetirementIncome = $estimatedRetirementIncome;

        return $this;
    }

    /**
     * Get estimatedRetirementIncome
     *
     * @return string
     */
    public function getEstimatedRetirementIncome()
    {
        return $this->estimatedRetirementIncome;
    }

    /**
     * Set estimatedSocialSecurityIncome
     *
     * @param string $estimatedSocialSecurityIncome
     *
     * @return profilesRetirementNeeds
     */
    public function setEstimatedSocialSecurityIncome($estimatedSocialSecurityIncome)
    {
        $this->estimatedSocialSecurityIncome = $estimatedSocialSecurityIncome;

        return $this;
    }

    /**
     * Get estimatedSocialSecurityIncome
     *
     * @return string
     */
    public function getEstimatedSocialSecurityIncome()
    {
        return $this->estimatedSocialSecurityIncome;
    }

    /**
     * Set replacementIncome
     *
     * @param string $replacementIncome
     *
     * @return profilesRetirementNeeds
     */
    public function setReplacementIncome($replacementIncome)
    {
        $this->replacementIncome = $replacementIncome;

        return $this;
    }

    /**
     * Get replacementIncome
     *
     * @return string
     */
    public function getReplacementIncome()
    {
        return $this->replacementIncome;
    }

    /**
     * Set yearsBeforeRetirement
     *
     * @param integer $yearsBeforeRetirement
     *
     * @return profilesRetirementNeeds
     */
    public function setYearsBeforeRetirement($yearsBeforeRetirement)
    {
        $this->yearsBeforeRetirement = $yearsBeforeRetirement;

        return $this;
    }

    /**
     * Get yearsBeforeRetirement
     *
     * @return integer
     */
    public function getYearsBeforeRetirement()
    {
        return $this->yearsBeforeRetirement;
    }

    /**
     * Set inflationAdjustedReplacementIncome
     *
     * @param string $inflationAdjustedReplacementIncome
     *
     * @return profilesRetirementNeeds
     */
    public function setInflationAdjustedReplacementIncome($inflationAdjustedReplacementIncome)
    {
        $this->inflationAdjustedReplacementIncome = $inflationAdjustedReplacementIncome;

        return $this;
    }

    /**
     * Get inflationAdjustedReplacementIncome
     *
     * @return string
     */
    public function getInflationAdjustedReplacementIncome()
    {
        return $this->inflationAdjustedReplacementIncome;
    }

    /**
     * Set currentPlanBalance
     *
     * @param string $currentPlanBalance
     *
     * @return profilesRetirementNeeds
     */
    public function setCurrentPlanBalance($currentPlanBalance)
    {
        $this->currentPlanBalance = $currentPlanBalance;

        return $this;
    }

    /**
     * Get currentPlanBalance
     *
     * @return string
     */
    public function getCurrentPlanBalance()
    {
        return $this->currentPlanBalance;
    }

    /**
     * Set otherAssets
     *
     * @param string $otherAssets
     *
     * @return profilesRetirementNeeds
     */
    public function setOtherAssets($otherAssets)
    {
        $this->otherAssets = $otherAssets;

        return $this;
    }

    /**
     * Get otherAssets
     *
     * @return string
     */
    public function getOtherAssets()
    {
        return $this->otherAssets;
    }

    /**
     * Set totalAssets
     *
     * @param string $totalAssets
     *
     * @return profilesRetirementNeeds
     */
    public function setTotalAssets($totalAssets)
    {
        $this->totalAssets = $totalAssets;

        return $this;
    }

    /**
     * Get totalAssets
     *
     * @return string
     */
    public function getTotalAssets()
    {
        return $this->totalAssets;
    }

    /**
     * Set assetTypes
     *
     * @param string $assetTypes
     *
     * @return profilesRetirementNeeds
     */
    public function setAssetTypes($assetTypes)
    {
        $this->assetTypes = $assetTypes;

        return $this;
    }

    /**
     * Get assetTypes
     *
     * @return string
     */
    public function getAssetTypes()
    {
        return $this->assetTypes;
    }

    /**
     * Set yearsLiveInRetirement
     *
     * @param float $yearsLiveInRetirement
     *
     * @return profilesRetirementNeeds
     */
    public function setYearsLiveInRetirement($yearsLiveInRetirement)
    {
        $this->yearsLiveInRetirement = $yearsLiveInRetirement;

        return $this;
    }

    /**
     * Get yearsLiveInRetirement
     *
     * @return float
     */
    public function getYearsLiveInRetirement()
    {
        return $this->yearsLiveInRetirement;
    }

    /**
     * Set annualIncomeNeededInRetirement
     *
     * @param string $annualIncomeNeededInRetirement
     *
     * @return profilesRetirementNeeds
     */
    public function setAnnualIncomeNeededInRetirement($annualIncomeNeededInRetirement)
    {
        $this->annualIncomeNeededInRetirement = $annualIncomeNeededInRetirement;

        return $this;
    }

    /**
     * Get annualIncomeNeededInRetirement
     *
     * @return string
     */
    public function getAnnualIncomeNeededInRetirement()
    {
        return $this->annualIncomeNeededInRetirement;
    }

    /**
     * Set estimatedSavingsAtRetirement
     *
     * @param string $estimatedSavingsAtRetirement
     *
     * @return profilesRetirementNeeds
     */
    public function setEstimatedSavingsAtRetirement($estimatedSavingsAtRetirement)
    {
        $this->estimatedSavingsAtRetirement = $estimatedSavingsAtRetirement;

        return $this;
    }

    /**
     * Get estimatedSavingsAtRetirement
     *
     * @return string
     */
    public function getEstimatedSavingsAtRetirement()
    {
        return $this->estimatedSavingsAtRetirement;
    }

    /**
     * Set recommendedMonthlyPlanContribution
     *
     * @param string $recommendedMonthlyPlanContribution
     *
     * @return profilesRetirementNeeds
     */
    public function setRecommendedMonthlyPlanContribution($recommendedMonthlyPlanContribution)
    {
        $this->recommendedMonthlyPlanContribution = $recommendedMonthlyPlanContribution;

        return $this;
    }

    /**
     * Get recommendedMonthlyPlanContribution
     *
     * @return string
     */
    public function getRecommendedMonthlyPlanContribution()
    {
        return $this->recommendedMonthlyPlanContribution;
    }

    /**
     * Set percentageOfIncomeFromPlan
     *
     * @param integer $percentageOfIncomeFromPlan
     *
     * @return profilesRetirementNeeds
     */
    public function setPercentageOfIncomeFromPlan($percentageOfIncomeFromPlan)
    {
        $this->percentageOfIncomeFromPlan = $percentageOfIncomeFromPlan;

        return $this;
    }

    /**
     * Get percentageOfIncomeFromPlan
     *
     * @return integer
     */
    public function getPercentageOfIncomeFromPlan()
    {
        return $this->percentageOfIncomeFromPlan;
    }

    /**
     * Set profile
     *
     * @param \classes\classBundle\Entity\profiles $profile
     *
     * @return profilesRetirementNeeds
     */
    public function setProfile(\classes\classBundle\Entity\profiles $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \classes\classBundle\Entity\profiles
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set retirementAge
     *
     * @param float $retirementAge
     *
     * @return profilesRetirementNeeds
     */
    public function setRetirementAge($retirementAge)
    {
        $this->retirementAge = $retirementAge;

        return $this;
    }

    /**
     * Get retirementAge
     *
     * @return float
     */
    public function getRetirementAge()
    {
        return $this->retirementAge;
    }

    /**
     * Set lifeExpectancy
     *
     * @param float $lifeExpectancy
     *
     * @return profilesRetirementNeeds
     */
    public function setLifeExpectancy($lifeExpectancy)
    {
        $this->lifeExpectancy = $lifeExpectancy;

        return $this;
    }

    /**
     * Get lifeExpectancy
     *
     * @return float
     */
    public function getLifeExpectancy()
    {
        return $this->lifeExpectancy;
    }
    /**
     * @return string
     */
    public function getParticipantsId()
    {
        return $this->participantsId;
    }

    /**
     * @param string $spousalWaiverForm
     */
    public function setParticipantsId($participantsId)
    {
        $this->participantsId = $participantsId;
    }    
}
