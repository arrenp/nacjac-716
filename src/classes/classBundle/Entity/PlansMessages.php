<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * messages
 *
 * @ORM\Table(name="plansMessages")
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansMessages extends Messages {
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
}
