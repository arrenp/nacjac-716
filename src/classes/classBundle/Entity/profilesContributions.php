<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class profilesContributions
{
	/**
     * @ORM\ManyToOne(targetEntity="profiles", inversedBy="contributions")
     * @ORM\JoinColumn(name="profileid", referencedColumnName="id")
     **/
	public $profile;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer",nullable=true)
     */
	public $userid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="planid", type="integer",nullable=true)
	 */
    public $planid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="profileid", type="integer",nullable=true)
	 */
    public $profileid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="currentYearlyIncome", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $currentYearlyIncome;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="paidFrequency", type="string", length = 15,nullable=true)
	 */
    public $paidFrequency;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="target", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $target;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="projected", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $projected;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="projectedEmployer", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $projectedEmployer;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="mode", type="string", length = 3,nullable=true)
	 */
    public $mode;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmode", type="string", length = 3,nullable=true)
	 */
    public $cmode;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="pre", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $pre;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="roth", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $roth;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="post", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $post;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="catchup", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $catchup;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="preCurrent", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $preCurrent;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="rothCurrent", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $rothCurrent;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="postCurrent", type="decimal",  scale = 2, precision = 18,nullable=true)
	 */
    public $postCurrent;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="isAca", type="smallint",nullable=true)
	 */
    public $isAca;
 	/**
	 * @var string
	 *
	 * @ORM\Column(name="isAcaOptout", type="smallint",nullable=true)
	 */
    public $isAcaOptout;
    /**
     * @var string
     *
     * @ORM\Column(name="deferralPercent", type="decimal",  scale = 2, precision = 18,nullable=true)
     */
    public $deferralPercent;    
    /**
     * @var string
     *
     * @ORM\Column(name="deferralDollar", type="decimal",  scale = 2, precision = 18,nullable=true)
     */
    public $deferralDollar;      
    /**
     * @var string
     *
     * @ORM\Column(name="rothPercent", type="decimal",  scale = 2, precision = 18,nullable=true)
     */
    public $rothPercent;  
    /**
     * @var string
     *
     * @ORM\Column(name="participantsId", type="integer")
     */
    public $participantsId;
    /**
     * @var string
     *
     * @ORM\Column(name="prePercent", type="decimal",  scale = 2, precision = 18,nullable=true)
     */
    public $prePercent;
    /**
     * @var string
     *
     * @ORM\Column(name="preDollar", type="decimal",  scale = 2, precision = 18,nullable=true)
     */
    public $preDollar;
    /**
     * @var string
     *
     * @ORM\Column(name="rothDollar", type="decimal",  scale = 2, precision = 18,nullable=true)
     */
    public $rothDollar;
    /**
     * @var string
     *
     * @ORM\Column(name="postPercent", type="decimal",  scale = 2, precision = 18,nullable=true)
     */
    public $postPercent;
    /**
     * @var string
     *
     * @ORM\Column(name="postDollar", type="decimal",  scale = 2, precision = 18,nullable=true)
     */
    public $postDollar;    
  
    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
     		$this->$key = "";
   		}
	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return profilesContributions
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set planid
     *
     * @param integer $planid
     *
     * @return profilesContributions
     */
    public function setPlanid($planid)
    {
        $this->planid = $planid;

        return $this;
    }

    /**
     * Get planid
     *
     * @return integer
     */
    public function getPlanid()
    {
        return $this->planid;
    }

    /**
     * Set profileid
     *
     * @param integer $profileid
     *
     * @return profilesContributions
     */
    public function setProfileid($profileid)
    {
        $this->profileid = $profileid;

        return $this;
    }

    /**
     * Get profileid
     *
     * @return integer
     */
    public function getProfileid()
    {
        return $this->profileid;
    }

    /**
     * Set currentYearlyIncome
     *
     * @param string $currentYearlyIncome
     *
     * @return profilesContributions
     */
    public function setCurrentYearlyIncome($currentYearlyIncome)
    {
        $this->currentYearlyIncome = $currentYearlyIncome;

        return $this;
    }

    /**
     * Get currentYearlyIncome
     *
     * @return string
     */
    public function getCurrentYearlyIncome()
    {
        return $this->currentYearlyIncome;
    }

    /**
     * Set paidFrequency
     *
     * @param string $paidFrequency
     *
     * @return profilesContributions
     */
    public function setPaidFrequency($paidFrequency)
    {
        $this->paidFrequency = $paidFrequency;

        return $this;
    }

    /**
     * Get paidFrequency
     *
     * @return string
     */
    public function getPaidFrequency()
    {
        return $this->paidFrequency;
    }

    /**
     * Set target
     *
     * @param string $target
     *
     * @return profilesContributions
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set projected
     *
     * @param string $projected
     *
     * @return profilesContributions
     */
    public function setProjected($projected)
    {
        $this->projected = $projected;

        return $this;
    }

    /**
     * Get projected
     *
     * @return string
     */
    public function getProjected()
    {
        return $this->projected;
    }

    /**
     * Set projectedEmployer
     *
     * @param string $projectedEmployer
     *
     * @return profilesContributions
     */
    public function setProjectedEmployer($projectedEmployer)
    {
        $this->projectedEmployer = $projectedEmployer;

        return $this;
    }

    /**
     * Get projectedEmployer
     *
     * @return string
     */
    public function getProjectedEmployer()
    {
        return $this->projectedEmployer;
    }

    /**
     * Set mode
     *
     * @param string $mode
     *
     * @return profilesContributions
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set cmode
     *
     * @param string $cmode
     *
     * @return profilesContributions
     */
    public function setCmode($cmode)
    {
        $this->cmode = $cmode;

        return $this;
    }

    /**
     * Get cmode
     *
     * @return string
     */
    public function getCmode()
    {
        return $this->cmode;
    }

    /**
     * Set pre
     *
     * @param string $pre
     *
     * @return profilesContributions
     */
    public function setPre($pre)
    {
        $this->pre = $pre;

        return $this;
    }

    /**
     * Get pre
     *
     * @return string
     */
    public function getPre()
    {
        return $this->pre;
    }

    /**
     * Set roth
     *
     * @param string $roth
     *
     * @return profilesContributions
     */
    public function setRoth($roth)
    {
        $this->roth = $roth;

        return $this;
    }

    /**
     * Get roth
     *
     * @return string
     */
    public function getRoth()
    {
        return $this->roth;
    }

    /**
     * Set catchup
     *
     * @param string $catchup
     *
     * @return profilesContributions
     */
    public function setCatchup($catchup)
    {
        $this->catchup = $catchup;

        return $this;
    }

    /**
     * Get catchup
     *
     * @return string
     */
    public function getCatchup()
    {
        return $this->catchup;
    }

    /**
     * Set preCurrent
     *
     * @param string $preCurrent
     *
     * @return profilesContributions
     */
    public function setPreCurrent($preCurrent)
    {
        $this->preCurrent = $preCurrent;

        return $this;
    }

    /**
     * Get preCurrent
     *
     * @return string
     */
    public function getPreCurrent()
    {
        return $this->preCurrent;
    }

    /**
     * Set rothCurrent
     *
     * @param string $rothCurrent
     *
     * @return profilesContributions
     */
    public function setRothCurrent($rothCurrent)
    {
        $this->rothCurrent = $rothCurrent;

        return $this;
    }

    /**
     * Get rothCurrent
     *
     * @return string
     */
    public function getRothCurrent()
    {
        return $this->rothCurrent;
    }

    /**
     * Set isAca
     *
     * @param integer $isAca
     *
     * @return profilesContributions
     */
    public function setIsAca($isAca)
    {
        $this->isAca = $isAca;

        return $this;
    }

    /**
     * Get isAca
     *
     * @return integer
     */
    public function getIsAca()
    {
        return $this->isAca;
    }

    /**
     * Set isAcaOptout
     *
     * @param integer $isAcaOptout
     *
     * @return profilesContributions
     */
    public function setIsAcaOptout($isAcaOptout)
    {
        $this->isAcaOptout = $isAcaOptout;

        return $this;
    }

    /**
     * Get isAcaOptout
     *
     * @return integer
     */
    public function getIsAcaOptout()
    {
        return $this->isAcaOptout;
    }

    /**
     * Set profile
     *
     * @param \classes\classBundle\Entity\profiles $profile
     *
     * @return profilesContributions
     */
    public function setProfile(\classes\classBundle\Entity\profiles $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \classes\classBundle\Entity\profiles
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set post
     *
     * @param string $post
     *
     * @return profilesContributions
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return string
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set postCurrent
     *
     * @param string $postCurrent
     *
     * @return profilesContributions
     */
    public function setPostCurrent($postCurrent)
    {
        $this->postCurrent = $postCurrent;

        return $this;
    }

    /**
     * Get postCurrent
     *
     * @return string
     */
    public function getPostCurrent()
    {
        return $this->postCurrent;
    }
    /**
     * @return string
     */
    public function getParticipantsId()
    {
        return $this->participantsId;
    }

    /**
     * @param string $spousalWaiverForm
     */
    public function setParticipantsId($participantsId)
    {
        $this->participantsId = $participantsId;
    }    
}
