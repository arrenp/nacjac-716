<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansTranslations extends Translations {
    /**
     * @ORM\Column(name="accountId", type="integer")
     */
    public $accountId;

    /**
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
}
