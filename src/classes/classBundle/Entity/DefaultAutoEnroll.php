<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use classes\classBundle\Entity\abstractclasses\AutoEnroll;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultAutoEnroll extends AutoEnroll
{
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;    
}
