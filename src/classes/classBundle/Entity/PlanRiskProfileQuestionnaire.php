<?php

namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\RiskProfileQuestionnaireSelected;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlanRiskProfileQuestionnaire extends RiskProfileQuestionnaireSelected
{   
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
}
