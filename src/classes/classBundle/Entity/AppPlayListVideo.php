<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AppPlayListVideo extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;    
    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer")
     */
    public $languageId;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    public $name;
    /**
     * @var integer
     *
     * @ORM\Column(name="url", type="text")
     */
    public $url;    
    /**
     * @var integer
     *
     * @ORM\Column(name="subtitleUrl", type="text")
     */
    public $subtitleUrl; 
}


