<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansPowerView
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var text
     *
     * @ORM\Column(name="hrFirstName", type="string", length = 255)
     */
    public $hrFirstName;
    /**
     * @var text
     *
     * @ORM\Column(name="hrLastName", type="string", length = 255)
     */
    public $hrLastName;    
    /**
     * @var text
     *
     * @ORM\Column(name="hrEmail", type="string", length = 255)
     */
    public $hrEmail;
    /**
     * @var text
     *
     * @ORM\Column(name="defaultFormLocation", type="string", length = 255)
     */
    public $defaultFormLocation;
    /**
     * @var text
     *
     * @ORM\Column(name="forgotPasswordLink", type="string", length = 255)
     */
    public $forgotPasswordLink;  
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
                
            $this->$key = "";
        }
    }    
}

