<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class campaigns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */    
    
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="mailEngine", type="string", length=255)
     */
    public $mailEngine;
    /**
     * @var string
     *
     * @ORM\Column(name="mailEngineCampaignid", type="string", length=255)
     */
    public $mailEngineCampaignid; 
     /**
     * @var string
     *
     * @ORM\Column(name="mailEngineTemplateid", type="string", length=255)
     */
    public $mailEngineTemplateid;
         /**
     * @var string
     *
     * @ORM\Column(name="mailEngineListid", type="string", length=255)
     */
    public $mailEngineListid; 
         /**
     * @var string
     *
     * @ORM\Column(name="mailEngineSendid", type="string", length=255)
     */
    public $mailEngineSendid;     
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;   
    /**
     * @var string
     *
     * @ORM\Column(name="categoryid", type="integer")
     */
    public $categoryid;   
    /**
     * @var string
     *
     * @ORM\Column(name="templateid", type="integer")
     */
    public $templateid;
    /**
     * @var string
     *
     * @ORM\Column(name="textTemplateid", type="integer")
     */
    public $textTemplateid;      
    /**
     * @var string
     *
     * @ORM\Column(name="signatureid", type="integer")
     */
    public $signatureid; 
    /**
     * @var string
     *
     * @ORM\Column(name="disclaimerid", type="integer")
     */
    public $disclaimerid;
    /**
     * @var string
     *
     * @ORM\Column(name="adviserid", type="integer")
     */
    public $adviserid;    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    public $title;    
    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255)
     */
    public $subject;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    public $description;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="html", type="text")
     */
    public $html;   
    /**
     * @var string
     *
     * @ORM\Column(name="textBody", type="text")
     */
    public $textBody;      
    /**
     * @var string
     *
     * @ORM\Column(name="fromEmail", type="string", length=255)
     */
    public $fromEmail;  
    /**
     * @var string
     *
     * @ORM\Column(name="fromName", type="string", length=255)
     */
    public $fromName;
   
    /**
     * @var string
     *
     * @ORM\Column(name="sentDate", type="string", length=255)
     */
    public $sentDate;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255)
     */
    public $tags;    
    /**
     * @var string
     *
     * @ORM\Column(name="gacampaignid", type="string", length=50)
     */
    public $gacampaignid;        
    /**
     * @var string
     *
     * @ORM\Column(name="sent", type="smallint")
     */
    public $sent; 
    /**
     * @var string
     *
     * @ORM\Column(name="communicationSpecialist", type="text")
     */
    public $communicationSpecialist;     
    
    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            $this->$key = "";
        }
        $salt = uniqid(mt_rand()); 
        $this->gacampaignid = time()."_".substr(hash('sha1', $salt.rand(1,1000000)),0,12);
    }

}
