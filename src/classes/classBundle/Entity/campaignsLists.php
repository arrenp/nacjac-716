<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class campaignsLists
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */    
    
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="emailAddress", type="string", length=255)
     */
    public $emailAddress;    
     /**
     * @var string
     *
     * @ORM\Column(name="campaigns", type="text")
     */
    public $campaigns; 
    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    public $firstName;  
    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    public $lastName;    
    
    public function __construct()//do not remove, will fail for doctrine add function
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            $this->$key = "";
        }
    }

}
