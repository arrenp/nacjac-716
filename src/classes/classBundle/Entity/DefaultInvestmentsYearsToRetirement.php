<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsYearsToRetirement;
use Doctrine\ORM\Mapping as ORM;
/**
 * DefaultInvestmentsYearsToRetirement
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultInvestmentsYearsToRetirement extends InvestmentsYearsToRetirement
{
    
}
