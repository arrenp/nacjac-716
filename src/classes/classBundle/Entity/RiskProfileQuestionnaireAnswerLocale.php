<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class RiskProfileQuestionnaireAnswerLocale extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="riskProfileQuestionnaireAnswerId", type="integer")
     */
    public $riskProfileQuestionnaireAnswerId;
    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer")
     */
    public $languageId;
    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text")
     */
    public $answer;
}



