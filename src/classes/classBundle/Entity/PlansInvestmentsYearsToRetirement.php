<?php
namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\InvestmentsYearsToRetirement;
use Doctrine\ORM\Mapping as ORM;
/**
 * PlansInvestmentsYearsToRetirement
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class PlansInvestmentsYearsToRetirement extends InvestmentsYearsToRetirement 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
}
