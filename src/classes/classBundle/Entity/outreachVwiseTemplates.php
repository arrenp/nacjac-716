<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class outreachVwiseTemplates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="type", type="string",length = 250)
	 */
    public $type;
  	/**
     * @var integer
     *
     * @ORM\Column(name="templatename", type="string",length = 250)
     */
    public $templatename;

  	/**
     * @var integer
     *
     * @ORM\Column(name="imageurl", type="string",length = 250)
     */
    public $imageurl;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="videotemplateid", type="integer")
	 */
    public $videotemplateid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="link", type="string",length = 250)
	 */
    public $link;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="staticWebPageUrl", type="string",length = 250)
	 */
    public $staticWebPageUrl;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
