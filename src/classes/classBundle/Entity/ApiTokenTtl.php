<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApiTokenTtl
 *
 * @ORM\Table(name="api_token_ttl")
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ApiTokenTtl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ttl", type="integer")
     */
    private $ttl;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ttl
     *
     * @param integer $ttl
     *
     * @return ApiTokenTtl
     */
    public function setTtl($ttl)
    {   
        $this->ttl = $ttl;
        return $this;
    }

    /**
     * Get ttl
     *
     * @return integer
     */
    public function getTtl()
    {
        return $this->ttl;
    }
}

