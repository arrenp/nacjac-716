<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ATBlueprintOutlook
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class ATBlueprintOutlook
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="profileid", type="integer")
     */
    public $profileid;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;

    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;

    /**
     * @var float
     *
     * @ORM\Column(name="originalDeferralPercent", type="float")
     */
    public $originalDeferralPercent;

    /**
     * @var float
     *
     * @ORM\Column(name="deferralPercent", type="float")
     */
    public $deferralPercent;

    /**
     * @var integer
     *
     * @ORM\Column(name="pretaxPercent", type="integer")
     */
    public $pretaxPercent;

    /**
     * @var integer
     *
     * @ORM\Column(name="rothPercent", type="integer")
     */
    public $rothPercent;

    /**
     * @var integer
     *
     * @ORM\Column(name="currentPerMonth", type="integer")
     */
    public $currentPerMonth;

    /**
     * @var integer
     *
     * @ORM\Column(name="dollarsDeferred", type="integer")
     */
    public $dollarsDeferred;

    /**
     * @var boolean
     *
     * @ORM\Column(name="improvedOutlook", type="boolean")
     */
    public $improvedOutlook;

    /**
     * @var boolean
     *
     * @ORM\Column(name="complete", type="boolean")
     */
    public $complete;

    public function __construct() {
        $this->complete = 0;

    }
}

