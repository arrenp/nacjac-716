<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class videoLibStats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
     /**
     * @var string
     *
     * @ORM\Column(name="partnerID", type="string", length=45)
     */
    public $partnerID;
    /**
     * @var string
     *
     * @ORM\Column(name="playDuration", type="string", length=45)
     */
    public $playDuration;  
     /**
     * @var string
     *
     * @ORM\Column(name="participantID", type="string", length=45)
     */
    public $participantID;   
      /**
     * @var string
     *
     * @ORM\Column(name="videoPlayed", type="string", length=45)
     */
    public $videoPlayed;    
    /**
     * @var string
     *
     * @ORM\Column(name="planProvider", type="string", length=45)
     */
    public $planProvider;
     /**
     * @var string
     *
     * @ORM\Column(name="widgetKey", type="string", length=45)
     */
    public $widgetKey;
       /**
     * @var string
     *
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;  
     /**
     * @var string
     *
     * @ORM\Column(name="videoLength", type="string", length=45)
     */
    public $videoLength;
      /**
     * @var integer
     *
     * @ORM\Column(name="percentageViewed", type="integer")
     */
    public $percentageViewed;  
    
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($this->$key != "id")
            $this->$key = "";
        }
    }

}
