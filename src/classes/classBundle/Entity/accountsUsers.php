<?php

namespace classes\classBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class accountsUsers extends BaseUser
{

    /**
     * @ORM\ManyToOne(targetEntity="accounts", inversedBy="accounts")
     * @ORM\JoinColumn(name="userid", referencedColumnName="id")
     */
    public $account;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="userid",type="integer",nullable=true)
	 */
    public $userid;
    /**
     * @var string
     *
     * @ORM\Column(name="roleid",type="integer",nullable=true)
     */
    public $roleid;
    /**
     * @var string
     * admin,adviser,etc
     * @ORM\Column(name="roleType", type="string", length=255,nullable=true)
     */ 
    public $roleType;
    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255,nullable=true)
     */
    public $firstname;
    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255,nullable=true)
     */
    public $lastname;


    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255,nullable=true)
     */
    public $phone;
    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=255,nullable=true)
     */
    public $phone2;
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255,nullable=true)
     */
    public $address;
    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255,nullable=true)
     */
    public $address2;
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255,nullable=true)
     */
    public $city;
    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255,nullable=true)
     */
    public $state;
    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=255,nullable=true)
     */
    public $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="notificationEmail", type="string", length=255,nullable=true)
     */
    public $notificationEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="notificationPhone", type="string", length=255,nullable=true)
     */
    public $notificationPhone;
   /**
     * @var string
     *
     * @ORM\Column(name="notificationSMS", type="string", length=255,nullable=true)
     */
    public $notificationSMS;
    /**
     * @var string
     *
     * @ORM\Column(name="notificationEmailActive", type="smallint", nullable=true)
     */
    public $notificationEmailActive;
    /**
     * @var string
     *
     * @ORM\Column(name="notificationPhoneActive", type="smallint", nullable=true)
     */
    public $notificationPhoneActive;
   /**
     * @var string
     *
     * @ORM\Column(name="notificationSMSActive", type="smallint", nullable=true)
     */
    public $notificationSMSActive;
    /**
     * @var string
     *
     * @ORM\Column(name="migrationPassword", type="string", length=255,nullable=true)
     */
    public $migrationPassword;
    /**
     * @var string
     *
     * @ORM\Column(name="autoAddAccount", type="smallint")
     */
    public $autoAddAccount;
    /**
     * @var string
     *
     * @ORM\Column(name="allPlans", type="smallint")
     */
    public $allPlans;
    
    /**
     * @var string
     *
     * @ORM\Column(name="passwordHistory", type="string", length=1000)
     */
    public $passwordHistory;   
    /**
     * @var string
     *
     * @ORM\Column(name="passwordExpirationDate", type="datetime")
     */
    public $passwordExpirationDate;   
    /**
     * @var string
     *
     * @ORM\Column(name="loginAttempts", type="integer")
     */
    public $loginAttempts;     
    public function setPlainPassword($password)
    {
        $this->errors = array();
        if (strlen($password) < 6 ) {
            $this->errors[] = "Password must be at least six characters";
        }

        if ($this->letterCount($password,'a','z') == 0) {
            $this->errors[] = "Must have at least one lowercase letter";
        }

        if ($this->letterCount($password,'A','Z') == 0) {
            $this->errors[] = "Must have at least one uppercase letter";
        }

        if ($this->letterCount($password,'0','9') == 0) {
            $this->errors[] = "Must have at least one number";
        }

        $passwordArray = str_split($password);
        $specialChar = false;
        foreach ($passwordArray as $character)
        {
            if ( !($character >= 'a'  && $character <= 'z')  && !($character >= 'A'  && $character <= 'Z') && !($character >= '0'  && $character <= '9')) {
                $specialChar = true;

            }
        }
        if (!$specialChar) {
            $this->errors[] = "Must have at least one special character";
        }

        global $kernel;
        $encoderService = $kernel->getContainer()->get('security.encoder_factory');
        $encoder        = $encoderService->getEncoder($this);
        $passwordHistoryArray = json_decode($this->passwordHistory);
        $currentDay =  new \DateTimeImmutable('now');
        
        foreach ($passwordHistoryArray as $oldPass)
        {
            if ($encoder->isPasswordValid($oldPass->password,$password, $oldPass->salt)) {
                $this->errors[] = "Password has been used in the last five password changes";
            }
            
            if (!empty($oldPass->passwordBirthDay)) {
                $passwordBirthDay = new \DateTime($oldPass->passwordBirthDay);
                
                if ((int)$passwordBirthDay->diff($currentDay)->format("%a") < 1) {
                    $this->errors[] = "Password can only be changed once a day";
                }
            }
        }

        if (count($this->errors) == 0)
        {
            $this->plainPassword = $password;
            $passwordHistory = array();
            $passwordHistory['password'] = $encoder->encodePassword($password, $this->getSalt());
            $passwordHistory['salt'] = $this->getSalt();  
            $passwordHistory['passwordBirthDay'] = $currentDay->format("Y-m-d H:i:s");
            
            if (count($passwordHistoryArray) == 0) {
                $passwordHistoryArray[] = $passwordHistory;
            } else {
                array_unshift($passwordHistoryArray,$passwordHistory);
            }
            if (count($passwordHistoryArray) > 5) {
                array_pop($passwordHistoryArray);
            }
            $this->passwordHistory = json_encode($passwordHistoryArray);
            $this->passwordExpirationDate = $currentDay->modify("+180 days");
        }
        return $this;
    }

    public function letterCount($password,$start,$finish)
    {
        $count = 0;
        for (; $start <= $finish; $start++) {
            if (substr_count($password,$start) > 0 ) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getAccountsGroups($entityClass)//getGroups already taken by vendor
    {
        return $this->account->getGroups($entityClass);
    }
    public function isLocked()
    {
        if (!$this->enabled)
        {
            return true;
        }
        return $this->loginAttempts >= 5;   
    }
    
    public function isAccountNonLocked() {
        if (!$this->enabled) {
            return false;
        }
        return $this->loginAttempts < 5;
    }
       
          
    public function unlock()
    {
        $this->locked = 0;
        $this->loginAttempts = 0;
    }


    public function __construct()//do not remove, will fail for doctrine add function
    {
	 	
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ( !in_array($key,array("lastLogin","expiresAt","credentialsExpireAt","passwordRequestedAt","passwordExpirationDate","confirmationToken")) ) {
                $this->$key  = "";
            }
        }
	 	parent::__construct();
	}
    
    public function setEmail($email) {
        parent::setEmail(trim($email));
    }
    
    public function setUsername($username) {
        parent::setUsername(trim($username));
    }
}
