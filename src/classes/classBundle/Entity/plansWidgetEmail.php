<?php
namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use classes\classBundle\Entity\abstractclasses\rkpEmail;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansWidgetEmail extends rkpEmail
{ 
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;      
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45)
     */
    public $type; 
}
