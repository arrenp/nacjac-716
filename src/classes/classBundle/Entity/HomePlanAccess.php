<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class HomePlanAccess {
	/**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
	/**
     * @ORM\Column(name="planid", type="integer")
     */
	public $planid;
	/**
     * @ORM\Column(name="accountsUsersId", type="integer")
     */
	public $accountsUsersId;
}
