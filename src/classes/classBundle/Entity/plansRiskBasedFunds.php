<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansRiskBasedFunds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
  	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
  	/**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    public $name;
    /**
     * @var string
     *
     * @ORM\Column(name="profile", type="string", length=10)
     */
    public $profile;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="minscore", type="integer")
	 */
    public $minscore;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="maxscore", type="integer")
	 */
    public $maxscore;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="thMinscore", type="integer")
	 */
	public $thMinscore;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="thMaxscore", type="integer")
	 */
	public $thMaxscore;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="orderid", type="integer")
	 */
    public $orderid;
    /**
     * @var integer
     *
     * @ORM\Column(name="customid", type="string",length=100)
     */
    public $customid; 
    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
