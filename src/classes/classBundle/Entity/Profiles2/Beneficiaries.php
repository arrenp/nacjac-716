<?php
namespace classes\classBundle\Entity\Profiles2;
class Beneficiaries extends BaseData
{
    public function __construct()
    {        
        $params = [];
        $params['string'] = 
        [
            "type","firstName","lastName","gender","relationship","maritalStatus","ssn",
            "city","state","zip","dob","address1","address2","phone","level","spousalWaiverForm","country","middleInitial"
        ];
        $params['float']  = ["percent"];
        $params['boolean'] = ["trustEstateCharityOther","foreignaddress"];
        $this->initializeFields($params);
    }      
}
