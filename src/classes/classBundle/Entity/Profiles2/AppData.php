<?php
namespace classes\classBundle\Entity\Profiles2;
class AppData extends BaseData
{
    public function __construct()
    {
        $params['string'] = 
            ["paidFrequency","mode","cmode","planName","planId","beneficiaryStatus","investmentsStatus","realignmentStatus",
            "contributionsStatus","catchupContributionStatus","aTTransactStatus","adviceStatus","aCAOptOutStatus",
            "enrollmentStatus","availability","autoEnrollType","autoEnrollDefaultFundName","autoEnrollDefaultFundTicker",
            "autoEscalateFundName","autoEscalateTicker","autoIncreaseFrequency","autoIncreaseType","riskQuestinonnaire","investmentType",
            "currentSection", "currentStep","firstName","lastName","email","riskXml","riskScore","riskThScore",
            "recommendedPortfolioName","riskProfileStatus","riskAnswers",
            "riskPosition","riskName","riskDescription","stadionPathId","portfolioName","defaultInvestmentType",
            "autoIncreaseStartDate","autoEscalateStartDate"
            ];
        $params['boolean'] = ["isAca","isAcaOptout","autoEnrollEnabled","autoEnrollDefaultFund","autoEnrollDefaultFundBasedOnAge",
            "eCOMMModal","autoEscalateEnabled","autoIncreaseEnabled","autoIncreaseFrequencyEnabled",
            "autoIncreaseOptIn", "autoEnrollOptOut", "autoEscalateOptOut"];
        $params['float'] = 
            ["currentYearlyIncome","pre","roth","post","catchup","target","projected","projectedEmployer",
            "preCurrent","rothCurrent","postCurrent","deferralPercent","deferralDollar","prePercent","preDollar",
            "rothPercent","rothDollar","postPercent","postDollar","currentBalance","autoEnrollPre","autoEnrollRoth","autoEnrollPost",
             "autoEscalatePercent","autoIncreaseAmountLimit","autoIncreasePercentLimit","autoIncreaseTimeLimit","estimatedRetirementIncome",
            "estimatedSocialSecurityIncome","replacementIncome","inflationAdjustedReplacementIncome","otherAssets","totalAssets",
            "lifeExpectancy","annualIncomeNeededInRetirement","estimatedSavingsAtRetirement","recommendedMonthlyPlanContribution","lifeExpectancy",
            "estimatedPensionIncome","autoIncreasePre","autoIncreaseRoth","autoIncreasePost","autoIncreaseCap","ContributionDlr",
            "annualIncomeDisplay","monthlyContributionTotal","monthlyContributionTotalEmployer","autoEscalatePre","autoEscalatePost",
            "autoEscalateRoth", "ContributionPct"
            ];
        $params['int'] = [
            "retireAge","autoIncreaseAmountDecimalPlaces","yearsBeforeRetirement",
            "yearsLiveInRetirement","percentageOfIncomeFromPlan","currentAge"
        ];
        $this->initializeFields($params);
        $this->fields['assetTypes']['type'] = "assetTypes";
        $this->fields['assetTypes']['value'] = new AssetTypes();
        $this->fields['investments']['type'] = "investments";
        $this->fields['investments']['value'] = [];
        $this->fields['beneficiaries']['type'] = "beneficiaries";
        $this->fields['beneficiaries']['value'] = [];
        ksort($this->fields);
    }
    public function set($field,$value)
    {
        if (parent::set($field,$value))
        {
            if ($field == "assetTypes")
                $this->fields['assetTypes']['value']->setFields($value);
            else if (in_array($field, ["investments","beneficiaries","questionnaire"]))
            {
                $this->fields[$field]['value'] = [];
                foreach ($value as $params)
                {
                    $classPath = 'classes\classBundle\Entity\Profiles2\\' . ucfirst($field);
                    $object = new $classPath;
                    $object->setFields($params);
                    $this->fields[$field]['value'][] = $object;
                }
            }
        }
    }
    public function getFullData()
    {
        $data =  $this->getData();
        $arrayTables = ["investments","beneficiaries"];
        foreach ($arrayTables as $table)       
            foreach ($data[$table]['value'] as $key => $investment)
                $data[$table]['value'][$key] = $data[$table]['value'][$key]->getData(); 
        $data["assetTypes"]['value'] = $data["assetTypes"]['value']->getData();
        $convertedData = [];
        foreach ($data as $key => $value)
            $convertedData[$key] = $value['value'];
        foreach ($convertedData['assetTypes'] as $key => $assetType)        
            $convertedData['assetTypes'][$key] = $assetType['value'];
        foreach ($arrayTables as $table)
            foreach ($convertedData[$table] as  $key => $array)
                foreach ($array as $key2 => $row)
                    $convertedData[$table][$key][$key2] = $row['value'];
        
        return  $convertedData;
    }
    public function getFullDataJson()
    {
        return json_encode($this->getFullData());
    }
    public function loadJson($json)
    {
        $jsonDecode = json_decode($json,true);
        foreach ($jsonDecode as $field => $value)
        $this->set($field, $value);        
    }
}
