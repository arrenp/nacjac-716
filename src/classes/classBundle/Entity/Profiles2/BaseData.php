<?php
namespace classes\classBundle\Entity\Profiles2;
class BaseData 
{
    protected $fields;
    protected function initializeFields($params)
    {
        foreach ($params['string'] as $type)        
            $this->fields[$type]['type'] = "string";
        foreach ($params['boolean'] as $type)
            $this->fields[$type]['type'] = "boolean";
        foreach ($params['float'] as $type)
            $this->fields[$type]['type'] = "float"; 
        foreach ($params['int'] as $type)
            $this->fields[$type]['type'] = "int";
        foreach ($params['DateTime'] as $type)
            $this->fields[$type]['type'] = "DateTime";
        foreach ($this->fields as $key => $field)
          $this->fields[$key]['value'] = null;
    }
    public function set($field,$value)
    {
        if ($value === null)
        {
            $this->setValue($field,$value);
            return false;
        }
        else
        switch ($this->fields[$field]['type'])
        {
            case "int":
                $this->setIntValue($field, $value);
                break;
            case "float":
                $this->setFloatValue($field, $value);
                break;
            case "string":
                $this->setStringValue($field, $value);
                break;
            case "boolean":
                $this->setBooleanValue($field, $value);
                break;
            case "DateTime":
                $this->setDateTimeValue($field, $value);
                break;
        }
        return true;
    }
    public function setFields($params)
    {
        foreach ($params as $key => $value)
            $this->set($key,$value);
    }
    public function getData()
    {
        return $this->fields;
    }
    public function getDataJson()
    {
        return json_encode($this->getData());
    }
    public function getField($field)
    {
        return $this->fields[$field];
    }
    public function getFieldValue($field)
    {
        return $this->getField($field)['value'];
    }  
    protected function setValue($field,$value)
    {
        if (array_key_exists('value',$this->fields[$field]))
        $this->fields[$field]['value']  = $value;
    }
    protected function setIntValue($field,$value)
    {
       $this->setValue($field,(int)$value);
    }
    protected function setFloatValue($field,$value)
    {
       $this->setValue($field,(float)$value);
    }
    protected function setStringValue($field,$value)
    {
       $this->setValue($field,(string)$value);
    }
    protected function setBooleanValue($field,$value)
    {
       if ($value === "true" )           
           $value = true;
       if ($value === "false")
           $value = false;
       $this->setValue($field,(boolean)$value);
    }
    protected function setDateTimeValue($field,$value)
    {
        if (is_string($value))       
            $value = new \DateTime($value);                    
        if ($value instanceof \DateTime)
            $this->setValue ($field, $value);
    }  
}
