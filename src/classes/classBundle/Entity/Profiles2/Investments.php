<?php
namespace classes\classBundle\Entity\Profiles2;
class Investments extends BaseData
{
    public function __construct()
    {        
        $params = [];
        $params['string'] = ["portfolioName","fundName","fundFactLink","prospectusLink"];
        $params['float']  = ["percent"];
        $this->initializeFields($params);
    }    
}
