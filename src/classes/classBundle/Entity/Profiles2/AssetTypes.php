<?php
namespace classes\classBundle\Entity\Profiles2;

class AssetTypes extends BaseData
{
    public function __construct()
    {        
        $params['boolean'] = [];
        for ($i = 1; $i <= 8; $i ++)
        $params['boolean'][] = "asset_type_option_".$i;
        $this->initializeFields($params);
    }    
}
