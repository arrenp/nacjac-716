<?php

namespace classes\classBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class AppSectionList extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var string
     *
     * @ORM\Column(name="section", type="string", length=50)
     */
    public $section;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=50)
     */
    public $description;
    /**
     * @var integer
     *
     * @ORM\Column(name="removeable", type="smallint")
     */
    public $removable;
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=50)
     */
    public $path;
    public function __construct()
    {
        $this->removable = 1;
    }
}