<?php
namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultContent extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="contentTypeId", type="integer")
     */
    public $contentTypeId;
    /**
     * @var integer
     *
     * @ORM\Column(name="sectionListId", type="integer")
     */
    public $sectionListId;
    /**
     * @var string
     *
     * @ORM\Column(name="step", type="string", length=255)
     */
    public $step;
    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     */
    public $label;
    /**
     * @var string
     *
     * @ORM\Column(name="englishContent", type="string", length=255)
     */
    public $englishContent;
    /**
     * @var string
     *
     * @ORM\Column(name="spanishContent", type="string", length=255)
     */
    public $spanishContent;
    /**
     * @var integer
     *
     * @ORM\Column(name="stepListId", type="integer")
     */
    public $stepListId;
    /**
     * @ORM\ManyToOne(targetEntity="AppStepList")
     * @ORM\JoinColumn(name="stepListId", referencedColumnName="id")
     */
    public $appStep;
}
