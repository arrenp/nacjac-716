<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansMedia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;
    /**
     * @var integer
     *
     * @ORM\Column(name="welcome", type="string", length = 100)
     */
    public $welcome;    
    /**
     * @var integer
     *
     * @ORM\Column(name="goodbye", type="string", length = 100)
     */
    public $goodbye;   
     /**
     * @var integer
     *
     * @ORM\Column(name="welcome_sc", type="string", length = 100)
     */
    public $welcome_sc;    
    /**
     * @var integer
     *
     * @ORM\Column(name="goodbye_sc", type="string", length = 100)
     */
    public $goodbye_sc;   
	/**
     * @var integer
     *
     * @ORM\Column(name="enddate_sc", type="date")
     */
    public $enddate_sc;   

    
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)               
        $this->$key = "";   
    }
}


