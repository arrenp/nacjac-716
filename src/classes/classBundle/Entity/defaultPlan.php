<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class defaultPlan extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
 	/**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
	public $userid;
    /**
     * @var integer
     *
     * @ORM\Column(name="appVersion", type="integer")
     */
    public $appVersion;
    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    public $company;
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50)
     */
    public $phone;
    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=50)
     */
    public $phone2;
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    public $address;
    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255)
     */
    public $address2;
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    public $city;
    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255)
     */
    public $state;
    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=10)
     */
    public $zip;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    public $email;
    /**
     * @var string
     *
     * @ORM\Column(name="redirectWebsiteAddress", type="string", length=255)
     */
    public $redirectWebsiteAddress;
	/**
     * @var string
     *
     * @ORM\Column(name="timeoutWebsiteAddress", type="string", length=255)
     */
    public $timeoutWebsiteAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="participantLoginWebsiteAddress", type="string", length=255)
     */
    public $participantLoginWebsiteAddress;
    /**
     * @var string
     *
     * @ORM\Column(name="errorNotificationEmail", type="string", length=255)
     */
    public $errorNotificationEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="profileNotificationEmail", type="string", length=255)
     */
    public $profileNotificationEmail;
    /**
     * @var string
     *
     * @ORM\Column(name="defaultEmailAddress", type="string", length=255)
     */
    public $defaultEmailAddress;
    /**
     * @var string
     *
     * @ORM\Column(name="providerLogoImage", type="string", length=255)
     */
    public $providerLogoImage;
    /**
     * @var string
     *
     * @ORM\Column(name="providerLogoImageThumb", type="string", length=255)
     */
    public $providerLogoImageThumb;
    /**
     * @var string
     *
     * @ORM\Column(name="providerLogoEmailImage", type="string", length=255)
     */
    public $providerLogoEmailImage;
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorLogoEmailImage", type="string", length=255)
     */
    public $sponsorLogoEmailImage;     
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorLogoImage", type="string", length=255)
     */
    public $sponsorLogoImage;
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorLogoImageThumb", type="string", length=255)
     */
    public $sponsorLogoImageThumb;
    /**
     * @var string
     *
     * @ORM\Column(name="bannerImage", type="string", length=255)
     */
    public $bannerImage;
    /**
     * @var string
     *
     * @ORM\Column(name="bannerLinkUrl", type="string", length=255)
     */
    public $bannerLinkUrl;
    /**
     * @var string
     *
     * @ORM\Column(name="interfaceColor", type="string", length=255)
     */
    public $interfaceColor;
    /**
     * @var string
     *
     * @ORM\Column(name="interfaceLayout", type="string", length=255)
     */
    public $interfaceLayout;
    /**
     * @var string
     *
     * @ORM\Column(name="advice", type="text",length = 65535)
     */
    public $advice;
    /**
     * @var string
     *
     * @ORM\Column(name="adviceStatus", type="smallint")
     */
    public $adviceStatus;
     /**
     * @var string
     *
     * @ORM\Column(name="adviceCompare", type="text",length = 65535)
     */
    public $adviceCompare;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="adviceDate", type="date")
	 */
    public $adviceDate;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="riskType", type="string", length=10)
	 */
    public $riskType;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="fundOrder", type="string", length=50)
	 */
    public $fundOrder;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="portfolioOrder", type="string", length=50)
	 */
    public $portfolioOrder;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="investmentsAutoScoring", type="smallint", length=50)
	 */
    public $investmentsAutoScoring;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="vadFrequency", type="string", length=50)
	 */
	public $vadFrequency;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="MSTClientSessionID", type="string", length=100)
	 */
	public $MSTClientSessionID;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="fundLink", type="smallint")
	 */
	public $fundLink;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="IRSCode", type="string",length = 20)
	 */
	public $IRSCode;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="defaultInvestment", type="smallint")
	 */
	public $defaultInvestment;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="deferralOff", type="smallint")
	 */
	public $deferralOff;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="pipContent", type="smallint")
	 */
	public $pipContent;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="pipPosition", type="smallint")
	 */
	public $pipPosition;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="matchPercent", type="integer")
	 */
	public $matchPercent;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="secondaryMatchCap", type="integer")
	 */
	public $secondaryMatchCap;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="secondaryMatchPercent", type="integer")
	 */
	public $secondaryMatchPercent;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="morningstar", type="integer")
	 */
	public $morningstar;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="deleted", type="smallint")
	 */
	public $deleted;	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="createdDate", type="datetime")
	 */
	public $createdDate;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="modifiedDate", type="datetime")
	 */
	public $modifiedDate;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="deletedDate", type="datetime")
	 */
	public $deletedDate;
    /**
     * @var string
     *
     * @ORM\Column(name="sections", type="string",length = 100)
     */
    public $sections;
    /**
     * @var string
     *
     * @ORM\Column(name="reportsStylesHeader", type="string",length = 20)
     */    
    public $reportsStylesHeader;
    /**
     * @var string
     *
     * @ORM\Column(name="reportsStylesFont", type="string",length = 20)
     */    
    public $reportsStylesFont;
    /**
     * @var string
     *
     * @ORM\Column(name="reportsInnerTable", type="string",length = 20)
     */    
    public $reportsInnerTable;
    /**
     * @var string
     *
     * @ORM\Column(name="reportsBackground", type="string",length = 20)
     */    
    public $reportsBackground;    
        /**
     * @var string
     *
     * @ORM\Column(name="sponsorConnect", type="smallint")
     */
    public $sponsorConnect; 
    /**
     * @var string
     *
     * @ORM\Column(name="sponsorConnectTemplate", type="text")
     */    
    public $sponsorConnectTemplate;    
    /**
     * @var string
     *
     * @ORM\Column(name="investorProfileEmail", type="string", length = 255)
     */
    public $investorProfileEmail;    
    /**
     * @var string
     *
     * @ORM\Column(name="advisorFirst", type="string", length = 100)
     */
    public $advisorFirst;
    /**
     * @var string
     *
     * @ORM\Column(name="advisorLast", type="string", length = 100)
     */
    public $advisorLast; 
      /**
     * @var string
     *
     * @ORM\Column(name="advisorEmail", type="string", length = 100)
     */
    public $advisorEmail;   
    /**
     * @var string
     *
     * @ORM\Column(name="advisorPhone", type="string", length = 100)
     */
    public $advisorPhone; 
    /**
     * @var string
     *
     * @ORM\Column(name="smartEnrollAllowed", type="smallint")
     */
    public $smartEnrollAllowed;
     /**
     * @var string
     *
     * @ORM\Column(name="smartPlanAllowed", type="smallint")
     */
    public $smartPlanAllowed; 
    /**
     * @var string
     *
     * @ORM\Column(name="irioAllowed", type="smallint")
     */
    public $irioAllowed;
    /**
     * @var string
     *
     * @ORM\Column(name="powerviewAllowed", type="smallint")
     */
    public $powerviewAllowed;     
    /**
    * @var integer
    *
    * @ORM\Column(name="fundGroupid", type="integer")
     */
    public $fundGroupid;
    /**
     * @var integer
     *
     * @ORM\Column(name="profileNotificationShowEmail", type="integer")
     */
    public $profileNotificationShowEmail;
    /**
     * @var integer
     *
     * @ORM\Column(name="enrollmentRedirectOn", type="smallint")
     */
    public $enrollmentRedirectOn;
    /**
     * @var string
     *
     * @ORM\Column(name="enrollmentRedirectAddress", type="string", length=255)
     */
    public $enrollmentRedirectAddress;
    
    public function __construct()
    {
        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {   
        	if ($this->$key != "id")
            $this->$key = "";
            if ($key == "adviceDate" || $key == "createdDate" || $key == "modifiedDate" || $key == "deletedDate")
            $this->$key = new \DateTime("0000-00-00");
                   
        }
        $this->investmentsAutoScoring = 1;
        $this->riskType = "RBP";
    }    


}
