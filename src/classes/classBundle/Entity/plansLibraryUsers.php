<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plansLibraryUsers
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class plansLibraryUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="planid", type="integer")
     */
    public $planid;

    /**
     * @var integer
     *
     * @ORM\Column(name="videoid", type="integer")
     */
    public $videoid;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set planid
     *
     * @param integer $planid
     *
     * @return plansLibraryUsers
     */
    public function setPlanid($planid)
    {
        $this->planid = $planid;

        return $this;
    }

    /**
     * Get planid
     *
     * @return integer
     */
    public function getPlanid()
    {
        return $this->planid;
    }

    /**
     * Set videoid
     *
     * @param integer $videoid
     *
     * @return plansLibraryUsers
     */
    public function setVideoid($videoid)
    {
        $this->videoid = $videoid;

        return $this;
    }

    /**
     * Get videoid
     *
     * @return integer
     */
    public function getVideoid()
    {
        return $this->videoid;
    }
}

