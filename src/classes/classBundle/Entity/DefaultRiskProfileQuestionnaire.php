<?php

namespace classes\classBundle\Entity;
use classes\classBundle\Entity\abstractclasses\RiskProfileQuestionnaireSelected;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class DefaultRiskProfileQuestionnaire extends RiskProfileQuestionnaireSelected
{
    
}