<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="media_localization")
 * @ORM\EntityListeners({"entityListener"})
 */
class MediaLocalization
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * The media that is linked to this localization
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="localizations")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    public $media;

    /**
     * The standardized abbreviation for the localization's language
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=7)
     */
    public $language;

    /**
     * The path of the original master file for this media
     * @var string
     *
     * @ORM\Column(name="master_file", type="string", length=255)
     */
    public $masterFile;

    /**
     * The path of the VTT file for this media
     * @var string
     *
     * @ORM\Column(name="captions_file", type="string", length=255)
     */
    public $captionsFile;

    /**
     * The version of the media, usually in the format MMDDYYYY
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=15)
     */
    public $version;

    /**
     * The duration of the media in seconds
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer")
     */
    public $duration;

    /**
     * Store the conversion state of the media.
     * Returns true if the media has been converted to the web ready formats, false otherwise
     * @var boolean
     *
     * @ORM\Column(name="ready", type="boolean")
     */
    public $ready;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    public $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    public $updatedAt;
    /**
     * 
     * @var string
     *
     * @ORM\Column(name="transcriptFile", type="string", length=255)
     */
    public $transcriptFile;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set media
     *
     * @param Media $media
     *
     * @return MediaLocalization
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return MediaLocalization
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set masterFile
     *
     * @param string $masterFile
     *
     * @return MediaLocalization
     */
    public function setMasterFile($masterFile)
    {
        $this->masterFile = $masterFile;

        return $this;
    }

    /**
     * Get masterFile
     *
     * @return string
     */
    public function getMasterFile()
    {
        return $this->masterFile;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return MediaLocalization
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set captionsFile
     *
     * @param string $captionsFile
     *
     * @return MediaLocalization
     */
    public function setCaptionsFile($captionsFile)
    {
        $this->captionsFile = $captionsFile;

        return $this;
    }

    /**
     * Get captionsFile
     *
     * @return string
     */
    public function getCaptionsFile()
    {
        return $this->captionsFile;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return MediaLocalization
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set ready
     *
     * @param boolean $ready
     *
     * @return MediaLocalization
     */
    public function setReady($ready)
    {
        $this->ready = $ready;

        return $this;
    }

    /**
     * Get ready
     *
     * @return boolean
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return MediaLocalization
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return MediaLocalization
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    public function getTranscriptFile()
    {
        return $this->transcriptFile;
    }
    public function setTranscriptFile($file)
    {
        $this->transcriptFile = $file;
    }    
    public function __construct()
    {
        $this->transcriptFile = "";
    }
    
}

