<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class surveyResults{
    
    
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    
     /**
     * @var string
     *
     * @ORM\Column(name="partnerId", type="string", length=45)
     */
    public $partnerId;
    
     /**
     * @var string
     *
     * @ORM\Column(name="planId", type="string", length=45)
     */
    public $planId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="results", type="string", length=255)
     */
    public $results;
    
    /**
     * @var string
     *
     * @ORM\Column(name="userIp", type="string", length=45)
     */
    public $userIp;    
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    public $dateStamp;    
    
    public function __construct()//do not remove, will fail for doctrine add function
    {

        $class_vars = get_class_vars(get_class($this));
        foreach ($class_vars as $key => $value)
        {
            if ($this->$key != "id") $this->$key = "";

        }
        
    }    
    
}

