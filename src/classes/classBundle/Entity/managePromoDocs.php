<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 */
class managePromoDocs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=1000)
     */
    public $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="orderid", type="integer")
     */
    public $orderid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="parentid", type="integer")
	 */
    public $parentid;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="additionalinfo", type="text")
	 */
    public $additionalinfo;

    public function __construct()
   	{
   		$class_vars = get_class_vars(get_class($this));
   		foreach ($class_vars as $key => $value)
   		{
   			if ($this->$key != "id")
     		$this->$key = "";
   		}
	}

}
