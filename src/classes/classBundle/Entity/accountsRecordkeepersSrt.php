<?php

namespace classes\classBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"entityListener"})
 *
 */
class accountsRecordkeepersSrt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    public $userid;
    /**
    * @var string
    *
    * @ORM\Column(name="siteToken", type="string", length=255)
    */
    public $siteToken;    
    /**
    * @var string
    *
    * @ORM\Column(name="userName", type="encrypted", length=255)
    */
    public $userName;  
    /**
    * @var string
    *
    * @ORM\Column(name="password", type="encrypted", length=255)
    */
    public $password;      
    /**
    * @var string
    *
    * @ORM\Column(name="type", type="string", length=255)
    */
    public $type;
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    public $url;
}

