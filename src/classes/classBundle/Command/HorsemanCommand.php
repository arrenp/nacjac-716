<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class HorsemanCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('app:HorsemanCommand')
            ->setDescription('Horseman csv planid renamer. Use a CSV file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $output->write("Enter Filename: ");
        $filename = rtrim(fgets(STDIN));
        if (!$this->endsWith($filename, ".csv")) {
            $filename .= ".csv";
        }

        if (($handle = fopen($filename, "r")) === FALSE) {
            $output->write("File not found at " . getcwd() . "/{$filename}\n");
            exit();
        }

        $output->write("Pick One:\n1) Migrate the planids from the old to the new\n2) Migrate the plan ids from the new to the old (revert): ");
        $option = rtrim(fgets(STDIN));

        if (strpos(strtolower($filename), 'iowa') !== false) {
            $tab = 1;
        } else if (strpos(strtolower($filename), 'national') !== false) {
            $tab = 2;
        }

        if (empty($tab)) {
            $output->write("Pick One:\n1) Iowa\n2) National: ");
            $tab = rtrim(fgets(STDIN));
        }

        if ($tab == 2) { // if national, because no ~ concat
            switch ($option) {
                case 1: $first = 2;
                    $second = 0;
                    break;
                case 2: $first = 0;
                    $second = 2;
                    break;
            }
        }

        $count = 0;
        $repository = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:plans");
        $repositorySurvey = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:SurveyEmployerPlans");
        $arraySet = array();

        $output->write("Starting planid rename job\n");
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            if (!$count++) {
                continue;
            }

            if ($tab == 1) {
                if ($option == 1) {
                    $plan = $repository->findBy(array('planid' => "{$data[0]}~{$data[1]}", 'partnerid' => 'HRCMNN_EDUCATE_PROD'));
                    $planSurvey = $repositorySurvey->findBy(array('planid' => "{$data[0]}~{$data[1]}"));
                    if (empty($plan)) {
                        $output->write("Planid not found: {$data[0]}~{$data[1]}\n");
                        continue;
                    } else if(array_key_exists("{$data[3]}", $arraySet)) {
                        $output->write("Multiple records with this new planid {$data[3]} has been found. Skipping\n");
                        continue;
                    }
                    $plan[0]->planid = $data[3];
                    $plan[0]->activePlanid = $data[3];
                    foreach ($planSurvey as $planS) {
                        $planS->planid = $data[3];
                        $em->persist($planS);
                    }
                    $arraySet[$data[3]] = true;
                    $em->persist($plan[0]);
                } else if ($option == 2) {
                    $plan = $repository->findBy(array('planid' => "{$data[3]}", 'partnerid' => 'HRCMNN_EDUCATE_PROD'));
                    $planSurvey = $repositorySurvey->findBy(array('planid' => "{$data[3]}"));
                    if (empty($plan)) {
                        $output->write("Planid not found: {$data[3]}\n");
                        continue;
                    } else if(array_key_exists("{$data[3]}", $arraySet)) {
                        $output->write("Multiple records with this new planid {$data[3]} has been found. Skipping\n");
                        continue;
                    }
                    $plan[0]->planid = "{$data[0]}~{$data[1]}";
                    $plan[0]->activePlanid = "{$data[0]}~{$data[1]}";
                    foreach ($planSurvey as $planS) {
                        $planS->planid = "{$data[0]}~{$data[1]}";
                        $em->persist($planS);
                    }
                    $arraySet[$data[3]] = true;
                    $em->persist($plan[0]);
                }
            } else if ($tab == 2) {
                $plan = $repository->findBy(array('planid' => "{$data[$second]}", 'partnerid' => 'HRCMNN_EDUCATE_PROD'));
                $planSurvey = $repositorySurvey->findBy(array('planid' => "{$data[$second]}"));
                if (empty($plan)) {
                    $output->write("Planid not found: {$data[$second]}\n");
                    continue;
                } else if(array_key_exists("{$data[$second]}", $arraySet)) {
                    $output->write("Multiple records with this new planid {$data[$second]} has been found. Skipping\n");
                    continue;
                }
                $plan[0]->planid = $data[$first];
                $plan[0]->activePlanid = $data[$first];
                foreach ($planSurvey as $planS) {
                    $planS->planid = $data[$first];
                    $em->persist($planS);
                }
                $data[$second] = true;
                $em->persist($plan[0]);
            }

            if ($count % 100 == 0) {
                $em->flush();
            }
        }
        $em->flush();
        $output->write("Completed planid rename job\n");
        fclose($handle);
    }

    public function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, -$length) === $needle);
    }

}
