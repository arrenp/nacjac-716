<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Console\Style\SymfonyStyle;

class BackupTableCommand extends ContainerAwareCommand {

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var \Doctrine\DBAL\Connection */
    private $connection;

    private $database_host;
    private $database_user;
    private $database_password;
    private $database_name;

    protected function configure()
    {
        $this
            ->setName('app:BackupTableCommand')
            ->setDescription('Backs up, then truncates old table rows')
            ->setHelp('nice')
            ->addOption('tableName', null, InputOption::VALUE_REQUIRED, "table name")
            ->addOption('outputDir', null, InputOption::VALUE_REQUIRED, "Output directory")
            ->addOption('months', null, InputOption::VALUE_REQUIRED, "how many months to retain searchable (the rest will be zipped and backed up to outputDir)")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $container = $this->getContainer();
        $this->connection = $container->get("database_connection");
        $this->database_host = $container->getParameter('database_host');
        $this->database_user = $container->getParameter('database_user');
        $this->database_password = $container->getParameter('database_password');
        $this->database_name = $container->getParameter('database_name');
        $outputDir = $input->getOption('outputDir');
        $this->months = $input->getOption('months');
        $this->tableName = $input->getOption('tableName');
        if (empty($this->months) || !is_numeric($this->months)) {
            throw new \Exception('Please enter a month');
        }
        if (empty($this->tableName)) {
            throw new \Exception('Please enter a table name');
        }
        if (!empty($outputDir)) {
            $outputDir = rtrim($outputDir, '/') . '/';
        }
        $this->outputDir = $outputDir;
        $this->createBackupTable($this->tableName);
    }

    function createBackupTable($tableName) {
        $backupTableName = $tableName . "Backup";


        try {
            $sql = "CREATE TABLE $backupTableName LIKE $tableName;";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();

            $queryDate = date("Y-m-d", strtotime("-" . $this->months . " months"));
            $sql = "INSERT $backupTableName SELECT * FROM $tableName WHERE timestamp <= '$queryDate';";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();

            $sql = "DELETE FROM $tableName WHERE timestamp <= '$queryDate';";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();

            $sql = "OPTIMIZE TABLE $tableName;";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();

            $this->createDump($tableName);
        } catch (\Exception $ex) {
            $errorMessages = [
                "Error creating tables",
                $ex
            ];
            $io = new SymfonyStyle($this->input, $this->output);
            $io->error($errorMessages);
        }
    }

    function createDump($tableName) {
        $backupName = $tableName . "-" . date("Y-m-d") . '-' . time();
        $tableName = $tableName . "Backup";

        try {

            $this->output->writeln("Creating dump");
            $command = ["mysqldump","-h{$this->database_host}","-u{$this->database_user}","-p{$this->database_password}","--lock-tables=false","--quick","--skip-extended-insert","{$this->database_name}","$tableName",">","{$this->outputDir}$backupName.sql"];
            $mysqldump = new Process(implode(' ', $command));
            $mysqldump->setTimeout(2 * 3600);
            $mysqldump->mustRun();

            $this->output->writeln("Compressing dump");
            $command = ['gzip',"{$this->outputDir}$backupName.sql"];
            $gzip = new Process(implode(' ', $command));
            $gzip->setTimeout(2 * 3600);
            $gzip->mustRun();

            if ($mysqldump->isSuccessful() && $gzip->isSuccessful()) {
                $this->output->writeln("dropping $tableName table");
                $sql = "DROP TABLE $tableName";
                $stmt = $this->connection->prepare($sql);
                $stmt->execute();
            }
        } catch (\Exception $ex) {
            $errorMessages = [
                "Error creating dump",
                $ex
            ];
            $io = new SymfonyStyle($this->input, $this->output);
            $io->error($errorMessages);
        }
    }
}
