<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use classes\classBundle\MappingTypes\EncryptedType;
use classes\classBundle\Entity\ProfilesATInitialize;
use classes\classBundle\Entity\ProfilesATBlueprint;
class ATCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('app:ATCommand')
            ->setDescription('migrate AT data')
        ;
    }    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("starting migration");
        $em = $this->getContainer()->get('doctrine')->getManager();
        $connection = $this->getContainer()->get('doctrine.dbal.default_connection');
        $sessionData = $connection->executeQuery("select id,userid,planid,profileId, AES_DECRYPT(sessionData, UNHEX('".EncryptedType::KEY."')) as sessionDatainvestmentsAT from profiles where AES_DECRYPT(sessionData, UNHEX('5E6D217336')) LIKE '%ArchitectIsEnrolled%'")->fetchAll();

        foreach ($sessionData as $data)
        {
            $test = json_decode($data['sessionDatainvestmentsAT']);

            if (!empty(json_decode($data['sessionDatainvestmentsAT'])->ACInitialize)) {
                $jsonData = json_decode($data['sessionDatainvestmentsAT'])->ACInitialize;
                $profilesATInitialize = new ProfilesATInitialize();
                $profilesATInitialize->userid = $data['userid'];
                $profilesATInitialize->planid = $data['planid'];
                $profilesATInitialize->profileid = $data['profileId'];
                $profilesATInitialize->wsArchitectTask = !empty($jsonData->WsArchitectTask) ? json_encode($jsonData->WsArchitectTask) : "";
                $profilesATInitialize->wsBlueprint = !empty($jsonData->WsBlueprint) ? json_encode($jsonData->WsBlueprint) : "";
                $profilesATInitialize->wsPlan = !empty($jsonData->WsPlan) ? json_encode($jsonData->WsPlan) : "";
                $profilesATInitialize->wsIndividual = !empty($jsonData->WsIndividual) ? json_encode($jsonData->WsIndividual) : "";
                $em->persist($profilesATInitialize);
            }

            if (!empty(json_decode($data['sessionDatainvestmentsAT'])->ACBluePrint)) {
                $jsonData = json_decode($data['sessionDatainvestmentsAT'])->ACBluePrint;
                $profilesATBlueprint = new ProfilesATBlueprint();
                $profilesATBlueprint->userid = $data['userid'];
                $profilesATBlueprint->planid = $data['planid'];
                $profilesATBlueprint->profileid = $data['profileId'];
                $profilesATBlueprint->wsArchitectTask = !empty($jsonData->WsArchitectTask) ? json_encode($jsonData->WsArchitectTask) : "";
                $profilesATBlueprint->wsBlueprint = !empty($jsonData->WsBlueprint) ? json_encode($jsonData->WsBlueprint) : "";
                $profilesATBlueprint->wsIndividual = !empty($jsonData->WsIndividual) ? json_encode($jsonData->WsIndividual) : "";
                $em->persist($profilesATBlueprint);
            }

            $em->flush();

        }
        $output->writeln("finished migration");       
    }     
}


