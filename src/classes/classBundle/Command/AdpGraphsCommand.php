<?php

namespace classes\classBundle\Command;

use classes\classBundle\Entity\surveyEmployers;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use classes\classBundle\MappingTypes\EncryptedType;
use classes\classBundle\Entity\ProfilesAdpGraphs;
class AdpGraphsCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('app:AdpGraphs')
            ->setDescription('migrate ada graph data')
        ;
    }    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("starting migration");
        $em = $this->getContainer()->get('doctrine')->getManager();
        $connection = $this->getContainer()->get('doctrine.dbal.default_connection');
        $sessionData = $connection->executeQuery("select id,userid,planid,profileId, AES_DECRYPT(sessionData, UNHEX('".EncryptedType::KEY."')) as sessionDatainvestmentsGraphData from profiles where AES_DECRYPT(sessionData, UNHEX('5E6D217336')) LIKE '%investmentsGraphData%'")->fetchAll();
        foreach ($sessionData as $data)
        {
            try
            {
            $jsonData = json_decode($data['sessionDatainvestmentsGraphData'])->investmentsGraphData;
            foreach($jsonData->table as $row)
            {
                if ($row->total > 0 )
                {
                    $profilesAdpGraph = new ProfilesAdpGraphs();
                    $profilesAdpGraph->userid = $data['userid'];
                    $profilesAdpGraph->planid = $data['planid'];
                    $profilesAdpGraph->profileid = $data['id'];
                    $profilesAdpGraph->description = $row->header->description ? $row->header->description: "";
                    $profilesAdpGraph->total = $row->total;
                    $profilesAdpGraph->color = $row->color->color ? $row->color->color: "";
                    $profilesAdpGraph->hoverColor = $row->color->hoverColor ? $row->color->hoverColor : "";
                    $em->persist($profilesAdpGraph);
                    $em->flush($profilesAdpGraph);
                }               
            }
            }
            catch (\Exception $e)
            {
                
            }
        }
        $output->writeln("finished migration");       
    }     
}


