<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class DumpXmlRawCommand extends ContainerAwareCommand {
    
    /** @var InputInterface */
    private $input;
    
    /** @var OutputInterface */
    private $output;
    
    /** @var \Doctrine\DBAL\Connection */
    private $connection;
    
    private $database_host;
    private $database_user;
    private $database_password;
    private $database_name;
    private $outputDir;
    
    /** @var \DateTimeImmutable */
    private $dt;
    
    protected function configure()
    {
        $this
            ->setName('app:DumpXmlRaw')
            ->setDescription('Dumps a month of xmlRaw entries to compressed sql files')
            ->setHelp('Dumps a month of xmlRaw entries to compressed sql files')
            ->addArgument('year', InputArgument::REQUIRED, "Year of xmlRaw transactions")
            ->addArgument('month', InputArgument::REQUIRED, "Month of xmlRaw transactions")
            ->addOption('outputDir', null, InputOption::VALUE_REQUIRED, "Output directory")
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        
        $container = $this->getContainer();
        $this->connection = $container->get("database_connection");
        $this->database_host = $container->getParameter('database_host');
        $this->database_user = $container->getParameter('database_user');
        $this->database_password = $container->getParameter('database_password');
        $this->database_name = $container->getParameter('database_name');

        $year = $input->getArgument("year");
        $month = $input->getArgument("month");
        $outputDir = $input->getOption('outputDir');
        if (!empty($outputDir)) {
            $outputDir = rtrim($outputDir, '/') . '/';
        }
        $this->outputDir = $outputDir;
        
        $this->dt = \DateTimeImmutable::createFromFormat("Y-n-d", "$year-$month-01");
        if ($this->dt === false) {
            $output->writeln("Invalid year and month");
            return;
        }

        $this->createDump($this->dt->format('Y-m-01'), $this->dt->format('Y-m-08'), "01");
        $this->createDump($this->dt->format('Y-m-08'), $this->dt->format('Y-m-15'), "02");
        $this->createDump($this->dt->format('Y-m-15'), $this->dt->format('Y-m-22'), "03");
        $this->createDump($this->dt->format('Y-m-22'), $this->dt->modify("+1 month")->format('Y-m-01'), "04");
    }
    
    function createDump($startDate, $endDate, $weekNumber) {
        $tableName = "xmlRaw_" . $this->dt->format('Ym') . "_$weekNumber";

        try {

            $this->output->writeln("Creating temporary table for $startDate to $endDate");
            $sql = "create table $tableName as select * from xmlRaw where timestamp between :startDate and :endDate";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute(['startDate' => $startDate, 'endDate' => $endDate]);
            
            $exists = $this->connection->fetchColumn("SHOW TABLES LIKE '$tableName'");
            if (empty($exists)) {
                $this->output->writeln("<error>Could not create temporary table for $startDate to $endDate</>");
                return;
            }
        
            $this->output->writeln("Creating sql file for $startDate to $endDate");
            $command = ["mysqldump","-h{$this->database_host}","-u{$this->database_user}","-p{$this->database_password}","--lock-tables=false","--quick","--skip-extended-insert","{$this->database_name}","$tableName",">","{$this->outputDir}$tableName.sql"];
            $mysqldump = new Process(implode(' ', $command));
            $mysqldump->mustRun();

            $this->output->writeln("Compressing sql file for $startDate to $endDate");
            $command = ['gzip',"{$this->outputDir}$tableName.sql"];
            $gzip = new Process(implode(' ', $command));
            $gzip->mustRun();

            if ($mysqldump->isSuccessful() && $gzip->isSuccessful()) {
                $this->output->writeln("Deleting entries from xmlRaw for $startDate to $endDate");
                $sql = "delete from xmlRaw where timestamp between :startDate and :endDate;";
                $stmt = $this->connection->prepare($sql);
                $stmt->execute(['startDate' => $startDate, 'endDate' => $endDate]);

                $this->output->writeln("Dropping temporary table for $startDate to $endDate");
                $sql = "drop table $tableName";
                $stmt = $this->connection->prepare($sql);
                $stmt->execute();
            }
        } catch (\Exception $ex) {
            $errorMessages = [
                "Error creating dump for week #$weekNumber ($startDate to $endDate)",
                $ex
            ];
            $io = new SymfonyStyle($this->input, $this->output);
            $io->error($errorMessages);
        }
    }
}
