<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Shared\General\SFTPConnection;

class RiskProfileCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:RiskProfileMigrate')
            ->setDescription('Migrate stuff');           
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get("RiskProfileQuestionnaireService")->migrate();
    }
}
