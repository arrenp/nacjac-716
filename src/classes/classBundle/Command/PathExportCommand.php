<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PathExportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:PathExportCommand')
            ->setDescription('')
        ;
    }  
    protected function execute(InputInterface $input, OutputInterface $output)
    {       
        $mysqlHostName = $this->getContainer()->getParameter('database_host');
        $mysqlUserName = $this->getContainer()->getParameter('database_user');
        $mysqlPassword = $this->getContainer()->getParameter('database_password');
        $mysqlDatabaseNameFrom = $this->getContainer()->getParameter('database_name');
        $file = "PathExport.sql";
        $tables = 
        [
            "AppContentType",
            "AppPlayList",
            "AppPlayListVideo",
            "AppSection",
            "AppSectionList",
            "AppStep",
            "AppStepList",
            "AppVideo",
            "AppVideoLocale",
            "DefaultContent",
            "Path",
            "PathAccount",
            "PathContent",
            "PathTypes",
            "AppStepContent"
        ];
        $tableString = implode(" ",$tables);
        $command='mysqldump --opt --lock-tables=false -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseNameFrom .' '.$tableString.' > ' .$file; 
        $output->writeLn($command);
        $output = [];
        $worked = 0;
        exec($command,$output,$worked);        
    }
}