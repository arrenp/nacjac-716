<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExtractFromXmlRawCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('app:ExtractFromXmlRaw')
            ->setDescription('Copies rows from xmlRaw table and optionally deletes')
            ->setHelp('Copies rows from xmlRaw table and optionally deletes')
            ->addArgument('year', InputArgument::REQUIRED, "Year of xmlRaw transactions")
            ->addArgument('month', InputArgument::REQUIRED, "Month of xmlRaw transactions")
            ->addOption('delete', null, InputOption::VALUE_NONE, "If present, delete copied records from xmlRaw")
            ->addOption('drop-existing', null, InputOption::VALUE_NONE, "If present, drop target table if it exists")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = $this->getContainer()->get("database_connection");

        $year = (int) $input->getArgument("year");
        $month = (int) $input->getArgument("month"); 
        $dropExisting = $input->getOption('drop-existing');
        $delete = $input->getOption('delete');
        $tableName = 'xmlRaw_' . $year . str_pad($month, 2, "0", STR_PAD_LEFT);
        
        /* @var $dt \DateTime */
        $dt = \DateTime::createFromFormat("Y-m", "$year-$month");
        if ($dt === false) {
            $output->writeln("Invalid year and month");
            return;
        }
        
        $conditions = sprintf("timestamp BETWEEN '%s' AND '%s'", $dt->format('Y-m-01 00:00:00'), $dt->format('Y-m-t 23:59:59'));   
        $exists = $connection->fetchColumn("(SELECT 1 FROM xmlRaw WHERE $conditions) UNION (SELECT 1 FROM xmlRaw_old WHERE $conditions) LIMIT 1");
        if (!$exists) {
            $output->writeln("There are no records for the target month");
            return;
        }
        
        $exists = $connection->fetchColumn("SHOW TABLES LIKE '$tableName'");
        if ($exists) {
            if (!$dropExisting) {
                $output->writeln("A table already exists for the target month");
                $output->writeln("Use the --drop-existing option to drop the table");
                return;
            }
            $connection->query("DROP TABLE IF EXISTS $tableName"); 
        } 
        $commonColumns = "id,profileUid,xmlUid,profileId,userid,planid,connection,action,data,timestamp";
        $sql = "CREATE TABLE $tableName SELECT $commonColumns,service FROM xmlRaw WHERE $conditions UNION SELECT $commonColumns,NULL as service FROM xmlRaw_old WHERE $conditions";
        $copyCount = $connection->executeUpdate($sql);
        $output->writeln("Copied $copyCount records to $tableName");
        
        
        if ($delete) {
            $sql = "DELETE FROM xmlRaw WHERE $conditions";
            $deleteCount = $connection->executeUpdate($sql);
            $sql = "DELETE FROM xmlRaw_old WHERE $conditions";
            $deleteCount += $connection->executeUpdate($sql);
            $output->writeln("Deleted $deleteCount records from xmlRaw/xmlRaw_old");
        }
    }
}
