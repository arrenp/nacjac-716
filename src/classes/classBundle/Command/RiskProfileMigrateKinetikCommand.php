<?php

namespace classes\classBundle\Command;

use classes\classBundle\Entity\PlanRiskProfileQuestionnaire;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class RiskProfileMigrateKinetikCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('app:RiskProfileMigrateKinetik')
            ->setDescription('Migrate kinetik risk profile');
    }
    
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) {
        
        $entityManager = $this->getContainer()->get("doctrine")->getManager();
        
        $riskProfileQuestionnaireId = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:RiskProfileQuestionnaire")->findOneBy(array('name' => 'smart401k'))->id;
        $plans = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:plans")->findBy(array('adviceStatus' => 2, 'userid' => array(1174, 1359, 1360)));
        foreach ($plans as $plan) {
            $riskProfileRow = new PlanRiskProfileQuestionnaire();
            $riskProfileRow->userid = $plan->userid;
            $riskProfileRow->planid = $plan->id;
            $riskProfileRow->riskProfileQuestionnaireId = $riskProfileQuestionnaireId;
            $riskProfileRow->isKinetik = 1;
            $entityManager->persist($riskProfileRow);
            $entityManager->flush();
        }
    }
}
