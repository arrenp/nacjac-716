<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Shared\General\SFTPConnection;
use classes\classBundle\Entity\profilesRiskProfileQuestions;
use classes\classBundle\Entity\profilesRiskProfileAnswers;
class RiskProfileProfilesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:RiskProfileProfilesCommand')
            ->setDescription('Migrate moar stuff');    
    }   
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = $this->getContainer()->get('doctrine.dbal.default_connection');
        $this->profilesRiskProfileQuestions = [];
        $this->profilesRiskProfileAnswers = [];
        $langugageRepo = $this->getContainer()->get("doctrine")->getRepository("classesclassBundle:languages");
        $languagesArray = ["en","es"];
        foreach ($languagesArray as $language)
            $languages[$language] = $langugageRepo->findOneBy(["name" => $language])->id;
        $em = $this->getContainer()->get('doctrine')->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('profilesRiskProfile.xml','profilesRiskProfile.ans','accounts.languagePrefix','profiles.language','profiles.id','profiles.userid','profiles.planid');
        $queryBuilder->from('classesclassBundle:profilesRiskProfile', 'profilesRiskProfile');
        $queryBuilder->leftJoin(
            'classesclassBundle:accounts',
            'accounts',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'accounts.id = profilesRiskProfile.userid'
        );
        $queryBuilder->leftJoin(
            'classesclassBundle:profiles',
            'profiles',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'profiles.id = profilesRiskProfile.profileid'
        );
        $result=  $queryBuilder->getQuery()->getScalarResult();
        $riskProfilePaths[] = [];
        $i = 1;
        $j = count($result);
        $counter = 1;
        foreach ($result as &$row)
        {
            $questionExists  = $connection->executeQuery("select id from profilesRiskProfileQuestions where profileid = ".$row['id']." limit 1")->fetch(); 
            $answerExists  = $connection->executeQuery("select id from profilesRiskProfileAnswers where profileid = ".$row['id']." limit 1")->fetch(); 
            if (!empty($questionExists['id']) && empty($answerExists))
            {
                $connection->executeQuery("delete from profilesRiskProfileQuestions where profileid = ".$row['id']);
            }
            if (empty($questionExists['id']))
            {
                $prefix  = "";
                $language = "en";
                if (!empty($row['languagePrefix']) && in_array($row['languagePrefix'],["adp","horacemann","lincoln","omnisp","smartenroll"]))
                {
                    $prefix = $row['languagePrefix'] . '_';
                }
                if (!empty($row['language']) && $row['language'] == "es")
                {
                    $language = "es";
                }
                $combined = strtolower($prefix.$language);
                if (!isset($riskProfilePaths[$combined]))
                    $riskProfilePaths[$combined] = $this->getContainer()->get('kernel')->locateResource('@SpeAppBundle/Resources/translations/'.$combined.'/riskProfile/');

                if (!empty($row['xml']) && !empty($row['ans']))
                {
                    $data = $this->getContainer()->get('spe.app.functions')->getRPQuestionAnswer($row['xml'], $row['ans'], $riskProfilePaths[$combined]);
                    $this->processData($row,$data,$languages[$language]);
                    if ($i % 1000 == 0)
                    {

                        foreach ($this->profilesRiskProfileQuestions as &$questionnaire)
                        foreach ($questionnaire as &$question)
                        {
                           $em->persist($question);
                        }
                        $em->flush();                    
                        foreach ($this->profilesRiskProfileAnswers as $answer)
                        {
                            $answer->questionId = $this->profilesRiskProfileQuestions[$answer->profileid][$answer->number]->id;
                            $em->persist($answer);
                        }
                        $em->flush();
                        $em->clear();
                        $this->profilesRiskProfileQuestions = [];
                        $this->profilesRiskProfileAnswers = [];
                    }
                    $i++;
                }
            }
            $output->write("Progress: $counter out of $j   \r");
            $counter++;
        }
        $em->flush();
        $output->writeln( "donezo");
    }
    public function processData($row,$data,$languageId)
    {
        $number = 1;
        foreach($data as $question)
        {
            $profilesRiskProfileQuestions = new profilesRiskProfileQuestions();
            $profilesRiskProfileQuestions->profileid = $row['id'];
            $profilesRiskProfileQuestions->userid = $row['userid'];
            $profilesRiskProfileQuestions->planid = $row['planid'];
            $profilesRiskProfileQuestions->question = $question['question'];
            $profilesRiskProfileQuestions->number = $number;
            $profilesRiskProfileQuestions->used = 1;
            $profilesRiskProfileQuestions->languageId = $languageId;
            $this->profilesRiskProfileQuestions[$row['id']][$number] = $profilesRiskProfileQuestions;
            $profilesRiskProfileAnswers = new profilesRiskProfileAnswers();
            $profilesRiskProfileAnswers->profileid = $row['id'];
            $profilesRiskProfileAnswers->userid = $row['userid'];
            $profilesRiskProfileAnswers->planid = $row['planid'];
            $profilesRiskProfileAnswers->answer = $question['answer'];
            $profilesRiskProfileAnswers->points = $question['points'];
            $profilesRiskProfileAnswers->questionId = $profilesRiskProfileQuestions->id;
            $profilesRiskProfileAnswers->thpoints = 0;
            $profilesRiskProfileAnswers->selected = 1;
            $profilesRiskProfileAnswers->number = $number;
            $this->profilesRiskProfileAnswers[] = $profilesRiskProfileAnswers;

            $number++;
        }
    }

}