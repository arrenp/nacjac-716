<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use classes\classBundle\MappingTypes\EncryptedType;
use classes\classBundle\Entity\ProfilesATInitialize;
use classes\classBundle\Entity\ProfilesATBlueprint;
use classes\classBundle\Entity\DefaultContent;
class ImportDefaultContentCommand extends ContainerAwareCommand 
{
    protected function configure()
    {
        $this
            ->setName('app:ImportDefaultContent')
            ->setDescription('Importing Default Content good for you')
        ;
    }  
    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $sectionListHash =  $this->getContainer()->get("PathsService")->getSectionListHash("description");
       $contentTypeHash = $this->getContainer()->get("PathsService")->getContentTypesHash("description");
        $file = fopen("defaultContent.csv","r");
        $i = 1;
        while(! feof($file))
        {
            $output->writeln($i);
            $defaultContent = new DefaultContent();
            $row = fgetcsv($file);
            $section = str_replace("_"," ",$row[3]);
            if ($section == "Confirmation")
                $section = "My Profile";
            
            $englishContent = $row[5];
            $spanishContent = $row[6];
            $contentType = $row[1];
            $step = $row[4];
            $label = $row[2];
            
            
            $defaultContent->englishContent = $englishContent;
            $defaultContent->spanishContent = $spanishContent;
            $defaultContent->sectionListId = $sectionListHash[$section]->id;
            $defaultContent->contentTypeId = $contentTypeHash[$contentType]->id;
            $defaultContent->step = $step;
            $defaultContent->label = $label;
            $em = $this->getContainer()->get('doctrine')->getManager();
            $em->persist($defaultContent);
            $em->flush();

            $i++;
        }

    }
}