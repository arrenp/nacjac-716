<?php

namespace classes\classBundle\Command;

use classes\classBundle\Entity\surveyEmployers;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateSurveyEmployersCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('app:MigrateSurveyEmployersCommand')
            ->setDescription('migrate survey data')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("starting migration");

        /* @var $em \Doctrine\ORM\EntityManager */
        $em              = $this->getContainer()->get("doctrine")->getManager();
        
        $query2          = $em->createQuery('SELECT p FROM classesclassBundle:surveyEmployers p WHERE p.employerGroupsTableId = 1 ORDER BY p.employerName');                       
        $k12Groups       = $query2->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        $query3          = $em->createQuery('SELECT p FROM classesclassBundle:surveyEmployers p WHERE p.employerGroupsTableId = 2 ORDER BY p.employerName');                       
        $areaEdGroups    = $query3->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY); 
        
        $query4          = $em->createQuery('SELECT p FROM classesclassBundle:surveyEmployers p WHERE p.employerGroupsTableId = 3 ORDER BY p.employerName');                       
        $collegeGroups   = $query4->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        $query5          = $em->createQuery('SELECT p FROM classesclassBundle:surveyEmployers p WHERE p.employerGroupsTableId = 4 ORDER BY p.employerName');                       
        $publicGroups    = $query5->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY); 
        
        $query6          = $em->createQuery('SELECT p FROM classesclassBundle:surveyEmployers p WHERE p.employerGroupsTableId = 5 ORDER BY p.employerName');
        
        $stateGroups     = $query6->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY); 
        
        $query7          = $em->createQuery('SELECT p FROM classesclassBundle:surveyEmployers p WHERE p.employerGroupsTableId IS NULL ORDER BY p.employerName');
        
        $nullGroups     = $query7->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY); 
        
        $this->hashArray($k12Groups,"employerName");
        $this->hashArray($areaEdGroups,"employerName");
        $this->hashArray($collegeGroups,"employerName");
        $this->hashArray($publicGroups,"employerName"); 
        $this->hashArray($stateGroups,"employerName"); 
        $this->hashArray($nullGroups,"employerName"); 
        
        
        foreach (['k12Groups','areaEdGroups','collegeGroups','publicGroups','stateGroups','nullGroups'] as $group) {
            foreach ($$group as $value) {
                if ($group != 'nullGroups') {
                    $query1 = $em->createQuery('SELECT p FROM classesclassBundle:surveyEmployers p WHERE p.employerName = :name AND p.employerGroupsTableId = :employerGroupsTableId')->setParameter('name', $value['employerName'])->setParameter('employerGroupsTableId', $value['employerGroupsTableId']);
                }
                else {
                    $query1 = $em->createQuery('SELECT p FROM classesclassBundle:surveyEmployers p WHERE p.employerName = :name AND p.employerGroupsTableId IS NULL')->setParameter('name', $value['employerName']);
                }
                
                $data   = $query1->getResult();
                
                $surveyEmployer = new surveyEmployers();
                $surveyEmployer->employerName = $value['employerName'];
                $surveyEmployer->employerGroupsTableId = $value['employerGroupsTableId'];
                $surveyEmployer->planId = null;
                $surveyEmployer->state = null;
                $surveyEmployer->type = null;
                $surveyEmployer->county = null;
                
                $em->persist($surveyEmployer);
                $em->flush();
                foreach ($data as $obj) {
                    $plan = new \classes\classBundle\Entity\SurveyEmployerPlans();
                    $plan->county = $obj->county;
                    $plan->planid = $obj->planId;
                    $plan->state = $obj->state;
                    $plan->surveyEmployerId = $surveyEmployer->id;
                    $plan->type = $obj->type;
                    $em->persist($plan);
                    $em->remove($obj);
                }
                
                $output->writeln("processed {$value['employerName']}");
                $em->flush();
            }
        }
        
        
        $output->writeln("finished migration");
    }
    
    private function hashArray(&$object,$fieldname)
    {
        foreach ($object as $key => $value)
        {
           if (is_int($key))
           {
            $object[$value[$fieldname]] = $value;
            unset ($object[$key]); 
           }
        }        
    }    
}
