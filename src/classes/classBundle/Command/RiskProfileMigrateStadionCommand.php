<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class RiskProfileMigrateStadionCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('app:RiskProfileMigrateStadion')
            ->setDescription('Migrate stadion risk profile')
            ->addArgument('action', \Symfony\Component\Console\Input\InputArgument::REQUIRED, 'delete or create');
    }
    
    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) {
        
        $entityManager = $this->getContainer()->get("doctrine")->getManager();
        
        /** @var \classes\classBundle\Services\Entity\RiskProfileQuestionnaireService $questionnaireService */
        $questionnaireService = $this->getContainer()->get("RiskProfileQuestionnaireService");
        
        if ($input->getArgument('action') === 'delete') {
            $questionnaires = $questionnaireService->findBy(['name' => 'stadionManagedAccount']);
            foreach ($questionnaires as $questionnaire) {
                $questionnaireService->delete($questionnaire->id);
            }
            $entityManager->flush();
        }
                
        if ($input->getArgument('action') === 'create') {
            $riskProfiles = array(
                array(
                    'prefix' => '',
                    'section' => 'stadionManagedAccount',
                    'name' => 'stadionManagedAccount',
                    'file' => 'stadionManagedAccount.xml',
                    'folder' => '',
                ),
            );
            $languages = ["en","es"];
            $questionnaireService->processRiskProfiles($riskProfiles, $languages);

            $questionnaire = $questionnaireService->findOneBy(['name' => 'stadionManagedAccount']);
            if (!is_null($questionnaire)) {
                $questionnaireService->saveStadion("Default", $questionnaire->id);
            }
        }
    }
}
