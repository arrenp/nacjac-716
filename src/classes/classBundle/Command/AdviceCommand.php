<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

error_reporting(E_ALL & ~E_NOTICE && ~E_WARNING);

class AdviceCommand extends ContainerAwareCommand {
    protected function configure()
    {
        $this
            ->setName('app:AdviceCommand')
            ->setDescription('Runs advice funds for all plans that adviceStatus is 1 or 2, emails old or new funds and sets correct adviceStatus')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->getContainer()->get('AdviceService')->populateAndTagFundsAll();
        $this->getContainer()->get('AdviceService')->notifyRecipientsOfChanges();
    }


}
