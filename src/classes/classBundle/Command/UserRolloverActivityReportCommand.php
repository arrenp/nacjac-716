<?php

namespace classes\classBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Shared\General\SFTPConnection;
use Symfony\Component\Console\Input\InputArgument;

class UserRolloverActivityReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:UserRolloverActivityReport')
            ->setDescription('User Rollover Activity Report for ameritas')
            ->addOption('userid', null, InputOption::VALUE_REQUIRED, 'Enter the account ID to run the report on.')
            ->addOption('start', null, InputOption::VALUE_OPTIONAL, 'start date: example 2017-09-13')
            ->addOption('end', null, InputOption::VALUE_OPTIONAL, 'end date: example 2017-11-13')
            ->addOption('hours', null, InputOption::VALUE_OPTIONAL, 'how many hours to subtract ')
            ->addOption('skipUpload', null, InputOption::VALUE_NONE, 'Skip upload')
            ->addOption('hubspot-list', null, InputOption::VALUE_REQUIRED, 'Hubspot list ID')
        ;


    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $generalService = $this->getContainer()->get("general.methods");
        $generalService->postToSlack("info", $this->getName(), "Job start");
        $start = $input->getOption('start');
        $end = $input->getOption('end');
        $hours = $input->getOption('hours');

        if ($start == null || $end == null) {
            $subtractHours = 'PT6H';
            if ($hours != null) {
                $subtractHours = 'PT' . $hours . 'H';
            }


            $date = new \DateTime();
            $end = $date->format('Y-m-d H:i:s');
            $date->sub(new \DateInterval($subtractHours));
            $start = $date->format('Y-m-d H:i:s');
        }

        $userid = $input->getOption('userid');

        $URARService = $this->getContainer()->get("UserRolloverActivityReportService");
        $data =
            [
                [
                    "First Name",
                    "Last Name",
                    "Plan ID",
                    "Call Us",
                    "Call Me",
                    "Email Me",
                    "User's Selection",
                    "HubSpot"
                ]
            ];

        foreach ($URARService->getProfiles($userid, $start, $end) as $item) {
            $item[] = 0;
            $data[] = $item;
        }
        
        $hubSpotListId = $input->getOption('hubspot-list'); #1231
        if ($hubSpotListId) {
            foreach ($URARService->getProfilesFromHubSpotList($hubSpotListId, $start, $end) as $item) {
                $item[] = 1;
                $data[] = $item;
            } 
        }
        $filename = "AMRTS_".date("Ymd_H_i")."_UAR.csv";
        $fp = fopen($filename, 'w');
        foreach ($data as $row)
        {
            fputcsv($fp, $row);

        }
        fclose($fp);

        $skipUpload = $input->getOption('skipUpload');
        if (!$skipUpload) {
            try {
                $loginSuccess = false;
                $ftpConfig = $URARService->getFtpConfig();
                $sftp = new SFTPConnection($ftpConfig['host'],$ftpConfig['port']);
                $sftp->login($ftpConfig['username'], $ftpConfig['password']);

                $loginSuccess = true;

                $sftp->uploadFile($filename, "/home/vwiseupload/" . $filename);
                $generalService->postToSlack("info", $this->getName(), "Filename: {$filename}");
                $generalService->postToSlack("info", $this->getName(), "FTP upload success");
                unlink($filename);
            } catch (\Exception $e) {
                $output->writeln($e);
                $generalService->postToSlack("error", $this->getName(), $e);
                if ($loginSuccess) {
                    $generalService->postToSlack("info", $this->getName(), "FTP upload failed");
                }
            }
        }

        $generalService->postToSlack("info", $this->getName(), "Job End");
    }
}