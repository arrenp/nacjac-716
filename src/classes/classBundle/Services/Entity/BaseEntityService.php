<?php
namespace classes\classBundle\Services\Entity;
use classes\classBundle\Services\BaseService;
use Symfony\Component\DependencyInjection\ContainerInterface;
class BaseEntityService extends BaseService
{
    public $entityName;
    public $class;
    public $doctrine;
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->init();
        $this->classPathAdd = "classes\\classBundle\\Entity\\".$this->entityName;
        //refactor note for parent class, use same logic as below for doctrine and em
        $this->doctrine = $this->container->get("doctrine");
        $this->em = $this->container->get("doctrine")->getManager();
        $this->class = $this->doctrine->getRepository($this->classPath.$this->entityName);
    }
    public final function findBy($findby = array(),$orderby = array(),$max = null,$offset = null)
    {
        return $this->class->findBy($findby,$orderby,$max,$offset);
    }
    public final function findAll()
    {
        return $this->class->findAll();
    }
    public final function findOneBy($findby = array(),$orderby = array())
    {
        return $this->class->findOneBy($findby,$orderby);
    }        
    public function init()//override init instead of constructor
    {
        
    }
    protected function copy($entity,$array)
    {
        $entity->copy($array);
    }
    protected final function persist($entity)
    {
        $this->em->persist($entity);
    }
    protected final  function flush()
    {
        $this->em->flush();
    } 
    protected final function remove($entity)
    {
        $this->em->remove($entity);
    }
    public function delete($entity,$options = array("flush" => 1))
    {
        $this->remove($entity);
        if ($options['flush'])
        {
            $this->em->flush();
        }
    }
    public function add($array,$options = array("flush" => 1))
    {
        $entity = new $this->classPathAdd;
        $this->copy($entity,$array);
        $this->persist($entity);     
        if ($options['flush'])
        {
            $this->flush();
        }
        return $entity;
    }
    public function save($entity,$array,$options = array("flush" => 1))
    {       
        $this->copy($entity,$array);
        if ($options['flush'])
        {
            $this->flush();
        }
        return $entity;
    }
    public function findLast($params = array())
    {
        return $this->findOneBy($params,array("id" => "desc"));
    }
    public function reorder($ids,$findby,$fieldname = "orderid")
    {
        $elements = $this->findBy($findby);
        $elementsHash = array();
        foreach ($elements as $element)
        {
            $elementsHash[$element->id] = $element;
        }
        $orderid = 0;
        foreach ($ids as $id)
        {
            $elementsHash[(int)$id]->$fieldname = $orderid++;
        }
        $this->em->flush();        
    }      
}