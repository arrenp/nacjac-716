<?php
namespace classes\classBundle\Services\Entity;
class SystemMessagesTagsService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "SystemMessagesTags";
    }     
    public function addTag($tag)
    {
        if (trim($tag['name']) != "")
        {
            $findTag = $this->findOneBy(array("name" => $tag['name']));
            if ($findTag != null)
            {
                return $findTag;
            }
            else
            {
                return  $this->add($tag);
            }      
        }
        return false;
    }
    public function addTags($tags)
    {
        $returnTags = array();
        foreach ($tags as $tag)
        {
            $returnTags[] = $this->addTag($tag);          
        }
        return $returnTags;
    }
    public function deleteTag($tagid)
    {
        $tag = $this->findOneBy(array("id" => $tagid));
        $this->em->remove($tag);
        $links = $this->container->get("SystemMessagesAndTagsLinkService")->findBy(array("systemMessageTagsId" => $tagid));
        foreach ($links as $link)
        {
            $this->em->remove($link);
        }
        $this->em->flush();
    }
    public function getTagsByMessageId($messageid)
    {
        $links = $this->container->get("SystemMessagesAndTagsLinkService")->findBy(array("systemMessageId" => $messageid));
        $tags = array();
        foreach ($links as $link)
        {
            $tag = $this->findOneBy(array("id" => $link->systemMessageTagsId));
            $tags[$tag->name] = $tag;
        }
        ksort($tags);
        return $tags;
    }
}
