<?php
namespace classes\classBundle\Services\Entity;
class RiskProfileQuestionnaireAnswerService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "RiskProfileQuestionnaireAnswer";
    }  
    public function getAnswers($questionid)
    {
        $answers = $this->findBy(["riskProfileQuestionnaireQuestionId" => $questionid],["displayOrder" => "asc"]);
        foreach ($answers as &$answer)
        {
            $answer->locale = $this->getAnswerLocales($answer->id);
        }
        return $answers;
    }
    public function getAnswerLocales($answerid)
    {
        $locales = $this->container->get("RiskProfileQuestionnaireAnswerLocaleService")->findBy(["riskProfileQuestionnaireAnswerId" => $answerid]);
        $localeHash = [];
        foreach ($locales as $local)
        {
            $localeHash[$local->languageId] = $local;
        }
        return $localeHash;
    }    
    public function addAnswer($params)
    {
        $orgParams = $params;
        unset($params['locales']);
        $lastExisting = $this->findOneBy(["riskProfileQuestionnaireQuestionId" => $params["riskProfileQuestionnaireQuestionId"]],["displayOrder" => "desc"]);
        $params['displayOrder'] = 0;
        if ($lastExisting != null)
        {
            $params['displayOrder'] = $lastExisting->displayOrder + 1;
        }
        $answer = $this->add($params);
        foreach ($orgParams['locales'] as $locale)
        {
            $locale['riskProfileQuestionnaireAnswerId'] =$answer->id;
            $this->container->get("RiskProfileQuestionnaireAnswerLocaleService")->add($locale);          
        }
    }
    public function editAnswer($params,$locales)
    {
        $entity = $this->findOneBy(["id" => $params["id"] ]);
        $this->save($entity,$params);        
        foreach ($locales as $locale)
        {
            $localeEntity = $this->container->get("RiskProfileQuestionnaireAnswerLocaleService")->findOneBy(["id" => $locale["id"] ]);
            $this->container->get("RiskProfileQuestionnaireAnswerLocaleService")->save($localeEntity,$locale);
        }
    }
    public function deleteAnswer($id)
    {
        $entity = $this->findOneBy(["id" => $id ]);
        $this->em->remove($entity);
        $locales = $this->container->get("RiskProfileQuestionnaireAnswerLocaleService")->findBy(["riskProfileQuestionnaireAnswerId" => $id]);
        foreach ($locales as $locale)
        {
            $this->em->remove($locale);
        }
        $this->em->flush();
    }
    public function saveAnswerOrder($ids)
    {
        $displayOrder = 0;
        foreach ($ids as $id)
        {
            $question = $this->findOneBy(["id" => $id]);
            $question->displayOrder = $displayOrder;
            $displayOrder++;
        }
        $this->em->flush();
    }
}


