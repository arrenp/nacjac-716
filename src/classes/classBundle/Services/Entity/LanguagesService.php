<?php
namespace classes\classBundle\Services\Entity;
class LanguagesService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "languages";
    }    
    public function getAll($hashIndex = "name")
    {
        $languages = $this->findAll();
        $languagesArray = array();
        foreach ($languages as $language)
        {
            $languagesArray[$language->$hashIndex] = $language;
        }
        return $languagesArray;
    }
}