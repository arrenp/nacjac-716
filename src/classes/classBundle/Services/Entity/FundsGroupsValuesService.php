<?php
namespace classes\classBundle\Services\Entity;
class FundsGroupsValuesService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "FundsGroupsValues";
    }
    public function getByGroupId($groupid)
    {
        return $this->findBy(array('groupid' => $groupid,"deleted" => 0),array("orderid" => "asc"));
    }
}