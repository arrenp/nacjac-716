<?php
namespace classes\classBundle\Services\Entity;
class AccountsService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "accounts";
    }
    public function getLanguages($userid)
    {
        $accountLanguages = array();
        $account = $this->findOneBy(array("id" => $userid ));
        $languages = $this->container->get("LanguagesService")->getAll();
        $languagesAvailable = $account->getLanguagesAvailable();
        foreach ($languagesAvailable as $language)
        {
            if ($languages[$language] != null) {
                $accountLanguages[$language] = $languages[$language];
            }
        }
        return $accountLanguages;
    }
}
