<?php
namespace classes\classBundle\Services\Entity;
class RiskProfileQuestionnaireQuestionService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "RiskProfileQuestionnaireQuestion";
    }   
    public function addQuestion($questionParams,$questions)
    {
        $lastExisting = $this->findOneBy(["riskProfileQuestionnaireId" => $questionParams["riskProfileQuestionnaireId"]],["displayOrder" => "desc"]);
        $questionParams['displayOrder'] = 0;
        if ($lastExisting != null)
        {
            $questionParams['displayOrder'] = $lastExisting->displayOrder + 1;
        }
        $entity = $this->add($questionParams);
        foreach ($questions as $question)
        {
            $question['riskProfileQuestionnaireQuestionId'] = $entity->id;
            $this->container->get("RiskProfileQuestionnaireQuestionLocaleService")->add($question);
        }
        return $entity;
    }
    public function editQuestion($locales)
    {
        foreach ($locales as $locale)
        {
            $entity = $this->container->get("RiskProfileQuestionnaireQuestionLocaleService")->findOneBy(["id" => $locale['id']]);
            $this->container->get("RiskProfileQuestionnaireQuestionLocaleService")->save($entity,$locale);
        }       
    }
    public function getQuestions($riskProfileQuestionnaireId)
    {
        $questions = $this->findBy(["riskProfileQuestionnaireId" => $riskProfileQuestionnaireId],["displayOrder" => "asc"]);
        foreach ($questions as &$question)
        {
            $question->questionsLocale = $this->getQuestionLocales($question->id);
        }
        return $questions;
    }
    public function getQuestionLocales($questionid)
    {
        $questionsLocale = $this->container->get("RiskProfileQuestionnaireQuestionLocaleService")->findBy(["riskProfileQuestionnaireQuestionId" => $questionid]);
        $questionsLocaleHash = [];
        foreach ($questionsLocale as $local)
        {
            $questionsLocaleHash[$local->languageId] = $local;
        }
        return $questionsLocaleHash;
    }
    public function deleteQuestion($id)
    {
        $question  = $this->findOneBy(["id" => $id]);
        $this->em->remove($question);
        $questionsLocale = $this->container->get("RiskProfileQuestionnaireQuestionLocaleService")->findBy(["riskProfileQuestionnaireQuestionId" => $id]);
        foreach ($questionsLocale as $locale)
        {
            $this->em->remove($locale);
        }
        $this->em->flush();
        $answers = $this->container->get("RiskProfileQuestionnaireAnswerService")->findBy(["riskProfileQuestionnaireQuestionId" => $id]);
        foreach ($answers as $answer)
        {
            $this->container->get("RiskProfileQuestionnaireAnswerService")->deleteAnswer($answer->id);
        }
    }
    public function saveQuestionOrder($ids)
    {
        $displayOrder = 0;
        foreach ($ids as $id)
        {
            $question = $this->findOneBy(["id" => $id]);
            $question->displayOrder = $displayOrder;
            $displayOrder++;
        }
        $this->em->flush();
    }
}
