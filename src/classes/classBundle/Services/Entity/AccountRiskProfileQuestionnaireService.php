<?php
namespace classes\classBundle\Services\Entity;
class AccountRiskProfileQuestionnaireService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "AccountRiskProfileQuestionnaire";
    }
    public function save($entity,$array,$options = array("flush" => 1))
    {   
        $entity = parent::save($entity,$array,$options);
        $parents = ["Default"];
        $riskProfileService = $this->container->get("RiskProfileQuestionnaireService");
        $riskProfileService->init();        
        if ($riskProfileService->checkIfNotSelected($entity))
        {
            foreach ($parents as $parent)
            {
                if ($this->container->get($parent."RiskProfileQuestionnaireService")->findOneBy(["riskProfileQuestionnaireId" => $entity->riskProfileQuestionnaireId ]) != null)
                {
                    $this->remove($entity);
                    $this->flush();
                    return null;
                }               
            }
        }     
        return $entity;
    }
}

