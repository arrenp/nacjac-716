<?php
namespace classes\classBundle\Services\Entity;
class PlanRiskProfileQuestionnaireService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "PlanRiskProfileQuestionnaire";
    }  
    public function add($array,$options = array("flush" => 1))
    {
        $array['userid'] = $this->container->get("PlansService")->findOneBy(["id" => $array['planid']])->userid;
        return parent::add($array,$options);
    }
    public function save($entity,$array,$options = array("flush" => 1))
    {
        $entity = parent::save($entity,$array,$options);
        $parents = ["Account","Default"];
        $riskProfileService = $this->container->get("RiskProfileQuestionnaireService");
        $riskProfileService->init();        
        if ($riskProfileService->checkIfNotSelected($entity))
        {
            foreach ($parents as $parent)
            {
                if ($this->container->get($parent."RiskProfileQuestionnaireService")->findOneBy(["riskProfileQuestionnaireId" => $entity->riskProfileQuestionnaireId ]) != null)
                {
                    $this->remove($entity);
                    $this->flush();
                    return null;
                }               
            }
        }     
        return $entity;
    }
}


