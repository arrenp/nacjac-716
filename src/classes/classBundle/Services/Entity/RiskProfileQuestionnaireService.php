<?php
namespace classes\classBundle\Services\Entity;
class RiskProfileQuestionnaireService extends BaseEntityService
{
    private $linkTables;
    public function init()
    {
        $this->entityName = "RiskProfileQuestionnaire";
        $this->linkTables = ["Plan","Account","Default"];
        $this->pathSelectedFields = $this->container->get("PathsService")->getPathRiskSelections();
        $this->selectedFields = array_merge(["selected","isStadion","isKinetik"], $this->pathSelectedFields);
    }
    public function checkIfNotSelected($entity)
    {
        $notSelected = true;
        foreach ($this->selectedFields as $field)
            $notSelected = $notSelected && !$entity->$field;
        return $notSelected;
    }
    public function getQuestionnairesParams($type,$params = array())
    {
        if ($type == "Account" && !isset($params['userid']))
        {
            return false;
        }
        if ($type == "Plan" && !isset($params['planid']))
        {
            return false;
        }
        return $params;
    }
    public function getQuestionnaires($type,$params = array())
    {
        $params = $this->getQuestionnairesParams($type,$params);
        if ($params === false)
        {
            return array();
        }
        $questionnairesLinks = $this->container->get($type."RiskProfileQuestionnaireService")->findBy($params);
        $questionnaires = [];
        foreach ($questionnairesLinks as $questionnairesLink)
        {        
            $questionnaires[$questionnairesLink->riskProfileQuestionnaireId] = 
            [
               "RiskProfileQuestionnaire" => $this->container->get("RiskProfileQuestionnaireService")->findOneBy(["id" => $questionnairesLink->riskProfileQuestionnaireId]),          
               "Link" => $questionnairesLink
            ];
            if ($questionnaires[$questionnairesLink->riskProfileQuestionnaireId]["RiskProfileQuestionnaire"] == null)
            {
                unset($questionnaires[$questionnairesLink->riskProfileQuestionnaireId]);
            }
        }
        return $questionnaires;
    }
    public function getQuestionnairesAll($type,$sentparams = array())
    {
        $params = array();
        foreach ($sentparams as $key => $value)
        {
            if (in_array($key,["userid","planid"]))
            {
                $params[$key] = $value;
            }
        } 
        $questionnaires = array();
        foreach ($this->linkTables as $table)
        {
            $questionnaires[$table] = null;
        }
        $questionnaires['Current'] = $this->getQuestionnaires($type,$params);
        if ($type == "Account" || $type == "Plan")
        {
            $questionnaires['Default'] = $this->getQuestionnaires("Default");
        }
        if ($type == "Plan")
        {
            $params['userid'] = $this->container->get("PlansService")->findOneBy(["id" => $params['planid']])->userid;
            unset($params['planid']);
            $questionnaires['Account'] = $this->getQuestionnaires("Account",$params);            
        }  
        $parents = ["Default","Account"];
        $checkHash = [];
        foreach ($this->selectedFields as $field)
        {
            $checkHash[$field] = false;
        }
        foreach ($questionnaires['Current'] as $questionnaire)
        {
            foreach ($this->selectedFields as $field)
            {
                if ($questionnaire["Link"]->$field)
                {
                    $checkHash[$field] = true;
                }
            }
        }

        foreach ($parents as $parent)
        {
            foreach ($questionnaires[$parent] as $questionnaire)
            {
                foreach ($this->selectedFields as $field)
                {
                    if ($checkHash[$field] )
                    {
                         $questionnaire["Link"]->$field = false;
                    }
                }
            }
        }
        
        foreach ($questionnaires["Account"] as $key => $value)
        {
            if (isset($questionnaires["Default"][$key]))
            {
                foreach ($this->selectedFields as $field)
                    $this->syncAttributes($questionnaires, $key, $field);
                unset($questionnaires["Account"][$key]);
            }
        }
                
        foreach ($questionnaires["Current"] as  $key => $questionnaire)
        {           
            foreach ($parents as $parent)
            {
                if ($questionnaires[$parent] != null && isset($questionnaires[$parent][$key]))
                {
                    unset($questionnaires["Current"][$key]);
                    foreach ($this->selectedFields as $field)
                        $questionnaires[$parent][$key]["Link"]->$field = $questionnaire["Link"]->$field;
                }
            }
        }
        return $questionnaires;
    }
    private function syncAttributes(&$questionnaires, $key, $field) {
        if ($questionnaires["Account"][$key]["Link"]->$field)
        {
            foreach ($questionnaires["Default"] as &$questionnaire)
            {
                $questionnaire["Link"]->$field = false;
            }
            $questionnaires["Default"][$key]["Link"]->$field = true;
        }
    }
    public function addQuestionnaire($params)
    {
        $questionnaire = $this->add($params);
        $this->container->get($params['type']."RiskProfileQuestionnaireService")->add(array_merge($params,["riskProfileQuestionnaireId" => $questionnaire->id]));     
        return $questionnaire;
    }
    public function delete($id, $options = array("flush" => 1))
    {
        $questionnaire = $this->findOneBy(["id" => $id]);
        $this->remove($questionnaire);
        $i = 0;
        foreach ($this->linkTables as $linkTable)
        {
            $linkTableEntries = $this->container->get($linkTable."RiskProfileQuestionnaireService")->findBy(["riskProfileQuestionnaireId" => $id]);
            foreach ($linkTableEntries as $linkTableEntry)
            {
                $this->container->get($linkTable."RiskProfileQuestionnaireService")->remove($linkTableEntry);
                if ($i % 100 == 0)
                {
                    $this->em->flush();
                }
                $i++;
            }
        }   
        $this->em->flush();
        $questions = $this->container->get("RiskProfileQuestionnaireQuestionService")->findBy(["riskProfileQuestionnaireId" => $id]);
        foreach ($questions as $question)
        {
            $this->container->get("RiskProfileQuestionnaireQuestionService")->deleteQuestion($question->id);
        }        
    }
    public function getLinkTables()
    {
        return $this->linkTables;
    }
    public function getQuestionnaireByPlanId($planid,$language)
    {
        $allLanguages = $this->container->get("LanguagesService")->getAll();
        $languageid = $this->container->get("LanguagesService")->findOneBy(["name" => $language])->id;
        $userid = $this->container->get("PlansService")->findOneBy(["id" => $planid])->userid;
        $riskProfile = null;
        if ($this->session->get('managedAccountOptInValue')) {
            $riskProfile = $this->getCustomRiskProfile($userid, $planid, "isStadion");
        }
        if ($this->session->get('smart401k'))
        {
            $riskProfile = $this->getCustomRiskProfile($userid, $planid, "isKinetik");
        }
        if ($riskProfile === null) 
        {
            $riskProfile = $this->container->get("PlanRiskProfileQuestionnaireService")->findOneBy(["planid" => $planid,"selected" => true]);
        }
        if ($riskProfile == null)
        {
            $riskProfile = $this->container->get("AccountRiskProfileQuestionnaireService")->findOneBy(["userid" => $userid,"selected" => true]);
        }
        if ($riskProfile == null)
        {
            $riskProfile = $this->container->get("DefaultRiskProfileQuestionnaireService")->findOneBy(["selected" => true]);
        }
        $questionnaire = $this->container->get("RiskProfileQuestionnaireService")->findOneBy(["id" => $riskProfile->riskProfileQuestionnaireId ]);
        $questionsEntity = $this->container->get("RiskProfileQuestionnaireQuestionService")->findBy(["riskProfileQuestionnaireId" => $questionnaire->id],["displayOrder" => "asc"]);
        $questions = [];
        $thscoresEnabled = false;
        $min = 0;
        $max = 0;
        foreach ($questionsEntity as $questionEntity)
        {
            $questionLocales = $this->container->get("RiskProfileQuestionnaireQuestionLocaleService")->findBy(["riskProfileQuestionnaireQuestionId" => $questionEntity->id,"languageId" => $allLanguages]);
            $questionLocale = $this->container->get("RiskProfileQuestionnaireQuestionLocaleService")->findOneBy(["riskProfileQuestionnaireQuestionId" => $questionEntity->id,"languageId" => $languageid]);
            $answersEntity = $this->container->get("RiskProfileQuestionnaireAnswerService")->findBy(["riskProfileQuestionnaireQuestionId" => $questionEntity->id],["displayOrder" => "asc"]);
            $answers = array();
            $currentMin = 0;
            $currentMax = 0;
            foreach ($answersEntity as $answerEntity)
            {
                $answer = $this->container->get("RiskProfileQuestionnaireAnswerLocaleService")->findOneBy(["riskProfileQuestionnaireAnswerId" => $answerEntity->id,"languageId" => $languageid]);
                $answer->points = $answerEntity->points;
                $answer->thpoints = $answerEntity->thpoints;
                $answerLocales = $this->container->get("RiskProfileQuestionnaireAnswerLocaleService")->findBy(["riskProfileQuestionnaireAnswerId" => $answerEntity->id,"languageId" => $allLanguages]);
                if ($answer->thpoints > 0)
                {
                    $thscoresEnabled = true;
                }
                if ($answer->points < $currentMin)
                {
                    $currentMin = $answer->points;
                }
                if ($answer->points > $currentMax)
                {
                    $currentMax = $answer->points;
                }
                $answer = (array)$answer;
                $answer['locales'] = $answerLocales;
                $answers[] = $answer;
            }
            $min += $currentMin;
            $max += $currentMax;
            $questions[] =
            [
                "questionLocales" => $questionLocales,
                "question" => (array)$questionLocale,
                "answers" => $answers,
                "showPoints" => $questionEntity->showPoints
            ];
        }
        return ["questions" => $questions,"thscoresEnabled" => $thscoresEnabled,"min" => $min,"max" => $max,"questionnaire" =>$questionnaire];
    }
    public function loadQuestionsForApp($planid,$language)
    {
        $questions = $this->getQuestionnaireByPlanId($planid,$language)["questions"];
        $appQuestions = array();
        $translator = $this->container->get('translator')->getMessages();
        foreach ($questions as $question)
        {
            $options = array();
            $points = array();
            foreach ($question['answers'] as $answer)
            {
                if (!$question['showPoints'])
                {
                    $options[] = $answer['answer'];
                    $points[] = $answer['points'];
                }
                else
                {
                    if ($answer['points'] == 1)
                    {
                        $pointsDescription = $translator['investments']['point'];
                    }
                    else
                    {
                        $pointsDescription = $translator['investments']['points'];
                    }                    
                    $options[] = $answer['answer']." (".$answer['points']." ".$pointsDescription.")";
                }    
            }
            $appQuestions[] = 
            [                
                "text" =>  $question['question']['question'],
                "options" => $options,
                "optionsPoints" => $options, // dunno why this is options twice
                "points" => $points,
                "audio" => $question['question']['audio']
            ];
        }
        $appQuestions['total']= count($appQuestions);
        return $appQuestions;
    }
    public function customScoringTypes()
    {
        return 
        [
            [
                "value" => "portfolioMatch",
                "description" => "Portfolio Match"
            ],
            [
                "value" => "smart401k",
                "description" => "Smart401k"
            ]
        ];
    }
    function migrate()
    {
        $riskProfiles = array();
        $prefixes = ["","adp","horacemann","Lincoln","omnisp"];
        $languages = ["en","es"];
        $sections = ["","riskProfileAA","riskProfileADP","riskProfileAXA2013","riskProfileAXA2014_Ibbotson","riskProfileLNCLN7","riskProfileLW","riskProfileSPA6","riskProfileSPE6","riskProfileWIL6","smart401k","loringWard","riskProfileABGIL","riskProfileAXA"];
        
        foreach ($sections as $section)
        {
            foreach ($prefixes as $prefix)
            {
                $sql = "select count(plans.id) as count FROM plans LEFT JOIN accounts ON accounts.id = plans.userid WHERE plans.sectionsTemp = :section AND languagePrefix = :prefix";
                $query = $this->em->getConnection()->prepare($sql);
                $query->bindValue("section",$section);
                $query->bindValue("prefix",$prefix);
                $query->execute();
                if ((int)$query->fetch()['count'] > 0 || $prefix == "")
                {
                    $riskProfile['prefix'] = $prefix;
                    $riskProfile['section'] = $section;
                    if ($section == "")
                    {
                        $file = "default";
                    }
                    else
                    {
                        $file = $section;
                    }
                    if ($prefix == "")
                    {
                        $folder = "";
                    }
                    else
                    {
                        $folder = $prefix."_";
                    }
                    
                    $name = $file;
                    if ($prefix != "")
                    {
                        $name .= " ".$prefix;
                        
                    }
                    $riskProfile['name'] = $name;
                    $riskProfile['file'] = $file.".xml";
                    $riskProfile['folder'] = $folder;
                    $riskProfile['section'] = $section;
                    $riskProfile['prefix'] = $prefix;
                    $riskProfiles[] = $riskProfile;
                }
            }            
        }    
        
        $this->processRiskProfiles($riskProfiles, $languages);

        $this->migrateLinkAccounts();
        $this->migrateLinkPlans();
    }
    public function processRiskProfiles($riskProfiles, $languages) {
        foreach ($riskProfiles as $profile)
        {
            $xmls = [];
            $success = true;
            foreach ($languages as $language)
            {
                try
                {
                    $path = $this->container->get('kernel')->locateResource('@SpeAppBundle/Resources/translations/'.$profile['folder'].$language."/riskProfile/".$profile['file']);
                    $filecontents = file_get_contents($path); 
                } 
                // load english version of same language (spanish missing for some)
                catch (\Exception $ex) 
                {
                    try
                    {
                        $path = $this->container->get('kernel')->locateResource('@SpeAppBundle/Resources/translations/'.$profile['folder']."en/riskProfile/".$profile['file']);
                        $filecontents = file_get_contents($path); 
                    }
                    catch (\Exception $ex) 
                    {
                        $success = false;
                    }
                }         
                $xmls[$language] = simplexml_load_string($filecontents); 
            }
            if ($success)
            {
                $this->addRiskProfile($profile,$xmls);
            }
        }
    }
    public function addRiskProfile($riskProfile,$xmls)
    {
        $languages = $this->container->get("LanguagesService")->getAll();
        $params['type'] = "Default";
        $params['name'] = $riskProfile['name'];
        $params['selected'] = false;
        if ($riskProfile['name'] == "default")
        {
            $params['selected'] = true;
        }
        $params['scoringXml'] = trim($xmls["en"]->scoring->asXml());
        if ($riskProfile['section'] == "smart401k")
        {
            $params['customScoringType'] = "smart401k";
        }
        if ($riskProfile['section'] == "loringWard")
        {
            $params['customScoringType'] = "portfolioMatch";
        }
        
        $englishQuestions = $xmls['en']->questions->question;
        $spanishQuestions = $xmls['es']->questions->question;        
        $questionnaire = $this->addQuestionnaire($params);
        
        $englishCount = count($englishQuestions);
        for($i = 0;$i < $englishCount;$i++)
        {
            $questionParams = ["riskProfileQuestionnaireId" => $questionnaire->id,"showPoints" => (bool)(string)$englishQuestions[$i]->showPoints];
            $questions = [];
            $questions[] = ["languageId" => $languages['en']->id, "question" => $englishQuestions[$i]->text,"audio" => $englishQuestions[$i]->audio];
            $questions[] = ["languageId" => $languages['es']->id, "question" => $spanishQuestions[$i]->text,"audio" => $spanishQuestions[$i]->audio];
            $question = $this->container->get("RiskProfileQuestionnaireQuestionService")->addQuestion($questionParams,$questions);
            $englishAnswers = $this->getAnswers($englishQuestions[$i],$i,$riskProfile['section']);
            $spanishAnswers = $this->getAnswers($spanishQuestions[$i],$i,$riskProfile['section']);
            foreach ($englishAnswers as $key => $answer)
            {
                $locales = [];
                $params = ["riskProfileQuestionnaireQuestionId" => $question->id,"points" => $answer['points'],"thpoints" => $answer['thpoints']];
                $locales[] = ["languageId" => $languages['en']->id,"answer" =>$answer['answer']];
                $locales[] = ["languageId" => $languages['es']->id,"answer" =>$spanishAnswers[$key]['answer']];
                $params['locales']=$locales;
                $this->container->get("RiskProfileQuestionnaireAnswerService")->addAnswer($params);
            }           
        }
    }
    public function migrateLinkAccounts()
    {
        $questionnaires = $this->findAll();
        $i = 0;
        foreach ($questionnaires as $questionnaire)
        {
            if ($questionnaire->name != "default")
            {
                $params = $this->questionnaireArray($questionnaire->name);
                $sql = "SELECT accounts.id FROM accounts LEFT JOIN defaultPlan ON defaultPlan.userid = accounts.id WHERE defaultPlan.sectionsTemp = :sections";
                $languagePrefix = "accounts.languagePrefix = :languagePrefix";
                if ($params['languagePrefix'] == "")
                {
                    $languagePrefix .= " OR  accounts.languagePrefix is null";
                } 
                $sql .= " AND (".$languagePrefix.") group by accounts.id";
                $query = $this->em->getConnection()->prepare($sql);
                $query->bindValue("sections",$params['sections']);
                $query->bindValue("languagePrefix",$params['languagePrefix']);
                $query->execute();       
                $ids = $query->fetchAll();
                foreach ($ids as $id)
                {                  
                    $this->container->get("AccountRiskProfileQuestionnaireService")->add(["riskProfileQuestionnaireId" => $questionnaire->id,"selected" => true,"userid" => $id['id'] ],["flush" => 0]);       
                    if ($i % 100 == 0)
                    {
                        $this->em->flush();
                    }
                    $i++;
                }               
            }
        }
        $this->em->flush();
    } 
    public function migrateLinkPlans()
    {
        $questionnaires = $this->findAll();
        $i = 0;
        foreach ($questionnaires as $questionnaire)
        {
            if ($questionnaire->name != "default")
            {
                $params = $this->questionnaireArray($questionnaire->name);
                $sql = "SELECT plans.id,plans.userid FROM plans LEFT JOIN accounts ON accounts.id = plans.userid LEFT JOIN defaultPlan ON defaultPlan.userid = plans.userid WHERE defaultPlan.sectionsTemp != plans.sectionsTemp AND plans.sectionsTemp = :sections";
                $languagePrefix = "accounts.languagePrefix = :languagePrefix";
                if ($params['languagePrefix'] == "")
                {
                    $languagePrefix .= " OR  accounts.languagePrefix is null";
                }               
                $sql .= " AND (".$languagePrefix.") group by plans.id";
                $query = $this->em->getConnection()->prepare($sql);
                $query->bindValue("sections",$params['sections']);
                $query->bindValue("languagePrefix",$params['languagePrefix']);
                $query->execute();       
                $ids = $query->fetchAll();
                foreach ($ids as $id)
                {                  
                    $this->container->get("PlanRiskProfileQuestionnaireService")->add(["riskProfileQuestionnaireId" => $questionnaire->id,"selected" => true,"userid" => $id['userid'],"planid" => $id['id'] ],["flush" => 0]);    
                    if ($i % 100 == 0)
                    {
                        $this->em->flush();
                    }
                    $i++;
                }                
            }
        }
        $this->em->flush();
    } 
    public function questionnaireArray($name)
    {
        $array = explode(" ",$name);
        if (count($array) == 1)
        {
            return ["sections" => $name,"languagePrefix" => ""];
        }
        else
        {
            return ["sections" => $array[0],"languagePrefix" => $array[1]];
        }
        return $array;
    }
    function getAnswers($question,$i,$xml)
    {
        $answers = array();
        foreach($question->options->children() as $option)
        {
            $answer['answer'] = (string)$option;
            $answer['points'] = (array)$option->attributes()['value'];
            $points = (int)$answer['points'][0];
            $answer['points'] = $points;
            $answer['thpoints'] = 0;
            $condition = in_array($xml, array('riskProfileAXA', 'riskProfileLNCLN7')) && !($i < 6);
            $condition = $condition || ($xml == 'riskProfileAXA2013' && !($i < 4));
            $condition = $condition || ($xml == 'riskProfileAXA2014_Ibbotson' && !($i > 1));
            $condition = $condition || ($xml == 'riskProfileABGIL' && ($i == 5));
            $condition = $condition || ($xml == 'riskProfileADP' && ($i == 6));
            if ($condition)
            {
                $answer['points'] = 0;
                $answer['thpoints'] = $points; 
            }
            $answers[$points] = $answer;
        }       
        return $answers;        
    }

    private function getCustomRiskProfile($userid, $planid, $custom) {
        $riskProfile = $this->container->get("PlanRiskProfileQuestionnaireService")->findOneBy(["planid" => $planid, $custom => true]);
        if ($riskProfile === null)
        {
            $riskProfile = $this->container->get("AccountRiskProfileQuestionnaireService")->findOneBy(["userid" => $userid, $custom => true]);
        }
        if ($riskProfile === null)
        {
            $riskProfile = $this->container->get("DefaultRiskProfileQuestionnaireService")->findOneBy([$custom => true]);
        }
        return $riskProfile;
    }
    public function saveStadion($type, $id, array $findByParams = array())
    {
        $this->saveField($type, $id, $findByParams,"isStadion");
    }
    public function saveField($type, $id, array $findByParams = array(),$field) {
        $defaults = $this->container->get("{$type}RiskProfileQuestionnaireService")->findBy(array_merge($findByParams, [$field => true]));
        foreach ($defaults as $default) {
            $default->{$field} = false;
        }
        
        $questionnaire = $this->container->get("{$type}RiskProfileQuestionnaireService")->findOneBy(array_merge($findByParams,['riskProfileQuestionnaireId' => $id]));
        if (is_null($questionnaire)) {
            $questionnaire = $this->container->get("{$type}RiskProfileQuestionnaireService")->add(array_merge($findByParams,['riskProfileQuestionnaireId' => $id]));
        }
        
        $this->container->get("{$type}RiskProfileQuestionnaireService")->save($questionnaire,[$field => true]);
        $this->container->get("doctrine")->getManager()->flush();
    }
    
}
