<?php
namespace classes\classBundle\Services\Entity;
class RecordkeeperAdviceFundsService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "recordkeeperAdviceFunds";
    }
    public function findByWithHash($params,$hashIndex)
    {
        $funds =  $this->findBy($params);
        $fundHash = [];
        foreach ($funds as $fund)
        {
            $fundHash[$fund->{$hashIndex}] = $fund;
        }
        return $fundHash;
    }
}
