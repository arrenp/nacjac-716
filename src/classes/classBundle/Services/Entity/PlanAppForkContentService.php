<?php


namespace classes\classBundle\Services\Entity;


use classes\classBundle\Entity\AccountAppForkContent;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;

class PlanAppForkContentService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "PlanAppForkContent";
    }

    public function getForkContentByPlan($planid, $locale)
    {
        $forkContent = $this->findOneBy(['planid' => $planid]);

        if (!is_null($forkContent))
        {
            $content = $forkContent->getContent($locale);
            $return = array_merge($content, [
                'box_1_color' => $forkContent->boxOneColor,
                'box_2_color' => $forkContent->boxTwoColor,
                'button_1_color' => $forkContent->buttonOneColor,
                'button_2_color' => $forkContent->buttonTwoColor,
                'custom_html' => $forkContent->customHtml,
                'published' => $forkContent->published
            ]);
            return $return;
        }

    }
}