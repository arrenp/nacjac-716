<?php
namespace classes\classBundle\Services\Entity;
class AccountsUsersSystemMessagesService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "AccountsUsersSystemMessages";
    }      
    public function addSystemMessage($accountsUsersId,$message,$tags)
    {
        $newMessage = $this->container->get("SystemMessagesService")->add($message);
        $newTags = $this->container->get("SystemMessagesTagsService")->addTags($tags);
        $links = $this->container->get("SystemMessagesAndTagsLinkService")->addLinks($newMessage,$newTags);
        $accountUserMessage = $this->add(["accountsUsersId" => $accountsUsersId,"systemMessageId" => $newMessage->id]);
        return ["message" => $newMessage,"tags" => $newTags, "links" => $links,"accountUserMessage" => $accountUserMessage];
    }
    public function addSystemMessages($users,$message,$tags)
    {
        $newMessage = $this->container->get("SystemMessagesService")->add($message);
        $newTags = $this->container->get("SystemMessagesTagsService")->addTags($tags);
        $this->container->get("SystemMessagesAndTagsLinkService")->addLinks($newMessage,$newTags);
        $i = 1;
        foreach ($users as $user)
        {
            $this->add(["accountsUsersId" => $user->getId(),"systemMessageId" => $newMessage->id],array("flush" => 0));            
            if ($i % 100 == 0)
            {
                $this->em->flush();
            }    
            $i++;
        }
        $this->em->flush();
    }
    public function getCountOfUnreadMessages($id,$checkForRead = true)
    {
        $sql = "SELECT COUNT(DISTINCT(ausm.id)) as count FROM AccountsUsersSystemMessages ausm LEFT JOIN SystemMessages sm ON ausm.systemMessageId = sm.id WHERE ausm.accountsUsersId = :accountsUsersId AND ausm.deleted = 0  AND sm.expirationDate > :currentDate ";        
        if ($checkForRead)
        {
            $sql = $sql." AND  ausm.isRead = 0 ";
        }
        $query = $this->em->getConnection()->prepare($sql);
        $query->bindValue("accountsUsersId",$id);
        $query->bindValue("currentDate",date("Y-m-d H:i:s"));
        $query->execute();
        return (int)$query->fetch()['count'];
    }
    public function deleteMessage($id)
    {
        return $this->save($this->findOneBy(["id" => $id]),["deleted" => 1]);
    }
    public function markIsRead($accountsUsersId)
    {
        $messages = $this->findBy(array("accountsUsersId" => $accountsUsersId));
        foreach ($messages as $message)
        {
            $message->isRead = 1;
        }
        $this->em->flush();
    }   
}
