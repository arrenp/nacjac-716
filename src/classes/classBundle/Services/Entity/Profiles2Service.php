<?php
namespace classes\classBundle\Services\Entity;
use classes\classBundle\Entity\plansDocuments;
use classes\classBundle\Entity\Profiles2;
use classes\classBundle\Entity\Profiles2\AppData;
class Profiles2Service extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "Profiles2";
    }     
    public function add($params)
    {
        $plan = $this->container->get("PlansService")->findOneBy(["id" => $params['planid']]);
        $participant = $this->container->get("ParticipantsService")->findOneBy(["id" => $params['participantid']]);
        $cookieExists = $this->findOneBy(["cookie" => $params['cookie']]);       
        if (!empty($plan) && !empty($participant) && !empty($params['cookie']) && empty($cookieExists))
        {
            $profiles2 = new $this->classPathAdd;
            $params['userid'] = $plan->userid;
            $params['updateDate'] = $params['createDate'] = new \DateTime("now");
            $this->copy($profiles2,$params);
            $appData = new appData();
            $appData->setFields($params['appData']);
            $profiles2->setAppData($appData);
            $this->persist($profiles2);
            $this->flush();
            return true;
        }
        return false;
    }  
    public function edit($findBy,$params)
    {
        $profile = $this->findOneBy($findBy);
        $profile->loadJson($profile->appData);
        if (!empty($profile))
        {
            $params['updateDate'] = new \DateTime("now");
            $appData = $profile->getAppDataArray();
            if (!empty($params['appData']))
            {
                $appData->setFields($params['appData']);
                $profile->setAppData($appData);
            }  
            if (isset($params['enrollmentStatus']))
                $profile->enrollmentStatus = $params['enrollmentStatus'];
            $this->flush();
            return true;
        }
        return false;
    }
    public function getAppData($findBy)
    {
        $profile =  $this->findOneBy($findBy);
        if (!empty($profile))
        {
            $profile->loadJson($profile->appData);
            $data = get_object_vars($profile->getData());
            $plansDocumentsRepo = $this->doctrine->getRepository(plansDocuments::class);
            $data['documents'] = $plansDocumentsRepo->findBy(array('planid' => $data['planid']));
            $data['createDate'] = $data['createDate']->format('l, F j, Y, g:i A e');
            return $data;
        }
    }
}