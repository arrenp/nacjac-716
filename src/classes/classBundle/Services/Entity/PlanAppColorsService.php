<?php
namespace classes\classBundle\Services\Entity;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;

class PlanAppColorsService extends BaseEntityService
{
    public function init()
    {
        $this->entityName = "PlanAppColors";
    }

    public function getColorsByPlan($planid)
    {
        /* @var $connection Connection */
        $connection = $this->em->getConnection();

        $statement = $connection->prepare("
            SELECT 
                COALESCE(NULLIF(p.hex1, ''), NULLIF(a.hex1, '')) AS hex1,
                COALESCE(NULLIF(p.hex2, ''), NULLIF(a.hex2, '')) AS hex2,
                COALESCE(NULLIF(p.hex3, ''), NULLIF(a.hex3, '')) AS hex3,
                COALESCE(NULLIF(p.hex4, ''), NULLIF(a.hex4, '')) AS hex4
            FROM
                AccountAppColors a
                LEFT JOIN PlanAppColors p ON p.userid = a.userid
            WHERE
                p.planid=:planid
        ");
        $statement->execute([':planid' => $planid]);
        $colors = $statement->fetch(FetchMode::ASSOCIATIVE);
        if ($colors === false)
        {
            $colors = array_fill_keys(["hex1", "hex2", "hex3", "hex4"], null);
        }

        return $colors;
    }
}
