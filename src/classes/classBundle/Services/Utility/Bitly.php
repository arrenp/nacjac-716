<?php

namespace classes\classBundle\Services\Utility;

class Bitly {
	
	private $baseDomain;
	
	private $accessToken;
	
	private $lastError;
	
	public function __construct($baseDomain, $accessToken) {
		$this->baseDomain = $baseDomain;
		$this->accessToken = $accessToken;
	}
	
	public function shorten($url) {
		$url = str_replace(['&','?','#',' '], ['%26','%3F','%23','%20'], $url); 
		$response = $this->call("/v3/shorten", ['longUrl' => $url]);
		return $response !== false ? $response["data"]["url"] : $url;
	}
	
	public function getLastError() {
		return $this->lastError;
	}
	
	protected function call($endpoint, array $params = []) {
		$ch = curl_init();
		$defaults = ['access_token' => $this->accessToken];
		$params = array_merge($defaults, $params);
		$url = 'https://' . $this->baseDomain . $endpoint . '?' . http_build_query($params);
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPGET, true); 
		
		$response = curl_exec($ch);  

		if ($response === false) {
			$this->lastError = curl_error($ch);
		}
		else {
			$response = json_decode($response, true);
			if ($response["status_code"] != 200) {
				$this->lastError = !empty($response["status_txt"]) ? $response["status_txt"] : "Unknown";
				$response = false;
			}
		}
		curl_close($ch);
		return $response;
	}
	
}
