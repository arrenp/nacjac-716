<?php
namespace classes\classBundle\Services\Autos;
use classes\classBundle\Services\BaseService;
class AutosService extends BaseService
{
    
    private $tablePrefix;
    private $userid;
    private $planid;
    private $findByParams;
    public function set($params)
    {
        if (!empty($params['userid']))
        {
            $this->tablePrefix = "Default";
            $this->userid = $params['userid'];
            $this->findByParams = ["userid" => $this->userid];
        }
        if (!empty($params['planid']))
        {
            $this->tablePrefix = "Plans";
            $this->planid = $params['planid'];    
            $plan = $this->em->getRepository($this->classPath . "plans")->findOneBy(array("id" => $this->planid));
            $this->findByParams = ["planid" => $this->planid,"userid" => $plan->userid];           
        }
        if (!empty($params['profileid']))
        {
            $this->tablePrefix = "Profiles";
            $this->profileid = $params['profileid'];
            $profile =   $this->em->getRepository($this->classPath . "profiles")->findOneBy(array("id" => $this->profileid));
            $this->findByParams = ["profileid" => $this->profileid,"userid" => $profile->userid,"planid" => $profile->planid];           
        }
    }
    public function getData($table)
    {
        return $this->em->getRepository($this->classPath .$this->tablePrefix.$table)->findOneBy($this->findByParams);
    }
    
    public function addData($table,$params)
    {
        $entity = $this->getData($table);
        if (empty($this->getData($table)))
        {
            $entityTable =  "classes\\classBundle\\Entity\\".$this->tablePrefix.$table;
            $entity = new $entityTable;
            foreach ($this->findByParams as $key => $value)
            {
                $entity->$key = $value;
            }
        }
        foreach ($params as $key => $value)        
        {
            if (in_array($table,['AutoIncrease','AutoIncrease2','AutoEscalate']) && $key === 'startDate' && is_string($value)) {
                $value = $this->processDateField($value);
            }
            $entity->$key = $value;
        }
        $this->em->getManager()->persist($entity);
        $this->em->getManager()->flush();
        return $entity;
    }
    public function removeData($table)
    {    
        $entity = $this->getData($table);
        if (!empty($entity))
        {
            $this->em->getManager()->remove($entity);
            $this->em->getManager()->flush();
        }        
    }
    protected function processDateField($value) {
        if ($value instanceof \DateTime) {
            return $value;
        }
        if (empty($value)) {
            return null;
        }
        
        $value = new \DateTime($value);
        return $value instanceof \DateTime ? $value : null;
    }
    public function isAutos($plan)
    {
        $autoService = $this;
        $autoService->set(["userid" => $plan->userid, "planid" => $plan->id]);

        $autoEnroll = $autoService->getData('AutoEnroll');
        $autoEscalate = $autoService->getData('AutoEscalate');
        $autoIncrease = $autoService->getData('AutoIncrease');

        if ($autoEnroll->enabled || $autoEscalate->enabled || $autoIncrease->enabled) {
            return true;
        }
        return false;
    }
}
