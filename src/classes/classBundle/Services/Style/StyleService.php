<?php
namespace classes\classBundle\Services\Style;

use classes\classBundle\Services\BaseService;
class StyleService extends BaseService
{
    public function getColors($interface)
    {
        $general = $this->container->get('GeneralMethods');
        $interfaceColor = $interface->interfaceColor;

        $colors = array();
        $general->addSTDClassToArray($colors,6);
        $colors[0]->value = "blue";
        $colors[0]->colors = array("003366","005599","3399EE","99CCEE","CCDDEE");
        $colors[1]->value = "green";
        $colors[1]->colors = array("004400","006600","008800","99CC99","DDEEDD");
        $colors[2]->value = "gold";
        $colors[2]->colors = array("776644","998866","ccbb99","ccccbb","eeeeee");
        $colors[3]->value = "red";
        $colors[3]->colors = array("660000","990000","cc0000","ccbbbb","eeeeee");
        $colors[4]->value = "burgundy";
        $colors[4]->colors = array("550000","770000","990000","bbaaaa","eeeeee");
        $colors[5]->value = "charcoal";
        $colors[5]->colors = array("333333","666666","999999","cccccc","eeeeee");

        for ($i = 0; $i < count($colors); $i++)
        {
            $colors[$i]->title = $colors[$i]->value;
            $colors[$i]->title[0] = strtoupper($colors[$i]->title[0]);

            if ($colors[$i]->value == $interfaceColor)
                $colors[$i]->checked = "checked";
            else
                $colors[$i]->checked = "";
        }
        return $colors;
    }
    public function getLayouts($interface)
    {
        $general = $this->container->get('GeneralMethods');
        $interfaceLayout = $interface->interfaceLayout;
        $layouts = array();
        $general->addSTDClassToArray($layouts,2);
        $layouts[1]->value = "top";
        $layouts[0]->value = "side";

        $layouts[1]->title = "Navigation at Top";
        $layouts[0]->title = "Navigation at Left";

        $layouts[1]->image = "spe-nav-top.svg";
        $layouts[0]->image = "spe-nav-side.svg";


        for ($i = 0; $i < count($layouts); $i++)
            if ($layouts[$i]->value == $interfaceLayout)
                $layouts[$i]->checked = "checked";
            else
                $layouts[$i]->checked = "";
        return $layouts;
    }
    public function saveStyle($params,$service)
    {
        $interfaceParams = [];
        foreach (["interfaceColor","interfaceLayout"] as $field)
            if ($params[$field])                
                $interfaceParams[$field] = $params[$field];
        $entity = $service->findOneBy(["id" => $params['id']]);
        $service->save($entity,$interfaceParams);
    }
    public function saveHex($service,$params,$findBy)
    {
        $entity = $service->findOneBy($findBy);
        if (empty($entity))
            $service->add($params);
        else
            $service->save($entity,$params);
    }
    public function hexColorsList()
    {
        $list =  
        [
            "hex1" =>
            [
                "description" => "Primary"
            ],
            "hex2" =>
            [
                "description" => "Secondary"
            ],
            "hex3" =>
            [
                "description" => "Tertiary"
            ],
            "hex4" =>
            [
                "description" => "Accent 1"
            ]
        ];
        foreach ($list as $key => $row)
        {
            $list[$key]['field'] = $key;
        }
        return $list;
        
    }
    public function hexColorValues($entity)
    {
        $list = $this->hexColorsList();
        foreach ($list as $key => $value)
        {
            $list[$key]['value'] = $entity->{$key };
        }
        return $list;
    }
}