<?php

namespace classes\classBundle\Services\HubSpot;

use classes\classBundle\Services\BaseService;
use Doctrine\Common\Collections\Criteria;
use HubSpot\HubSpotBundle\Services\HubSpotContactsService;

class HubSpotDataMigrationService extends BaseService {
    
    private $emailToProperties = [];
    private $emailToPlanId = [];
    private $emailToNoEmail = [];
    private $emails = [];
    
    /** @var HubSpotContactsService */
    private $hubSpotService;
    
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $enrolleeRepository;
    
    public function __construct($container) {
        parent::__construct($container);
        $this->hubSpotService = $this->container->get('hub_spot.contacts');
        $this->enrolleeRepository = $this->container->get('doctrine')->getRepository('classesclassBundle:launchToolRecipients');
    }
    
    public function initializeSessionData() {
        $this->session->set('hubSpotDataMigrationCurrent', 0);
        $this->session->set('hubSpotDataMigrationTotal', 0);
        $this->session->set('hubSpotDataMigrationInvalidEmails', []);
        
        $this->emailToProperties = [];
        $this->emailToPlanId = [];
        $this->emails = [];
    }
    
    public function processAccount($accountId) {
        $this->initializeSessionData();
        $entityManager = $this->container->get('doctrine')->getManager();
        
        /* @var $enrolleeRepository \Doctrine\ORM\EntityRepository */
        $enrolleeRepository = $entityManager->getRepository('classesclassBundle:launchToolRecipients');
        $plansRepository = $entityManager->getRepository('classesclassBundle:plans');
        $statement = $plansRepository->createQueryBuilder('plans')
            ->select('plans.planid')
            ->distinct()
            ->where('plans.userid = :userid')
            ->andWhere('plans.participantDetails = :participantDetails')
            ->andWhere('plans.deleted = :deleted')
            ->setParameter('userid', $accountId)
            ->setParameter('participantDetails', 1)
            ->setParameter('deleted', 0)
            ->getQuery();
        $planIdsInTheWrongFormat = $statement->getResult();

        $planIds = [];
        foreach ($planIdsInTheWrongFormat as $planId) {
            $planIds[] = $planId['planid'];
        }
        
        $current = 0;
        $total = 0;
        
        // Calculate total enrollees
        foreach ($planIds as $planId) {
            $plan = $plansRepository->findOneBy(['planid' => $planId, 'userid' => $accountId, 'deleted' => 0]);
            $criteria = $this->getCriteria($plan->id, $accountId, $planId);
            $total += $enrolleeRepository->createQueryBuilder('e')->select('COUNT(e.id)')->addCriteria($criteria)->getQuery()->getSingleScalarResult();
        }
        
        $this->session->set('hubSpotDataMigrationTotal', $total);
        $this->session->save();
        $this->hubSpotService->clearInvalidEmails();
        
        foreach ($planIds as $planId) {

            $plan = $plansRepository->findOneBy(['planid' => $planId, 'userid' => $accountId, 'deleted' => 0]);
            $criteria = $this->getCriteria($plan->id, $accountId, $planId);
            $enrollees = $enrolleeRepository->matching($criteria);
            
            foreach ($enrollees as $enrollee) {
                $current = $current + 1;
                
                if (false === ($properties = $this->createArray($plan, $enrollee))) {
                    continue;
                }

                $this->emailToProperties[$properties['email']] = $properties;
                $this->emailToPlanId[$properties['email']] = $plan->id;
                $this->emailToNoEmail[$properties['email']] = $properties['noemail'];

                $result = $this->hubSpotService->addToBatch($properties['email'], $properties);
                $this->processResponse($result, $current);   
            }
        }
        
        $result = $this->hubSpotService->flushBatch();
        $this->processResponse($result, $current);

        $currentInvalidEmails = $this->session->get('hubSpotDataMigrationInvalidEmails');
        $this->session->set('hubSpotDataMigrationCurrent', $total);
        return ['invalidEmails' => $currentInvalidEmails, 'emails' => $this->emails];
    }
    
    public function processPlan($plan) {
        $this->initializeSessionData();
        $current = 0;
        $criteria = $this->getCriteria($plan->id);
        $total = $this->enrolleeRepository->createQueryBuilder('e')->select('COUNT(e.id)')->addCriteria($criteria)->getQuery()->getSingleScalarResult();
        
        $this->session->set('hubSpotDataMigrationTotal', $total);
        $this->session->save();
        $this->hubSpotService->clearInvalidEmails();
        
        $enrollees = $this->enrolleeRepository->matching($criteria);

        foreach ($enrollees as $enrollee) {
            $current = $current + 1;

            if (false === ($properties = $this->createArray($plan, $enrollee))) {
                continue;
            }

            $this->emailToProperties[$properties['email']] = $properties;
            $this->emailToPlanId[$properties['email']] = $plan->id;
            $this->emailToNoEmail[$properties['email']] = $properties['noemail'];

            $result = $this->hubSpotService->addToBatch($properties['email'], $properties);
            $this->processResponse($result, $current);   
        }
        
        $result = $this->hubSpotService->flushBatch();
        $this->processResponse($result, $current);
        $this->session->set('hubSpotDataMigrationCurrent', $total);
    }
    
    
    public function processEnrollee($enrolleeId) {
        $entityManager = $this->container->get('doctrine')->getManager();
        
        $enrolleeRepository = $entityManager->getRepository('classesclassBundle:launchToolRecipients');
        $plansRepository = $entityManager->getRepository('classesclassBundle:plans');
        
        $enrollee = $enrolleeRepository->find($enrolleeId);
        $plan = $plansRepository->find($enrollee->planid);
        
        if ($enrollee === null || $plan === null) {
            return false;
        }
        
        $properties = $this->createArray($plan, $enrollee);
        $result = $this->hubSpotService->replaceContact($properties['email'], $properties);
        
        return $result !== false;
    }
    
    private function createArray($plan, $enrollee) {
        $email = trim($enrollee->email);
        $properties = [];
        if ($email == 'Email') {
            return false;
        }
        else if ($email == '') {
            $email = "No Email - {$enrollee->firstName}, {$enrollee->lastName}";
            $properties['noemail'] = true;
        }
        $properties['participant_id'] = $enrollee->id;
        $properties['plan_id'] = $plan->planid;
        $properties['partner_id'] = $plan->partnerid;
        $properties['firstname'] = $enrollee->firstName;
        $properties['lastname'] = $enrollee->lastName;
        $statusCode = 0;
        $statusDescription = 'Not Enrolled';
        if (in_array(strtolower(trim($enrollee->enrolled)), ['y', 'yes'])) {
            $statusCode = 1;
            $statusDescription = 'Enrolled';
        }
        $properties['status_code'] = $statusCode;
        $properties['status_code_description'] = $statusDescription;
        $properties['mobilephone'] = '';
        $properties['email'] = $email;
        $properties['first_eligible_status_date'] = '';
        $properties['last_eligible_status_date'] = '';
        $properties['status_code_at_enrollment'] = '';
        $properties['provider_logo'] = !empty($plan->providerLogoEmailImage) ? $plan->providerLogoEmailImage : $plan->providerLogoImage;
        $properties['sponsor_logo'] = !empty($plan->sponsorLogoEmailImage) ? $plan->sponsorLogoEmailImage : $plan->sponsorLogoImage;
        $properties['smart_plan'] = ($plan->smartPlanAllowed ? 'Y' : 'N');
        $properties['smart_enroll'] = ($plan->smartEnrollAllowed ? 'Y' : 'N');
        $properties['smart_plan_participant_login_website_address'] = ($plan->smartPlanAllowed ? $plan->participantLoginWebsiteAddress : '');
        $partLoginWebsiteAddress = $plan->participantLoginWebsiteAddress;
        if (!empty($partLoginWebsiteAddress)) {
            $partLoginWebsiteAddress .= (strrpos($partLoginWebsiteAddress, "?") !== false) ? "&" : "?";
            $partLoginWebsiteAddress .= "smartenrollpartid=" . $enrollee->smartEnrollPartId;
        }
        $properties['smart_enroll_participant_login_website_address'] = ($plan->smartEnrollAllowed ? $partLoginWebsiteAddress : '');
        $properties['hr_contact_first_name'] = $plan->HRFirst;
        $properties['hr_contact_last_name'] = $plan->HRLast;
        $properties['hr_contact_email'] = $plan->HREmail;
        $properties['support_contact_first_name'] = $plan->supportContactFirstName;
        $properties['support_contact_last_name'] = $plan->supportContactLastName;
        $properties['contact_email'] = $plan->supportContactEmail;
        $properties['contact_phone'] = $plan->customerServiceNumber;
        if (!is_null($plan->lastDayForLoans)) {
            $properties['last_day_for_loans'] = $plan->lastDayForLoans->format('m/d/Y');
        }
        if (!is_null($plan->freezeTransactionsDate)) {
            $properties['freeze_transactions_date'] = $plan->freezeTransactionsDate->format('m/d/Y');
        }
        if (!is_null($plan->takeoverDate)) {
            $properties['takeover_date'] = $plan->takeoverDate->format('m/d/Y');
        }
        if (!is_null($plan->freezeEndDate)) {
            $properties['freeze_end_date'] = $plan->freezeEndDate->format('m/d/Y');
        }
        $properties['sox_notice_url'] = $plan->soxNoticeURL;
        $properties['transition_period_over'] = $plan->transitionPeriodOver ? 'Yes' : 'No';
        $properties['plan_name'] = $plan->name;
        
        return $properties;
    }

    private function processResponse($result, $current) {
        if ($result === HubSpotContactsService::BATCH_ERROR || $result === HubSpotContactsService::BATCH_FLUSHED) {
            // Update progress only when contacts sent to hubspot
            $this->session->set('hubSpotDataMigrationCurrent', $current);
            
            $invalidEmails = $this->hubSpotService->getInvalidEmails(); 
            if (count($invalidEmails) > 0) {
                
                // Extract valid emails and try to resend to hubspot
                $validEmails = array_diff_key($this->emailToProperties, array_flip($invalidEmails));
                $resubmitResult = $this->hubSpotService->replaceGroup($validEmails);
                if (!$resubmitResult) {
                    $invalidEmails = array_merge($invalidEmails, array_keys($validEmails));
                }
            }
            else if ($result === HubSpotContactsService::BATCH_ERROR) {
                $invalidEmails = array_keys($this->emailToProperties);
            }
            
            // Map invalid emails to enrollee id and plan id and store in session
            $currentInvalidEmails = $this->session->get('hubSpotDataMigrationInvalidEmails');
            foreach ($invalidEmails as $invalidEmail) {
                //$currentInvalidEmails[$invalidEmail] = $this->emailToProperties[$invalidEmail]['participant_id'];
                $currentInvalidEmails[$invalidEmail] = [
                    'participant_id' => $this->emailToProperties[$invalidEmail]['participant_id'],
                    'plan_id' => $this->emailToPlanId[$invalidEmail],
                    'noemail' => $this->emailToNoEmail[$invalidEmail]
                ];
            }
            $this->session->set('hubSpotDataMigrationInvalidEmails', $currentInvalidEmails);
            
            // Update stats
            $sentEmailsWithInvalidsRemoved = array_diff_key($this->emailToPlanId, array_flip($invalidEmails));
            foreach ($sentEmailsWithInvalidsRemoved as $email => $planid) {
                $this->emails[$planid]["sent"][] = $email;
            }

            $this->hubSpotService->clearInvalidEmails();
            $this->session->save();
            
            $this->emailToProperties = [];  
            $this->emailToPlanId = [];
        }
    }
    
    protected function getCriteria($planid, $accountId = null, $recordkeeperPlanid = null) {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->eq('planid', $planid));
        if (!is_null($accountId) && !is_null($recordkeeperPlanid)) {
            $criteria->orWhere(
                $criteria->expr()->andX(
                    $criteria->expr()->eq('recordkeeperPlanid', $recordkeeperPlanid),
                    $criteria->expr()->eq('partnerid', $accountId)
                )
            );
        }
        return $criteria;
    }
}
