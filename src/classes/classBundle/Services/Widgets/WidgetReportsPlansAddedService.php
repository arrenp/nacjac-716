<?php
namespace classes\classBundle\Services\Widgets;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Services\BaseService;
use Shared\General\GeneralMethods;
class WidgetReportsPlansAddedService extends BaseService
{
    public function calculatePlansAddedReport($request) {
        $report = array();
        $report[0]['header'] = array("Account ID" => "userid",
            "Plan ID" => "planid",
            "Plan Name" => 'name',
            "Company" => "company",
            'Total Count' => 'totcount');

        $data = $this->getData($request);

        $i = 0;
        $currentAccount = null;
        $count = 0;

        foreach ($data as $row) {
            if ($row['userid'] != $currentAccount) {
                if ($currentAccount != null) {
                    $this->addCountRow($currentAccount, $count, $report, $i);
                    $count = 0;
                    $i++;
                }
                $currentAccount = $row['userid'];
            }

            $report[$i]['userid'] = $row['userid'];
            $report[$i]['planid'] = $row['id'];
            $report[$i]['name'] = $row['name'];
            $report[$i]['company'] = $row['company'];
            $count++;
            $i++;
        }

        if ($count > 0) {
            $this->addCountRow($currentAccount, $count, $report, $i);
        }

        return $report;
    }

    private function addCountRow($currentAccount, $count, &$report, $i) {
        $report[$i]['userid'] = $currentAccount;
        $report[$i]['totcount'] = $count;
    }

    public function getData($request)
    {
        ini_set('memory_limit', '512M');
        $GeneralMethods = $this->container->get("GeneralMethods");
        $endDate = $GeneralMethods->addDaysToString($request['endDate']);
        $connection = $this->connection;

        $sql = "SELECT plans.id,plans.userid,plans.name,accounts.company from plans LEFT JOIN accounts on accounts.id = plans.userid where createdDate <= '".$endDate."' AND createdDate >= :startDate and not (deleted = 1 and deletedDate <= :endDate) AND plans.smartPlanAllowed =1";
        if (isset($request['userid'])) {
            $sql .= " AND userid = :userid";
        }
        $sql .= " order by userid";

        $stmt = $connection->prepare($sql);
        $stmt->bindValue('startDate', $request['startDate']);
        $stmt->bindValue('endDate', $endDate);

        if (isset($request['userid'])) {
            $stmt->bindValue('userid', $request['userid']);
        }

        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
}

