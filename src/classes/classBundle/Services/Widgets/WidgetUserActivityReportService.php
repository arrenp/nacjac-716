<?php
namespace classes\classBundle\Services\Widgets;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Services\BaseService;
use Shared\General\GeneralMethods;
use  classes\classBundle\MappingTypes\EncryptedType;
class WidgetUserActivityReportService extends BaseService
{
    public function getData($type,$request)
    {
        $GeneralMethods = $this->container->get("GeneralMethods");
        $endDate = $GeneralMethods->addDaysToString($request['endDate']);
        $connection = $this->connection;
        $dateString = " AND CTAStat.createDate <= '".$endDate."' AND CTAStat.createDate >= '".$request['startDate']."' ";
        $appendSql = " AND CTAStat.cta IN (".$this->container->get("WidgetReportsGlobalService")->returnCTAListQuery($type).")  AND CTAStat.id = (SELECT MAX(CTAStat2.id) FROM CTAStat CTAStat2 WHERE CTAStat2.uniqid = CTAStat.uniqid AND  CTAStat2.cta IN (".$this->container->get("WidgetReportsGlobalService")->returnCTAListQuery($type).")".$dateString.")".$dateString;
        if (!isset($request['planid']))
        $appendSql = $appendSql." AND plans.testing = 0 ";   
        $appendSql = $appendSql." ORDER by plans.partnerid ";
        $selectString = "SELECT CTAStat.*,plans.name as plans_name,plans.partnerid as plans_partnerid,plans.planid as plans_planid,AES_DECRYPT(CTAStat.firstName, UNHEX('".EncryptedType::KEY."')) as firstName,AES_DECRYPT(CTAStat.email, UNHEX('".EncryptedType::KEY."')) as email FROM CTAStat LEFT JOIN plans ON plans.id = CTAStat.planid WHERE ";
        if (isset($request['adviserid']))
        {
            $sql = "SELECT * FROM accountsUsers WHERE id = ".$request['adviserid'];
            $adviser = $this->connection->executeQuery($sql)->fetch();
            if ($adviser['allPlans'])
            $sql = $selectString." CTAStat.userid = ".$adviser['userid']." ".$appendSql;
            else
            $sql = $selectString." CTAStat.planid IN (SELECT plans.id FROM plans LEFT JOIN accountsUsersPlans ON accountsUsersPlans.planid = plans.id WHERE accountsUsersPlans.accountsUsersId = ".$request['adviserid'].")".$appendSql;
        }
        else if (isset($request['planid']))
        $sql = $selectString." CTAStat.planid = ".$request['planid']." ".$appendSql;
        else if (isset($request['userid']))
        $sql = $selectString." CTAStat.userid = ".$request['userid']." ".$appendSql;
        else 
        $sql = $selectString." CTAStat.id > 0 ".$appendSql;
        $result = $connection->fetchAll($sql);
        return $result;        
    }
    
    public function calculateData($type,$request)
    {
        $data = $this->getData($type,$request);
        $this->performance = array();
        if (!isset($request['reportListType']))
        $this->initPerformance();
        $performances = array();
        $report = array();
        if ($type == "powerview")
        {
            $report[0]['header'] = array("Plan Name" => "planname",
            "Plan allow for online deferral changes" => "defchanges",
            "Date engaged" => "createDate",
            "First Name" => "firstname","Last Name" => "lastname","Email" => "email",
            "Age" => "age","Salary" => "salary",
            "Estimated balance at retirement" => "projectedIncome",
            "Estimated monthly payout at retirement" => "projectedMonthlyIncome",            
            "Match Received" => "matchreceived",
            "Match Transacted" => "matchtrans",
            "Match current deferral rate" => "predeferral",
            "Match Deferral Rate Change Amount" => "diffDeferral",
            "Match Deferral Rate Change Transacted" => "curdeferral",
            "Revisit SmartPlan Received" => "revistSmartplanReceived",
            "Revisit SmartPlan clicked SP button" => "spbutton",
            "Check Out Provider's Website" => "providersWebsite","CTA Check Out Provider's Website clicked to engage" => "providersWebsiteClicked"        
            );
        }
        if ($type == "irio")
        {         
            $report[0]['header'] = array("Plan Name" => "planname",
            "Plan allow for online deferral changes" => "defchanges",
             "Date engaged" => "createDate",
            "First Name" => "firstname","Last Name" => "lastname","Email" => "email",
            "Age" => "age","Salary" => "salary",
             "Estimated balance at retirement presented" => "projectedIncome",
             "Estimated monthly payout at retirement presented" => "projectedMonthlyIncome",                
            "Deferral Rate received from RKP" => "preDef",
            "Deferral Rate transacted to RKP" => "rothDef","Deferral Rate Change" => "defchange",
            "Deferral Rate email instructions sent" => "deferralEmailInstructionsSent",
            "Percent increase selected(1,2,3)" => "percentIncreasedSelected",
            "Other retirement account balance" => "otherRetirementAccountBalance",
            "Spouse's retirement account balance" => "totalSpouseAccountBalance","Spouse's annual income" => "totalSpouseAnnualIncome","Spouse's age" => "spouseAge");                        
        }
        $isrollOverReport = !isset($request['adviserid']) && !isset($request['planid']);
        if ($isrollOverReport)
        {
            $report[0]['header']  = array_merge(array("Client Name" => "clientname"),$report[0]['header'] );
        }
        $connection = $this->connection;
        foreach ($report[0]['header'] as $key => $value)
        {
            $newkey = str_replace(" ","&nbsp;",$key);
            unset($report[0]['header'][$key]);
            $report[0]['header'][$newkey] = $value;         
        }
        $i = 0;
        foreach ($data as $row)
        {
            if (isset($request['reportListType']))
            $this->performance =   $performances[$row['planid']];
            $name = explode(" ",$row['firstName']);
            if ($isrollOverReport)
            $report[$i]['clientname'] = $row['plans_partnerid'];
            $report[$i]['id'] = $row['id'];
            $report[$i]['planname'] = $row['plans_name'];
            $report[$i]['defchanges'] = $row['postContributions'];
            $report[$i]['firstname'] = isset($name[0]) ? $name[0] : '';
            $report[$i]['lastname'] = isset($name[1]) ? $name[1] : '';
            $report[$i]['email'] = $row['email'];
            $report[$i]['text'] = "Place holder";
            $report[$i]['age'] = $row['age'];
            $report[$i]['salary'] = "$".number_format($row['salary'],2);
            $report[$i]['createDate'] = $row['createDate'];
            if ($row['projectedMonthlyIncome'] != null)
            $report[$i]['projectedMonthlyIncome']  = "$".number_format($row['projectedMonthlyIncome'],2);
            else
            $report[$i]['projectedMonthlyIncome'] = "No data";
            
            if ($row['projectedIncome'] != null)
            $report[$i]['projectedIncome']  = "$".number_format($row['projectedIncome'],2);
            else
            $report[$i]['projectedIncome'] = "No data"; 
            
            if ($row['maxMatch'] > 0)
            $report[$i]['matchreceived'] = 1;
            if ($type == "irio")
            {
                $report[$i]['preDef'] = $row['predeferral'];
                $report[$i]['rothDef'] = $row['curdeferral'];
                $report[$i]['defchange'] = $row['curdeferral'] - $row['predeferral'];//cur - pre
                $report[$i]['dropoff'] = $row['complete'];//completed
                $report[$i]['deferralEmailInstructionsSent'] = (int)!$row['postContributions'];//completed
                $report[$i]['percentIncreasedSelected'] = $row['curdeferral'] - $row['predeferral'];//cur - pre
                $report[$i]['otherRetirementAccountBalance'] = "$".number_format($row['otherRetirementAccountBalance'],2);//other_balance from data session
                $report[$i]['totalSpouseAccountBalance'] = "$".number_format($row['totalSpouseAccountBalance'],2);
                $report[$i]['totalSpouseAnnualIncome'] = "$".number_format($row['totalSpouseAnnualIncome'],2);//data session
                $report[$i]['spouseAge'] = $row['spouseAge'];//data session
            }
            if ($type == "powerview")
            {
                //$report[$i]['employerMatch'] = $row['employerMatch'];
                $sql = "SELECT * FROM CTAStat where cta ='%TYPE%' AND uniqid = '".$row['uniqid']."' ORDER by id desc LIMIT 1";
                $sqlPowerviewRedirect = str_replace("%TYPE%","PowerviewRedirect",$sql);
                $sqlPowerviewSmartPlan = str_replace("%TYPE%","PowerviewSmartPlan",$sql);
                $sqlPowerview = str_replace("%TYPE%","Powerview",$sql);
                $powerview = $connection->executeQuery($sqlPowerview)->fetch();
                $powerviewRedirect = $connection->executeQuery($sqlPowerviewRedirect)->fetch();
                $powerviewSmartPlan = $connection->executeQuery($sqlPowerviewSmartPlan)->fetch();
                $report[$i]['spbutton'] = 0;
                $report[$i]['providersWebsite'] = 0;
                $report[$i]['providersWebsiteClicked'] = 0;
                $report[$i]['matchreceived'] = 0;
                $report[$i]['matchtrans'] = 0;
                $report[$i]['curdeferral'] = 0;
                $report[$i]['diffDeferral'] = 0;
                $report[$i]['predeferral'] = 0;
                $report[$i]['onlineDeferralChanges'] = "placeholder";
                $report[$i]['revistSmartplanReceived'] = 0;
                if ($powerview != null)
                {
                    if ($powerview['complete'])
                    {
                        $report[$i]['matchtrans'] = 1;
                        $report[$i]['matchreceived'] = 1;
                    }
                    $report[$i]['predeferral'] = $powerview['predeferral'];
                    $report[$i]['diffDeferral'] = $powerview['diffDeferral'];
                    $report[$i]['curdeferral'] = $powerview['curdeferral'];
                }
                if ($powerviewSmartPlan != null)
                {
                    $report[$i]['spbutton'] = $powerviewSmartPlan['complete'];
                    $report[$i]['revistSmartplanReceived'] = 1;
                }
                if ($powerviewRedirect != null)
                {                    
                    $report[$i]['providersWebsiteClicked'] =  $powerviewRedirect['complete'];
                    $report[$i]['providersWebsite'] = 1;
                }               
            }           
            $this->updatePerformance($row,$report[$i]);   
            if (isset($request['reportListType']))
            {
                $this->finalizePerformance();
                $performances[$row['planid']] = $this->performance;
            }
            $i++;
        }
        if (!isset($request['reportListType']))
        {
            $this->finalizePerformance();
        }
        else
        $this->performance = $performances;
        return $report;
    }
    public function initPerformance()
    {
        $this->performance['deferralOff'] = 0;
        $this->performance['deferralOn'] = 0;
        $this->performance['totalSalary'] = 0;
        $this->performance['totalRecords'] = 0;
        $this->performance['totalBalance'] = 0;
        $this->performance['totalBalanceCounter'] = 0;
        $this->performance['numberOfUsersReceivedMaxMatch'] = 0;   
        $this->performance['numberOfTransactions'] = 0; 
        $this->performance['maxMatchdeferralEntry'] = 0;
        $this->performance['maxMatchdeferralExit'] = 0;
        $this->performance['maxMatchdeferralTotal'] = 0;
        $this->performance['providersWebsiteClicked'] = 0;
        $this->performance['spbutton'] = 0;
        $this->performance['0-8_trans'] = 0;
        $this->performance['8-16_trans'] = 0;
        $this->performance['16-24_trans'] = 0;
        $this->performance['trans_total_time'] = 0;
        $this->performance['totalNumberOfUsersDeferralOff'] = 0;
    }
    public function updatePerformance($row,$report)
    {
        $this->performance['partnerid'] = $row['plans_partnerid'];
        $this->performance['planid'] = $row['plans_planid'];
        $this->performance['accounts'][$row['userid']] = 1;
        $this->performance['plans'][$row['planid']] = 1;
        
        if (!$report['defchanges'])
        {
            $this->performance['plansDefOff'][$row['planid']] = 1;
            $this->performance['totalNumberOfUsersDeferralOff']++;
        }
        else
        $this->performance['plansDefOn'][$row['planid']] = 1;
        
        

        
        $this->performance['totalSalary'] = $this->performance['totalSalary'] + $row['salary'];
        if ($report['projectedIncome'] != "No data")
        {
            $this->performance['totalBalance'] = $this->performance['totalBalance'] + $row['projectedIncome'];
            $this->performance['totalBalanceCounter']++;
        }
        if ($report['matchreceived'])
        $this->performance['numberOfUsersReceivedMaxMatch']++;
        if ($report['matchtrans'])
        $this->performance['numberOfTransactions']++;
        if ($report['matchreceived'])
        {
            $this->performance['maxMatchdeferralEntry'] = $this->performance['maxMatchdeferralEntry'] + $report['predeferral'];
            $this->performance['maxMatchdeferralExit'] = $this->performance['maxMatchdeferralExit'] + $report['curdeferral'];
            $this->performance['maxMatchdeferralTotal']++;
        }
        if ($report['providersWebsiteClicked'])
        $this->performance['providersWebsiteClicked']++;
        if ($report['spbutton'])
        $this->performance['spbutton']++;
        $createDate = explode(" ",$row['createDate'])[1];
        if ($report['matchtrans'] )
        $this->updateTransTimePerformanceAll($createDate);
        if ($report['spbutton'] )
        $this->updateTransTimePerformanceAll($createDate);  
        if ($report['providersWebsiteClicked'] )
        $this->updateTransTimePerformanceAll($createDate);    
        $this->performance['totalRecords']++;
    }
    function updateTransTimePerformanceAll($createDate)
    {
        if (!$this->updateTransTimePerformance($createDate,"08:00:00","0-8_trans"))
        if (!$this->updateTransTimePerformance($createDate,"16:00:00","8-16_trans"))
        $this->updateTransTimePerformance($createDate,"24:00:00","16-24_trans");
    }
    function updateTransTimePerformance($createDate,$time,$performanceKey)
    {
        if (strcmp($createDate,$time) < 0)
        {
            $this->performance[$performanceKey]++;
            $this->performance['trans_total_time']++;
            return true;
        }
        return false;
    }
    public function finalizePerformance()
    {
        if ($this->performance['totalRecords'] == 0)
        {
            $this->performance['averageSalary'] = 0;
            $this->performance['averageBalance']  = 0;
            $this->performance['numberofclients'] = 0;
            $this->performance['numberofplans'] = 0;
            $this->performance['maxMatchdeferralEntryAverage'] = 0;
            $this->performance['maxMatchdeferralExit'] = 0;
            $this->performance['0-8_trans_percent'] = 0;
            $this->performance['8-16_trans_percent'] = 0;
            $this->performance['16-24_trans_percent'] = 0;
            $this->performance['deferralOff'] = 0;
            $this->performance['deferralOn'] = 0;
        }
        else
        {
            $this->performance['averageSalary'] = $this->performance['totalSalary']/$this->performance['totalRecords'];
            if ($this->performance['totalBalanceCounter'] > 0)
            $this->performance['averageBalance'] = $this->performance['totalBalance']/$this->performance['totalBalanceCounter'];
            else
            $this->performance['averageBalance'] = 0;
            $this->performance['numberofclients'] = count($this->performance['accounts']);
            $this->performance['numberofplans'] = count($this->performance['plans']);
            $this->performance['deferralOff'] = count($this->performance['plansDefOff']);
            $this->performance['deferralOn'] = count($this->performance['plansDefOn']);
            if ($this->performance['maxMatchdeferralTotal'] == 0)
            {
                $this->performance['maxMatchdeferralEntryAverage'] = 0;
                $this->performance['maxMatchdeferralExitAverage'] = 0;
            }
            else
            {
               $this->performance['maxMatchdeferralEntryAverage'] = $this->performance['maxMatchdeferralEntry']/$this->performance['maxMatchdeferralTotal'];
               $this->performance['maxMatchdeferralExitAverage'] = $this->performance['maxMatchdeferralExit']/$this->performance['maxMatchdeferralTotal'];
            }
            if ($this->performance['trans_total_time'] == 0)
            {
                $this->performance['0-8_trans_percent'] = 0;
                $this->performance['8-16_trans_percent'] = 0;
                $this->performance['16-24_trans_percent'] = 0;                
            }
            else
            {
                $this->performance['0-8_trans_percent'] =  ($this->performance['0-8_trans']/$this->performance['trans_total_time']) * 100;
                $this->performance['8-16_trans_percent'] = ($this->performance['8-16_trans']/$this->performance['trans_total_time']) * 100;
                $this->performance['16-24_trans_percent'] = ($this->performance['16-24_trans']/$this->performance['trans_total_time']) * 100;  
            }
        }

    }    
}