<?php
namespace classes\classBundle\Services\Widgets;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Services\BaseService;
use classes\classBundle\Classes\ga;
use classes\classBundle\Classes\mailgunEngine;
use Shared\General\GeneralMethods;
class WidgetsPerformanceReportService extends BaseService
{
    public function globals($request)
    {
        $GeneralMethods = $this->container->get("GeneralMethods");
        $request['endDate'] = $GeneralMethods->addDaysToString($request['endDate']);
        $this->reportType = $request['reportType'];
        $this->dateString = " AND CTAStat.createDate >= '".$request['startDate']."' AND CTAStat.createDate <= '".$request['endDate']."' " ;
        $this->dateString2 = str_replace("CTAStat","CTAStat2",$this->dateString);        
        $this->type = "global";
        $this->userid = 0;
        $this->adviserid = 0;
        $this->planid = 0;
        $this->request = $request;       
        if (isset($request['userid']))
        {
            $this->type = "provider";
            $this->userid = $request['userid'];
        }
        if (isset($request['adviserid']))
        {
            $this->type = "adviser";            
            $this->adviserid = $request['adviserid'];
        }
        if (isset($request['planid']))
        {
            $this->type = "plan";   
            $this->planid = $request['planid'];
        }
    }
    public function sqlUnique($ctas,$additionalSql)
    {
        $querystring = $this->sqlCTAListQueryString($ctas);
        return " CTAStat.id = (SELECT MAX(CTAStat2.id) FROM CTAStat CTAStat2 WHERE  CTAStat2.cta IN(".$querystring.") ".$this->dateString2." ".$additionalSql.") ";             
    }
    public function sqlUniqueUser($ctas = null)
    {
        return $this->sqlUnique($ctas," AND CTAStat.planid = CTAStat2.planid AND CTAStat.uniqid = CTAStat2.uniqid ");       
    }
    public function sqlUniquePlan($ctas = null)
    {
        return $this->sqlUnique($ctas," AND CTAStat.planid = CTAStat2.planid ");            
    }
    public function sqlCTAListQueryString($ctas = null)
    {
        $querystring = "";
        if ($ctas == null)
        $querystring = $this->container->get("WidgetReportsGlobalService")->returnCTAListQuery($this->reportType);
        else
        foreach ($ctas as $cta)
        {
            if ($querystring != "")
            $querystring = $querystring.",";
            $querystring = $querystring."'".$cta."'";
        }
        return $querystring;
    }
    public function sqlWhere($ctas = null)
    {
        $querystring = $this->sqlCTAListQueryString($ctas);
        $request =$this->request;
        $sqlWhere = " WHERE CTAStat.cta IN(".$querystring.") ".$this->dateString;   
        if (isset($request['userid']))
        {
            $sqlWhere = $sqlWhere." AND CTAStat.userid = ".$this->userid." ";           
        }
        if (isset($request['adviserid']))
        {
            $sql = "SELECT * FROM accountsUsers WHERE id = ".$request['adviserid'];
            $adviser = $this->connection->executeQuery($sql)->fetch();                        
            if ($adviser['allPlans'])
            $sqlWhere = $sqlWhere." AND CTAStat.userid = ".$adviser['userid']." ";           
            else
            $sqlWhere = $sqlWhere." AND CTAStat.planid IN (SELECT planid FROM accountsUsersPlans WHERE accountsUsersId='".$this->adviserid."')";
        }
        if (isset($request['planid']))
        {

            $sqlWhere = $sqlWhere." AND CTAStat.planid = ".$this->planid;
        }
        if (!isset($request['planid']))
        $sqlWhere = $sqlWhere." AND CTAStat.planid not in (SELECT id FROM plans WHERE testing = 1) ";
        return $sqlWhere;
    }
    public function addRow(&$report,$description,$value)
    {
        $counter = count($report);
        $report[$counter]['description'] = $description;
        $report[$counter]['value'] = $value;
    }
    public function name()
    {
        if ($this->type == "global")
        return "vWise";
        if ($this->type == "provider")
        {
            $sql = "Select * FROM accounts WHERE id =".$this->userid;
            $account =  $this->connection->executeQuery($sql)->fetch();
            return $account['company']." - ".$account['partnerid'];
        }
        if ($this->type == "adviser")
        {
            $sql = "Select * FROM accountsUsers WHERE id =".$this->adviserid;
            $accountUsers =  $this->connection->executeQuery($sql)->fetch();
            return $accountUsers['firstname']." - ".$accountUsers['lastname'];
        }
        if ($this->type == "plan")
        {
            $sql = "Select * FROM plans WHERE id =".$this->planid;
            $plan =  $this->connection->executeQuery($sql)->fetch();
            return $plan['name']." - ".$plan['planid'];            
        }
    }
    public function transacted($period)
    {
        if ($this->reportType == "irio")
        $type = "Irio";
        if ($this->reportType == "powerview")
        $type = "Powerview";        
        $top = $this->transactedSegment($period,$type);
        $bottom = 0;
        for($i = 1; $i <= 3;$i++)
        {
            $bottom = $bottom + $this->transactedSegment($i,$type);
        }
        if ($bottom == 0)
        return 0;
        else
        {
            
         $percent = ($top/$bottom * 100);
         return number_format($percent,2)."%";
        }
    }
    public function transactedSegment($period,$type)
    {
        /*
        if ($type == "Powerview")
        $ctas = array("Powerview","PowerviewRedirect","PowerviewSmartPlan");
        else
        $ctas = array($type);    
         * 
         */
        if ($period == 1)
        {
            $startTime = "00:00:00";
            $endTime = "08:00:00";
        }
        if ($period == 2)
        {
            $startTime = "08:00:00";
            $endTime = "16:00:00";
        }
        if ($period == 3)
        {
            $startTime = "16:00:00";
            $endTime = "24:00:00";
        } 
        if ($type == "Powerview")
        $ctas = array("Powerview","PowerviewRedirect","PowerviewSmartPlan");
        else
        $ctas = array("Irio");
        $createDateSql = "DATE_FORMAT(CTAStat.createDate,'%H:%i:%s')";
        $sql = "SELECT COUNT(id) as count  FROM CTAStat  ".$this->sqlWhere($ctas)."  AND complete = 1  AND "
        . " ".$createDateSql." >= '".$startTime."' AND ".$createDateSql." < '".$endTime."' AND ".$this->sqlUniqueUser($ctas);
        return $this->connection->executeQuery($sql)->fetch()['count'];          
    }
    public function revistSmartPlan()
    {
        $type = 'PowerviewSmartPlan';
        $ctas = array($type);
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere($ctas)." AND ".$this->sqlUniqueUser($ctas)." AND  CTAStat.cta = '".$type."'";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }    
    public function providersWebsiteClicked()
    {
        $type = 'PowerviewRedirect';
        $ctas = array($type);
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere($ctas)." AND ".$this->sqlUniqueUser($ctas)." AND  CTAStat.cta = '".$type."'";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }
    public function maximizeMatchEnterDeferralRate()
    {
        $type = 'Powerview';
        $ctas = array($type);
        $sql = "SELECT AVG(predeferral) as count FROM CTAStat  ".$this->sqlWhere($ctas)." AND ".$this->sqlUniqueUser($ctas)." AND  CTAStat.cta = '".$type."'";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }
    public function maximizeMatchExitDeferralRate()
    {
        $type = 'Powerview';
        $ctas = array($type);       
        $sql = "SELECT AVG(curdeferral) as count FROM CTAStat  ".$this->sqlWhere($ctas)." AND ".$this->sqlUniqueUser($ctas)." AND  CTAStat.cta = '".$type."'";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }    
    public function maximizeMatchTransactions()
    {
        $type = 'Powerview';
        $ctas = array($type);         
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere($ctas)." AND ".$this->sqlUniqueUser($ctas)." AND CTAStat.complete = 1 AND CTAStat.cta = '".$type."'";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }
    public function maximizeMatchVisted()
    {
        $type = 'Powerview';
        $ctas = array($type); 
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere($ctas)." AND ".$this->sqlUniqueUser($ctas)." AND  CTAStat.cta = '".$type."'";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    } 
    public function totalNumberOfPlans()
    {
        $sql = "SELECT COUNT(DISTINCT(CTAStat.planid)) as count FROM CTAStat  ".$this->sqlWhere();
        return $this->connection->executeQuery($sql)->fetch()['count'];
    }
    public function totalNumberOfAccounts()
    {
        $sql = "SELECT COUNT(DISTINCT(CTAStat.userid)) as count FROM CTAStat  ".$this->sqlWhere();
        return $this->connection->executeQuery($sql)->fetch()['count'];
    }
    public function totalNumberOfPlansDeferralOff()
    {
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniquePlan()." AND CTAStat.postContributions = 0";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }
    public function totalNumberOfPlansDeferralOn()
    {
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniquePlan()." AND CTAStat.postContributions = 1";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }
    public function totalNumberOfUniqueUsers()
    {
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser(); 
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }  
    public function reportHeader()
    {
        return array("Description" => "description","Value" => "value");
    }
    public function totalNumberOfUsersDeferralOff()
    {
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser()." AND CTAStat.postContributions = 0";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }
    public function totalNumberOfUsersDeferralOn()
    {
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser()." AND CTAStat.postContributions = 1";  
        return $this->connection->executeQuery($sql)->fetch()['count'];   
    }    
    public function select($percent)
    {
        $sql = "SELECT COUNT(id) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser()." AND (CTAStat.curdeferral-CTAStat.predeferral) = ".$percent; 
        return $this->connection->executeQuery($sql)->fetch()['count'];       
    }
    public function enteringDeferral()
    {
        $sql = "SELECT AVG(CTAStat.predeferral) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser(); 
        return $this->connection->executeQuery($sql)->fetch()['count'];
    }
    public function exitDeferral()
    {
        $sql = "SELECT AVG(CTAStat.curdeferral) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser(); 
        return $this->connection->executeQuery($sql)->fetch()['count'];
    }
    public function deferralChange()
    {
        $exit = $this->exitDeferral();
        $enter = $this->enteringDeferral();
        if ($exit != 0)
        return (($exit/$enter)-1)*100;  
        return 0;
    }
    public function averageSalary()
    {
        $sql = "SELECT AVG(CTAStat.salary) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser(); 
        return $this->connection->executeQuery($sql)->fetch()['count'];
    }  
    public function totalSpouseBalance()
    {
        $sql = "SELECT SUM(CTAStat.totalSpouseAccountBalance) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser(); 
        return $this->connection->executeQuery($sql)->fetch()['count'];
    }   
    
    public function selectedDeferralAll()
    {
        $return = "";
        $total = $this->totalNumberOfUniqueUsers();
        if ($total == 0)
        return "N/A";
        for ($i = 1; $i <= 3;$i++)
        {
            if ($i > 1)
            $return = $return.",";
            $return = $return. number_format(($this->select($i)/$total) * 100,2)."%";           
        }
        return $return;
    }
    public function selectedDeferral($i)
    {
        $total = $this->totalNumberOfUniqueUsers();
        if ($total == 0)
        return "N/A";
        $return = number_format(($this->select($i)/$total) * 100,2)."%";                   
        return $return;        
    }
    public function averageBalance()
    {
        $sql = "SELECT AVG(CTAStat.balance) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser(); 
        return $this->connection->executeQuery($sql)->fetch()['count'];
    }
    public function averageOtherRetirementAccountBalance()
    {
        $sql = "SELECT AVG(CTAStat.otherRetirementAccountBalance) as count FROM CTAStat  ".$this->sqlWhere()." AND ".$this->sqlUniqueUser(); 
        return $this->connection->executeQuery($sql)->fetch()['count'];               
    }
    public function ga()
    {
        if ($this->ga == null)
        $this->ga = new ga("124352310");  
        $this->gaParams =  array(
        'start-date' => $this->request['startDate'],
        'end-date'   => $this->request['endDate'],
        'filters'    => 'ga:customVarValue5=@'.$this->reportType.';ga:customVarValue4=@widget.smartplanenterprise.com'
        );
        
        if ($this->userid != 0)
        $this->gaParams['filters'] = $this->gaParams['filters'].";".'ga:customVarValue1=@'.$this->userid;  
        if ($this->planid != 0)
        $this->gaParams['filters'] = $this->gaParams['filters'].";".'ga:customVarValue2=@'.$this->planid;
        if ($this->adviserid != 0)
        {
            $sql = "SELECT * FROM accountsUsers WHERE id = ".$this->adviserid;
            $adviser = $this->connection->executeQuery($sql)->fetch();
            if (!$adviser['allPlans'])
            {
                $sql = "SELECT planid FROM accountsUsersPlans WHERE accountsUsersId=".$this->adviserid;
                $plans = $this->connection->fetchAll($sql);
                
                foreach ($plans as $plan)
                {
                    $gastring = 'ga:customVarValue2=@'.$plan['planid'];
                    $this->gaParams['filters'] = $this->gaParams['filters'].";".$gastring;
                }
                if (!isset($this->gaParams['filters']))
                $this->gaParams['filters'] = 'ga:customVarValue1=@-1';
            }
            else
            $this->gaParams['filters'] = $this->gaParams['filters'].";".'ga:customVarValue1=@'.$adviser['userid'];           
        }
        //var_dump($this->gaParams);
        //echo $this->gaParams['filters'];
        //$this->gaParams['filters'] = 'ga:customVarValue1=@1294,ga:customVarValue1=@1299';
    }
    public function topBrowser()
    {   
        return $this->ga->topBrowser($this->gaParams);        
    }
    public function topCities()
    {
        return $this->ga->topCities($this->gaParams);        
    }
    public function topMobileDevice()
    {
        return $this->ga->topMobileDevice($this->gaParams);
    }
    public function totalVisits()
    {
        return $this->ga->totalVisits($this->gaParams);
    }
    public function totalUniqueVisits()
    {
        return $this->ga->totalUniqueVisits($this->gaParams);
    }
    public function percentageOfUniqueVisits()
    {
        return $this->ga->percentageOfUniqueVisits($this->gaParams);
    }
    public function returningUsersPercent()
    {
        return $this->ga->returningUsersPercent($this->gaParams);
    }
    public function getEmailStats()
    {            
        $reportType = strtoupper($this->reportType);
        $mailgunEngine = new mailgunEngine();
        $sqlPlans = "SELECT * FROM CTAStat LEFT JOIN plans ON plans.id = CTAStat.planid WHERE testing = 0 %additional% group by  plans.id";
        if ($this->userid != 0)
        {
           $additional = " AND CTAStat.userid = ".$this->userid;
           $sqlPlans = str_replace("%additional%",$additional,$sqlPlans);
           $plans = $this->connection->executeQuery($sqlPlans)->fetchAll();
        }          
        else if ($this->planid != 0)
        {
           $repository = $this->em->getRepository('classesclassBundle:plans');
           $plan = $repository->findOneBy(array("id" => $this->planid));
           $tag = $reportType."_".$plan->partnerid."_".$plan->planid;
        }    
        else 
        {
            $sqlPlans = str_replace("%additional%","",$sqlPlans);
            $plans = $this->connection->executeQuery($sqlPlans)->fetchAll();
        }

        $startTime = new \dateTime($this->request['startDate']);
        $endTime = new \dateTime($this->request['endDate']);
        
        $diff = (int)$startTime->diff($endTime)->format("%a");
        
        $start = date("D, d M Y",$startTime->getTimestamp());
        $end = date("D, d M Y",$endTime->getTimestamp());
        
        $start = $start." 00:00:00 PSD";
        $end = $end." 23:59:59 PSD";
        
        $options = array("start" => $start,'end' => $end);
        if ($diff > 365)
        $options['resolution']  = "month";
        else
        $options['resolution'] = "day";
        
        if ($this->planid != 0)
        return $mailgunEngine->getStatsViaTagId($tag,$options);     
        else
        {
            foreach ($plans as $plan)
            {
                $tag = $reportType."_".$plan['partnerid']."_".$plan['planid'];
                if (!isset($stats))
                $stats = $mailgunEngine->getStatsViaTagId($tag,$options);
                else
                $mailgunEngine->appendStats ($stats,$mailgunEngine->getStatsViaTagId($tag,$options));                    
            }
            return $stats;
        }
    }
}


