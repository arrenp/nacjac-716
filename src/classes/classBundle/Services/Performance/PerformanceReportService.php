<?php
namespace classes\classBundle\Services\Performance;

use classes\classBundle\Services\BaseService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Classes\ga;
//use Permissions\RolesBundle\Classes\rolesPermissions;
use Plans\PlansBundle\Controller\ga\GoogleAnalyticsAPI;
ini_set("memory_limit","2000M");
set_time_limit(0);

class PerformanceReportService extends BaseService
{
    public function getPlansSql($account, $plan, $acctLevel, $start, $end)
    {
        $tableName = "plans";
        $whereArray = [];

        if($acctLevel == "advisor") {
            if($plan != "")
                return 1;
            $sql = "SELECT COUNT(*) AS count FROM accountsUsersPlans INNER JOIN plans ON plans.id = accountsUsersPlans.planid  WHERE accountsUsersPlans.accountsUsersId = '$account' AND testing = 0 and (deletedDate >= '$start' or deleted = 0) and (createdDate <= '$end') and (plans.smartEnrollAllowed = 1 OR plans.smartPlanAllowed = 1)";

        } else {
            array_push($whereArray,
                $this->accountQuery($account, $tableName, $acctLevel),
                $this->planQuery($plan, $tableName));
            $whereString = $this->createWhereSql($whereArray, false);

            $sql = "SELECT COUNT(*) AS count FROM " . $tableName ." WHERE " . $whereString . " and plans.testing = 0 and (deletedDate >= '$start' or deleted = 0) and (createdDate <= '$end') and (plans.smartEnrollAllowed = 1 OR plans.smartPlanAllowed = 1)";
            if ($whereString == "") {
                $sql = "SELECT COUNT(*) AS count FROM " . $tableName ." WHERE " . $whereString . "plans.testing = 0 and (deletedDate >= '$start' or deleted = 0) and (createdDate <= '$end') and (plans.smartEnrollAllowed = 1 OR plans.smartPlanAllowed = 1)";
            }

        }
        return $sql;       
    }
    public function getPlansCount($account, $plan, $acctLevel, $start, $end)
    {        
        return number_format($this->connection->executeQuery($this->getPlansSql($account, $plan, $acctLevel, $start, $end))->fetch()['count']);
    }

    public function getUniqueUsersCount($account, $plan, $start, $end, $acctLevel = null) //savedprofiels
    {
        $tableName = "profiles";
        $whereArray = [];
        $leftJoinArray = [];

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "reportDate >= '" . $start . "'",
            "reportDate <= '" . $end . "'",
            "reportDate is not null",
            "reportDate != ''",
            "profiles.reportDate != '0000-00-00 00:00:00'",
            "profiles.id > 0",
            "profiles.planid != 0",
            "profiles.userid != 0",
            "profilestatus = 1");
        $whereString = $this->createWhereSql($whereArray);

        array_push($leftJoinArray,
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on profiles.planid = plans.id");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);

        $sql = "SELECT COUNT(DISTINCT(participantid)) AS count FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereString;
        return $this->connection->executeQuery($sql)->fetch()['count'];
    }

    public function getNewEnrolleesCount($account, $plan, $start, $end, $acctLevel = null)
    {
        $tableName = "participants";
        $whereArray = [];
        $leftJoinArray = [];

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "profiles.reportDate >= '" . $start . "'",
            "profiles.reportDate <= '" . $end . "'",
            "profilesContributions.preCurrent = 0",
            "profilesContributions.rothCurrent = 0",
            "profiles.currentBalance = 0");
        $whereString = $this->createWhereSql($whereArray);

        array_push($leftJoinArray,
            "profiles on profiles.participantid = participants.id",
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on profiles.planid = plans.id");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);

        $sql = "SELECT COUNT(DISTINCT(profiles.participantid)) AS count FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereString;
        return number_format($this->connection->executeQuery($sql)->fetch()['count']);
    }

    public function getNewEnrolleesAvgDef($account, $plan, $start, $end, $acctLevel = null)
    {
        //need to round to two decimal places at the end
        $tableName = "participants";
        $whereArray = [];
        $leftJoinArray = [];

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "profiles.reportDate >= '" . $start . "'",
            "profiles.reportDate <= '" . $end . "'",
            "profilesContributions.preCurrent = 0",
            "profilesContributions.rothCurrent = 0",
            "profiles.currentBalance = 0");
        $whereString = $this->createWhereSql($whereArray);

        array_push($leftJoinArray,
            "profiles on profiles.participantid = participants.id",
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on profiles.planid = plans.id");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);

        $sql = "SELECT AVG(profilesContributions.deferralPercent) AS avg FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereString;
        return round($this->connection->executeQuery($sql)->fetch()['avg'],2);

    }

    public function getInvestmentTypes($account, $plan, $start, $end, $acctLevel = null)
    {
        $tableName = "profiles";
        $investmentTypes = ["ADVICE","CUSTOM", "DEFAULT", "RISK", "TARGET"];
        $whereArray = [];
        $leftJoinArray = [];

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "profiles.reportDate >= '" . $start . "'",
            "profiles.reportDate <= '" . $end . "'");
        $whereString = $this->createWhereSql($whereArray);

        array_push($leftJoinArray,
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on profiles.planid = plans.id",
            "profilesInvestments  on profiles.id = profilesInvestments.profileid");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);


        $sum = 0;
        foreach($investmentTypes as $investment) {
            $sql[$investment] = "SELECT COUNT(DISTINCT(profiles.participantid)) AS count FROM " . $tableName . " " . $leftJoinString . " WHERE $whereString AND profilesInvestments.type = '" . $investment . "'";
            $result[$investment] = $this->connection->executeQuery($sql[$investment])->fetch()['count'];

            if(is_numeric($result[$investment]))
                $sum += $result[$investment];
        }

        foreach($investmentTypes as $investment) {
            $result[$investment] = round(($result[$investment] / $sum) * 100);
        }

        return $result;
    }

    public function getAvgSal($account, $plan, $start, $end, $acctLevel = null)
    {
        $tableName = "participants";
        $whereArray = [];
        $leftJoinArray = [];

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "profiles.reportDate >= '" . $start . "'",
            "profiles.reportDate <= '" . $end . "'");
        $whereString = $this->createWhereSql($whereArray);

        array_push($leftJoinArray,
            "profiles on profiles.participantid = participants.id",
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on profiles.planid = plans.id");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);

        $sql = "SELECT AVG(profilesContributions.currentYearlyIncome) AS avg FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereString;
        return number_format(round($this->connection->executeQuery($sql)->fetch()['avg'],2),2);
    }

    public function getPlansWithActivity($account, $plan, $start, $end, $acctLevel = null)
    {
        $sql = $this->getPlansSql($account, $plan, $acctLevel, $start, $end)." AND plans.id IN (SELECT profilesInner.planid FROM profiles profilesInner  WHERE profilesInner.planid= plans.id AND profilesInner.reportDate >= '$start' and profilesInner.reportDate <= '$end')";
        return number_format($this->connection->executeQuery($sql)->fetch()['count']);
    }

    public function getAvgEnteringDefRate($account, $plan, $start, $end, $acctLevel = null)
    {
        return $this->getAvgDefRate($account, $plan, $start, $end, $acctLevel, 0);
    }

    public function getAvgExitingDefRate($account, $plan, $start, $end, $acctLevel = null)
    {
        return $this->getAvgDefRate($account, $plan, $start, $end, $acctLevel, 1);
    }


    public function getAvgDefRate($account, $plan, $start, $end, $acctLevel = null, $exiting)
    {
        //called by exiting and entering defrate
        $tableName = "profilesContributions";
        $whereArray = [];
        $leftJoinArray = [];

        $innerTableName = "profiles";

        if($exiting) {
            $innerSelectSql = "max(profiles.reportDate)";
            $selectSql = "AVG(coalesce(deferralPercent,0))";
        } else {
            $innerSelectSql = "min(profiles.reportDate)";
            $selectSql = "AVG(coalesce(preCurrent,0)+coalesce(rothCurrent,0))";
        }


        $innerSql = "SELECT " . $innerSelectSql . " FROM " . $innerTableName . " WHERE profilesContributions.profileid = profiles.id AND profiles.reportDate >= '" . $start ."' AND profiles.reportDate <= '" . $end . "' GROUP BY profiles.participantid";


        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "(profiles.currentBalance > 0 OR coalesce(preCurrent,0)+coalesce(rothCurrent,0) > 0)",
            "profilesContributions.deferralPercent != 0",
            "profiles.reportDate = ( ". $innerSql . " )");
        $whereString = $this->createWhereSql($whereArray);

        array_push($leftJoinArray,
            "profiles on profiles.id = profilesContributions.profileid",
            "plans on plans.id = profiles.planid");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);

        $sql = "SELECT " . $selectSql . " AS avg FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereString;
        return round($this->connection->executeQuery($sql)->fetch()['avg'],2);
    }

    public function getAvgAccountBalance($account, $plan, $start, $end, $acctLevel = null)
    {
        $whereArray = [];
        $leftJoinArray = [];

        $innerTableName = "profiles";

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereArray,
            $this->accountQuery($account, $innerTableName, $acctLevel),
            $this->planQuery($plan, $innerTableName),
            "profiles.reportDate >= '" . $start . "'",
            "profiles.reportDate <= '" . $end . "'");
        $whereString = $this->createWhereSql($whereArray);

        array_push($leftJoinArray,
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on plans.id = profiles.planid");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);

        $innerSql = "SELECT currentBalance FROM " . $innerTableName . " ". $leftJoinString . " WHERE " . $whereString . " GROUP BY participantid DESC";


        $sql = "SELECT AVG(currentBalance) AS avg FROM ( " . $innerSql . " ) AS SQ";
        return number_format(round($this->connection->executeQuery($sql)->fetch()['avg'],2), 2);
    }


    public function getTotalOutsideAssets($account, $plan, $start, $end, $acctLevel = null)
    {
        $tableName = "profiles";
        $whereArray = [];
        $leftJoinArray = [];

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "profiles.reportDate >= '" . $start . "'",
            "profiles.reportDate <= '" . $end . "'");
        $whereString = $this->createWhereSql($whereArray);

        array_push($leftJoinArray,
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on profiles.planid = plans.id",
            "(SELECT MAX(id) as id, profileid FROM profilesRetirementNeeds WHERE otherAssets > 0 GROUP BY profileid) profilesRetirementNeedsLatest ON profiles.id = profilesRetirementNeedsLatest.profileid",
            "profilesRetirementNeeds ON profilesRetirementNeeds.id = profilesRetirementNeedsLatest.id");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);

        $sql = "SELECT SUM(profilesRetirementNeeds.otherAssets) AS sum FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereString;
        return number_format($this->connection->executeQuery($sql)->fetch()['sum']);
    }

    public function getIraUsersCount($account, $plan, $start, $end, $acctLevel = null)
    {
        return $this->getAssetTypeUsersCount($account, $plan, $start, $end, $acctLevel, "IRA");
    }

    public function getFormerRetirementUsersCount($account, $plan, $start, $end, $acctLevel = null)
    {
        return $this->getAssetTypeUsersCount($account, $plan, $start, $end, $acctLevel, "Former Retirement Account");
    }

    public function getAssetTypeUsersCount($account, $plan, $start, $end, $acctLevel = null, $assetType)
    {
        $tableName = "profiles";
        $whereArray = [];
        $leftJoinArray = [];

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "profilesRetirementNeeds.assetTypes LIKE '%" . $assetType ."%'",
            "profiles.reportDate >= '" . $start . "'",
            "profiles.reportDate <= '" . $end . "'");
        $whereString = $this->createWhereSql($whereArray);

        array_push($leftJoinArray,
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on profiles.planid = plans.id",
            "profilesRetirementNeeds ON profiles.id = profilesRetirementNeeds.profileid");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);

        $sql = "SELECT COUNT(distinct(profiles.participantid)) AS count FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereString;
        return $this->connection->executeQuery($sql)->fetch()['count'];
    }

    public function getTransacted($start, $end, $timeStart, $timeEnd, $account, $plan, $acctLevel)
    {
        //3 dif times 12a-8a, 8a-4p, 4p-1159p
        $tableName = "profiles";
        $whereTransactedArray = [];
        $whereAllArray = [];
        $leftJoinArray = [];

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }

        array_push($whereAllArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "profiles.reportDate >= '" . $start . "'",
            "profiles.reportDate <= '" . $end . "'");
        $whereAllString = $this->createWhereSql($whereAllArray);

        $whereTransactedArray = $whereAllArray;

        array_push($whereTransactedArray,
            "DATE_FORMAT(profiles.reportDate,'%H:%i:%s') >= '" . $timeStart . "'",
            "DATE_FORMAT(profiles.reportDate,'%H:%i:%s') <= '" . $timeEnd . "'");
        $whereTransactedString = $this->createWhereSql($whereTransactedArray);

        array_push($leftJoinArray,
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on profiles.planid = plans.id");
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);

        $transactedTimeSql = "SELECT COUNT(distinct(profiles.participantid)) AS count FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereTransactedString;
        $allSql = "SELECT COUNT(distinct(profiles.participantid)) AS count FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereAllString;;

        $transactedResult = $this->connection->executeQuery($transactedTimeSql)->fetch()['count'];
        $allResult = $this->connection->executeQuery($allSql)->fetch()['count'];
        $result = 0;

        if ($allResult != 0){
            $result = ($transactedResult / $allResult) * 100;
        }

        return round($result, 2);
    }


    public function getPlanBasicsCount($account, $plan, $start, $end, $acctLevel = null)
    {
        return $this->getVidStats($account, $plan, $start, $end, $acctLevel, 1);
    }

    public function getRetirementNeedsCount($account, $plan, $start, $end, $acctLevel = null)
    {
        return $this->getVidStats($account, $plan, $start, $end, $acctLevel, 2);
    }

    public function getRTQCount($account, $plan, $start, $end, $acctLevel = null)
    {
        return $this->getVidStats($account, $plan, $start, $end, $acctLevel, 3);
    }

    public function getPlanInvestmentsCount($account, $plan, $start, $end, $acctLevel = null)
    {
        return $this->getVidStats($account, $plan, $start, $end, $acctLevel, 4);
    }

    public function getPlanContributionsCount($account, $plan, $start, $end, $acctLevel = null)
    {
        return $this->getVidStats($account, $plan, $start, $end, $acctLevel, 5);
    }

    public function getVidStats($account, $plan, $start, $end, $acctLevel = null, $vidType)
    {
        //plan basics = 1
        //retirementneeds = 2
        //riskprofile = 3
        //investments = 4
        //contributions = 5
        $tableName = "profiles";
        $whereArray = [];
        $leftJoinArray = [];

        if($acctLevel == "advisor") {
            $whereArray[] = $this->createAdvisorSql($account);
        }
        //base parameters for all queries
        array_push($whereArray,
            $this->accountQuery($account, $tableName, $acctLevel),
            $this->planQuery($plan, $tableName),
            "profiles.reportDate >= '" . $start . "'",
            "profiles.reportDate <= '" . $end . "'");

        array_push($leftJoinArray,
            "profilesContributions on profiles.id = profilesContributions.profileid",
            "plans on profiles.planid = plans.id");

        if($vidType == 1) {
            array_push($whereArray,
                "videoStatsSpe.id is not null",
                "videoStatsSpe.videoPlayed LIKE '02%'");

            array_push($leftJoinArray,
                "videoStatsSpe on profiles.participantid = videoStatsSpe.participantid");
        } elseif($vidType == 2) {
            array_push($whereArray,
                "profilesRetirementNeeds.yearsLiveInRetirement is not null");

            array_push($leftJoinArray,
                "profilesRetirementNeeds on profiles.id = profilesRetirementNeeds.profileid");
        } elseif($vidType == 3) {
            array_push($whereArray,
                "profilesRiskProfile.name in ('Aggressive','Conservative','Conservative Plus','Moderate','Moderate Plus','Moderately Aggressive','Moderately Conservative')");

            array_push($leftJoinArray,
                "profilesRiskProfile on profiles.id = profilesRiskProfile.profileid");
        } elseif($vidType == 4) {
            array_push($whereArray,
                "profilesInvestments.id is not null");

            array_push($leftJoinArray,
                "profilesInvestments on profiles.id = profilesInvestments.profileid");
        } elseif($vidType == 5) {
            array_push($whereArray,
                "profilesContributions.id is not null");
        }


        $whereString = $this->createWhereSql($whereArray);
        $leftJoinString = $this->createLeftJoinSql($leftJoinArray);
        $sql = "SELECT COUNT(distinct(profiles.participantid)) AS count FROM " . $tableName . " ". $leftJoinString . " WHERE " . $whereString;
        return number_format($this->connection->executeQuery($sql)->fetch()['count']);
    }

    public function planQuery($plan, $tableName)
    {
        $planString = "";
        if($plan !== "") {
            if($tableName == 'profiles' || $tableName == 'participants' || $tableName == 'profilesContributions') {
                $planString = "profiles.planid = ". $plan;// Profiles
            } else if ($tableName == 'plans') {
                $planString = "plans.id = " . $plan;
            }
        }
        return $planString;
    }

    public function accountQuery($account, $tableName, $acctLevel)
    {
        if ($acctLevel == "advisor")
            return "";
        $accountString = "";
        $accountsInnerSql = "select id FROM accounts accountsInner WHERE  accountsInner.reportable = 0";
            if($tableName == 'profiles' || $tableName == 'participants' || $tableName == 'profilesContributions') {
                if($account !== "")
                {
                    $accountString = "profiles.userid = ". $account;// Profiles                    
                }
                else
                {
                    $accountString =  "profiles.userid NOT IN (".$accountsInnerSql.")";
                }
            } else if ($tableName == 'plans') {
                if($account !== "")
                {
                    $accountString = "plans.userid = ". $account;
                }
                else
                {
                    $accountString =  "plans.userid NOT IN (".$accountsInnerSql.")";
                }
            }
        return $accountString;
    }

    public function createWhereSql($whereArray, $pushExtras = true)
    {
        if ($pushExtras) {
            array_push($whereArray, "plans.deleted = 0", "plans.testing = 0");//SO FAR need for getPlansCount, getUniqueUsersCount..
            $whereArray[] = "(profilesRetirementNeeds.currentYearlyIncome  <  plansModules.maximumAnnualIncome OR plansModules.maximumAnnualIncome is null OR plansModules.maximumAnnualIncome = 0)";
            $whereArray[] = "(profilesContributions.currentYearlyIncome  <  plansModules.maximumAnnualIncome OR plansModules.maximumAnnualIncome is null OR plansModules.maximumAnnualIncome = 0)";
        }       

        return $this->arrayToSql($whereArray, "AND");

    }

    public function createAdvisorSql($account)
    {
        $sql =  "SELECT accountsUsersPlans.planid as planid FROM accountsUsersPlans WHERE accountsUsersPlans.accountsUsersId = '$account'";//take sql out of here...i think
        $result = $this->connection->executeQuery($sql)->fetchAll();
        $planIdArray = [];
        foreach($result as $plan) {
            $planIdArray[] = $plan['planid'];
        }
        $advisorSql = $this->arrayToSql($planIdArray, ",");
        $advisorSql = " plans.id IN (".$advisorSql.")";
        return $advisorSql;

    }

    /**
     * @param $array
     * @param $delim
     * @return string
     */
    private function arrayToSql($array, $delim)
    {
        $partSqlString = "";
        $first = true;
        foreach ($array as $sqlClause) {
            if ($sqlClause) {
                if ($first == true) {
                    $partSqlString .= $sqlClause;
                    $first = false;
                } else {
                    $partSqlString .= " ".$delim." " . $sqlClause;
                }
            }
        }
        return $partSqlString;
    }
    public function checkIfExist($table, $array)
    {
        foreach($array as $index => $string) 
        {
            if (strpos($string, $table.".") !== FALSE)
            {
                return true;
            }
        }        
        return false;
    }
    public function createLeftJoinSql($leftJoinArray)
    {
        if (!$this->checkIfExist("profilesRetirementNeeds", $leftJoinArray))
        {
            $leftJoinArray[]  = "profilesRetirementNeeds on profilesRetirementNeeds.profileid = profiles.id";
        }
        if (!$this->checkIfExist("plansModules", $leftJoinArray))
        {
            $leftJoinArray[]  = "plansModules on plansModules.planid = plans.id";           
        }
        $leftJoinSql = $this->arrayToSql($leftJoinArray, "LEFT JOIN");
        return "LEFT JOIN " . $leftJoinSql;

    }
    public function vWiseReport($params)
    {
        error_reporting(0);
        $planName = "All";
        $acctLevel    = $params['level'];
        $prov = $params['prov'];
        $account = $params['acct'];
        $plan = $params['plan'];
        $var2 = $params['var2'];
        if ($plan != "" && $acctLevel == "advisor")
        {
            $acctLevel = "vWise";
            $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:accountsUsers');
            $user = $repository->findOneBy(array('id' => $account));
            $account = $user->userid;
        }
        //make sure date is formated for google analytics
        $start = date_format(date_create($params['start']), 'Y-m-d');
        $end = date_format(date_create($params['end']), 'Y-m-d');

        if($prov === ' -Choose' || $prov === ' -All'){
            $provider = 'All';
        }else{
            $provider = $prov;
        }


        if($account !== ''){
            $account1 = "profiles.userid = '$account' AND ";// Profiles
            $account2 = "userid = '$account' and ";
        }else{
            $account1 = '';
            $account2 = '';
        }

        if($plan !== ''){
            $plan1 = "profiles.planid = '$plan' AND ";// Profiles
            $plan2 = "id = '$plan' AND ";

            $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:plans');
            $planResult = $repository->findOneBy(array('id' => $plan));
            if($planResult != null)
            {
                $planName = $planResult->planid."-".$planResult->name;
                if ($planResult->sponsorLogoImage != "")
                {
                    $logo_url = $planResult->sponsorLogoImage;
                }               
            }

        }else{
            $plan1 = '';
            $plan2 = '';
        }

        if($var2 ==='' || !$var2 ){
            $var2 = '';
        }


        $plans = $this->getPlansCount($account, $plan, $acctLevel, $start, $end);
        $avgEnter   = 0;
        $avgExit    = 0;
        $saved      = $this->getUniqueUsersCount($account, $plan, $start, $end, $acctLevel);
        $new        = $this->getNewEnrolleesCount($account, $plan, $start, $end, $acctLevel);
        $avg        = $this->getNewEnrolleesAvgDef($account, $plan, $start, $end, $acctLevel);
        $risk       = $this->getInvestmentTypes($account, $plan, $start, $end, $acctLevel);
        $ota        = $this->getTotalOutsideAssets($account, $plan, $start, $end, $acctLevel);
        $iras       = $this->getIraUsersCount($account, $plan, $start, $end, $acctLevel);//$this->iraUsers($account, $plan, $fix, $start, $end,$acctLevel);
        $former     = $this->getFormerRetirementUsersCount($account, $plan, $start, $end, $acctLevel);//$this->formerRet($account, $plan, $fix, $start, $end,$acctLevel);
        $avgBal     = $this->getAvgAccountBalance($account, $plan, $start, $end, $acctLevel);
        $avgSal     = $this->getAvgSal($account, $plan, $start, $end, $acctLevel);
        $avgEnter   = $this->getAvgEnteringDefRate($account, $plan, $start, $end, $acctLevel);
        $avgExit    = $this->getAvgExitingDefRate($account, $plan, $start, $end, $acctLevel);
        $ztoe       = $this->getTransacted($start, $end, '00:00:00', '08:00:00', $account, $plan, $acctLevel);
        $etof       = $this->getTransacted($start, $end, '08:00:00', '16:00:00', $account, $plan, $acctLevel);
        $ftot       = $this->getTransacted($start, $end, '16:00:00', '23:59:59', $account, $plan, $acctLevel);
        $planBasics = $this->getPlanBasicsCount($account, $plan, $start, $end,$acctLevel);
        $planRN     = $this->getRetirementNeedsCount($account, $plan, $start, $end,$acctLevel);
        $planRTQ    = $this->getRTQCount($account, $plan, $start, $end,$acctLevel);
        $planInv    = $this->getPlanInvestmentsCount($account, $plan, $start, $end,$acctLevel);
        $planCont   = $this->getPlanContributionsCount($account, $plan, $start, $end,$acctLevel);
        $plansWithActivity = $this->getPlansWithActivity($account, $plan, $start, $end, $acctLevel);

        //$ids = $this->getIds();
        $avgDelta = 0;

        $indicator = "";

        if($saved != 0) {
            $iraPercent = round(($iras/$saved)*100, 2);
            $formerPercent = round(($former/$saved)*100, 2);
        } else {
            $iraPercent = 0;
            $formerPercent = 0;
        }



        if($avgEnter > $avgExit && $avgExit != 0){
            $avgDelta  = (($avgEnter - $avgExit) / $avgExit) * 100;
            $indicator = '-';
        }else if ($avgEnter != 0){
            $avgDelta  = (($avgExit - $avgEnter) / $avgEnter) * 100;
            $indicator = '+';
        }

        //Google stuff

        $browser = $this->browsers($start, $end, $var2);
        $device  = $this->mobileDevice($start, $end, $var2);
        $topcity = $this->cities($start, $end, $var2);

        $views = $this->views($start, $end, $var2);
        //$qtest = $this->qtest($start, $end);
        $checkRiskIndexes = ["ADVICE","CUSTOM","DEFAULT","RISK","TARGET"];
        $data = array(
            "provider_name" => $provider,
            "num_plans" => $plans,
            "unique_users_smartplan" => number_format($saved),
            "new_enrollees" => $new,
            "avg_deferral_new_enrollees" => $avg,
            "avg_salary" => $avgSal,
            "entering_deferral_rate" => round($avgEnter, 2),
            "exiting_deferral_rate" => round($avgExit, 2),
            "delta_entering_vs_exiting" => $indicator.round($avgDelta, 2),
            "avg_account" => $avgBal,
            "advice" => $risk["ADVICE"],
            "custom" => $risk["CUSTOM"],
            "default" => $risk["DEFAULT"],
            "risk" => $risk["RISK"],
            "target" => $risk["TARGET"],
            "total_outside_assets" => $ota,
            "users_ira_percent" => $iraPercent,
            "users_ira" => number_format($iras),
            "users_former_plan_percent" => $formerPercent,
            "users_former_plan" => number_format($former),
            "top_browser" => $browser,
            "top_mobile_device" => $device,
            "transacted_12_8" => $ztoe,
            "transacted_8_4" => $etof,
            "transacted_4_1159" => $ftot,
            "top25_cities" => $topcity,
            "visit_total" => $views,
            "plan_basics" => $planBasics,
            "retirement_needs" => $planRN,
            "risk_profile" => $planRTQ,
            "investments" => $planInv,
            "contributions" => $planCont,
            "plan_name" => $planName,
            "start_date" => $start,
            "end_date" => $end,
            "plans_with_activity" => $plansWithActivity,
            "baseurl" => $this->container->getParameter("pdfExportUrl")."reports/"
        );
        $data['investmentTypes'] =   'Advice: '. $data["advice"] . ' Custom: ' . $data["custom"] . ' Default: ' . $data["default"] . ' Risk: ' . $data["risk"] . ' Target: ' . $data["target"];

        if(isset($logo_url))
        {
            $data['logo_url'] = $logo_url;
        }
        return $data;        
    }
    
    private function queryDataAction($x){
        
        $ga = new GoogleAnalyticsAPI('service');
        $ga->auth->setClientId('891041475744-ttakqc55rr0d0k2so2likptb554bfck7.apps.googleusercontent.com');
        $ga->auth->setEmail('891041475744-ttakqc55rr0d0k2so2likptb554bfck7@developer.gserviceaccount.com');
        $ga->auth->setPrivateKey($this->container->get('kernel')->getRootDir().'/../src/Plans/PlansBundle/Controller/ga/spe_analytics.p12');

        $auth = $ga->auth->getAccessToken();

        // Try to get the AccessToken
        if ($auth['http_code'] == 200) {
                $accessToken  = $auth['access_token'];
                $tokenExpires = $auth['expires_in'];
                $tokenCreated = time();
        } else {
                echo "Session Expired.";
        }
        
        // Set the accessToken and Account-Id
        $ga->setAccessToken($accessToken);
        $ga->setAccountId('ga:68222810');
        
        return $ga->query($x);
    }
    function googleQueries($metric, $filter, $start, $end, $sort, $var2){

        if($var2 !== ''){
            
        $googleData = array(
                            'metrics'    => $metric,
                            'dimensions' => $filter,
                            'start-date' => $start,
                            'end-date'   => $end,
                            'sort'       => $sort,
                            'filters'    => 'ga:customVarValue2=@'.$var2
                            );

        }else{
            
        $googleData = array(
                            'metrics'    => $metric,
                            'dimensions' => $filter,
                            'start-date' => $start,
                            'end-date'   => $end,
                            'sort'       => $sort,
                            );
        
        }

        //gapps query
        $result = $this->queryDataAction($googleData);          
        return $result;
    }    

    function browsers($start, $end, $var2){
        
        $data = $this->googleQueries('ga:pageviews', 'ga:browser', $start, $end, 'ga:browser', $var2);
       
        if (array_key_exists('rows', $data)) {
            
            $top = 0;
           
            for($i = 0; $i < count($data['rows']); $i++){
                
                if($data['rows'][$i][1] > $top){
                    
                    $top = $data['rows'][$i][1];
                    $browser = $data['rows'][$i][0];
                    
                }
            }
            
            
            $result = $browser;
        }else{
            $result = "Insufficient Data";
        } 
            
        return $result;
        
    }
     function views($start, $end, $var2){
        
        $data = $this->googleQueries('ga:pageviews, ga:uniquePageViews', '', $start, $end, '-ga:pageviews', $var2);
        
        $total = $data['totalsForAllResults']['ga:pageviews'];

        if($total != 0){
        $unique    = (($data['totalsForAllResults']['ga:pageviews'] - $data['totalsForAllResults']['ga:uniquePageViews']) / $data['totalsForAllResults']['ga:pageviews']) * 100;
        $returning = 100 - round($unique, 2);

        return number_format($total).' <b>Unique:</b> '.round($unique, 2).'% <b>Returning:</b> '.round($returning, 2).'%';
        }else{
            return "Insufficient Data";
        }
    }
    
    function mobileDevice($start, $end, $var2){
        
        $data   = $this->googleQueries('ga:pageviews', 'ga:mobileDeviceInfo', $start, $end, '-ga:pageviews', $var2);
        
        if (array_key_exists('rows', $data)) {
            
            $top = 0;
       
            for($i = 0; $i < count($data['rows']); $i++){
                
                if($data['rows'][$i][1] > $top){
                    
                    $top     = $data['rows'][$i][1];
                    $mobile = $data['rows'][$i][0];
                    
                }
            }

            $result = $mobile;
            
        }else{
            $result = "Insufficient Data";
        }        
            
        return $result;
    } 
    
    function cities($start, $end, $var2){
        
        $data   = $this->googleQueries('ga:pageviews', 'ga:city', $start, $end, '-ga:pageviews', $var2);
    
        if (array_key_exists('rows', $data)) {
        $result = $data['rows'];

        $top    = array_slice($result, 0, 25);

        foreach($top as $place){
            $topc[] = $place[0];
        }

        $markup = implode(' ',$topc);
        }else{
            $markup = "Insufficient Data";
        }
        return $markup;
            
    }  
    public function reportObject($data)
    {
        $descriptions = 
        [
            "provider_name" => "Provider Name",
            "num_plans" => "Total number of plans in date range",
            "plans_with_activity" => "Plans with activity in date range",
            "unique_users_smartplan" => "Total number of unique users that completed SmartPlan",
            "new_enrollees" => "Total number of new enrollees",
            "avg_deferral_new_enrollees" => "Average deferral new enrollees",
            "avg_salary" => "Average salary",
            "entering_deferral_rate" => "Existing participant average entering deferral rate",
            "exiting_deferral_rate" => "Existing participant average exiting deferral rate",
            "delta_entering_vs_exiting" => "Average Delta Entering vs Exiting SmartPlan",
            "avg_account" => "Average account balance",
            "investmentTypes" => "Percentage of participants choosing each investment type",
            "total_outside_assets" => "Total outside assets",
            "users_ira" => "Number of users with IRA's",
            "users_former_plan" => "Number of users with former retirement plan",
            "top_browser" => "Top browser",
            "top_mobile_device" => "Top mobile device",
            "transacted_12_8" => "Percentage of users who transacted between 12 - 8 am",
            "transacted_8_4" => "Percentage of users who transacted between 8 am - 4 pm",
            "transacted_4_1159" => "Percentage of users who transacted between 4 pm - 11:59 pm",
            "top25_cities" => "Top 25 cities for unique visits",
            "visit_total" => "Visit totals",
            "plan_basics" => "Viewed Plan Basics",
            "retirement_needs" => "Completed RN",
            "risk_profile" => "Completed RTQ",
            "investments" => "Completed Investments",
            "contributions" => "Completed Contributions"
            
        ];
        $report = array();
        foreach ($data as $key =>$value)
        {
            $row = array();
            $row['value'] = $value;
            $row['description'] = $descriptions[$key];
            if ($row['description'] != null)
            {
                $report[$key] = $row;
            }
        }
        return $report;
    }
    public function reportHtml($data)
    {
        $report = $this->reportObject($data);
        $html = '<table id="perf-data" class="table table-striped">';
        foreach ($report as $row)
        {           
            $html .= "<tr>".
                     "<td>".$row['description']."</td>".
                     "<td>".$row['value']."</td>".
                     "</tr>";
        }
        $html .= "</table>";
        return $html;
    }
    public function reportCsv($data)
    {
        $csv = array();
        $report = $this->reportObject($data);
        foreach ($report as $row)
        {
            $csvrow = array();
            $csvrow[] = $row['description'];
            $csvrow[] = strip_tags($row['value']);
            $csv[] = $csvrow;
        }
        $filename = "temp.csv";
        
        $fp = fopen($filename, 'w');

        foreach ($csv as $row) 
        {
            fputcsv($fp, $row);
        }
        fclose($fp);
        $csvContents = file_get_contents($filename);
        unlink($filename);
        return $csvContents;
    }
    public function reportPdf($data)
    {        
        $data['isPdf'] = true;
        $html = $this->container->get("templating")->render('SettingsSettingsBundle:Performance:viewreport.html.twig', $data);
        $this->container->get('knp_snappy.pdf')->setOption("viewport-size","1920x1080");
        $fileContents = $this->container->get('knp_snappy.pdf')->getOutputFromHtml($html);
        return $fileContents;
    }
}
