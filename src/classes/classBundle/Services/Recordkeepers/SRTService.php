<?php
namespace classes\classBundle\Services\Recordkeepers;
use classes\classBundle\Services\BaseService;
use classes\classBundle\Entity\accounts;
class SRTService extends BaseService
{
    public function sid()
    {
        $session = $this->session;
        $userid = $session->get("userid");

        $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:accounts');
        $currentmember = $repository->findOneBy(array('id' => $userid));

        $dbuser = urldecode($currentmember->recordkeeperUser);
        $dbpass = urldecode($currentmember->recordkeeperPass);
        

        // get a session id
        $recordkeeperUrl = $this->getRecordkeeperUrl();
        $url = "https://$recordkeeperUrl/SRTDB_tk.asmx/AuthenticateUser?userName=$dbuser&password=$dbpass";

        $sidXml = file_get_contents($url);
        $sid = new \SimpleXMLElement($sidXml);
        return $sid;
    }    
    public function getQueryId($type) {
        $session = $this->session;
        $userid = $session->get("userid");
        $em = $this->em->getManager();
        
        $query = $em->createQuery("
            SELECT 
                COALESCE(a.queryid, sc.queryid) AS queryid
            FROM
                classesclassBundle:serverConfigRecordkeepersQueryIds sc
                LEFT JOIN classesclassBundle:accountsRecordkeepersQueryIds a WITH a.type = sc.type AND a.userid=:userid
            WHERE
                sc.type=:type
        ")->setParameters(array('userid'=>$userid, 'type'=>$type));
        $queryid = $query->getSingleScalarResult();
        return $queryid;
    }
    public function getRecordkeeperUrl() 
    {
        $session = $this->session;
        $userid = $session->get("userid");
        $sql = "select connectionType,recordkeeperUrl,recordkeeperVersion FROM accounts WHERE id = ".$userid;
        $account = $this->connection->executeQuery($sql)->fetch();
        if ((int) $account['recordkeeperVersion'] !== accounts::RECORDKEEPER_VERSION_SRT_2_MIGRATION)
        {
            $account['recordkeeperUrl'] = $account['recordkeeperUrl']."/sddav2";
        }
        return $account['recordkeeperUrl'];
    }
    public function advicedataXML($planid)
    {
        $sid = $this->sid();
        //$Query = "15591cea-e31e-4ebe-9126-1157f7a5240c";
        $queryId = $this->getQueryId("adviceQuery");
        $parameters = "'$planid','$planid'";
        $recordkeeperUrl = $this->getRecordkeeperUrl();
        $url = "https://$recordkeeperUrl/SRTDB_tk.asmx/ExecuteQueryWDDX?sessionID=$sid&sqlID=$queryId&parameters=$parameters";
        $rawXml = file_get_contents($url);
        return $rawXml;
    }  
    public function advicelist($planid,$userid = null)
    {
        $session = $this->session;
        if (!empty($userid))
        {
            $session->set("userid",$userid);
        }
        else
        {
            $userid = $session->get("userid");
        }
        $plan = $this->em->getRepository($this->classPath."plans")->findOneBy(["userid" => $userid,"planid" => $planid,"deleted" => 0]);
        $portfolioXML = $this->advicedataXML($plan->activePlanid);
        $wddx = new \SimpleXMLElement($portfolioXML);
        $wddx = utf8_encode($wddx);
        $data = wddx_deserialize($wddx);
        foreach ($data as $var)
        {
            foreach ($var as $name => $field)
            {
                if ($name == "souf_heading_str")
                {
                    $indx = 0;
                    foreach ($field as $value)
                    {

                        $adviceList[$indx]->name = (string)$value;
                        $indx++;
                    }
                }

                if ($name == "inv_ticker_cid")
                {
                    $indx = 0;
                    foreach ($field as $value)
                    {

                        $adviceList[$indx]->ticker = $value;
                        $indx++;
                    }
                }
                for ($i = 0; $i < $indx; $i++)
                {
                    $adviceList[$i]->cusip = "";
                }
            }            
        }



        return $adviceList;
    }
}


