<?php
namespace classes\classBundle\Services;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseService
{
    var $container;
    var $em;
    var $connection;
    var $session;
    
    public function __construct(containerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get("doctrine");
        $this->connection = $this->container->get("doctrine.dbal.default_connection");
        $this->session = $this->container->get('session');
        $this->classPath = "classesclassBundle:";
    }
    protected function copyParamsToEntity(&$entity,$params)
    {
        foreach ($params as $key => $value)
            $entity->$key = $value;
    } 
    protected function findOneBy($table,$findOneBy)
    {
        return $this->em->getRepository($this->classPath.$table)->findOneBy($findOneBy);
    }
    protected function findBy($table,$findOneBy,$order = [])
    {       
        return $this->em->getRepository($this->classPath.$table)->findBy($findOneBy,$order);        
    }
}

