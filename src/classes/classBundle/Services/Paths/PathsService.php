<?php
namespace classes\classBundle\Services\Paths;
use classes\classBundle\Entity\AppStep;
use classes\classBundle\Entity\AppStepContent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Entity\AppSectionList;
use classes\classBundle\Entity\AppSection;
use classes\classBundle\Entity\Path;
use classes\classBundle\Entity\PathTypes;
use classes\classBundle\Entity\PathAccount;
use classes\classBundle\Services\BaseService;
use classes\classBundle\Entity\AccountRiskProfileQuestionnaire;
use classes\classBundle\Entity\AppContentType;
use classes\classBundle\Entity\DefaultContent;
use classes\classBundle\Entity\PathContent;
class PathsService extends BaseService
{
    private $id;
    private $pathId;
    private $userid;//optional
    private $planid;//optional
    private $pathContentHashed;
    protected $lang = "en";
    public function addPath($id,$params)
    {
        $path = new path();
        $this->copyParamsToEntity($path,$params);
        $this->setId($id);
        $path->createdBy = $this->id;
        $path->updatedBy = $this->id;
        $this->em->getManager()->persist($path);
        $this->em->getManager()->flush();
        return $path;
    }
    public function setLang($lang)
    {
        $this->lang = $lang;
    }
    public function setId($id)
    {
        $user = $this->findOneBy("accountsUsers",["id" => $id]);
        if (empty($user))
            throw new \Exception("User does not exist");
        $this->id = $id;
    }
    public function updatePathLog($path)
    {
        if (empty($this->id))
            throw new \Exception("User not assigned to path");
        $path->updatedDate = new \Datetime("now");
        $path->updatedBy = $this->id;
        $this->em->getManager()->flush();        
    }
    public function updatePathLogById($pathid)
    {
        $path = $this->findOneBy("Path",["id" => $pathid]);
        if (empty($path))
            throw new \Exception("Path does not exist");
        $this->updatePathLog($path);
    }
    public function getPath($findOneBy)
    {
         return $this->findOneBy("Path",$findOneBy);
    }
    public function getPathFull($findOneBy, $sectionFindBy = [])
    {
         $path = $this->getPath($findOneBy);
         $sectionFindBy = array_merge(["pathId" => $path->id], $sectionFindBy);
         $path->sections = $this->getSectionsFull($sectionFindBy);
         $path->type = $this->getPathType(["id" => $path->pathTypeId]);
         $this->container->get("PlayListService")->setLang($this->lang);
         $path->playLists = $this->container->get("PlayListService")->getPlayListsFull(["pathId" => $path->id ]);
         return $path;
    }
    public function getPathFullApi($findOneBy, $sectionFindBy = [])
    {
        $fullPath = $this->getPathFull($findOneBy,$sectionFindBy);
        foreach ($fullPath->sections as $key => $section)
        {
            $defaultContent = $this->getDefaultContent(["sectionListId" => $section->section->id,"step" => "navigation","label" => "title" ]);
            if (!empty($defaultContent))
            {
                $content = $this->getContent($defaultContent->id,$fullPath->id);
                if (!empty($content))
                {
                    $fullPath->sections[$key]->section->description = $content->englishContent;
                    if ($this->lang == "es")
                        $fullPath->sections[$key]->section->description = $content->spanishContent;
                }
            }
        }
        return $fullPath;
    }
    public function getPathsMulti($findBy)
    {
        return $this->findBy("Path",$findBy);
    }
    public function editPath($findOneBy,$params)
    {
        $path = $this->getPath($findOneBy); 
        $this->copyParamsToEntity($path,$params);
        $this->em->getManager()->flush();   
        $this->updatePathLog($path);
    }
    public function deletePath($findOneBy)
    {
        $path = $this->getPath($findOneBy);     
        $this->deleteSections(["pathId" => $path->id]);
        $playListService = $this->container->get("PlayListService");
        $playListService->setId($this->id);
        $playListService->deletePlayLists(["pathId" => $path->id]);
        $this->deletePathContents(["pathId" => $path->id]);
        $this->em->getManager()->remove($path);
        $this->em->getManager()->flush();  
    }
    public function addToSectionList($params)
    {
        $item = new AppSectionList();
        $this->copyParamsToEntity($item,$params);        
        $this->em->getManager()->persist($item);
        $this->em->getManager()->flush();
    }
    public function getSectionListItem($params)
    {
        return $this->findOneBy("AppSectionList",$params);   
    }
    public function editSectionListItem($findByParams,$updateParams)
    {
        $item = $this->getSectionListItem($findByParams);
        $this->copyParamsToEntity($item,$updateParams);                
        $this->em->getManager()->flush();
    }
    public function deleteSectionListItem($findByParams)
    {
        $item = $this->getSectionListItem($findByParams);
        $this->deleteSections(["appSectionListId" => $item->id]);
        $this->em->getManager()->remove($item);        
        $this->em->getManager()->flush();
    }
    public function getSectionList()
    {
        return $this->findBy("AppSectionList", []);
    }
    public function getSectionListHash($field)
    {
        $sectionList =  $this->getSectionList();
        $hash = [];
        foreach ($sectionList as $item)
        {
            $hash[trim($item->{$field})] = $item;
        }
        return $hash;
    }
    public function getSectionListHashBy($field)
    {
        $sectionList = $this->getSectionList();
        $sectionListHash = [];
        foreach ($sectionList as $section)
        $sectionListHash[$section->$field] = $section;
        return $sectionListHash;
    }
    public function getSectionListRemaining($pathid)
    {
        $sections = $this->getSectionListHashBy("id");
        $sectionsPath = $this->getPathFull(["id" => $pathid])->sections;
        foreach ($sectionsPath as $section)
            unset($sections[$section->section->id]); 
        return $sections;        
    }
    public function addSection($params)
    {
        $section = new AppSection();        
        $appSectionListId = $params['appSectionListId'];        
        $stepListSteps = $this->findBy("AppStepList",["appSectionListId" => $appSectionListId]);  
        if (isset($params['appSteps']))
        {
            foreach ($params['appSteps'] as $step)
            {
                $stepListStep = $this->findOneBy("AppStepList",["id" => $step->appStepListId]);
                $appStep = new AppStep();
                $appStep->section = $section;
                $appStep->setAppStepList($stepListStep);
                foreach ($step->contents as $content)
                {
                    $appContent = new AppStepContent();
                    $appContent->name = $content->name;
                    $appContent->type = $content->type;
                    $appContent->englishContent = $content->englishContent;
                    $appContent->spanishContent = $content->spanishContent;
                    $appContent->step = $appStep;
                    $appStep->contents->add($appContent);
                }
                $section->steps->add($appStep);
            }
        }
        else
        {
            foreach ($stepListSteps as $stepListStep) {
                $appStep = new AppStep();
                $appStep->section = $section;
                $appStep->setAppStepList($stepListStep);
                $section->steps->add($appStep);
            }
        }
        $this->copyParamsToEntity($section,$params);  
        if (!isset($params['sort']))
        $section->sort = time();
        if ($this->validateSection($section))
        {
            $this->em->getManager()->persist($section);
            $this->em->getManager()->flush();     
            $this->updatePathLogById($section->pathId);
            return $section;
        }
        return false;
    } 
    public function getSection($findOneBy)
    {
        return $this->findOneBy("AppSection",$findOneBy);   
    }
    public function getSectionFull($findOneBy)
    {
        $section = $this->getSection($findOneBy);
        return $this->getSectionFullEntity($section);
    }
    public function getPathContentHashed($findBy)
    {
        if (empty($this->pathContentHashed))
        {
            $pathContent = $this->getPathContents($findBy);
            $this->pathContentHashed = [];
            foreach ($pathContent as &$content)           
                $this->pathContentHashed[$content->defaultContentId] = $content;            
        }
        return $this->pathContentHashed;
    }
    private function getStepFullEntity($section)
    {
        $stepData = [];
        $pathContent = $this->getPathContentHashed(["pathId" => $section->pathId ]);
        foreach ($section->steps as $step)
        {
            $contents = [];
            $replaceRules = [];
            foreach ($step->step->contents as $content)
            {
                $contents[$content->label] = $content;
                if (isset($pathContent[$content->id]))
                {
                    $pathContentRow = $pathContent[$content->id];
                    if (!empty($pathContentRow->englishContent))
                    {
                        $contents[$content->label]->englishContent = $pathContentRow->englishContent;
                        if ($this->lang == "en")
                            $contents[$content->label]->content = $pathContentRow->englishContent;
                    }
                    if (!empty($pathContentRow->spanishContent))
                    {
                        $contents[$content->label]->spanishContent = $pathContentRow->spanishContent;
                        if ($this->lang == "es")
                            $contents[$content->label]->content = $pathContentRow->spanishContent;
                    }
                    if (!$pathContentRow->enable)
                    {
                        if (empty($pathContentRow->getRules()))
                        {
                            unset($contents[$content->label]);
                        }
                        else
                        {
                            $replaceRules[$content->label] = $pathContentRow->getRules();
                        }
                    }
                }
            }
            $stepContents = [];
            foreach ($step->contents as $stepContent)
            {
                $content = $stepContent->englishContent;
                if ($this->lang == "es" && !empty($stepContent->spanishContent))
                {
                    $content = $stepContent->spanishContent;
                }
                $stepContents[] = [
                    'name' => $stepContent->name,
                    'englishContent' => $stepContent->englishContent,
                    'spanishContent' => $stepContent->spanishContent,
                    'content' => $content
                ];
            }

            if ($step->active || !empty($replaceRules)) {
                $stepData[$step->step->name] = [
                    'name' => $step->step->name,
                    'path' => $step->step->path,
                    'active' => $step->active,
                    'contents' => $contents,
                    'replaceRules' => $replaceRules,
                    'id' => $step->id,
                    'stepContents' => $stepContents
                ];
            }
        }
        return $stepData;
    }
    public function getSectionFullEntity($section)
    {
        $section->section = $this->getSectionListItem(["id" => $section->appSectionListId]);
        $section->contents = $this->getDefaultContents(["sectionListId" => $section->appSectionListId]);
        $section->name = $section->section->section;
        $pathContent = $this->getPathContentHashed(["pathId" => $section->pathId ]);
        foreach ($section->contents as $keySection => $section2)
        {
            foreach ($section2 as $keyContent => $content)
            {
                if (isset($pathContent[$content->id]))
                {
                    $pathContentRow = $pathContent[$content->id];
                    if (!empty($pathContentRow->englishContent))
                    {
                        $section->contents[$keySection][$keyContent]->englishContent = $pathContentRow->englishContent;
                        if ($this->lang == "en")
                            $section->contents[$keySection][$keyContent]->content = $pathContentRow->englishContent;
                    }
                    if (!empty($pathContentRow->spanishContent))
                    {
                        $section->contents[$keySection][$keyContent]->spanishContent =$pathContentRow->spanishContent;
                        if ($this->lang == "es")
                            $section->contents[$keySection][$keyContent]->content = $pathContentRow->spanishContent;
                    }
                    if (!$pathContentRow->enable)
                        unset($section->contents[$keySection][$keyContent]);
                }
            }
        }
        return $section;        
    }
    public function getSections($findBy)
    {
        return $this->findBy("AppSection",$findBy,["sort" => "asc"]);   
    }
    public function getSectionsFull($findBy)
    {
        $sections = $this->getSections($findBy);  
        $sectionsHash = [];
        foreach ($sections as $section)      
        {
            $section = $this->getSectionFullEntity($section);
            $section->steps = $this->getStepFullEntity($section);
            $sectionsHash[$section->section->section] = $section;
        }
        return $sectionsHash;
    }
    public function editSection($findOneBy,$params)
    {
        $section = $this->getSection($findOneBy);
        $this->copyParamsToEntity($section,$params);  
        if ($this->validateSection($section))
        {            
            $this->em->getManager()->flush();
            $this->updatePathLogById($section->pathId);
            return true;
        }
        return false;
    }
    public function deleteSection($findOneBy)
    {
        $section = $this->getSection($findOneBy);
        $this->deleteSectionByEntity($section);
    }
    public function deleteSectionByEntity($section)
    {
        $appSectionListItem = $this->getSectionListItem(["id" => $section->appSectionListId]);
        if ($appSectionListItem->removable)
        {
            $this->em->getManager()->remove($section);
            $this->em->getManager()->flush();
            $this->updatePathLogById($section->pathId);
        }        
    }
    public function deleteSections($findBy)
    {
        $sections = $this->getSections($findBy);
        foreach ($sections as $section)
            $this->deleteSectionByEntity($section);
    }
    public function validateSection($section)
    {
        try
        {
            return 
                !empty($this->getPath(["id" => $section->pathId])) 
                    && 
                !empty($this->getSectionListItem(["id" => $section->appSectionListId]));
        } 
        catch (\Exception $ex) 
        {
            return false;
        }
    }
    public function reorderSections($appSectionListIds,$findByParams)
    {
        $appSections = $this->em->getRepository($this->classPath."AppSection")->findBy($findByParams);
        $appSectionsHash = [];
        foreach ($appSections as $section)
        {
            $appSectionsHash[$section->appSectionListId] = $section;
        }
        $sort = 0;
        foreach ($appSectionListIds as $id)
        {
            $appSection = $appSectionsHash[$id];
            $appSection->sort = $sort++;
        }
        $this->em->getManager()->flush();
        $this->updatePathLogById($appSection->pathId);
    }
    public function reorderSectionsBy($field,$order,$findByParams)
    {
        $appSectionList =  $this->em->getRepository($this->classPath."AppSectionList")->findBy([],[$field => $order]);
        $ids = [];
        foreach ($appSectionList as $section)
        {
            $ids[] = $section->id;
        }
        $this->reorderSections($ids,  $findByParams);
    }

    public function getAccountPaths($id)
    {
        return  $this->getPaths($this->findOneBy("accounts",["id" => $id]));
    }
    public function setAccountPaths($id,$params)
    {
        $account = $this->findOneBy("accounts",["id" => $id]);
        $this->setPaths($account,$params);
    }
    public function getPlanPaths($id)
    {
        return  $this->getPaths($this->findOneBy("plans",["id" => $id]));
    }
    public function getPlanPathsApp($id)
    {
        $paths = $this->getPlanPaths($id);
        $plan = $this->findOneBy("plans",["id" => $id]);
        $enrolling = $plan->type == "smartenroll";
        $accountPaths = $this->getAccountPaths($plan->userid);
        $types = $this->getPathTypes();
        foreach ($accountPaths as $key => $path)
        {
            if (!isset($paths[$key]))
                $paths[$key] = $path;
        }      
        foreach ($types as $type)
            if ($plan->{$type->type."PathId"} == -1)
               unset($paths[$type->type]);
        $paths['guided'] = $paths['chosen'] =  null;
        if (!$enrolling)
        {
            if (!empty($paths['standard']))
                $paths['guided'] = $paths['standard'];
        }
        else if (!empty($paths['smartenroll']))
                $paths['guided'] = $paths['smartenroll'];
        if (empty($paths['smartexpress'])  && !empty($paths['guided']))
            $paths['chosen'] = $paths['guided'];
        if (empty($paths['guided']) && !empty($paths['smartexpress']))
            $paths['chosen'] = $paths['smartexpress'];
        return $paths;
    }
    public function setPlanPaths($id,$params)
    {
        $plan = $this->findOneBy("plans",["id" => $id]);
        $this->setPaths($plan,$params);
    }
    public function addPathType($params)
    {
        $type = new PathTypes();
        $this->copyParamsToEntity($type,$params);
        $this->em->getManager()->persist($type);
        $this->em->getManager()->flush();      
    }
    public function getPathType($findOneBy)
    {
        return  $this->findOneBy("PathTypes",$findOneBy);   
    }
    public function editPathType($findOneBy,$params)
    {
        $type = $this->getPathType($findOneBy);
        $this->copyParamsToEntity($type,$params);
        $this->em->getManager()->flush();      
    }
    public function deletePathType($findOneBy)
    {
        $type = $this->getPathType($findOneBy);
        $path = $this->getPath(["pathTypeId" => $type->id]);
        if (!empty($path))
            throw new \Exception("Cannot delete path type, one or more paths assigned");
        $this->em->getManager()->remove($type);      
        $this->em->getManager()->flush();      
    }
    public function getPathTypes()
    {
        return  $this->findBy("PathTypes",[]);   
    }
    public function getPathRiskSelections()
    {
        $entity = new AccountRiskProfileQuestionnaire;
        $pathTypes = $this->getPathTypes();
        $selections = [];
        foreach ($pathTypes as $type)
            if (isset($entity->{$type->type."Selected"}))
                $selections[] = $type->type."Selected";
        return $selections;
    }
    public function getPathIdSections()
    {
        $types = $this->getPathTypes();
        $sections = [];
        foreach ($types as $type)
        {
            $sections[] = $type->type."PathId";
        }
        return $sections;
    }
    public function getPaths($entity)
    {
        $sections = $this->getPathIdSections();
        $paths = [];
        foreach ($sections as  $section)
            if (!empty($entity->$section))
            {
               $path = $this->getPath(["id" => $entity->$section]);
               if (!empty($path))
               {
                   $path->pathType = $this->getPathType(["id" => $path->pathTypeId]);
                   $paths[substr($section, 0, -6)] = $path;
               }
            }
        return $paths;
    }
    public function addPathAccount($params)
    {
        $pathAccount = new PathAccount();
        $this->copyParamsToEntity($pathAccount,$params);
        $this->em->getManager()->persist($pathAccount);
        $this->em->getManager()->flush();                    
    }
    public function getPathAccount($params)
    {
        return $this->findOneBy("PathAccount",$params);
    }
    public function deletePathAccount($params)
    {
        $pathAccount = $this->getPathAccount($params);
        $this->em->getManager()->remove($pathAccount);
        $this->em->getManager()->flush();          
    }
    public function copyPath($pathId,$id = null)
    {
        if (!empty($id))
            $this->setId($id);
        $this->container->get("PlayListService")->setId($this->id);

        $currentPath = $this->getPath(["id" => $pathId]);
        $currentPathArray = (array)$currentPath;
        foreach (["id","createdDate","updatedDate"] as $key)
            unset($currentPathArray[$key]);
        $currentPathArray['path'] = $currentPathArray['path']."_copy";
        $currentPathArray['description'] = $currentPathArray['description']." copy";
        $newPath = $this->addPath($this->id,$currentPathArray);
        $fullPath = $this->getPathFull(["id" => $pathId]);
        $this->em->getManager()->clear();
        foreach ($fullPath->sections as $section)
        {
            $sectionArray =[];
            $sectionArray['pathId'] = (string)$newPath->id;
            $sectionArray['appSectionListId'] = (string)$section->appSectionListId;
            
            if (!empty($sectionArray['pathId']) && !empty($sectionArray['appSectionListId']))
            {
                $appSteps = $this->findBy("AppStep",["appSectionId" => $section->id]);
                $sectionArray['appSteps'] = $appSteps;
                $this->addSection($sectionArray);                
            }
        }
        
        foreach ($fullPath->playLists as $playList)
        {
            $playListArray = (array)$playList;
            foreach (["id","pathId"] as $key)
                unset($playListArray[$key]);
            $playListArray['pathId'] = $newPath->id;
            $newPlayList = $this->container->get("PlayListService")->addPlayList($playListArray);
            foreach ($playList->videos as $video)
            {
                $videoArray = (array)$video;
                foreach (["id","pathId"] as $key)
                    unset($playListArray[$key]);
                $videoArray['playListId'] = $newPlayList->id;
                $video = $this->container->get("PlayListService")->addVideo($videoArray);
                foreach ($video->locales as $locale)
                {
                    $localeArray = (array)$locale;
                    $localeArray['videoId'] = $video->id;
                    $this->container->get("PlayListService")->addLocale($localeArray);                   
                }
            }
        }
        $this->copyPathContents($pathId,$newPath->id);
    }
    public function addContentType($params)
    {
        $appContentType = new AppContentType();
        $this->copyParamsToEntity($appContentType,$params);
        $this->em->getManager()->persist($appContentType);
        $this->em->getManager()->flush();         
    } 
    public function getContentType($params)
    {
        return $this->findOneBy("AppContentType",$params);
    }
    public function getContentTypes($params = [])
    {
        $contentTypes = $this->findBy("AppContentType",$params);
        foreach ($contentTypes as $contentType)
        {
            $contentTypes[$contentType->id] = $contentType;
        }
        return $contentTypes;

    }
    public function getContentTypesHash($field)
    {
        $contentTypes = $this->getContentTypes();
        $hash = [];
        foreach ($contentTypes as $item)
        {
            $hash[trim($item->{$field})] = $item;
        }
        return $hash;        
    }
    public function editContentType($params,$updateParams)
    {
        $appContentType =  $this->findOneBy("AppContentType",$params);
        $this->copyParamsToEntity($appContentType,$updateParams);
        $this->em->getManager()->flush();           
    }
    public function deleteContentType($params)
    {
        $appContentType =  $this->findOneBy("AppContentType",$params);
        $this->em->getManager()->remove($appContentType);
        $this->em->getManager()->flush();           
    }
    
    public function getDefaultContents($findBy)
    {
        $contents =  $this->findBy("DefaultContent",$findBy);
        $contentsHash = [];
        foreach ($contents as &$content)
        {
            $content->content = $content->englishContent;
            if ($this->lang == "es" && !empty($content->spanishContent))
            $content->content = $content->spanishContent;
            $contentType = $this->getContentType(["id" => $content->contentTypeId]);
            $content->contentType =$contentType->type;
            $content->contentTypeDescription = $contentType->description;
            $contentsHash[$content->step][$content->label] = $content;            
        }
        return $contentsHash;
    }
    public function getDefaultContent($findBy)
    {
        return  $this->findOneBy("DefaultContent",$findBy);
    }
    public function getPathContentList($defaultFindBy = [],$pathFindBy = [])
    {
        if ($defaultFindBy['sectionId'])
        {
            $section = $this->getSection(["id" => $defaultFindBy['sectionId'] ]);
            if ($section)
            $defaultFindBy['sectionListId'] = $section->appSectionListId;  
            unset($defaultFindBy['sectionId']);
        }
        $contents = $this->getDefaultContents($defaultFindBy);
        return $contents;
    }
    public function addDefaultContent($params)
    {        
        if ($this->validateContent($params))
        {
            $defaultContent = new DefaultContent();
            $this->copyParamsToEntity($defaultContent,$params);      
            $this->em->getManager()->persist($defaultContent);
            $this->em->getManager()->flush();
        }
    }
    private function validateContent($params)
    {
        $contentType = $this->getContentType(["id" => $params['contentTypeId']]);
        $sectionListItem = $this->getSectionListItem(["id" => $params['sectionListId']]);
        return (!empty($contentType) && !empty($sectionListItem));
    }
    public function editDefaultContent($params)
    {
        $defaultContent = $this->getDefaultContent(["id" => $params['contentId']]);
        $params['sectionListId'] = $defaultContent->sectionListId;
        if ($this->validateContent($params) && $defaultContent)
        {
            $this->copyParamsToEntity($defaultContent,$params);      
            $this->em->getManager()->flush();
        }
    }
    public function deleteDefaultContents($findBy)
    {
        $defaultContents = $this->getDefaultContents($findBy);
        foreach ($defaultContents as $defaultContent)
        {
            $this->em->getManager()->remove($defaultContent);
            $this->em->getManager()->flush();
        }
    }
    public function deleteDefaultContent($findOneBy)
    {
        $defaultContent = $this->getDefaultContent($findOneBy);
        $this->em->getManager()->remove($defaultContent);
        $this->em->getManager()->flush();        
    }
    public function getPathContent($findOneby)
    {
        return  $this->findOneBy("PathContent",$findOneby);
    }
    public function getPathContents($findby)
    {
        return  $this->findBy("PathContent",$findby);
    }
    public function copyPathContents($pathId,$copyToPathId)
    {
        $contents = $this->getPathContents(["pathId" => $pathId]);
        foreach ($contents as $content)
        {
            $newContent = new PathContent();
            $this->copyParamsToEntity($newContent, $content);
            $newContent->pathId = $copyToPathId;
            $this->em->getManager()->persist($newContent);
            $this->em->getManager()->flush();  
        }
    }
    public function deletePathContents($findBy)
    {
        $contents = $this->getPathContents($findBy);
        foreach ($contents as $content)
        {
            $this->em->getManager()->remove($content);
            $this->em->getManager()->flush();              
        }
    }
    public function getContent($defaultContentId,$pathId)
    {
        $content = $this->getPathContent(["defaultContentId" => $defaultContentId, "pathId" => $pathId]);
        if (empty($content))
        {
            $content = $this->getDefaultContent(["id" => $defaultContentId]);
            $content->contentTable = "default";
        }
        else 
             $content->contentTable = "path";

        return $content;
    }
    public function updatePathContent($findOneBy,$params)
    {
        $original = $params;
        unset($params['rules']);
        $content = $this->getPathContent($findOneBy);
        if (empty($content))
        {
            $content = new PathContent();
            $params = array_merge($params,$findOneBy);
            $this->em->getManager()->persist($content);        
        }
        $this->copyParamsToEntity($content,$params);
        if (isset($original['rules']))
        {
            $content->setRules($original['rules']);
        }
        $this->em->getManager()->flush();
    }
    public function deletePathContent($findOneBy)
    {
        $content = $this->getPathContent($findOneBy);
        var_dump($content);
        $this->em->getManager()->remove($content);
        $this->em->getManager()->flush();
    }
    public function setPathId($pathId)
    {
        $this->pathId = $pathId;
    }
    public function setUserId($userid)
    {
        $this->userid = $userid;
    }
    public function setPlanId($planid)
    {
        $this->planid = $planid;
    }

    private function setPaths($entity,$params)
    {
        $sections = $this->getPathIdSections();
        foreach ($sections as  $section)
        {
            if (isset($params[$section]))
            {
                if (empty($params[$section]))
                    $entity->$section  = null;
                else
                    $entity->$section = $params[$section];
            }
        }
    }
}

