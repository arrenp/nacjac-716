<?php
namespace classes\classBundle\Services\Paths;
use classes\classBundle\Entity\AppPlayList;
use classes\classBundle\Entity\AppVideo;
use classes\classBundle\Entity\AppVideoLocale;
use classes\classBundle\Entity\AppPlayListVideo;
use Symfony\Component\DependencyInjection\ContainerInterface;
class PlayListService extends PathsService
{
    public function addPlayList($params)
    {
        $playList = new AppPlayList();
        $path = $this->getPath(["id" => $params['pathId']]);
        if ($path == null)
            throw new \Exception("Path doesn't exist, cannot add playlist");
        $this->copyParamsToEntity($playList,$params);
        $this->em->getManager()->persist($playList);
        $this->em->getManager()->flush(); 
        $this->updatePathLog($path);
        return $playList;
    }   
    public function getPlayList($params)
    {
        return $this->findOneBy("AppPlayList",$params);
    }
    public function getPlayListFull($params)
    {
        $playList = $this->getPlayList($params);
        return $this->getPlayListFullEntity($playList);
    }
    public function getPlayListsFull($params)
    {
        $returnMe = array();
        $playLists = $this->getPlayLists($params);
        foreach ($playLists as &$playList) {
            $playList = $this->getPlayListFullEntity($playList);
            $returnMe[$playList->name] = $playList;
        }
        return $returnMe;
    }
    public function getPlayListFullEntity($playList)
    {
        $playList->videos = $this->getVideosFull(["playListId" => $playList->id ]);
        return $playList;
    }
    public function getPlayLists($params)
    {
        return $this->findBy("AppPlayList",$params);
    }
    public function editPlayList($params,$updateParams)
    {
        $playList = $this->getPlayList($params);
        $this->copyParamsToEntity($playList,$updateParams);                
        $path = $this->getPath(["id" => $playList->pathId]);
        if ($path == null)
            throw new \Exception("Path doesn't exist, cannot edit playlist");
        $this->em->getManager()->flush(); 
        $this->updatePathLog($path);
    }
    public function deletePlayList($params)
    {
        $playList = $this->getPlayList($params);
        $this->deletePlayListEntity($playList);
        $this->updatePathLogById($playList->pathId);        
    }
    public function deletePlayListEntity($playList)
    {
        $this->deleteVideos(["playListId"  => $playList->id ]);
        $this->em->getManager()->remove($playList); 
        $this->em->getManager()->flush(); 
    }
    public function deletePlayLists($params)
    {
        $playLists = $this->getPlayLists($params);
        foreach ($playLists as $playList)
            $this->deletePlayListEntity($playList);
        if (!empty($playList))
        $this->updatePathLogById($playList->pathId);        
    }
    public function addVideo($params)
    {
        $video = new AppVideo();
        $this->copyParamsToEntity($video,$params);  
        $playList = $this->getPlayList(["id" => $params['playListId']]);
        if ($playList == null)
            throw new \Exception("Adding video to non existant playlist");
        $video->pathId = $playList->pathId;
        $this->em->getManager()->persist($video);
        $this->em->getManager()->flush();         
        $this->updatePathLogById($video->pathId);
        return $video;
    }
    public function getVideo($findByParams)
    {
        return $this->findOneBy("AppVideo",$findByParams);
    }
    public function getVideoFull($findByParams)
    {
        $video = $this->getVideo($findByParams);
        return $this->getVideoFullEntity($video);
    }
    public function getVideoFullEntity($video)
    {
        $video->locales = $this->getLocales(["videoId" => $video]);
        return $video;
    }
    public function getVideosFull($findByParams)
    {
        $videos = $this->getVideos($findByParams);
        foreach ($videos as &$video)
            $video = $this->getVideoFullEntity ($video);
        return $videos;
    }
    public function getVideos($findByParams,$orderBy = ["step" => "asc"])
    {
        return $this->findBy("AppVideo",$findByParams,$orderBy);
    }
    public function editVideo($params,$updateParams)
    {
        $video = $this->getVideo($params);
        $this->copyParamsToEntity($video,$updateParams);  
        $this->em->getManager()->flush();         
        $this->updatePathLogById($video->pathId);
    }
    public function deleteVideo($params)
    {
        $video = $this->getVideo($params);
        $this->deleteVideoEntity($video);
        $this->updatePathLogById($video->pathId);
    }
    public function deleteVideoEntity($video)
    {
        $this->deleteLocales(["videoId" => $video->id]);
        $this->em->getManager()->remove($video);
        $this->em->getManager()->flush(); 
    }
    public function deleteVideos($params)
    {
        $videos = $this->getVideos($params);
        foreach ($videos as $video)
            $this->deleteVideoEntity($video);     
        if (!empty($video))
        $this->updatePathLogById($video->pathId);
    }
    public function getLanguage($params)
    {
        return $this->findOneBy("languages",$params);
    }
    public function addLocale($params)
    {
        $locale = new AppVideoLocale();
        $this->copyParamsToEntity($locale,$params);
        $video = $this->getVideo(["id" => $locale->videoId]);
        if (empty($video))
            throw new \Exception("Cannot add locale to non existant video");
        if (empty($this->getLanguage(["id" => $locale->languageId])))
            throw new \Exception("Non-existant language assigned to locale");
            
        $locale->pathId = $video->pathId;
        $locale->playListId = $video->playListId;
        $this->em->getManager()->persist($locale);
        $this->em->getManager()->flush();    
        $this->updatePathLogById($locale->pathId);

    }
    public function getLocale($params)
    {
        return $this->findOneBy("AppVideoLocale",$params);
    }
    public function getLocales($params)
    {
        return $this->findBy("AppVideoLocale",$params);
    }
    public function deleteLocales($params)
    {
        $locales = $this->getLocales($params);
        foreach ($locales as $locale)
            $this->deleteLocaleEntity ($locale);
        if (!empty($local))
            $this->updatePathLogById($locale->pathId);
    }
    public function editLocale($params,$updateParams)
    {
        $locale = $this->getLocale($params);
        $this->copyParamsToEntity($locale,$updateParams);
        $this->em->getManager()->flush();
        $this->updatePathLogById($locale->pathId);
    }
    public function deleteLocale($params)
    {
        $locale = $this->getLocale($params);
        $this->deleteLocaleEntity($locale);
        $this->updatePathLogById($locale->pathId);
    }
    public function deleteLocaleEntity($locale)
    {
        $this->em->getManager()->remove($locale);        
        $this->em->getManager()->flush();     
    } 
    public function reorderVideos($playListId,$ids)
    {
        $videos = $this->getVideos(["playListId" => $playListId]);
        $videosHash = [];
        foreach ($videos as $video)
            $videosHash[$video->id] = $video;
        $step = 0;
        foreach ($ids as $id)
        {
            if (!empty($videosHash[$id]))
               $videosHash[$id]->step = $step++;             
        }
        $this->em->getManager()->flush();     
    }
    public function addLocalesToVideo($locales)
    {
        $videoId = current($locales)['videoId'];
        $this->deleteLocales(["videoId" => $videoId]);
        foreach($locales as $locale)
            $this->addLocale($locale);
    }
    public function addPlayListVideo($params)
    {
        $playListVideo = new AppPlayListVideo();
        $this->copyParamsToEntity($playListVideo,$params);
        $this->em->getManager()->persist($playListVideo);
        $this->em->getManager()->flush();  
    }
    public function getPlayListVideo($params)
    {
        return $this->findOneBy("AppPlayListVideo",$params); 
    }
    public function getPlayListVideos($params)
    {
        return $this->findBy("AppPlayListVideo",$params); 
    }
    public function getPlayListVideosFull($params= [])
    {
        $videos =  $this->getPlayListVideos($params);
        $languages  = $this->container->get("LanguagesService")->getAll("id");
        foreach ($videos as &$video)
        {
            $video->language = $languages[$video->languageId];
        }
        return $videos;
    }
    public function editPlayListVideo($params,$updateParams)
    {
        $playListVideo = $this->getPlayListVideo($params);
        $this->copyParamsToEntity($playListVideo,$updateParams);
        $this->em->getManager()->persist($playListVideo);
        $this->em->getManager()->flush();         
    }
    public function deletePlayListVideo($params)
    {
        $playListVideo = $this->getPlayListVideo($params);
        $this->em->getManager()->remove($playListVideo);
        $this->em->getManager()->flush();         
    }
}
