<?php
namespace classes\classBundle\Services\AmeritasCustom;
use classes\classBundle\Services\BaseService;
use Shared\General\GeneralMethods;
class AccountRolloverReportService extends BaseReportService
{
    public function callUsCount($options = array())
    {
        $options['where'][] =  "callUs is not null";
        return $this->resultCountProfiles($options);
    }
    public function callMeCount($options = array())
    {
        $options['where'][] =  "callMe is not null";
        return $this->resultCountProfiles($options);
    }
    public function emailMeCount($options = array())
    {
        $options['where'][] =  "emailMe is not null";
        return $this->resultCountProfiles($options);
    }    
    public function enrollmentContactViewedTimesDistinct($options = array())
    {
        $options['where'][] =  "enrollmentContactViewedTimes > 0";
        return $this->resultCountProfiles($options);
    }
    public function enrollmentContactViewedTimes($options = array())
    {
        $sql = "select SUM(enrollmentContactViewedTimes) as count FROM profiles";
        $sql =  $this->appendSql($sql,$options);
        $result = $this->connection->executeQuery($sql)->fetch()['count']; 
        if ($result == "")
        {
            $result = 0;
        }
        return $result;
    }
    public function resultCountProfilesQuery($options)
    {
        $sql = "select COUNT(DISTINCT(participantid)) as count FROM profiles";
        return $this->appendSql($sql,$options);
    }
    public function appendSql($sql,$options)
    {
        $sql = $sql." WHERE id > 0 ";
        if (isset($options['where']))
        {
            foreach ($options['where'] as $query)
            {
                $sql = $sql." AND ".$query." ";
            }
        }
        if (isset($options['start']))
        {
            $sql = $sql." AND ";
            $sql = $sql." reportDate >= '".$options['start']."' ";
        }
        if (isset($options['end']))
        {
            $general = $this->container->get("GeneralMethods");
            $options['end'] = $general->addDaysToString($options['end']);
            $sql = $sql." AND ";
            $sql = $sql." reportDate <= '".$options['end']."' ";
        }        
        return $sql;
    }
    public function resultCountProfiles($options)
    {
        $sql = $this->resultCountProfilesQuery($options);
        return $this->connection->executeQuery($sql)->fetch()['count'];        
    }    
}
