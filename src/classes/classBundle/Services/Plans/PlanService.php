<?php
namespace classes\classBundle\Services\Plans;
use classes\classBundle\Entity\plans;
use classes\classBundle\Services\BaseService;
use Symfony\Component\DependencyInjection\ContainerInterface;
class PlanService extends BaseService
{
    public function currentAccount()
    {         
        return $this->em->getRepository($this->classPath . "accounts")->findOneBy(array("id" => $this->session->get("userid")));
    }
    public function currentPlan()
    {
        return $this->em->getRepository($this->classPath . "plans")->findOneBy(array("id" => $this->session->get("planid")));       
    }
    public function currentPlanModules()
    {
        return $this->em->getRepository($this->classPath . "plansModules")->findOneBy(array("planid" => $this->session->get("planid")));       
    }
    public function syncModules($types)
    {
        $currentplan = $this->currentplan();
        $plans = $this->syncModulesPlans();
        foreach ($plans as &$plan)
        {
            foreach ($currentplan as $key => $value)
            {
                if (!in_array($key,array("id","planid","userid")))
                {
                    $plan->$key = $value;
                }
            }           
        }
        $this->em->flush();
    }
    public function currentTypes()
    {
        $account = $this->currentAccount();
        $return = array();
        if ($account->smartPlanEnabled)
        $return['smartplan'] = $this->addType("SmartPlan","smartplan");
        if ($account->smartEnrollEnabled)
        $return['smartenroll'] = $this->addType("SmartEnroll","smartenroll");
        
        if ($this->session->get("masterRoleType") == "vwise" || $this->session->get("masterRoleType") == "vwise_admin")
        {
            if ($account->powerviewEnabled)
            $return['powerview'] = $this->addType("PowerView","powerview");
            if ($account->irioEnabled)
            $return['irio'] = $this->addType("Irio","irio");
        }
        return $return;
    }
    public function syncModulesTypes()
    {
        $types = $this->currentTypes();
        $currentplan = $this->currentPlan();
        if (isset($types[$currentplan->type]))
        unset($types[$currentplan->type]);               
        return $types;
    }
    public function syncModulesPlans()
    {
        $types = $this->syncModulesTypes();
        $currentplan = $this->currentplan();
        $plans = array();
        $params = array("planid" => $currentplan->planid,"partnerid" => $currentplan->partnerid,"deleted" => 0);
        foreach ($types as $type)
        {
            $params['type'] = $type['value'];
            $plan = $this->em->getRepository($this->classPath."plans")->findOneBy($params);   
            if ($plan != null)
            {
                $module = $this->em->getRepository($this->classPath."plansModules")->findOneBy(array("planid" => $plan->id));
                $plans[$type['value']] = $module;
                $plans[$type['value']]->type = $type['value'];
                $plans[$type['value']]->description = $type['description'];
            }
        }    
        return $plans;
    }
    public function addType($description,$value)
    {
        return array("description" => $description, "value" => $value);
    }
    
    public function update(plans &$plan, $data) {
        $updated = false;
        foreach ($data as $key => $value) {
            if ($plan->$key !== $value) {
                $updated = true;
            }
            $plan->$key = $value;
        }
        if ($updated) {
            $this->connection->executeUpdate('update launchToolRecipients set contactStatus="UPDATED" where planid = :planid', ['planid' => $plan->id]);
        }
    }
}
