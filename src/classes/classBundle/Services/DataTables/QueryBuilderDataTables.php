<?php
namespace classes\classBundle\Services\DataTables;
use classes\classBundle\Services\BaseService;
class QueryBuilderDataTables extends BaseService
{
    public function jsonObject($table,$fields,$params)//zero customization, just read all from table
    {        
        $rows = $this->autoAddResult($table,$fields,$params);
        $response = [];
        $response['recordsTotal'] = $this->totalResult($table);
        $response['recordsFiltered']  =$this->filteredResult($table,$fields,$params);
        $response['params'] = $params; 
        return ["rows" => $rows, "response" => $response ];
    }
    public function jsonObjectQueryBuilder($table,$fields,$params,$queryBuilder,$count = "a.id")//create your own querybuilder then apply autologic for datatables
    {
        $queryBuilderData =  clone $queryBuilder;
        $queryBuilderTotal = clone $queryBuilder;
        $queryBuilderFiltered = clone $queryBuilder;
        $queryBuilderTotal->resetDQLPart('groupBy');
        $queryBuilderFiltered->resetDQLPart('groupBy');
        $query = $this->autoAdd($table,$fields,$params,$queryBuilderData)->getQuery();
        $rows = $query->getResult();     
        $response = [];
        $queryBuilderRecordsFiltered = $this->autoAddFiltered($table,$fields,$params,$queryBuilderFiltered);
        $response['recordsFiltered']  =$this->filteredTotal($table,$fields,$params,$queryBuilderRecordsFiltered,$count)->getQuery()->getSingleScalarResult();       
        $response['recordsTotal'] = $this->total($table,$queryBuilderTotal,$count)->getQuery()->getSingleScalarResult();
        $response['params'] = $params; 
        $response['query'] = $query->getSQL();
        $response['fields'] = $fields;
        return ["rows" => $rows, "response" => $response ];       
    }
    public function queryBuilder($table,$variable = 'a')
    {
        $accountRepo = $this->container->get("doctrine")->getRepository('classesclassBundle:'.$table);
        $queryBuilder = $accountRepo->createQueryBuilder($variable);
        return $queryBuilder;
    }
    public function autoAdd($table,$fields,$params,$queryBuilder = null)
    {
        if (empty($queryBuilder))
        {
            $queryBuilder = $this->queryBuilder($table);
        }
        $queryBuilder =$this->addPagination($queryBuilder,$params);
        $queryBuilder =$this->addOrderBy($queryBuilder,$params,$fields);
        $queryBuilder =$this->addSearch($queryBuilder,$params,$fields);
        return $queryBuilder;
    }
    public function autoAddQuery($table,$fields,$params,$queryBuilder = null)
    {
        if (empty($queryBuilder))
        {
            $queryBuilder = $this->autoAdd($table,$fields,$params);
        }
        return $queryBuilder->getQuery();
    }
    public function autoAddResult($table,$fields,$params,$query = null)
    {
        if (empty($query))
        {
            $query= $this->autoAddQuery($table,$fields,$params);
        }
        return $query->getResult();
    }    
    public function total($table,$queryBuilder = null,$count= 'a.id')
    {
        if (empty($queryBuilder))
        {
            $queryBuilder = $this->queryBuilder($table);
        }
        $queryBuilder->select('count('.$count.')');
        return $queryBuilder;
    }
    public function totalQuery($table,$queryBuilder = null)
    {
        if (empty($queryBuilder))
        {
            $queryBuilder = $this->total($table);
        }
        return $queryBuilder->getQuery();
    }
    private function totalResult($table,$query = null)
    {
        if (empty($query))
        {
            $query = $this->totalQuery($table);
        }
        return $query->getSingleScalarResult();
    }    
    public function autoAddFiltered($table,$fields,$params,$queryBuilder = null)
    {
        if (empty($queryBuilder))
        {
            $queryBuilder = $this->queryBuilder($table);
        }
        $queryBuilder = $this->addOrderBy($queryBuilder,$params,$fields);
        $queryBuilder = $this->addSearch($queryBuilder,$params,$fields);
        return $queryBuilder;
    }
    public function filteredTotal($table,$fields,$params,$queryBuilder = null,$count= 'a.id')
    {
        if (empty($queryBuilder))
        {
            $queryBuilder = $this->autoAddFiltered($table,$fields,$params);
        }
        $queryBuilder->select('count('.$count.')');
        return $queryBuilder;
    }
    public function filteredQuery($table,$fields,$params,$queryBuilder = null)
    {
        if (empty($queryBuilder))
        {
            $queryBuilder = $this->filteredTotal($table,$fields,$params);
        }
        return $queryBuilder->getQuery();
    }
    public function filteredResult($table,$fields,$params,$query = null)
    {
        if (empty($query))
        {
            $query = $this->filteredQuery($table,$fields,$params);
        }
        return $query->getSingleScalarResult();
    }
    public function addSearch($queryBuilder,$params,$fields)
    {
        if ($params['search']['value'] != "")
        {
            $cond = "";
            foreach ($fields as $field)
            {
                if ($cond != "")
                {
                    $cond = $cond." OR ";
                    
                }
                $cond = $cond.$field." LIKE ?1 ";
            }
            $cond = " ( ".$cond." ) ";
            $queryBuilder->andWhere($cond);
            $queryBuilder->setParameter(1, "%".$params['search']['value']."%");
        }
        return $queryBuilder;
    }
    public function addPagination($queryBuilder,$params)
    {
        $queryBuilder->setFirstResult($params["start"]);
        $queryBuilder->setMaxResults($params["length"]);
        return $queryBuilder;
    }  
    public function addOrderBy($queryBuilder,$params,$fields)
    {
        $order = $params['order'][0];        
        $queryBuilder->orderBy($fields[$order['column']],$order['dir']);
        return $queryBuilder;
    }
    public function formatResult($tables,$results)
    {
        $table = reset($tables);
        $return = [];
        $params = [];
        foreach ($results as $result)
        {
            $className = explode('\\',get_class ($result));
            $className = end($className);
            if (in_array($className,$tables))
            {
                if (isset($params[$className]))
                {
                    $return[] = $params;
                    $params = [];
                }
                $params[$className] = $result;
            }
        }
        if ($params != [])
        {
            $return[] = $params;
        }
        foreach ($return as $key => $row)
            foreach($tables as $table)
                if (!isset($return[$key][$table]))
                    $return[$key][$table]= null;  
        return $return;
    }
}
