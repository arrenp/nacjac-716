<?php

namespace Sessions\AdminBundle\Services;

class AdminSessionService {

    public $userid;
    public $clientid;
    public $roleType;
    public $planid;
    public $appurl;
    public $section;
    public $currentpage;
    public $newclientid;
    public $masterid;
    public $session;
    public $debugbar;

    public function __construct($session)
    {
        $this->session = $session;
        $this->userid = $this->session->get("userid");
        $this->clientid = $this->session->get("clientid");
        $this->roleType = $this->session->get("roleType");
        $this->planid = $this->session->get("planid");
        $this->appurl = $this->session->get("appurl");
        $this->section = $this->session->get("section");//which section of the admin are you in, accounts, support, plans, settings
        $this->currentpage = $this->session->get("currentpage");//which page of the admin are you on
        $this->newclientid = $this->session->get("newclientid");
        $this->masterid = $this->session->get("masterid");
        $this->sessionid = $this->session->get("_csrf/authenticate");
        $this->openchat = $this->session->get("openchat");
        $this->debugbar = $this->session->get("debugbar");
        $this->masterRoleType = $this->session->get("masterRoleType");
        $this->manageUserid = $this->session->get("manageUserid");
        $this->type = $this->session->get("type");
        $this->adminPlanType = $this->session->get("adminPlanType");
    }
    public function set($key,$value)
    {
        $this->session->set($key,$value);
        $this->$key = $value;
    }
    
}
