<?php 

include '../../../controller/DefaultController.php';

?>

{{ render(controller('TemplatesTemplatesBundle:AdminTemplate:header', {'pagename': 'AdminStats'})) }}
<div id="content">
    <div class="col-md-12">
         <div class="col-md-12">
   
            <div class="panel panel-default">
                <div class="panel-heading">Plan Comparison</div>
                <div class="panel-body">
                    <div id="superchart"></div>
                </div>
            </div>
        </div>       
            <div class="col-md-4">
                <div class="panel panel-default" id="datepicker" style="min-height:264px;">
                    <div class="panel-heading">Choose A Date Range</div>
                    <div class="panel-body">
                        <p>Start Date:</p>
                        <input type="text" class="form-control" id="startdatepicker" placeholder="Start Date" name="startdatepicker" value="{{startdate}}">
                        <br>
                        <p>End Date</p>
                        <input type="text" class="form-control" id="enddatepicker" placeholder="End Date" name="enddatepicker" value="{{enddate}}">
                        <input class="buttons" id="apply_date" type="button" value="Apply">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Choose A Partner</div>
                    <div class="panel-body">
                        <select multiple="multiple" id="partner_select">
                            
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Choose A Plan</div>
                    <div class="panel-body">
                        <select multiple="multiple" id="plan_select">
                            <option>Bar</option>
                        </select>
                    </div>
                </div>
            </div>
    </div>    
</div>
{{ render(controller('TemplatesTemplatesBundle:AdminTemplate:footer')) }}
<script type="text/javascript">  
    
    
    
    $(document).ready(function(){
        
    $("#plan_select").bootstrapDualListbox({});
    $("#partner_select").bootstrapDualListbox({});

      
      $('#apply_date').click(function(){
        $.post("{{path('_adminstats_index')}}", {startdate:$("#startdatepicker").val(), enddate:$("#enddatepicker").val()}).done(function(data){
               $('#partner_select').append(data);
        } );
      });
      
        //Datepickers
        $('#startdatepicker').datepicker({dateFormat: "yy-mm-dd" });
        $('#enddatepicker').datepicker({ dateFormat: "yy-mm-dd" });
        $('#startDate').datepicker({ dateFormat: "yy-mm-dd" });
        
        
        //Chart
        $('#superchart').highcharts({
        var mydates = {{datadates}};
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Total Visits'
        },
        subtitle: {
            text: 'By Date Range'
        },
        xAxis: [{
            <?php ?>,
            labels: {rotation:70}
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Average All',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Visits',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} Hits',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Plan: Admin Stats',
            type: 'column',
            yAxis: 1,
            {{data}},
            tooltip: {
                valueSuffix: ' Hits'
            }

        }, {
            name: 'All Plans',
            type: 'spline',
            {{data}},
            tooltip: {
                valueSuffix: 'Hits'
            }
        }]
    });      
});

    //Select Boxes

/*
    $('#partner_select').DualListBox();
    $('#plan_select').DualListBox();
  
    $(document).ready(function(){

        $('#button').click(function(){
        $.post("",
        $("#myVar").serialize())
                          .done(function( data ) {
                                        console.log(data);
                  });
              });
          });
*/ 
</script>

         

