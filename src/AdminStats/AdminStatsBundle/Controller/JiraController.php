<?php
namespace AdminStats\AdminStatsBundle\Controller;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;



class JiraController extends Controller
{
    public function indexAction()
    {
    	$session = $this->get('adminsession');
        $session->set("section","Stats");
        return $this->render('AdminStatsAdminStatsBundle:Jira:index.html.twig');
    }
}

