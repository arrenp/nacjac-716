<?php

namespace AdminStats\AdminStatsBundle\Controller;

use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
require_once(getcwd()."/../src/Utilities/dompdf/dompdf_config.inc.php");

class FiduciaryReportController extends Controller
{

    public function indexAction()
    {
        
        $session = $this->get('adminsession');
        $session->set("section", "Stats");

        return $this->render('AdminStatsAdminStatsBundle:FiduciaryReport:index.html.twig');
    }

    public function planListAction(Request $request)
    {
        $session = $this->get('adminsession');
        $session->set("section", "Stats");
        return $this->render('AdminStatsAdminStatsBundle:FiduciaryReport:planlist.html.twig', array("userid" => $request->query->get("userid")));
    }

    public function reportAction(Request $request)
    {
        $data = $this->reportData($request->query->get("planid"), $request->query->get("start"), $request->query->get("end"));

        return $this->render('AdminStatsAdminStatsBundle:FiduciaryReport:report.html.twig', array("stats" => $data));
    }
    public function pdfAction(Request $request)
    {
        ini_set("max_execution_time", "999");

        $htmlString = $this->pdfHtml($request);
        $dompdf = new \DOMPDF();
        //style must be css 2.1 in order for dompdf to work.
        $dompdf->load_html($htmlString);
        $dompdf->render();
        $dompdf->stream("FiduciaryReport.pdf");  
        return new Response("");
    }
    public function pdfHtml($request)
    {
        return $this->reportAction($request)->getContent()."<style>.fiduciary-alt:nth-child(even){background-color:#ccc;} table {margin-bottom:20px;} .stat-container {margin-bottom:10px;} h3 {color:white;} th{text-align:left;}</style>";
    }
    public function emailAction(Request $request)
    {
        $filename = sys_get_temp_dir()."/stats.pdf";
        $htmlString = $this->pdfHtml($request);
        $dompdf = new \DOMPDF();
        $dompdf->load_html($htmlString);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents($filename, $output);
        $emails = explode(",",$request->query->get("emails"));
        
        foreach ($emails as $email)
        {
            $emailContents = "Attached is your Fiduciary Report";
            $message = \Swift_Message::newInstance()
            ->setSubject('SmartPlan Test Email')
            ->setFrom('info@smartplanenterprise.com')
            ->setTo(trim($email))
            ->attach(\Swift_Attachment::fromPath($filename))      
            ->setBody(
            $emailContents,'text/html');
            //render email template and send email
            $this->get('mailer')->send($message);            
        }

        
        return new Response("");
    }
    public function emailFormAction(Request $request)
    {
        
        $request = $request->query->all();
        return $this->render('AdminStatsAdminStatsBundle:FiduciaryReport:emailForm.html.twig',array("request" => $request));
    }
    public function csvAction(Request $request)
    {
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"FiduciaryReport.csv\";");
        header("Content-Transfer-Encoding: binary");
        $data = $this->reportData($request->query->get("planid"), $request->query->get("start"), $request->query->get("end"));

        $csv = "Fiduciary Report\n";
        $csv = $csv."Plan,".$data->planName."\n";
        $csv = $csv."From,".$data->start."-".$data->end."\n";
        
        foreach ($data->data as $stat)
        {
            $csv = $csv.$stat->participant['name']."\n";
            foreach ($data->header as $header)
            $csv = $csv.$header.",";
            $csv = $csv."\n";
            foreach ($stat->stats as $row)
            {
                $csv = $csv.$row['date'].",".$row['time'].",".$row['videoPlayed'].",".$row['playDuration'].",".$row['videoLength'].",".$row['percentageViewed']."%";
                $csv = $csv."\n";
            }
        }
                
        echo $csv;

        
        return new Response("");
    }

    public function reportData($planid, $start, $end)
    {
        $startExplode = explode("/", $start);
        $endExplode = explode("/", $end);
        $start = $startExplode[2] . "/" . $startExplode[0] . "/" . $startExplode[1];
        $end = $endExplode[2] . "/" . $endExplode[0] . "/" . $endExplode[1];
        $report = new \stdClass();
        $connection = $this->get('doctrine.dbal.default_connection');
        $planSql = "SELECT * FROM plans WHERE id= " . $planid . " LIMIT 1";
        $plan = $connection->executeQuery($planSql)->fetch();
        $accountSql = "SELECT * FROM defaultPlan WHERE userid= " . $plan['userid'] . " LIMIT 1";
        $account = $connection->executeQuery($accountSql)->fetch();
        $reportStyles = array('reportsStylesHeader', 'reportsStylesFont', 'reportsInnerTable', 'reportsBackground');
        foreach ($reportStyles as $style)
        {
            $report->$style = '';
            if ($account[$style] != "")
            $report->$style = $account[$style];
            if ($plan[$style] != "")
            $report->$style = $plan[$style];
            
        }
        
        if ($plan['providerLogoImage'] == ""){
        $plan['providerLogoImage'] = "https://admin.smartplanenterprise.com/images/alpha.png";
        } 
        $report->planName = $plan['name'];
        $report->plan = $plan;
        $report->start = $start;
        $report->end = $end;
        $report->header = array();
        $report->header[0] = "Date";
        $report->header[1] = "Time";
        $report->header[3] = "File Name";
        $report->header[4] = "Duration";
        $report->header[5] = "Video Length";
        $report->header[6] = "Percent Watched";
        $profiles = $this->get('profiles.service');
        $report->data = array();
        


        $videoStatsSpeSql = "SELECT * FROM videoStatsSpe WHERE planid = " . $planid . " AND timestamp >= '" . $start . "' AND timestamp <= '" . $end . " 23:59:59' group by uniqid";
        $videoStatsSpe = $connection->executeQuery($videoStatsSpeSql)->fetchAll();

        foreach ($videoStatsSpe as $stat)
        {
            $report->data[$stat['uniqid']] = new \stdClass();
            $participantSql = "SELECT *," . $profiles->generateQueryFields("participants") . " FROM participants WHERE id=" . $stat['participantid'] . " LIMIT 1";
            $participant = $connection->executeQuery($participantSql)->fetch();
            $participant['name'] = $this->formatFullName($participant['participants_firstName'], $participant['participants_lastName']);
            $report->data[$stat['uniqid']]->participant = $participant;
            $videoStatsSpeSql2 = "SELECT * FROM videoStatsSpe WHERE planid = " . $planid . " AND uniqid = '" . $stat['uniqid'] . "'";
            $report->data[$stat['uniqid']]->stats = $connection->executeQuery($videoStatsSpeSql2)->fetchAll();
            $report->data[$stat['uniqid']]->id = $stat['uniqid'];
            foreach ($report->data[$stat['uniqid']]->stats as &$stat2)
            {
                $explodeTimeStamp = explode(" ", $stat2['timestamp']);
                $stat2['date'] = $explodeTimeStamp[0];
                $stat2['time'] = $explodeTimeStamp[1];
            }
        }
        return $report;
    }

    public function formatName($name)
    {
        if ($name == null) $name = "";
        return trim($name);
    }

    public function formatFullName($name, $name2)
    {
        $name = $this->formatName($name) . " " . $this->formatName($name2);
        return trim($name);
    }

}
