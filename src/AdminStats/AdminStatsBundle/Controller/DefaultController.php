<?php
namespace AdminStats\AdminStatsBundle\Controller;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Plans\PlansBundle\Controller\ga\GoogleAnalyticsAPI;


class DefaultController extends Controller
{
    
    public function _construct($x){
        $this->x = $x;
    }
    
    public function indexAction()
    {
        
        $this->init();

        $defaultstartdate = date('Y-m-d',strtotime('-30 days'));
        $today = getdate();
        $defaultenddate = $today['year']."-".sprintf("%02s", $today['mon'])."-".sprintf("%02s",$today['mday']);

            //Params
        $parrams = array(
                         'dimensions' => 'ga:date',
                         'metrics'    => 'ga:pageviews',
                         'start-date' => $defaultstartdate,
                         'end-date'   => $defaultenddate
                         );
            
        
        $parentAccts = $this->queryDataAction($parrams);
        $data        = $this->dataPoints($parentAccts);
        $averages    = $this->averages($parentAccts, $data);
        $thedates    = $this->graphDates($parentAccts);

        $partnerOpts = array(
                             'dimensions' => 'ga:customVarValue2',
                             'metrics'    => 'ga:pageviews',
                             'start-date' => $defaultstartdate,
                             'end-date'   => $defaultenddate
                             );
                       
        $partnerSelects = $this->queryDataAction($partnerOpts);
        $finals = $this->formatPartners($partnerSelects);
        $options = array_unique($finals);
               
        foreach($options as $g){
                //echo options
                $partners[] =  '<option value="'.$g.'" style="font-size:8px;">'.$g.'</option>';
        } 
        
        return $this->render('AdminStatsAdminStatsBundle:Default:index.html.twig', array(
                                                                                         'data'      => $data, 
                                                                                         'startdate' => $defaultstartdate,
                                                                                         'enddate'   => $defaultenddate,
                                                                                         'average'   => $averages,
                                                                                         'partners'  => $partners,
                                                                                         'dates'     => $thedates
                                                                                         ));
    }
    
public function getDatesAction(){
        
        $getDates = Request::createFromGlobals();
	$getDates->getPathInfo();        
        
        $defaultstartdate = $getDates->request->get('startdate');
        $defaultenddate = $getDates->request->get('enddate');
       
            //Params
            $graphData = array(
                                'dimensions' => 'ga:date',
                                'metrics'    => 'ga:pageviews',
                                'start-date' => $defaultstartdate,
                                'end-date'   => $defaultenddate
                                );
            
            //gapps query
        $parentAccts = $this->queryDataAction($graphData);  
        $data        = $this->dataPoints($parentAccts);
        $averages    = $this->averages($parentAccts, $data);        
        $thedates    = $this->graphDates($parentAccts);
        
        /*trouble starts here*/
        $partnerOpts = array(
                             'dimensions' => 'ga:customVarValue2',
                             'metrics'    => 'ga:pageviews',
                             'start-date' => $defaultstartdate,
                             'end-date'   => $defaultenddate
                             );
                       
        $partnerSelects = $this->queryDataAction($partnerOpts);
        $finals = $this->formatPartners($partnerSelects);
        $options = array_unique($finals);
               
        foreach($options as $g){
                //echo options
                $partners[] =  $g;
        }  

        $parts = implode(',', $partners);

        $result = array(
                        $data,
                        $averages,
                        $thedates,
                        $parts
                        );
        
        return new Response(implode(';', $result));

    }
    
    public function getPlansAction(){
        
        $thePlan = Request::createFromGlobals();
	$thePlan->getPathInfo();
        
        $partner = $thePlan->request->get('partner');
        $defaultstartdate = $thePlan->request->get('startdate');
        $defaultenddate = $thePlan->request->get('enddate');  
        
                $partners = array(
                             'dimensions' => 'ga:customVarValue2',
                             'metrics'    => 'ga:pageviews',
                             'start-date' => $defaultstartdate,
                             'end-date'   => $defaultenddate
                             );
                
            //Params
        $partnerOpts = array( 
                             'dimensions' => 'ga:date',
                             'metrics'    => 'ga:pageviews',
                             'start-date' => $defaultstartdate,
                             'end-date'   => $defaultenddate,
                             'filters'    => 'ga:customVarValue2=@'.$partner
                             ); 
            //gapps query
    $partnerAccts = $this->queryDataAction($partners);
    $parentAccts  = $this->queryDataAction($partnerOpts);
    $values       = $this->searchSort($partnerAccts, $partner);
    $vals         = implode(',', $values);
    $newDates     = $this->graphDates($parentAccts);
    $data         = $this->dataPoints($parentAccts);

            
        $result = array(
                        $partner,
                        $vals,
                        $newDates,
                        $data
                        );
        
        return new Response(implode(';',$result));
        
    }
    
    public function comparePLansAction(){
        
        $thePlan = Request::createFromGlobals();
	$thePlan->getPathInfo();
        
        $plan = $thePlan->request->get('plan');
        $defaultstartdate = $thePlan->request->get('startdate');
        $defaultenddate = $thePlan->request->get('enddate'); 
        
        $planDetails = array( 
                             'dimensions' => 'ga:date',
                             'metrics'    => 'ga:pageviews',
                             'start-date' => $defaultstartdate,
                             'end-date'   => $defaultenddate,
                             'filters'    => 'ga:customVarValue2=='.$plan
                             );
        $planData  = $this->queryDataAction($planDetails);
        $planDates = $this->graphDates($planData);
        $planHits  = $this->dataPoints($planData);
        
        $results = array(
                         $plan,
                         $planDates,
                         $planHits
                        );
        
        return New Response(implode(";", $results));
        
    }
    
    private function searchSort($idValues, $partner){
        
            foreach($idValues['rows'] as $m){
                $plans[] = $m;
            }
  
            foreach($plans as $d){
                $theplan[] = $d[0]; 
            }

            $count = 0;
            
            for ($f = 0; $f < sizeof($theplan); $f++) {
                $count++;

                if(preg_match('/'.$partner.'/', $theplan[$f])){
                $one[] = $f;
                }
            }
            
            foreach($one as $index){
                $values[] = $idValues['rows'][$index][0];
            }
        
        return $values;
    }
    
    private function dataPoints($dPoints){
            $data = "";
            $count_rows = count($dPoints['rows']);
            
            for ($i = 0; $i < $count_rows; $i++) {
            	$data .= $dPoints['rows'][$i][1].",";
            }   
            
            $data = substr($data,0,-1);
            $data .= "";
            
            return $data;
    }

    private function averages($avData, $dPoints){
            
        $averages = "";
        $count_rows = count($avData['rows']);
        
            for ($i = 0; $i < $count_rows; $i++) {
                
                $sums = array($avData['rows'][$i][1]);
                
                $sum = array_sum($sums);
                $total = $count_rows;
                
                $average = $total / $sum;
                
            	$averages .= $average.",";
                
            }   
            
            $averages = substr($dPoints,0,-1);
            $averages .= "";
            
            return $averages;
    }

    private function graphDates($gDates){
            $thedates = "";
            $count_rows = count($gDates['rows']);
            
            for ($k = 0; $k < $count_rows; $k++) {
                    $datetime = $gDates['rows'][$k][0];
                    $yyyy = substr($datetime,0,4);
                    $mm = substr($datetime,4,2);
                    $monthName = date("M", mktime(0, 0, 0, $mm, 10));                    	
                    $dd = substr($datetime,6,2);
                    $dt_formatted = $monthName."-".$dd;                   	         	
                    $thedates .= "'".$dt_formatted."'".",";
                }            
 
                $thedates = substr($thedates,0,-1);
                $thedates .= "";
                
                return $thedates;
    }
    
    private function formatPartners($partners){
        
           foreach($partners['rows'] as $myKey){
            //Isolate and format values for options.
            $levOne   = $myKey[0];
            $unwanted = array('{', '}', '$');
            $levTwo   = str_replace($unwanted, '', $levOne);
            $levThree = preg_replace('/[0-9]/', '', $levTwo);
            $finals[] = $levThree;
        }
        
        return $finals;
    }

    private function queryDataAction($x){
        
        $ga = new GoogleAnalyticsAPI('service');
        $ga->auth->setClientId('891041475744-ttakqc55rr0d0k2so2likptb554bfck7.apps.googleusercontent.com');
        $ga->auth->setEmail('891041475744-ttakqc55rr0d0k2so2likptb554bfck7@developer.gserviceaccount.com');
        $ga->auth->setPrivateKey('../src/Plans/PlansBundle/Controller/ga/spe_analytics.p12');

        $auth = $ga->auth->getAccessToken();

        // Try to get the AccessToken
        if ($auth['http_code'] == 200) {
                $accessToken = $auth['access_token'];
                $tokenExpires = $auth['expires_in'];
                $tokenCreated = time();
        } else {
                echo "Session Expired.";
        }
        
        // Set the accessToken and Account-Id
        $ga->setAccessToken($accessToken);
        $ga->setAccountId('ga:68222810');
        
        return $ga->query($x);
    }
    
     public function init()
    {
           $session = $this->get('adminsession');
           $session->set("section","Stats");
           $session->set("currentpage","AdminStats");
    }
    
}
