<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PingController extends Controller
{
    public function echoAction(Request $request)
    {
        $echo = $request->headers->get('echo');
        return new Response(json_encode(['echo' => $echo]));
    }
}