<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use classes\ClassBundle\Entity\ApiWhitelist;
use classes\ClassBundle\Entity\ApiUser;
use classes\classBundle\Entity\IrioSession;
use WidgetsBundle\Services\FormulaService;

class IrioController extends Controller
{
    protected $sid = "test";
    public function testWidgetAction(Request $request)
    {
        $header = $request->headers;
        $header->set("age",28);
        $header->set("balance",1000);
        $header->set("maxMatchDollar",2000);
        $header->set("compensationTotals",5000);
        $header->set("matchTier1Percent",1);
        $header->set("matchTier1Cap",2);
        $header->set("matchTier2Percent",3);
        $header->set("matchTier2Cap",4);   
        $header->set("partnerId",$request->query->get("partnerId","AMRTS_OMNIAMRTS_PROD"));
        $header->set("planID",$request->query->get("planID","307511"));
        $header->set("planName","donkey kong is good for you");
        $header->set("retirementAge",65);
        $header->set("autoEnrollAvailable",1);
        $header->set("partID","abcdefg");
        $header->set("firstName","Akira");
        $header->set("lastName","Kurusu");
        $header->set("dateOfBirth","04/01/1986");
        $header->set("partStatus","66");
        $header->set("balance",33333);
        $header->set("allowcontributionchange",1);
        $header->set("premaxDeferralPercent",5);
        $header->set("preTaxSourceType","preTax");
        $header->set("preTaxSource","A");        
        $header->set("preTaxName","AA");
        $header->set("preTaxCurrentPercent",1);
        $header->set("preTaxDefaultPercent",2);
        $header->set("preTaxCurrentYTD",100);
        $header->set("rothSource","B");        
        $header->set("rothSourceType","roth");
        $header->set("rothName","BB");
        $header->set("rothCurrentPercent",3);
        $header->set("rothDefaultPercent",4); 
        //irio doesn't transact postTax yet, prob doesn't matter 
        $header->set("postTaxSource","C");        
        $header->set("postTaxSourceType","C");
        $header->set("postTaxCurrentPercent",5);
        $header->set("postTaxDefaultPercent",6);
        $header->set("postTaxCurrentYTD",300);     
        $header->set("matchTier1Percent",5); 
        $header->set("matchTier1Cap",10);
        $header->set("matchTier2Percent",15); 
        $header->set("matchTier2Cap",20);    
        $header->set("matchCurrentYTD",400);
        $header->set("sid","rawr");
        $header->set("user_id",9001);
        $header->set("partID","riversinthedesert");
        $header->set("phone","0123456789");
        $session = $this->get("session");
        $session->set("header",$header);
        $this->convertHeaderSessionToRkpSession();
        return $this->render("ApiBundle:Irio:loader.html.twig");
    }
    public function testWidgetFullAction()
    {
        return $this->render("ApiBundle:Irio:test.html.twig");  
    }
    public function widgetAction(Request $request)
    {
        $header = $request->request;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('planid' => $header->get("planID"),"partnerid" => $header->get("partnerId"),"deleted" => 0));
        if (is_null($plan)) {
            return new Response(json_encode(["html" => $this->render('WidgetsBundle:Irio:error.html.twig')->getContent()]));
        }
        $session = $this->get("session");
        $session->set("header",$header);
        $this->convertHeaderSessionToRkpSession();
        $this->get('FormulaService')->getData();  
        $this->saveSessionToTable();
        $data = $this->get('FormulaService')->getData();
        $data['lang'] = "en";
        $data['viewOnly'] = $header->get('viewOnly') == 'yes' ? true : false;
        $tpl = [
            'data' => $data,
        ];
        return new Response(json_encode(["html" => $this->render('WidgetsBundle:Irio:iriobody2.html.twig', $tpl)->getContent()]));
    }
    public function convertHeaderSessionToRkpSession()
    {
        $header = $this->get("session")->get("header");
        $rkp = [];
        $planData = new \stdClass();
        $planData->firstName = $header->get("firstName");
        $planData->lastName = $header->get("lastName");
        $planData->email = $header->get("email");        
        $planData->planName = $header->get("planName");
        $planData->dateOfBirth = $header->get("dateOfBirth");
        $planData->phone = $header->get("phone");
        $planData->totalBalance = $header->get("balance");
        $planData->contributionbysource = new \stdClass();
        $planData->contributionbysource->pre =  $header->get("preTaxCurrentYTD");
        $planData->contributionbysource->match =  $header->get("matchCurrentYTD");
        $planData->contributionbysource->post =  $header->get("postCurrentYTD");
        $planData->planAllowsDeferralsWidget = strtolower($header->get("allowcontributionchange")) == "yes";
        $planData->rkpExtras = new \stdClass();
        if (!empty((float)$header->get("preTaxCurrentAmount")) || !empty((float)$header->get("rothCurrentAmount")))
        {
            $planData->preCurrent = (float)$header->get("preTaxCurrentAmount");
            $planData->rothCurrent = (float)$header->get("rothCurrentAmount");
            $planData->partDeferralSetting = "dollar";
            $planData->preMax =  (float)$header->get("premaxDeferralAmount");
        }
        else
        {
            $planData->preCurrent = (float)$header->get("preTaxCurrentPercent");
            $planData->rothCurrent = (float)$header->get("rothCurrentPercent"); 
            $planData->partDeferralSetting = "percent";
            $planData->preMax =  (float)$header->get("premaxDeferralPercent");
        }
        $maxType = "pre";
        $planData->maxDeferralPercent = (float)$header->get("premaxDeferralPercent");
        $planData->maxDeferralAmount = (float)$header->get("premaxDeferralAmount");
        if (empty($planData->maxDeferralPercent) && empty($planData->maxDeferralAmount) && (!empty($header->get("rothmaxDeferralPercent")) || !empty($header->get("rothmaxDeferralAmount")) ))
        {
            $planData->maxDeferralPercent = (float)$header->get("rothmaxDeferralPercent");
            $planData->maxDeferralAmount = (float)$header->get("rothmaxDeferralAmount");
            $maxType = "roth";
        }    
                        
        $planData->partStatus = $header->get("partStatus");
        $planData->user_id = $header->get("user_id");
        
        $planData->partACAStatus = false;
        
        if (strtolower($header->get("autoEnrollAvailable")) == "yes")
        {
            foreach (["preTaxDefaultPercent","rothDefaultPercent","postTaxDefaultPercent"] as $type)
            {
                $planData->partACAStatus  = $planData->partACAStatus  || (float)$header->get($type) > 0;
            }
        }
        $planData->rkpExtras->hasroth = !(empty((int)$planData->rothCurrent)) || $maxType == "roth";
        $matchInformation = new \stdClass();        
        $matchInformation->maxmatchdollar = $header->get("maxMatchDollar");
        $matchInformation->compensationtotals = $header->get("compensationTotals");
        $matchInformation->matchdetails = new \stdClass();
        $matchInformation->matchdetails->matchstep1 = $header->get("matchTier1Percent");
        $matchInformation->matchdetails->matchrate1 = $header->get("matchTier1Cap");
        $matchInformation->matchdetails->matchstep2 = $header->get("matchTier2Percent");
        $matchInformation->matchdetails->matchrate2 = $header->get("matchTier2Cap");

        // ignore case on payroll frequency due to inconsistent parameter name passing
        // if we don't find payrollFrequency, we try payrollfrequency
        $payrollFrequency = $header->get('payrollFrequency');
        if ($payrollFrequency == null) {
            $payrollFrequency = $header->get('payrollfrequency');
        }
        $matchInformation->cycle = strtoupper($payrollFrequency);

        $planData->matchinformation = $matchInformation;
        $rkp['participant_uid'] = $header->get("partID");        
        $rkp['plan_data'] = $planData;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $rkp['plan_id']= $repository->findOneBy(array('planid' => $header->get("planID"),"partnerid" => $header->get("partnerId"),"deleted" => 0))->id;
        $this->get("session")->set("rkp",$rkp);
        
        $requestSession = [];
        $requestSession["partner_id"] = $header->get("partnerId");
        $requestSession["planid"]  = $header->get("planID");
        $requestSession["sid"]  =$header->get("sid");      
        $requestSession["part_id"] = $header->get("partID");
        $requestSession['lang'] = "en";
        $requestSession["type"]  = "irio";
        if (!empty($header->get("lang")))
        {
            $requestSession['lang'] = $header->get("lang");
        }
        $requestSession['type']  = "irio";
        $this->get("session")->set("testingTrans",!empty($header->get("testing")));
        $this->get("session")->set("REQUEST",$requestSession );

        $plansRkp = new \stdClass();
        $plansRkp->retirementAge = 65;
        if (!empty($header->get("retirementAge")))
        {
            $plansRkp->retirementAge = $header->get("retirementAge");
        }
        if (!empty($header->get("retirementage")))
        {
            $plansRkp->retirementAge = $header->get("retirementage");
        }       
        $this->get("session")->set("plansRkp",$plansRkp);
        $this->get("session")->set("widgetHeader",1);
        $this->get("session")->set('api_url',$this->container->getParameter( 'api_url' ));
        $common = [];
        $common['roth'] = $planData->rkpExtras->hasroth;
        $this->get("session")->set("common",$common);
        $this->get("session")->set("cta","irio");
        $this->sid = $header->get("sid");
        $fields = ["preTaxCurrent","rothCurrent","postTaxCurrent"];
        $percentDeferral = $dollarDeferral = false;
        $nonNumericDeferral = true;
        foreach ($fields as $field)
        {     
            $percentDeferral = $percentDeferral || (float)$header->get($field."Percent") > 0;
            $dollarDeferral = $dollarDeferral || (float)$header->get($field."Amount") > 0;
            
            $nonNumericDeferral = $nonNumericDeferral && !is_numeric($header->get($field."Percent")) && !is_numeric($header->get($field."Amount"));
        }
        $percentDeferralMax = $dollarDeferralMax = false;
        $deferralOptionType = "pre";
        foreach (["pre","roth"] as $field)
        {
            $percentDeferralMax = $percentDeferralMax || (float)$header->get($field."maxDeferralPercent") > 0;
            $dollarDeferralMax = $dollarDeferralMax || (float)$header->get($field."maxDeferralAmount") > 0;
            if ($percentDeferralMax  || $dollarDeferralMax)
            {        
                $deferralOptionType = $field;
                break;
            }
        }
        $this->get("session")->set("percentDeferralMax",$percentDeferralMax && !$dollarDeferralMax);
        $this->get("session")->set("dollarDeferralMax",$dollarDeferralMax && !$percentDeferralMax);
        $this->get("session")->set("deferralOptionType",$deferralOptionType);
        $this->get("session")->set("mixedDeferral",$percentDeferral && $dollarDeferral);
        $this->get("session")->set("deferralOption",$percentDeferralMax && $dollarDeferralMax);
        $this->get("WidgetService")->setLang();
        $this->get("session")->set("contributionAmountChoice",$nonNumericDeferral);

    }
    public function saveSessionToTable()
    {
            if (!empty($this->get("session")->get("cta")) &&  strtolower($this->get("session")->get("cta")) != "irio"  )
                return;
            $irioSession = new IrioSession();
            $irioSession->sid = $this->sid;
            $irioSession->data =  serialize($this->get("session")->all());
            $em = $this->getDoctrine()->getManager();
            $em->persist($irioSession);
            $em->flush();     
    }
    public function readSessionFromTable()
    {
        $session = $this->get("session");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:IrioSession');
        $irioSession = $repository->findOneBy(["sid" => $this->sid],["id" => "desc"]);
        $sessionData = unserialize($irioSession->data);
        foreach ($sessionData as $key => $row)
        {
            $session->set($key,$row);
        } 
    }
    public function renderAction(Request $request)
    {
        $session = $this->get("session");
        if (!empty($request->request->get("sid",null)))
        {
            $this->sid = $request->request->get("sid",null);
        }
        $this->readSessionFromTable();
        $request2 = $session->get("REQUEST");
        $request2['lang'] = $request->request->get("lang","en");
        $this->get("session")->set("REQUEST",$request2);
        $this->get("WidgetService")->setLang();
        $data = $session->get("DATA");
        $data['lang'] = $request2['lang'];
        $data['viewOnly'] = $session->get('header')->get('viewOnly') == 'yes' ? true : false;
        $this->saveSessionToTable();
        $tpl = [
            'data' => $data
        ];
        return $this->render('WidgetsBundle:Irio:iriobody2.html.twig', $tpl);
    }
    public function widgetOldAction(Request $request)
    {
        $formulaService = $this->get('FormulaService');

        $data = $this->callFormulaService($request, $formulaService);
        $amount = $data['projectedMonthlyRetirement_0'];

        $html = $this->get('twig')->render('ApiBundle:Irio:widget.html.twig', $data);
        $this->get("session")->set("application","irio");
        return new Response(json_encode(['html' => $html]));
    }

    public function formulaAction(Request $request)
    {
        $formulaService = $this->get('FormulaService');

        $returnData = $this->callFormulaService($request, $formulaService);
        if ($returnData['currentAge'] > $returnData['retirementAge'])
            return new Response(json_encode(["amount"=> 0]));

        $amount = $returnData['projectedMonthlyRetirement_0'];

        return new Response(json_encode(['amount' => $amount]));
    }

    public function callFormulaService(Request $request, FormulaService $formulaService)
    {
        $age = $request->headers->get('age');
        $preCurrent = $request->headers->get('preCurrent');
        $rothCurrent = $request->headers->get('rothCurrent');
        $balance = $request->headers->get('balance');
        $maxMatchDollar = $request->headers->get('maxMatchDollar');
        $compensationTotals = $request->headers->get('compensationTotals');
        $matchTier1Percent = $request->headers->get('matchTier1Percent');
        $matchTier1Cap = $request->headers->get('matchTier1Cap');
        $matchTier2Percent = $request->headers->get('matchTier2Percent');
        $matchTier2Cap = $request->headers->get('matchTier2Cap');
        $salary = $request->headers->get('salary');

        $data = [];
        $data['age'] = $age;
        $data['preCurrent'] = $preCurrent;
        $data['rothCurrent'] = $rothCurrent;
        $data['balance'] = $balance;
        $data['maxmatchdollar'] = $maxMatchDollar;
        $data['retireAge'] = 65;
        $data['salary'] = $salary;
        $data['balance'] = $balance;
        $data['matchTier1Percent'] = $matchTier1Percent;
        $data['matchTier1Cap'] = $matchTier1Cap;
        $data['matchTier2Percent'] = $matchTier2Percent;
        $data['matchTier2Cap'] = $matchTier2Cap;

        $em = $this->container->get('doctrine')->getManager();
        $repository = $em->getRepository('classesclassBundle:Apr');
        $apr = $repository->findOneBy(array('age' => $data['retireAge'], 'year' => date('Y')));

        $data['apr'] = isset($apr->apr) ? $apr->apr : 0;
        $data['apr_join'] = isset($apr->apr_join) ? $apr->apr_join : 0;

        $matchInformation = new \stdClass();
        $matchInformation->maxmatchdollar = $maxMatchDollar;
        $matchInformation->compensationtotals = $compensationTotals;
        $matchInformation->matchdetails = new \stdClass();
        $matchInformation->matchdetails->matchstep1 = $matchTier1Percent;
        $matchInformation->matchdetails->matchrate1 = $matchTier1Cap;
        $matchInformation->matchdetails->matchstep2 = $matchTier2Percent;
        $matchInformation->matchdetails->matchrate2 = $matchTier2Cap;
        $data['matchInformation'] = $matchInformation;

        $formulaService->_calcMatch($data);
        $returnData = $formulaService->_calculateNew($data);
        
        return $returnData;
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
       parent::setContainer($container);
       $this->sid = $this->get("session")->getId();
    }

}