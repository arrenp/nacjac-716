<?php

namespace ApiBundle\Controller;

use classes\classBundle\Entity\ApiLog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use classes\classBundle\Entity\ApiWhitelist;
use classes\classBundle\Entity\ApiUser;
use classes\classBundle\Entity\ApiToken;
use classes\classBundle\Entity\ApiTokenTtl;
use Symfony\Component\Security\Acl\Exception\Exception;

class DefaultController extends Controller
{
    
    public function indexAction($name)
    {
        return $this->render('ApiBundle:Default:index.html.twig', array('name' => $name));
    }

    protected function isSessionTokenPresent($token)
    {
        return ($token != null);
    }

    protected function returnToken(Request $request, \Memcached $memcache, EntityManager $entityManager, $accountId, $apiId, $apiUserId)
    {
        $tokenTtl = $entityManager->getRepository('classesclassBundle:ApiTokenTtl');
        $ttlData = $tokenTtl->findOneBy(['id' => 1]);
        $seconds = 300;
        if ($ttlData) {
            $seconds = $ttlData->getTtl();
        }

        $token = uniqid('api_', true);
        $this->get("session")->set("token",$token);

        $values = ['apiId' => $apiId, 'apiUserId' => $apiUserId, 'accountId' => $accountId];
        $memcache->set($token, $values, $seconds);

        return $this->returnResponse($request, $entityManager, json_encode(['token' => $token]));
    }

    protected function isSessionTokenExpired(\Memcached $memcache, $token)
    {
        $value = $memcache->get($token);

        if ($value) {
            return false;
        } else {
            return true;
        }
    }

    protected function getApiUserId(\Memcached $memcache, $token)
    {
        $apiUserId = null;
        $value = $memcache->get($token);
        if ($value) {
            $apiUserId = $value['apiUserId'];
        }
        return $apiUserId;
    }

    protected function getApiId(EntityManager $entityManager, $controller, $method)
    {
        $apiId = false;
        $apis = $entityManager->getRepository('classesclassBundle:Apis');
        $api = $apis->findOneBy(['controller' => $controller, 'method' => $method]);
        if ($api) {
            if (!$api->getEnabled()) {
                $apiId = $api->getId();
            } else {
                $apiId = true;
            }
        }
        return $apiId;
    }

    protected function isCallAllowedForAccount(EntityManager $entityManager, $apiId, $apiUserId)
    {
        $isCallAllowed = false;
        $apiUserAuths = $entityManager->getRepository('classesclassBundle:ApiUserAuth');
        $userAuth = $apiUserAuths->findOneBy(['apiId' => $apiId, 'apiUserId' => $apiUserId]);
        if ($userAuth) {
            if (!$userAuth->getEnabled()) {
                $isCallAllowed = false;
            } else {
                $isCallAllowed = true;
            }
        }
        return $isCallAllowed;
    }

    // TODO add logic here
    protected function doesCallExceedRateLimit()
    {
        return false;
    }

    protected function isCallAllowedUnauthorized($controller, $method)
    {
        if (($controller == 'login') && ($method == 'auth')) {
            return true;
        }
        return false;
    }

    protected function areUsernameAndPasswordValid(EntityManager $entityManager, $username, $password)
    {
        $valid = false;
        $users = $entityManager->getRepository('classesclassBundle:ApiUser');
        $user = $users->findOneBy(['username' => $username, 'password' => $password]);
        if ($user) {
            $valid = true;
        }
        return $valid;
    }

    protected function getAccountAndApiUserIds(EntityManager $entityManager, $username, $password)
    {
        $accountId = null;
        $apiUserId = null;
        $users = $entityManager->getRepository('classesclassBundle:ApiUser');
        $user = $users->findOneBy(['username' => $username, 'password' => $password]);
        if ($user) {
            $accountId = $user->getAccountId();
            $apiUserId = $user->getId();
        }
        return [$accountId, $apiUserId];
    }

    protected function logCall(Request $request, EntityManager $entityManager, $content, $status, $apiId = null, $apiUserId = null)
    {
        $log = new ApiLog();

        $log->setApiId($apiId);
        $log->setApiUserId($apiUserId);
        $log->setHeaders("Request: " . json_encode($request->request->all()) . "\nHeaders: " . json_encode($request->headers->all()));
        $log->setIp($request->getClientIp());
        $log->setResponse($content);
        $log->setStatus($status);
        $log->setUrl($request->getRequestUri());
        $log->setTimestamp(new \DateTime());
        $log->setToken($this->get("session")->get("token"));
        $entityManager->persist($log);
        $entityManager->flush();
        $this->get("session")->set("apiLogId",$log->getId());
    }

    protected function returnResponse(Request $request, EntityManager $entityManager, $content = '', $status = 200)
    {
        $this->logCall($request, $entityManager, $content, $status);
        if (!empty($this->get("session")->get("application")))
        {
            $this->updateXml($this->get("session")->get("application"));
        }
        return new Response($content, $status);
    }

    protected function returnForbidden(Request $request, EntityManager $entityManager, $content = 'Forbidden', $status = 403)
    {
        return $this->returnResponse($request, $entityManager, $content, $status);
    }

    protected function returnUnauthorized(Request $request, EntityManager $entityManager, $content = 'Unauthorized', $status = 401)
    {
        return $this->returnResponse($request, $entityManager, $content, $status);
    }

    protected function returnNotFound(Request $request, EntityManager $entityManager, $content = 'Not Found', $status = 404)
    {
        return $this->returnResponse($request, $entityManager, $content, $status);
    }

    protected function returnTooManyRequests(Request $request, EntityManager $entityManager, $content = 'Too Many Requests', $status = 429)
    {
        return $this->returnResponse($request, $entityManager, $content, $status);
    }

    protected function returnServerError(Request $request, EntityManager $entityManager, $content = 'Server Error', $status = 500)
    {
        return $this->returnResponse($request, $entityManager, $content, $status);
    }

    protected function returnOk(Request $request, EntityManager $entityManager, $content = 'OK', $status = 200)
    {
        return $this->returnResponse($request, $entityManager, $content, $status);
    }

    protected function processAPICall(Request $request, $controller, $method)
    {
        $controller = ucfirst($controller);
        $response = $this->forward("ApiBundle:{$controller}:{$method}", ['request' => $request]);
        return $response->getContent();
    }

    public function processApiCallAction(Request $request)
    {
        $memcache = new \Memcached();
        if (!count($memcache->getServerList())) {
            $memcache->addServer('127.0.0.1', 11211);
        }

        $entityManager = $this->container->get('doctrine')->getManager();

        $token = $request->headers->get('token');
        if (empty($token))
            $token = $request->request->get('token');       
        $this->get("session")->set("token",$token);
        $uri = $request->getRequestUri();
        list(, , $controller, $method) = explode('/', $uri);

        $apiId = $this->getApiId($entityManager, $controller, $method);
        if (!$this->isSessionTokenPresent($token)) {
            if ($this->isCallAllowedUnauthorized($controller, $method)) {
                $username = $request->request->get('username');
                $password = $request->request->get('password');
                if (empty($username) && empty($password)) {
                    $username = $request->headers->get('username');
                    $password = $request->headers->get('password');
                }
                if (!$this->areUsernameAndPasswordValid($entityManager, $username, $password)) {
                    return $this->returnUnauthorized($request, $entityManager);
                } else {
                    list($accountId, $apiUserId) = $this->getAccountAndApiUserIds($entityManager, $username, $password);
                    return $this->returnToken($request, $memcache, $entityManager, $accountId, $apiId, $apiUserId);
                }
            } else {
                return $this->returnUnauthorized($request, $entityManager);
            }
        } else {
            if (!$this->isSessionTokenExpired($memcache, $token)) {
                $apiUserId = $this->getApiUserId($memcache, $token);
                if ($apiId) {
                    if ($this->isCallAllowedForAccount($entityManager, $apiId, $apiUserId)) {
                        if (!$this->doesCallExceedRateLimit()) {
                            $content = $this->processApiCall($request, $controller, $method);
                            return $this->returnResponse($request, $entityManager, $content);
                        } else {
                            return $this->returnTooManyRequests($request, $entityManager);
                        }
                    } else {
                        return $this->returnForbidden($request, $entityManager);
                    }
                } else {
                    return $this->returnNotFound($request, $entityManager);
                }
            } else {
                return $this->returnUnauthorized($request, $entityManager);
            }
        }
    }

    public function updateXml($application)
    {
        $entityManager = $this->container->get('doctrine')->getManager();
        $repo = $entityManager->getRepository('classesclassBundle:ApiLog');
        $log = $repo->findOneBy(['id' => $this->get("session")->get("apiLogId")]);
        $log->setApplication($application);
        $entityManager->flush();
    }
}
