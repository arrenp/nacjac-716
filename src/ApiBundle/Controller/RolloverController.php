<?php

namespace ApiBundle\Controller;

use classes\classBundle\Entity\participants;
use classes\classBundle\Entity\profiles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Spe\AppBundle\Templating\Helper\Functions;

class RolloverController extends Controller
{
    public function widgetAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $firstName = $request->request->get('firstName');
        $lastName = $request->request->get('lastName');
        $ssn = $request->request->get('partID');
        $planid = $request->request->get('planID');
        $language = $request->request->get('lang');
        $email = $request->request->get("email");

        $participants = new participants();
        $participants->setFirstName($firstName);
        $participants->setLastName($lastName);
        $participants->setEmployeeId($ssn);
        $em->persist($participants);

        $profiles = new profiles();
        $profiles->setParticipant($participants);
        $profiles->setReportDate(null);
        $profiles->setPlanid($planid);
        $profiles->setProfileId(uniqid('RLO_'));

        $em->persist($profiles);
        $em->flush();
        $profilesId = $profiles->getId();

        if ($language == 'es') {
            $this->get('translator')->setLocale('es');
        }

        $html = $this->get('twig')->render('ApiBundle:Rollover:widget.html.twig', array('id' => $profilesId, 'language' => $language, 'email' => $email));

        return new Response(json_encode(['html' => $html]));
    }


}