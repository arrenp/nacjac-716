<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * Ajax controller.
 * @Route("/playlist")
 */
class PlayListController extends Controller
{   
    /**
     * @Route("/load/{playlist}/", name="loadPlayList")
     */
    public function loadAction($playlist)
    {
        $playListFindOneBy = ["name" => $playlist,"pathId" => $this->get("session")->get("pathId")];
        $playList = $this->ps->getPlayListFull($playListFindOneBy);
        if (empty($playList->id))
            $this->ps->addPlayList(["pathId" => $this->get("session")->get("pathId"),"name" => $playlist]);
        $playList = $this->ps->getPlayListFull($playListFindOneBy);
        return $this->render("playlist.html.twig",["playList" => $playList]);
    } 
    /**
     * @Route("/addPlayList", name="addPlayListVideo")
     */
    public function addPlayListVideoAction(Request $request)
    {
        $playList = $this->ps->getPlayList(["id" => $request->query->get("playListId")]);
        return $this->render("addvideo.html.twig",["playList" => $playList]);
    }  
    /**
     * @Route("/addPlayList/saved", name="addPlayListVideoSaved")
     */
    public function addPlayListVideoSavedAction(Request $request)
    {
       $this->ps->addVideo($request->request->all());
       return new Response("");
    }  
    /**
     * @Route("/editPlayListVideo", name="editPlayListVideo")
     */
    public function editPlayListVideoAction(Request $request)
    {
        $video = $this->ps->getVideo(["id" => $request->query->get("id")]);
        $sectionName = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:AppPlayList')->findOneBy(array('id' => $video->playListId))->name;
        $isPB = ($sectionName === "planBasics" ? true : false);
        if ($isPB)
            $appModules = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:AppModule')->findAll();
        return $this->render("editvideo.html.twig",["video" => $video, "isPB" => $isPB, "appModules" => $appModules]);

    }
    /**
     * @Route("/getModuleSelection", name="getModuleSelection")
     */
    public function getModuleSelectionAction(Request $request)
    {
        $module = $request->request->get('module');
        $moduleid = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:AppModule')->findOneBy(array('module' => $module))->id;
        $sections = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:AppModuleSelection')->findBy(array('moduleid' => $moduleid));
        return new JsonResponse($sections);
    }
    /**
     * @Route("/editPlayList/saved", name="editPlayListVideoSaved")
     */
    public function editPlayListVideoSavedAction(Request $request)
    {
       $this->ps->editVideo(["id" => $request->request->get("id")],$request->request->all());
       return new Response("");
    } 
    /**
     * @Route("/deletePlayListVideo", name="deletePlayListVideo")
     */
    public function deletePlayListVideoAction(Request $request)
    {
        $video = $this->ps->getVideo(["id" => $request->query->get("id")]);
        return $this->render("deletevideo.html.twig",["video" => $video]);
    }  
    /**
     * @Route("/deletePlayListVideo/saved", name="deletePlayListVideoSaved")
     */
    public function deletePlayListVideoSavedAction(Request $request)
    {
        $this->ps->deleteVideo(["id" => $request->request->get("id")]);
    } 
    /**
     * @Route("/saveVideoOrder", name="saveVideoOrder")
     */
    public function saveVideoOrderAction(Request $request)
    {
        $this->ps->reorderVideos($request->request->get("playListId"),$request->request->get("ids"));
        return new Response("");
    }  
    /**
     * @Route("/loadPlayListTable", name="loadPlayListTable")
     */
    public function loadPlayListTableAction(Request $request)
    {
        $playList = $this->ps->getPlayListFull(["id" => $request->request->get("id")]);
        return $this->render("loadplaylist.html.twig",["playList" => $playList]);
    }  
    /**
     * @Route("/playList/ManageLocales", name="playListManageLocales")
     */
    public function playListManageLocalesAction(Request $request)
    {
        $video = $this->ps->getVideoFull(["id" => $request->query->get("id")]);
        $languages = $this->get("LanguagesService")->getAll("id");
        $playList = $this->ps->getPlayList(["id" => $video->playListId]);
        foreach ($languages as &$language)
        {
            $language->url = "";
            $language->subtitleUrl = "";
            $language->videos = $this->ps->getPlayListVideos(["languageId" => $language->id,"name" => $playList->name]);
        }
        foreach ($video->locales as $locale)
        {
            $languages[$locale->languageId]->url = $locale->url;
            $languages[$locale->languageId]->subtitleUrl = $locale->subtitleUrl;
        }
        return $this->render("managelocales.html.twig",["video" => $video,"languages" =>$languages]);
    }  
    /**
     * @Route("/playList/ManageLocales/saved", name="playListManageLocalesSaved")
     */
    public function playListManageLocalesSavedAction(Request $request)
    {
        $this->ps->addLocalesToVideo($request->request->get("locales"));
        return new Response("");
    }  
    /**
     * @Route("/playList/ManageLocales/updateInputs", name="playListManageLocalesUpdateInputs")
     */
    public function playListManageLocalesUpdateInputsAction(Request $request)
    {
        $video = $this->ps->getPlayListVideo(["id" => $request->request->get("id")]);
        return new JsonResponse($video);
    }      
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->ps = $this->get('PlayListService');
        $this->ps->setId($this->get("session")->get("masterid"));
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:playlist/".$file,$params);
    } 
}

