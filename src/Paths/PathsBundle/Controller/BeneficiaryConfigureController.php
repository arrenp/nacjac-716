<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/configure/investments/")
 */  
class BeneficiaryConfigureController extends BaseConfigureController
{   
    /**
     * @Route("/configure", name="_paths_configureInvestments")
     */
    public function configureAction()
    {
        return $this->renderWithConfigure("beneficiary.html.twig");
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:configure/beneficiary/".$file,$params);
    }      
}
