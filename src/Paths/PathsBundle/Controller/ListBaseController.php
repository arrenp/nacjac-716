<?php

namespace Paths\PathsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListBaseController extends Controller
{ 
    public function unsearchableArray($headerArray)
    {
        $types = $this->ps->getPathTypes();
        $unsearchableArray = [];
        $unsearchableArrayIndex = count($headerArray);
        foreach ($types as $type)
        {
            $unsearchableArray[] = $unsearchableArrayIndex;
            $unsearchableArrayIndex++;
        }
        $unsearchableArray[] = $unsearchableArrayIndex;
        return $unsearchableArray;
    }
    public function header($headerArray)
    {
        $types = $this->ps->getPathTypes();
        foreach ($types as $type)
            $headerArray[] = $type->description;    
        $headerArray[] = "Risk Profile";
        $header = "";
        foreach ($headerArray as $headerSection)
        $header = $header."<th>".$headerSection."</th>";    
        return $header;
    }
    public function addRisk($type,$entity,&$row)
    {
        $row[]  = parent::render("PathsPathsBundle:Default:addRow.html.twig",["type" => $type,"entity" => $entity])->getContent();
    }
    public function addTypeRows($entity,$types,&$row,$disabled = false)
    {
        $entityPaths = $this->ps->getPaths($entity);
        foreach ($types as $type)
        {
            $paths = $this->ps->getPathsMulti(["pathTypeId" => $type->id,"active" => 1]);
            $paths = $this->filterTypeRowPaths($paths,$entity);
            $selectedPath = null;
            if (isset($entityPaths[$type->type]))
                $selectedPath = $entityPaths[$type->type];
            $disabledValue = $entity->{$type->type."PathId"} == -1;
            $row[] = parent::render("PathsPathsBundle:Default:typesSelect.html.twig",["paths" => $paths,"selectedPath" => $selectedPath,"entity" => $entity,"type" => $type,"disabled" => $disabled,"disabledValue" => $disabledValue ])->getContent();
        }
    }
    public function filterTypeRowPaths($paths,$entity)
    {
        return $paths;
    }
}
