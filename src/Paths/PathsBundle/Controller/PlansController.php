<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PlansController extends ListBaseController
{ 
    /**
     * @Route("/plans", name="_paths_plans")
     */
    public function plansAction()
    {
        $headerArray = ["id","partnerid","planid"];        
        $params = 
        [
            "header" => $this->header($headerArray),
            "unsearchableArray" => $this->unsearchableArray($headerArray)
        ];
        return $this->render("plans.html.twig",$params);
    } 
    /**
     * @Route("/plans/getPlans", name="_paths_plans_getplans")
     */
    public function getPlansAction(Request $request)
    {
        $queryBuilder  = $this->get("QueryBuilderDataTables")->queryBuilder("plans");              
        $queryBuilder->where("a.deleted = :deleted");
        $queryBuilder->setParameter("deleted",0);
        $result = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder("plans",["a.id","a.partnerid","a.planid"],$request->request->all(),$queryBuilder);
        $data = [];
        $types = $this->ps->getPathTypes();
        foreach ($result['rows'] as $plan)
        {
            $row = [
                $plan->id,
                $plan->partnerid,
                $plan->planid
            ];
            $this->addTypeRows($plan,$types,$row,true);
            $this->addRisk("Plan",$plan,$row);
            $data[] = $row;
        }      
        $response = $result['response'];
        $response['data'] = $data;       
        return new JsonResponse($response);
    }
    /**
     * @Route("/plans/savePaths", name="_paths_plans_savepaths")
     */    
    public function savePaths(Request $request)
    {
        $paths = $request->get("paths");
        $i = 1;
        $em = $this->getDoctrine()->getManager();
        foreach ($paths as $path)
        {
            $this->ps->setPlanPaths(['id' => $path['id']],$path);
            if ($i % 100 == 0)
            $em->flush();    
            $i++;
        }
        $em->flush();
        return new Response("");
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->ps = $this->get('PathsService');
        $this->ps->setId($this->get("session")->get("masterid"));
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:plans/".$file,$params);
    }     
    public function filterTypeRowPaths($paths,$entity)
    {
        foreach ($paths as $key => $path)
        {
            $params = ['pathId' =>  $path->id, "userid" => $entity->userid ];
            if (!$this->ps->getPathAccount($params) && !$path->general)
                unset($paths[$key]);
        }
        return $paths;
    }
}