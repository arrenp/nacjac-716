<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/configure/contributions/")
 */  
class ContributionsConfigureController extends BaseConfigureController
{   
    /**
     * @Route("/configure", name="_paths_configureContributions")
     */
    public function configureAction()
    {
        return $this->renderWithConfigure("contributions.html.twig");
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:configure/contributions/".$file,$params);
    }      
}

