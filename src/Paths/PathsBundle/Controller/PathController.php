<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PathController extends Controller
{
    /**
     * @Route("/", name="_paths_index")
     */
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Paths");
        
        return $this->render('index.html.twig');
    }
    /**
     * @Route("/getPaths", name="_paths_getPaths")
     */
    public function getPathsAction(Request $request)
    {
        $data = $this->get("QueryBuilderDataTables")->jsonObject("Path",["a.id","a.path"],$request->request->all());
        $htmlRows = [];
        foreach ($data['rows'] as $path)
        {
            $basePath = "getPaths/";
            $htmlRows[] = 
            [
                $path->id,
                $path->path,
                $this->render($basePath."active.html.twig",["path" => $path])->getContent(),
                $this->render($basePath."edit.html.twig",["path" => $path])->getContent(),
                $this->render($basePath."setaccounts.html.twig",["path" => $path])->getContent(),
                $this->render($basePath."copypath.html.twig",["path" => $path])->getContent(),
                $this->render($basePath."configure.html.twig",["path" => $path])->getContent(),
                $this->render($basePath."delete.html.twig",["path" => $path, "id" => $path->id])->getContent()
                
            ];
        }
        
        $data['response']['data'] = $htmlRows;
        return new JsonResponse($data['response']);
    }
    /**
     * @Route("/deleteform", name="_paths_delete_form")
     */
    public function deleteFormRenderAction(Request $request) {
        return $this->render("deleteform.html.twig", array('id' => $request->query->get('id')));
    }
    /**
     * @Route("/deleteform/saved", name="_paths_delete_form_save")
     */
    public function deleteFormSavedAction(Request $request) {
        $id = $request->request->get('id');
        $this->ps->deletePath(array('id' => $id));
        return new Response('deleted');
    }
    /**
     * @Route("/addform", name="_paths_add_form")
     */
    public function addFormRenderAction(Request $request)
    {
        $params = 
        [
            "types" => $this->ps->getPathTypes()           
        ];
        return $this->render("addform.html.twig",$params);      
    }
    /**
     * @Route("/addform/saved", name="_paths_add_form_saved")
     */    
    public function addFormSavedAction(Request $request) {
        $params = $request->request->all();
        $params['general'] = $params['general'] == "on";
        $this->ps->addPath($this->get("session")->get("masterid"),$params);
        return new Response('added');
    }
    /**
     * @Route("/editform", name="_paths_edit_form")
     */
    public function editFormRenderAction(Request $request)
    {
        $params = 
        [
            "types" => $this->ps->getPathTypes(),
            "path" => $this->ps->getPathFull(["id" => $request->query->get("id")])
        ];
        return $this->render("editform.html.twig",$params);      
    }
    /**
     * @Route("/editform/saved", name="_paths_edit_form_saved")
     */    
    public function editFormSavedAction(Request $request) {
        $params = $request->request->all();
        $params['general'] = $params['general'] == "on";
        $this->ps->editPath(["id" => $request->request->get("id") ],$params);
        return new Response('saved');
    }
    /**
     * @Route("/copyPath", name="_paths_copyPath")
     */   
    public function copyPathAction(Request $request)
    {
        $this->get("session")->set("pathCopyId",$request->query->get("id"));
        return $this->render("copypath.html.twig");
    }
    /**
     * @Route("/copyPath/saved", name="_paths_copyPath_saved")
     */   
    public function copyPathSavedAction(Request $request)
    {
        $this->ps->copyPath($this->get("session")->get("pathCopyId"));     
        return new Response("");
    }    
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->ps = $this->get('PathsService');
        $this->ps->setId($this->get("session")->get("masterid"));
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:".$file,$params);
    }
}
