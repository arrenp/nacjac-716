<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ConfigureController extends BaseConfigureController
{
    /**
     * @Route("/configure/", name="_paths_configure")
     */  
    public function configureAction(Request $request)
    {
        if (!empty($request->query->get("id")))
            $this->get("session")->set("pathId",$request->query->get("id"));
        return $this->renderWithConfigure("configure.html.twig");         
    }    
    /**
     * @Route("/addsection", name="_paths_configure_addsection")
     */  
    public function addSectionAction(Request $request)
    {        
        $params = 
        [
            "sections" => $this->ps->getSectionListRemaining($this->get("session")->get("pathId"))
        ];
        return $this->render("addsection.html.twig",$params);
    }  
    /**
     * @Route("/addsection/saved", name="_paths_configure_addsection_saved")
     */  
    public function addSectionSavedAction(Request $request)
    {   
        $this->ps->addSection(["pathId" => $this->get("session")->get("pathId"),"appSectionListId" => $request->request->get("appSectionListId") ]);
        return new Response("saved");
    }  
    /**
     * @Route("/editsection", name="_paths_configure_editsection")
     */
    public function editSection(Request $request)
    {
        $this->get("session")->set("sectionId",$request->query->get("sectionId"));
        
        $section = $this->ps->getSectionFull(["id" => $request->query->get("sectionId") ]);
        $sectionListItem = $section->section;
        switch ($sectionListItem->section)
        {
            case "risk":
                return $this->forward("Paths\PathsBundle\Controller\RiskConfigureController::configureRiskProfileAction");
            case "planBasics":
                return $this->forward("Paths\PathsBundle\Controller\PlanBasicsConfigureController::configureAction");
            case "contributions":
                return $this->forward("Paths\PathsBundle\Controller\ContributionsConfigureController::configureAction");
            case "myprofile":
                return $this->forward("Paths\PathsBundle\Controller\MyProfileConfigureController::configureAction");
            case "retirementNeeds":
                return $this->forward("Paths\PathsBundle\Controller\RetirementNeedsConfigureController::configureAction");
            case "investments":
                return $this->forward("Paths\PathsBundle\Controller\InvestmentsConfigureController::configureAction");
            case "beneficiary_page":
                return $this->forward("Paths\PathsBundle\Controller\BeneficiaryConfigureController::configureAction");
            case "rollover":
                return $this->forward("Paths\PathsBundle\Controller\RolloverConfigureController::configureAction");
            case "goodbye":
                return $this->forward("Paths\PathsBundle\Controller\GoodbyeConfigureController::configureAction");
        }
        return $this->configureAction($request);
    }
    public function configureRiskProfile()
    {
        return $this->render("risk/risk.html.twig");
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->ps = $this->get('PathsService');
        $this->ps->setId($this->get("session")->get("masterid"));
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:configure/".$file,$params);
    }
    /**
     * @Route("/configure/reorder/saved", name="_paths_section_form_reorder_saved")
     */
    public function sectionFormSavedAction(Request $request) {
        $this->ps->reorderSections($request->request->get('sectionList'), array('pathId' => $request->request->get('pathId')));
        return new Response('saved');
    }
    /**
     * @Route("/configure/delete/", name="_paths_section_form_delete")
     */
    public function sectionFormDeleteAction(Request $request) 
    {
        $section = $this->ps->getSection(["id" => $request->query->get("id")]);
        return $this->render("deletesection.html.twig",["section" => $section]);
    }
    /**
     * @Route("/configure/delete/saved", name="_paths_section_form_delete_saved")
     */
    public function sectionFormDeleteSavedAction(Request $request) {
        $this->ps->deleteSection(array('id' => $request->request->get('id')));
        return new Response('savedtacos');
    }

    /**
     * @Route("/configure/active/saved", name="_paths_section_form_active_saved")
     */
    public function sectionFormActiveSavedAction(Request $request) {
        $this->ps->editSection(array('id' => $request->request->get('id')), array('active' => $request->request->get('active')));
        return new Response('saved');
    }
}
