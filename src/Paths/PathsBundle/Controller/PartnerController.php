<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PartnerController extends ListBaseController
{   
    /**
     * @Route("/partner", name="_paths_partner")
     */
    public function partnerAction()
    {
        $headerArray = ["id","partnerid"];
        $params = 
        [
            "header" => $this->header($headerArray),
            "unsearchableArray" => $this->unsearchableArray($headerArray)
        ];
        return $this->render("partner.html.twig",$params);
    }   
    /**
     * @Route("/partner/getPartners", name="_paths_partner_getpartners")
     */
    public function getPartnersAction(Request $request)
    {
        $data = $this->get("QueryBuilderDataTables")->jsonObject("accounts",["a.id","a.partnerid"],$request->request->all());
        $htmlRows = [];
        $types = $this->ps->getPathTypes();
        foreach ($data['rows'] as $account)
        {
            $row = 
            [
                $account->id,
                $account->partnerid,              
            ];
            $this->addTypeRows($account,$types,$row);
            $this->addRisk("Account",$account,$row);
            $htmlRows[] = $row;           
        }       
        $data['response']['data'] = $htmlRows;
        return new JsonResponse($data['response']);        
    }
    /**
     * @Route("/partner/savePaths", name="_paths_partner_savepaths")
     */    
    public function savePaths(Request $request)
    {
        $paths = $request->get("paths");
        $i = 1;
        $em = $this->getDoctrine()->getManager();
        foreach ($paths as $path)
        {
            $this->ps->setAccountPaths(['id' => $path['id']],$path);
            if ($i % 100 == 0)
            $em->flush();    
            $i++;
        }
        $em->flush();
        return new Response("");
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->ps = $this->get('PathsService');
        $this->ps->setId($this->get("session")->get("masterid"));
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:partner/".$file,$params);
    } 
}

