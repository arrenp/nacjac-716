<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/configure/goodbye/")
 */  
class GoodbyeConfigureController extends BaseConfigureController
{   
    /**
     * @Route("/configure", name="_paths_configureGoodbye")
     */
    public function configureAction()
    {
        return $this->renderWithConfigure("goodbye.html.twig");
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:configure/goodbye/".$file,$params);
    }      
}

