<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * @Route("/playListSections")
 */  
class PlayListSectionsController extends Controller
{
    /**
     * @Route("/", name="_paths_playListSections")
     */  
    public function indexAction()
    {
        return $this->render("index.html.twig",["sections" => $this->sections()]);
    }
    public function sections()
    {
        $sections =  
        [
            "planBasics" =>
            [
                "description" => "Plan Basics Videos"
            ],
            "retirementNeedsStarting" =>
            [
                "description" => "Retirement Needs Starting Videos"
            ],
            "retirementNeedsEnding" =>
            [
                "description" => "Retirement Needs Ending Videos"
            ],            
            "riskStarting" =>
            [
                "description" => "Risk Profile Starting Videos"
            ],   
            "riskEnding" =>
            [
                "description" => "Risk Profile Ending Videos"
            ],  
            "investmentsStarting" =>
            [
                "description" => "Investments Starting Videos"
            ],   
            "investmentsEnding" =>
            [
                "description" => "Investments Ending Videos"
            ], 
            "contributionsStarting" =>
            [
                "description" => "Contributions Starting Videos"
            ],   
            "contributionsEnding" =>
            [
                "description" => "Contributions Ending Videos"
            ], 
        ];
        foreach ($sections as $key => $section)
        {
            $sections[$key]['name'] = $key;
        }
        return $sections;
    }
    /**
     * @Route("/setVideos", name="_paths_playListSections_setVideos")
     */  
    public function setVideosAction(Request $request)
    {
        if (isset($this->sections()[$request->query->get("name")]))    
        {
            $this->get("session")->set("videoName",$request->query->get("name"));
            return $this->render("setVideos.html.twig",["videos" => $this->videoList($this->get("session")->get("videoName"))]);   
        }
        return new Response("");
    }
    /**
     * @Route("/getVideos", name="_paths_playListSections_getVideos")
     */     
    public function getVideosAction(Request $request)
    {
        return $this->render("getVideos.html.twig",["videos" => $this->videoList($this->get("session")->get("videoName"))]);                   
    }
    public function videoList($name)
    {
        $this->get("session")->set("videoName",$name);
        return $this->ps->getPlayListVideosFull(["name" => $name]);        
    }
    /**
     * @Route("/addVideo", name="_paths_playListSections_addVideo")
     */  
    public function addVideoAction(Request $request)
    {
        $languages = $this->get("LanguagesService")->getAll("id");
        return $this->render("addvideo.html.twig",["languages" => $languages]);
    }
    /**
     * @Route("/addVideo/saved", name="_paths_playListSections_addVideoSaved")
     */  
    public function addVideoSavedAction(Request $request)
    {
        $params = $request->request->all();
        $params['name'] = $this->get("session")->get("videoName");
        $this->ps->addPlayListVideo($params);
        return new Response("");
    }
    /**
     * @Route("/editVideo", name="_paths_playListSections_editVideo")
     */  
    public function editVideoAction(Request $request)
    {
        $languages = $this->get("LanguagesService")->getAll("id");
        $video = $this->ps->getPlayListVideo(["id" => $request->query->get("id")]);
        return $this->render("editvideo.html.twig",["languages" => $languages,"video" => $video]);
    }
    /**
     * @Route("/editVideo/saved", name="_paths_playListSections_editVideoSaved")
     */  
    public function editVideoSavedAction(Request $request)
    {
        $params = $request->request->all();
        $params['name'] = $this->get("session")->get("videoName");
        $this->ps->editPlayListVideo(["id" => $request->request->get("id")],$params);
        return new Response("");
    }
    /**
     * @Route("/deleteVideo", name="_paths_playListSections_deleteVideo")
     */  
    public function deleteVideoAction(Request $request)
    {
        $video = $this->ps->getPlayListVideo(["id" => $request->query->get("id")]);
        return $this->render("deletevideo.html.twig",["video" => $video]);
    }
    /**
     * @Route("/deleteVideo/saved", name="_paths_playListSections_deleteVideoSaved")
     */  
    public function deleteVideoSavedAction(Request $request)
    {
        $this->ps->deletePlayListVideo(["id" => $request->request->get("id")]);
        return new Response("");
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->ps = $this->get('PlayListService');
        $this->ps->setId($this->get("session")->get("masterid"));
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:playListSections/".$file,$params);
    }
}