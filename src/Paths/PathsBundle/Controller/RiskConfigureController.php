<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/configure/risk/")
 */  
class RiskConfigureController extends BaseConfigureController
{   
    /**
     * @Route("/configureRiskProfile", name="_paths_configureRiskProfile")
     */
    public function configureRiskProfileAction()
    {
        return $this->renderWithConfigure("risk.html.twig");
    }    
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:configure/risk/".$file,$params);
    }   
}

