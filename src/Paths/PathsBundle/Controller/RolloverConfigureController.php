<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/configure/rollover/")
 */  
class RolloverConfigureController extends BaseConfigureController
{   
    /**
     * @Route("/configure", name="_paths_configureRollover")
     */
    public function configureAction()
    {
        return $this->renderWithConfigure("rollover.html.twig");
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:configure/rollover/".$file,$params);
    }      
}

