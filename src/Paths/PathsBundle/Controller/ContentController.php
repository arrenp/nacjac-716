<?php

namespace Paths\PathsBundle\Controller;
use classes\classBundle\Entity\AppContentType;
use classes\classBundle\Entity\AppSection;
use classes\classBundle\Entity\AppStep;
use classes\classBundle\Entity\AppStepContent;
use classes\classBundle\Entity\AppStepList;
use classes\classBundle\Entity\DefaultContent;
use classes\classBundle\Entity\Path;
use classes\classBundle\Entity\PathContent;
use classes\classBundle\Entity\Profiles2\AppData;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * Ajax controller.
 * @Route("/content")
 */
class ContentController extends Controller
{   
    /**
     * @Route("/index", name="ContentIndex")
     */
    public function indexAction()
    {
        return $this->render("index.html.twig");
    } 
    /**
     * @Route("/list", name="ContentList")
     */
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(AppSection::class);
        $section = $repository->find($this->get("session")->get("sectionId"));

        $pathContentsHashed = [];
        $repository = $this->getDoctrine()->getRepository(PathContent::class);
        $pathContents = $repository->findBy(['pathId' => $section->pathId]);
        foreach ($pathContents as $pathContent) {
            $pathContentsHashed[$pathContent->defaultContentId] = $pathContent;
        }

        $contents = $this->get("PathsService")->getPathContentList(["sectionId" => $this->get("session")->get("sectionId")]);

        return $this->render("list.html.twig",[
            "contents" => $contents,
            "contentTypes" => $this->get("PathsService")->getContentTypes(),
            "section" => $section,
            "pathContents" => $pathContentsHashed
        ]);
    }
    /**
     * @Route("/add", name="ContentAdd")
     */    
    public function addContentAction(Request $request)
    {
        $selectedContentType  = $this->get("PathsService")->getContentType(["id" =>$request->query->get("contentTypeId")]);
        $contentTypes = $this->get("PathsService")->getContentTypes();
        return $this->render("add.html.twig",["selectedContentType" => $selectedContentType,"contentTypes" => $contentTypes]);
    }
    /**
     * @Route("/add/saved", name="ContentAddSaved")
     */   
    public function addContentSavedAction(Request $request)
    {
        $params = $request->request->all();
        $params['sectionListId']= $this->get("PathsService")->getSection(["id" => $this->get("session")->get("sectionId")])->appSectionListId;
        $this->get("PathsService")->addDefaultContent($params);
        return new Response("");
    }
    /**
     * @Route("/edit", name="ContentEdit")
     */    
    public function editContentAction(Request $request)
    {
        $content  = $this->get("PathsService")->getDefaultContent(["id" => $request->query->get("id")]);
        $selectedContentType = $this->get("PathsService")->getContentType(["id" => $content->contentTypeId]);
        $contentTypes = $this->get("PathsService")->getContentTypes();
        return $this->render("edit.html.twig",["selectedContentType" => $selectedContentType,"contentTypes" => $contentTypes,"content" => $content]);
    }
    /**
     * @Route("/edit/saved", name="ContentEditSaved")
     */    
    public function editContentSavedAction(Request $request)
    {
        $params = $request->request->all();
        $this->get("PathsService")->editDefaultContent($params);
        return new Response("");
    }
    /**
     * @Route("/delete", name="ContentDelete")
     */    
    public function deleteContentAction(Request $request)
    {
        return $this->render("delete.html.twig",["id" => $request->query->get("id")]);
    }
    /**
     * @Route("/deleteSaved", name="ContentDeleteSaved")
     */    
    public function deleteContentSavedAction(Request $request)
    {
        $this->get("PathsService")->deleteDefaultContent(["id" => $request->request->get("id")]);
        
    }
    /**
     * @Route("/editPathContent", name="EditPathContent")
     */    
    public function editPathContentAction(Request $request)
    {
        $content = $this->get("PathsService")->getContent($request->query->get("id"),$this->get("session")->get("pathId"));
        return $this->render("editPathContent.html.twig",["content" => $content,"defaultContentId" => $request->query->get("id") ]);
    }
    /**
     * @Route("/editPathContentSaved", name="EditPathContentSaved")
     */    
    public function editPathContentSavedAction(Request $request)
    {
        $findOneBy = ["defaultContentId" => $request->request->get("defaultContentId"),"pathId" => $this->get("session")->get("pathId")];
        $params = $request->request->all();
        $this->get("PathsService")->updatePathContent($findOneBy,$params);
        return new Response("");
    }
    /**
     * @Route("/deletePathContentSaved", name="DeletePathContentSaved")
     */    
    public function deletePathContentSavedAction(Request $request)
    {
        $this->get("PathsService")->deletePathContent(["id" => $request->request->get("id")]);
        return new Response("");
    }
    /**
     * @Route("/togglePathContent", name="TogglePathContent")
     */
    public function togglePathContentAction(Request $request)
    {
        $findOneBy = ["defaultContentId" => $request->request->get("id"),"pathId" => $this->get("session")->get("pathId")];
        $this->get("PathsService")->updatePathContent($findOneBy,$request->request->all());
        return new Response("");
    }
    /**
     * @Route("/togglePathStep", name="TogglePathStep")
     */
    public function togglePathStepAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(AppStep::class);
        $step = $repository->find($request->get("id"));
        $step->active = $request->get("active", false);
        $entityManager->flush();
        return new Response("");
    }
    /**
     * @Route("/saveStepOrder", name="SaveStepOrder")
     */
    public function saveStepOrderAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(AppStep::class);
        $entityManager = $this->getDoctrine()->getManager();
        $order = $request->get('order');
        foreach ($order as $sort => $stepId)
        {
            $step = $repository->find($stepId);
            $step->sort = $sort;
        }
        $entityManager->flush();
    }
    /**
     * @Route("/replacePathContent", name="ReplacePathContent")
     */
    public function replacePathContentAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(DefaultContent::class);
        $content = $repository->find($request->get("id"));
        $repository = $this->getDoctrine()->getRepository(AppContentType::class);
        $type = $repository->find($content->contentTypeId);
        $pathContent = $this->get("PathsService")->getContent($request->get("id"),$this->get("session")->get("pathId"));
        $appData = new AppData();
        $fields = array_keys($appData->getData());

        $rules = $pathContent->contentTable == 'path' ? $pathContent->getRules() : null;
        if (empty($rules))
        {
            $rules = PathContent::getDefaultRules();
        }

        return $this->render("replacePathContent.html.twig",[
            "content" => $content, "type" => $type, "fields" => $fields, "rules" => $rules
        ]);
    }
    /**
     * @Route("/replacePathContentSaved", name="ReplacePathContentSaved")
     */
    public function replacePathContentSaved(Request $request)
    {
        $findOneBy = ["defaultContentId" => $request->get("defaultContentId"), "pathId" => $this->get("session")->get("pathId")];
        $params = $request->request->all();
        $this->get("PathsService")->updatePathContent($findOneBy, $params);
        $this->addFlash('success', 'Replacement rules saved');
        return new Response("");
    }

    /**
     * @Route("/step", name="Step")
     */
    public function step(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(AppStep::class);
        $appStep = $repository->find($request->get("id"));

        $pathContentsHashed = [];
        $repository = $this->getDoctrine()->getRepository(PathContent::class);
        $pathContents = $repository->findBy(['pathId' => $appStep->section->pathId]);
        foreach ($pathContents as $pathContent) {
            $pathContentsHashed[$pathContent->defaultContentId] = $pathContent;
        }

        return $this->render('step.html.twig', [
            'step' => $appStep,
            'contentTypes' => $this->get("PathsService")->getContentTypes(),
            'pathContents' => $pathContentsHashed
        ]);
    }

    /**
     * @Route("/addStepContent", name="AddStepContent")
     */
    public function addStepContent(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(AppStep::class);
        $content = new AppStepContent();
        $content->step = $repository->find($request->get("stepId"));
        return $this->render('editStepContent.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/editStepContent", name="EditStepContent")
     */
    public function editStepContent(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(AppStepContent::class);
        $content = $repository->find($request->get("id"));
        return $this->render('editStepContent.html.twig', ['content' => $content]);
    }

    /**
     * @Route("/removeStepContent", name="RemoveStepContent")
     */
    public function removeStepContent(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(AppStepContent::class);
        $content = $repository->find($request->get("id"));
        $entityManager->remove($content);
        $entityManager->flush();
        return new Response();
    }

    /**
     * @Route("/saveStepContent", name="SaveStepContent")
     */
    public function saveStepContent(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        if ($request->request->has("id"))
        {
            $repository = $this->getDoctrine()->getRepository(AppStepContent::class);
            $content = $repository->find($request->get("id"));
        }
        else
        {
            $content = new AppStepContent();
            $repository = $this->getDoctrine()->getRepository(AppStep::class);
            $step = $repository->find($request->get("stepId"));
            $content->step = $step;
            $entityManager->persist($content);
        }
        $content->englishContent = $request->get("englishContent");
        $content->spanishContent = $request->get("spanishContent");
        $content->name = $request->get("name");
        $content->type = AppStepContent::TYPE_AUDIO;
        $entityManager->flush();

        return new Response();
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->ps = $this->get('PlayListService');
        $this->ps->setId($this->get("session")->get("masterid"));
        $this->ps->setPathId($this->get("session")->get("pathId"));
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:content/".$file,$params);
    } 
}

