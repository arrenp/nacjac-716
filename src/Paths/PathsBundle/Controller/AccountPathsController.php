<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * @Route("/configure/accountPaths/")
 */  
class AccountPathsController extends ListBaseController
{   
    /**
     * @Route("", name="_paths_accountPaths")
     */
    public function indexAction(Request $request)
    {
        $this->get("session")->set("accountPathId",$request->query->get("id"));
        return $this->render("index.html.twig");
    }
    /**
     * @Route("/getAccounts", name="_paths_accountPaths_getAccounts")
     */
    public function getAccountsAction(Request $request)
    {
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $queryBuilder->select('a','PathAccount');
        $queryBuilder->from('classesclassBundle:accounts', 'a');
        $queryBuilder->leftJoin(
            'classesclassBundle:PathAccount',
            'PathAccount',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'a.id = PathAccount.userid and PathAccount.pathId= :pathId'
        );
        $queryBuilder->setParameter("pathId",$this->get("session")->get("accountPathId"));
        $data = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder("accounts",["a.id","a.partnerid"],$request->request->all(),$queryBuilder);
        $htmlRows = [];
        $rowData = $this->get("QueryBuilderDataTables")->formatResult(["accounts","PathAccount"],$data['rows']);
        foreach ($rowData as $row)
        {
            $row = 
            [
                $row['accounts']->id,
                $row['accounts']->partnerid,    
                $this->render("list/add.html.twig",["checked" => !empty($row['PathAccount']),"userid" => $row['accounts']->id,"pathId" => $this->get("session")->get("accountPathId") ])->getContent(),
            ];   
            $htmlRows[] = $row;
        }       
        $data['response']['data'] = $htmlRows;
        return new JsonResponse($data['response']);        
    }  
    /**
     * @Route("/toggleAccount", name="_paths_accountPaths_toggleAccount")
     */
    public function togglePathAccountAction(Request $request)
    {
        $pathParams = ["userid" => $request->request->get("userid"),"pathId" => $request->request->get("pathId")];
        $pathAccount = $this->ps->getPathAccount($pathParams);
        if ($request->request->get("on") && !$pathAccount)        
            $this->ps->addPathAccount($pathParams);        
        else if (!$request->request->get("on")  && $pathAccount)
            $this->ps->deletePathAccount($pathParams);
        return new Response("");
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->ps = $this->get('PathsService');
        $this->ps->setId($this->get("session")->get("masterid"));
    }
    public function render($file,$params = [])
    {
        return parent::render("PathsPathsBundle:Default:accountPaths/".$file,$params);
    }    
}
