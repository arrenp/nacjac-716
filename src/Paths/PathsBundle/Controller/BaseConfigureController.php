<?php

namespace Paths\PathsBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BaseConfigureController extends Controller
{
    public function renderWithConfigure($file,$params = [])
    {
        $paramsAuto =   
        [
            "path" => $this->ps->getPathFull(["id" => $this->get("session")->get("pathId")]),
            "showAddButton" => !empty($this->ps->getSectionListRemaining($this->get("session")->get("pathId")))
        ];
        $params = array_merge($params,$paramsAuto);
        return $this->render($file,$params);
    } 
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->ps = $this->get('PathsService');
        $this->ps->setId($this->get("session")->get("masterid"));
    }
}

