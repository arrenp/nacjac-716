<?php

namespace Account\AccountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Shared\DocumentsBundle\Classes\documents;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\videoLibrary;
use Utilities\pdf\pdfClass;
use classes\classBundle\Entity\dataTablesExport;
use Utilities\mailer\PHPMailer;
use Utilities\mailer\SMTP;
use Utilities\mailer\mailerInit;
set_time_limit(0);
require_once(getcwd()."/../src/Utilities/dompdf/dompdf_config.inc.php");

class MediaWidgetController extends Controller
{

    public $header = "<th>Id</th><th>PartnerID</th><th>PlayDuration</th><th>VideoLength</th><th>Percentage Viewed</th><th>ParticipantID</th><th>VideoPlayed</th><th>PlanProvider</th><th>Date</th>";
    public $header2 = "<th>Video</th><th>Views</th><th>Average Percentaged Viewed</th><th>Popularity Index</th>";

    public function indexAction($emailed = false,$startDate = null, $endDate = null)
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $methods = $this->get('GeneralMethods');
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:dataTablesExport');
        $dataTablesExports = $repository->findBy(array('accountsUsersId' => $session->clientid));
        foreach ($dataTablesExports as $dataTablesExport)
        {
            $em->remove($dataTablesExport);
        }
        $em->flush();
        $sections = array();
        $connection = $this->get('doctrine.dbal.default_connection');

        $session->set("section", "Account");

        $methods->addSection($sections, "settings", "Settings");
        $methods->addSection($sections, "stats", "Stats");

        $formats = array();
        $methods->addSection($formats, 0, "Constrained");
        $methods->addSection($formats, 1, "Expanded");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $session->userid));


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:product');
        $product = $repository->findOneBy(array('name' => 'widget'));


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:APIAccounts');
        $APIAccount = $repository->findOneBy(array('userid' => $session->userid, 'active' => 1, 'productid' => $product->id));



        $embedCode = "https://videolib.smartplanenterprise.com/vWidget.js";

        $sql = "SELECT * FROM APIAccounts LEFT join accounts on APIAccounts.userid = accounts.id ORDER by accounts.id";
        $APIAccounts = $connection->fetchAll($sql);


        $sql = "SELECT * FROM media WHERE  applications LIKE '%\"widget\"%' AND shareable = 1";
        $connection = $this->get('doctrine.dbal.default_connection');
        $medias = $connection->fetchAll($sql);



        foreach ($medias as &$media)
        {
            $media['movienumber'] = $media['product'] . $media['module'];
            $media['mp4'] = $media['mp4url'];
            $media['webm'] = $media['webmurl'];
            $media['videolink'] = $this->generateUrl('_plans_outreach_videoplayer_videoonly_WithoutLibrary') . "?mp4=" . $media['mp4'] . "&webm=" . $media['webm'];
            $media['checked'] = "";
            $media['pdf'] = "https://c06cc6997b3d297a6674-653de9dab23a7201285f2586065c1865.ssl.cf1.rackcdn.com/transcript/" . $media['transcriptFile'] . ".pdf";
            $sql = "SELECT * FROM videoLibrary WHERE partnerid = '" . $account->partnerid . "' AND mediaid =  " . $media['id'];
            $library = $connection->fetchAll($sql);
            if (count($library) > 0) $media['checked'] = "checked";
        }



        if ($APIAccount != null)
        {
            $stats = $this->getStats(null, null, null);
            $httpmethod = $methods->httpmethod();         
            return $this->render("AccountAccountBundle:MediaWidget:index.html.twig", array("sections" => $sections, "formats" => $formats, "APIAccount" => $APIAccount, "embedCode" => $embedCode, "account" => $account, "session" => $session, "medias" => $medias, "stats" => $stats, "APIAccounts" => $APIAccounts, "header" => $this->header, "header2" => $this->header2, "emailed" => $emailed,"startDate" => $startDate,"endDate" => $endDate,"httpmethod" => $httpmethod));
        }
        else return new Response("");
    }

    public function exportToPdfAction()
    {       
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('adminsession');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $accountsUsers = $repository->findOneBy(array('id' => $session->clientid));
        
        $startDate = $request->query->get("startDate", "");
        $endDate = $request->query->get("endDate", "");
        
                
        $stats = $this->getStats($startDate, $endDate, $request->query->get("key", ""));
        $top = '<table width = "100%" ><tbody>';
        $top = $top . '<tr><td>Number of Unique Participants:</td><td align = "right"> ' . $stats->numberOfUniqueParticipants . "<br/></td></tr>";
        $top = $top . '<tr><td>Number of  Participants:</td><td align = "right"> ' . $stats->numberOfParticipants . "<br/></td></tr>";
        $top = $top . '<tr><td>Number of  Unique Movies:</td><td align = "right"> ' . $stats->numberOfUniqueMovies . "<br/></td></tr>";
        $top = $top . '<tr><td>Number of  PartnerID: </td><td align = "right"> ' . $stats->numberOfPartnerID . "<br/></td></tr>";
        $top = $top . '<tr><td>Number of  Plan Provider: </td><td align = "right"> ' . $stats->numberOfPlanProvider . "</td></tr>";
        $top = $top . "</tbody></table><br/><br/>";

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:dataTablesExport');
        $dataTablesExport1 = $repository->findOneBy(array('accountsUsersId' => $session->clientid, "name" => "mediaWidget1"));
        $dataTablesExport2 = $repository->findOneBy(array('accountsUsersId' => $session->clientid, "name" => "mediaWidget2"));
        $tablehead = '<table width = "100%" style = "font-size:10px">';
        $tablefooter = '</table><br/>';
        $table1 = "";
        $table2 = "";
        if ($dataTablesExport1 != null)
        $table1 = $tablehead . str_replace("<th>Id</th>", "", $dataTablesExport1->data) . $tablefooter;
        if ($dataTablesExport2 != null)
        $table2 = $tablehead . $dataTablesExport2->data . $tablefooter;


        $partnerIdGraph = '<img src = "' . $request->query->get("partnerIdGraph", "") . '"/><br/><br/>';

        $planProviderGraph = '<img src = "' . $request->query->get("planProviderGraph", "") . '"/><br/><br/>';
        $videoPlayedGraph = '<img src = "' . $request->query->get("videoPlayedGraph", "") . '"/><br/><br/>';


        $data = $top . $table1 . $partnerIdGraph . $planProviderGraph . $videoPlayedGraph . $table2;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:dataTablesExport');
        $dataTablesExports = $repository->findBy(array('accountsUsersId' => $session->clientid));
        

        
        foreach ($dataTablesExports as $dataTablesExport)
        {
            $em->remove($dataTablesExport);
        }
        $em->flush();

        $dompdf = new \DOMPDF();
        $dompdf->load_html($data);
        $dompdf->render();
        $output = $dompdf->output(); 
        $name = "VideoLibraryReport.pdf";
        if ($request->query->get("emailed") == "email")
        {           
            
            $filename = sys_get_temp_dir()."/".$name;
                                
            file_put_contents($filename, $output);
            
            $emailContents = "Attached is your Subscription for Video Library";
            $message = \Swift_Message::newInstance()
            ->setSubject('Smartplan Subscription for Video Library')
            ->setFrom('info@smartplanenterprise.com')
            ->setTo(trim($accountsUsers->getEmail()))
            ->attach(\Swift_Attachment::fromPath($filename))      
            ->setBody(
            $emailContents,'text/html');
            //render email template and send email
            $this->get('mailer')->send($message); 
            
            return new Response("Email Sent");
        }
        
        else return new Response($dompdf->stream($name));
    }

    public function emailSubscriptionAction()
    {
        $session = $this->get('adminsession');
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:reportingSubscriptions');
        $subscription = $repository->findOneBy(array('accountsUsersId' => $session->clientid,"name" => "VideoLibrary" ));
        if ($subscription->howoften == "monthly")
        {
            $startDate = explode(" ",date("Y-m-d H:i:s",strtotime("-1 month")))[0];
            $endDate = explode(" ",date("Y-m-d H:i:s"))[0];
        }
        if ($subscription->howoften == "quarterly")
        {
            $startDate = explode(" ",date("Y-m-d H:i:s",strtotime("-3 month")))[0];
            $endDate = explode(" ",date("Y-m-d H:i:s"))[0];
        }
        if ($subscription->howoften == "daily")
        {
            $startDate = explode(" ",date("Y-m-d H:i:s",strtotime("-1 day")))[0];
            $endDate = explode(" ",date("Y-m-d H:i:s"))[0];
        }  
        if ($subscription->howoften == "weekly")
        {
            $startDate = explode(" ",date("Y-m-d H:i:s",strtotime("-1 week")))[0];
            $endDate = explode(" ",date("Y-m-d H:i:s"))[0];
        } 
        if ($subscription->howoften == "yearly")
        {
            $startDate = explode(" ",date("Y-m-d H:i:s",strtotime("-1 year")))[0];
            $endDate = explode(" ",date("Y-m-d H:i:s"))[0];
        }        
        $subscription->lastProcessed =  new \DateTime("now");
        $em->flush();
        //echo $startDate."<br/>";
       // echo $endDate."<br/>";
        return $this->indexAction(true,$startDate,$endDate);
    }

    public function buildTableDataAction()
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('adminsession');
        $request = Request::createFromGlobals();
        $request->getPathInfo();

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:dataTablesExport');
        $dataTablesExport = $repository->findOneBy(array('accountsUsersId' => $session->clientid, "name" => $request->request->get("name")));
        if ($dataTablesExport == null)
        {
            $dataTablesExport = new dataTablesExport();
            $dataTablesExport->accountsUsersId = $session->clientid;
            $dataTablesExport->name = $request->request->get("name");
            $dataTablesExport->data = $dataTablesExport->data . $request->request->get("data");
            $em->persist($dataTablesExport);
        }
        else $dataTablesExport->data = $dataTablesExport->data . $request->request->get("data");
        $em->flush();
        return new Response("");
    }

    public function getStatsAction()
    {
        $startDate = null;
        $endDate = null;
        $key = null;
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        if ($request->request->get("startDate", "") != "")
        {
            $startDate = $request->request->get("startDate", "");
        }
        if ($request->request->get("endDate", "") != "")
        {
            $endDate = $request->request->get("endDate", "") . " 23:59:59";
        }
        if ($request->request->get("key", "") != "")
        {
            $key = $request->request->get("key", "");
        }


        return $this->render("AccountAccountBundle:MediaWidget:graphs.html.twig", array("stats" => $this->getStats($startDate, $endDate, $key), "header2" => $this->header2, "header" => $this->header));
    }

    public function getStats($startDate = null, $endDate = null, $key = null)
    {
        $session = $this->get('adminsession');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $session->userid));


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:product');
        $product = $repository->findOneBy(array('name' => 'widget'));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:APIAccounts');
        $APIAccounts = $repository->findBy(array('userid' => $session->userid, 'active' => 1, 'productid' => $product->id));

        $stats = new \stdClass();
        $connection = $this->get('doctrine.dbal.default_connection');

        $sql = "UPDATE videoLibStats set percentageViewed = (playDuration/videoLength) * 100 WHERE percentageViewed = -1";
        $connection->executeQuery($sql);


        $whereSql = " WHERE partnerID NOT IN('michel','kevin','111')  AND widgetKey in (";
        $inSql = "";
        foreach ($APIAccounts as $APIAccount)
        {
            if ($inSql != "") $inSql = $inSql . ",";
            $inSql = $inSql . "'".$APIAccount->apiKey."'";
        }
        if ($key != null) $inSql = $key;
        $whereSql = $whereSql . $inSql . ") ";

        $stats->keys = $inSql;
        if ($startDate != null && $endDate != null) $whereSql = $whereSql . " AND videoLibStats.date >= '" . $startDate . "' AND videoLibStats.date <= '" . $endDate . "' ";


        $sql = "SELECT COUNT(DISTINCT(participantID)) FROM videoLibStats " . $whereSql;
        $stats->numberOfUniqueParticipants = $connection->fetchAll($sql)[0]['COUNT(DISTINCT(participantID))'];
        $sql = "SELECT COUNT(participantID) FROM videoLibStats " . $whereSql;
        $stats->numberOfParticipants = $connection->fetchAll($sql)[0]['COUNT(participantID)'];
        $sql = "SELECT COUNT(DISTINCT(videoPlayed)) FROM videoLibStats " . $whereSql;
        $stats->numberOfUniqueMovies = $connection->fetchAll($sql)[0]['COUNT(DISTINCT(videoPlayed))'];
        $sql = "SELECT COUNT(DISTINCT(partnerID)) FROM videoLibStats " . $whereSql;
        $stats->numberOfPartnerID = $connection->fetchAll($sql)[0]['COUNT(DISTINCT(partnerID))'];
        $sql = "SELECT COUNT(DISTINCT(planProvider)) FROM videoLibStats " . $whereSql;
        $stats->numberOfPlanProvider = $connection->fetchAll($sql)[0]['COUNT(DISTINCT(planProvider))'];

        $sql = "SELECT * FROM videoLibStats " . $whereSql . " group by partnerID";
        $videoLibStats = $connection->fetchAll($sql);
        $stats->partnerIdGraph = array();
        foreach ($videoLibStats as $videoLibStat)
        {
            $partnerid = $videoLibStat['partnerID'];
            $sql = "SELECT COUNT(*) FROM videoLibStats " . $whereSql . " AND partnerID = '" . $partnerid . "'";
            $stats->partnerIdGraph[$partnerid] = $connection->fetchAll($sql)[0]['COUNT(*)'];
        }

        $sql = "SELECT * FROM videoLibStats " . $whereSql . " group by planProvider";
        $videoLibStats = $connection->fetchAll($sql);
        $stats->planProviderGraph = array();
        foreach ($videoLibStats as $videoLibStat)
        {
            $planProvider = $videoLibStat['planProvider'];
            $sql = "SELECT COUNT(*) FROM videoLibStats " . $whereSql . " AND planProvider = '" . $planProvider . "'";
            $stats->planProviderGraph[$planProvider] = $connection->fetchAll($sql)[0]['COUNT(*)'];
        }

        $sql = "SELECT * FROM videoLibStats LEFT JOIN media on (media.mp4url LIKE CONCAT('%',videoLibStats.videoPlayed,'%') ) " . $whereSql . " group by videoPlayed";
        $videoLibStats = $connection->fetchAll($sql);
        //var_dump($videoLibStats);
        $stats->videoPlayedGraph = array();
        $stats->mostPopularTable = array();
        foreach ($videoLibStats as $videoLibStat)
        {
            $videoPlayed = $videoLibStat['videoPlayed'];
            $sql = "SELECT COUNT(*) FROM videoLibStats " . $whereSql . " AND videoPlayed = '" . $videoPlayed . "'";
            $stats->videoPlayedGraph[$videoPlayed . " " . $videoLibStat['title']] = $connection->fetchAll($sql)[0]['COUNT(*)'];
            $stats->mostPopularTable[$videoLibStat['id']] = new \stdClass();
            $stats->mostPopularTable[$videoLibStat['id']]->title = $videoPlayed . " " . $videoLibStat['title'];
            $stats->mostPopularTable[$videoLibStat['id']]->views = $connection->fetchAll($sql)[0]['COUNT(*)'];
            $sqlAverage = "SELECT avg(percentageViewed) FROM videoLibStats " . $whereSql . " AND videoPlayed = '" . $videoPlayed . "'";
            $stats->mostPopularTable[$videoLibStat['id']]->percentageViewed = 0;
            $stats->mostPopularTable[$videoLibStat['id']]->popularityIndex = 0;
            if ($connection->fetchAll($sqlAverage)[0]['avg(percentageViewed)'] != 0)
            {
                $stats->mostPopularTable[$videoLibStat['id']]->percentageViewed = $connection->fetchAll($sqlAverage)[0]['avg(percentageViewed)'];
                $stats->mostPopularTable[$videoLibStat['id']]->popularityIndex = $stats->mostPopularTable[$videoLibStat['id']]->views / $stats->mostPopularTable[$videoLibStat['id']]->percentageViewed;
            }
        }

        $stats->startDate = $startDate;
        $stats->endDate = $endDate;
        $partnerIdGraph_xAxix_categories = "";
        $partnerIdGraph_series_data = "";
        foreach ($stats->partnerIdGraph as $key => $value)
        {
            if ($partnerIdGraph_xAxix_categories != "")
            {
                $partnerIdGraph_xAxix_categories = $partnerIdGraph_xAxix_categories.",";
                $partnerIdGraph_series_data = $partnerIdGraph_series_data.",";
            }
            $partnerIdGraph_xAxix_categories =  $partnerIdGraph_xAxix_categories.'"'.$key.'"';
            $partnerIdGraph_series_data = $partnerIdGraph_series_data.$value;
            
        }
        $stats->partnerIdGraphOptionsJson = '
        {
           "chart": {
                "type": "column"
            },
           "title": {
                "text": "By PartnerID"
            },
            "xAxis": {
                "categories": ['.$partnerIdGraph_xAxix_categories.']
            },
            "yAxis": {

                "title": {
                    "text": "Times PartnerID Called"
                }
            } ,   
            "credits": {
                "enabled": false
            },
            "series": [{
                "name":"Partners",
                "data": ['.$partnerIdGraph_series_data.']
            }]
        }
        ';
        
        $stats->partnerIdGraphOptionsJson = str_replace("\n","", $stats->partnerIdGraphOptionsJson );     
        
        $stats->partnerIdGraphOptions = json_decode($stats->partnerIdGraphOptionsJson,true);
        $stats->partnerIdGraphOptionsStringify = json_encode($stats->partnerIdGraphOptions,false);
        
        $planProviderGraph_xAxix_categories = "";
        $planProviderGraph_series_data = "";
        foreach ($stats->planProviderGraph as $key => $value)
        {
            if ($planProviderGraph_xAxix_categories != "")
            {
                $planProviderGraph_xAxix_categories = $planProviderGraph_xAxix_categories.",";
                $planProviderGraph_series_data = $planProviderGraph_series_data.",";
            }
            $planProviderGraph_xAxix_categories =  $planProviderGraph_xAxix_categories.'"'.$key.'"';
            $planProviderGraph_series_data = $planProviderGraph_series_data.$value;

        }
        
        $stats->planProviderGraphOptionsJson = '    
        {
            "chart": {
                "type": "column"
            },
            "title": {
                "text": "By Plan Provider"
            },
            "xAxis": {
                "categories": ['.$planProviderGraph_xAxix_categories.']
            },
            "yAxis": {

                "title": {
                    "text": "Times Plan Provider Called"
                }
            } ,   
            "credits": {
                "enabled": false
            },
            "series": [{
                "name":"Plan Providers",
                "data": ['.$planProviderGraph_series_data.']
            }]
        }
        ';
        $stats->planProviderGraphOptionsJson = str_replace("\n","", $stats->planProviderGraphOptionsJson );
        $stats->planProviderGraphOptions = json_decode($stats->planProviderGraphOptionsJson,true);
        $stats->planProviderGraphOptionsStringify = json_encode($stats->planProviderGraphOptions,false);
        
        //var_dump($stats->planProviderGraphOptionsJson);
        //var_dump($stats->planProviderGraphOptionsStringify);
        $videoPlayedGraph_series_data = "";
       
        foreach ($stats->videoPlayedGraph as $key => $value)
        {
            if ($videoPlayedGraph_series_data != "")
            {
                $videoPlayedGraph_series_data = $videoPlayedGraph_series_data.",";
            }
            $videoPlayedGraph_series_data = $videoPlayedGraph_series_data.'{"name":"'.$key.'","y":'.$value.'}';
        }
        
        
        $stats->videoPlayedGraphOptionsJson = '
        {
            "chart": {
                "plotBackgroundColor": null,
                "plotBorderWidth": null,
                "plotShadow": false,
                "type": "pie"
            },
            "title": {
                "text": "Video Percentages"
            },
            "tooltip": {
                "pointFormat": "{series.name}: <b>{point.percentage:.1f}%</b>"
            },
            "plotOptions": {
                "pie": {
                    "allowPointSelect": true,
                    "cursor": "pointer",
                    "dataLabels": {
                        "enabled": true,
                        "format": "<b>{point.name}</b>: {point.percentage:.1f} %",
                        "style": {
                            "color":  "black"
                        }
                    },
                    "showInLegend": true

                }
            },
            "series": [{
                "name": "Brands",
                "colorByPoint": true,
                "data": ['.$videoPlayedGraph_series_data.']
            }]
        }            

        ';
        
        $stats->videoPlayedGraphOptionsJson= str_replace("\n","", $stats->videoPlayedGraphOptionsJson );
        //var_dump($stats->videoPlayedGraphOptionsJson);
        $stats->videoPlayedGraphOptions = json_decode($stats->videoPlayedGraphOptionsJson,true);
        $stats->videoPlayedGraphOptionsStringify = json_encode($stats->videoPlayedGraphOptions,false);    
        
        //var_dump($stats->videoPlayedGraphOptionsJson);
        //var_dump($stats->videoPlayedGraphOptionsStringify);
        
        //var_dump($partnerIdGraphOptions);
        //var_dump($partnerIdGraphOptionsStringify);
        
        
               
        
        return $stats;
    }

    public function previewAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $partnerid = $request->query->get("partnerid");
        $key = $request->query->get("key");
        $participantid = $request->query->get("participantid");
        $format = $request->query->get("format");
        return $this->render("AccountAccountBundle:MediaWidget:preview.html.twig", array("partnerid" => $partnerid, "key" => $key, "key" => $key, "participantid" => $participantid, "format" => $format));
    }

    public function saveMediaListAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $session->userid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:videoLibrary');
        $library = $repository->findBy(array('partnerid' => $account->partnerid));
        foreach ($library as $media) $em->remove($media);

        $ids = explode(",", $request->request->get("ids"));

        foreach ($ids as $id)
        {
            $videoLibrary = new videoLibrary();
            $videoLibrary->partnerid = $account->partnerid;
            $videoLibrary->mediaid = $id;
            $em->persist($videoLibrary);
        }
        $em->flush();
        return new Response("saved");
    }

    public function toggleMediaAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('adminsession');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $session->userid));
        $id = $request->request->get("id");
        $checked = $request->request->get("checked");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:videoLibrary');
        $library = $repository->findOneBy(array('partnerid' => $account->partnerid, "mediaid" => $id));
        $saved = 0;
        if ($library == null && $checked == "true")
        {
            $videoLibrary = new videoLibrary();
            $videoLibrary->partnerid = $account->partnerid;
            $videoLibrary->mediaid = $id;
            $em->persist($videoLibrary);
        }
        else if ($checked == "false" && $library != null)
        {
            $em->remove($library);
        }
        $em->flush();
        return new Response("saved");
    }

    public function viewPdfAction()
    {
        $file = getcwd() . "/newsletters/VWISE_NEWSLETTER_QUARTER_1_FINAL_small_file_size.pdf";
        $filename = 'Custom file name for the.pdf'; /* Note: Always use .pdf at the end. */
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Accept-Ranges: bytes');
        return new Response(@readfile($file));
    }

}
