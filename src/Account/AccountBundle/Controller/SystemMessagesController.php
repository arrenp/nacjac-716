<?php

namespace Account\AccountBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sessions\AdminBundle\Classes\adminsession;
class SystemMessagesController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","mailbox");
        $this->get("AccountsUsersSystemMessagesService")->markIsRead($session->clientid);
        return $this->render("index.html.twig");
    }    
    public function datatablesAction(Request $request)
    {
        $response= new \stdClass();
        $response->data = array();
        $session = $this->get('adminsession');
        $count = $this->get("AccountsUsersSystemMessagesService")->getCountOfUnreadMessages($session->clientid,false);
        $messages= $this->get("SystemMessagesService")->dataTablesQuery($request,false,$session->clientid)->fetchAll(); 
        $messageCount = $this->get("SystemMessagesService")->dataTablesQuery($request,true,$session->clientid)->fetch()['count'];
        foreach ($messages as $message)
        {
            $tags = $this->get("SystemMessagesTagsService")->getTagsByMessageId($message['smid']);
            if ($message['expirationDate'] != null)
            {
                $message['expirationDate'] = explode(" ",$message['expirationDate'])[0];
            }
            $row = array();
            $row[] = $message['id'];
            $row[] = $this->get("SystemMessagesService")->dataTablesMessage($message['message']);
            $row[] = parent::render("ManageManageBundle:SystemMessages:Search/messagestags.html.twig",array("tags" => $tags))->getContent();
            $row[] = $message['expirationDate'];
            $row[] = $this->render("Search/actionbuttons.html.twig",array("message" => $message ))->getContent();
            $response->data[] = $row;
        }
        $response->recordsTotal = $count;
        $response->recordsFiltered = $messageCount;
        return new JsonResponse($response);
    }
    public function deleteMessageAction(Request $request)
    {
        return $this->render("deletemessage.html.twig");
    }
    public function deleteMessageSavedAction(Request $request)
    {
        return new JsonResponse($this->get("AccountsUsersSystemMessagesService")->deleteMessage($request->request->get("id")));
    }
    public function render($file,$params = array())
    {
        return parent::render('AccountAccountBundle:SystemMessages:'.$file,$params);
    }
    public function viewSystemMessageAction(Request $request)
    {      
        return $this->render("viewmessage.html.twig",["message" => $this->get("SystemMessagesService")->findOneBy(["id" =>$request->query->get("id")])]);
    }
}
