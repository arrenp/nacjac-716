<?php

namespace Account\AccountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use classes\classBundle\Entity\accountsUsers;
use classes\classBundle\Entity\advisersPlans;
use classes\classBundle\Entity\accountsUsersPlans;
use classes\classBundle\Entity\outreachCategoriesDeny;
use classes\classBundle\Entity\outreachTemplatesDeny;
use classes\classBundle\Entity\accountsUsersAdditionalRoles;
use Sessions\AdminBundle\Classes\adminsession;

class UsersController extends Controller
{

    public function loginasAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array("id" => $request->query->get("id", "")));
        return $this->render('AccountAccountBundle:Default:loginas.html.twig', array("user" => $user));
    }

    public function loginasSavedAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array("id" => $request->request->get("id", "")));
        if ($user == null)
        {
            $user = $repository->findOneBy(array("id" => $request->query->get("id", "")));
        }
        if ($user != null)
        {
            $session = $this->get('adminsession');
            if ($session->masterRoleType == "vwise_admin" || $session->masterRoleType == "vwise" || ($session->masterRoleType == "admin_master" && ($user->roleType == "admin" || $user->roleType == "adviser") ) || ($session->masterRoleType == "admin" && $user->roleType == "adviser")   ) 
            {
                $session->set("newclientid", $user->id);
            }
        }
        return new Response("");
    }

    public function returnToOriginalAccountAction()
    {
        $session = $this->get('adminsession');
        $session->set("newclientid", $session->masterid);
        return new Response("");
    }

    public function loginEnabledAction()
    {
        $session = $this->get('adminsession');
        if ($session->newclientid == "") return new Response("true");
        else return new Response("false");
    }

    public function addAction()//add a user
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $userid = $this->userid();
        if ($request->query->get("id") != "") $userid = $request->query->get("id");
        $userFields = $this->userFields();
        $session = $this->get('adminsession');
        $types = $this->typesAvailable($session);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $admin_master_existant = $repository->findOneBy(array("roleType" => "admin_master", "userid" => $userid));
        if ($admin_master_existant != null)
        {
            for ($i = 0; $i < count($types); $i++) if ($types[$i] == "admin_master") unset($types[$i]);
        }
        $roleList = $this->roleList(null, null, $types);
        return $this->render('AccountAccountBundle:Default:adduser.html.twig', array('userFields' => $userFields, 'parentid' => $userid, 'roleList' => $roleList));
    }

    public function typesAvailable($parameter)//could be session or user
    {
        $types = null;
        if ($parameter->roleType == "admin") $types = array("adviser");

        if ($parameter->roleType == "admin_master") $types = array("adviser", "admin");

        if ($parameter->roleType == "vwise") $types = array("adviser", "admin", "admin_master");


        if ($parameter->roleType == "vwise_admin") $types = array("adviser", "admin", "admin_master", "vwise", "vwise_admin");

        return $types;
    }

    public function getRoleTypeAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $roleid = $request->request->get("roleid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRolesTypes');
        $role = $repository->findOneBy(array("roleid" => $roleid));
        return new Response($role->type);
    }

    public function roleList($roleType = null, $accountsUsersId = null, $availableTypes = null)
    {

        if ($roleType == null && $availableTypes == null) $accountsRoleTypesFindBy = array();
        else if ($roleType != null) $accountsRoleTypesFindBy = array("type" => $roleType);
        else if ($availableTypes != null) $accountsRoleTypesFindBy = array("type" => $availableTypes);

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRolesTypes');
        $roleTypes = $repository->findBy($accountsRoleTypesFindBy, array("type" => "ASC"));
        $roles = array();
        $userRoles = array();
        $i = 0;
        if ($roleType != null && $accountsUsersId != null)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
            $currentaccount = $repository->findOneBy(array("id" => $accountsUsersId));
            $userRoles[] = $currentaccount->roleid;
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersAdditionalRoles');
            $additionalRoles = $repository->findBy(array("accountsUsersId" => $accountsUsersId));
            foreach ($additionalRoles as $role)
            {
                $userRoles[] = $role->roleid;
            }
        }
        foreach ($roleTypes as $roleType)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRoles');
            $role = $repository->findOneBy(array("id" => $roleType->roleid));
            if ($role != null)
            {
                $roles[$i] = new \stdClass();
                $roles[$i]->name = $role->name;
                $roles[$i]->checked = "";
                $roles[$i]->type = $roleType->type;
                $roles[$i]->id = $role->id;
                foreach ($userRoles as $userRole)
                {
                    if ($roles[$i]->id == $userRole) $roles[$i]->checked = "checked";
                }
                $i++;
            }
        }
        return $roles;
    }

    public function addTemplatesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $userid = $request->request->get("userid");
        $clientid = $request->request->get("clientid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $theclient = $repository->findOneBy(array('id' => $clientid));
        if ($theclient->roleType == "adviser")
        {
            $theuser = $repository->findOneBy(array('userid' => $userid), array("id" => "DESC"));

            $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachCategoriesDeny');
            $categories = $repository->findBy(array('userid' => $userid, 'adviserid' => 0));

            $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachTemplatesDeny');
            $templates = $repository->findBy(array('userid' => $userid, 'adviserid' => 0));

            foreach ($categories as $category)
            {
                $outreachCategoriesDeny = new outreachCategoriesDeny();
                $outreachCategoriesDeny->userid = $userid;
                $outreachCategoriesDeny->categoryid = $category->categoryid;
                $outreachCategoriesDeny->adviserid = $theuser->id;
                $em->persist($outreachCategoriesDeny);
                $em->flush();
            }

            foreach ($templates as $template)
            {
                $outreachTemplatesDeny = new outreachTemplatesDeny();
                $outreachTemplatesDeny->userid = $userid;
                $outreachTemplatesDeny->templateid = $template->templateid;
                $outreachTemplatesDeny->adviserid = $theuser->id;
                $em->persist($outreachTemplatesDeny);
                $em->flush();
            }

            return new Response("added templates");
        }
        return new Response("not of role adviser, no templates added");
    }

    public function editAction(Request $request)
    {
        $session = $this->get('adminsession');
        $session->set("section", "Account");
        $id = $this->clientid($request);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $theuser = $repository->findOneBy(array('id' => $id));
        $userid = $theuser->userid;
        $userFields = $this->userFields();
        $this->copyUserAttributes($userFields, $theuser);
        $roleList = $this->roleList($theuser->roleType, $id);

        return $this->render('AccountAccountBundle:Default:edituser.html.twig', array('clientid' => $id, 'userFields' => $userFields, 'parentid' => $userid, "roleList" => $roleList, "theuser" => $theuser));
    }

    public function addAdditionalRolesAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $accountsUsersId = $request->request->get("accountsUsersId", "");
        $ids = $request->request->get("ids", "");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersAdditionalRoles');
        $oldroles = $repository->findBy(array('accountsUsersId' => $accountsUsersId));
        foreach ($oldroles as $role)
        {
            $em->remove($role);
            $em->flush();
        }
        if ($ids != "")
        {
            $ids = explode(",", $ids);
            foreach ($ids as $id)
            {
                $additionalRole = new accountsUsersAdditionalRoles();
                $additionalRole->accountsUsersId = $accountsUsersId;
                $additionalRole->roleid = $id;
                $em->persist($additionalRole);
                $em->flush();
            }
        }
        return new Response("Saved");
    }

    public function deleteAction(Request $request)
    {
        $session = $this->get('adminsession');
        $session->set("section", "Account");
        $id = $this->clientid($request);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array('id' => $id));
        return $this->render('AccountAccountBundle:Default:deleteuser.html.twig', array('clientid' => $id, 'user' => $user));
    }

    public function deleteAdviserDataAction()
    {

        $em = $this->getDoctrine()->getManager();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $userid = $this->userid();
        $adviserid = $request->request->get("adviserid", -1);

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:advisersPlans');
        $plans = $repository->findBy(array('adviserid' => $adviserid));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachCategoriesDeny');
        $categories = $repository->findBy(array('adviserid' => $adviserid));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachTemplatesDeny');
        $templates = $repository->findBy(array('adviserid' => $adviserid));
        $i = 0; //need a counter for big sets, flush after every remove is very slow
        foreach ($plans as &$plan)//send as reference for better memory optimization
        {
            $em->remove($plan);
            if ($i == 100)
            {
                $em->flush();
                $i = 0;
            }
            $i++;
        }

        foreach ($categories as $category)//keep code simple for small sets
        {
            $em->remove($category);
            $em->flush();
        }

        foreach ($templates as $template)
        {
            $em->remove($template);
            $em->flush();
        }
        $em->flush();
        return new Response("saved");
    }

    public function setPlansIsCheckedAction(Request $request, $planid)
    {
        $session = $request->getSession();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersPlans');
        $adviserPlan = $repository->findOneBy(array('accountsUsersId' => $session->get("editclientid"), 'planid' => $planid));
        if ($adviserPlan != null) return new Response("checked");
        else return new Response("");
    }

    public function setPlansTogglePlanAction(Request $request)
    {
        $session = $request->getSession();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $planid = $request->request->get("planid");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsersPlans');
        $adviserPlan = $repository->findOneBy(array('accountsUsersId' => $session->get("editclientid"), 'planid' => $planid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $adviser = $repository->findOneBy(array('id' => $session->get("editclientid")));


        if ($adviserPlan != null) $em->remove($adviserPlan);
        else
        {
            
            $adviserPlan = new accountsUsersPlans();


            $adviserPlan->userid = $adviser->userid;

            $repository = $em->getRepository('classesclassBundle:plans');
            $adviserPlan->plan = $repository->findOneBy(array("id" => $planid));
            $adviserPlan->accountsUsersId = $session->get("editclientid");
            $em->persist($adviserPlan);
        }
        $em->flush();
        return new Response("saved");
    }

    public function setplansAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $theadviser = $repository->findOneBy(array('id' => $request->query->get("id")));
        $userid = $theadviser->userid;
        $this->clientid($request);
        return $this->render('AccountAccountBundle:Default:setadviserplans.html.twig', array("userid" => $userid));
    }

    private function userid()//returns current userid
    {
        $session = $this->get('adminsession');
        $userid = $session->userid;
        return $userid;
    }

    private function clientid(Request $request)
    {
        $id = $request->query->get('id');
        $session = $request->getSession();


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $adviser = $repository->findOneBy(array("id" => $id));
        if ($adviser == null) return -1;

        $session->set("editclientid", $id);

        return $id;
    }

    public function userlistAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $parameters = array("userid" => $request->request->get("userid", ""));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $currentuser = $repository->findOneBy(array("id" => $session->clientid));

        $parameters['roleType'] = $this->typesAvailable($currentuser);

        $usersFound = $repository->findBy($parameters, array("id" => "desc"));
        $users = array();
        foreach ($usersFound as &$user)
        {
            if ($user->id != $currentuser->id) $users[] = $user;
        }
        return $this->render('AccountAccountBundle:Search:userlist.html.twig', array("items" => $users));
    }

    private function userFields()
    {
        $fields['firstname']['description'] = "First Name: ";
        $fields['lastname']['description'] = "Last Name: ";
        $fields['phone']['description'] = "Phone Number: ";
        $fields['phone2']['description'] = "Second Phone Number: ";
        $fields['address']['description'] = "Address: ";
        $fields['address2']['description'] = "Address 2: ";
        $fields['city']['description'] = "City: ";
        $fields['state']['description'] = "State: ";
        $fields['zip']['description'] = "Zip: ";
        $fields['email']['description'] = "Email: ";
        $fields['password']['description'] = "Password: ";
        return $fields;
    }

    private function copyUserAttributes(&$fields, $adviser) //copies doctrine adviser variable relevant fields
    {
        foreach ($fields as $key => $value)
        {
            if (isset($adviser->$key)) $fields[$key]['value'] = $adviser->$key;
        }
        $fields["email"]['value'] = $adviser->getEmail();
        $fields['password']['value'] = "";
    }
    
    public function addUserAction(Request $request)
    {
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $user = new accountsUsers();
        $repository = $em->getRepository('classesclassBundle:accounts');
        $user->account = $repository->findOneBy(array("id" => $arr['userid']));  
        $this->userCopyRequestObject($user,$arr);
        if (count($user->errors) == 0)
        {
            $date = new \DateTime();
            $date->modify('+180 days');
            $user->passwordExpirationDate = $date;
            $em->persist($user);
            $em->flush();
        }
        $response = new JsonResponse(array("errors" => $user->errors,"id" => $user->id));
        return $response;
    }
    public function editUserAction(Request $request)
    {
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array("id" => $arr['id'])); 
        $this->userCopyRequestObject($user,$arr);
        if (count($user->errors) == 0) {
            $em->flush();
        }
        $response = new JsonResponse(array("errors" => $user->errors,"id" => $user->id));
        return $response;
    }    
    public function userCopyRequestObject($user,$arr)
    {
        foreach ($arr as $key => $value)
        {
            if (!in_array($key, array("email", "password", "enabled"))) {
                $user->$key = $value;
            }
        }
        if ($arr['email'] != "")
        {
            $user->setEmail($arr['email']);
            $user->setUsername($arr['email']);
            $user->setEnabled(1);
        }
        $user->errors = array();
        if ($arr['password'] != "") {
            $user->setPlainPassword($arr['password']);
        }
    }
    public function unlockUserAction(Request $request)
    {
        return $this->render('AccountAccountBundle:Default:unlockuser.html.twig');
    }
    public function unlockUserSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array("id" => $request->request->get("id")));    
        $user->unlock();
        $em->flush();
        return new Response("");
    }    
}
