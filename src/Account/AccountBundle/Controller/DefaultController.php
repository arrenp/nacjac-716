<?php

namespace Account\AccountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Classes\dataTablesSearch;
class DefaultController extends Controller
{
	public function checkCredsAction()
	{
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
		$emailCheck= $repository->findOneBy(array('email' => $request->request->get('email','')));
		if ($emailCheck == null)
		return new Response(0);
		else
		{
			$emailCheck= $repository->findOneBy(array('id' => $request->request->get('id','')));
			if ($emailCheck == null)
			return new Response(1);
			if ($emailCheck->getEmail() == $request->request->get('email','') )
			return new Response(0);
			return new Response(1);
		}
	}
	public function saveContactInfoAction()
	{
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$session =  $this->get('adminsession');
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
		$currentuser = $repository->findOneBy(array('id' => $session->clientid ));	
		$em = $this->getDoctrine()->getManager();
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
		$currentaccount = $repository->findOneBy(array('id' => $session->userid ));
		$arr =  $request->request->all();
		while (list($key, $value) = each($arr))
	    {
	    	if ($key != "email" && $key != "password" && $key != "" )
	    	{
		    	$currentuser->$key = $value;
		    	$currentaccount->$key = $value;
	    	}
	    }
	    $currentuser->setEmail($request->request->get("email"));
	    $currentuser->setUsername($request->request->get("email"));
        $currentuser->setConfirmationToken(null);
        $currentuser->errors = array();
	    if (trim($request->request->get("email")) != "" && trim($request->request->get("newPassword")) != "" ) {
	        $currentuser->setPlainPassword($request->request->get("newPassword"));
	    }
		$this->get('fos_user.user_manager')->updateUser($currentuser, false);
	    if (count($currentuser->errors) == 0) {
            $em->flush();
        }
        $response = new JsonResponse(array('errors' => $currentuser->errors));
	   return  $response;
	}	
    public function indexAction()
    {
		$session =  $this->get('adminsession');
		$session->set("section","Account");
		$request = Request::createFromGlobals();
    	$request->getPathInfo();
		$permissions = $this->get('rolesPermissions');
		$writeable = $permissions->writeable("Account");
		$generalfunctions = $this->get('generalfunctions');		

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
		$user = $repository->findOneBy(array('id' => $session->clientid ));	


		$repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
		$account = $repository->findOneBy(array('id' => $session->userid ));	

    	return $this->render('AccountAccountBundle:Default:index.html.twig', array('user' => $user,'account' => $account,'currentuser' => $currentuser, 'userlogincreds' => $logincreds,"writeable" => $writeable,"session" => $session));
    }

    public function testDataTablesAction()
    {
    	//$postDatatable = $this->get("sg.datatable.view");
    	//$repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
		//$accounts = $repository->findAll();
		//$header = ""
		//foreach ($accounts as $account)
		//{
    	//}
    	$request = Request::createFromGlobals();
    	$request->getPathInfo();
    	$search = new dataTablesSearch($this,$request);
    	$search->search();



		return new Response("");

    }
}
