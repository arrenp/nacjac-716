<?php
namespace Manage\ManageBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use classes\classBundle\Entity\TranslationSources;
use classes\classBundle\Entity\DefaultTranslations;
use classes\classBundle\Entity\AccountsTranslations;
class TranslationUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:TranslationUpdate')->setDescription('Updates default translations')->setHelp('Read description');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write("Enter Partnerid (leave blank if not none): ");
        $partnerId = rtrim(fgets(STDIN));
        $em = $this->getContainer()->get('doctrine')->getManager();
        $findByParams  = ["active" => 1,"partnerid" => null];
        $accountId = 0;
        if (!empty($partnerId))
        {
            $account = $em->getRepository('classesclassBundle:accounts')->findOneBy(["partnerid" => $partnerId  ]);
            if (!empty($account))
            {
                $accountId = $account->id;
                $findByParams["partnerid"] = $account->partnerid;;  
            }
        }
        $TranslationQueue = $em->getRepository('classesclassBundle:TranslationQueue')->findBy($findByParams);
        foreach ($TranslationQueue as $translation)
        {
            $language = $em->getRepository('classesclassBundle:languages')->findOneBy(["name" => $translation->language ]);
            $TranslationSource = $em->getRepository('classesclassBundle:TranslationSources')->findOneBy(["sourceKey" => $translation->sourceKey,"domain" => $translation->domain]);
            if ($TranslationSource == null)
            {
                $TranslationSource = new TranslationSources();
                $TranslationSource->sourceKey = $translation->sourceKey;
                $TranslationSource->domain = $translation->domain;  
                $em->persist($TranslationSource);
                $em->flush();
            }
            $DefaultTranslation = $em->getRepository('classesclassBundle:DefaultTranslations')->findOneBy(["sourceId" => $TranslationSource->id,"languageId" => $language->id]);
            if ($DefaultTranslation == null)
            {
                $DefaultTranslation = new DefaultTranslations();
                $DefaultTranslation->translationSource = $TranslationSource;
                $DefaultTranslation->languageId = $language->id;
                $DefaultTranslation->content = $translation->content;
                $em->persist($DefaultTranslation);
            } else {
                if (empty($accountId))
                {
                    $DefaultTranslation->content = $translation->content;
                }
            }        
            if (!empty($accountId))
            {                
                $AccountTranslation = $em->getRepository('classesclassBundle:AccountsTranslations')->findOneBy(["accountId" => $accountId,"sourceId" => $TranslationSource->id,"languageId" => $language->id]);  
                if ($AccountTranslation == null)
                {
                    $AccountTranslation = new AccountsTranslations();
                    $AccountTranslation->accountId = $accountId;
                    $AccountTranslation->translationSource = $TranslationSource;
                    $AccountTranslation->languageId = $language->id;
                    $AccountTranslation->content = $translation->content;
                    $em->persist($AccountTranslation);
                } else {
                    $AccountTranslation->content = $translation->content;
                }                
            }
            $translation->active = 0;
            $em->flush();
        }
        $output->writeln("Moved over translations");
    }
}
