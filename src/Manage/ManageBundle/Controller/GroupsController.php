<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Entity\communicationSpecialist;
use classes\classBundle\Entity\accountManagers;
class GroupsController extends Controller
{
    public $groups = array();
    public $sections = array();
    public function indexAction($group)
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        $this->setGroups();
        $group = $this->groups[$group];
        return $this->render('ManageManageBundle:Groups:index.html.twig', array("group" => $group) );
    }
    public function setGroups()
    {
        $this->addGroup("accountManagers","Account Manager");
        $this->addGroup("communicationSpecialist","Communication Specialist");
    }
    public function addUserAction(Request $request)
    {
        $this->init($request);
        $this->sections($request->query->get("group"));       
        return $this->render('ManageManageBundle:Groups:adduser.html.twig',array("group" => $request->query->get("group"),"sections" => $this->sections));
    }
    public function addUserSavedAction(Request $request)
    {
        $this->init($request);
        $groupName = $request->request->get("group");
        if ($groupName == "accountManagers")
        $group = new accountManagers();
        if ($groupName == "communicationSpecialist")
        $group = new communicationSpecialist();
        $this->copyToGroupObject($group,$request);
        $this->em->persist($group);
        $this->em->flush();
        return new Response("");
    }    
    public function editUserAction(Request $request)
    {
        $this->init($request);
        $this->sections($request->query->get("group"),$request->query->get("id"));
        return $this->render('ManageManageBundle:Groups:edituser.html.twig',array("group" => $request->query->get("group"),"id" =>  $request->query->get("id"),"sections" => $this->sections));
    }
    public function editUserSavedAction(Request $request)
    {
        $this->init($request);
        $repository = $this->em->getRepository('classesclassBundle:'.$request->request->get("group"));
        $group= $repository->findOneBy(array("id" => $request->request->get("id")));       
        $this->copyToGroupObject($group,$request);
        $this->em->flush();
        return new Response("");
    }        
    public function deleteUserAction(Request $request)
    {
        $this->init($request);
        $this->sections($request->query->get("group"),$request->query->get("id"));
        return $this->render('ManageManageBundle:Groups:deleteuser.html.twig',array("group" => $request->query->get("group"),"id" =>  $request->query->get("id"),"sections" => $this->sections));
    }    
    public function deleteUserSavedAction(Request $request)
    {
        $this->init($request);
        $repository = $this->em->getRepository('classesclassBundle:'.$request->request->get("group"));
        $group= $repository->findOneBy(array("id" => $request->request->get("id")));   

        
        $connection = $this->get('doctrine.dbal.default_connection');
        $sql = "SELECT id,".$this->accountGroupField($request->request->get("group"))." FROM accounts WHERE ".$this->accountGroupField($request->request->get("group"))." LIKE '%".$request->request->get("id")."%'";
        $accounts= $connection->fetchAll($sql);
        
        foreach ($accounts as $account2) 
        {
            $repository = $this->em->getRepository('classesclassBundle:accounts');
            $account= $repository->findOneBy(array("id" => $account2['id'])); 
            $this->setAccountsGroup($account,$request->request->get("id"),"false",$request->request->get("group"));
        }
                         
        $this->em->remove($group);
        $this->em->flush();
        return new Response("");
    }        
    public function addGroup($name,$description)
    {
        $this->groups[$name] = array("name" => $name,"description" => $description);
    }
    public function init($request = null)
    {

        $this->em = $this->getDoctrine()->getManager();
    }
    public function sections($group,$id = 0)
    {
        $repository = $this->em->getRepository('classesclassBundle:'.$group);
        $user= $repository->findOneBy(array("id" => $id));
        $this->addSection("First Name","firstName",$user);
        $this->addSection("Last Name","lastName",$user);
        $this->addSection("Email Address","emailAddress",$user);
        $this->addSection("Phone Number","phoneNumber",$user);
    }
    public function addSection($description,$name,$user)
    {
        $this->sections[$name] = array("name" => $name,"description" => $description);
        if ($user != null)
        $this->sections[$name]['value'] = $user->$name;
        else
        $this->sections[$name]['value'] = "";    
    }
    public function copyToGroupObject(&$group,$request)
    {
        $requestArray = $request->request->all();
        foreach ($requestArray as $key => $value)
        {
            if ($key === "group") {
                continue;
            }
            if ($key === "emailAddress") {
                $value = trim($value);
            }
            $group->$key = $value;
        }        
    }
    public function setGroupsForAccountAction(Request $request)
    {
        $this->setGroups();
        $request = $request->query->all();
        return $this->render('ManageManageBundle:Groups:setgroupsforaccount.html.twig',array("groups" => $this->groups,"request" => $request));
    }
    public function setGroupsForAccountShowGroupAction(Request $request)
    {    
        $request = $request->request->all();
        
        return $this->render('ManageManageBundle:Groups:setgroupsforaccountshowgroup.html.twig',array("request" => $request));      
    }
    public function setGroupsForAccountCheckedAction($userid,$group,$id)
    {
        $this->init();
        $repository = $this->em->getRepository('classesclassBundle:accounts');
        $account= $repository->findOneBy(array("id" => $userid)); 
        $field  = $this->accountGroupField($group);
        $explodeField = explode(",",$account->$field);
        foreach($explodeField as &$field2)
        {
            if ($field2 == $id)
            return new Response("checked");
        }
        return new Response("");
    }
    public function setGroupsForAccountCheckedSaveAction(Request $request)
    {
        $this->init();
        $repository = $this->em->getRepository('classesclassBundle:accounts');
        $account= $repository->findOneBy(array("id" => $request->request->get("userid"))); 
        $this->setAccountsGroup($account,$request->request->get("id"),$request->request->get("checked"),$request->request->get("group"));
        return new Response("");
    }
    public function setAccountsGroup($account,$id,$checked,$group)
    {
        $field  = $this->accountGroupField($group);

        $explodeField = array();
        if (trim($account->$field) != "")
        $explodeField = explode(",",$account->$field);
        if ($checked == "true")
        {
            
            if (!in_array($id,$explodeField))
            {
                $explodeField[] = $id;
                $account->$field = implode(",",$explodeField);
            }
        }
        if ($checked == "false")
        {
            $count = count($explodeField);
            for ($i = 0; $i < $count; $i++)
            if ($explodeField[$i] == $id)
            {
                unset($explodeField[$i]);
                break;
            }
            $account->$field = implode(",",$explodeField);
        }
        $this->em->flush();        
        
    }
    
    public function viewAccountGroupsAction($userid)
    {
        $this->init();
        $repository = $this->em->getRepository('classesclassBundle:accounts');
        $account= $repository->findOneBy(array("id" => $userid));
        $this->setGroups();
        $response = "";
        
        foreach($this->groups as $group)
        {
            $field = $this->accountGroupField($group['name']);           
            if ($account->$field != "")
            {
                
                $response = $response."
                <hr/>
                <h3>".$group['description']."</h3>
                <table class = \"table table-striped\">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email Address</th>
                            <th>Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>";
                
                $ids = explode(",",$account->$field);
                foreach ($ids as $id)
                {
                    $repository = $this->em->getRepository('classesclassBundle:'.$group['name']);
                    $currentGroup= $repository->findOneBy(array("id" => $id));
                    $response = $response."<tr><td>".$currentGroup->firstName."</td><td>".$currentGroup->lastName."</td><td>".$currentGroup->emailAddress."</td><td>".$currentGroup->phoneNumber."</td>";

                }
                $response = $response."</body></table>";
                 
            } 
        }
        return new Response($response);
    }
    public function accountGroupField($group)
    {
        if ($group == "communicationSpecialist")
        return "communicationSpecialist";
        if ($group == "accountManagers")
        return "accountManager";    
    }
}
?>
