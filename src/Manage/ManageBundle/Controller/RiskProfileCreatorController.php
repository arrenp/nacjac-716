<?php

namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sessions\AdminBundle\Classes\adminsession;
class RiskProfileCreatorController extends Controller
{
    public function indexAction(Request $request)
    {                
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        $params = $request->query->all();
        $this->calculateParamsType($params);
        $params['urlParams'] = $this->paramsToUrlParams($params);
        $params['questionnaires'] = $this->get("RiskProfileQuestionnaireService")->getQuestionnairesAll($params['type'],$params);
        $session->set("questionnaireUrl",$request->getUri());;
        return $this->render("index.html.twig",$params);
    }
    public function calculateParamsType(&$params)
    {
        $params['type'] = "Default";
        if (isset($params['userid']))
        {
            $params['type'] = "Account";
        }
        if (isset($params['planid']))
        {
            $params['type'] = "Plan";
        }          
    }
    public function paramsToUrlParams($params)
    {
        $urlParams = "";
        foreach ($params as $key => $value)
        {           
            if ($urlParams != "")
            {
                $urlParams = $urlParams."&";
            }
            $urlParams = $urlParams.$key."=".$value;
        }
        return $urlParams;
    }
    public function addQuestionnaireAction(Request $request)
    {       
        return $this->render("addquestionnaire.html.twig", $request->query->all());
    }
    public function addQuestionnaireSavedAction(Request $request)
    {       
        $params = $request->request->all();
        $this->calculateParamsType($params);
        $this->get("RiskProfileQuestionnaireService")->addQuestionnaire($params);
        return new Response("saved");
    }
    public function render($file,$params = array()) 
    {
        return parent::render('ManageManageBundle:RiskProfileCreator:'.$file,$params); 
    }
    public function loadCurrentQuestionnairesAction(Request $request)
    {
        $params = $request->request->all();
        $this->calculateParamsType($params);
        $params['questionnaires'] = $this->get("RiskProfileQuestionnaireService")->getQuestionnairesAll($params['type'],$params);
        return $this->render("currentquestionnaires.html.twig", $params);
    }
    public function saveSelectedAction(Request $request)
    {
        $params = $request->request->all();
        $this->calculateParamsType($params);  
        $findByParams = array();
        if ($params['type'] == "Account")
        {
            $findByParams = ["userid" => $params['userid']];
        }
        else if ($params['type'] == "Plan")
        {
            $findByParams = ["planid" => $params['planid']];
        }
        $riskProfileQuestionnaireSelected = $this->get($params['type']."RiskProfileQuestionnaireService")->findOneBy(array_merge($findByParams,["selected" => true]));        
        $riskProfileQuestionnaire = $this->get($params['type']."RiskProfileQuestionnaireService")->findOneBy(array_merge($findByParams, ['riskProfileQuestionnaireId' => $params['riskProfileQuestionnaireId']]));
        if ($riskProfileQuestionnaire  == null)
        {
            $riskProfileQuestionnaire = $this->get($params['type']."RiskProfileQuestionnaireService")->add(array_merge($findByParams,['riskProfileQuestionnaireId' => $params['riskProfileQuestionnaireId']]));
        }
        $riskProfileQuestionnaireSaved = $this->get($params['type']."RiskProfileQuestionnaireService")->save($riskProfileQuestionnaire,["selected" => true]);
        if ($riskProfileQuestionnaireSelected != null && $riskProfileQuestionnaireSelected->id != $riskProfileQuestionnaireSaved->id)
        {
            $this->get($params['type']."RiskProfileQuestionnaireService")->save($riskProfileQuestionnaireSelected,["selected" => false]);
        }
        $riskProfileQuestionnaireService= $this->get("RiskProfileQuestionnaireService");
        $riskProfileQuestionnaireService->init();
        if (isset($params['stadion'])) 
            $riskProfileQuestionnaireService->saveStadion($params['type'], $params['stadion'], $findByParams);       
        foreach ($riskProfileQuestionnaireService->pathSelectedFields as $field)    
            if($params[$field])
                $this->get("RiskProfileQuestionnaireService")->saveField($params['type'], $params[$field], $findByParams,$field);
        return new Response("");
    }
    public function editQuestionnaireAction(Request $request)
    {
        $questionnare = $this->get("RiskProfileQuestionnaireService")->findOneBy(["id" =>$request->query->get("id")]);
        return $this->render("editquestionnaire.html.twig",["questionnaire" => $questionnare]);
    }
    public function editQuestionnaireSavedAction(Request $request)
    {
        $questionnare = $this->get("RiskProfileQuestionnaireService")->findOneBy(["id" =>$request->request->get("id")]);
        $this->get("RiskProfileQuestionnaireService")->save($questionnare,["name" => $request->request->get("name")]);
        return new Response("");
    }
    public function deleteQuestionnaireAction(Request $request)
    {
        $questionnare = $this->get("RiskProfileQuestionnaireService")->findOneBy(["id" =>$request->query->get("id")]);
        return $this->render("deletequestionnaire.html.twig",["questionnaire" => $questionnare]);
    }
    public function deleteQuestionnaireSavedAction(Request $request)
    {
        $this->get("RiskProfileQuestionnaireService")->delete($request->request->get("id"));
        return new Response("");
    }
    public function setScoringXmlAction(Request $request)
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        $questionnaire = $this->get("RiskProfileQuestionnaireService")->findOneBy(["id" =>$request->query->get("id")]);
        return $this->render("editscoring.html.twig",["questionnaire" => $questionnaire,"types" => $this->get("RiskProfileQuestionnaireService")->customScoringTypes() ]);
    }
    
    public function validateXmlAction(Request $request)
    {
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($request->request->get("xml"));
        if ($xml === false) 
        {
            return new JsonResponse(["success" => false,"errors" =>libxml_get_errors()]);
        } 
        $questionnaire = $this->get("RiskProfileQuestionnaireService")->findOneBy(["id" =>$request->request->get("id")]);
        $params = array();
        $params['scoringXml'] = trim($request->request->get("xml"));
        $customScoringType = $request->request->get("customScoringType");
        if ($customScoringType == "")
        {
            $customScoringType = null;
        }
        $params['customScoringType'] =$customScoringType;
        $this->get("RiskProfileQuestionnaireService")->save($questionnaire,$params);
        return new JsonResponse(["success" => true]);        
    }
}