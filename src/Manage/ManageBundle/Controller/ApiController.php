<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\accounts;
use classes\classBundle\Entity\APIAccounts;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Classes\plansMethods;
use Permissions\RolesBundle\Classes\rolesPermissions;
class ApiController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");       
        return $this->render('ManageManageBundle:Api:index.html.twig');        
    }
    public function listkeysAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get('id');
        $writeable = $this->writeable();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:product');
        $products= $repository->findAll();       
        return  $this->render('ManageManageBundle:Api:listkeys.html.twig',array("id" => $id,"writeable" => $writeable,"products" => $products));
    }
    public function generateKeyAction()
    {
        $session = $this->get('adminsession');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $currentaccount = $repository->findOneBy(array('id'=>$session->userid));
        $salt = uniqid(mt_rand());  
        $key = hash('sha1', $salt.$currentaccount->partnerid);
        return new Response($key);
    }

    public function writeable()
    {
        $permissions = $this->get('rolesPermissions');
        return $permissions->writeable("ManageApi");
    }
    public function createSelectProductKeyBoxAction($id)
    {
        $select = '<select id =\"productid'.$id.'\">';
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:product');
        $products= $repository->findAll(); 
        $repository = $em->getRepository('classesclassBundle:APIAccounts');
        $apiAccount = $repository->findOneBy(array("id" => $id)); 
        foreach ($products as $product)
        {
            $selected = "";
            if ($product->id == $apiAccount->productid)
            $selected = "selected";
            $select = $select.'<option value = \"'.$product->id.'\" '.$selected.'>'.$product->name.'</option>';
        }
        $select = $select."</select>";
        return new Response($select);
    }
}
?>