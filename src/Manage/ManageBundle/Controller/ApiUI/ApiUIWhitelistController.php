<?php

namespace Manage\ManageBundle\Controller\ApiUI;

use classes\classBundle\Entity\ApiWhitelist;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiUIWhitelistController extends BaseApiController {
    
    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        return $this->render("ManageManageBundle:ApiUI:ApiUIWhitelist/index.html.twig");
    }

    public function addAction(Request $request) {
                $action = "add";
		return $this->render('ManageManageBundle:ApiUI:ApiUIWhitelist/editApiWhitelist.html.twig', ['action' => $action,"button" => $this->getButtonAttrs($action)]);
	}
    
    public function editAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:ApiWhitelist");
        $ip = $repository->find($request->get("id"));
        $action = "edit";
        return $this->render('ManageManageBundle:ApiUI:ApiUIWhitelist/editApiWhitelist.html.twig', ['action' => $action, 'ip' => $ip,"button" => $this->getButtonAttrs($action) ]);
    }
    
    public function saveAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:ApiWhitelist");
        if ($request->get("action") === "edit") {
            $ip = $repository->find($request->request->get("id"));
        }
        else {
            $ip = $repository->findOneBy(['ip' => trim($request->request->get("ip"))]);
        }
        if ($ip === null) {
            $ip = new ApiWhitelist();
            $em->persist($ip);
        }
        $ip->setIp($request->request->get("ip"));
        $em->flush();
        return new Response("success");
    }
    
	public function deleteAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$ip = $this->getDoctrine()->getRepository("classesclassBundle:ApiWhitelist")->find($request->request->get("id"));
		$em->remove($ip);
		$em->flush();
		return new Response("success");
	}

}
