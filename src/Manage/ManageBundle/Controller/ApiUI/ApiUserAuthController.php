<?php

namespace Manage\ManageBundle\Controller\ApiUI;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\accounts;
use classes\classBundle\Entity\APIAccounts;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Classes\plansMethods;
use Permissions\RolesBundle\Classes\rolesPermissions;
use classes\classBundle\Entity\ApiUserAuth;
use Symfony\Component\HttpFoundation\JsonResponse;
class ApiUserAuthController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        return $this->render('ManageManageBundle:ApiUI:ApiUserAuth/index.html.twig');
    }

    public function ApiUserAuthAction()
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUser');
        $user = $repository->findAll();
    }

    public function addApiUserAuthAction()
    {
        return $this->render('ManageManageBundle:ApiUI:ApiUserAuth/add.html.twig');
    }

    public function addApiUserAuthSavedAction(Request $request)
    {
        $apiUserId = $request->request->get('apiUserId');
        $apiId = $request->request->get('apiId');
        $enabled = $request->request->get('enabled');

        $insert = new ApiUserAuth();
        $insert->setApiUserId($apiUserId);
        $insert->setApiId($apiId);
        $insert->setEnabled(($enabled == 'Yes' ? 1 : 0));

        $em = $this->getDoctrine()->getManager();
        $em->persist($insert);
        $em->flush();

        return new Response('saved');
    }

    public function editApiUserAuthAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUserAuth');
        $id = $request->query->get("id");
        $user = $repository->findOneBy(array('id' => $id));

        return $this->render('ManageManageBundle:ApiUI:ApiUserAuth/edit.html.twig', array("user" => $user));
    }

    public function editApiUserAuthSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $apiUserId = $request->request->get('apiUserId');
        $apiId = $request->request->get('apiId');
        $enabled = $request->request->get('enabled');

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUserAuth');
        $user = $repository->findOneBy(array("id" => $id));
        $user->setApiUserId($apiUserId);
        $user->setApiId($apiId);
        $user->setEnabled(($enabled == 'Yes' ? 1 : 0));

        $em->persist($user);
        $em->flush();

        return new Response("Saved");
    }

    function deleteApiUserAuthAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUserAuth');
        $user = $repository->findOneBy(array('id' => $request->query->get("id")));

        return $this->render('ManageManageBundle:ApiUI:ApiUserAuth/delete.html.twig', array("user" => $user));
    }

    public function deleteApiUserAuthSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUserAuth');
        $user = $repository->findOneBy(array('id' => $id));

        $em->remove($user);
        $em->flush();
        return new Response("Deleted");

    }
    
    public function searchApiUserAuthAction(Request $request) {
        $fields = ['a.id', "AES_DECRYPT(username, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."'))", 'CONCAT(api.controller,"/",api.method)', 'a.enabled'];
        $connection = $this->get("database_connection");
        $req = $request->request->all();
        $fromstring = " FROM api_user_auth AS a LEFT JOIN api_user AS user ON user.id = a.api_user_id LEFT JOIN apis AS api ON api.id = a.api_id ";
        
        $parameters = [];
        $search = $request->get("search");
        $searchstring = "";
        if (isset($search['value']) && $search['value'] !== '') {
            $searchstring .= " WHERE 1=0 ";
            foreach ($fields as $field) {
                $searchstring .= " OR $field LIKE :search ";
                $parameters[':search'] = '%'.$search['value'].'%';
            }
        }

        $orderByArr = [];
        foreach ($req["order"] as $val) {
            $orderBy = $req["columns"][$val["column"]]["data"];
            if (isset($fields[$orderBy])) {
                $dir = strtolower($val["dir"]) === "desc" ? "desc" : "asc";
                $orderByArr[] = "{$fields[$orderBy]} $dir";
            }
        }
        $orderByString = !empty($orderByArr) ? " ORDER BY " . implode(", ", $orderByArr) : "";

        $queryCountString = "SELECT COUNT(a.id) $fromstring";
        $recordsTotal = $connection->fetchColumn($queryCountString, $parameters);

        $queryFilteredCountString = "SELECT COUNT(a.id) $fromstring $searchstring";
        $recordsFiltered = $connection->fetchColumn($queryFilteredCountString, $parameters);

        $queryString = sprintf("SELECT ".implode(",", $fields)." $fromstring $searchstring $orderByString LIMIT %d, %d", (int) $req["start"], (int) $req['length']);
        
        $statement = $connection->prepare($queryString);
        $statement->execute($parameters);
        $results = $statement->fetchAll(\PDO::FETCH_NUM);

        foreach ($results as &$row) {
            $row[] = '<span class="glyphicon glyphicon-edit" title="edit" onclick="ajaxModal(\''.$this->generateUrl('_manage_API_editApiUserAuth', ['id' => $row[0]]).'\',\'Edit User Auth\')"></span>'.'<span class="glyphicon glyphicon-trash" title="delete" onclick="ajaxModal(\''.$this->generateUrl('_manage_API_deleteApiUserAuth', ['id' => $row[0]]).'\',\'Delete User Auth\')"></span>';
        }
        
        return new JsonResponse([
            "draw" => (int) $req["draw"],
            "recordsTotal" => (int) $recordsTotal,
            "recordsFiltered" => (int) $recordsFiltered,
            "data" => $results
        ]);
    }
}
?>