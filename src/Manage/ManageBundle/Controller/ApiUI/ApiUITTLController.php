<?php

namespace Manage\ManageBundle\Controller\ApiUI;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\ApiTokenTtl;
use Sessions\AdminBundle\Classes\adminsession;
class ApiUITTLController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        return $this->render('ManageManageBundle:ApiUI:TTL/ttl.html.twig', ["token" =>  $this->getToken()]);
    }
    
    public function getToken()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:ApiTokenTtl');
        $ApiTokenTtl = $repository->findOneBy(array());
        if ($ApiTokenTtl == null)
        {
            $ApiTokenTtl = new ApiTokenTtl();
        }
        return $ApiTokenTtl;
    }
    public function saveTokenAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $token = $this->getToken();
        $token->setTtl($request->request->get("ttl"));
        $em->persist($token);
        $em->flush();
        return new Response("");
    }
}
?>
