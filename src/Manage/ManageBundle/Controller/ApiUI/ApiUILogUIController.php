<?php

namespace Manage\ManageBundle\Controller\ApiUI;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

ini_set('memory_limit','2G');
class ApiUILogUIController extends Controller
{
    private $fields = [
        'id' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
        'api_user_id' => ['orderable'=>true,'searchable'=>true,'isPulldown' => true],
        'api_id' => ['orderable'=>true,'searchable'=>true,'isPulldown' => true],
        'headers' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
        'response' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
        'ip' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
        'status' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
        'url' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
        'application' => ['orderable'=>true,'searchable'=>true,'isPulldown' => true],
        'timestamp' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
    ];


    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        $connection = $this->container->get("database_connection");

        $usersMap = $this->getApiUserId($connection);
        $apiIdMap = $this->getApiId($connection);

        $this->fields['api_user_id']['options'] = $usersMap;
        $this->fields['api_id']['options'] = $apiIdMap;
        $this->fields['application']['options'] = $this->getApiApplication($connection);


        return $this->render("ManageManageBundle:ApiUI:ApiUILogUI/index.html.twig", ['fields' => $this->fields]);
    }

    private function getApiUserId($connection) {
        $sql = "select id, AES_DECRYPT(username, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."')) as username from api_user";

        $result = $connection->fetchAll($sql);
        $usersMap = array();
        foreach ($result as $item) {
            if ($item['id'] != null) {
                $usersMap[$item['id']] = $item['id'] . ": " . $item['username'];
            }
        }

        return $usersMap;
    }

    private function getApiApplication($connection) {
        $sql = "select application from apiLogApplications";

        $result = $connection->fetchAll($sql);
        $applicationSet = array();
        foreach ($result as $item) {
            if (!empty($item['application']))
            {
                $applicationSet[$item['application']] = $item['application'];
            }
        }

        return $applicationSet;
    }

    private function getApiId($connection) {

        $sql = "select * from apis";
        $apiIds = $connection->fetchAll($sql);
        $apiMap = array();
        foreach ($apiIds as $api) {
            $apiMap[$api['id']] = "{$api['id']}: {$api['controller']}/{$api['method']}";
        }

        return $apiMap;
    }

    public function searchAction(Request $request) {
        $connection = $this->get("database_connection");
        $req = $request->request->all();
        $searchstring = " WHERE 1 = 1 ";
        $parameters = [];

        foreach ($this->fields as $name => $field) {
            if (!empty($req[$name]) && $field['searchable']) {
                $searchstring .= " AND $name LIKE :$name ";
                $parameters[":$name"] = '%' . $req[$name] . '%';
            }
        }

        if (!empty($req['startDate'])) {
            $searchstring .= " AND timestamp >= :startDate ";
            $parameters[':startDate'] = $req['startDate'];
        }

        if (!empty($req['endDate'])) {
            $searchstring .= " AND timestamp <= :endDate ";
            $parameters[':endDate'] = $req['endDate'];
        }

        $orderByArr = [];
        foreach ($req["order"] as $val) {
            $orderBy = $req["columns"][$val["column"]]["data"];
            if (isset($this->fields[$orderBy]) && $this->fields[$orderBy]['orderable']) {
                $dir = strtolower($val["dir"]) === "desc" ? "desc" : "asc";
                $orderByArr[] = "$orderBy $dir";
            }
        }
        $orderByArr[] = "a.id ASC";
        $orderByString = " ORDER BY " . implode(", ", $orderByArr);

        $queryCountString = "SELECT COUNT(a.id) FROM api_log a $searchstring";
        $recordsTotal = $connection->fetchColumn($queryCountString, $parameters);

        $queryFilteredCountString = "SELECT COUNT(a.id) FROM api_log a $searchstring";
        $recordsFiltered = $connection->fetchColumn($queryFilteredCountString, $parameters);

        $queryString = sprintf("SELECT *, AES_DECRYPT(headers, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."')) as headers FROM api_log a $searchstring $orderByString LIMIT %d, %d", (int) $req["start"], (int) $req['length']);
        $items = $connection->fetchAll($queryString, $parameters);

        foreach ($items as $key => $item) {
            $items[$key]['id'] = '<a href="javascript:ajaxModal(\''.$this->generateUrl('_manage_API_log_ui_display', ['id' => $item['id']]).'\',\'API Log Entry\',\'large\')">'.$item['id'].'</a>';
            $items[$key]['data'] = htmlspecialchars(strlen($item['data']) > 50 ? substr($item['data'], 0, 50) . "..." : $item['data']);
            $items[$key]['data'] = '<a href="javascript:ajaxModal(\''.$this->generateUrl('_manage_API_log_ui_display', ['id' => $item['id']]).'\',\'API Log Entry\',\'large\')">'.$items[$key]['data'].'</a>';
            $response = htmlspecialchars($items[$key]['response']);
            if(strlen($response) > 512) {
                $response = '<a href="javascript:ajaxModal(\''.$this->generateUrl('_manage_API_log_ui_display', ['id' => $item['id']]).'\',\'API Log Entry\',\'large\')">' . substr($response, 0, 512) . "..." .'</a>';
            }
            $items[$key]['response'] = $response;
        }

        return new JsonResponse([
            "draw" => (int) $req["draw"],
            "recordsTotal" => (int) $recordsTotal,
            "recordsFiltered" => (int) $recordsFiltered,
            "data" => $items
        ]);
    }

    public function displayAction($id) {
        $connection = $this->get("database_connection");
        $record = $connection->fetchAssoc("SELECT *, AES_DECRYPT(headers, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."')) as headers FROM api_log WHERE id=:id",[':id'=>$id]);
        return $this->render("ManageManageBundle:ApiUI/ApiUILogUI:display.html.twig", ['record' => $record]);
    }

}
?>