<?php

namespace Manage\ManageBundle\Controller\ApiUI;

use classes\classBundle\Entity\Apis;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiUIController extends BaseApiController
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");       
        return $this->render('ManageManageBundle:ApiUI:ApiUI/index.html.twig');
    }
    
    public function addAction() {
                $action = "add";
		return $this->render('ManageManageBundle:ApiUI:ApiUI/editApi.html.twig', ['action' => $action,"button" => $this->getButtonAttrs($action)]);
	}
    
    public function editAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:Apis");
        $api = $repository->find($request->get("id"));
        $action = "edit";
        return $this->render('ManageManageBundle:ApiUI:ApiUI/editApi.html.twig', ['action' =>$action, 'api' => $api,"button" => $this->getButtonAttrs($action)]);
    }
    
    public function saveAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:Apis");
        if ($request->get("action") === "edit") {
            $api = $repository->find($request->request->get("id"));
        }
        else {
            $api = new Apis();
            $em->persist($api);
        }
        $api->setController(trim($request->request->get("controller")));
        $api->setMethod(trim($request->request->get("method")));
        $api->setEnabled(trim($request->request->get("enabled")));
        $em->flush();
        return new Response("success");
    }
    
	public function deleteAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$api = $this->getDoctrine()->getRepository("classesclassBundle:Apis")->find($request->request->get("id"));
		$em->remove($api);
		$em->flush();
		return new Response("success");
	}
}
?>