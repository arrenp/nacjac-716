<?php

namespace Manage\ManageBundle\Controller\ApiUI;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\accounts;
use classes\classBundle\Entity\APIAccounts;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Classes\plansMethods;
use Permissions\RolesBundle\Classes\rolesPermissions;
use classes\classBundle\Entity\ApiUser;
class ApiUserController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        return $this->render('ManageManageBundle:ApiUI:ApiUser/index.html.twig');
    }

    public function ApiUserAction()
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUser');
        $user = $repository->findAll();
    }

    public function addApiUserAction()
    {
        return $this->render('ManageManageBundle:ApiUI:ApiUser/add.html.twig');
    }

    public function addApiUserSavedAction(Request $request)
    {
        $accountId = $request->request->get('accountId');
        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $enabled = $request->request->get('enabled');
        $rateLimit = $request->request->get('rateLimit');

        $insert = new ApiUser();
        $insert->setAccountId($accountId);
        $insert->setUsername($username);
        $insert->setPassword($password);
        $insert->setEnabled(($enabled == 'Yes' ? 1 : 0));
        $insert->setRateLimit($rateLimit);

        $em = $this->getDoctrine()->getManager();
        $em->persist($insert);
        $em->flush();

        return new Response('saved');
    }

    public function editApiUserAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUser');
        $id = $request->query->get("id");
        $user = $repository->findOneBy(array('id' => $id));

        return $this->render('ManageManageBundle:ApiUI:ApiUser/edit.html.twig', array("user" => $user));
    }

    public function editApiUserSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $accountId = $request->request->get('accountId');
        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $enabled = $request->request->get('enabled');
        $rateLimit = $request->request->get('rateLimit');

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUser');
        $user = $repository->findOneBy(array("id" => $id));
        $user->setAccountId($accountId);
        $user->setUsername($username);
        $user->setPassword($password);
        $user->setEnabled(($enabled == 'Yes' ? 1 : 0));
        $user->setRateLimit($rateLimit);

        $em->persist($user);
        $em->flush();

        return new Response("Saved");
    }

    function deleteApiUserAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUser');
        $user = $repository->findOneBy(array('id' => $request->query->get("id")));

        return $this->render('ManageManageBundle:ApiUI:ApiUser/delete.html.twig', array("user" => $user));
    }

    public function deleteApiUserSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ApiUser');
        $user = $repository->findOneBy(array('id' => $id));

        $em->remove($user);
        $em->flush();
        return new Response("Deleted");

    }
}
?>