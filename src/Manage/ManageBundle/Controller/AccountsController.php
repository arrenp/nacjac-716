<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\accounts;
use classes\classBundle\Entity\defaultModules;
use classes\classBundle\Entity\defaultPlan;
use classes\classBundle\Entity\APIAccounts;
use classes\classBundle\Entity\accountsUsersEnabledAccounts;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Classes\plansMethods;
use Permissions\RolesBundle\Classes\rolesPermissions;
use classes\classBundle\Entity\participants;
use classes\classBundle\Entity\accountsRecordkeepersQueryIds;
use classes\classBundle\Entity\AccountsBatchFileNotificationEmails;
use classes\classBundle\Entity\launchToolRecipients;
use classes\classBundle\Entity\AccountAppColors;
set_time_limit(0);
class AccountsController extends Controller
{
    public $sections;
    public $userid;
    public $binaryToStringArray;
    public function indexAction()
    {
    	$this->init();
        $writeable = $this->writeable();
        $session = $this->get('adminsession');
        $roleType = $session->roleType;
        $clientid = $session->clientid;
        $filter =true;
    	return $this->render('ManageManageBundle:Accounts:index.html.twig',array("sections" => $this->sections,"reliusSections" => $this->reliusSections(),"SRTSections" => $this->SRTSections(),"EnvisageSections" => $this->EnvisageSections(),"ExpertPlanSections" => $this->ExpertPlanSections(),"DSTSections" => $this->DSTSections(), "AmrtsSections" => $this->AmrtsSections(), "writeable" => $writeable,"roleType" => $roleType,"clientid" => $clientid,"filter" => $filter));
    }
    public function indexDatatableOptionsAction()
    {
        $this->init();
        $writeable = $this->writeable();
        $session = $this->get('adminsession');
        $roleType = $session->roleType;
        $clientid = $session->clientid;
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $filter = $request->request->get("filter");
        return $this->render('ManageManageBundle:Accounts:accountdatatableoptions.html.twig',array('clientid' => $clientid,'roleType' => $roleType,'writeable' => $writeable,"filter" => $filter));
    }
    public function hideAccountAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $request->request->get("id")));
        $account->visible = 0;
        $em->flush();
        return new Response("");
    }
    public function showAccountAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $request->request->get("id")));
        $account->visible = 1;
        $em->flush();
        return new Response("");
    }
    public function manageAccountListAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $clientid = $request->query->get("clientid");

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array('id' => $clientid));
        if ($user->autoAddAccount)
        $user->autoAddAccountChecked = "checked";
        else
        $user->autoAddAccountChecked = "";


        return $this->render('ManageManageBundle:Accounts:manageaccounts.html.twig',array("clientid" => $clientid,"user" => $user));
    }
    public function manageAccountListAutoAddAccountAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $clientid = $request->request->get("clientid");
        $repository = $em->getRepository('classesclassBundle:accountsUsers');
        $user = $repository->findOneBy(array('id' => $clientid));
        $user->autoAddAccount = $request->request->get("autoAddAccount");
        $em->flush();
        return new Response("");
    }    
    public function manageAccountListSaveAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $userid= $request->request->get("userid");
        $clientid = $request->request->get("clientid");

        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $userid));

        $repository = $em->getRepository('classesclassBundle:accountsUsersEnabledAccounts');
        $searched = $repository->findOneBy(array('userid' => $userid,"accountsUsersId" => $clientid));
        if ($searched == null)
        {
            $accountsUsersEnabledAccounts = new accountsUsersEnabledAccounts();
            $accountsUsersEnabledAccounts->account = $account;
            $accountsUsersEnabledAccounts->accountsUsersId = $clientid;
            $em->persist($accountsUsersEnabledAccounts);
            $em->flush();
        }
        else
        {
            $em->remove($searched);
            $em->flush();
        }
        return new Response("");
    }
    public function manageAccountListCheckedAction($clientid,$userid)
    {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accountsUsersEnabledAccounts');
        $searched = $repository->findOneBy(array('userid' => $userid,"accountsUsersId" => $clientid));
        if ($searched != null)
        return new Response("checked");
        else
        return new Response("");
    }

    public function writeable()
    {
        $permissions = $this->get('rolesPermissions');
        return $permissions->writeable("ManageAccounts");
    }


    public function addAccountAction()
    {
    	$this->init();
    	return $this->render('ManageManageBundle:Accounts:addaccount.html.twig',array("sections" => $this->sections));
    }

    public function init($account = null)
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
    	$this->sections = array();
    	$this->addSection("partnerid","Partner Id","text","block");
        $this->addSection("company","Company","text","block");
    	$this->addSection("connectionType","Connection Type","select","block","",array("Relius","SRT","ExpertPlan","Envisage","DST","Educate","Alerus","RetRev","Omniinspty","Omniinspty2", "Omniamrts", "RetRev2","SRT2", "Omnimoa"));
        $this->addSection("connectionClass","Connection Class","keyValueSelect","block","",array(""=>"None","amtrust"=>"amtrust","omniamrtssoap12"=>"omniamrtssoap12","horseman"=>"horseman"));
        $this->addSection("recordkeeperVersion","Connection Version","text","block","1");
        $this->addSection("connectionClass","Connection Class","text","block");
    	$this->addSection("recordkeeperTpaid","TPA id","text","none");
    	$this->addSection("recordkeeperAdviceProviderCd","Advice Provider Cd","text","none");
    	$this->addSection("recordkeeperUrl","Service Url","text","none");
    	$this->addSection("recordkeeperUser","Username","text","none");
    	$this->addSection("recordkeeperPass","Password","text","none");
        
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accountsRecordkeepersQueryIds');
        $this->accountsRecordkeepersQueryIdsArray = array(
            'plansQuery'=>'Plan Query ID',
            'riskBasedFundsQuery'=>'Risk Based Funds Query ID',
            'portfolioQuery'=>'Portfolio Query ID',
            'adviceQuery'=>'Advice Query ID'
        );
        
        foreach ($this->accountsRecordkeepersQueryIdsArray as $type=>$desc) {
            $queryid = "";
            if ($account != null) {
                $recordkeepersQuery = $repository->findOneBy(array('userid'=>$account->id, 'type'=>$type));
                $queryid = !empty($recordkeepersQuery) ? $recordkeepersQuery->queryid : "";
            }
            $this->addSection($type, $desc, "text", "none", $queryid, null, 'accountsRecordkeepersQueryIds', false);
        }  
    	
    	$this->addSection("recordkeeperPortfoliosAvailable","Portfolios Available","select","none","",array("Yes","No"));
    	$this->addSection("recordkeeperRiskBasedFundsAvailable","Risk Based Funds Available","select","none","",array("Yes","No"));
        $this->addSection("retirementNeedsFlow", "Retirement Needs Flow", "keyValueSelect", "block", 1, array(1 => "Current Social Security Only", 2 => "Enhanced Social Security Only", 3 => "Pension Only", 4 => "Enhanced Social Security and Pension"), "defaultModules");
        $this->addSection("socialSecurityMultiplier","Social Security Multiplier","text","block","25", null, "defaultModules");
        $this->addSection("pensionEstimatorURL","Pension Estimator URL","text","block","", null, "defaultModules");

    	$this->addSection("investmentsMinScore","Investments Min Score","text","block",6);
    	$this->addSection("investmentsMaxScore","Investments Max Score","text","block",54);
    	$this->addSection("ACAon","AcaOn","select","block","",array("Yes","No"));
    	$this->addSection("MSTClientID","MSTClientID","text","block","");
        $this->addSection("adviceAvailable","Advice Available","select","block","",array("Yes","No"));
        $this->addSection("qdiaAvailable","QDIA Available","select","block","",array("Yes","No"));
        $this->addSection("language","Language","select","block","",array("Yes","No"));
        $this->addSection("dstEncryption","DST Encryption","select","none","",array("RSA","AES"));

        $connection = $this->get('doctrine.dbal.default_connection');
        $languages = $connection->fetchAll('SELECT * FROM languages WHERE name != "" group by  name');
        $language = array();
        $path = "../src/Spe/AppBundle/Resources/translations/";
        $translator = $this->get("translator");
        foreach ($languages as $lang)
        {
            if ($translator->hasLocale($lang["name"])) {
                $language[$lang["description"]] = $lang["name"];
            }
            if ($account != null && $account->languagePrefix != null &&  $account->languagePrefix != "" && file_exists($path.$account->languagePrefix."_".$lang["name"]))
            {
                $language[$account->languagePrefix." ".$lang["description"]] = $account->languagePrefix."_".$lang["name"];
            }
        }
        if ($account != null && $account->language == 1)
        $languageAvailableDisplay = "block";
        else
        $languageAvailableDisplay = "none";

        $this->addSection("languagesAvailable","Languages Available","languages",$languageAvailableDisplay,"en",$language);
        if ($account != null && $account->languagePrefix != "")
        $retirementNeedsVersionParams = array("standard","by age");
        else
        $retirementNeedsVersionParams = array("standard");
        $this->addSection("languagePrefix","Language Prefix","text","block","");
        $this->addSection("retirementNeedsVersion","Retirement Needs Version","select","block","standard",$retirementNeedsVersionParams,"defaultModules");
        $this->addSection("retirementNeedsVersionOnOff","Retirement Needs Version On Off","select","block","",array("Yes","No"));
        $this->addSection("fundLink","Fund Link","select","block","",array("Yes","No"),"defaultPlan");
        $this->addSection("pipContent","pipContent","select","block","",array(0,1,2,3,5),"defaultPlan");
        $this->addSection("pipPosition","pipPosition","select","block","2",array(0,1,2),"defaultPlan");

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:LNCLNKeys');
        $LNCLNKeysObject = $repository->findAll(); 
        $LNCLNKeys = array();
        foreach ($LNCLNKeysObject as $key)
        {
            $LNCLNKeys[]  = $key->name;
        }

        $this->addSection("LNCLNKey","LNCLNKey","select","none","",$LNCLNKeys);
        $this->addSection("percentageOfIncomeFromPlan","Percentage Of Income From Plan","text","block",30,null,"defaultModules");
        $this->addSection("replacementIncomePercent","Replacement Income Percent","text","block",80,null,"defaultModules");
        $this->addSection("spAvailable","S&P available","select","block","No",array("Yes","No"));
        $this->addSection("interfaceLayout","Interface Layout","select","block","side",array("side","top","bottom"),"defaultPlan");
        $this->addSection("interfaceColor","Interface Color","select","block","blue",array("blue","green","gold","red","burgundy","charcoal","abgil","custom1","LNCLN","moa"),"defaultPlan");
        $this->addSection("sections","sections","select","block","riskProfileSPE6",array('riskProfileSPE6','riskProfileSPA6','riskProfileAA','riskProfileAXA','riskProfileAXA2013','riskProfileAXA2014_Ibbotson','riskProfileLNCLN7','riskProfileLW','riskProfileS401k','riskProfileSPE','riskProfileWIL6','riskProfileADP'),"defaultPlan");        
        $this->addSection("inflation","Flex Path - Inflation","text","block",0,null,"defaultModules");
        $this->addSection("return","Flex Path- Return","text","block",0,null,"defaultModules");
        $this->addSection("flexPlan","FlexPath","select","block",0,array("Yes","No"),"defaultModules");
        $this->addSection("flexPlanAvailable","FlexPath - Available","select","block","No",array("Yes","No"));
        $this->addSection("reportsStylesHeader","Reports Styles Header","colorpicker","block","",null,"defaultPlan");
        $this->addSection("reportsStylesFont","Reports Styles Font","colorpicker","block","",null,"defaultPlan");
        $this->addSection("reportsInnerTable","Reports Inner Table","colorpicker","block","",null,"defaultPlan");
        $this->addSection("reportsBackground","Reports Background","colorpicker","block","",null,"defaultPlan");
        $this->addSection("languageDropdown","Language Dropdown","select","block",0,array("Yes","No"),"defaultModules");
        $this->addSection("sponsorConnect","Smart Connect","select","block",0,array(0,1),"defaultPlan");
        $this->addSection("ECOMMModal","ECOMM Modal","select","block",0,array("Yes","No"),"defaultModules");
        $this->addSection("ECOMMModalChecked","ECOMM Modal Checked","select","block",0,array("Yes","No"),"defaultModules");
        $this->addSection("smartEnrollClass","SmartEnroll Class","select","block",null,array("epic","horsemen","heritage","srt","relius"));
        $this->addSection("smartEnrollPath","SmartEnroll Path","text","block");
        $this->addSection("smartPlanEnabled","Smart Plan Navigation","select","block","No",array("Yes","No"));
        $this->addSection("smartEnrollEnabled","Smart Enroll Navigation","select","block","No",array("Yes","No"));
        $this->addSection("irioEnabled","Irio Navigation","select","block","No",array("Yes","No"));
        $this->addSection("powerviewEnabled","Powerview Navigation","select","block","No",array("Yes","No"));        
        $this->addSection("smartPlanAllowed","Default SmartPlan on","select","block","side",array("Yes","No"),"defaultPlan");
        $this->addSection("smartEnrollAllowed","Default SmartEnroll on","select","block","side",array("Yes","No"),"defaultPlan");
        $this->addSection("irioAllowed","Default Irio on","select","block","side",array("Yes","No"),"defaultPlan");
        $this->addSection("powerviewAllowed","Default Powerview on","select","block","side",array("Yes","No"),"defaultPlan");
        $this->addSection("irioFromEmailName", "Irio From Email Name", "text", "block");
        $this->addSection("rolloverWorkflow","Default Rollover on","select","block","Standard",array("Yes","No"),"defaultModules");
        $this->addSection("beneficiaryType","Beneficiary Type","select","block","Standard",array("Standard","Srt","Relius"));
        $this->addSection("riskBasedQuestionnaire","Risk Based Questionnaire","select","block","Yes",array("Yes","No"),"defaultModules");
        $this->addSection("personalInvestorProfile","Personal Investor Profile","select","block","Standard",array("Yes","No"));
        $this->addSection("rolloverWorkflowEnabled","Rollover Workflow","select","block","Standard",array("Yes","No"));
        $this->addSection("riskType","Risk Type","select","block","RBP",array("RBP","RBF"),"defaultPlan");

        $fundsGroups = $this->get("FundsGroupsService")->getList();
        $fundsGroupsArray = array();
        foreach ($fundsGroups as $group)
        {
            $fundsGroupsArray[$group->name] = $group->id;
        }
        $this->addSection("fundGroupid","Fund Group","select","block","Standard",$fundsGroupsArray,"defaultPlan",true,array("useKeyDescription" => true));

        $repository = $this->getDoctrine()->getRepository("classesclassBundle:accounts");
        $accounts = $repository->findBy(array('translationId' => null), array('company' => 'ASC'));
        $accountsIndexed = array();
        foreach ($accounts as $currAccount) {
            if ($account == null || $account->id != $currAccount->id) {
                $accountsIndexed[$currAccount->id] = "{$currAccount->company} (ID: {$currAccount->id})";
            }
        }
        $this->addSection("investmentsGraph","Default Investments Graph","select","block","Standard",array("Disabled" => 0,"Enabled" => 1),"defaultModules",true,array("useKeyDescription" => true));
        $this->addSection("translationId","Translation Account","keyValueSelect","block","",$accountsIndexed);
        $this->addSection("combinedContributionAmount","Combined Contribution Amount","select","block",0,range(0,100),"defaultModules");
        $this->addSection("postTax","Post Tax","select","block","Standard",array("Yes","No"),"defaultModules");
        $this->addSection("minimumContribution","Traditional Minimum Contribution","select","block",0,range(0,5),"defaultModules");
        $this->addSection("maximumContribution","Traditional Maximum Contribution","select","block",0,$this->maximumContributionArrayValues(),"defaultModules");
        $this->addSection("minimumContributionRoth","Roth Minimum Contribution","select","block",0,range(0,5),"defaultModules");
        $this->addSection("maximumContributionRoth","Roth Maximum Contribution","select","block",0,$this->maximumContributionArrayValues(),"defaultModules");
        $this->addSection("minimumContributionPostTax","Post Tax Minimum Contribution","select","block",0,range(0,5),"defaultModules");
        $this->addSection("maximumContributionPostTax","Post Tax Maximum Contribution","select","block",0,$this->maximumContributionArrayValues(),"defaultModules");
        $this->addSection("compounding","Compounding","select","block","Standard",array("Yes","No"),"defaultModules");
        $this->addSection("profileNotificationShowEmail","Show full name in Profile Notification email","select","block","Standard",array("Off" => 0,"On" => 1),"defaultPlan",true,array("useKeyDescription" => true));
        $this->addSection("reportable","Reportable","select","block","No",array("Yes","No"));
        $this->addSection("eligibleStatus","Eligible","text","block",'',null);
        $this->addSection("enrolledStatus","Enrolled","text","block",'',null);
        $this->addSection("maximumAnnualIncome","Maximum Annual Income","text","block",'',null ,"defaultModules");
        $this->addSection("trustedContactEnabled", "Trusted Contact Enabled", "select", "block", "No", array("Yes","No"));
        $this->addSection("trustedContactOn", "Trusted Contact Plan Default", "select", "block", "No", array("Yes","No"), "defaultModules");
        $this->addSection("trustedContactServiceUrl", "Trusted Contact Service URL", "text", "block");
        $this->addSection("stadionManagedAccountEnabled", "Stadion Managed Account Enabled", "select", "block", "No", array("Yes","No"));
        $this->addSection("enrollmentRedirectEnabled", "Enrollment Redirect Enabled", "select", "block", "No", array("Yes","No"));
        $this->addSection("enrollmentRedirectOn", "Enrollment Redirect Plan Default", "select", "block", "No", array("Yes","No"), "defaultPlan");
        $this->addSection("investmentUpdateOption","Investment Update Option","select","block","realignment",array("realignment","rebalance"));
        $this->addSection("beneficiaryAutoAdd", "Beneficiary Auto Add", "select", "block", "No", array("Yes","No"), "defaultModules");
        for ($i = 1; $i <= 4; $i++)
        $this->addSection("hex".$i,"Hex".$i,"colorpicker","block","",null,"AccountAppColors");
        $this->addSection("beneficiaryAudioEnabled","Beneficiary Audio Enabled","select","block","No",array("Yes","No"));
        $this->addSection("PIPWidget","PIPWidget","select","block","No",array("Yes","No"));
        $this->addSection("autoPlayAudio","Auto Play Audio","select","block","Yes",array("Yes","No"),"defaultModules");
        $this->binaryToStringArray = array("powerviewEnabled","irioEnabled","smartPlanEnabled","smartEnrollEnabled","recordkeeperPortfoliosAvailable","recordkeeperRiskBasedFundsAvailable","ACAon","adviceAvailable","qdiaAvailable","language","retirementNeedsVersionOnOff","fundLink","spAvailable","flexPlan","flexPlanAvailable","languageDropdown","ECOMMModal","ECOMMModalChecked","smartEnrollEnabled","smartPlanAllowed","smartEnrollAllowed","irioAllowed","powerviewAllowed","riskBasedQuestionnaire","personalInvestorProfile","postTax","compounding","reportable","rolloverWorkflow","rolloverWorkflowEnabled","postTax","compounding","trustedContactEnabled","trustedContactOn","stadionManagedAccountEnabled","enrollmentRedirectEnabled","enrollmentRedirectOn","beneficiaryAutoAdd","beneficiaryAudioEnabled","PIPWidget","autoPlayAudio");



    }
    public function validateContribution($request)
    {
        $errorString = "";
        $sections = [
            [
                "name" => "Contribution",
                "description" =>  "Traditional"
            ],
            [
                "name" => "ContributionRoth",
                "description" =>  "Roth"
            ],
            [
                "name" => "ContributionPostTax",
                "description" =>  "Post Tax"
            ]
        ];
        foreach ($sections as $section)
        {
            if ($request->request->get("minimum".$section['name']) >  $request->request->get("maximum".$section['name']) )
            {
                $errorString  = $errorString."Minimum ".$section['description']." Contribution cannot be greater than Maximum ".$section['description']." Contribution\n";
            }
        }
        return $errorString;
    }
    public function maximumContributionArrayValues()
    {
        $values = array();
        for ($i = 0; $i <= 100; $i = $i+5)
        {
            $values[] = $i;
        }
        return $values;
    }
    public function addAccountSavedAction()
    {
        $this->init();
        $newaccount = new accounts();
        $defaultModules = new defaultModules();
        $defaultPlan = new defaultPlan();
        $appColors = new AccountAppColors;
        $request = Request::createFromGlobals();
        if ($this->validateContribution($request) != "")
        {
            return new Response($this->validateContribution($request));
        }
        $em = $this->getDoctrine()->getManager();
        
        foreach ($this->accountsRecordkeepersQueryIdsArray as $type=>$desc) {
            if (!empty($request->request->get($type))) {
                $recordkeepersQuery = new accountsRecordkeepersQueryIds();
                $recordkeepersQuery->type = $type;
                $recordkeepersQuery->queryid = $request->request->get($type);
                $recordkeepersQuery->account = $newaccount;
                $em->persist($recordkeepersQuery);
            }
        }
        
        $request->getPathInfo();
        if ($this->partneridExists($request->request->get("partnerid")))
        return new Response("Account Already exists");

        if ($request->request->get("connectionType") == "Educate")
        {
            $participant = new participants();
            $participant->connection = "educate";
            $em->persist($participant);
            $em->flush();
            $newaccount->educateParticipantId = $participant->id;
        }


        $this->copyRequestToAccount($request,$newaccount);
        $newaccount->subscribedate = new \DateTime("now");
        $em->persist($newaccount);
        $em->flush();
        $defaultModules->userid = $newaccount->id;
        $defaultPlan->userid = $newaccount->id;
        $appColors->userid = $newaccount->id;
        $this->copyRequestToAccount($request,$defaultModules,"defaultModules");
        $this->copyRequestToAccount($request,$defaultPlan,"defaultPlan");
        $this->copyRequestToAccount($request,$appColors,"AccountAppColors");       
        //$defaultPlan->reportsStylesHeader =  $request->request->get("reportsStylesHeader");
        $em->persist($defaultModules);
        $em->persist($defaultPlan);
        $em->persist($appColors);
        $em->flush();

        $repository = $em->getRepository('classesclassBundle:accountsUsers');
        $accountsUsers = $repository->findBy(array('autoAddAccount' => 1));   
        foreach ($accountsUsers as $accountsUser)
        {
            $accountsUsersEnabledAccounts = new accountsUsersEnabledAccounts();
            $accountsUsersEnabledAccounts->account = $newaccount;
            $accountsUsersEnabledAccounts->accountsUsersId = $accountsUser->id;
            $em->persist($accountsUsersEnabledAccounts);
        }
        $em->flush();

        return new Response("Saved");

    }
    function copyRequestToAccount($request,&$account,$table = null)
    {
        
        $arr =  $request->request->all();
        foreach ($arr as $key => $value)
        {
            if (isset($this->sections[$key]['table']) && ($table == null || ($table != null && $this->sections[$key]['table'] == $table)))
                $account->$key = $value;
            
        }
        foreach ($this->binaryToStringArray as $section)
        {
            if (isset($this->sections[$section]['table']) && ($table == null || ($table != null && $this->sections[$section]['table'] == $table)))
            {
                if ($request->request->get($section) == "Yes")
                $account->$section = 1;
                else
                $account->$section = 0;
            }
        }

        $account->socialSecurityMultiplier = $account->socialSecurityMultiplier/100.0;
        if ($account->pensionEstimatorURL == "") {
            $account->pensionEstimatorURL = null;
        }
        if ($account->translationId == "") {
            $account->translationId = null;
        }

    }
    function partneridExists($partnerid,$currentaccount = null)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accounts');
        $accountsearched = $repository->findOneBy(array('partnerid' => $partnerid));

        if ($currentaccount != null && $accountsearched != null)
        if ($currentaccount->id == $accountsearched->id)
        $accountsearched = null;


        if ($accountsearched != null)
        return true;
        return false;
    }

    public function editAccountAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();


        $repository = $em->getRepository('classesclassBundle:accounts');
        $currentaccount= $repository->findOneBy(array('id' => $request->query->get("id") ) );
        $methods = $this->get('GeneralMethods');

        if ($currentaccount != null)
        {
            $this->userid = $currentaccount->id;
            $repository = $em->getRepository('classesclassBundle:defaultModules');
            $defaultModules= $repository->findOneBy(array('userid' => $request->query->get("id") ) );

            $repository = $em->getRepository('classesclassBundle:defaultPlan');
            $defaultPlan= $repository->findOneBy(array('userid' => $request->query->get("id") ) ); 
            if ($defaultModules == null) 
            {
                $defaultModules = new defaultModules();
                $defaultModules->userid = $currentaccount->id;
               
                $em->persist($defaultModules);
                $em->flush();
            }
            if ($defaultPlan == null) 
            {
                $defaultPlan = new defaultModules();
                $defaultPlan->userid = $currentaccount->id;
               
                $em->persist($defaultPlan);
                $em->flush();
            }
        }

        $this->init($currentaccount);
        if ($methods->isSrt($currentaccount))
            $sections = $this->SRTSections();

        if ($currentaccount->connectionType == "Envisage")
            $sections = $this->EnvisageSections();

        if ($currentaccount->connectionType == "Relius")
            $sections = $this->reliusSections();

        if ($currentaccount->connectionType == "ExpertPlan")
            $sections = $this->ExpertPlanSections();

        if ($currentaccount->connectionType == "Omniamrts")
            $sections = $this->AmrtsSections();

        if ($currentaccount->connectionType == "Omnimoa") // for now Omnimoa and Omniamrts are the same
            $sections = $this->AmrtsSections();

        if ($currentaccount->connectionType == "DST")
            $sections =  $this->DSTSections();


        if (isset($sections) && count($sections) > 0 )
        foreach ($sections as $name)
        $this->sections[$name]['display'] = "block";

        
        foreach ($this->binaryToStringArray as $section)
        {
            $this->sectionFormatBinaryToString($currentaccount,$section);
            $this->sectionFormatBinaryToString($defaultModules,$section);
            $this->sectionFormatBinaryToString($defaultPlan,$section);
        }
        $this->sections['socialSecurityMultiplier']['value'] = $this->sections['socialSecurityMultiplier']['value']*100.0;

        return $this->render('ManageManageBundle:Accounts:editaccount.html.twig',array("sections" => $this->sections,"id" => $this->userid));        
    }

    public function editAccountSavedAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        if ($this->validateContribution($request) != "")
        {
            return new Response($this->validateContribution($request));
        }
        $em = $this->getDoctrine()->getManager();
        $request->getPathInfo();
        $repository = $em->getRepository('classesclassBundle:accounts');
        $currentaccount= $repository->findOneBy(array('id' => $request->request->get("id") ) ); 
        
        $query = $em->createQuery('DELETE classesclassBundle:accountsRecordkeepersQueryIds q WHERE q.userid = ' . (int) $currentaccount->id);
        $query->execute(); 
        foreach ($this->accountsRecordkeepersQueryIdsArray as $type => $desc) {
            if (!empty($request->request->get($type))) {
                $repository = $em->getRepository('classesclassBundle:accountsRecordkeepersQueryIds');
                $recordkeepersQuery = $repository->findOneBy(array('userid' => $currentaccount->id, 'type' => $type));
                if ($recordkeepersQuery == null) {
                    $recordkeepersQuery = new accountsRecordkeepersQueryIds;
                }
                
                $recordkeepersQuery->type = $type;
                $recordkeepersQuery->queryid = $request->request->get($type);
                $recordkeepersQuery->account = $currentaccount;
                $em->persist($recordkeepersQuery);
            }
        }
        
        $oldpartnerid = $currentaccount->partnerid;       
        if ($this->partneridExists($request->request->get("partnerid"),$currentaccount))
        return new Response("Account Already exists");
        $this->copyRequestToAccount($request,$currentaccount);
        $repository = $em->getRepository('classesclassBundle:defaultModules');
        $defaultModules= $repository->findOneBy(array('userid' => $request->request->get("id") ) );
        $repository = $em->getRepository('classesclassBundle:defaultPlan');
        $defaultPlan= $repository->findOneBy(array('userid' => $request->request->get("id") ) );
        $appColors = $this->get("AccountAppColorsService")->findOneBy(['userid' => $request->request->get("id")]);
        $this->copyRequestToAccount($request,$appColors,"AccountAppColors");
        $this->copyRequestToAccount($request,$defaultModules,"defaultModules");
        $this->copyRequestToAccount($request,$defaultPlan,"defaultPlan");
        $em->flush();
        $connection = $this->get('doctrine.dbal.default_connection');
        if ($oldpartnerid != $request->request->get("partnerid"))
        $connection->exec("UPDATE plans set partnerid = '".$request->request->get("partnerid")."' WHERE userid=".$request->request->get("id"));
        
        if (!$currentaccount->smartEnrollEnabled)
        $connection->exec("UPDATE plans set smartEnroll = 0 WHERE userid = ".$currentaccount->id);
       
        return new Response("Saved");
    }
    function sectionFormatBinaryToString($account,$section)
    {
        if (isset($account->$section))
        {
            if ($account->$section  == "1")
            $this->sections[$section]['value'] = "Yes";

            if ($account->$section  == "0")
            $this->sections[$section]['value'] = "No";
        }

    }

    public function listUsersAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo(); 
        $writeable = $this->writeable();
        return $this->render('ManageManageBundle:Accounts:Search/listusers.html.twig',array("userid" => $request->query->get("id"),"writeable" => $writeable));        
    }

    public function addSection($name,$description,$type,$display,$value = "", $values = null,$table = "accounts", $overrideValue = true,$options = array("useKeyDescription" => false))
    {
    	$this->sections[$name]['name'] = $name;
    	$this->sections[$name]['description'] = $description;
    	$this->sections[$name]['type'] = $type;
    	$this->sections[$name]['display'] = $display;
    	$this->sections[$name]['value'] = $value;
        $this->sections[$name]['options'] = $options;
    	if ($values != null)
    	    $this->sections[$name]['values'] = $values;
        $this->sections[$name]['table'] = $table;
        
        if ($this->userid != null && $overrideValue)
        {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('classesclassBundle:'.$table);
            if ($table == "accounts")
            $params = array('id' => $this->userid);
            else
            $params = array('userid' => $this->userid);
            $accountsearched = $repository->findOneBy($params);
            $this->sections[$name]['value'] = $accountsearched->$name;
        }

    }

    public function deleteAccountAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accounts');
        $account= $repository->findOneBy(array('id' => $request->query->get("id") ) ); 
        return $this->render('ManageManageBundle:Accounts:deleteaccount.html.twig',array("account" => $account));        
    }

    public function deleteAccountDeletedAction()
    {
        $generalMethods = $this->get('GeneralMethods');
        $plansMethods = new plansMethods($this->container);
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();

       
        $repository = $em->getRepository('classesclassBundle:accounts');
        $account= $repository->findOneBy(array('id' => $request->request->get("id")));

        $plansMethods->deleteplans($account->id);

        $accountSections = array(
            "accountsUsers",
            "defaultDocuments",
            "defaultFunds",
            "defaultLibraryHotTopics",
            "DefaultMessages",
            "defaultModules",
            "defaultPlan",
            "defaultPortfolioFunds",
            "defaultPortfolios",
            "accountsRecordkeepersQueryIds",
            "AccountAppColors"
            );
        foreach ($accountSections as $section)
        $generalMethods->removeTableData($section,"userid",$account->id);

        $generalMethods->removeTableData("accounts","id",$account->id);

        return new Response("Account Deleted");
    }
    public function SRTSections()
    {
		return array("recordkeeperAdviceProviderCd","recordkeeperUrl","recordkeeperUser","recordkeeperPass","recordkeeperPortfoliosAvailable","recordkeeperRiskBasedFundsAvailable", "plansQuery", "riskBasedFundsQuery", "portfolioQuery", "adviceQuery");
    }
    public function EnvisageSections()
    {
		return array("recordkeeperUrl","recordkeeperUser","recordkeeperPass");
    }
    public function reliusSections()
    {
    	return array("recordkeeperTpaid","recordkeeperAdviceProviderCd","recordkeeperUrl","recordkeeperUser","recordkeeperPass","recordkeeperPortfoliosAvailable","recordkeeperRiskBasedFundsAvailable");
    }

    public function ExpertPlanSections()
    {
    	return array("recordkeeperUrl","recordkeeperUser","recordkeeperPortfoliosAvailable");

    }

    public function AmrtsSections()
    {
        return array("recordkeeperUrl","recordkeeperUser","recordkeeperPass");

    }

    public function DSTSections()
    {
        return array("dstEncryption","LNCLNKey");
    }

    public function apiTestAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accounts');
        $accounts= $repository->findAll();
        return $this->render('ManageManageBundle:Accounts:apitest.html.twig',array("accounts" => $accounts));        
    }

    public function apiTestTableContentsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:APIAccounts');
        $APIAccounts= $repository->findBy(array(),array("id" => "DESC"));
        return $this->render('ManageManageBundle:Accounts:apitesttablecontents.html.twig',array("APIAccounts" => $APIAccounts));        

    }
    public function apiTestGenerateKeyAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();

        $key = $request->request->get("key");

        $filteredKey = "";
        for ($i = 0; $i < strlen($key); $i++)
        if ($key[$i] != " ")
        $filteredKey = $filteredKey.$key[$i];

        $key = $filteredKey;

        if (trim($key) == "")
        $key = "newapikey";

        while (strlen($key) < 25)
        $key = $key.$key;
        $newkey = "";
        for ($i = 0; $i < 25; $i++)
        $newkey = $newkey.$key[$i];


        $newkey = str_shuffle($newkey);

        return new Response($newkey);
    }
    public function apiTestAddApiAccountAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        if ($this->findkey($request->request->get("key")))
        return new Response("Key already exists!");
        else
        {
            $key = new APIAccounts();
            $key->userid = $request->request->get("userid");
            $key->apiKey = $request->request->get("key");
            $key->ipaddress = $request->request->get("ipaddress");
            $key->active = $request->request->get("active");
            $em->persist($key);
            $em->flush();
        }
        return new Response("Added");
    }

    public function findkey($key)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:APIAccounts');
        $key= $repository->findOneBy(array('apiKey' => $key));
        if ($key != null)
        return true;
        return false;
    }
    public function listPlansAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');

        return $this->render('ManageManageBundle:Accounts:listplans.html.twig',array("userid" => $request->query->get("id"),"roleType" => $session->roleType));

    }

    public function highestUserIdAction($id)
    {
        $session = $this->get('adminsession');
        $roleType = $session->roleType;
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:accountsUsers');
        if ($roleType == "vwise_admin")
        $types = array("vwise_admin","vwise","admin_master","admin","adviser");
        if ($roleType == "vwise")
        $types = array("vwise","admin_master","admin","adviser");

        foreach ($types as $type)
        {
            $account = $repository->findOneBy(array("roleType" => $type,"userid" => $id));
            if ($account != null)
            {
                return new Response($account->id);
            }
        }
        return new Response("");
    }

    public function copyPlanToAnotherAccountAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:plans');
        $plan= $repository->findOneBy(array('id' => $request->query->get("id")));        
        return $this->render('ManageManageBundle:Accounts:copyplantoanotheraccount.html.twig',array("plan" => $plan));
    }
    public function planExistsAction($partnerid,$planid)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:plans');
        $plan= $repository->findOneBy(array('partnerid' => $partnerid,"planid" => $planid));
        if ($plan != null)
        return new Response("false");
        return new Response("true");
    }    

	public function batchFileNotificationEmailsAction(Request $request) {
		return $this->render('ManageManageBundle:Accounts:batchFileNotificationEmails.html.twig', $request->query->all());
	}
	public function addBatchFileNotificationEmailAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$repository = $this->getDoctrine()->getRepository("classesclassBundle:AccountsBatchFileNotificationEmails");
		$email = $repository->findOneBy(['email' => trim($request->get("email"))]);
		if ($email === null) {
			$email = new AccountsBatchFileNotificationEmails;
			$email->email = trim($request->get("email"));
			$email->userid = $request->get("userid");
			$em->persist($email);
			$em->flush();
		}
		return new Response("success");
	}
	public function deleteBatchFileNotificationEmailAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$email = $this->getDoctrine()->getRepository("classesclassBundle:AccountsBatchFileNotificationEmails")->find($request->get("id"));
		$em->remove($email);
		$em->flush();
		return new Response("success");
	}

	public function uploadEmailListAccountAction(Request $request) {
        $userId = $request->query->get('id');
        return $this->render('ManageManageBundle:Accounts:emailListAccounts.html.twig',array("userId" => $userId));
    }

    public function uploadEmailListAccountProcessAction(Request $request) {
        $userId = $request->request->get('userId');
        $em = $this->getDoctrine()->getManager();
        $csv = array_map('str_getcsv', file($request->files->get("csvFile")->getRealPath()));
        $mappingHash = array("planid","firstName","lastName","email");
        if (preg_replace("/[^a-z0-9.]+/i", "", trim($csv[0][0])) == 'Planid') {
            unset($csv[0]);
        }
        $response = new Response("");

        $repo = $em->getRepository("classesclassBundle:plans");

        if (is_array($csv) && count($csv) > 0) {
            $recipientsRepository = $em->getRepository("classesclassBundle:launchToolRecipients");
            foreach ($csv as $row) {
                if (count(array_filter($row)) == 0) {
                    continue;
                }
                $rowHash = [];
                foreach($row as $key => $field)
                {
                    if (!empty($mappingHash[$key])) {
                        $rowHash[$mappingHash[$key]] = trim($field, " " . chr(160));
                    }
                }
                $currplan = $repo->findOneBy(array("userid" => $userId, "planid" => $row[0]));
                if (!empty($currplan)) {
                    $recipient = $recipientsRepository->findOneBy(['email' => $rowHash['email'], 'planid' => $currplan->id]);
                    if (is_null($recipient)) {
                        $recipient = new launchToolRecipients();
                    }
                    else {
                        $recipient->contactStatus = "UPDATED";
                        $recipient->lastModified = time();
                    }
                    foreach ($rowHash as $key => $field) {
                        $recipient->$key = $field;
                    }
                    $recipient->planid = $currplan->id;
                    $recipient->partnerid = $userId;
                    $recipient->enrolled = "y";
                    $em->persist($recipient);
                }
            }
            $em->flush();
            $this->uploadEmailNotification($userId);

            return $response;

        }
        $response->setStatusCode(500);
        return $response;
    }

    private function uploadEmailNotification($userId) {
        $session = $this->get('adminsession');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account= $repository->findOneBy(array('id' => $userId));
        $communicationSpecialistsIds = explode(",",$account->communicationSpecialist);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:communicationSpecialist');
        $communicationSpecialists = $repository->findBy(array('id' => $communicationSpecialistsIds));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $currentuser = $repository->findOneBy(array('id' => $session->clientid));
        if ($currentuser == null) {
            $currentuser = $repository->findOneBy(array('id' => $session->adviserid));
        }

        foreach ($communicationSpecialists as $communicationSpecialist)
        {
            $subject = "{$currentuser->firstname} {$currentuser->lastname} uploaded a client list in SmartConnect.";

            $contents = "<h3>The following account uploaded a client list in SmartConnect.</h3>";
            $contents .= "<b>First Name:</b> {$currentuser->firstname}<br/><br/>";
            $contents .= "<b>Last Name:</b> {$currentuser->lastname}<br/><br/>";
            $contents .= "<b>Partner ID:</b> {$session->userid}<br/><br/>";

            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom('info@smartplanenterprise.com')
                ->setTo($communicationSpecialist->emailAddress)
                ->setBody($contents,'text/html');
            $this->get('mailer')->send($message);
        }
    }
}
?>
