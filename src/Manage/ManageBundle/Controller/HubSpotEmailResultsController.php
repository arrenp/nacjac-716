<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Services\HubSpot\HubSpotEmailResultsService;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
set_time_limit(0);

class HubSpotEmailResultsController extends Controller {
    
    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section","Stats");
        $workflowService = $this->get("hub_spot.workflows");
        $workflows = $workflowService->getWorkflows();
        $filterString = 'Transactional';
        foreach ($workflows as $key => $workflow) {
            if (substr($workflow['name'], 0, strlen($filterString)) === $filterString) {
                unset($workflows[$key]);
            }
        }
        $names = array_column($workflows, 'name');
        array_multisort($names, $workflows);
        return $this->render('ManageManageBundle:Reports:HubSpotEmailResults/index.html.twig', ['workflows' => $workflows]);
    }
    
    public function createReportAction(Request $request) {
        $emailResultsService = $this->get("HubSpotEmailResultsService");
        $outputFilePath = tempnam(sys_get_temp_dir(), 'EmailResultsByContactReport_');
        $result = $emailResultsService->createReport($outputFilePath, $request->get("workflowId"), $request->get("startDate"), $request->get("endDate"));
        if ($result === HubSpotEmailResultsService::RESULT_REPORT_CREATED) {
            $request->getSession()->set('EmailResultsByContactReportPath', $outputFilePath);
            return new JsonResponse(['success' => true, 'data' => 'Report Created']);
        }
        if ($result === HubSpotEmailResultsService::RESULT_ERROR_WORKFLOW_NOT_FOUND) {
            return new JsonResponse(['success' => false, 'data' => 'Workflow not found']);
        }
        if ($result === HubSpotEmailResultsService::RESULT_ERROR_NO_WORKFLOW_ACTIONS) {
            return new JsonResponse(['success' => false, 'data' => 'No workflow actions found']);
        }
        if ($result === HubSpotEmailResultsService::RESULT_ERROR_NO_CAMPAIGN_ID) {
            return new JsonResponse(['success' => false, 'data' => 'No campaign id found for "email" action']);
        }
        if ($result === HubSpotEmailResultsService::RESULT_ERROR_NO_CAMPAIGNS) {
            return new JsonResponse(['success' => false, 'data' => 'No campaigns found']);
        }
        return new JsonResponse(['success' => false, 'data' => 'Unknown error occurred']);
    }
    
    public function downloadReportAction(Request $request) {
        $outputFilePath = $request->getSession()->get('EmailResultsByContactReportPath');
        $response = new BinaryFileResponse($outputFilePath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'EmailResultsByContactReport.csv');
        $response->headers->set('Content-Type', 'text/csv');
        return $response;
    }
}
