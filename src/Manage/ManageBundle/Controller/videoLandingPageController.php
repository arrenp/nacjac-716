<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use classes\classBundle\Entity\videoLandingPages;
use classes\classBundle\Entity\videoLandingPagesStats;
use classes\classBundle\Entity\videoLandingPagesCustomTags;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
class videoLandingPageController extends Controller
{

    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "MailChimp");
        return $this->render('ManageManageBundle:videoLandingPage:index.html.twig');
    }

    public function addAction()
    {
        return $this->render('ManageManageBundle:videoLandingPage:add.html.twig');
    }
    


    public function editAction(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:videoLandingPages');

        $videoLandingPages = $repository->findOneBy(array("id" => $request->query->get("id")));

        return $this->render('ManageManageBundle:videoLandingPage:edit.html.twig', array("videoLandingPages" => $videoLandingPages));
    }

    public function addSavedAction(Request $request)
    {
        $videoLandingPages = new videoLandingPages();
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        foreach ($arr as $key => $value)
        {
            $videoLandingPages->$key = $value;
        }
        $em->persist($videoLandingPages);
        $em->flush();
        return new Response("");
    }

    public function editSavedAction(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:videoLandingPages');
        $videoLandingPages = $repository->findOneBy(array("id" => $request->request->get("id")));
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        foreach ($arr as $key => $value)
        {
            $videoLandingPages->$key = $value;
        }
        $em->flush();
        return new Response("T" . $videoLandingPages->id);
    }
    
    

    public function previewNoFilterAction(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:videoLandingPages');
        $videoLandingPages = $repository->findOneBy(array("id" => $request->query->get("id")));
        return new Response($videoLandingPages->html);
    }

    public function previewAction(Request $request)
    {
        $requestArray = $request->query->all();
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:videoLandingPages');
        $videoLandingPages = $repository->findOneBy(array("id" => $request->query->get("id")));

        $doc = new \DOMDocument();
        @$doc->loadHTML($this->previewNoFilterAction($request)->getContent());
        $meta = $doc->getElementsByTagName('meta');
        foreach ($meta as $element) 
        {
            foreach ($element->attributes as $node) 
                $tag[$node->name] = $node->value;           
            $tags [$tag['name']]= $tag['content'];
        }
        $planid = $request->query->get("planid", "intro");
        $adviserid = $request->query->get("adviserid", ""); //for participantLoginWebsiteAddress
        $partnerid = $request->query->get("partnerid", "VWISE_EDUCATE_PROD");
        $templateid = $request->query->get("templateid", "0"); //must pass in for participantLoginWebsiteAddress to work
        $campaignid = $request->query->get("campaignid", "");
        $emailid = $request->query->get("emailid", "");
        $smartplanid = $request->query->get("smartplanid", "");
        $userid = $request->query->get("userid", "");
        
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:plans');
        if ($smartplanid != "" && $userid != "")
        {
            $plan = $repository->findOneBy(array("userid" => $userid, "id" => $smartplanid,"deleted" => 0));
            if ($plan != null)
            {
                $planid = $plan->planid;
                $partnerid = $plan->partnerid;
            }
        }
        else
        $plan = $repository->findOneBy(array("partnerid" => $partnerid, "planid" => $planid,"deleted" => 0));
        
        if ($plan == null)
        return new Response("Invalid Plan");
        
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array("id" => $plan->userid));
        
        
        $gastring = $planid . "_" . $partnerid;
        if ($campaignid != "")
        $gastring = $campaignid;

        $mp4 = $request->query->get("mp4", "9251/9251_VWS_062415.mp4");
        $poster = $request->query->get("poster", "9251/placeholder.png");
        if (substr_count($mp4,"http") == 0)
        $mp4url = "https://44fc3a2d1dc04efe1f02-4e2999b3e1a2f52bb8d17ee2268e655c.ssl.cf1.rackcdn.com/".$mp4;
        else
        $mp4url = $mp4;

        $logos = new \stdClass();
        $logos->leftImageUrl = $this->forward('PlansPlansBundle:Outreach:providerLogo', array("planid" => $plan->id,"campaignid" => $campaignid))->getContent();
        $logos->rightImageUrl = $this->forward('PlansPlansBundle:Outreach:sponsorLogo', array("planid" => $plan->id,"campaignid" => $campaignid))->getContent();
        $eventTag = $videoLandingPages->id . "_" . $videoLandingPages->name . "_" . $mp4;


        $participantLoginPageUrl = $this->forward('PlansPlansBundle:Outreach:participantLoginWebsiteAddress', array("planid" => $plan->id, "adviserid" => $adviserid, "templateid" => $templateid))->getContent();
        //$videoLandingPages->html
       // foreach 
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:videoLandingPagesCustomTags');
        $videoLandingPagesCustomTags = $repository->findAll();
        //var_dump($tags);
        foreach ($videoLandingPagesCustomTags as $videoLandingPagesCustomTag)
        {
            //echo "custom_".$videoLandingPagesCustomTag->name;
            if (isset($tags["custom_".$videoLandingPagesCustomTag->name]))
            {
                $videoLandingPages->html = str_replace("<vwise_custom_".$videoLandingPagesCustomTag->name."/>",$videoLandingPagesCustomTag->html,$videoLandingPages->html); 
               // echo "test2";
            }
            
            
        }
        
        foreach ($tags as $name => $value)
        {
            
            if ($name == "video" && $value == 1)
            {
                $resolution = "width:100%;height:100%";
                $endactionhtml = 'mediaelement.load(); $("#main-vid").hide();$("#placeholderimage").show();
                 ';
                $additionaljavascript = "";
                if (isset($tags['gateway']) && $tags['gateway'])
                {
                    if (isset($tags['video_resolution'])) $resolution = $tags['video_resolution'];
                    $endactionhtml = $endactionhtml."
                   
                    $('#videoContainer').animate({opacity:'.25'},1000,function()
                    {
                        $('.ribbon').animate(
                        {
                            top: '120px'
                          },1000,function(){
                            $('.replay_button').show().animate({top:'410px'},1000,function(){
                              $('.replay_button').css('z-index','50');
                            });
                          }
                        );
                    });
                    playerLocked = 1;
                    ";
                    $additionaljavascript = "
                    $( document ).ready(function() 
                    {    
                        $('.replay_button').click(function()
                        {
                            $(this).hide();
                            $('.ribbon').animate({ top: '530px' },1000,function()
                            {
                              $('#videoContainer').animate({opacity:'1'});
                              replayVideo();
                            });
                        });
                        


                        $('#reminderForm').validate({
                          errorPlacement: function(error, element) {},
                          submitHandler: function(form) {
                            var reminderTime = $('input[name=date]').val() + ' ' + $('input[name=time]').val();
                            form.submit();
                          }
                        });
                        



                    });
                    
                    mediaelement.addEventListener('play', function(e) 
                    {
                        if (!playerLocked)
                        mediaelement.play();
                    });     
                    
                    "
                    ;
                }

                $videoLandingPages->html = str_replace("<vwise_video/>", '
 
                <div  style="width:100%;" id = "videoContainer">
                   <div id = "placeholderimage" style = "display:none;margin: 0 auto;' . $resolution . ';display:none"><img src = "https://44fc3a2d1dc04efe1f02-4e2999b3e1a2f52bb8d17ee2268e655c.ssl.cf1.rackcdn.com/'.$poster.'" onclick = "replayVideo()" style = "position:relative" /></div>

                    <div id = "videoPlayer" style="margin: 0 auto;' . $resolution . '"></div>
                </div>




                <script type="text/javascript">
                  var playedVideo = 0;
                  var playerLocked = 0;
                  var currentTime = 0;
                  var videoEnded = 0;
                  var $element = $(\'<video id="main-vid" src="' . $mp4url . '" poster = "https://44fc3a2d1dc04efe1f02-4e2999b3e1a2f52bb8d17ee2268e655c.ssl.cf1.rackcdn.com/' . $poster . '" controls="controls" style = "width:100%;height:100%"  ></video>\');
                  var mediaelement = new MediaElement($element.get(0), {});
                  var weight = [];
                  var xYPosition = [];
                  var WeightString = \'\';
                  var xYPositionString = \'\';
                  mediaelement.addEventListener(\'ended\', function(e) 
                  {

                      videoEndAction();

                  }, false);
                  mediaelement.addEventListener(\'loadeddata\', function(e) 
                  {
                      if (playedVideo == 0)
                      {
                          //setTimeout(function(){ mediaelement.play() }, 1000);
                          _gaq.push([\'_trackEvent\', \'Video\' , \'Loaded\',\'' . $eventTag . '\']);
                          mediaelement.play();
                          playedVideo++;
                          saveMediaStats();
                      }
                  }, false);
                  
                  mediaelement.addEventListener(\'timeupdate\', function(e) 
                  {
                    //if (!videoEnded)
                    saveMediaStats("watched=" + mediaelement.currentTime);
                    //alert(mediaelement.currentTime);
                  }, false);
                  mediaelement.addEventListener(\'pause\', function(e) 
                  {
                    //if (!videoEnded)
                    saveMediaStats("watched=" + mediaelement.currentTime);
                    //alert(mediaelement.currentTime);
                  }, false);

                  mediaelement.addEventListener(\'error\', function(e) 
                  {   

                      if (mediaelement.currentTime == 0 && playedVideo)
                      {
                          mediaelement.load();
                          videoEndAction();
                      }

                  }, false);

                  $(\'#videoPlayer\').html(mediaelement);

                  function videoEndAction()
                  {     
                    _gaq.push([\'_trackEvent\', \'Video\' , \'Ended\',\'' . $eventTag . '\']);
                     saveMediaStats("complete=1&watched=" + mediaelement.duration); 
                     videoEnded = 1;
                     
                     
                    ' . $endactionhtml . '
                  }
                  function saveMediaStats(additionalargs)
                  {
                    
                    if(typeof(additionalargs)===\'undefined\') 
                    additionalargs = "";
                    else
                    additionalargs = "&" + additionalargs;
                    var page = "' . $this->generateUrl('_manage_videoLandingPage_saveStatRecord') . '";
                    var querystring = "campaignid='.$campaignid.'&emailid='.$emailid.'&userid='.$plan->userid.'&planid='.$plan->id.'&videoLandingPagesId='.$videoLandingPages->id.'&played=1&mp4='.$mp4.'&length=" +  mediaelement.duration +  additionalargs;    
                    //alert(querystring);
                    $.post(page, querystring).done(function(data) 
                    {
                        
                    });
                  }
                  
                  //window.onbeforeunload = function(){ saveMediaStats("watched=" + mediaelement.currentTime); };
                  /*  $(document).ready(function()
                    {
                        $(window).bind("beforeunload", function() { 
                            saveMediaStats("watched=" + mediaelement.currentTime);
                        });
                    });  
                    */
                  
                    $("body").mousemove(function(ev) 
                    {
                      if (mediaelement.currentTime > 0 && !mediaelement.paused) 
                      {
                        lastX = ev.pageX;
                        lastY = ev.pageY;
                        if(xYPosition.length < 6) 
                        {
                          xYPosition.push(lastX + \':\' + lastY); 
                          weight.push(ev.timeStamp);
                        }
                        if(xYPosition.length == 5) 
                        {
                          for(var index1 = 0; index1 < xYPosition.length; index1++) 
                          {
                            xYPositionString += xYPosition[index1] + \',\';
                          }
                          for(var index = 0; index < weight.length; index++) 
                          { 
                            WeightString +=  weight[index] + \',\';
                          }
                          indexXYPosition = xYPositionString.lastIndexOf(",");
                          indexWeight = WeightString.lastIndexOf(",");
                          xYPositionString = xYPositionString.substring(0, indexXYPosition) + xYPositionString.substring(indexXYPosition + 1);
                          WeightString = WeightString.substring(0, indexWeight) + WeightString.substring(indexWeight + 1);
                          
                          
                          saveMediaStats("weight=" + WeightString + "&mousePosition=" + xYPositionString);
                          
                          //alert(\'X:Y \' +  xYPositionString);
                          //alert(\'Weight \' + WeightString);
                          weight = []
                          xYPosition = [];
                          WeightString = \'\';
                          xYPositionString = \'\';
                        }
                      }
                    });


                    ' . $additionaljavascript . '
                </script>
                <!-- Video goes here. This is the poster image for loading and when the video is finished-->
                ', $videoLandingPages->html);
                $videoheader = '
             <link href="https://gateway.smartplanenterprise.com/css/custom-theme/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">    
            <script src="https://gateway.smartplanenterprise.com/html5.js"></script>
            <script src="https://admin.smartplanenterprise.com/js/lib/jquery.js"></script>
            <script src ="https://admin.smartplanenterprise.com/js/mediaelement/build/mediaelement-and-player.min.js"></script>
            <link rel="stylesheet" href="https://admin.smartplanenterprise.com/js/mediaelement/build/mediaelementplayer.css" />
            <script src="https://gateway.smartplanenterprise.com/js/jquery.validate.min.js"></script>
            <script src="//code.jquery.com/jquery-1.10.2.js"></script>
            <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
            <script src="https://gateway.smartplanenterprise.com/js/timepicker.js"></script>
            <script>   

            ';
                $videoheaderbottom = '
            </script> 
            <script>
            function replayVideo()
            {
                mediaelement.play();
                $("#placeholderimage").hide();
                $("#main-vid").show();

            }
            </script>
            </head>      
            ';

                $videoheadergateway = '
            $( document ).ready(function() 
            {
                $(\'input[name=date]\').datepicker();

                $(\'input[name=time]\').timepicker({
                controlType: \'select\',
                timeFormat: \'hh:mm tt\',
                stepMinute: 15,
                hourMin: 5,
                hourMax: 22
                });
                var reminderFormCopy = $( "#reminderForm" ).clone();
                reminderFormCopy.attr("id","reminderForm2");
                reminderFormCopy.hide();
                reminderFormCopy.appendTo("body");
                
                $( "#reminderForm" ).submit(function( event ) 
                {      
                    event.preventDefault();
                    if ($(\'#reminderForm input[name=date]\').val() != "" && $(\'#reminderForm input[name=time]\').val() != "")
                    {
                        _gaq.push([\'_trackEvent\', \'Reminder\' , \'Date\',$(\'#reminderForm input[name=date]\').val()]);
                        _gaq.push([\'_trackEvent\', \'Reminder\' , \'Time\',$(\'#reminderForm input[name=time]\').val()]);
                        $(\'#reminderForm2 input[name=date]\').val($(\'#reminderForm input[name=date]\').val());
                        $(\'#reminderForm2 input[name=time]\').val($(\'#reminderForm input[name=time]\').val());
                        $( "#reminderForm2" ).submit();
                    }
                    else
                    {
                        alert("Date and Time field must be filled out");
                    }
                });


            });
            ';
                if (isset($tags['gateway']) && $tags['gateway'])
                $videoheader = $videoheader . $videoheadergateway . $videoheaderbottom;
                else
                $videoheader = $videoheader . $videoheaderbottom;
                $videoLandingPages->html = str_replace("</head>", $videoheader, $videoLandingPages->html);
            }
            if ($name == "provider_logo_url" && $value == 1)
            {
                $videoLandingPages->html = str_replace("<vwise_provider_logo_url/>", $logos->leftImageUrl, $videoLandingPages->html);
            }
            if ($name == "sponsor_logo_url" && $value == 1)
            {
                $videoLandingPages->html = str_replace("<vwise_sponsor_logo_url/>", $logos->rightImageUrl, $videoLandingPages->html);
            }
            if ($name == "participant_login_page_url" && $value == 1)
            {
                $videoLandingPages->html = str_replace("<vwise_participant_login_page_url/>", $participantLoginPageUrl, $videoLandingPages->html);
            }
            if ($name == "return_parameters" && $value == 1)
            {
                $returnParameters = "";

                foreach ($requestArray as $key => $value)
                {
                    if ($key != "id") $returnParameters = $returnParameters . "&" . $key . "=" . $value;
                }

                $videoLandingPages->html = str_replace("<vwise_return_parameters/>", $returnParameters, $videoLandingPages->html);
            }
            if ($name == "event_tag" && $value == 1)
            {
                $videoLandingPages->html = str_replace("<vwise_event_tag/>", $eventTag, $videoLandingPages->html);
            }
            if ($name == "domain" && $value == 1)
            {
                $videoLandingPages->html = str_replace("<vwise_domain/>", $request->server->get('HTTP_HOST'), $videoLandingPages->html);
            }
            if ($name == "companywebaddress" && $value == 1)
            {
                $videoLandingPages->html = str_replace("<vwise_companyWebAddress/>", $account->companyWebAddress, $videoLandingPages->html);
            }
            if (substr_count($name,"videopage") > 0)
            {
                $landingpageid = str_replace("videopage","",$name);
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:videoLandingPages');
                $videoLandingPage = $repository->findOneBy(array('id' => $landingpageid)); 
                $url = $this->generateUrl('_manage_videoLandingPage_preview',array("name" => $videoLandingPage->name,"id" => $videoLandingPage->id),true)."&smartplanid=".$smartplanid."&userid=".$userid."&templateid=".$templateid."&adviserid=".$adviserid;
                $videoLandingPages->html = str_replace("<vwise_".$name."/>",$url, $videoLandingPages->html);
            }
            if (substr_count($name,"redirect_") > 0)
            {
                $videoLandingPages->html = str_replace("<vwise_".$name."/>",$request->query->get($name,"https://smartplanenterprise.com"),$videoLandingPages->html);
            }            
            
            
        }


        
        $videoLandingPages->html = str_replace("<head>", "<head>
  <script>
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29981405-31']);
  _gaq.push(['_setCustomVar',2,'Plan ID',".json_encode($gastring).",2]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  </script>
", $videoLandingPages->html);

        return new Response($videoLandingPages->html);
    }



    public function reminderAction(Request $request)
    {
        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename="SmartPlan Reminder.ics"');

        $title = $request->request->get('title');
        $location = $request->request->get('location');
        $notes = stripslashes($request->request->get('notes'));

        $date = $request->request->get('date');
        $time = $request->request->get('time');

        $dateArray = explode('/', $date);

        $month = $dateArray[0];
        $day = $dateArray[1];
        $year = $dateArray[2];
        $dateUTC = $year . $month . $day;

        $timeArray = explode(' ', $time);
        $period = $timeArray[1];
        $time = $timeArray[0];

        $hour = substr($time, 0, 2);
        $minute = substr($time, 3, 2);
        if ($period === 'pm') $hour = $hour + 12;
        $endHour = $hour + 1;
        $seconds = '00';

        $startStr = $year . $month . $day . 'T' . $hour . $minute . $seconds;
        $endStr = $year . $month . $day . 'T' . $endHour . $minute . $seconds;

        $icsFile = "BEGIN:VCALENDAR\n";
        $icsFile .= "PRODID:-//Google Inc//Google Calendar 70.9054//EN\n";
        $icsFile .= "VERSION:2.0\n";
        $icsFile .= "CALSCALE:GREGORIAN\n";
        $icsFile .= "METHOD:PUBLISH\n";
        $icsFile .= "BEGIN:VEVENT\n";
        $icsFile .= "DTSTART:" . $startStr . "\n";
        $icsFile .= "DTEND:" . $endStr . "\n";
        $icsFile .= "DESCRIPTION:" . $notes . " " . $location . "\n";
        $icsFile .= "LOCATION:" . $location . "\n";
        $icsFile .= "SEQUENCE:0\n";
        $icsFile .= "STATUS:CONFIRMED\n";
        $icsFile .= "SUMMARY:" . $title . "\n";
        $icsFile .= "TRANSP:OPAQUE\n";
        $icsFile .= "END:VEVENT\n";
        $icsFile .= "END:VCALENDAR";


        $response = new Response($icsFile);
        
        $response->headers->set('Content-Type', 'text/calendar');
        return $response;
        
    }


    public function saveStatRecordAction(Request $request)
    {
        $generalMethods = $this->get('GeneralMethods');
        $add = false;
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:videoLandingPagesStats');
        $videoLandingPagesStats = $repository->findOneBy(array("videoLandingPagesId" => $request->request->get("videoLandingPagesId"), "sessionid" => $session->getId(), "ipaddress" => $generalMethods->get_client_ip()));
        if ($videoLandingPagesStats == null)
        {
            $videoLandingPagesStats = new videoLandingPagesStats();
            $videoLandingPagesStats->sessionid = $session->getId();
            $videoLandingPagesStats->ipaddress = $generalMethods->get_client_ip();
            $videoLandingPagesStats->length = $request->request->get("length",0);
            $add = true;
        }
        $arr = $request->request->all();
        foreach ($arr as $key => $value)
        {
            if ($key != "id" && $key != "watched" && $key != "weight" && $key != "mousePosition" && $key != "length")
            $videoLandingPagesStats->$key = $value;
        }
        if (floatval($videoLandingPagesStats->watched) -  floatval($request->request->get("watched",0)) < 0    )
        $videoLandingPagesStats->watched = floatval($request->request->get("watched",0));
        if ($request->request->get("weight","") != "" )
        {
            if ($videoLandingPagesStats->weight != "")
            $videoLandingPagesStats->weight = $videoLandingPagesStats->weight."|";
            $videoLandingPagesStats->weight = $videoLandingPagesStats->weight.$request->request->get("weight","");
        }
        if ($request->request->get("mousePosition","") != "" )
        {
            if ($videoLandingPagesStats->mousePosition != "")
            $videoLandingPagesStats->mousePosition = $videoLandingPagesStats->mousePosition."|";
            $videoLandingPagesStats->mousePosition = $videoLandingPagesStats->mousePosition.$request->request->get("mousePosition","");
        }        
        
        if ($add) $em->persist($videoLandingPagesStats);
        $em->flush();
        return new Response($request->request->get("id"));
    }
    public function addTagAction()
    {
        return $this->render('ManageManageBundle:videoLandingPage:addtag.html.twig');
    }
    
    public function addTagSavedAction(Request $request)
    {
        $videoLandingPages = new videoLandingPagesCustomTags();
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        foreach ($arr as $key => $value)
        {
            $videoLandingPages->$key = $value;
        }
        $em->persist($videoLandingPages);
        $em->flush();
        return new Response("");        
    }
    
    

    public function editTagAction(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:videoLandingPagesCustomTags');

        $tag = $repository->findOneBy(array("id" => $request->query->get("id")));

        return $this->render('ManageManageBundle:videoLandingPage:edittag.html.twig', array("tag" => $tag));
    }
    
    public function editTagSavedAction(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('classesclassBundle:videoLandingPagesCustomTags');
        $videoLandingPagesCustomTags = $repository->findOneBy(array("id" => $request->request->get("id")));
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        foreach ($arr as $key => $value)
        {
            $videoLandingPagesCustomTags->$key = $value;
        }
        $em->flush();
        return new Response("");
    }    
    
}
