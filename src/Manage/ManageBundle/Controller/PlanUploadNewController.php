<?php
namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use classes\classBundle\Entity\plans;
use classes\classBundle\Entity\plansModules;
use classes\classBundle\Entity\plansMessaging;
use classes\classBundle\Entity\planuploadHeaders;
use classes\classBundle\Entity\plansLibraryHotTopics;
use classes\classBundle\Entity\plansFunds;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansPortfolioFunds;
use classes\classBundle\Entity\plansRiskBasedFunds;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Entity\planuploadTableColumnMappings;
use classes\classBundle\Entity\FundsGroupsValues;
use classes\classBundle\Entity\PlansMessages;
use Shared\MessagingBundle\Classes\Messaging;

use classes\classBundle\Entity\PlansInvestmentsLinkYearsToRetirementAndScore;
use classes\classBundle\Entity\PlansInvestmentsConfigurationColors;
use classes\classBundle\Entity\PlansInvestmentsConfigurationFundsGroupValuesColors;
use classes\classBundle\Entity\PlansInvestmentsScore;
use classes\classBundle\Entity\PlansInvestmentsYearsToRetirement;
use classes\classBundle\Entity\PlansInvestmentsConfigurationDefaultFundsGroupValuesColors;
use classes\classBundle\Entity\launchUsers;
use classes\classBundle\Entity\plansDocuments;
set_time_limit(0);

class PlanUploadNewController extends Controller
{
    private $scoreMap = null;
    private $yearsToRetirementMap = null;
    public function indexAction(Request $request)
    {   
        $this->initializeFileUpload($request);
        unlink($this->filename);
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        $params = $this->getParams();

        $accountid = $request->query->get('id');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $accountid));
        $params = array_merge(array('account' => $account), $params);
        return $this->render('ManageManageBundle:PlanUploadNew:index.html.twig', $params);
    }
    public function mappingSectionsAction()
    {
        return $this->render('ManageManageBundle:PlanUploadNew:mappingsections.html.twig',$this->getParams());
    }
    public function getParams()
    {
        $actions = $this->getActions();
        $types = $this->getTypes();
        $categories = $this->getCategories();
        $uploadOptions = $this->getUploadOptions();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:planuploadHeaders');   
        $planuploadHeaders = $repository->findBy(array(),array("id" => "desc"));
        $planuploadHeadersHash = array();
        foreach ($planuploadHeaders as $header) {
            $planuploadHeadersHash[$header->action."_".$header->type."_".$header->category][] =  $header;
        }
                
        $mappingSections = array();
        foreach ($actions as  $action)
        {
            foreach ($types as $type)
            {
                foreach ($categories as $category)
                {
                    $hashString = $action['value']."_".$type['value']."_".$category['value'];
                    $mappingSections[$hashString]['id'] = $hashString;
                    $mappingSections[$hashString]['urlparams'] = "action=".$action['value']."&type=".$type['value']."&category=".$category['value'];
                    $mappingSections[$hashString]['list'] = array();
                    $mappingSections[$hashString]['categoryDesc'] = $category['description'];
                    if (isset($planuploadHeadersHash[$hashString])) {
                        $mappingSections[$hashString]['list']= $planuploadHeadersHash[$hashString];
                    }                    
                }
            }
        }
        $mappingSections = json_decode(json_encode($mappingSections),true);
        $params = 
        [
            "actions" => $actions,
            "types" => $types,
            "categories" => $categories,
            "mappingSections" => $mappingSections,
            "uploadOptions" => $uploadOptions
        ];
        return $params;
    }

    public function addMappingAction(Request $request)
    {
        ini_set('auto_detect_line_endings', true);
        $this->initializeFileUpload($request);
        if (!file_exists($this->filename)) {
            $theCsvHeader = array();
        }
        else {
            
            if ($this->isXmlFile()) {
                $header = array();
                $xml = simplexml_load_file($this->filename);
                foreach ($xml->children() as $val) {
                    $header = array_merge($header, array_diff(array_keys(get_object_vars($val)), $header));
                }
            }
            else {
                $fp = fopen($this->filename, 'r');
                $header = str_getcsv(fgets($fp));
                fclose($fp);
            }

            $theCsvHeader = array();
            foreach ($header as $val) {
                $theCsvHeader[] = array(
                    'description' => $val,
                    'table' => '',
                    'field' => ''
                );
            }
        }
        $fields = $this->getFields($request->query->get("category"),$request->get("type"),$request->query->get("action"));
        
        return $this->render('ManageManageBundle:PlanUploadNew:addmapping.html.twig',array(
            "includeFields" => $fields,
            "name" => "", 
            "theCsvHeader"=>$theCsvHeader
        ));
    }
    public function getFields($category,$type,$action)
    {
        if ($category == "plan") {
            $fields = $this->getHeaderPlansFields($action, $type);
        }
        if ($category == "fund") {
            $fields = $this->getHeaderFundsFields($action);    
        }
        if ($category == "portfolio" || $category == "riskbasedfund") {
            $fields = $this->getHeaderPortfoliosFields($action); 
        }
        if ($category == "portfoliofunds") {
            $fields = $this->getHeaderPortfolioFundsFields($action);
        }
        if ($category == "messaging") {
            $fields = $this->getHeaderMessagesFields($action);
        }
        if ($category == "documents") {
            $fields = $this->getHeaderDocumentsFields($action);
        }
        return $fields;
    }
    protected function getFieldsHash($category,$type,$action) {
        $fields = $this->getFields($category, $type, $action);
        $fieldsHash = array();
        foreach ($fields as $field) {
            $fieldsHash[$field['table']][$field['field']] = $field;
        }
        return $fieldsHash;
    }
    public function editMappingAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:planuploadHeaders');   
        $planuploadHeaders = $repository->findOneBy(array("id" => $request->query->get("id")));
        $fields = json_decode($planuploadHeaders->header,true);
        $allFields = $this->getFields($planuploadHeaders->category,$planuploadHeaders->type,$planuploadHeaders->action);
        
        return $this->render('ManageManageBundle:PlanUploadNew:editmapping.html.twig',array(
            "includeFields" => $allFields,
            "name" => $planuploadHeaders->name, 
            "theCsvHeader"=>$fields
        ));
    }
    public function editMappingSavedAction(Request $request)
    {    
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:planuploadHeaders');
        $planuploadHeaders = $repository->findOneBy(array("id" => $request->request->get("id")));
        $fields = $this->getFields($planuploadHeaders->category,$planuploadHeaders->type,$planuploadHeaders->action);
        $validation = $this->mappingValidate($request, $fields);
        if ($validation !== true) {
            return new Response($validation);
        }     
        $requestArray = $request->request->all();
        $this->saveMapping($requestArray,$planuploadHeaders,false);        
        return new Response("saved-alert");
    }
    public function deleteMappingAction()
    {
        return $this->render('ManageManageBundle:PlanUploadNew:mappingdelete.html.twig');
    }
    public function deleteMappingSavedAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:planuploadHeaders');   
        $planuploadHeaders = $repository->findOneBy(array("id" => $request->request->get("id")));
        $em = $this->getDoctrine()->getManager();
        $em->remove($planuploadHeaders);
        $em->flush();  
        return new Response("deleted");
    }
    function mappingValidate($request, $fields)
    {
        if (trim($request->request->get("name")) == "") {
            return "validationfailurename";
        }
        
        $requiredHash = array();
        foreach ($fields as $val) {
            if ($val['required']) {
                $requiredHash[$val['table'].'/'.$val['field']] = $val;
            }
        }
        
        foreach ($request->get('list') as $val) {
            $hash = $val['table'].'/'.$val['field'];
            if (isset($requiredHash[$hash])) {
                unset($requiredHash[$hash]);
            }
            
            if (empty($val['description'])) {
                return "validationfailureheader";
            }
        }
        
        if (count($requiredHash) > 0) {
            return "validationfailurefield";
        }

        return true;
    }
    public function saveMapping($requestArray,$planuploadHeaders,$add)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($requestArray as $key => $row)
        $planuploadHeaders->$key = $row;           
        $planuploadHeaders->header = json_encode($requestArray['list']);
        if ($add)
        $em->persist($planuploadHeaders);
        $em->flush();          
    }
    public function addMappingSavedAction(Request $request)
    {    
        $fields = $this->getFields($request->get('category'),$request->get('type'),$request->get('action'));
        $validation = $this->mappingValidate($request, $fields);
        if ($validation !== true) {
            return new Response($validation);
        }
        $requestArray = $request->request->all();
        $planuploadHeaders = new planuploadHeaders();
        $this->saveMapping($requestArray,$planuploadHeaders,true);        
        return new Response("saved-alert");
    }
    public function getHeaderPortfolioFundsFields($action)
    {
        $fields = array();
        $table = "plansPortfolioFunds";
        $this->addHeaderField($fields, "Plan Id", "planid",$table,true);
        $this->addHeaderField($fields, "Portfolio Name", "name",$table,true);
        $this->addHeaderField($fields, "Ticker", "ticker",$table,true);
        if ($action != "delete")
        {
            $this->addHeaderField($fields, "Percent", "percent",$table,true);
        }
        return $fields;
    }
    public function getHeaderPlansFields($action,$type)
    {
        if ($action == "delete")
        {
            $fields = array();
            $table = "plans";
            $this->addHeaderField($fields, "Plan Id", "planid",$table,true);
            return $fields;
        }
        else {
            return array_merge($this->getHeaderPlans($type),$this->getHeaderModules(),$this->getHeaderSponsorConnectRequests(),$this->getHeaderHexColors());
        }
        
    }
    public function getHeaderFundsFields($action)
    {
        $fields = array();
        $table = "plansFunds";
        if ($action == "delete")
        {
            $this->addHeaderField($fields, "Plan Id", "planid",$table,true);           
            $this->addHeaderField($fields, "Fund Ticker", "ticker",$table,true);
        }    
        else
        {
            $this->addHeaderField($fields, "Plan Id", "planid",$table,true);
            $this->addHeaderField($fields, "Fund Name", "name",$table,true);
            $this->addHeaderField($fields, "Fund Ticker", "ticker",$table,true);
            $this->addHeaderField($fields, "Fund Id", "fundId",$table);
            $this->addHeaderField($fields, "Fund Link", "link",$table);
            $this->addHeaderField($fields, "Fund Order Id", "orderid",$table);
            $this->addHeaderField($fields, "QDIA Default", "qdiaDefault", $table);
            $this->addHeaderField($fields, "QDIA Video", "qdiaVideo", $table);
            $this->addHeaderField($fields, "QDIA URL", "qdiaUrl", $table);
            $this->addHeaderField($fields, "Display On/Off", "display",$table);
            $this->addHeaderField($fields, "Fund Position", "position",$table);
            
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:FundsGroups'); 
            $fundsGroupsArray = $repository->findAll();
            foreach ($fundsGroupsArray as $fundsGroups) {
                $this->addHeaderField($fields,  "{$fundsGroups->name} Group Value", $fundsGroups->id, "fundsGroupsValues");
            }
        }
        return $fields;
    }
    public function getHeaderPortfoliosFields($action)
    {
        if ($action == "delete")
        {
            $fields = array();
            $table = "plansPortfolios";
            $this->addHeaderField($fields, "Plan Id", "planid",$table,true); 
            $this->addHeaderField($fields, "Portfolio Name", "name",$table,true);
        }
        else {
            $fields = $this->getHeaderFieldsFilter("plansPortfolios",array("planid","name","profile"));
        }
        return $fields;
    }
    public function getHeaderPlans($type)
    {
        $fields = array();
        $table = "plans";
        $this->addHeaderField($fields, "Plan Id", "planid",$table,true);
        $this->addHeaderField($fields, "Name", "name",$table,true);
        $this->addHeaderField($fields, "Redirect Address", "redirectWebsiteAddress",$table);
        $this->addHeaderField($fields, "Error Notification", "errorNotificationEmail",$table);
        $this->addHeaderField($fields, "Profile Notification", "profileNotificationEmail",$table);
        $this->addHeaderField($fields, "Interface Color", "interfaceColor",$table);
        $this->addHeaderField($fields, "Interface Layout", "interfaceLayout",$table);
        $this->addHeaderField($fields, "Risk Type", "riskType",$table);
        $this->addHeaderField($fields, "vAd Frequency", "vadFrequency",$table);
        $this->addHeaderField($fields, "Fund Link", "fundLink",$table);
        $this->addHeaderField($fields, "IRS Code", "IRSCode",$table);
        $this->addHeaderField($fields, "PIP Content", "pipContent",$table);
        $this->addHeaderField($fields, "PIP Position", "pipPosition",$table);
        $this->addHeaderField($fields, "Sections - Risk Questionnaire", "sections",$table);
        $this->addHeaderField($fields, "Sponsor Connect", "sponsorConnect",$table);
        $this->addHeaderField($fields, "Timeout Website", "timeoutWebsiteAddress",$table);
        if ($type === 'smartplan') {
            $this->addHeaderField($fields, "SmartPlan Allowed", "smartPlanAllowed",$table);
            $this->addHeaderField($fields, "Irio Allowed", "irioAllowed",$table);
            $this->addHeaderField($fields, "Powerview Allowed", "powerviewAllowed",$table);
        }
        $this->addHeaderField($fields, "Advisor First", "advisorFirst", $table);
        $this->addHeaderField($fields, "Advisor Last", "advisorLast", $table);
        $this->addHeaderField($fields, "Advisor Email", "advisorEmail", $table);
        $this->addHeaderField($fields, "Advisor Phone", "advisorPhone", $table);
        $this->addHeaderField($fields, "Use Enrollment Redirect", "enrollmentRedirectOn", $table);
        $this->addHeaderField($fields, "Enrollment Redirect Address", "enrollmentRedirectAddress", $table);
        $this->addHeaderField($fields, "Support Contact First Name", "supportContactFirstName", $table);
        $this->addHeaderField($fields, "Support Contact Last Name", "supportContactLastName", $table);
        $this->addHeaderField($fields, "Support Contact Email", "supportContactEmail", $table);
        $this->addHeaderField($fields, "Support Contact Phone", "customerServiceNumber", $table);
        $this->addHeaderField($fields, "Last Day for Loans", "lastDayForLoans", $table);
        $this->addHeaderField($fields, "Freeze Transactions Date", "freezeTransactionsDate", $table);
        $this->addHeaderField($fields, "Takeover Date", "takeoverDate", $table);
        $this->addHeaderField($fields, "Freeze End Date", "freezeEndDate", $table);
        $this->addHeaderField($fields, "Sox Notice URL", "soxNoticeURL", $table);
        $this->addHeaderField($fields, "Initial Plan", "initialPlan", $table);
        $this->addHeaderField($fields, "Standard Path", "standardPathId", $table);
        $this->addHeaderField($fields, "SmartEnroll Path", "smartenrollPathId", $table);
        $this->addHeaderField($fields, "SmartExpress Path", "smartexpressPathId", $table);
        return $fields;
    }
    public function getHeaderModules()
    {
        $fields = array();
        $table = "plansModules";
        $this->addHeaderField($fields, "Deferral Type", "deferralType",$table);
        $this->addHeaderField($fields, "Matching", "matchingContributions",$table);
        $this->addHeaderField($fields, "Catchup", "catchup", $table);
        $this->addHeaderField($fields, "Loans", "loans", $table);
        $this->addHeaderField($fields, "Enrollment Type", "enrollment", $table);
        $this->addHeaderField($fields, "Vesting", "vesting", $table);
        $this->addHeaderField($fields, "Beneficiary", "beneficiaries", $table);
        $this->addHeaderField($fields, "Beneficiary Auto Add", "beneficiaryAutoAdd", $table);
        $this->addHeaderField($fields, "Deferral Format", "deferralFormat", $table);
        $this->addHeaderField($fields, "Investment Selector Default", "investmentSelectorDefault", $table);
        $this->addHeaderField($fields, "Risk Based Investment", "investmentSelectorRiskBasedPortfolio", $table);
        $this->addHeaderField($fields, "Custom Choice", "investmentSelectorCustomChoice", $table);
        $this->addHeaderField($fields, "Minimum Contribution Pre tax", "minimumContribution", $table);
        $this->addHeaderField($fields, "Maximum Contribution Pre tax", "maximumContribution", $table);
        $this->addHeaderField($fields, "Matching Percent", "matchingPercent", $table);
        $this->addHeaderField($fields, "Matching Cap", "matchingCap", $table);
        $this->addHeaderField($fields, "Secondary Matching Percent", "secondaryMatchingPercent", $table);
        $this->addHeaderField($fields, "Secondary Matching Cap", "secondaryMatchingCap", $table);
        $this->addHeaderField($fields, "Language Dropdown", "languageDropdown", $table);
        $this->addHeaderField($fields, "Retirement Needs Flow", "retirementNeedsFlow", $table);
        $this->addHeaderField($fields, "Social Security Multiplier", "socialSecurityMultiplier", $table);
        $this->addHeaderField($fields, "Pension Estimator URL", "pensionEstimatorURL", $table);
        $this->addHeaderField($fields, "Investment Selector Target Maturity Fund", "investmentSelectorTargetMaturityFund", $table);
        $this->addHeaderField($fields, "Minimum Contribution Roth", "minimumContributionRoth", $table);
        $this->addHeaderField($fields, "Maximum Contribution Roth", "maximumContributionRoth", $table);
        $this->addHeaderField($fields, "Minimum Contribution Post Tax", "minimumContributionPostTax", $table);
        $this->addHeaderField($fields, "Maximum Contribution Post Tax", "maximumContributionPostTax", $table);
        $this->addHeaderField($fields, "Is Post Tax", "postTax", $table);
        $this->addHeaderField($fields, "Investments Graph", "investmentsGraph", $table);
        $this->addHeaderField($fields, "Combined Contribution Amount", "combinedContributionAmount", $table);
        $this->addHeaderField($fields, "Use Rollover Workflow", "rolloverWorkflow", $table);
        $this->addHeaderField($fields, "Use Trusted Contact", "trustedContactOn", $table);
        return $fields;
        
    }
    public function getHeaderHexColors()
    {
        $fields = array();
        $table = "PlanAppColors";
        for($i = 1; $i <=4; $i++)        
            $this->addHeaderField($fields, "Hex $i", "hex$i",$table);        
        return $fields;
        
    }
    protected function getHeaderSponsorConnectRequests() {
        $fields = array();
        $table = "sponsorConnectRequests";
        
        $this->addHeaderField($fields, "From First Name (RC)", "fromFirstName_rc", $table);
        $this->addHeaderField($fields, "From Last Name (RC)", "fromLastName_rc", $table);
        $this->addHeaderField($fields, "From Email Address (RC)", "fromEmailAddress_rc", $table);
        $this->addHeaderField($fields, "From Phone (RC)", "fromPhone_rc", $table);
        
        $this->addHeaderField($fields, "From First Name (CSM)", "fromFirstName_csm", $table);
        $this->addHeaderField($fields, "From Last Name (CSM)", "fromLastName_csm", $table);
        $this->addHeaderField($fields, "From Email Address (CSM)", "fromEmailAddress_csm", $table);
        $this->addHeaderField($fields, "From Phone (CSM)", "fromPhone_csm", $table);
        
        $this->addHeaderField($fields, "To First Name", "toFirstName", $table);
        $this->addHeaderField($fields, "To Last Name", "toLastName", $table);
        $this->addHeaderField($fields, "To Email Address", "toEmailAddress", $table);
        $this->addHeaderField($fields, "To Phone", "toPhone", $table);
        return $fields;
    }
    
    public function getHeaderMessagesFields() {
        $fields = array();
        $table = "PlansMessages";
        
        $messaging = new Messaging($this, "settings");
        $messagesPanes = $messaging->messagesPanes($this->getDoctrine()->getManager());
        
        $this->addHeaderField($fields, "Plan Id", "planid", $table, true);
        foreach ($messagesPanes as $row) {
            $this->addHeaderField($fields, $row['description'], $row['name'], $table);
            $this->addHeaderField($fields, $row['description'] . ' Active', $row['name'] . 'Active', $table);
        }
        
        return $fields;
    }
    public function getHeaderDocumentsFields($type) {
        $table = "plansDocuments";
        $fields = array();
        if ($type === "delete") {
            $this->addHeaderField($fields, "Plan Id", "planid", $table, true);
            $this->addHeaderField($fields, "Name", "name", $table, true);
        }
        else {
            $this->addHeaderField($fields, "Plan Id", "planid", $table, true);
            $this->addHeaderField($fields, "Name", "name", $table, true);
            $this->addHeaderField($fields, "URL", "url", $table);
        }
        return $fields;
    }
    public function generateDescription($orgstring)
    {
        $string = "";
        $stringArray = str_split($orgstring);
        foreach ($stringArray as $value)
        {
            if ($string == "")
            $string = strtoupper($value);
            else if ($value == strtoupper($value))
            $string = $string." ".strtoupper($value);    
            else
            $string = $string.$value;
        }
        if ($string == "Planid")
        $string = "PlanId";
        return $string;
    }
    public function addHeaderField(&$fields,$description,$value,$table,$required = false)
    {
        $field['description'] = $description;
        $field['table'] = $table;
        $field['field'] = $value;
        $field['required'] = $required;
        $fields[] = $field;
    }
    public function getHeaderFieldsFilter($table,$required = array())
    {
        $fields = $this->getHeaderFields($table); 
        foreach ($fields as  $key => $field)
        {
            if (in_array($field['field'],array("partnerid","userid","id"))) {
                unset($fields[$key]);
            }
            else if (in_array($field['field'],$required)) {
                $fields[$key]['required'] = true;
            }
        }
        return $fields;
    }
    public function getHeaderFields($table)
    {
        $fields = $this->get("doctrine.dbal.default_connection")->executeQuery("SHOW COLUMNS FROM ".$table)->fetchAll();
        $newfields = array();
        foreach($fields as  $key => $field) {
            $this->addHeaderField($newfields,$this->generateDescription($field['Field']),$field['Field'],$table);
        }
        return $newfields;        
    }
    public function getActions()
    {
        $actions =
        [
            "add" => 
            [
                "description" => "Add data",
                "value" => "add"
            ],  
//            "delete" => 
//            [
//                "description" => "Delete data",
//                "value" => "delete"
//            ], 
            "change" => 
            [
                "description" => "Change data",
                "value" => "change"
            ]
        ];
        return $actions;
    }
    public function getTypes()
    {
        $types = 
        [
            "smartplan" =>
            [
                "description" => "Smartplan",
                "value" => "smartplan"
            ],
            "smartenroll"  =>
            [
                "description" => "Smartenroll",
                "value" => "smartenroll"
            ]          
        ];
        return $types;
    }
    public function getCategories()
    {
        $categories = 
        [
            "plan"  =>
            [
                "description" => "Plan",
                "value" => "plan",
            ],
            "fund"  =>
            [
                "description" => "Fund",
                "value" => "fund"
            ],
            "portfolio"  =>
            [
                "description" => "Portfolio",
                "value" => "portfolio"
            ],
            "riskbasedfund" =>
            [
                "description" => "Risk Based Fund",
                "value" => "riskbasedfund"
            ],
            "portfoliofunds" =>
            [
                "description" => "Portfolios/Funds",
                "value" => "portfoliofunds"
            ],
            "messaging" =>
            [
                "description" => "Messaging",
                "value" => "messaging"
            ],
            "documents" =>
            [
                "description" => "Documents",
                "value" => "documents"
            ]
            
        ];
        $categories["plan"]["tables"] = array_merge($this->plansTables(), $this->optionalPlansTables());
        $categories["fund"]["tables"][0]["plan"] = "plansFunds";
        $categories["portfolio"]["tables"][0]["plan"] = "plansPortfolios";
        $categories["riskbasedfund"]["tables"][0]["plan"] = "plansRiskBasedFunds";
        $categories["portfoliofunds"]["tables"][0]["plan"] = "plansPortfolioFunds";
        $categories["messaging"]["tables"][0]["plan"] = "plansMessages";
        $categories["documents"]["tables"][0]["plan"] = "plansDocuments";
        return $categories;
    }
    private function getUploadOptions() {
        $options = [
            'enableAutoscoring' => [
                'label' => 'Enable Autoscoring',
                'categories' => ['portfolio','riskbasedfund'],
                'actions' => ['add','change'],
                'types' => ['smartplan','smartenroll'],
            ]
        ];
        return $options;   
    }
    public function getMappingAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:planuploadHeaders');   
        $planuploadHeaders = $repository->findOneBy(array("id" => $request->request->get("id")));
        $planuploadHeaders->html = $this->render("ManageManageBundle:PlanUploadNew:selectedmappingheader.html.twig",array("header" => json_decode($planuploadHeaders->header,true)))->getContent();
        $response = new  Response(json_encode($planuploadHeaders));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    public function validateAction(Request $request, $limit = 5)
    {
        $this->errors = array();
        $this->initializeFileUpload($request);
        if (!file_exists($this->filename)) {
            $this->addError("File was not uploaded");
        }
        
        if (count($this->errors) == 0) {
            $this->initializeVariables($request);
        }
        
        if (count($this->errors) == 0) {
            $this->validateNumberOfColumns();
        }
         
        if (count($this->errors) == 0) {
            $this->validateHeader();
        }
        
        if (count($this->errors) == 0) {
            $this->validateCsvContents();
        }
        
        if (count($this->errors) > 0)
        {
            if (isset($this->errors['table'])) {
                ksort($this->errors['table']);
            }
            return $this->render('ManageManageBundle:PlanUploadNew:validation.html.twig', array(
                'errors'=>$this->errors, 
                'csv'=>$this->csv,
                'headers'=>$this->headers,
                'limit'=>$limit
            ));
        }
        return new Response($this->csvLength);
    }
    function validateCsvContents()
    {
        $errors = array();
        unset($this->csv[0]);
        if ($this->planuploadHeaders->category == "plan") {
            $errors = $this->validateTablePlans();
        }
        if ($this->planuploadHeaders->category == "fund") {
            $errors = $this->validateTablePlansFunds();   
        }
        if ($this->planuploadHeaders->category == "portfolio" || $this->planuploadHeaders->category == "riskbasedfund") {
            $errors = $this->validateTablePlansPortfolios();
        }
        if ($this->planuploadHeaders->category == "portfoliofunds") {
            $errors = $this->validateTablePortfolioFunds(); 
        }
        if ($this->planuploadHeaders->category == "messaging") {
            $errors = $this->validateTablePlansMessages();
        }
        if ($this->planuploadHeaders->category == "documents") {
            $errors = $this->validateTablePlansDocuments();
        }
        return $errors;
    }
    function validateNumberOfColumns()
    {
        if ($this->headerCount != $this->headerCount2) {
            $this->addError("Header number of columns do not match");  
        }
    }
    function validateHeader()
    {
        for($i = 0; $i < $this->headerCount; $i++) {
            if ($this->csv[0][$i] != $this->headers[$i]['description']) {
                $this->addError("Header value (".$this->headers[$i]['description'].") Expected, but (".$this->csv[0][$i].") provided");
            }
        }
    }
    protected function initializeFileUpload(Request $request) {
        $session = $request->getSession();
        $this->filename = sys_get_temp_dir() . '/planupload_new_'.$session->get("clientid").'.tmp';
    }
    function initializeVariables($request)
    {
        set_time_limit(0);
        ini_set("max_execution_time", "0");
        ini_set('auto_detect_line_endings', true);
        $this->initializeFileUpload($request);
        $this->request = $request;
        $this->session = $this->request->getSession();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:planuploadHeaders');   
        $this->planuploadHeaders = $repository->findOneBy(array("id" => $this->request->request->get("mapping")));
        $this->headers = json_decode($this->planuploadHeaders->header,true);
        $this->fieldsHash = $this->getFieldsHash($this->planuploadHeaders->category, $this->planuploadHeaders->type, $this->planuploadHeaders->action);
        $this->planUploadSession = array();

        if ($this->isXmlFile()) {
            $xml = simplexml_load_file($this->filename);
            $csv = array();
            $csv[] = array_column($this->headers, 'description');
            foreach ($xml->children() as $node) {
                $row = array();
                foreach ($this->headers as $index => $val) {
                    if (count($node->{$val['description']}) > 1) {
                        $element = dom_import_simplexml($node->{$val['description']});
                        $this->addError("Duplicate tag '{$val['description']}' at line {$element->getLineNo()}");
                    }
                    $row[$index] = isset($node->{$val['description']}) ? (string) $node->{$val['description']} : "";
                }
                $csv[] = $row;
            }
            $this->csv = $csv;
        }
        else {
            $this->csv = array_map('str_getcsv', file( $this->filename ));
        }

        $this->planUploadSession['totalRecords'] = count($this->csv)-1;
        $this->headerCount = count(array_filter($this->csv[0]));
        $this->headerCount2 = count($this->headers);
        $this->planidPosition = $this->getPositionOfCsvField("planid");
        $this->findplanBy = array('userid' => $this->request->request->get("userid"),'type' => $this->planuploadHeaders->type,'deleted' => 0);
        $this->em = $this->getDoctrine()->getManager();       
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');   
        $this->account = $repository->findOneBy(array("id" => $this->request->request->get("userid"))); 
        $this->partnerid = $this->account->partnerid;
        $this->csvLength = count($this->csv)-1;
        $this->session->set("csvLength",$this->csvLength);
        $this->step = 1;
        $this->session->set("step",$this->step);
        $this->session->save();
        $currentDate = new \DateTime("now");
        $this->currentDateString = $currentDate->format('YmdHis');
        $this->linearMapping = array(
            'plansModules/socialSecurityMultiplier' => 0.01
        );
    }
    function increaseStep()
    {
        if ($this->step <= $this->csvLength)
        {
            $this->session->set("step",$this->step);
            $this->session->save();
            $this->step++;
            //usleep (1000 );
        }
    }
    function checkProgressAction(Request $request)
    {
        $session = $request->getSession();
        $params = array("step" => $session->get("step"),"csvLength" => $session->get("csvLength"), "isComplete" => $session->get("isComplete"));
        $response = new Response(json_encode($params,true));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    function stopProgressAction(Request $request) {
        $session = $request->getSession();
        $session->set("stopProgress", true);
        return new Response("success");
    }
    function validateTablePlans()
    {
        $errors = array();
        $filters = 
        [
            0 => 
            [
                "position" => $this->planidPosition,
                "description" => "planid"
            ]
        ];
        $errors = $this->checkForDuplicates($filters);
        foreach ($this->csv as $index => $row)
        {
            if (!empty(array_filter($row))) {
                $details = $this->populateDetails(array("planid"), $row);
                if ($this->planuploadHeaders->action == "add") {
                    $tmpErrors = $this->validateGetPlanErrors($row,"checkDuplicate");
                    $this->addError($tmpErrors, $index, $this->planidPosition);
                }
                else {
                    $tmpErrors = $this->validateGetPlanErrors($row,"checkExistance");
                    $this->addError($tmpErrors, $index, $this->planidPosition);
                }

                $tmpErrors = array();
                $tmpErrors = array_merge($tmpErrors,$this->validateCsv($this->headers,$this->csv,$this->request,"plans",$row,$details));
                $tmpErrors = array_merge($tmpErrors,$this->validateCsv($this->headers,$this->csv,$this->request,"plansModules",$row,$details));
                //$errors = array_merge($errors,$this->validateCsv($this->headers,$this->csv,$this->request,"plansMessaging",$row,$details));

                foreach ($tmpErrors as $currErr) {
                    $this->addError($currErr['message'], $index, $this->getPositionOfCsvField($currErr['fieldRaw']));
                }   
            }
            $this->increaseStep();
        } 
        return $errors;
    }
    function populateDetails($fields,$row)
    {
        $details = array();
        $details['line'] =  $this->step+1;
        $details['params'] = array();
        foreach ($fields as $field)
        {
            $index = $this->getPositionOfCsvField($field);
            $details['params'][$field]['description'] = $this->headers[$index]['description'];
            $details['params'][$field]['value'] = $row[$index];
        }
        return $details;
    }
    function checkForDuplicates($filters)
    {
        $errors = array();
        $hashPlanIds = array();
        foreach ($this->csv as $index => $row)
        {
            if (!empty(array_filter($row))) {
                $hash = "";
                $description = "Line ".($index+1).": Duplicate Entry in CSV, ";
                foreach ($filters as $filter)
                {
                    if ($hash != "")
                    $hash = $hash."|_|";
                    $hash = $hash.$row[$filter['position']];

                    $description = $description.$filter['description']." ".$row[$filter['position']]." ";
                }        
                if (isset($hashPlanIds[$hash]) && $hashPlanIds[$hash]) {
                    foreach ($filters as $val) {
                        $this->addError($description, $index, $val['position']);
                    }
                }
                $hashPlanIds[$hash] = true;
            }
        }      
        return $errors;
    }
    function validateTablePlansFunds()
    {
        $errors = array();
        $tickerPosition = $this->getPositionOfCsvField("ticker");
        $filters = 
        [
            0 => 
            [
                "position" => $this->planidPosition,
                "description" => "planid"
            ],
            1 => 
            [
                "position" => $tickerPosition,
                "description" => "ticker"
            ]
        ];
        $errors = $this->checkForDuplicates($filters);
        foreach ($this->csv as $index => $row)
        {
            if (!empty(array_filter($row))) {
                $tmpErrors = $this->validateGetPlanErrors($row,"checkExistance");
                $this->addError($tmpErrors, $index, $this->planidPosition);
                if (count($tmpErrors) == 0)
                {    
                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                    $plan = $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));
                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
                    $fund = $repository->findOneBy(array("planid" => $plan->id,"ticker" => $row[$tickerPosition]));
                    if ($fund != null && $this->planuploadHeaders->action == "add") {
                        $message = "Line ".($this->step+1).": Fund already exists, planid: ".$row[$this->planidPosition].", ticker: ".$row[$tickerPosition];
                        $this->addError($message, $index, $this->planidPosition);
                    }

                    if ($fund == null && $this->planuploadHeaders->action != "add") {
                        $message = "Line ".($this->step+1).": Fund doesn't exists, planid: ".$row[$this->planidPosition].", ticker: ".$row[$tickerPosition];
                        $this->addError($message, $index, $this->planidPosition);
                    }

                }
                $details = $this->populateDetails(array("planid","ticker"), $row);
                $tmpErrors = $this->validateCsv($this->headers,$this->csv,$this->request,"plansFunds",$row,$details);
                foreach ($tmpErrors as $currError) {
                    $this->addError($currError['message'], $index, $this->getPositionOfCsvField($currError['fieldRaw']));
                }   
            }
            $this->increaseStep();
        }
        return $errors;
    }
    function validateTablePlansPortfolios()
    {
        $errors = array();
        $namePosition = $this->getPositionOfCsvField("name");
        if ($this->planuploadHeaders->category == "portfolio") {
            $table = "plansPortfolios";
        }
        else {
            $table = "plansRiskBasedFunds";
        }
        $filters = 
        [
            0 => 
            [
                "position" => $this->planidPosition,
                "description" => "planid"
            ],
            1 => 
            [
                "position" => $namePosition,
                "description" => "name"
            ]
        ];
        $errors = $this->checkForDuplicates($filters);
        foreach ($this->csv as $index => $row)
        {
            if (!empty(array_filter($row))) {
                $tmpErrors = $this->validateGetPlanErrors($row,"checkExistance");
                $this->addError($tmpErrors, $index, $this->planidPosition);
                $details = $this->populateDetails(array("planid","name"), $row);

                $tmpErrors = $this->validateCsv($this->headers,$this->csv,$this->request,"plansPortfolios",$row,$details);
                foreach ($tmpErrors as $currError) {
                    $this->addError($currError['message'], $index, $this->getPositionOfCsvField($currError['fieldRaw']));
                }
            }
            $this->increaseStep();
        }
        return $errors;
    }
    function validateTablePortfolioFunds()
    {
        $errors = array();
        $percentPerPortfolio = array();
        $portfolioToLine = array();
        $tickerPosition = $this->getPositionOfCsvField("ticker");
        $portfolioNamePosition = $this->getPositionOfCsvField("name");
        $percentPosition = $this->getPositionOfCsvField("percent");
        $filters = 
        [
            0 => 
            [
                "position" => $this->planidPosition,
                "description" => "planid"
            ],
            1 => 
            [
                "position" => $portfolioNamePosition,
                "description" => "name"
            ],
            2 => 
            [
                "position" => $tickerPosition,
                "description" => "ticker"
            ]            
        ];
        $errors = $this->checkForDuplicates($filters);
        foreach ($this->csv as $index => $row)
        {
            if (!empty(array_filter($row))) {
                $tmpErrors = $this->validateGetPlanErrors($row,"checkExistance");
                $this->addError($tmpErrors, $index, $this->planidPosition);
                if (count($tmpErrors) == 0)
                {               
                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                    $plan = $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));
                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
                    $fund = $repository->findOneBy(array("planid" => $plan->id,"ticker" => $row[$tickerPosition] ));  
                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansPortfolios');
                    $portfolio = $repository->findOneBy(array("planid" => $plan->id,"name" => $row[$portfolioNamePosition] ));  
                    if ($fund == null) {
                        $message = 'Fund not found, planid: '.$row[$this->planidPosition].", ticker: ".$row[$tickerPosition];
                        $this->addError($message, $index, $tickerPosition);
                    }
                    if ($portfolio == null) {
                        $message = 'Portfolio not found, planid: '.$row[$this->planidPosition].", name: ".$row[$portfolioNamePosition]; 
                        $this->addError($message, $index, $portfolioNamePosition);
                    }
                    if ($portfolio != null)
                    {
                        if (!isset($percentPerPortfolio[$portfolio->id])) {
                            $percentPerPortfolio[$portfolio->id] = 0;
                        }
                        $percentPerPortfolio[$portfolio->id] += $row[$percentPosition];
                        $portfolioToLine[$portfolio->id][] = $index;
                    }
                }
                $details = $this->populateDetails(array("planid","ticker","name"), $row);

                $tmpErrors = $this->validateCsv($this->headers,$this->csv,$this->request,"plansPortfolioFunds",$row,$details);
                foreach ($tmpErrors as $currError) {
                    $this->addError($currError['message'], $index, $this->getPositionOfCsvField($currError['fieldRaw']));
                }
            }
            $this->increaseStep();
        }
        
        foreach ($percentPerPortfolio as $portfolioId => $total) {
            if ($total != 100) {
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansPortfolios');
                $portfolio = $repository->find($portfolioId);
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan = $repository->find($portfolio->planid);
                foreach ($portfolioToLine[$portfolioId] as $line) {
                    $message = "The total combined percent for portfolio \"{$portfolio->name}\" for plan \"{$plan->planid}\" does not equal 100";
                    $this->addError($message, $line, $percentPosition);
                }
            }
        }
        return $errors;
    }
    function validateTablePlansMessages()
    {   
        $errors = array();
        $filters = 
        [
            0 => 
            [
                "position" => $this->planidPosition,
                "description" => "planid"
            ]
        ];
        $errors = $this->checkForDuplicates($filters);
        foreach ($this->csv as $index => $row)
        {
            if (!empty(array_filter($row))) {

                $tmpErrors = $this->validateGetPlanErrors($row,"checkExistance");
                $this->addError($tmpErrors, $index, $this->planidPosition);

                foreach ($this->headers as $pos => $header) {
                    if (substr_compare($header['field'], 'Active', -6) === 0) {
                        $value = $this->getTableColumnMappingValue("PlansMessages", $header['field'], $row[$pos]);
                        if (!in_array($value, array('0','1'), true)) {
                            $this->addError("{$header['description']} has to be one of the following values: '0','1', value \"".$row[$pos]."\" found", $index, $pos);
                        }
                    }
                } 
            }
            $this->increaseStep();
        } 
        return $errors;
    }
    function validateTablePlansDocuments()
    {
        $errors = array();
        $namePosition = $this->getPositionOfCsvField("name");
        $filters = 
        [
            0 => 
            [
                "position" => $this->planidPosition,
                "description" => "planid"
            ],
            1 => 
            [
                "position" => $namePosition,
                "description" => "name"
            ]
        ];
        $errors = $this->checkForDuplicates($filters);
        foreach ($this->csv as $index => $row)
        {
            if (!empty(array_filter($row))) {
                $tmpErrors = $this->validateGetPlanErrors($row,"checkExistance");
                $this->addError($tmpErrors, $index, $this->planidPosition);
                $details = $this->populateDetails(array("planid","name"), $row);

                $tmpErrors = $this->validateCsv($this->headers, $this->csv, $this->request, "plansDocuments", $row, $details);
                foreach ($tmpErrors as $currError) {
                    $this->addError($currError['message'], $index, $this->getPositionOfCsvField($currError['fieldRaw']));
                }
            }
            $this->increaseStep();
        }
        return $errors;
    }
    function getPositionOfCsvField($field)
    {
        $position = -1;
        $i = 0;
        foreach ($this->headers as $row)
        {
            if ($row['field'] == $field) 
            {
                $position = $i;
                break;
            }
            $i++;
        }    
        return $position;
    }
    function getPositionOfCsvTable($table) {
        $pos = false;
        $i = 0;
        foreach ($this->headers as $val) {
            if ($val['table'] == 'fundsGroupsValues') {
                $pos = $i;
                break;
            }
            $i++;
        }
        return $pos;
    }
    function validateGetPlanErrors($row,$mode)
    {
        $errors = array();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));
        if ($plan != null  && $mode == "checkDuplicate") {
            $message = 'Duplicate Entry in database, '.$this->headers[$this->planidPosition]['description'].': '.$row[$this->planidPosition]; 
        }  
        if ($plan == null  && $mode == "checkExistance") {
            $message = 'Plan does not exist, '.$this->headers[$this->planidPosition]['description'].': '.$row[$this->planidPosition];
        }
        if (!empty($message)) {
            $errors[] = "Line ".($this->step+1).": ".$message;
        }
        return $errors;
    } 
    public function validateCsv($headers,$csv,$request,$table,$row,$details)
    {     
        $orgTable = $table;
        $table =  "classes\classBundle\Entity\\".$table;
        $object = new $table;
        $rowCount = count($row);           
        $class_vars = get_class_vars(get_class($object));
        foreach ($class_vars as $key => $value) {
            $object->$key = "*DO_NOT_VALIDATE*";
        }
        for($i = 0; $i< $rowCount; $i++) 
        {
            if ($headers[$i]['table'] == $orgTable)
            {
                $column = trim($headers[$i]['field']);
                if (isset($object->$column)) {
                    $object->$column = $this->getTableColumnMappingValue($orgTable, $column, $row[$i]);
                }
            }
        }     
        $object->planid = $row[$this->getPositionOfCsvField("planid")];
        $headersHash = array();
        foreach ($this->headers as $headerRow) {
            $headersHash[$headerRow['field']] = $headerRow['description'];
        } 
        return $this->get("EntityValidator")->validate($object,$details,$headersHash);
    }
    protected function createSnapshots() {
        switch ($this->planuploadHeaders->category) {
            case "plan": 
                foreach ($this->plansTables() as $table) {
                    $this->createSnapshot($table['plan']);
                }
                foreach ($this->optionalPlansTables() as $table) {
                    $this->createSnapshot($table['plan']);
                }
                break;
            case "fund": 
                $this->createSnapshot("plansFunds"); 
                break;
            case "portfolio": 
                $this->createSnapshot("plansPortfolios"); 
                break;
            case "riskbasedfund": 
                $this->createSnapshot("plansRiskBasedFunds"); 
                break;
            case "portfoliofunds": 
                $this->createSnapshot("plansPortfolioFunds"); 
                break;
            case "messaging": 
                $this->createSnapshot("plansMessages"); 
                break;
            case "documents":
                $this->createSnapshot("plansDocuments");
                break;
        }
    }
    public function uploadAction(Request $request)
    {
        $this->initializeVariables($request);
        $this->createSnapshots();
        $this->em->getConnection()->beginTransaction();
        $this->session->set("stopProgress", false);
        try
        {
            unset($this->csv[0]);
            if ($this->planuploadHeaders->category == "plan") {
                $this->uploadTablePlans();
            }
            if ($this->planuploadHeaders->category == "fund") {
                $this->uploadTableFunds();
            }
            if ($this->planuploadHeaders->category == "portfolio" || $this->planuploadHeaders->category == "riskbasedfund") {
                $this->uploadTablePortfolios();
            }
            if ($this->planuploadHeaders->category == "portfoliofunds") {
                $this->uploadTablePortfolioFunds();
            }
            if ($this->planuploadHeaders->category == "messaging") { 
                $this->uploadTableMessages();
            }
            if ($this->planuploadHeaders->category == "documents") {
                $this->uploadTableDocuments();
            }
            $this->em->getConnection()->commit();

        }
        catch (\Exception $e) 
        {
            $this->em->getConnection()->rollBack();
            throw $e;
        }     
        return new Response("success");
    }
    public function plansTables()
    {
        return 
        [
            0 =>
            [
                "default" => "defaultPlan",
                "plan" => "plans"
            ],
            1 =>
            [
                "default" => "defaultModules",
                "plan" => "plansModules"
            ],
            2 =>
            [
                "default" => "defaultLibraryHotTopics",
                "plan" => "plansLibraryHotTopics"                
            ],
            3 =>
            [
                "default" => "DefaultInvestmentsConfiguration",
                "plan" => "PlansInvestmentsConfiguration"
            ],
            4 =>
            [
                "default" => "DefaultInvestmentsConfigurationDefaultFundsGroupValuesColors",
                "plan" => "PlansInvestmentsConfigurationDefaultFundsGroupValuesColors"
            ],
            5 =>
            [
                "default" => "DefaultInvestmentsConfigurationColors",
                "plan" => "PlansInvestmentsConfigurationColors"
            ],
            6 => 
            [
                "default" => "AccountAppColors",
                "plan" => "PlanAppColors",
            ]
        ];
    }
    protected function optionalPlansTables() {
        return 
        [
            0 =>
            [
                "plan" => "sponsorConnectRequests"
            ]
        ];
    }
    public function uploadTablePlans()
    {
        $this->tables = $this->plansTables();
        if ($this->planuploadHeaders->action == "add") {
            $this->uploadTablePlansAdd();
        }
        if ($this->planuploadHeaders->action == "change") {
            $this->uploadTablePlansChange();
        }
        if ($this->planuploadHeaders->action == "delete") {
            $this->uploadTablePlansDelete();
        }
    }
    public function updateDateTimeFields(&$object,$fields)
    {
        foreach ($fields as $field)
        {
            if (!empty($object->$field) && !($object->$field instanceof \DateTime))
            {
                $object->$field = new \DateTime($object->$field);
            }
        }
    }
    public function plansDateFields()
    {
        $plans = new plans();
        $plans->validationObject();
        return $plans->validationDateTime;
    }
    public function uploadTablePlansAdd()
    {
        $userid = $this->request->get("userid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:DefaultMessages');
        $defaultMessages = $repository->findBy(array("userid" => $userid));
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultDocuments');
        $defaultDocuments = $repository->findBy(array("userid" => $userid));
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:DefaultLibraryUsers');
        $defaultLibraryUsers = $repository->findBy(array("userid" => $userid));
        
        $defaults = array();
        $class_vars = array();
        foreach ($this->tables as $table) {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:'.$table['default']);
            $data = $repository->findOneBy(array("userid" => $userid));
            if ($data) {
                $defaults[$table['default']] = $data;
                $class_vars[$table['plan']] = array_keys(get_class_vars("classes\classBundle\Entity\\" . $table['plan']));
            }
        }

        foreach ($this->csv as $row)
        {
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $planAdded = array();
                foreach ($this->tables as $table)
                {
                    if (array_key_exists($table['default'], $defaults)) {
                        $entityPath =  "classes\classBundle\Entity\\".$table['plan'];
                        $planAdded[$table['plan']] = new $entityPath;
                        $this->copyDefaultData($planAdded[$table['plan']],$defaults[$table['default']], $class_vars[$table['plan']]);
                    }
                }
                $this->copyRowValues($planAdded,$row);
                $this->updateDateTimeFields($planAdded['plans'],$this->plansDateFields());
                $planAdded['plans']->type = $this->planuploadHeaders->type;
                $planAdded['plans']->smartEnrollAllowed = $this->planuploadHeaders->type === 'smartenroll' ? 1 : 0;
                $planAdded['plans']->modifiedDate =  new \dateTime('now');
                $planAdded['plans']->createdDate =  new \dateTime('now');
                $planAdded['plans']->partnerid = $this->partnerid;
                $this->copyPathData($planAdded['plans']);
                $this->em->persist($planAdded['plans']);
                $this->em->flush();
                if (isset($planAdded['sponsorConnectRequests'])) {
                    $this->reassignSponsorConnectRequestsFields($planAdded['sponsorConnectRequests']);
                    $planAdded['launchUsers'] = new launchUsers();
                    $planAdded['launchUsers']->firstName = $planAdded['sponsorConnectRequests']->toFirstName;
                    $planAdded['launchUsers']->lastName = $planAdded['sponsorConnectRequests']->toLastName;
                    $planAdded['launchUsers']->telephone = $planAdded['sponsorConnectRequests']->toPhone;
                    $planAdded['launchUsers']->email = $planAdded['sponsorConnectRequests']->toEmailAddress;
                    $planAdded['launchUsers']->partnerid = $this->account->id;
                    $planAdded['launchUsers']->urlPass = launchUsers::getRandSalt();
                    $planAdded['launchUsers']->uploadDate = null;
                }
                $investmentGraphEnabled = $planAdded['plansModules']->investmentsGraph;
                $this->copyTableDataException($userid, $planAdded['plans']->id, "Default", array('userid' => $userid), $investmentGraphEnabled);
                       
                foreach ($planAdded as $key => $value)
                {
                    if ($key === "plans") {
                        continue;
                    }
                    $planAdded[$key]->planid = $planAdded['plans']->id;
                    if (property_exists($planAdded[$key], "userid")) {
                        $planAdded[$key]->userid = $userid;
                    }
                    $this->em->persist($planAdded[$key]);
                }
                
                $this->copyExistingData("PlansMessages", $defaultMessages, $planAdded['plans']->id);
                $this->copyExistingData("plansDocuments", $defaultDocuments, $planAdded['plans']->id);
                $this->copyExistingData("plansLibraryUsers", $defaultLibraryUsers, $planAdded['plans']->id);                
                unset($planAdded);
            }
            $this->increaseStep();
        }


        $this->em->flush();
    }
    private function copyPathData(&$plan,$planOriginal = null)
    {
        $pathTypes = $this->get("PathsService")->getPathIdSections();
        foreach ($pathTypes as $type)
        {
            if (!empty($plan->{$type}))
            {
                $path = $this->get("PathsService")->getPath(["path" => $plan->{$type}]);
                if (!empty($path))
                    $plan->{$type} = $path->id;
            }
            else if ($planOriginal != null)           
                $plan->{$type} = $planOriginal->{$type};           
        }
    }
    private function copyExistingData($table, $dataArray, $planid) {
        if (in_array($table, ["PlansMessages", "plansDocuments", "plansLibraryUsers"])) {
            $classPath = "classes\classBundle\Entity\\$table";
            $properties = array_keys(get_class_vars($classPath));
            foreach ($dataArray as $existingEntity) {
                $newEntity = new $classPath;
                foreach ($properties as $value) {
                    if ( isset($existingEntity->$value)) {
                        $newEntity->$value = $existingEntity->$value;
                    }
                }
                $newEntity->planid = $planid;
                $this->em->persist($newEntity);
            }
            $this->em->flush();
        }
        
    }
    private function populateADPMap($tablename) {
        $defaultInvestmentsScoreRepo = $this->getDoctrine()->getRepository('classesclassBundle:' . $tablename . 'InvestmentsScore');
        $result = $defaultInvestmentsScoreRepo->findAll();
        $this->scoreMap = array();
        foreach ($result as $row) {
            $this->scoreMap[$row->id] = $row->min;
        }

        $yearsToRetirementRepo = $this->getDoctrine()->getRepository('classesclassBundle:' . $tablename . 'InvestmentsYearsToRetirement');
        $result = $yearsToRetirementRepo->findAll();
        $this->yearsToRetirementMap = array();
        foreach ($result as $row) {
            $this->yearsToRetirementMap[$row->id] = $row->min;
        }

    }

    private function copyTableDataException($userid, $planid, $tablename, $findby, $investmentGraphEnabled) {
        if ($this->scoreMap == null && $this->yearsToRetirementMap == null) {
            $this->populateADPMap($tablename);
        }
        $em = $this->getDoctrine()->getManager();
        $minToId = array();

        $defaultInvestmentsScoreRepo = $this->getDoctrine()->getRepository('classesclassBundle:' . $tablename . 'InvestmentsScore');
        $defaultScore = $defaultInvestmentsScoreRepo->findBy($findby);
        foreach ($defaultScore as $ds) {
            $insert = new PlansInvestmentsScore();
            $insert->planid = $planid;
            $insert->userid = $userid;
            $insert->min = $ds->min;
            $insert->label = $ds->label;
            $em->persist($insert);
            $em->flush();

            $minToId[$ds->min] = $insert->id;
        }

        $repo = $this->getDoctrine()->getRepository('classesclassBundle:' . $tablename . 'InvestmentsConfigurationFundsGroupValuesColors');
        $defaultConfig = $repo->findBy($findby);
        foreach ($defaultConfig as $dc) {
            $insert = new PlansInvestmentsConfigurationFundsGroupValuesColors();
            $insert->planid = $planid;
            $insert->userid = $userid;
            $insert->min = $dc->min;
            $insert->color = $dc->color;
            $insert->hoverColor = $dc->hoverColor;
            $insert->fundGroupValueId = $dc->fundGroupValueId;
            $insert->isCustom = $dc->isCustom;

            $defaultMin = $this->scoreMap[$dc->scoreMinId];

            $insert->scoreMinId = $minToId[$defaultMin];
            $em->persist($insert);

        }
        $em->flush();

        $yearsToRetirementRepo = $this->getDoctrine()->getRepository('classesclassBundle:' . $tablename . 'InvestmentsYearsToRetirement');
        $defaultYears = $yearsToRetirementRepo->findBy($findby);
        $yearsToId = array();
        foreach ($defaultYears as $dy) {
            $insert = new PlansInvestmentsYearsToRetirement();
            $insert->planid = $planid;
            $insert->userid = $userid;
            $insert->min = $dy->min;
            $em->persist($insert);
            $em->flush();

            $yearsToId[$dy->min] = $insert->id;
        }

        $repo = $this->getDoctrine()->getRepository('classesclassBundle:' . $tablename . 'InvestmentsLinkYearsToRetirementAndScore');
        $defaultLink = $repo->findBy($findby);
        foreach ($defaultLink as $dl) {
            $insert = new PlansInvestmentsLinkYearsToRetirementAndScore();
            $insert->planid = $planid;
            $insert->userid = $userid;
            $insert->type = $dl->type;

            $defaultMin = $this->scoreMap[$dl->InvestmentsScoreId];
            $insert->InvestmentsScoreId = $minToId[$defaultMin];

            $defaultYear = $this->yearsToRetirementMap[$dl->InvestmentsYearsToRetirementId];
            $insert->InvestmentsYearsToRetirementId = $yearsToId[$defaultYear];

            $em->persist($insert);
        }

        if ($investmentGraphEnabled) {
            $repo = $this->getDoctrine()->getRepository('classesclassBundle:defaultPortfolios');
            $defaultPortfolios = $repo->findBy($findby);

            foreach ($defaultPortfolios as $dp) {
                $insert = new plansPortfolios();
                $properties = array_keys(get_class_vars("classes\\classBundle\\Entity\\plansPortfolios"));
                $this->copyDefaultData($insert,$dp, $properties);
                $insert->planid = $planid;
                $em->persist($insert);
            }
        }


        $em->flush();

    }
    public function uploadTablePlansChange()
    {
        unset($this->tables[0]);
        $launchUsersRepository = $this->getDoctrine()->getRepository(launchUsers::class);
        foreach ($this->csv as $row)
        {
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $plans = array();
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plans["plans"] = $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));
                foreach ($this->tables as $table)
                {
                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:'.$table['plan']);
                    $plans[$table['plan']] = $repository->findOneBy(array("planid" => $plans["plans"]->id));
                }
                foreach ($this->optionalPlansTables() as $table) 
                {
                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:'.$table['plan']);
                    $plans[$table['plan']] = $repository->findOneBy(array("planid" => $plans["plans"]->id));
                }
                $plansOriginal =  unserialize(serialize($plans));
                $this->copyRowValues($plans,$row);
                $this->updateDateTimeFields($plans['plans'],$this->plansDateFields());
                $this->copyPathData($plans['plans'],$plansOriginal['plans']);
                $plans['plans']->modifiedDate =  new \dateTime('now');
                if (isset($plans['sponsorConnectRequests'])) {
                    $this->reassignSponsorConnectRequestsFields($plans['sponsorConnectRequests']);
                    $plans['launchUsers'] = $launchUsersRepository->findOneBy(['planid' => $plans["plans"]->id]);
                    $plans['launchUsers']->firstName = $plans['sponsorConnectRequests']->toFirstName;
                    $plans['launchUsers']->lastName = $plans['sponsorConnectRequests']->toLastName;
                    $plans['launchUsers']->telephone = $plans['sponsorConnectRequests']->toPhone;
                    $plans['launchUsers']->email = $plans['sponsorConnectRequests']->toEmailAddress;
                }
                unset($plans);
            }
            $this->increaseStep();
        }
        $this->em->flush();
    }
    public function uploadTablePlansDelete()
    {
        foreach ($this->csv as $row)
        {
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan= $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));
                $findDeletedBy =  $this->findplanBy;
                unset($findDeletedBy['deleted']);
                $deletedPlan = $repository->findOneBy(array_merge($findDeletedBy,array('planid' => $row[$this->planidPosition]),array("deleted" => "desc")));
                $plan->deleted = $deletedPlan->delete + 1;
                $this->em->flush();  
            }
            $this->increaseStep();
        }
    }
    function copyRowValues(&$object,$row)
    {
        foreach ($this->headers as $pos => $headerRow)
        {
            if (empty($headerRow['field']) || !isset($row[$pos]) || !isset($this->fieldsHash[$headerRow['table']][$headerRow['field']])) {
                continue;
            }
            
            if (is_array($object)) {
                if (!isset($object[$headerRow['table']])) {
                    $entityPath = "classes\classBundle\Entity\\" . $headerRow['table'];
                    $object[$headerRow['table']] = new $entityPath;
                }
                $entity =& $object[$headerRow['table']];
            }
            else {
                $entity =& $object;
            }
            $value = $this->getTableColumnMappingValue($headerRow['table'], $headerRow['field'], $row[$pos]);
            $value = $this->getLinearMappingValue($headerRow['table'], $headerRow['field'], $value);
            $entity->{$headerRow['field']} = $value;
        }
    }
    protected function getLinearMappingValue($table, $column, $value) {
        if (is_numeric($value) && isset($this->linearMapping["$table/$column"])) {
            $value = $this->linearMapping["$table/$column"] * $value;
        }
        return $value;
    }
    function copyDefaultData(&$plan,$default,$properties)
    {
        foreach ($properties as $value) {
            if ( isset($default->$value)) {
                $plan->$value = $default->$value;
            }
        }
    }
    public function uploadTableFunds()
    {
        if ($this->planuploadHeaders->action == "add") {
            $this->uploadTableFundsAdd();
        }
        if ($this->planuploadHeaders->action == "change") {
            $this->uploadTableFundsChange();   
        }
        if ($this->planuploadHeaders->action == "delete"){ 
            $this->uploadTableFundsDelete(); 
        }
    }
    
    protected function replaceFundsGroupsValues(&$plansFunds, $row) {
        $pos = $this->getPositionOfCsvTable('fundsGroupsValues');
        $insertedFundsGroupsValueId = false;
        if ($pos !== false) {

            $repository = $this->getDoctrine()->getRepository('classesclassBundle:FundsGroupsValues');
            $fundsGroupsValues = $repository->findOneBy(array('value'=>$row[$pos], 'groupid'=>$this->headers[$pos]['field']));
            if ($fundsGroupsValues == null) {
                $fundsGroupsValues = new FundsGroupsValues;
                $this->em->persist($fundsGroupsValues);
            }
            $fundsGroupsValues->value = $row[$pos];
            $fundsGroupsValues->groupid = $this->headers[$pos]['field'];
            $this->em->flush();
            $insertedFundsGroupsValueId = $fundsGroupsValues->id;
        }
        if (!empty($insertedFundsGroupsValueId)) {
            $plansFunds->groupValueId = $insertedFundsGroupsValueId;
        }
    }
    
    public function uploadTableFundsAdd()
    {
        foreach ($this->csv as $row)
        {        
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan= $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));
                $plansFunds = new plansFunds();
                $this->copyRowValues($plansFunds,$row);
                $plansFunds->planid = $plan->id;
                $plansFunds->userid = $this->request->request->get("userid");
                $this->replaceFundsGroupsValues($plansFunds, $row);
                $this->em->persist($plansFunds);
                $this->em->flush();
            }
            $this->increaseStep();
        }
    }
    public function uploadTableFundsChange()
    {
        foreach ($this->csv as $row)
        {
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan= $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));     
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
                $plansFunds= $repository->findOneBy(array("planid" => $plan->id,"ticker" => $row[$this->getPositionOfCsvField("ticker")]));   
                $this->copyRowValues($plansFunds,$row);
                $plansFunds->planid = $plan->id;
                $this->replaceFundsGroupsValues($plansFunds, $row);
                $this->em->flush();
            }
            $this->increaseStep();
        }
    }
    public function uploadTableFundsDelete()
    {
        foreach ($this->csv as $row)
        { 
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan= $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));     
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
                $plansFunds= $repository->findOneBy(array("planid" => $plan->id,"ticker" => $row[$this->getPositionOfCsvField("ticker")]));   
                $this->em->remove($plansFunds);
                $this->em->flush();
            }
            $this->increaseStep();
        }
    }    
    public function uploadTablePortfolios()
    {
        if ($this->planuploadHeaders->category == "portfolio") {
            $this->table = "plansPortfolios";
        }
        else {
            $this->table = "plansRiskBasedFunds";
        }
        if (in_array($this->planuploadHeaders->action, ["add", "change"])) {
            $this->uploadTablePortfoliosReplace();
        }
        if ($this->planuploadHeaders->action == "delete") {
            $this->uploadTablePortfoliosDelete();
        }    
    }    
    public function uploadTablePortfoliosAdd()
    {
        foreach ($this->csv as $row)
        { 
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan= $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition]))); 
                $class = "classes\classBundle\Entity\\".$this->table;
                $portfolio = new $class;
                $this->copyRowValues($portfolio,$row);
                $portfolio->planid = $plan->id;
                $portfolio->userid = $this->request->request->get("userid");
                $this->em->persist($portfolio);
                $this->em->flush();
            }
            $this->increaseStep();
        }
    }
    public function uploadTablePortfoliosChange()
    {
        foreach ($this->csv as $row)
        { 
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan= $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));    
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:'.$this->table);
                $portfolio= $repository->findOneBy(array("planid" => $plan->id,"name" => $row[$this->getPositionOfCsvField("name")])); 
                $this->copyRowValues($portfolio,$row);
                $portfolio->planid = $plan->id;
                $this->em->flush();
            }
            $this->increaseStep();
        }        
    }   
    public function uploadTablePortfoliosReplace() {
        $portfoliosByPlanId = array();
        $riskSections = ["riskProfileConservative","riskProfileModeratelyConservative","riskProfileModerate","riskProfileModeratelyAggressive","riskProfileAggressive"];
        $enableAutoscoring = $this->request->request->get("enableAutoscoring");
        $min = $this->account->investmentsMinScore !== 0 ? $this->account->investmentsMinScore : 6;
        $max = $this->account->investmentsMaxScore !== 0 ? $this->account->investmentsMaxScore : 54;
        $class = "classes\classBundle\Entity\\".$this->table;
        foreach ($this->csv as $row)
        { 
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan = $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));
                $plan->investmentsAutoScoring = $enableAutoscoring ? 1 : $plan->investmentsAutoScoring;
                $plan->riskType = ($this->table == 'plansPortfolios' ? 'RBP' : 'RBF');
                $profilePos = $this->getPositionOfCsvField("profile");
                $profile = isset($row[$profilePos]) ? $this->getTableColumnMappingValue('plansPortfolios', 'profile', $row[$profilePos]) : "ALL";
                $portfoliosByPlanId[$plan->id][$profile][] = $row;
            }
            $this->increaseStep();
        }
        $portfoliosRepository = $this->getDoctrine()->getRepository('classesclassBundle:' . $this->table);
        $portfolioFundsRepository = $this->getDoctrine()->getRepository('classesclassBundle:plansPortfolioFunds');
        $plansModulesRepository = $this->getDoctrine()->getRepository('classesclassBundle:plansModules');
        foreach ($portfoliosByPlanId as $planid => $profiles) {
            // Delete all existing portfolios by planid
            $existingPortfolios = $portfoliosRepository->findBy(['planid' => $planid]);
            array_walk($existingPortfolios, array($this, 'deleteEntity'));
            
            // Delete all existing portfoliofunds by planid
            $existingPortfolioFunds = $portfolioFundsRepository->findBy(['planid'=>$planid]);
            array_walk($existingPortfolioFunds, array($this, 'deleteEntity'));
            
            $plansModules = $plansModulesRepository->findOneBy(['planid' => $planid]);
            $sectionCount = count(array_diff_key($profiles, array_flip(["ALL"])));
            $rangePerSection = ($max - $min) / $sectionCount;
            $currentmin = $min;
            $currentmax = 0;
            $sectionMax = $min;
            
            foreach ($profiles as $profile => $portfolios) {
                if (isset($riskSections[$profile])) {
                    // If one of the risk profiles
                    $plansModules->{$riskSections[$profile]} = 1;
                    $sectionMax = $sectionMax + $rangePerSection;
                    $i = 0;
                    foreach ($portfolios as $row) {
                        $currentmax = $currentmin + floor($rangePerSection/count($portfolios));
                        if ($i + 1 == count($portfolios)) {
                            $currentmax = floor($sectionMax);
                        }
                        $portfolio = new $class;
                        $this->copyRowValues($portfolio,$row);
                        if ($enableAutoscoring) {
                            $portfolio->minscore = $currentmin;
                            $portfolio->maxscore = $currentmax;
                        }
                        $portfolio->planid = $planid;
                        $portfolio->userid = $this->request->request->get("userid");
                        $this->em->persist($portfolio);
                        $currentmin = $currentmax + 1;
                        $i++;
                    }
                }
                else {
                    // If not in a risk profile (i.e. "ALL")
                    foreach ($portfolios as $row) {
                        $portfolio = new $class;
                        $this->copyRowValues($portfolio,$row);
                        if ($enableAutoscoring) {
                            $portfolio->minscore = 0;
                            $portfolio->maxscore = 0;
                        }
                        $portfolio->planid = $planid;
                        $portfolio->userid = $this->request->request->get("userid");
                        $this->em->persist($portfolio);
                    }
                }
                
            }
        }
        $this->em->flush();
    }
    public function uploadTablePortfoliosDelete()
    {
        foreach ($this->csv as $row)
        { 
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan= $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));    
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:'.$this->table);
                $portfolio= $repository->findOneBy(array("planid" => $plan->id,"name" => $row[$this->getPositionOfCsvField("name")]));
                $this->em->remove($portfolio); 
                $this->em->flush();    
            }
            $this->increaseStep();
        }
    }    
    public function uploadTablePortfolioFunds()
    {
        if ($this->planuploadHeaders->action == "add") {
            $this->uploadTablePortfolioFundsAdd();
        }
        if ($this->planuploadHeaders->action == "change") {
            $this->uploadTablePortfolioFundsAdd();
        }
        if ($this->planuploadHeaders->action == "delete") {
            $this->uploadTablePortfolioFundsDelete();
        }
    } 
    public function uploadTablePortfolioFundsAdd()
    {
        $portfoliosToRemoveBy = array();
        $orderid = 0;
        foreach ($this->csv as $row)
        { 
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $data = $this->uploadTablePortfolioFundsGetRowData($row);
                $plansPortfolioFunds = new plansPortfolioFunds();
                $this->copyRowValues($plansPortfolioFunds,$row);
                $plansPortfolioFunds->portfolioid = $data['plansPortfolios']->id;
                $plansPortfolioFunds->fundid = $data['plansFunds']->id;
                $plansPortfolioFunds->planid = $data['plans']->id;
                $plansPortfolioFunds->userid = $this->request->request->get("userid");
                $plansPortfolioFunds->orderid = $orderid++;
                $this->em->persist($plansPortfolioFunds);
                $portfoliosToRemoveBy[$data['plansPortfolios']->id] = $data['plansPortfolios']->id;
            }
            $this->increaseStep();
        }
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansPortfolioFunds');
        foreach ($portfoliosToRemoveBy as $portfolioId) {
            $portfoliosFundsToRemove = $repository->findBy(array('portfolioid' => $portfolioId));
            array_walk($portfoliosFundsToRemove, array($this, 'deleteEntity'));
        }
        $this->em->flush();
    }
    public function uploadTablePortfolioFundsChange()
    {
        foreach ($this->csv as $row)
        { 
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $data = $this->uploadTablePortfolioFundsGetRowData($row);
                $this->copyRowValues($data['plansPortfolioFunds'],$row); 
                $data['plansPortfolioFunds']->planid = $data['plans']->id;
                $data['plansPortfolioFunds']->userid = $this->request->request->get("userid");
                $this->em->flush();  
            }
            $this->increaseStep();
        }
    }
    public function uploadTablePortfolioFundsDelete()
    {
        foreach ($this->csv as $row)
        { 
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $data = $this->uploadTablePortfolioFundsGetRowData($row);
                $this->em->remove($data['plansPortfolioFunds']);
                $this->em->flush();
            }
            $this->increaseStep();
        }
    }
    public function uploadTablePortfolioFundsGetRowData($row)
    {
        $data = array();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $data['plans']= $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));    
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansPortfolios');
        $data['plansPortfolios']= $repository->findOneBy(array("planid" => $data['plans']->id,"name" => $row[$this->getPositionOfCsvField("name")]));  
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        $data['plansFunds']= $repository->findOneBy(array("planid" => $data['plans']->id,"ticker" => $row[$this->getPositionOfCsvField("ticker")])); 
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansPortfolioFunds');
        $data['plansPortfolioFunds']= $repository->findOneBy(array("planid" => $data['plans']->id,"portfolioid" => $data['plansPortfolios']->id,"fundid" => $data['plansFunds']->id));
        return $data;
    }
    public function uploadTableMessages() {
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanes');
        $messagesPanes = $repository->findAll();
        $messageNameToId = array();
        foreach ($messagesPanes as $row) {
            $messageNameToId[$row->name] = $row->id;
        }
        
        $plansRepository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plansMessagesRepository = $this->getDoctrine()->getRepository('classesclassBundle:PlansMessages');
        $toUpdate = array();
        foreach ($this->csv as $row)
        {
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                
                $plan = $plansRepository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));     
                foreach ($this->headers as $pos => $header) {
                    if (substr($header['field'], -6) === 'Active') {
                        $messageName = substr($header['field'], 0, -6);
                        $column = 'isActive';
                    }
                    else {
                        $messageName = $header['field'];
                        $column = 'message';
                    }
                    
                    if (!in_array($messageName, array_keys($messageNameToId))) {
                        continue;
                    }

                    $messagesPanesId = $messageNameToId[$messageName];
                    if (!isset($toUpdate[$plan->id.'_'.$messagesPanesId])) {
                        $toUpdate[$plan->id.'_'.$messagesPanesId] = $plansMessagesRepository->findOneBy(array("planid" => $plan->id,"messagesPanesId" => $messagesPanesId));
                    }
                    if (isset($toUpdate[$plan->id.'_'.$messagesPanesId])) {
                        $toUpdate[$plan->id.'_'.$messagesPanesId]->planid = $plan->id;
                        $toUpdate[$plan->id.'_'.$messagesPanesId]->$column = $this->getTableColumnMappingValue("PlansMessages", $header['field'], $row[$pos]);
                    }
                }
            }
            $this->increaseStep();
        } 
        $this->em->flush();
    }
    public function uploadTableDocuments()
    {
        if ($this->planuploadHeaders->action == "add") {
            $this->uploadTableDocumentsAdd();
        }
        if ($this->planuploadHeaders->action == "change") {
            $this->uploadTableDocumentsChange();   
        }
        if ($this->planuploadHeaders->action == "delete"){ 
            $this->uploadTableDocumentsDelete(); 
        }
    }
    
    public function uploadTableDocumentsAdd()
    {
        foreach ($this->csv as $row)
        {        
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan= $repository->findOneBy(array_merge($this->findplanBy,array('planid' => $row[$this->planidPosition])));
                $plansDocuments = new plansDocuments();
                $this->copyRowValues($plansDocuments, $row);
                $plansDocuments->planid = $plan->id;
                $plansDocuments->userid = $this->request->request->get("userid");
                $plansDocuments->dateAdded = new \DateTime("now");
                $this->em->persist($plansDocuments);
                $this->em->flush();
            }
            $this->increaseStep();
        }
    }
    public function uploadTableDocumentsChange()
    {
        foreach ($this->csv as $row)
        {
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan = $repository->findOneBy(array_merge($this->findplanBy, array('planid' => $row[$this->planidPosition])));     
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansDocuments');
                $plansDocuments= $repository->findOneBy(array("planid" => $plan->id, "name" => $row[$this->getPositionOfCsvField("name")]));   
                $this->copyRowValues($plansDocuments,$row);
                $plansDocuments->planid = $plan->id;
                $this->em->flush();
            }
            $this->increaseStep();
        }
    }
    public function uploadTableDocumentsDelete()
    {
        foreach ($this->csv as $row)
        { 
            if (!empty(array_filter($row))) {
                if ($this->session->get('stopProgress', false)) {
                    break;
                }
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan = $repository->findOneBy(array_merge($this->findplanBy, array('planid' => $row[$this->planidPosition])));     
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansDocuments');
                $plansDocuments = $repository->findOneBy(array("planid" => $plan->id,"name" => $row[$this->getPositionOfCsvField("name")]));   
                $this->em->remove($plansDocuments);
                $this->em->flush();
            }
            $this->increaseStep();
        }
    }    
    protected function deleteEntity(&$entity, $key) {
        $this->em->remove($entity);
    }
    private function getClass($object)
    {
        $explodename = explode("\\",get_class($object));
        return $explodename[count($explodename)-1];
    }
    public function createSnapshot($table)
    {
        $generalfunctions = $this->get('generalfunctions');
        $tblname = "snapshot_" . $table."_".$this->request->request->get("userid")."_".$this->currentDateString;
        $hash = "ss_" . $this->currentDateString . '_' . $generalfunctions->createHash(32);

        $sql = "Create Table " . $hash . " ENGINE=InnoDB as (SELECT * FROM ".$table." WHERE userid = ".$this->request->request->get("userid").")";
        $this->get("doctrine.dbal.default_connection")->executeQuery($sql);

        $sql = "insert into SnapshotTable(id, name, hash) values (NULL, '{$tblname}', '{$hash}');";
        $this->get("doctrine.dbal.default_connection")->executeQuery($sql);

    }
    public function getSnapshots()
    {
        $sql = 'select name from SnapshotTable';

        $tables = $this->get("doctrine.dbal.default_connection")->executeQuery($sql)->fetchAll();
        $snapshotTables = array();
        foreach ($tables as $table)
        {
            if (substr_count(current($table),"snapshot_") > 0)
            $snapshotTables[current($table)] = current($table);
        }
        return $snapshotTables;
    }         
    public function revertCategories($userid)
    {
        $categories = $this->getCategories();
        $snapshotTables = $this->getSnapshots();
        foreach ($categories as &$category)
        {
            foreach ($category['tables'] as $table)
            {
                foreach($snapshotTables as $snapshotTable)
                {
                    $explodeSnapshotTable = explode("_",$snapshotTable);
                    if ($explodeSnapshotTable[1] == $table['plan'] && $explodeSnapshotTable[2] ==  $userid)
                    {
                        $date = date_create(end($explodeSnapshotTable));
                        $bothTables = array("table" => $explodeSnapshotTable[1],"snapshot" => $snapshotTable);
                        $category['snapshot'][end($explodeSnapshotTable)]['time'] =date_format($date, 'Y-m-d H:i:s');
                        $category['snapshot'][end($explodeSnapshotTable)]['timevalue'] = end($explodeSnapshotTable);
                        $category['snapshot'][end($explodeSnapshotTable)]['tables'][] = $bothTables;
                    }
                }
                krsort($category['snapshot']);
            }
        }     
        return $categories;
    }
    public function revertAction(Request $request)
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");        
        $categories = $this->revertCategories($request->query->get("id"));
        return $this->render('ManageManageBundle:PlanUploadNew:revert.html.twig',array("categories" => $categories));
    }
    public function revertSavedAction(Request $request)
    {
        $categories = $this->revertCategories($request->request->get("id"));
        $category = $categories[$request->request->get("category")];
        $snapshots = $category['snapshot'][$request->request->get("timevalue")]['tables'];
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:SnapshotTable');

        foreach ($snapshots as $snapshot)
        {
            $sql = "DELETE FROM ".$snapshot['table']." WHERE userid=".$request->request->get("id");
            $this->get("doctrine.dbal.default_connection")->executeQuery($sql);
        }
        foreach ($snapshots as $snapshot)
        {
            $tablename = $repository->findOneBy(array('name' => $snapshot['snapshot']))->hash;
            $sql = "INSERT INTO ".$snapshot['table']." (SELECT * FROM ".$tablename.")";
            $this->get("doctrine.dbal.default_connection")->executeQuery($sql);
        }        
        return new Response("saved");
    }
    public function revertModalAction()
    {        
        return $this->render('ManageManageBundle:PlanUploadNew:revertmodal.html.twig');
    }
    public function snapshotManagementAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");          
        return $this->render('ManageManageBundle:PlanUploadNew:snapshotmanagement.html.twig',array('snapshotTables' => $this->getSnapshots()));
    }
    public function snapshotManagementModalAction()
    {
        return $this->render('ManageManageBundle:PlanUploadNew:snapshotmanagementmodal.html.twig');
    }     
    public function snapshotManagementDeleteSnapshotAction(Request $request)
    {
        $snapshotTables = $this->getSnapshots();
        if (isset($snapshotTables[$request->request->get("snapshot")]))
        {
            $sql = "DROP TABLE ".$request->request->get("snapshot");
            $this->get("doctrine.dbal.default_connection")->executeQuery($sql);            
        }
        return new Response("");
    }
    
    public function uploadTheCsvAction(Request $request) {
        if ($request->files->get("csvfile") == null) {
            $errors[] = "File was not uploaded";
        }
        
        $this->initializeFileUpload($request);
        $request->files->get("csvfile")->move(dirname($this->filename), basename($this->filename));
        
        if (count($errors) > 0) {
            $response = new Response(json_encode($errors));
            $response->headers->set('Content-Type', 'application/json');            
            return $response;
        }
        return new Response("success");
    }
    
    protected function addError($messages, $row = null, $col = null) {
        if (empty($messages)) {
            return;
        }
        
        if (!isset($this->errors)) {
            $this->errors = array();
        }
        
        $messages = is_array($messages) ? $messages : array($messages);
        foreach ($messages as $message) {
            if (isset($row)) {
                if (isset($col) && $col >= 0) {
                    // Column-spanning error message
                    $this->errors['table'][$row]['column'][$col][] = $message;
                }
                else {
                    // Row-spanning error message
                    $this->errors['table'][$row]['row'][] = $message;
                }
            }
            else {
                $this->errors['global'][] = $message;
            }
        }
    }
    
    protected function getTableColumnMappingValue($table, $column, $from) {
        $from = trim($from);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:planuploadTableColumnMappings');  
        $mappings = $repository->findOneBy(array('tableName'=>$table, 'columnName'=>$column));
        
        $currMappings = !empty($mappings->mappings) ? json_decode($mappings->mappings, true) : array();
        
        
        if (isset($currMappings[strtolower($from)]) && $column != 'planid') {
            $to = $currMappings[strtolower($from)];
        }
        else {
            $to = $from;
        }
        
        return $to; 
    }
    
    public function tableColumnMappingsCurrentAction(Request $request, $table, $column) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:planuploadTableColumnMappings');  
        $mappings = $repository->findOneBy(array('tableName'=>$table, 'columnName'=>$column));
        
        $currMappings = !empty($mappings->mappings) ? json_decode($mappings->mappings, true) : array();
        
        return $this->render('ManageManageBundle:PlanUploadNew:tablecolumnmappingscurrent.html.twig', array('currMappings' => $currMappings));
    }
    
    public function tableColumnMappingsAction(Request $request) {
        $table = $request->get('table');
        $column = $request->get('column');
        $from = $request->get('from', '');
        
        $entityPath = "\classes\classBundle\Entity\\" . $table;
        $entity = new $entityPath;
        $validOptions = !empty($entity->validationOptions[$column]) ? $entity->validationOptions[$column] : array();
        
        return $this->render('ManageManageBundle:PlanUploadNew:tablecolumnmapping.html.twig', array(
            'table' => $table,
            'column' => $column,
            'from' => $from,
            'validOptions' => $validOptions
        ));
    }
    
    public function tableColumnMappingsSavedAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:planuploadTableColumnMappings'); 
        $em = $this->getDoctrine()->getManager();
        $mappings = $repository->findOneBy(array('tableName'=>$request->get('table'), 'columnName'=>$request->get('column')));
        
        if ($mappings == null) {
            $mappings = new planuploadTableColumnMappings();
            $mappings->tableName = $request->get('table');
            $mappings->columnName = $request->get('column');
            $em->persist($mappings);
        }
        $currMappings = !empty($mappings->mappings) ? json_decode($mappings->mappings, true) : array();
        $currMappings[strtolower(trim($request->get('key')))] = trim($request->get('value'));
        
        $mappings->mappings = json_encode($currMappings);
        $em->flush();
        
        return new Response("success");
    }
    
    private function isXmlFile() {
        $fp = fopen($this->filename, "r");
        if ($fp === false) {
            return false;
        }
        $line = fgets($fp);
        fclose($fp);
        return substr($line, 0, 5) === "<?xml";
    }
    
    /**
     * This method takes care of custom logic when adding and changing plans that include sponsorConnectRequests fields.
     * 
     * According to the following rules, it will update the fromFirstName,fromLastName,fromEmailAddress, 
     * and fromPhone properties of sponsorConnectRequests using temporary placeholder variables.
     * 
     * If data exists for both RC* and CSM*, use data from RC*
     * If data exists for RC* only, use data from RC*
     * If data exists for CSM* only, use data from CSM* 
     * 
     * @param \classes\classBundle\Entity\sponsorConnectRequests $sponsorConnectRequest
     */
    private function reassignSponsorConnectRequestsFields(&$sponsorConnectRequest) {
        
        $csmExists = !empty($sponsorConnectRequest->fromFirstName_csm) || !empty($sponsorConnectRequest->fromLastName_csm) ||
            !empty($sponsorConnectRequest->fromEmailAddress_csm) || !empty($sponsorConnectRequest->fromPhone_csm);
        $rcExists = !empty($sponsorConnectRequest->fromFirstName_rc) || !empty($sponsorConnectRequest->fromLastName_rc) ||
            !empty($sponsorConnectRequest->fromEmailAddress_rc) || !empty($sponsorConnectRequest->fromPhone_rc);
        
        if (($csmExists && $rcExists) || $rcExists) {
            if (isset($sponsorConnectRequest->fromFirstName_rc)) {
                $sponsorConnectRequest->fromFirstName = $sponsorConnectRequest->fromFirstName_rc;
            }
            if (isset($sponsorConnectRequest->fromLastName_rc)) {
                $sponsorConnectRequest->fromLastName = $sponsorConnectRequest->fromLastName_rc;
            }
            if (isset($sponsorConnectRequest->fromEmailAddress_rc)) {
                $sponsorConnectRequest->fromEmailAddress = $sponsorConnectRequest->fromEmailAddress_rc;
            }
            if (isset($sponsorConnectRequest->fromPhone_rc)) {
                $sponsorConnectRequest->fromPhone = $sponsorConnectRequest->fromPhone_rc;
            }
        }
        else {
            if (isset($sponsorConnectRequest->fromFirstName_csm)) {
                $sponsorConnectRequest->fromFirstName = $sponsorConnectRequest->fromFirstName_csm;
            }
            if (isset($sponsorConnectRequest->fromLastName_csm)) {
                $sponsorConnectRequest->fromLastName = $sponsorConnectRequest->fromLastName_csm;
            }
            if (isset($sponsorConnectRequest->fromEmailAddress_csm)) {
                $sponsorConnectRequest->fromEmailAddress = $sponsorConnectRequest->fromEmailAddress_csm;
            }
            if (isset($sponsorConnectRequest->fromPhone_csm)) {
                $sponsorConnectRequest->fromPhone = $sponsorConnectRequest->fromPhone_csm;
            }
        }
    }
}
?>
