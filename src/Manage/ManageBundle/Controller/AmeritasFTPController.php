<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Entity\AmeritasFTP;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\ApiTokenTtl;
use Sessions\AdminBundle\Classes\adminsession;
class AmeritasFTPController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        $credentials = $this->getFTPCredentials();

        return $this->render('ManageManageBundle:AmeritasFTP:index.html.twig', array('credentials' => $credentials));
    }

    private function getFTPCredentials() {
        $em = $repository = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:AmeritasFTP');
        if (empty($repository->findAll())) {
            $credentials = new AmeritasFTP();
            $credentials->ip = "";
            $credentials->user = "";
            $credentials->pass = "";
            $em->persist($credentials);
            $em->flush();
            return $credentials;
        }
        return $repository->findAll()[0];
    }

    public function saveCredentialsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $credentials = $this->getFTPCredentials();
        $credentials->setIp($request->request->get("ip"));
        $credentials->setUser($request->request->get("user"));
        $credentials->setPass($request->request->get("pass"));
        $em->persist($credentials);
        $em->flush();
        return new Response("success");
    }

}
?>
