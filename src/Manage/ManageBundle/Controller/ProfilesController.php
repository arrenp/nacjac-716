<?php

namespace Manage\ManageBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Controller\DefaultController;
class ProfilesController extends Controller
{

    public function indexAction()
    {
        $generalfunctions = $this->get('generalfunctions'); 
        $session =  $this->get('adminsession');
		$session->set("section","Manage");
        $currentpage = "ManageProfiles";
        $session->set("currentpage",$currentpage);
        $profilesClassVar = $this->get('profiles.service');
        $sections = $profilesClassVar->sections();
        $profilelist = $generalfunctions->search("profiles","false","false","profiles","PlansPlansBundle:Profiles:Search/profiles.html.twig","profiles.id","profiles","inline-block","classesclassBundle:Search:searchprofiles.html.twig","profiles.id DESC,profiles.id", "Newest-Oldest,Oldest-Newest","","profileList");
        return $this->render('ManageManageBundle:Profiles:index.html.twig',array("profilelist" => $profilelist,"sections" => $sections,"session" => $session, "currentpage" => $currentpage));
    }

    public function displayProfileAction($profileId=null)
    {
        return $this->forward('SpeAppBundle:Default:profileReport', array("profileId" => $profileId , "fromAdmin" => true));
    }

    
}