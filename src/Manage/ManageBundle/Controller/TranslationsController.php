<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Entity\PlansTranslations;
use classes\classBundle\Entity\AccountsTranslations;
use classes\classBundle\Entity\DefaultTranslations;
use classes\classBundle\Entity\TranslationSources;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;


class TranslationsController extends Controller {
    
    public function indexAction($isDefault = false, $isAccount = false, $isPlan = false) {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:accounts");
        $accounts = $repository->findBy(array(), array('company' => 'ASC'));
        
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $languages = $repository->findAll();

        return $this->render("ManageManageBundle:Translations:index.html.twig", array(
            "languages" => $languages, 
            "accounts" => $accounts,
            'plans' => array(),
            'isDefault' => $isDefault,
            'isAccount' => $isAccount,
            'isPlan' => $isPlan
        ));
    }

    public function getPlansAction(Request $request) {
        $userid = $request->request->get('userid');
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:plans");
        $plans = $repository->findBy(array('userid' => $userid, 'deleted' => 0), array('id' => 'desc'));

        return $this->render('ManageManageBundle:Translations:optionstemplate.html.twig', array('plans' => $plans));
    }

    public function getParentAccountsTranslationsAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $languages = $repository->findAll();

        $parentTranslations = array();
        $sourceId = $request->query->get('id');

        $defaultRepo = $this->getDoctrine()->getRepository("classesclassBundle:DefaultTranslations");
        foreach ($languages as $language) {
            $parent = $defaultRepo->findOneBy(array('sourceId' => $sourceId, 'languageId' => $language->id));
            $parentTranslations[$language->id] = $parent->content;
        }

        return $this->render('ManageManageBundle:Translations:parentview.html.twig', array('source' => "Default", 'languages' => $languages, 'parentTranslations' => $parentTranslations));
    }

    public function getParentPlansTranslationsAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $languages = $repository->findAll();

        $parentTranslations = array();
        $sourceId = $request->query->get('id');
        $accountId = $request->query->get('accountId');

        $accountsRepo = $this->getDoctrine()->getRepository("classesclassBundle:AccountsTranslations");
        $defaultRepo = $this->getDoctrine()->getRepository("classesclassBundle:DefaultTranslations");
        $sourcetable = "Accounts";
        foreach ($languages as $language) {
            $parent = $accountsRepo->findOneBy(array('sourceId' => $sourceId, 'languageId' => $language->id, 'accountId' => $accountId));
            if ($parent == null) {
                $sourcetable = "Default";
                $parent = $defaultRepo->findOneBy(array('sourceId' => $sourceId, 'languageId' => $language->id));
            }
            $parentTranslations[$language->id] = $parent->content;
        }

        return $this->render('ManageManageBundle:Translations:parentview.html.twig', array('source' => $sourcetable, 'languages' => $languages, 'parentTranslations' => $parentTranslations));
    }
    
    public function searchAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $languages = $repository->findAll();
        $languageMap = array();
        foreach ($languages as $language) {
            $languageMap[$language->id] = $language->name; 
        }

        $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
        
        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = $repository->createQueryBuilder("s");
        $query->leftJoin("s.defaultTranslations", "dt")->leftJoin("s.accountsTranslations", "at")->leftJoin('s.plansTranslations', 'pt');
        $filteredCountQuery = $repository->createQueryBuilder('s')->select("COUNT(DISTINCT s.id)");
        $filteredCountQuery->leftJoin("s.defaultTranslations", "dt")->leftJoin("s.accountsTranslations", "at")->leftJoin('s.plansTranslations', 'pt');
        $totalCountQuery = $repository->createQueryBuilder('s')->select("COUNT(DISTINCT s.id)");
        $parameters = array();
        
        $accountId = (int) $request->get("accountId");
        $planid = (int) $request->get("planid");

        if (!empty($request->get("sourceKey"))) {
            $query->andWhere("s.sourceKey LIKE :sourceKey");
            $filteredCountQuery->andWhere("s.sourceKey LIKE :sourceKey");
            $parameters['sourceKey'] = '%'.$request->get("sourceKey").'%';
        }
        if (!empty($request->get("domain"))) {
            $query->andWhere("s.domain LIKE :domain");
            $filteredCountQuery->andWhere("s.domain LIKE :domain");
            $parameters['domain'] = '%'.$request->get("domain").'%';
        }
        if (!empty($request->get("content"))) {
            if ($request->get("isAccount")) {
                $query->andWhere("at.accountId = $accountId");
                $query->andWhere("at.content LIKE :content");
                $filteredCountQuery->andWhere("at.accountId = $accountId");
                $filteredCountQuery->andWhere("at.content LIKE :content");
                $parameters['content'] = '%'.$request->get("content").'%';
            } else if($request->get("isPlan")) {
                $query->andWhere("pt.planid = $planid");
                $query->andWhere("pt.content LIKE :content");
                $filteredCountQuery->andWhere("pt.planid = $planid");
                $filteredCountQuery->andWhere("pt.content LIKE :content");
                $parameters['content'] = '%'.$request->get("content").'%';
            }
            else {
                $query->andWhere("dt.content LIKE :content");
                $filteredCountQuery->andWhere("dt.content LIKE :content");
                $parameters['content'] = '%'.$request->get("content").'%';
            }
        }
        $query->setParameters($parameters);
        $filteredCountQuery->setParameters($parameters);
        
        foreach ($request->get("order") as $val) {
            $query->addOrderBy('s.'.$request->get("columns")[$val["column"]]["data"], $val["dir"]);
        }
        $query->groupBy("s.id");
        $query->addOrderBy("s.id", "ASC");
        $query->setFirstResult($request->get("start"));
        $query->setMaxResults($request->get("length"));
        
        $data = $query->getQuery()->getResult();
        $recordsFiltered = $filteredCountQuery->getQuery()->getSingleScalarResult();
        $recordsTotal = $totalCountQuery->getQuery()->getSingleScalarResult();

        
        foreach ($data as &$translationSource) {
            $translationSource->translations = array();
            $translations = $request->get("isAccount") ? $translationSource->accountsTranslations : ($request->get("isDefault") ? $translationSource->defaultTranslations : $translationSource->plansTranslations);

            foreach ($translations as $translation) {
                if ($request->get("isAccount") && $translation->accountId !== $accountId) {
                    continue;
                } else if ($request->get("isPlan") && $translation->planid !== $planid) {
                    continue;
                }

                $translationSource->translations[$translation->languageId] = array(
                    'content' => $translation->content,
                    'id' => $translation->id
                );
            }
        }

        $results = array(
            "draw" => (int) $request->get("draw"),
            "recordsTotal" => (int) $recordsTotal,
            "recordsFiltered" => (int) $recordsFiltered,
            "data" => $data
        );
        
        if ($request->get("export")) {
            $response = new StreamedResponse(function() use ($data, $languages) {
                $handle = fopen('php://output', 'w+');
                fwrite($handle, "\xEF\xBB\xBF");

                $header = array('Id', 'Key', 'Domain');
                foreach ($languages as $language) {
                    $header[] = ucwords($language->description);
                }
                fputcsv($handle, $header);

                foreach ($data as $row) {
                    $csvRow = array($row->id, $row->sourceKey, $row->domain);
                    foreach ($languages as $language) {
                        $csvRow[] = $row->translations[$language->id]['content'] ?  $row->translations[$language->id]['content'] : "";
                    }
                    fputcsv($handle, $csvRow);
                }
                fclose($handle);
            });
            $response->headers->set('Content-Type', 'text/csv');
            $response->headers->set('Content-Disposition','attachment; filename="TranslationsExport.csv"');
            return $response;
        }

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'application/json');
        return $response; 
    }
    
    public function addAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
        $translationSource = $repository->find($request->get("sourceId"));
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $language = $repository->find($request->get("languageId"));
        return $this->render("ManageManageBundle:Translations:translationEdit.html.twig", array(
            'action' => 'add',
            'language' => $language,
            'translationSource' => $translationSource
        ));
    }
    
    public function editDefaultTranslationsAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:DefaultTranslations");
        $translation = $repository->find($request->get("id"));
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
        $translationSource = $repository->find($translation->sourceId);
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $language = $repository->find($translation->languageId);
        
        return $this->render("ManageManageBundle:Translations:translationEdit.html.twig", array(
            'action' => 'edit',
            'language' => $language,
            'translationSource' => $translationSource,
            'translation' => $translation
        ));
    }
    
    public function editAccountsTranslationsAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:AccountsTranslations");
        $translation = $repository->find($request->get("id"));
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
        $translationSource = $repository->find($translation->sourceId);
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $language = $repository->find($translation->languageId);

        return $this->render("ManageManageBundle:Translations:translationEdit.html.twig", array(
            'action' => 'edit',
            'language' => $language,
            'translationSource' => $translationSource,
            'translation' => $translation
        ));
    }

    public function editPlansTranslationsAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:PlansTranslations");
        $translation = $repository->find($request->get("id"));
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
        $translationSource = $repository->find($translation->sourceId);
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $language = $repository->find($translation->languageId);

        return $this->render("ManageManageBundle:Translations:translationEdit.html.twig", array(
            'action' => 'edit',
            'language' => $language,
            'translationSource' => $translationSource,
            'translation' => $translation
        ));
    }

    
    public function saveDefaultTranslationsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        if ($request->get("action") === "add") {
            $translation = new DefaultTranslations();
            
            $translation->sourceId = $request->get("sourceId");
            $translation->languageId = $request->get("languageId");
            $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
            $translation->translationSource = $repository->find($request->get("sourceId"));
            $em->persist($translation);
        }
        else {
            $repository = $this->getDoctrine()->getRepository("classesclassBundle:DefaultTranslations");
            $translation = $repository->find($request->get("id"));
        }
        $translation->content = $request->get("content");

        $em->flush();
        return new Response("success");
    }
    
    public function saveAccountsTranslationsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        if ($request->get("action") === "add") {
            $translation = new AccountsTranslations();
            
            $translation->sourceId = $request->get("sourceId");
            $translation->languageId = $request->get("languageId");
            $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
            $translation->translationSource = $repository->find($request->get("sourceId"));
            $translation->accountId = $request->get("accountId");
            $em->persist($translation);
        }
        else {
            $repository = $this->getDoctrine()->getRepository("classesclassBundle:AccountsTranslations");
            $translation = $repository->find($request->get("id"));
        }
        $translation->content = $request->get("content");

        $em->flush();
        return new Response("success");
    }

    public function savePlansTranslationsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        if ($request->get("action") === "add") {
            $translation = new PlansTranslations();

            $translation->sourceId = $request->get("sourceId");
            $translation->languageId = $request->get("languageId");
            $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
            $translation->translationSource = $repository->find($request->get("sourceId"));
            $translation->accountId = $request->get("accountId");
            $translation->planid = $request->get("planid");
            $em->persist($translation);
        }
        else {
            $repository = $this->getDoctrine()->getRepository("classesclassBundle:PlansTranslations");
            $translation = $repository->find($request->get("id"));
        }
        $translation->content = $request->get("content");

        $em->flush();
        return new Response("success");
    }

    public function deleteAccountsTranslationsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:AccountsTranslations");
        $translation = $repository->find($request->get("id"));
        $em->remove($translation);
        $em->flush();

        return new Response("success");
    }

    public function getPlansTranslationsAction(Request $request) {
        $sourceId = $request->request->get('sourceId');
        $accountId = $request->request->get('accountId');
        $languageid = $request->request->get('languageid');

        $sourcetable = "Accounts";
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:AccountsTranslations");
        $translation = $repository->findOneBy(array('sourceId' => $sourceId, 'accountId' => $accountId, 'languageId' => $languageid));
        if ($translation == null) {
            $sourcetable = "Default";
            $repository = $this->getDoctrine()->getRepository("classesclassBundle:DefaultTranslations");
            $translation = $repository->findOneBy(array('sourceId' => $sourceId, 'languageId' => $languageid));
        }

        $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
        $source = $repository->findOneBy(array('id' => $sourceId));

        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $language = $repository->findOneBy(array('id' => $languageid));

        $data = array();
        $data['content'] = $translation->content;
        $data['domain'] = $source->domain;
        $data['key'] = $source->sourceKey;
        $data['language'] = $language->description;
        $data['source'] = $sourcetable;

        return new JsonResponse($data);
    }

    public function getAccountsTranslationsAction(Request $request) {
        $sourceId = $request->request->get('sourceId');
        $languageid = $request->request->get('languageid');

        $repository = $this->getDoctrine()->getRepository("classesclassBundle:DefaultTranslations");
        $translation = $repository->findOneBy(array('sourceId' => $sourceId, 'languageId' => $languageid));

        $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
        $source = $repository->findOneBy(array('id' => $sourceId));

        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $language = $repository->findOneBy(array('id' => $languageid));

        $data = array();
        $data['content'] = $translation->content;
        $data['domain'] = $source->domain;
        $data['key'] = $source->sourceKey;
        $data['language'] = $language->description;
        $data['source'] = "Default";

        return new JsonResponse($data);
    }

    public function deletePlansTranslationsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:PlansTranslations");
        $translation = $repository->find($request->get("id"));
        $em->remove($translation);
        $em->flush();

        return new Response("success");
    }
    
    public function addSourceAction() {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:languages");
        $languages = $repository->findAll();
        return $this->render("ManageManageBundle:Translations:translationSourceAdd.html.twig", array('languages'=>$languages));
    }
    
    public function editSourceAction($id) {
        return $this->render("ManageManageBundle:Translations:translationSourceEdit.html.twig");
    }
    
    public function saveSourceAction(Request $request) {
        $sourceKey = trim($request->get("sourceKey"));
        $domain = trim($request->get("domain"));
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:TranslationSources");
        $existing = $repository->findOneBy(array("sourceKey" => $sourceKey, "domain" => $domain));
        
        if ($existing != null) {
            return new Response("sourceExists");
        }
        
        $translationSource = new TranslationSources();
        $translationSource->sourceKey = $sourceKey;
        $translationSource->domain = $domain;
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($translationSource);
        $em->flush();
        
        foreach ($request->get("translation") as $languageId => $eachTranslation) {
            if ($request->get("isAccount")) {
                $translation = new AccountsTranslations;
                $translation->accountId = $request->get("accountId", 0);
            }
            else {
                $translation = new DefaultTranslations;
            }
            $translation->translationSource = $translationSource;
            $translation->languageId = $languageId;
            $translation->content = $eachTranslation;
            $em->persist($translation);
        }
        $em->flush();

        return new Response("success");
    }
}
