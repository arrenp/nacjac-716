<?php

namespace Manage\ManageBundle\Controller;

use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HubSpotDiagnosticToolController extends Controller {
    
    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "emailCampaigns");
        return $this->render('ManageManageBundle:HubSpotDiagnosticTool:index.html.twig');
    }
    
    public function showContactAction(Request $request) {
        $email = $request->get("email");
        /* @var $hubSpotContactService \HubSpot\HubSpotBundle\Services\HubSpotContactsService */
        $hubSpotContactService = $this->get("hub_spot.contacts");
        $contact = $hubSpotContactService->getContactByEmail($email, ['propertyMode' => 'value_only']);
        if (!empty($contact)) {
            ksort($contact['properties']);
        }
        
        /* @var $hubSpotEmailService \HubSpot\HubSpotBundle\Services\HubSpotEmailService */
        $hubSpotEmailService = $this->container->get('hub_spot.emails');
        $eventTypes = $hubSpotEmailService->getEmailEventTypes();
        
        return $this->render('ManageManageBundle:HubSpotDiagnosticTool:showContact.html.twig', ['contact' => $contact, 'email' => $email, 'eventTypes' => $eventTypes]);
    }
    
    public function getEmailEventsAction(Request $request) {
        /* @var $hubSpotEmailService \HubSpot\HubSpotBundle\Services\HubSpotEmailService */
        $hubSpotEmailService = $this->container->get('hub_spot.emails');
        
        $data = [];
        $eventTypes = $request->get("eventTypes", []);
        foreach ($eventTypes as $eventType) {
            $events = $hubSpotEmailService->getEmailEvents(null, null, $request->get("email"), $eventType['value']);
            foreach ($events as $event) {
                $data[] = [$event['type'], date('Y-m-d H:i:s', $event['created'] / 1000)];
            }
        }
        return new JsonResponse(['data' => $data]);
    }
    
    public function getListMembershipsAction(Request $request) {
        /* @var $hubSpotContactService \HubSpot\HubSpotBundle\Services\HubSpotContactsService */
        $hubSpotContactService = $this->get("hub_spot.contacts");
        $contact = $hubSpotContactService->getContactById($request->get("contactId"), ['propertyMode' => 'value_only']); 
        
        /* @var $hubSpotListService \HubSpot\HubSpotBundle\Services\HubSpotContactListsService */
        $hubSpotListService = $this->get("hub_spot.contactlists");
        
        $listMemberships = [];
        if (!empty($contact)) {
            $listIds = array_column($contact['list-memberships'], 'static-list-id');
            $response = $hubSpotListService->getGroupById($listIds);
            $listMemberships = isset($response['lists']) ? $response['lists'] : [];
        }
        return $this->render('ManageManageBundle:HubSpotDiagnosticTool:listMemberships.html.twig', ['listMemberships' => $listMemberships]);
    }
    
    public function getWorkflowMembershipsAction(Request $request) {
        /* @var $hubSpotWorkflowService \HubSpot\HubSpotBundle\Services\HubSpotWorkflowService */
        $hubSpotWorkflowService = $this->get("hub_spot.workflows");
        
        $workflowMemberships = $hubSpotWorkflowService->getCurrentEnrollment($request->get("contactId"));
        return $this->render('ManageManageBundle:HubSpotDiagnosticTool:workflowMemberships.html.twig', ['workflowMemberships' => $workflowMemberships]);
    }
}
