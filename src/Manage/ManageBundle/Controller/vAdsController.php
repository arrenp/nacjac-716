<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Shared\DocumentsBundle\Classes\documents;
use Permissions\RolesBundle\Classes\rolesPermissions;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\videoLibrary;
use Utilities\pdf\pdfClass;
use classes\classBundle\Entity\dataTablesExport;
use Utilities\mailer\PHPMailer;
use Utilities\mailer\SMTP;
use Utilities\mailer\mailerInit;
set_time_limit(0);
require_once(getcwd()."/../src/Utilities/dompdf/dompdf_config.inc.php");

class vAdsController extends Controller
{
    
    public $header = "<th>Id</th><th>PartnerID</th><th>PlayDuration</th><th>VideoLength</th><th>Percentage Viewed</th><th>ParticipantID</th><th>VideoPlayed</th><th>PlanProvider</th><th>Date</th>";
    public $header2 = "<th>Video</th><th>Views</th><th>Average Percentaged Viewed</th><th>Popularity Index</th>";    
    
    public function indexAction($emailed = false,$startDate = null, $endDate = null)
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        
        $session = $this->get('adminsession');
        $session->set("manageUserid",$request->query->get("id"));
        $methods = $this->get('GeneralMethods');
        $session->set("section", "Manage");

        $sections = array();
        $connection = $this->get('doctrine.dbal.default_connection');

        

        $methods->addSection($sections, "settings", "Settings");
        $methods->addSection($sections, "stats", "Stats");

        $formats = array();
        $methods->addSection($formats, 0, "New");
        
        $methods->addSection($formats, 1, "Old");
        

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $session->manageUserid));


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:product');
        $product = $repository->findOneBy(array('name' => 'vad'));


        $repository = $this->getDoctrine()->getRepository('classesclassBundle:APIAccounts');
        $APIAccount = $repository->findOneBy(array('userid' => $session->manageUserid, 'active' => 1, 'productid' => $product->id));



        $embedCode = "https://videolib.smartplanenterprise.com/vWidget.js";

        $sql = "SELECT * FROM APIAccounts LEFT join accounts on APIAccounts.userid = accounts.id ORDER by accounts.id";
        $APIAccounts = $connection->fetchAll($sql);


        $sql = "SELECT * FROM media WHERE  applications LIKE '%\"vad\"%' AND shareable = 1";
        $connection = $this->get('doctrine.dbal.default_connection');
        $medias = $connection->fetchAll($sql);



        foreach ($medias as &$media)
        {
            $media['movienumber'] = $media['product'] . $media['module'];
            $media['mp4'] = $media['mp4url'];
            $media['webm'] = $media['webmurl'];
            $media['videolink'] = $this->generateUrl('_plans_outreach_videoplayer_videoonly_WithoutLibrary') . "?mp4=" . $media['mp4'] . "&webm=" . $media['webm'];
            $media['checked'] = "";
            $sql = "SELECT * FROM vAdsMediaAuth WHERE userid = '" . $account->id . "' AND mediaid =  " . $media['id'];
            $library = $connection->fetchAll($sql);
            if (count($library) > 0) $media['checked'] = "checked";
        }

        if ($APIAccount != null)
        {
            $stats = $this->getStats($startDate,$endDate,$session->manageUserid);
            $httpmethod = $methods->httpmethod();      
            return $this->render("ManageManageBundle:Vads:index.html.twig", array("sections" => $sections, "formats" => $formats, "APIAccount" => $APIAccount, "embedCode" => $embedCode, "account" => $account, "session" => $session, "medias" => $medias, "stats" => $stats, "APIAccounts" => $APIAccounts, "header" => $this->header, "header2" => $this->header2, "emailed" => $emailed,"startDate" => $startDate,"endDate" => $endDate,"httpmethod" => $httpmethod));
        }
        
        else return new Response("");
    }
    public function saveMediaListAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $connection = $this->get('doctrine.dbal.default_connection');
        $sql = "SELECT * FROM vAdsMediaAuth WHERE userid = '".$session->manageUserid."' AND  mediaid = '".$request->request->get("id")."'";
        $media  = $connection->executeQuery($sql)->fetch();
        if ($request->request->get("checked") == "true")
        $sql = "INSERT INTO vAdsMediaAuth (userid,mediaid) VALUES ('".$session->manageUserid."','".$request->request->get("id")."')";
        else 
        $sql = "DELETE FROM vAdsMediaAuth WHERE userid ='".$session->manageUserid."' AND mediaid = '".$request->request->get("id")."'";
        $connection->executeQuery($sql);
        return new Response("");
    }    
    public function getStats($startDate = null, $endDate = null,$userid = null,$planid = null)
    {
        $GeneralMethods = $this->get('GeneralMethods');
        $session = $this->get('adminsession');
        $connection = $this->get('doctrine.dbal.default_connection');
        $appendSql = " userid = '".$userid."'";
        if ($planid != null)
        $appendSql = $appendSql." AND planid = '".$planid."'";
        
        if ($startDate != null)
        $appendSql = $appendSql." AND date >= '".$startDate."'";
        
        if ($endDate != null)
        $appendSql = $appendSql." AND date <= '".$endDate."'";        
            
            
        $statsFetch = array();
        $sql = "SELECT COUNT(id) as total FROM vAdsStats WHERE ".$appendSql;       
        $statsFetch  = array_merge($statsFetch,$connection->executeQuery($sql)->fetch());
        $sql = "SELECT COUNT(DISTINCT(cookieid)) as uniqueTotal FROM vAdsStats WHERE ".$appendSql;       
        $statsFetch  = array_merge($statsFetch,$connection->executeQuery($sql)->fetch());        
        $sql = "SELECT COUNT(id) as timesClosed FROM vAdsStats WHERE ".$appendSql." AND closed = 1";       
        $statsFetch  = array_merge($statsFetch,$connection->executeQuery($sql)->fetch());          
        $stats['graph1']['json'] = 
        '
                   {
           "chart": {
                "type": "column"
            },
           "title": {
                "text": "Vads Stats"
            },
            "xAxis": {
                "categories": ["Stats"]
              
            },
            "yAxis": {

                "title": {
                    "text": "Total"
                }
            } ,   
            "credits": {
                "enabled": false
            },
            "series": 
            [
                {
                    "name":"Total Views",
                    "data": ['.$statsFetch['total'].']
                },
                {
                    "name":"Unique Views",
                    "data": ['.$statsFetch['uniqueTotal'].']
                },
                {
                    "name":"Total Times Closed",
                    "data": ['.$statsFetch['timesClosed'].']
                }              
                               
            ]
        }             
        ';
        
        $GeneralMethods->generateGraphsObject($stats);
        return $stats;
    }
    public function getStatsAction(Request $request)
    {

        $return = array("startDate" => null, "endDate" => null, "userid" => null, "planid" => null);
        $get = $request->query->all();
        $post = $request->request->all();
        
        foreach($get as  $key => $value)
        $return[$key] = $value;
        foreach($post as  $key => $value)
        $return[$key] = $value;
        
        if ($return['endDate'] != null) 
        $return['endDate']  =   $return['endDate']." 23:59:59";
        
        
                 
        return $this->render("ManageManageBundle:Vads:graphs.html.twig", array("stats" => $this->getStats($return['startDate'], $return['endDate'], $return['userid'],$return['planid'])));
    }
    public function exportToPdfAction(Request $request)
    {
        $dompdf = new \DOMPDF();
        $data = '<img src = "'.$request->query->get("graph1").'" />';
        $dompdf->load_html($data);
        $dompdf->render();
        $dompdf->output(); 
        $name = "VideoLibraryReport.pdf";
        return new Response($dompdf->stream($name));
    }
}

