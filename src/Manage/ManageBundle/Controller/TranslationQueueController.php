<?php
namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
class TranslationQueueController extends Controller
{
    private $inputFields = 
    [
        [
            "name" => "sourceKey",
            "description" => "Source Key",
            "required" => true
        ],
        [
            "name" => "domain",
            "description" => "Domain",
            "required" => true
        ],
        [
            "name" => "language",
            "description" => "Language",
            "required" => true
        ],
        [
            "name" => "content",
            "description" => "Content",
            "required" => true
          
        ],
        [
            "name" => "active",
            "description" => "Active",
            "required" => true
        ],  
        [
            "name" => "partnerid",
            "description" => "PartnerId",
            "required" => false
        ],         
    ];
    public function indexAction()
    {
        $this->get("session")->set("Section","Manage");
        return $this->render("index.html.twig");
    }   
    public function listAction(Request $request)
    {
        $table = "TranslationQueue";
        $result = $this->get("QueryBuilderDataTables")->jsonObject($table,["a.id","a.sourceKey","a.domain","a.language","a.content"],$request->request->all());
        $data = [];
        foreach ($result['rows'] as $translation)
        {
            $data[] = [
                $translation->id,
                $translation->sourceKey,
                $translation->domain,
                $translation->language,
                $translation->content,
                $translation->active,
                $translation->partnerid,
                $this->render("buttons.html.twig",["id" => $translation->id ])->getContent()
            ];
        }      
        $response = $result['response'];
        $response['data'] = $data;
        return new JsonResponse($response);
    }    
    public function addAction(Request $request)
    {
        return $this->render("add.html.twig",["inputFields" => $this->inputFields]);
    }
    public function addSavedAction(Request $request)
    {
        $params = $request->request->all();
        if (empty($params['partnerid']))
            unset($params['partnerid']);
        $this->get("TranslationQueueService")->add($params);
        return new Response("");
    }
    public function editAction(Request $request)
    {
        $translation = $this->get("TranslationQueueService")->findOneBy(["id" => $request->query->get("id")]);
        $this->inputFieldsWithTranslation($translation);
        return $this->render("edit.html.twig",["inputFields" => $this->inputFields,"id" => $request->query->get("id")]);
    }
    public function editSavedAction(Request $request)
    {
        $params = $request->request->all();
        if (empty($params['partnerid']))
            $params['partnerid'] = null;
        $translation = $this->get("TranslationQueueService")->findOneBy(["id" => $request->request->get("id")]);
        $this->get("TranslationQueueService")->save($translation,$params);
        return new Response("");
    }
    public function inputFieldsWithTranslation($translation)
    {
        foreach ($this->inputFields as &$field)
        {
            $field['value'] = $translation->{$field['name']};
        }        
    }
    public function deleteAction(Request $request)
    {
        $translation = $this->get("TranslationQueueService")->findOneBy(["id" => $request->query->get("id")]);
        $this->inputFieldsWithTranslation($translation);
        return $this->render("delete.html.twig",["inputFields" => $this->inputFields,"id" => $request->query->get("id")]);
    }
    public function deleteSavedAction(Request $request)
    {
        $translation = $this->get("TranslationQueueService")->findOneBy(["id" => $request->request->get("id")]);
        $this->get("TranslationQueueService")->delete($translation);
        return new Response("");
    }
    public function render($file,$params = array())
    {
        return parent::render('ManageManageBundle:TranslationQueue:'.$file,$params);
    }
    public function uploadCsvAction(Request $request)
    {
        $csv = array_map('str_getcsv', file($request->files->get("csvFile"))); 
        foreach ($csv as $row)
        {
            $params = 
            [
                "language" => "en",
                "active" => 1,
                "partnerid" => $request->request->get("partnerid")
            ];
            $params['sourceKey'] = $row[0];
            $params['domain'] = $row[1];
            $params['content'] = $row[2];
            if (!empty(trim($row[3])))
                $params['language'] = $row[3];
            if (trim($row[4]) === '0')
                $params['active'] = 0;
            if (!empty(trim($row[5])))
                $params['partnerid'] = $row[5];
            if (empty($params['partnerid']))
                $params['partnerid'] = null;
            $this->get("TranslationQueueService")->add($params);
        }
        return new Response("chickendog");
    }
}