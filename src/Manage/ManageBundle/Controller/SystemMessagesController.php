<?php
namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Sessions\AdminBundle\Classes\adminsession;
set_time_limit(0);
class SystemMessagesController extends Controller
{
    public function indexAction(Request $request)
    {             
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        return $this->render("index.html.twig");
    }   
    public function sendMessageAllAction(Request $request)
    {
        return $this->render("sendmessage.html.twig",array("sendMessagePostUrl" =>$this->generateUrl('_manage_system_messages_sendmessage_all_sent')));
    }
    public function sendMessageParams($request)
    {
        $message['message'] = $request->request->get("message");
        $message['expirationDate'] = new \DateTime($request->request->get("expirationDate"));
        $tags = $request->request->get("tags");
        $realTags = [];
        foreach ($tags as $id)
        {
            $realTags[]['name'] = $this->get("SystemMessagesTagsService")->findOneBy(["id" => $id])->name;
        }
        return ["message" => $message, "tags" => $realTags];
    }
    public function sendMessages($users,$request)
    {
        $params = $this->sendMessageParams($request);
        return new JsonResponse($this->get("AccountsUsersSystemMessagesService")->addSystemMessages($users,$params['message'],$params['tags']));        
    }
    public function sendMessageAllSentAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $users= $repository->findAll();
        return $this->sendMessages($users,$request);
    }
    public function sendMessageAccountAction(Request $request)
    {
        return $this->render("sendmessage.html.twig",array("sendMessagePostUrl" =>$this->generateUrl('_manage_system_messages_sendmessage_account_sent')."?userid=".$request->query->get("userid")));
    }
    public function sendMessageAccountSentAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $users= $repository->findBy(array("userid" => $request->query->get("userid")) );
        return $this->sendMessages($users,$request);
    }   
    public function sendMessageUserAction(Request $request)
    {
        return $this->render("sendmessage.html.twig",array("sendMessagePostUrl" =>$this->generateUrl('_manage_system_messages_sendmessage_user_sent')."?id=".$request->query->get("id")));
    }    
    public function sendMessageUserSentAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $user= $repository->findOneBy(array("id" => $request->query->get("id")) );
        $params = $this->sendMessageParams($request);
        return new JsonResponse($this->get("AccountsUsersSystemMessagesService")->addSystemMessage($user->getId(),$params['message'],$params['tags']));
    }       
    public function tagsAction(Request $request)
    {
        return $this->render("tags.html.twig");
    }
    public function addTagAction(Request $request)
    {      
        return $this->render("addtag.html.twig");
    }
    public function addTagSavedAction(Request $request)
    {
        return new jsonResponse($this->get("SystemMessagesTagsService")->addTag($request->request->all()));
    }
    public function editTagAction(Request $request)
    {      
        return $this->render("edittag.html.twig",array("tag" => $this->get("SystemMessagesTagsService")->findOneBy(array("id" => $request->query->get("id")))));
    }
    public function editTagSavedAction(Request $request)
    {
        return new jsonResponse($this->get("SystemMessagesTagsService")->save($this->get("SystemMessagesTagsService")->findOneBy(array("id" => $request->request->get("id"))),["name" => $request->request->get("name")]));
    }
    public function deleteTagAction(Request $request)
    {
        return $this->render("deletetag.html.twig",array("tag" => $this->get("SystemMessagesTagsService")->findOneBy(array("id" => $request->query->get("id")))));        
    }
    public function deleteTagSavedAction(Request $request)
    {
        $this->get("SystemMessagesTagsService")->deleteTag($request->request->get("id"));
        return new Response("");        
    }    
    public function render($file,$params = array())
    {
        return parent::render('ManageManageBundle:SystemMessages:'.$file,$params);
    }
    public function messagesDatatablesAction(Request $request)
    {
        $response= new \stdClass();
        $response->data = array();
        $count = $this->get("SystemMessagesService")->dataTablesTotalCount();
        $messages= $this->get("SystemMessagesService")->dataTablesQuery($request,false)->fetchAll(); 
        $messageCount = $this->get("SystemMessagesService")->dataTablesQuery($request,true)->fetch()['count'];
        foreach ($messages as $message)
        {
            $tags = $this->get("SystemMessagesTagsService")->getTagsByMessageId($message['smid']);
            if ($message['expirationDate'] != null)
            {
                $message['expirationDate'] = explode(" ",$message['expirationDate'])[0];
            }
            $row = array();
            $row[] = $message['id'];
            $row[] = $this->get("SystemMessagesService")->dataTablesMessage($message['message']);
            $row[] = $this->render("Search/messagestags.html.twig",array("tags" => $tags))->getContent();
            $row[] = $message['expirationDate'];
            $row[] = $this->render("Search/actionbuttons.html.twig",array("message" => $message ))->getContent();
            $response->data[] = $row;
        }
        $response->recordsTotal = $count;
        $response->recordsFiltered = $messageCount;
        return new JsonResponse($response);
    }
}

