<?php

namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
class RiskProfileCreatorAnswerController extends Controller
{
    public function indexAction(Request $request)
    {    
        $languages = $this->get("LanguagesService")->getAll("id");
        $params = 
        [
            "questionid" => $request->query->get("id"),
            "answers" => $this->get("RiskProfileQuestionnaireAnswerService")->getAnswers($request->query->get("id")),
            "languages" => $languages
        ];
        return $this->render("index.html.twig",$params);
    }
    public function addAction(Request $request)
    {
        $this->get("RiskProfileQuestionnaireAnswerService")->addAnswer($request->request->all());
        return new Response("saved");
    }
    public function editAction(Request $request)
    {
        $this->get("RiskProfileQuestionnaireAnswerService")->editAnswer($request->request->get("params"),$request->request->get("locales"));
        return new Response("");
    }
    public function deleteAction(Request $request)
    {
        $this->get("RiskProfileQuestionnaireAnswerService")->deleteAnswer($request->request->get("id"));
        return new Response("");
    }
    public function loadAnswersAction(Request $request)
    {
        $params = 
        [
            "questionid" => $request->request->get("id"),
            "answers" => $this->get("RiskProfileQuestionnaireAnswerService")->getAnswers($request->request->get("id")),
            "languages" => $this->get("LanguagesService")->getAll("id")
        ];
        return $this->render("loadanswers.html.twig",$params);        
    }
    public function reorderAction(Request $request)
    {
        $params = 
        [
            "questionid" => $request->query->get("id"),
            "answers" => $this->get("RiskProfileQuestionnaireAnswerService")->getAnswers($request->query->get("id")),
            "languages" => $this->get("LanguagesService")->getAll("id")
        ];
        return $this->render("reorder.html.twig",$params);
    }
    public function reorderSaveAction(Request $request)
    {
        $this->get("RiskProfileQuestionnaireAnswerService")->saveAnswerOrder($request->request->get("ids"));
        return new Response("");
    }
    public function render($file,$params = array()) 
    {
        return parent::render('ManageManageBundle:RiskProfileCreatorAnswer:'.$file,$params); 
    }    
}