<?php
namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use classes\classBundle\Entity\fileConverter;
use classes\classBundle\Entity\fileConverterHeaders;
use classes\classBundle\Entity\fileConverterMappings;
class FileConverterController extends Controller
{
    public function indexAction(Request $request)
    {
        $request->getSession()->set("section","Manage");
        return $this->render('ManageManageBundle:FileConverter:index.html.twig',array("converters" => $this->getFileConverters()));
    }
    public function getFileConvertersAction()
    {
        return $this->render('ManageManageBundle:FileConverter:getfileconverters.html.twig',array("converters" => $this->getFileConverters()));
    }
    public function getFileConverters()
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverter');   
        $converters = $repository->findAll();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterHeaders');   
        foreach ($converters as &$converter)
        {
            $converter->headers = $repository->findBy(array("converterid" => $converter->id));
        }
        return $converters;       
    }
    public function addAction()
    {
        return $this->render('ManageManageBundle:FileConverter:add.html.twig');
    }
    public function addSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $converter = new fileConverter();
        $converter->name = $request->request->get("name");
        $em->persist($converter);
        $em->flush();
        return new Response("");
    }
    public function editAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverter'); 
        $converter = $repository->findOneBy(array("id" => $request->query->get("id")));        
        return $this->render('ManageManageBundle:FileConverter:edit.html.twig',array("converter" => $converter));
    }
    public function editSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverter'); 
        $converter = $repository->findOneBy(array("id" => $request->request->get("id")));
        $converter->name = $request->request->get("name");
        $em->flush();
        return new Response("");
    }   
    public function deleteAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverter'); 
        $converter = $repository->findOneBy(array("id" => $request->query->get("id")));        
        return $this->render('ManageManageBundle:FileConverter:delete.html.twig',array("converter" => $converter));
    }
    public function deleteSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverter'); 
        $converter = $repository->findOneBy(array("id" => $request->request->get("id")));
        $em->remove($converter);
        $em->flush();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterHeaders');
        $headers = $repository->findby(array("converterid" => $request->request->get("id")));
        foreach ($headers as $header)
        {
            $this->deleteHeader($header->id);
        }
        return new Response("");
    } 
    public function addHeaderAction(Request $request)
    {
        return $this->render('ManageManageBundle:FileConverter:addheader.html.twig');
    }
    public function addHeaderSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $header = new fileConverterHeaders();
        $header->name = $request->request->get("name");
        $header->converterid = $request->request->get("converterid");
        $em->persist($header);
        $em->flush();        
        return new Response("");
    }    
    public function editHeaderAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterHeaders'); 
        $header = $repository->findOneBy(array("id" => $request->query->get("id")));
        return $this->render('ManageManageBundle:FileConverter:editheader.html.twig',array("header" => $header));
    }
    public function editHeaderSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterHeaders'); 
        $header = $repository->findOneBy(array("id" => $request->request->get("id")));        
        $header->name = $request->request->get("name");
        $em->flush();        
        return new Response("");
    }     
    public function deleteHeaderAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterHeaders'); 
        $header = $repository->findOneBy(array("id" => $request->query->get("id")));
        return $this->render('ManageManageBundle:FileConverter:deleteheader.html.twig',array("header" => $header));        
    }
    public function deleteHeader($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterHeaders'); 
        $header = $repository->findOneBy(array("id" => $id));        
        $em->remove($header);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterMappings'); 
        $mappings = $repository->findBy(array("headerid" => $id));
        foreach ($mappings as $mapping)
        {
            $em->remove($mapping);
        }
        $em->flush();          
    }
    public function deleteHeaderSavedAction(Request $request)
    {
        $this->deleteHeader($request->request->get("id"));
        return new Response("");
    }
    public function editMappingsAction(Request $request)
    {
        return $this->render('ManageManageBundle:FileConverter:editmappings.html.twig',array("mappings" => $this->getMappings($request->query->get("id"))));
    }
    public function getMappingsAction(Request $request)
    {
        return $this->render('ManageManageBundle:FileConverter:getmappings.html.twig',array("mappings" => $this->getMappings($request->request->get("id"))));
    }
    public function addMappingAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $mapping = new fileConverterMappings();
        $mapping->from = $request->request->get("from");
        $mapping->to = $request->request->get("to");
        $mapping->headerid = $request->request->get("headerid");
        $em->persist($mapping);
        $em->flush();
        return new Response("");
    }
    public function editMappingAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterMappings'); 
        $mapping = $repository->findOneBy(array("id" => $request->request->get("id")));          
        $mapping->from = $request->request->get("from");
        $mapping->to = $request->request->get("to");
        $em->flush();
        return new Response("");
    }   
    public function deleteMappingAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterMappings'); 
        $mapping = $repository->findOneBy(array("id" => $request->request->get("id")));          
        $em->remove($mapping);
        $em->flush();
        return new Response("");
    }       
    public function getMappings($id)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterMappings'); 
        $mappings = $repository->findBy(array("headerid" => $id),array("id" => "desc"));  
        return $mappings;
    }
    public function uploadFileAction(Request $request)
    {
        return $this->render('ManageManageBundle:FileConverter:uploadfile.html.twig');
    }
    public function downLoadFileAction(Request $request)
    {
        $file = $request->files->get("file");
        header("Cache-Control: must-revalidate");
        header("Pragma: must-revalidate");
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename=converted_'.$file->getClientOriginalName());
        header("Content-type: application/vnd.ms-excel");
        header('Content-Transfer-Encoding: binary');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterHeaders');
        $headers = $repository->findBy(array("converterid" => $request->request->get("id")));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:fileConverterMappings');
        $headerHash = array();
        foreach ($headers as $header)
        {
            $mappings = $repository->findBy(array("headerid" => $header->id));
            foreach ($mappings as $mapping)
            {
                $headerHash[$header->name][$mapping->from] = $mapping->to;
            }
        }
        $csv = array_map('str_getcsv', file($file->getRealPath()));
        $header = $csv[0];
        unset($csv[0]);
        foreach ($csv as &$row)
        {
            $rowCount = count($row);
            for ($i = 0; $i < $rowCount; $i++)
            {              
                if (isset($headerHash[$header[$i]][$row[$i]]))
                {
                    $row[$i] = $headerHash[$header[$i]][trim($row[$i])];
                }
            }
        }
        $temp = tmpfile();
        fputcsv($temp,$header);
        foreach ($csv as &$row)
        {
            fputcsv($temp,$row);
        }
        rewind($temp);
        $response =  stream_get_contents($temp);
        fclose($temp);
        return new Response($response);
    }
}
