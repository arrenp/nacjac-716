<?php

namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
class ReportStylesController extends Controller
{
    public $sections;

    public function indexAction(Request $request)
    {
        $generalMethods = $this->get('GeneralMethods');
        $this->sections = array();
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPlan');
        $defaultPlan = $repository->findOneBy(array('userid' => $request->query->get("userid") ));   
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $request->query->get("planid") ));        
        $generalMethods->addSection($this->sections,"reportsStylesHeader","Header");
        $generalMethods->addSection($this->sections,"reportsStylesFont","Font");
        $generalMethods->addSection($this->sections,"reportsInnerTable","Inner Table");
        $generalMethods->addSection($this->sections,"reportsBackground","Background"); 
                
        foreach ($this->sections as &$section)
        {
            $field = $section->field;
            $section->value = "";
            if ($defaultPlan->$field != "")
            $section->value = $defaultPlan->$field;
            if ($plan->$field != "")
            $section->value = $plan->$field;    
        }    
        
        
         
        return $this->render('ManageManageBundle:ReportStyles:index.html.twig', array('sections' => $this->sections,"planid" => $request->query->get("planid")));
    }
}
