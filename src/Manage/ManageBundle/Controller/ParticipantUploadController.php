<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Entity\accounts;
use classes\classBundle\Entity\launchToolRecipients;
use classes\classBundle\Entity\plans;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ParticipantUploadController extends Controller {
    
    protected function getFileName() {
        $session = $this->get("session");
        return 'participant_upload_'.$session->get("masterid").'.tmp';
    }
    protected function getDirName() {
        return sys_get_temp_dir();
    }
    protected function addField(array &$fields, $name, $required) {
        $fields[$name] = ['required' => $required, 'name' => $name];
    }
    protected function getMappingFields() {
        $fields = [];
        $this->addField($fields, 'firstName', true);
        $this->addField($fields, 'lastName', true);
        $this->addField($fields, 'email', true);
        $this->addField($fields, 'recordkeeperPlanid', true);
        $this->addField($fields, 'enrolled', true);
        $this->addField($fields, 'smartEnrollPartId', false);
        $this->addField($fields, 'deleted', false);
        return $fields;
    }
    protected function getMappingFieldsAsOptions() {
        $fields = $this->getMappingFields();
        $options = '';
        foreach ($fields as $value) {
            $options .= '<option value="'.$value['name'].'">'.$value['name'].'</option>';
        }
        return $options;
    }
    
    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "emailCampaigns");
        $repository = $this->getDoctrine()->getRepository(accounts::class);
        $accounts = $repository->findBy([], ['company' => 'ASC']);
        $fieldOptions = $this->getMappingFieldsAsOptions();
        return $this->render('ManageManageBundle:ParticipantUpload:index.html.twig', ['accounts' => $accounts, 'fieldOptions' => $fieldOptions]);
    }
    
    public function uploadAction(Request $request) {
        ini_set('auto_detect_line_endings', true);
        $file = $request->files->get("uploadFile")->move($this->getDirName(), $this->getFileName());
        $csv = array_map('str_getcsv', file($file->getRealPath()));
        
        $header = array_shift($csv);
        $html = $this->renderView('ManageManageBundle:ParticipantUpload:csvmapping.html.twig', ['header' => $header, 'csv' => $csv]);
        return new JsonResponse(['success' => true, 'html' => $html]);
    }
    
    public function processAction(Request $request) {
        ini_set('auto_detect_line_endings', true);
        
        $filePath = $this->getDirName() . '/' . $this->getFileName();
        if (!is_readable($filePath)) {
            return new JsonResponse(['success' => false, 'data' => "File not found"]);
        }
        
        $post = $request->request->all();
        $entityManager = $this->getDoctrine()->getManager();
        $enrolleeRespository = $this->getDoctrine()->getRepository(launchToolRecipients::class);
        $planRepository = $this->getDoctrine()->getRepository(plans::class);
        $csv = array_map('str_getcsv', file($filePath));
        $header = array_shift($csv);
        $fields = $this->getMappingFields();
        $required = array_filter($fields, function($item) {return $item['required'];});
        
        $isHeaderValid = true;
        $mappedFieldHash = [];
        foreach ($post['mappedField'] as $key => $value) {
            $from = isset($value['from']) ? $value['from'] : '';
            if ($from !== $header[$key]) {
                $isHeaderValid = false;
            }
            if (isset($value['to']) && isset($required[$value['to']])) {
                unset($required[$value['to']]);
            }
            if (isset($value['to'])) {
                $mappedFieldHash[$value['to']] = $key;
            }
        }
        

        if (!$isHeaderValid) {
            return new JsonResponse(['success' => false, 'data' => "CSV headers don't match"]);
        }
        
        if (count($required) > 0) {
            return new JsonResponse(['success' => false, 'data' => "All required fields have not been mapped"]);
        }
        
        $insertCount = 0;
        $updateCount = 0;
        $noPlanCount = 0;
        $plans = [];
        foreach ($csv as $key => $row) {
            if (count(array_filter($row)) == 0) {
                continue;
            }
            $row = array_map('trim', $row);
            $email = $row[$mappedFieldHash['email']];
            $planid = $row[$mappedFieldHash['recordkeeperPlanid']];
            
            if (!isset($plans[$planid])) {
                $plan = $planRepository->findOneBy(['planid' => $planid, 'userid' => $post['accountId'], 'type' => $post['type'], 'deleted' => 0]);
                $plans[$planid] = !is_null($plan) ? $plan->id : null;
            }
            $smartplanid = $plans[$planid];

            if ($plan === null) {
                $noPlanCount++;
                continue;
            }
            
            $recipient = $enrolleeRespository->findOneBy(['email' => $email, 'planid' => $plan->id, 'partnerid' => $post['accountId']]);
            if ($recipient === null) {
                $recipient = new launchToolRecipients();
                $recipient->contactStatus = 'NEW';
                $entityManager->persist($recipient);
                $insertCount++;
            }
            else {
                $recipient->contactStatus = 'UPDATED';
                $recipient->lastModified = time();
                $updateCount++;
            }
            
            $recipient->email = $email;
            $recipient->planid = $smartplanid;
            $recipient->partnerid = $post['accountId'];
            $recipient->enrolled = $row[$mappedFieldHash['enrolled']] ? 'Y' : 'N';
            
            foreach ($fields as $field) {
                if (isset($mappedFieldHash[$field['name']])) {
                    $recipient->{$field['name']} = $row[$mappedFieldHash[$field['name']]];
                }
            }
            
            if ($key % 500 === 0) {
                $entityManager->flush();
            }
        }
        $entityManager->flush();
        return new JsonResponse(['success' => true, 'data' => ['noPlanCount'=>$noPlanCount,'insertCount'=>$insertCount,'updateCount'=>$updateCount]]);
    }
}
