<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\accountsRoles;
use classes\classBundle\Entity\accountsRolesTypes;
use Sessions\AdminBundle\Classes\adminsession;

class RolesController extends Controller {

    public $roleTypes;
    public $investmentMessage = "Only available for Educate, Envisage, DST Accounts";

    public function indexAction() {
        $this->init();
        $generalfunctions = $this->get('generalfunctions');
        $rolelist = $generalfunctions->search("accountsRoles", "false", "false", "Roles", "ManageManageBundle:Roles:Search/rolelist.html.twig", "name,type", "roles", "inline-block", "classesclassBundle:Search:search.html.twig");
        return $this->render('ManageManageBundle:Roles:index.html.twig', array("rolelist" => $rolelist));
    }

    public function addRoleAction() {
        $this->init();
        return $this->render('ManageManageBundle:Roles:addrole.html.twig', array("roleTypes" => $this->roleTypes));
    }

    public function addRoleSavedAction() {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $name = $request->request->get("name");
        $types = explode(",", $request->request->get("types"));
        $role = new accountsRoles();
        $role->name = $name;
        $em->persist($role);
        $em->flush();
        foreach ($types as $type) {
            $roleType = new accountsRolesTypes();
            $roleType->type = $type;
            $roleType->roleid = $role->id;
            $em->persist($roleType);
            $em->flush();
        }

        return new Response("Saved");
    }

    public function editRoleAction() {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRoles');
        $role = $repository->findOneBy(array('id' => $request->query->get("id")));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRolesTypes');
        $roleTypes = $repository->findBy(array('roleid' => $request->query->get("id")));

        foreach ($roleTypes as $roleType) {
            $this->roleTypes[$roleType->type]->checked = "checked";
        }
        return $this->render('ManageManageBundle:Roles:editrole.html.twig', array("currentrole" => $role, "roleTypes" => $this->roleTypes));
    }

    public function editRoleSavedAction() {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $name = $request->request->get("name");
        $types = explode(",", $request->request->get("types"));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRoles');
        $role = $repository->findOneBy(array('id' => $request->request->get("id")));
        $role->name = $name;
        $em->flush();

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRolesTypes');
        $roleTypes = $repository->findBy(array('roleid' => $request->request->get("id")));

        foreach ($roleTypes as $type) {
            $em->remove($type);
            $em->flush();
        }

        foreach ($types as $type) {
            $roleType = new accountsRolesTypes();
            $roleType->type = $type;
            $roleType->roleid = $role->id;
            $em->persist($roleType);
            $em->flush();
        }

        return new Response("Saved");
    }

    function setPermissionsAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRoles');
        $currentrole = $repository->findOneBy(array('id' => $request->query->get("id")));
        $permissionsDashboard = $this->permissionsDashboard();
        $permissionsSupport = $this->permissionsSupport();
        $permissionsAccount = $this->permissionsAccount();
        $permissionsSettings = $this->permissionsSettings();
        $permissionsPlans = $this->permissionsPlans();
        $permissionsManage = $this->permissionsManage();
        $permissionsEmailCampaigns = $this->permissionsEmailCampaigns();
        $permissionsStats = $this->permissionsStats();
        $this->copyPermissions($permissionsDashboard, $currentrole);
        $this->copyPermissions($permissionsSupport, $currentrole);
        $this->copyPermissions($permissionsAccount, $currentrole);
        $this->copyPermissions($permissionsSettings, $currentrole);
        $this->copyPermissions($permissionsPlans, $currentrole);
        $this->copyPermissions($permissionsManage, $currentrole);
        $this->copyPermissions($permissionsEmailCampaigns, $currentrole);
        $this->copyPermissions($permissionsStats, $currentrole);
        return $this->render('ManageManageBundle:Roles:setpermissions.html.twig', array("role" => $currentrole, "permissionsDashboard" => $permissionsDashboard, "permissionsSupport" => $permissionsSupport,
                    "permissionsAccount" => $permissionsAccount, "permissionsSettings" => $permissionsSettings, "permissionsPlans" => $permissionsPlans, "permissionsManage" => $permissionsManage,  "permissionsEmailCampaigns" => $permissionsEmailCampaigns, "permissionsStats" => $permissionsStats)
        );
    }

    function deleteRoleAction() {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRoles');
        $role = $repository->findOneBy(array('id' => $request->query->get("id")));


        return $this->render('ManageManageBundle:Roles:deleterole.html.twig', array("role" => $role));
    }

    public function deleteRoleSavedAction() {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRoles');
        $role = $repository->findOneBy(array('id' => $request->request->get("id")));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsUsers');
        $accountUser = $repository->findOneBy(array('roleid' => $role->id));
        if ($accountUser == null) {
            $em->remove($role);
            $em->flush();
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:accountsRolesTypes');
            $roles = $repository->findBy(array('roleid' => $request->request->get("id")));
            foreach ($roles as $role) {
                $em->remove($role);
                $em->flush();
            }
            return new Response("Deleted");
        } else
            return new Response("One or more users assoicated with this role, cannot delete role until users have been assigned to a different role");
    }

    private function permissionsEmailCampaigns() {//list of descriptions for dashboard section of the admin
        $dashboard['permissionsEmailCampaigns']['description'] = "Email Campaigns";
        return $dashboard;
    }

    private function permissionsStats() {//list of descriptions for dashboard section of the admin
        $dashboard['permissionsStatsPlanComparison']['description'] = "Stats - Plan Comparison";
        $dashboard['permissionsStatsTickets']['description'] = "Stats - Tickets";
        $dashboard['permissionsStatsLaunchStat']['description'] = "Stats - Lunch Stat";
        $dashboard['permissionsStatsFiduciaryReport']['description'] = "Stats - Fiduciary Report";
        return $dashboard;
    }

    private function permissionsDashboard() {//list of descriptions for dashboard section of the admin
        $dashboard['permissionsDashboard']['description'] = "Dashboard";
        return $dashboard;
    }

    private function permissionsSupport() {//list of descriptions for support section of the admin
        $support['permissionsSupport']['description'] = "Support";
        return $support;
    }

    private function permissionsAccount() {//list of descriptions for account section of the admin
        $account['permissionsAccount']['description'] = "Account";
        $account['permissionsAccountVideoLibrary']['description'] = "Video Library";
        return $account;
    }

    private function permissionsNotifications() {//list of descriptions for notifications section of the admin
        $account['permissionsNotifications']['description'] = "Notifications";
        return $account;
    }

    private function permissionsSettings() {//list of descriptions for settings section of the admin

        $settings['permissionsSettingsInvestmentsFunds']['description'] = "Funds";
        $settings['permissionsSettingsInvestmentsFunds']['description2'] = $this->investmentMessage;
        $settings['permissionsSettingsInvestmentsPortfolios']['description'] = "Portfolios";
        $settings['permissionsSettingsInvestmentsPortfolios']['description2'] = $this->investmentMessage;
        $settings['permissionsSettingsDocuments']['description'] = "Documents";
        $settings['permissionsSettingsContact']['description'] = "Contact";
        $settings['permissionsSettingsMessaging']['description'] = "Messaging";
        $settings['permissionsSettingsLibrary']['description'] = "Library";
        $settings['permissionsSettingsVads']['description'] = "Vads";
        $settings['permissionsSettingsPrintMaterials']['description'] = "Promotional Materials";
        $settings['permissionsSettingsAccountLevelReporting']['description'] = "Account level reporting";
        $settings['permissionsSettingsEmail']['description'] = "Email Templates Permissions";
        $settings['permissionsSettingsDeploymentUrls']['description'] = "Deployment Urls";
        $settings['permissionsSettingsInvestorProfile']['description'] = "Investor Profile";
        $settings['permissionsSettingsAutos']['description'] = "Autos";
        $settings['permissionsSettingsInterface']['description'] = "Interface";
        return $settings;
    }

    private function permissionsPlans() {//list of descriptions for plans section of the admin
        //$this->init_investmentMessage();
        $plans['permissionsPlansPlanIdentification']['description'] = "Plan Idenficiation";
        $plans['permissionsPlansHome']['description'] = "Home";
        $plans['permissionsPlansBranding']['description'] = "Branding";
        $plans['permissionsPlansModules']['description'] = "Modules";
        //$plans['permissionsPlansInvestments']['description'] = "Investments";
        $plans['permissionsPlansInvestmentsFunds']['description'] = "Funds";
        $plans['permissionsPlansInvestmentsFunds']['description2'] = $this->investmentMessage;
        $plans['permissionsPlansInvestmentsPortfolios']['description'] = "Portfolios";
        $plans['permissionsPlansInvestmentsPortfolios']['description2'] = $this->investmentMessage;
        $plans['permissionsPlansQdia']['description'] = "QDIA";
        $plans['permissionsPlansQdia']['description2'] = "Only available if qdia is enabled for the Account";
        $plans['permissionsPlansAssociate']['description'] = "Risk Profile Configuration";
        $plans['permissionsPlansAdvice']['description'] = "Advice";
        $plans['permissionsPlansAdvice']['description2'] = "Only available if advice is enabled for the Account";
        $plans['permissionsPlansMessaging']['description'] = "Messaging";
        $plans['permissionsPlansLibrary']['description'] = "Library";
        $plans['permissionsPlansDocuments']['description'] = "Documents";
        $plans['permissionsPlansContact']['description'] = "Contact";
        $plans['permissionsPlansDeploy']['description'] = "Deploy";
        $plans['permissionsPlansEmail']['description'] = "Email";
        $plans['permissionsPlansVads']['description'] = "Vads";
        $plans['permissionsPlansPrintPromotion']['description'] = "Promotional Materials";
        $plans['permissionsPlansProfiles']['description'] = "Profiles";
        $plans['permissionsPlansAnalyticsCities']['description'] = "Analytics Cities";
        $plans['permissionsPlansAnalyticsVisitorsOverview']['description'] = "Analytics Visitors Overview";
        $plans['permissionsPlansAnalyticsPages']['description'] = "Analytics Pages";
        $plans['permissionsPlansInterface']['description'] = "Interface";
        $plans['permissionsplansNavSponsorConnect']['description'] = "Smart Connect";
        $plans['permissionsplansNavSponsorConnectList']['description'] = "Smart Connect Email List";
        $plans['permissionsPlansMedia']['description'] = "Media";
        $plans['permissionsPlansPowerView']['description'] = "PowerView";
        $plans['permissionsPlansIrio']['description'] = "Irio";
        $plans['permissionsPlansPowerViewEmailList']['description'] = "PowerView Email List";
        $plans['permissionsPlansInvestorProfile']['description'] = "Investor Profile";
        $plans['permissionsPlansRiskBasedFunds']['description'] = "Risk Based Funds";
		$plans['permissionsPlansAccessControl']['description'] = "Access Control";
        $plans['permissionsPlansOutreachHR']['description'] = "Outreach HR";
        $plans['permissionsPlansConversions']['description'] = "Conversions";
        $plans['permissionsPlansAutos']['description'] = "Autos";
        return $plans;
    }

    private function permissionsManage() {
        $manage['permissionsManageAccounts']['description'] = "Accounts";
        $manage['permissionsManageRoles']['description'] = "Roles";
        $manage['permissionsManageApi']['description'] = "Api";
        $manage['permissionsManageOutreachPlans']['description'] = "Plan Templates";
        $manage['permissionsManageOutreachVwise']['description'] = "Vwise Templates";
        $manage['permissionsManageAnalyticsProfiles']['description'] = "Profiles";
        $manage['permissionsManagePortal']['description'] = "Portal";
        $manage['permissionsManagePromoDocs']['description'] = "Promo Docs";
        $manage['permissionsManageSalesPromoDocs']['description'] = "Sales Promo Docs";
        $manage['permissionsManageMedia']['description'] = "Media";
        $manage['permissionsExportDatabase']['description'] = "Export Database";
        $manage['permissionsManageLNCLNKeys']['description'] = "Lincoln Keys";
        $manage['permissionsManageCron']['description'] = "Cron";
        $manage['permissionsReportsConversionReport']['description'] = "Conversion";
        $manage['permissionsReportsUserActivityReport']['description'] = "User Activity Report";
        $manage['permissionsReportsPerformanceReport']['description'] = "Performance Report";
        $manage['permissionsManageGroupsAccountManagers']['description'] = "Account Managers";
        $manage['permissionsManageGroupsCommunicationSpecialist']['description'] = "Communication Specialists";
        $manage['permissionsManageSponsorConnect']['description'] = "Sponsor Connect";

        $manage['permissionsManageReportsPowerViewPerformance']['description'] = "Powerview Performance Report";
        $manage['permissionsManageReportsPowerViewUserActivity']['description'] = "Powerview User Activity Report";
        $manage['permissionsManageReportsIrioPerformance']['description'] = "Irio Performance Report";
        $manage['permissionsManageReportsIrioUserActivity']['description'] = "Irio User Activity Report";
        $manage['permissionsPlansLibraryVideos']['description'] = "Manage Library Videos";
        $manage['permissionsManageFundGroups']['description'] = "Fund Groups";
        $manage['permissionsAuditLog']['description'] = "Audit Log";
        $manage['permissionsManageTranslations']['description'] = "Translations";
        $manage['permissionsManageMessagingPanes']['description'] = "Messaging Panes";
        $manage['permissionsManageApiUI']['description'] = "Api UI";
        $manage['permissionsManageSystemMessages']['description'] = "System Messages";
        $manage['permissionsManageRiskProfileCreator']['description'] = "Risk Profile Creator";
        $manage['permissionsManageXmlRawUi']['description'] = "XML Log";
        $manage['permissionsManageAmeritasFTP']['description'] = "Ameritas FTP Credentials";
        $manage['permissionsManageKitchenSink']['description'] = "Kitchen Sink";
        $manage['permissionsManageTranslationQueue']['description'] = "Translation Queue";
        $manage['permissionsManageHDMI']['description'] = "HDMI Access";
        $manage['permissionsManageReportsEmailResults']['description'] = "Email Results";
        $manage['permissionsManageSurvey']['description'] = "Survey";
        return $manage;
    }

    private function copyPermissions(&$role, $therole) { //sets radio dials for adviser permissions to check or not checked depending on adviser doctrine variabe
        foreach ($role as $key => $value)
        {
            $role[$key]['none'] = "";
            $role[$key]['read'] = "";
            $role[$key]['full'] = "";
            $role[$key][$therole->$key] = "checked";
        }
    }

    public function init() {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        $types = array("admin_master", "admin", "adviser", "vwise", "vwise_admin");
        foreach ($types as $type)
            $this->addRoleType($type);
    }

    public function addRoleType($type) {
        $this->roleTypes[$type] = new \stdClass();
        $this->roleTypes[$type]->type = $type;
        $this->roleTypes[$type]->checked = "";
    }

}

?>
