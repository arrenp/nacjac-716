<?php
namespace Manage\ManageBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sessions\AdminBundle\Classes\adminsession;
class FundsGroupsController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");        
        return $this->render('ManageManageBundle:FundsGroups:index.html.twig'); 
    }     
    public function addGroupAction()
    {
        return $this->render('ManageManageBundle:FundsGroups:addgroup.html.twig'); 
    }
    public function addGroupSavedAction(Request $request)
    {
        $FundsGroupService = $this->get("FundsGroupsService");
        $FundsGroupService->add($request->request->all());
        return new Response("");
    }
    public function editGroupAction(Request $request)
    {
        $group = $this->get("FundsGroupsService")->findOneBy(array('id' => $request->query->get("id")));
        return $this->render('ManageManageBundle:FundsGroups:editgroup.html.twig',array("group" => $group));
    }
    public function editGroupSavedAction(Request $request)
    {
        $FundsGroupService = $this->get("FundsGroupsService");
        $group = $FundsGroupService->findOneBy(array('id' => $request->request->get("id")));
        $FundsGroupService->save($group,$request->request->all()) ;
        return new Response("");
    }  
    public function deleteGroupAction(Request $request)
    {
        $group = $this->get("FundsGroupsService")->findOneBy(array('id' => $request->query->get("id")));
        return $this->render('ManageManageBundle:FundsGroups:deletegroup.html.twig',array("group" => $group));
    }   
    public function deleteGroupSavedAction(Request $request)
    {
        $FundsGroupService = $this->get("FundsGroupsService");
        $group = $FundsGroupService->findOneBy(array('id' => $request->request->get("id")));
        $FundsGroupService->save($group,array("deleted" => 1));
        return new Response("");
    }  
    public function reorderGroupAction()
    {
        return $this->render('ManageManageBundle:FundsGroups:reordergroup.html.twig',array("groups" => $this->get("FundsGroupsService")->getList()));
    }
    public function reorderGroupSavedAction(Request $request)
    {
        $this->get("FundsGroupsService")->reorder($request->request->get("ids"),array('deleted' => 0));
        return new Response("");
    }
    public function listAction(Request $request)
    {
        return $this->render('ManageManageBundle:FundsGroups:list.html.twig',array("values" => $this->get("FundsGroupsValuesService")->getByGroupId($request->query->get("id"))));
    } 
    public function listAddAction(Request $request)
    {
        $this->get("FundsGroupsValuesService")->add($request->request->all());
        return new Response("");
    }   
    public function listEditAction(Request $request)
    {
        $fundsGroupsValue = $this->get("FundsGroupsValuesService")->findOneBy(array("id" => $request->request->get("id")));
        $this->get("FundsGroupsValuesService")->save($fundsGroupsValue,$request->request->all());
        return new Response("");
    }
    public function listDeleteAction(Request $request)
    {
        $fundsGroupsValue = $this->get("FundsGroupsValuesService")->findOneBy(array("id" => $request->request->get("id")));
        $this->get("FundsGroupsValuesService")->save($fundsGroupsValue,array("deleted" => 1));
        return new Response("");
    }    
    public function reloadListAction(Request $request)
    {
        return $this->render('ManageManageBundle:FundsGroups:valuelist.html.twig',array("values" => $this->get("FundsGroupsValuesService")->getByGroupId($request->query->get("id"))));        
    }
    public function reorderListAction(Request $request)
    {
        $this->get("FundsGroupsValuesService")->reorder($request->request->get("ids"),array('deleted' => 0));
        return new Response("");
    }
}