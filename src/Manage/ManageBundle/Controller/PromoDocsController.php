<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\members;
use Sessions\AdminBundle\Classes\adminsession;

use Shared\PromoDocsBundle\Classes\promoDocs;

class PromoDocsController extends Controller
{

    public function indexAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $session->set("section", "Manage");  
        $id = $request->query->get("id", "");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsCategories');
        $currentcategory = $repository->findOneBy(array("id" => $id));

        $promoDocs = new promoDocs($this->getDoctrine());
        $promoDocsSections = $promoDocs->promoDocsSections($id);


        return $this->render("ManageManageBundle:PromoDocs:index.html.twig", array("promoDocsSections" => $promoDocsSections, "currentcategory" => $currentcategory));
    }

    public function listCategoriesAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsCategories');
        $categories = $repository->findAll();
        return $this->render("ManageManageBundle:PromoDocs:listcategories.html.twig", array("categories" => $categories));
    }

    public function additionalInfoAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();

        $id = $request->query->get("id", "");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocs');
        $currentdoc = $repository->findOneBy(array("id" => $id));


        return $this->render("ManageManageBundle:PromoDocs:additionalinfo.html.twig", array("currentdoc" => $currentdoc));
    }

    public function managePromoDocsCategoriesAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $session->set("section", "Manage");        
        $id = $request->query->get("id", "");
        $promoDocs = new promoDocs($this->getDoctrine());
        $promoDocsCategories = $promoDocs->promoDocsCategories();
        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocscategories.html.twig', array("promoDocsCategories" => $promoDocsCategories));
    }

    public function managePromoDocsCategoriesSaveCategoryOrderAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $idstring = $request->request->get("ids", "");
        $ids = explode(",", $idstring);


        for ($i = 0; $i < count($ids); $i++)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsCategories');
            $category = $repository->findOneBy(array('id' => $ids[$i]));
            $category->orderid = $i;
            $this->getDoctrine()->getManager()->flush();
        }

        return new Response("");
    }

    public function managePromoDocsCategoriesAddCategoryAction()
    {
        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocscategoriesaddcategory.html.twig');
    }

    public function managePromoDocsCategoriesEditCategoryAction()
    {

        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsCategories');
        $currentcategory = $repository->findOneBy(array('id' => $id));

        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocscategorieseditcategory.html.twig', array('currentcategory' => $currentcategory));
    }

    public function managePromoDocsCategoriesDeleteCategoryAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsCategories');
        $currentcategory = $repository->findOneBy(array('id' => $id));

        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocscategoriesdeletecategory.html.twig', array('currentcategory' => $currentcategory));
    }

    public function managePromoDocsCategoriesDeleteCategorySaveAction()
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $promoDocs = new promoDocs($em);
        $promoDocs->deleteCategory($id);
        return ($this->redirect($this->generateUrl('_manage_promo_docs_manage')));
    }

    public function managePromoDocsManageCategoryAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $promoDocs = new promoDocs($this->getDoctrine());
        $promoDocsSections = $promoDocs->promoDocsSections($id);

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsCategories');
        $category = $repository->findOneBy(array('id' => $id));


        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocsmanagecategory.html.twig', array("promoDocsSections" => $promoDocsSections, 'categoryid' => $id, 'category' => $category));
    }

    public function managePromoDocsManageCategoryAddSectionAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocsmanagecategoryaddsection.html.twig', array("categoryid" => $id));
    }

    public function managePromoDocsManageCategorySaveSectionOrderAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $idstring = $request->request->get("ids", "");
        $ids = explode(",", $idstring);

        for ($i = 0; $i < count($ids); $i++)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsSections');
            $section = $repository->findOneBy(array('id' => $ids[$i]));
            $section->orderid = $i;
            $this->getDoctrine()->getManager()->flush();
        }

        return new Response("");
    }

    public function managePromoDocsManageCategoryEditSectionAction()
    {

        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsSections');
        $currentsection = $repository->findOneBy(array('id' => $id));


        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocscategorieseditsection.html.twig', array('currentsection' => $currentsection));
    }

    public function managePromoDocsManageCategoryDeleteSectionAction()
    {

        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsSections');
        $currentsection = $repository->findOneBy(array('id' => $id));


        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocscategoriesedeletesection.html.twig', array('currentsection' => $currentsection));
    }

    public function managePromoDocsManageCategoryDeleteSectionSaveAction()
    {
        $em = $this->getDoctrine();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsSections');
        $section = $repository->findOneBy(array('id' => $id));
        $promoDocs = new promoDocs($em);
        $promoDocs->deleteSection($id);


        //$categoryid = $section->parentid;
        $redirectUrl = $this->generateUrl('_manage_promo_docs_manage_manage_category');
        $redirectUrl = $redirectUrl . '?id=' . $section->parentid;
        return ($this->redirect($redirectUrl));
    }

    public function managePromoDocsManageCategoryAddDocumentAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsSections');
        $section = $repository->findOneBy(array('id' => $id));
        $categoryid = $section->parentid;

        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocsmanagecategoryadddocument.html.twig', array("sectionid" => $id, "categoryid" => $categoryid));
    }

    public function managePromoDocsManageCategoryEditDocumentAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocs');
        $document = $repository->findOneBy(array('id' => $id));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsSections');
        $section = $repository->findOneBy(array('id' => $document->parentid));



        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsCategories');
        $category = $repository->findOneBy(array('id' => $section->parentid));

        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocsmanagecategoryeditdocument.html.twig', array("document" => $document, "section" => $section, "category" => $category));
    }

    public function managePromoDocsManageCategoryDeleteDocumentAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocs');
        $document = $repository->findOneBy(array('id' => $id));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsSections');
        $section = $repository->findOneBy(array('id' => $document->parentid));



        $repository = $this->getDoctrine()->getRepository('classesclassBundle:managePromoDocsCategories');
        $category = $repository->findOneBy(array('id' => $section->parentid));

        return $this->render('ManageManageBundle:AdminControlPanel:managepromodocsmanagecategorydeletedocument.html.twig', array("document" => $document, "section" => $section, "category" => $category));
    }

}

?>
