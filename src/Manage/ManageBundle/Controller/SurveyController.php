<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Entity\accounts;
use classes\classBundle\Entity\surveyEmployerGroups;
use classes\classBundle\Entity\SurveyEmployerPlans;
use classes\classBundle\Entity\surveyEmployers;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SurveyController extends Controller {
    
    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        
        $repository = $this->getDoctrine()->getRepository(accounts::class);
        $accounts = $repository->findBy([], ['company' => 'ASC']);
        return $this->render('ManageManageBundle:Survey:groups/index.html.twig', ['accounts' => $accounts]);
    }
    
    public function listGroupsAction(Request $request) {
        $surveyEmployerGroups = $this->getSurveyEmployerGroups($request->get("partnerid"));
        return $this->render('ManageManageBundle:Survey:groups/list.html.twig', ['surveyEmployerGroups' => $surveyEmployerGroups, 'partnerid' => $request->get('partnerid')]);
    }
    
    public function addGroupAction(Request $request) {
        $surveyEmployerGroup = new surveyEmployerGroups();
        $surveyEmployerGroup->parentId = $request->get("parentId");
        $surveyEmployerGroup->partnerId = $request->get("partnerId");
        $surveyEmployerGroups = $this->getSurveyEmployerGroups($request->get("partnerId"));
        return $this->render('ManageManageBundle:Survey:groups/edit.html.twig', ['surveyEmployerGroups' => $surveyEmployerGroups, 'surveyEmployerGroup' => $surveyEmployerGroup]);
    }
    
    public function editGroupAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(surveyEmployerGroups::class);
        $surveyEmployerGroup = $repository->find($request->get("id"));
        $surveyEmployerGroups = $this->getSurveyEmployerGroups($surveyEmployerGroup->partnerId);
        unset($surveyEmployerGroups[$request->get("id")]);
        return $this->render('ManageManageBundle:Survey:groups/edit.html.twig', ['surveyEmployerGroups' => $surveyEmployerGroups, 'surveyEmployerGroup' => $surveyEmployerGroup]);
    }
    
    public function saveGroupAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(surveyEmployerGroups::class);
        if ($request->get("action") === "add") {
            $surveyEmployerGroup = new surveyEmployerGroups();
        }
        else {
            $surveyEmployerGroup = $repository->find($request->get('id'));
        }
        $surveyEmployerGroup->description = $request->get("description");
        $surveyEmployerGroup->groupName = $request->get("groupName");
        $surveyEmployerGroup->isEmployer = (bool) $request->get("isEmployer");
        $surveyEmployerGroup->partnerId = $request->get("partnerId");
        $entityManager->persist($surveyEmployerGroup);
        $entityManager->flush();
        
        $surveyEmployerGroups = $this->getSurveyEmployerGroups($request->get("partnerId"));
        
        $subarray = $surveyEmployerGroups;
        $parentId = $request->get("parentId");
        if ($parentId && isset($surveyEmployerGroups[$parentId]) && empty($surveyEmployerGroups[$surveyEmployerGroup->id]['children'])) {
            $surveyEmployerGroup->parentId = $request->get("parentId");
            $subarray = $surveyEmployerGroups[$parentId]['children'];
        }
        $surveyEmployerGroup->displayOrder = max(array_column($subarray, 'displayOrder') + [0]) + 1;
        $entityManager->flush();
        
        return new Response("success");
    }
    
    public function deleteGroupAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(surveyEmployerGroups::class);
        $group = $repository->find($request->get("id"));
        $entityManager->remove($group);
        $entityManager->flush();
        return new Response("success");
    }
    
    public function saveGroupOrderAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(surveyEmployerGroups::class);
        $entityManager = $this->getDoctrine()->getManager();
        foreach ($request->get("group", []) as $index => $groupId) {
            $group = $repository->find($groupId);
            $group->displayOrder = $index;
        }
        $entityManager->flush();
        return new Response("success");
    }
    
    protected function getSurveyEmployerGroups($partnerid) {
        $entityManager = $this->getDoctrine()->getManager();
        
        $employersGroupsQuery = $entityManager->createQuery('SELECT p FROM classesclassBundle:surveyEmployerGroups p INDEX BY p.id WHERE p.partnerId = :partnerId ORDER BY p.displayOrder ASC');
        $employersGroupsQuery->setParameter('partnerId', $partnerid);
        $employersGroups = $employersGroupsQuery->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        $parents = array_filter($employersGroups, function($var) { return empty($var['parentId']); });
        $children = array_diff_key($employersGroups, $parents);
        
        foreach ($parents as &$parent) {
            $parent['children'] = [];
        }
        
        foreach ($children as $child) {
            $parentId = $child['parentId'];
            if (isset($parents[$parentId])) {
                $parents[$parentId]['children'][] = $child;
            }
        } 
        
        return $parents;
    }
    
    public function employersAction(Request $request) {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        
        $repository = $this->getDoctrine()->getRepository(surveyEmployerGroups::class);
        $surveyEmployerGroup = $repository->find($request->get("groupId"));
        
        return $this->render('ManageManageBundle:Survey:employers/index.html.twig', ['surveyEmployerGroup' => $surveyEmployerGroup]);
    }
    
    public function searchEmployersAction(Request $request) {
        $table = "surveyEmployers";
        $repository = $this->getDoctrine()->getRepository(surveyEmployers::class);
        $queryBuilder = $repository->createQueryBuilder('a');
        if ($request->get("groupId")) {
            $queryBuilder->where('a.employerGroupsTableId = :groupId')->setParameter('groupId', $request->get("groupId"));
        }
        $result = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder($table,["a.id","a.employerName"],$request->query->all(),$queryBuilder);
        
        $data = [];
        foreach ($result['rows'] as $row)
        {
            $data[] = [
                $row->id,
                $row->employerName,
                $this->renderView('ManageManageBundle:Survey:employers/actions.html.twig', ['employer' => $row])
            ];
        }
        $response = $result['response'];
        $response['data'] = $data;
        return new JsonResponse($response);
    }
    
    public function addEmployerAction(Request $request) {
        $employer = new surveyEmployers();
        $employer->employerGroupsTableId = $request->get("groupId");
        return $this->render('ManageManageBundle:Survey:employers/edit.html.twig', ['employer' => $employer]);
    }
    
    public function editEmployerAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(surveyEmployers::class);
        $employer = $repository->find($request->get("employerId"));
        return $this->render('ManageManageBundle:Survey:employers/edit.html.twig', ['employer' => $employer]);
    }
    
    public function deleteEmployerAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(surveyEmployers::class);
        $employer = $repository->find($request->get("employerId"));
        $entityManager->remove($employer);
        $entityManager->flush();
        return new Response("success");
    }
    
    public function saveEmployerAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        if ($request->get("action") === "add") {
            $surveyEmployer = new surveyEmployers();
            $entityManager->persist($surveyEmployer);
        }
        else {
            $repository = $this->getDoctrine()->getRepository(surveyEmployers::class);
            $surveyEmployer = $repository->find($request->get("id"));
        }
        $surveyEmployer->employerName = $request->get("employerName");
        $surveyEmployer->employerGroupsTableId = $request->get("employerGroupsTableId");
        $entityManager->flush();
        return new Response("success");
    }
    
    public function plansAction(Request $request) {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        
        $repository = $this->getDoctrine()->getRepository(surveyEmployers::class);
        $surveyEmployer = $repository->find($request->get("employerId"));
        
        return $this->render('ManageManageBundle:Survey:plans/index.html.twig', ['surveyEmployer' => $surveyEmployer]);
    }
    
    public function searchPlansAction(Request $request) {
        $table = "SurveyEmployerPlans";
        $repository = $this->getDoctrine()->getRepository(SurveyEmployerPlans::class);
        $queryBuilder = $repository->createQueryBuilder('a');
        $queryBuilder->where('a.surveyEmployerId = :employerId')->setParameter('employerId', $request->get("employerId"));
        $result = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder($table,["a.id","a.type","a.planid","a.county","a.state"],$request->query->all(),$queryBuilder);
        
        $data = [];
        foreach ($result['rows'] as $row)
        {
            $data[] = [
                $row->id,
                $row->type,
                $row->planid,
                $row->county,
                $row->state,
                $this->renderView('ManageManageBundle:Survey:plans/actions.html.twig', ['plan' => $row])
            ];
        }
        $response = $result['response'];
        $response['data'] = $data;
        return new JsonResponse($response);
    }
    
    public function addPlanAction(Request $request) {
        $plan = new SurveyEmployerPlans();
        $plan->surveyEmployerId = $request->get("employerId");
        return $this->render('ManageManageBundle:Survey:plans/edit.html.twig', ['plan' => $plan]);
    }
    
    public function editPlanAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(SurveyEmployerPlans::class);
        $plan = $repository->find($request->get("planId"));
        return $this->render('ManageManageBundle:Survey:plans/edit.html.twig', ['plan' => $plan]);
    }
    
    public function deletePlanAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(SurveyEmployerPlans::class);
        $plan = $repository->find($request->get("planId"));
        $entityManager->remove($plan);
        $entityManager->flush();
        return new Response("success");
    }
    
    public function savePlanAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        if ($request->get("action") === "add") {
            $plan = new SurveyEmployerPlans();
            $entityManager->persist($plan);
        }
        else {
            $repository = $this->getDoctrine()->getRepository(SurveyEmployerPlans::class);
            $plan = $repository->find($request->get("id"));
        }
        $plan->surveyEmployerId = $request->get("surveyEmployerId");
        $plan->type = $request->get("type");
        $plan->planid = $request->get("planid");
        $plan->county = $request->get("county");
        $plan->state = $request->get("state");
        $entityManager->flush();
        return new Response("success");
    }
}
