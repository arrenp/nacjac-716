<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Entity\HubSpotAlertRecipients;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HubSpotAlertRecipientsController extends Controller {
    
    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "emailCampaigns");
        
        return $this->render('ManageManageBundle:HubSpotAlertRecipients:index.html.twig');
    }
    
    public function saveAction(Request $request) {
        $id = $request->get("recipientId");
        $entityManager = $this->getDoctrine()->getManager();
        if (!empty($id)) {
            $repository = $this->getDoctrine()->getRepository(HubSpotAlertRecipients::class);
            $recipient = $repository->find($id);
        }
        else {
            $recipient = new HubSpotAlertRecipients();
            $entityManager->persist($recipient);
        }
        $recipient->emailAddress = $request->get("emailAddress");
        $entityManager->flush();
        return new JsonResponse(['success' => true]);
        
    }
    
    public function deleteAction(Request $request) {
        $id = $request->get("recipientId");
        $repository = $this->getDoctrine()->getRepository(HubSpotAlertRecipients::class);
        $entityManager = $this->getDoctrine()->getManager();
        $recipient = $repository->find($id);
        $entityManager->remove($recipient);
        $entityManager->flush();
        
        return new JsonResponse(['success' => true]);
    }
    
    public function searchAction(Request $request) {
        $types = explode(",", $request->get("types"));
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:HubSpotAlertRecipients");
        
        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = $repository->createQueryBuilder('r');
        $filteredCountQuery = $repository->createQueryBuilder('r')->select('COUNT(DISTINCT r.id)');
        $totalCountQuery = $repository->createQueryBuilder('r')->select('COUNT(DISTINCT r.id)');
        
        $search = $request->get("search");
        if (isset($search['value']) && $search['value'] !== '') {
            $query->where('r.emailAddress LIKE :emailAddress')->setParameter('emailAddress', '%'.$search['value'].'%');
            $filteredCountQuery->where('r.emailAddress LIKE :emailAddress')->setParameter('emailAddress', '%'.$search['value'].'%');
        }
        
        foreach ($request->get("order") as $val) {
            $query->addOrderBy($types[$request->get("columns")[$val["column"]]["data"]], $val["dir"]);
        }
        
        $query->setFirstResult($request->get("start"));
        $query->setMaxResults($request->get("length"));
        
        
        $recipients = $query->getQuery()->getResult();
        $data = [];
        
        foreach ($recipients as $recipient) {
            $data[] = [
                $recipient->emailAddress,
                '<span title="Edit" aria-label="Edit" class="glyphicon glyphicon-pencil" onclick="updateRecipient('.$recipient->id.',\''.$recipient->emailAddress.'\')"></span><span title="Delete" aria-label="Delete" class="glyphicon glyphicon-remove-circle text-danger" onclick="deleteRecipient('.$recipient->id.')"></span>'
            ];
        }
        
        $recordsFiltered = $filteredCountQuery->getQuery()->getSingleScalarResult();
        $recordsTotal = $totalCountQuery->getQuery()->getSingleScalarResult();

        $results = array(
            "draw" => (int) $request->get("draw"),
            "recordsTotal" => (int) $recordsTotal,
            "recordsFiltered" => (int) $recordsFiltered,
            "data" => $data
        );

        return new JsonResponse($results); 
    }
}
