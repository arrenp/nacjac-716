<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Sessions\AdminBundle\Classes\adminsession;

class IndexController extends Controller
{
    public function indexAction()
    {

    	$session = $this->get('adminsession');
    	$session->set("section","Manage");
		
    	return $this->render('ManageManageBundle:Index:index.html.twig');
    }

}
?>
