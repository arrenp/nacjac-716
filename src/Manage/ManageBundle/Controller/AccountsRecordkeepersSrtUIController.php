<?php

namespace Manage\ManageBundle\Controller;

use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use classes\classBundle\Entity\accountsRecordkeepersSrt;

class AccountsRecordkeepersSrtUIController extends Controller {

    private $fields = [
        'id' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
        'userid' => ['orderable'=>true,'searchable'=>true,'isPulldown' => true],
        'siteToken' => ['orderable'=>true,'searchable'=>true,'isPulldown' => false],
        'userName' => ['orderable'=>true,'searchable'=>true,'isPulldown' => false],
        'password' => ['orderable'=>true,'searchable'=>true,'isPulldown' => false],
        'type' => ['orderable'=>true,'searchable'=>true,'isPulldown' => false],
        'url' => ['orderable'=>false,'searchable'=>true,'isPulldown' => false],
        'actions' => ['orderable'=>false,'searchable'=>false],
    ];

    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        $connection = $this->container->get("database_connection");
        $sql = "select distinct(userid) from accountsRecordkeepersSrt order by userid";
        $options = $connection->fetchAll($sql);
        $optionsArray = array();
        foreach ($options as $value) {
            $optionsArray[] = $value['userid'];
        }

        $this->fields['userid']['options'] = $optionsArray;
        return $this->render("ManageManageBundle:AccountsRecordkeepersSrtUI:index.html.twig", ['fields' => $this->fields]);
    }

    public function searchAction(Request $request) {
        $connection = $this->get("database_connection");
        $req = $request->request->all();
        $searchstring = " WHERE 1 = 1 ";
        $parameters = [];
        $encryptedFields = array('userName', 'password');

        foreach ($this->fields as $name => $field) {
            if (!empty($req[$name]) && $field['searchable']) {
                if (in_array($name, $encryptedFields)) {
                    $searchstring .= " AND $name LIKE AES_ENCRYPT(:$name,UNHEX('" . $this->container->getParameter('HEX_AES_KEY'). "'))";
                    $parameters[":$name"] = $req[$name];
                } else {
                    $searchstring .= " AND $name LIKE :$name ";
                    $parameters[":$name"] = '%' . $req[$name] . '%';
                }
            }
        }

        $orderByArr = [];
        foreach ($req["order"] as $val) {
            $orderBy = $req["columns"][$val["column"]]["data"];
            if (isset($this->fields[$orderBy]) && $this->fields[$orderBy]['orderable']) {
                $dir = strtolower($val["dir"]) === "desc" ? "desc" : "asc";
                $orderByArr[] = "$orderBy $dir";
            }
        }
        $orderByArr[] = "a.id ASC";
        $orderByString = " ORDER BY " . implode(", ", $orderByArr);

        $queryCountString = "SELECT COUNT(a.id) FROM accountsRecordkeepersSrt a $searchstring";
        $recordsTotal = $connection->fetchColumn($queryCountString, $parameters);

        $queryFilteredCountString = "SELECT COUNT(a.id) FROM accountsRecordkeepersSrt a $searchstring";
        $recordsFiltered = $connection->fetchColumn($queryFilteredCountString, $parameters);

        $queryString = sprintf("SELECT *, AES_DECRYPT(userName, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."')) as userName, AES_DECRYPT(password, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."')) as password FROM accountsRecordkeepersSrt a $searchstring $orderByString LIMIT %d, %d", (int) $req["start"], (int) $req['length']);
        $items = $connection->fetchAll($queryString, $parameters);

        foreach ($items as &$item) {
            $item['actions'] = '<span class = "glyphicon glyphicon-edit" title = "edit" onclick = "ajaxModal(\'/admin/manage/accountsRecordkeepersSrt/edit?id=' . $item['id'] . '\',\'Edit Entry\')"></span>
	        <span class = "glyphicon glyphicon-trash" title = "delete" onclick = "ajaxModal(\'/admin/manage/accountsRecordkeepersSrt/delete?id=' . $item['id'] . '\',\'Delete Entry\')"></span></a>';
        }
        return new JsonResponse([
            "draw" => (int) $req["draw"],
            "recordsTotal" => (int) $recordsTotal,
            "recordsFiltered" => (int) $recordsFiltered,
            "data" => $items
        ]);
    }

    public function addAction() {
        $accounts = $this->getAccountList();
        return $this->render("ManageManageBundle:AccountsRecordkeepersSrtUI:add.html.twig", array('accounts' => $accounts));
    }

    public function addSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $params = $request->request->all();

        $entry = new accountsRecordkeepersSrt;
        foreach ($params as $key => $value) {
            $entry->$key = $value;
        }
        $em->persist($entry);
        $em->flush();
        return new Response('hi');
    }

    public function editAction(Request $request) {
        $id = $request->query->get('id');
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:accountsRecordkeepersSrt");
        $edit = $repository->findOneBy(array('id' => $id));
        $accounts = $this->getAccountList();
        return $this->render("ManageManageBundle:AccountsRecordkeepersSrtUI:edit.html.twig", array('edit' => $edit, 'accounts' => $accounts));
    }

    public function editSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:accountsRecordkeepersSrt");
        $params = $request->request->all();

        $entry = $repository->findOneBy(array('id' => $params['id']));
        foreach ($params as $key => $value) {
            $entry->$key = $value;
        }
        $em->persist($entry);
        $em->flush();
        return new Response('hi');
    }

    public function deleteAction(Request $request) {
        $id = $request->query->get('id');
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:accountsRecordkeepersSrt");
        $delete = $repository->findOneBy(array('id' => $id));
        return $this->render("ManageManageBundle:AccountsRecordkeepersSrtUI:delete.html.twig", array('delete' => $delete));
    }

    public function deleteSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:accountsRecordkeepersSrt");
        $id = $request->request->get('id');

        $entry = $repository->findOneBy(array('id' => $id));
        $em->remove($entry);
        $em->flush();
        return new Response('hi');
    }

    public function getAccountList() {
        $em = $this->getDoctrine()->getEntityManager();
        $qb = $em->getRepository("classesclassBundle:accounts")->createQueryBuilder('a');
        $accounts = $qb->select('a')
            ->select('a.id, a.company')
            ->getQuery()
            ->getResult();

        return $accounts;
    }
}