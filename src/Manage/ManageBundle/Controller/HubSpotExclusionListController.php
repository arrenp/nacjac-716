<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Entity\accounts;
use classes\classBundle\Entity\plans;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HubSpotExclusionListController extends Controller {
    
    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "emailCampaigns");
        return $this->render('ManageManageBundle:HubSpotExclusionList:index.html.twig');
    }
    
    public function searchAccountsAction() {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        /* @var $queryBuilder QueryBuilder */
        $queryBuilder = $repository->createQueryBuilder('accounts')
            ->select('accounts.id, accounts.partnerid, count(plans.id) as plansCount, accounts.isExcludedFromHubSpot')
            ->distinct()
            ->leftJoin('classesclassBundle:plans', 'plans', 'WITH', 'plans.userid = accounts.id and plans.isExcludedFromHubSpot=1')
            ->groupBy('accounts.id')
            ->getQuery();
        $accounts = $queryBuilder->getResult();
        return new JsonResponse(["data" => $accounts]);
    }
    
    public function searchPlansAction(Request $request) {
        $planRepository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        /* @var $queryBuilder QueryBuilder */
        $queryBuilder = $planRepository->createQueryBuilder('plans')
            ->select('plans.id, plans.planid, plans.isExcludedFromHubSpot')
            ->where('plans.userid = :userid')
            ->andWhere('plans.deleted = 0')
            ->setParameter('userid', $request->get("userid"))
            ->distinct()
            ->getQuery();
        $plans = $queryBuilder->getResult();
        
        $accountRepository = $this->getDoctrine()->getRepository(accounts::class);
        $account = $accountRepository->find($request->get("userid"));
        
        return new JsonResponse(["data" => $plans, "isExcludedFromHubSpot" => $account->isExcludedFromHubSpot, "partnerid" => $account->partnerid]);
    }
    
    public function updatePlanAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(plans::class);
        $plan = $repository->find($request->get("id"));
        $plan->isExcludedFromHubSpot = (bool) $request->get("value");
        $entityManager->flush();
        return new Response("success");
    }
    
    public function updateAccountAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(accounts::class);
        $account = $repository->find($request->get("id"));
        $account->isExcludedFromHubSpot = (bool) $request->get("value");
        $entityManager->flush();
        return new Response("success");
    }
    
    public function clearExclusionsAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(plans::class);
        $plans = $repository->findBy(['userid' => $request->get("id"), 'isExcludedFromHubSpot' => true]);
        foreach ($plans as $plan) {
            $plan->isExcludedFromHubSpot = false;
        }
        $accountRepository = $this->getDoctrine()->getRepository(accounts::class);
        $account = $accountRepository->find($request->get("id"));
        $account->isExcludedFromHubSpot = false;
        $entityManager->flush();
        return new Response("success");
    }
    
}
