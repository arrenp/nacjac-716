<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
set_time_limit(0);
ini_set("memory_limit","2000M");
class ReportsController extends Controller
{
    //profiles
    public function ConversionReportAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Stats");
        return $this->render('ManageManageBundle:Reports:Conversion/index.html.twig'); 
    }

    public function ConversionGetReportAction(Request $request)
    {        
        $report = $this->ConversionGetReport($request);   
        return $this->render('ManageManageBundle:Reports:Conversion/report.html.twig',array("report" => $report));
    }
     
    public function ConversionGetReport($request)
    {        
        $connection = $this->get('doctrine.dbal.default_connection');
        $return = array();
        $parameters = $request->query->all();
        $counter = 0;
        
        if (isset($parameters['adviserid']))
        {
            $accountsUsers = $connection->executeQuery("SELECT firstname,email,lastname,id,userid, allplans FROM accountsUsers WHERE id=".$parameters['adviserid'])->fetch();
            if ($accountsUsers != null)
            {
                if ($accountsUsers['allplans'] == 0)
                {
                    $advisersPlansArray =  $connection->fetchAll("SELECT accountsUsersPlans.planid as planid FROM accountsUsersPlans LEFT JOIN plans on accountsUsersPlans.planid = plans.id WHERE accountsUsersPlans.accountsUsersId = ".$parameters['adviserid']);  
                    $advisersPlans = "";
                    
                    foreach ($advisersPlansArray as $advisersPlan)
                    {
                        if ($counter > 0)
                        $advisersPlans = $advisersPlans.",";
                        $advisersPlans   = $advisersPlans.$advisersPlan['planid'];
                        $counter++;
                    }
                }
                else
                $parameters['userid'] =  $accountsUsers['userid'];
            }
            $return['title'] = $accountsUsers['firstname']." ".$accountsUsers['lastname']." ".$accountsUsers['email'];
        }
        else if (isset($parameters['planid']))
        {
            $plan = $connection->executeQuery("SELECT * FROM plans WHERE id = ".$parameters['planid'])->fetch();
            $return['title'] = $plan['name']." ".$plan['planid'];
        }
        else if (isset($parameters['userid']))
        {
            $account = $connection->executeQuery("SELECT * FROM accounts WHERE id = ".$parameters['userid'])->fetch();
            $return['title'] = $account['partnerid']." ".$account['company'];
        }     
        else 
        $return['title'] = "Vwise";
        
        $queries['planBasics']['name'] = "Plan Basics";
        $queries['retirementNeeds']['name'] = "Retirement Needs";
        $queries['riskProfile']['name'] = "Risk Profile";
        $queries['investments']['name'] = "Investments";
        $queries['contributions']['name'] = "Contributions";
      
        $queries['planBasics']['participantquery'] =  "SELECT COUNT(distinct(profiles.uniqid)) as count FROM profiles  LEFT JOIN videoStatsSpe ON profiles.participantid = videoStatsSpe.participantid WHERE videoStatsSpe.videoPlayed LIKE '%0208%'";
        $queries['retirementNeeds']['participantquery'] = "SELECT COUNT(distinct(profiles.uniqid)) as count FROM profiles LEFT join profilesRetirementNeeds on profiles.id = profilesRetirementNeeds.profileid WHERE  profilesRetirementNeeds.yearsLiveInRetirement is not null";
        $queries['riskProfile']['participantquery'] = "SELECT COUNT(distinct(profiles.uniqid)) as count FROM profiles LEFT JOIN profilesRiskProfile on profiles.id = profilesRiskProfile.profileid WHERE profilesRiskProfile.name in ('Aggressive','Conservative','Conservative Plus','Moderate','Moderate Plus','Moderately Aggressive','Moderately Conservative')";
        $queries['investments']['participantquery'] = "SELECT COUNT(distinct(profiles.uniqid)) as count FROM profiles LEFT JOIN profilesInvestments on profiles.id = profilesInvestments.profileid WHERE profilesInvestments.id is not null";
        $queries['contributions']['participantquery'] = "SELECT COUNT(distinct(profiles.uniqid)) as count FROM profiles LEFT JOIN profilesContributions on profiles.id = profilesContributions.profileid WHERE profilesContributions.id is not null";
        foreach ($queries as &$query)
        {
            if (isset($parameters['startDate']))            
            $query['participantquery'] = $query['participantquery']." AND DATE(profiles.reportDate) >= '".$parameters['startDate']."' ";
            if (isset($parameters['endDate']))            
            $query['participantquery'] = $query['participantquery']." AND DATE(profiles.reportDate) <= '".$parameters['endDate']."' ";
            if (isset($parameters['planid'])) 
            $query['participantquery'] = $query['participantquery']." AND profiles.planid = '".$parameters['planid']."' ";   
            if (isset($parameters['userid'])) 
            $query['participantquery'] = $query['participantquery']." AND profiles.userid = '".$parameters['userid']."' ";
            if (isset($advisersPlans))
            $query['participantquery'] = $query['participantquery']." AND profiles.planid IN (".$advisersPlans.")";
            $query['profilequery'] = str_replace("COUNT(distinct(profiles.uniqid))","COUNT(*)",$query['participantquery']);
            $query['participantqueryresult'] = $connection->executeQuery($query['participantquery'])->fetch()['count'];
            $query['profilequeryresult'] = $connection->executeQuery($query['profilequery'])->fetch()['count'];
        }
        $return['queries'] = $queries;
        if (isset($advisersPlans))
        $return['advisersPlans'] = $advisersPlans;
        $return['parameters'] = $parameters;
        return $return;
    }
    public function UserActivityReportAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Stats");
        return $this->render('ManageManageBundle:Reports:UserActivity/index.html.twig'); 
    }
    public function UserActivityReportGetReportAction(Request $request)
    {
        $parameters = $request->query->all();
        $dateString = $this->getDateSql($parameters);
        $connection = $this->get('doctrine.dbal.default_connection');
        $selectPlanFieldsSql = "SELECT plans.id as smartplanid,plans.name as name, plans.planid as planid, plans.providerLogoImage as providerLogoImage ";
        $reports = array();
        if (isset($parameters['adviserid']))
        {              
            $accountsUsers = $connection->executeQuery("SELECT userid,allplans FROM accountsUsers WHERE id=".$parameters['adviserid'])->fetch();
            if ($accountsUsers != null)
            {
                if ($accountsUsers['allplans'] == 0)
                $advisersPlans =  $connection->fetchAll($selectPlanFieldsSql."FROM accountsUsersPlans LEFT JOIN plans on accountsUsersPlans.planid = plans.id  LEFT JOIN profiles ON accountsUsersPlans.planid = profiles.planid WHERE accountsUsersPlans.accountsUsersId = ".$parameters['adviserid']." AND profiles.id is not null ".$dateString." AND plans.id is not null group by plans.id"); 
                else
                $advisersPlans =  $connection->fetchAll($selectPlanFieldsSql."FROM plans LEFT JOIN profiles ON plans.id = profiles.planid  WHERE plans.userid = ".$accountsUsers['userid']." AND profiles.id is not null ".$dateString." group by plans.id" ); 
            }
            foreach ($advisersPlans as $plan)
            {
                $planid = $plan['smartplanid'];
                $parameters['planid'] = $planid;
                $this->addReport($reports,$this->UserActivityReport($parameters),$plan);               
            }
        }
        elseif (isset($parameters['userid'])) {
            $accountsPlans = $connection->fetchAll($selectPlanFieldsSql."FROM plans LEFT JOIN profiles ON plans.id = profiles.planid WHERE plans.userid = :userid AND profiles.id is not null group by plans.id", [':userid' => $parameters['userid']]);
            foreach ($accountsPlans as $plan) {
                $parameters['planid'] = $plan['smartplanid'];
                $this->addReport($reports,$this->UserActivityReport($parameters),$plan);
            }
        }
        else
        {
            $plan = $connection->executeQuery($selectPlanFieldsSql."FROM plans LEFT JOIN profiles ON plans.id = profiles.planid WHERE plans.id = ".$parameters['planid']." AND profiles.id is not null  group by plans.id")->fetch();
            $this->addReport($reports,$this->UserActivityReport($parameters),$plan);
        }
        if ($parameters['type']  == "excel")
        {
            return $this->irioExcelReport($reports);
        }    
        else
        {
            $reportCount = count($reports);
            for ($i = 0; $i < $reportCount;$i++)
            {
                $this->printReports($reports[$i]['data'],$reports[$i]['plan']);
            }
        }
        return new Response("");
    }
    public function addReport(&$reports,$data,$plan)
    {
        $report = array();
        $report['data'] = $data;
        $report['plan'] = $plan;
        $report['data']['existingParticipants']['description'] = "Existing Participants";
        $report['data']['newEnrolles']['description'] = "New Enrollees";
        $report['data']['participantsNearingRetirement']['description'] = "Participants Nearing Retirement";
        $report['data']['participantsWithOutsideAssets']['description'] = "Participants With Outside Assets";
        $reports[] = $report;
    }
    public function irioExcelReport($reports, $savePath = null)
    {
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->setActiveSheetIndex(0);
        $number = 1;
        $styleArrayPlanHeader = array(
            'font'  => array(
            'size'  => 14,
            'name'  => 'Calibri',
        ) ,
        'fill' => array(
            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'fed86f')
         )
         );
       
        $styleArrayHeader = $styleArrayPlanHeader;
        $styleArrayHeader['font']['size'] = 11;
        $styleArrayHeader['font']['bold'] = true;
           
           
        $styleArray = $styleArrayHeader;
        $styleArray['font']['bold'] = false;
        $styleArray['borders']['allborders'] = array(
             'style' => \PHPExcel_Style_Border::BORDER_THIN,
             'color' => array('rgb' => '000000')                              
        );
        unset($styleArray['fill']);
        
        foreach ($reports as $report)
        {
            $planHeaderSet = false;
            
            foreach ($report['data'] as $key => $section)
            {               
                if (isset($section[0]['header']))
                {
                    $header = $section[0]['header'];
                    unset($section[0]['header']);
                }
                if (isset($section['description']))
                {
                    $description = $section['description'];
                    unset($section['description']);
                } 
                if (is_array($section) && count($section[0]) > 0)
                {
                    if (!$planHeaderSet)
                    {
                        $letter = "A";
                        $phpExcelObject->setActiveSheetIndex(0)->setCellValue($letter.$number, $report['plan']['name']);
                        $letter = "B";
                        $phpExcelObject->setActiveSheetIndex(0)->setCellValue($letter.$number, $report['plan']['planid']);                        
                        $number++;
                    }

                    $letter = "A";
                    $phpExcelObject->setActiveSheetIndex(0)->setCellValue($letter.$number, $description);
                    $number++;
                    foreach($header as $key => $value)
                    {
                        $phpExcelObject->setActiveSheetIndex(0)->setCellValue($letter.$number, $key);
                        $phpExcelObject->getActiveSheet()->getStyle($letter.$number)->applyFromArray($styleArrayHeader);
                        $previousLetter = $letter;
                        $letter++;
                        $keys[] = $value;
                    }
                    $phpExcelObject->getActiveSheet()->getStyle("A".($number-1).":".$previousLetter.($number-1))->applyFromArray($styleArrayHeader);
                    if (!$planHeaderSet)
                    {
                        $phpExcelObject->getActiveSheet()->getStyle("A".($number-2).":".$previousLetter.($number-2))->applyFromArray($styleArrayPlanHeader);
                        $planHeaderSet = true;
                    }
                    $number++; 
                    $letter = "A";
                    foreach ($section as $key => $row)
                    {
                        foreach ($keys as $key)
                        {
                            if (!is_array($row[$key]))
                            {
                                $phpExcelObject->setActiveSheetIndex(0)->setCellValue($letter.$number, $row[$key]);
                                $phpExcelObject->getActiveSheet()->getStyle($letter.$number)->applyFromArray($styleArray);
                                $letter++;
                            }
                        }
                        $letter = "A";
                        $number++;
                    }
                    $number = $number + 5;
                }
                unset($keys);
            }
        }
        for ($letter = "A"; $letter <= "Z"; $letter++)
        {
            $phpExcelObject->getActiveSheet()->getColumnDimension($letter)->setAutoSize(true);
        }
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        if (!empty($savePath)) {
            $writer->save($savePath);
            return;
        }
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'UserActivityReport.xlsx'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
        return $response;
    }
    public function setConcatLength(&$connection)
    {
        $count = $connection->executeQuery("SELECT COUNT(id) as count FROM plans")->fetch()['count'];
        $characterLength = strlen((string)$connection->executeQuery("SELECT MAX(id) as max  FROM plans")->fetch()['max']) + 1;
        $concatLength = $count * $characterLength;

        $connection->executeQuery("SET SESSION group_concat_max_len = ".$concatLength);        
    }
    public function printReports(&$report,$plan)
    {   
        $logo = $plan['providerLogoImage'];
        if ($logo != "")
        $logo = '<div><img src = "'.$logo.'"  style = "max-width:250px" /></div>';
        $valid = (count($report['existingParticipants'][0]) > 1 || count($report['newEnrolles'][0]) > 1 || count($report['participantsNearingRetirement'][0]) > 1 ||  count($report['existingParticipants'][0]) > 1);
        if ($valid)
        {
            echo "<h4><strong>Plan Name</strong> ".$plan['name'].' <!--comma--> <strong style = "margin-left:20px" >Planid</strong> '.$plan['planid'].' <!--comma--> <strong style = "margin-left:20px">Smartplanid</strong> '.$plan['smartplanid']."</h4>".$logo;
            $this->printReport($report['existingParticipants'],"Existing Participants");
            $this->printReport($report['newEnrolles'],"New Enrollees");  
            $this->printReport($report['participantsNearingRetirement'],"Participants Nearing Retirement");
            $this->printReport($report['participantsWithOutsideAssets'],"Participants With Outside Assets");
            echo "<!--newline-->";
        }    
    }
    public function printReport($report,$reportName)
    {
        if (isset($report['description']))
        {
            unset($report['description']);
        }
        if (count($report[0]) == 1)
        return 0;
        $counter = 0;
        $keys = array();
        
        
        echo "<strong>".$reportName."</strong> <!--newline-->";
        echo '<table class = "table table-striped">';
        
        foreach ($report as $key => $row)
        {
            if ($counter == 0)
            {
                
                echo "<thead><tr>";    
                foreach ($row['header'] as $key => $value)
                {
                    echo "<th>".$key."</th>";
                    $keys[] = $value;
                }
                echo "</tr></thead>";
            }
            echo "<tr>";
            
            foreach ($keys as $key)
            {

                echo "<td>".$row[$key]."</td>";
                
            }
            echo "</tr>";
            $counter++;
        } 
        echo "</table>";
    }
    public function UserActivityReport($parameters)
    {
        $return = array();
        $return['existingParticipants'] = array();
        $return['newEnrolles'] = array();
        $return['participantsNearingRetirement'] = array();
        $return['participantsWithOutsideAssets'] = array();
        $commonHeader = array("First Name" => "firstName","Last Name" => "lastName","Email" => "email","Age" => "age", "Salary" => "salary");
        $defHeader = array("Deferral Pre-Application" => "preDef","Deferral Post Application" => "postDef","Deferral Rate Change" => "changeDef");
        $return['existingParticipants'][0]['header'] = array_merge($commonHeader,$defHeader,array("Balance" => "planBalance","Investment Type" => "investmentType","Meeting / Exceeding Employer Match" => "match"));
        $return['newEnrolles'][0]['header'] = array_merge($commonHeader,array("Annual Deferral" => "deferral_rate","Investment Type" => "investmentType","Meeting / Exceeding Employer Match" => "match"));
        $return['participantsNearingRetirement'][0]['header'] = array_merge($commonHeader,$defHeader,array("Outside Assets" => "outsideAssets","Balance" => "planBalance","Investment Type" => "investmentType"));
        $return['participantsWithOutsideAssets'][0]['header'] = array_merge($commonHeader,array("Outside Assets" => "outsideAssets","Type Bonds" =>"assetType_bonds","Type Cert. of Dep." => "assetType_certificate_of_deposit"
        ,"Type Former Retirement Acct" => "assetType_former_retirement_account","Type IRA" => "assetType_Individual_retirement_account","Type Mutual Funds: Bonds" => "assetType_mutual_funds_bonds"
        ,"Type Mutual Funds: Stocks" => "assetType_mutual_funds_stocks","Type Money Market Fd" =>"assetType_money_market_funds","Type Stocks" =>"assetType_mutual_funds_stocks","Not Specified" => "assetType_not_specified"));
        $connection = $this->get('doctrine.dbal.default_connection');
        $return['age'] = 58;
        if (isset($parameters['age']))
        $return['age'] = $parameters['age'];
 
        if (isset($parameters['planid']))
        {
            $this->generateProfilesAndUsers($parameters,$return,$connection);
            if (isset($return['profiles']))
            {
                $this->existingParticipants($return['profiles'][$parameters['planid']],$return,$connection);
                $this->newEnrolles($return['profiles'][$parameters['planid']],$return,$connection);
                $this->participantsNearingRetirement($return['profiles'][$parameters['planid']],$return,$connection);
                $this->participantsWithOutsideAssets($return['profiles'][$parameters['planid']],$return,$connection);
            }
        }
        
        return $return;
    }
    function generateProfilesAndUsers($parameters,&$return,&$connection)
    {
        $profiles = $this->get('profiles.service');
        //$vars = "";
        $varString = "";
        $varsArray = array("profiles.uniqid","profilesContributions.preCurrent","profilesContributions.rothCurrent","profiles.currentBalance"
        ,"profilesRetirementNeeds.currentYearlyIncome","profilesInvestments.type","profilesContributions.pre","profilesContributions.roth","profilesContributions.deferralPercent","profilesContributions.deferralDollar"
        ,"profilesRetirementNeeds.otherAssets","profilesRetirementNeeds.assetTypes","plansModules.matchingPercent","plansModules.secondaryMatchingPercent","plansModules.matchingCap","plansModules.secondaryMatchingCap","profilesContributions.mode","profilesContributions.currentYearlyIncome","profilesContributions.paidFrequency");
        $dateString = $this->getDateSql($parameters);
        foreach ($varsArray as $var) {
            $varString = $varString . "," . $var . " as " . str_replace(".", "_", $var);
        }
        $sql = "SELECT ".$profiles->generateQueryFields("participants",1,0).$varString."  FROM profiles LEFT JOIN participants ON participants.id = profiles.participantid LEFT JOIN profilesContributions ON profiles.id = profilesContributions.profileid LEFT JOIN profilesInvestments ON profiles.id = profilesInvestments.profileid LEFT JOIN profilesRetirementNeeds ON profiles.id = profilesRetirementNeeds.profileid LEFT join plansModules ON profiles.planid = plansModules.planid WHERE profiles.planid=".$parameters['planid']." ".$dateString."  ORDER by profiles.id desc";
        $profiles2 = $connection->fetchAll($sql);
        
        foreach($profiles2 as $profile)
        {
            foreach ($profile as &$value)
            if ($value == null)
            $value = "";
            
            $profile['participants_firstName'] = $this->get('spe.app.smart')->speDecrypt($profile['participants_firstName']);
            $profile['participants_lastName'] = $this->get('spe.app.smart')->speDecrypt($profile['participants_lastName']);
            $profile['participants_email'] =$this->get('spe.app.smart')->speDecrypt($profile['participants_email']);
            $profile['participants_birthDate'] = trim(str_replace("/","-",$this->get('spe.app.smart')->speDecrypt($profile['participants_birthDate'])));
			/* strtotime does not handle m-d-Y. Converting to Y-m-d */
			$dt = \DateTime::createFromFormat("m-d-Y", $profile['participants_birthDate']);
			$profile['participants_birthDate'] =  $dt ? date('Y-m-d', $dt->getTimestamp()) : $profile['participants_birthDate'];
			/* end */

            if (isset($return['profiles'][$parameters['planid']][$profile['participants_uniqid']]))
            $count = count($return['profiles'][$parameters['planid']][$profile['participants_uniqid']]);
            else
            $count = 0;
            $return['profiles'][$parameters['planid']][$profile['participants_uniqid']][$count] = $profile;
        }
    }
    function getDateSql($parameters)
    {
        $dateString = "";
        $general = $this->get('GeneralMethods');
        if (isset($parameters['startDate']) && isset($parameters['endDate']))
        {
            $parameters['endDate'] = date($general->addDaysToString($parameters['endDate']));
            $dateString = " AND reportDate >= '".$parameters['startDate']."' AND reportDate <= '".$parameters['endDate']."' ";
        }
        return $dateString;
    }
    function existingParticipants(&$plan,&$return,&$connection)
    {
       $counter = 0;
       foreach ($plan as $participant)
       { 
            if ($this->deferralRateFromRkp($participant[0]) > 0 && $participant[0]['profiles_currentBalance'] > 0)
            {
                $this->commonFields($return['existingParticipants'][$counter],$participant[0]);
                $this->planBalance($return['existingParticipants'][$counter],$participant[0]);
                $this->investementType($return['existingParticipants'][$counter],$participant[0]);
                $this->defChange($return['existingParticipants'][$counter],$participant[0]);
                $this->match($return['existingParticipants'][$counter],$participant[0]);
                $counter++; 
            }
       }
    }
    function newEnrolles(&$plan,&$return,&$connection)
    {
       $counter = 0;
       foreach ($plan as $participant)
       {
            if ($this->deferralRateFromRkp($participant[0]) == 0 && $participant[0]['profiles_currentBalance'] == 0)
            {
                $this->commonFields($return['newEnrolles'][$counter],$participant[0]);
                $this->defChange($return['newEnrolles'][$counter],$participant[0]);
                $this->investementType($return['newEnrolles'][$counter],$participant[0]);
                $this->match($return['newEnrolles'][$counter],$participant[0]);
                $counter++; 
            }
       }
    }
    function participantsNearingRetirement(&$plan,&$return,&$connection)
    {
        $counter = 0;
        foreach ($plan as $participant)
        {   
            if ($this->particiantNearingRetirement($participant[0],$return['age']) )
            {
                $this->commonFields($return['participantsNearingRetirement'][$counter],$participant[0]);
                $this->defChange($return['participantsNearingRetirement'][$counter],$participant[0]);
                $this->planBalance($return['participantsNearingRetirement'][$counter],$participant[0]);
                $this->investementType($return['participantsNearingRetirement'][$counter],$participant[0]);
                $this->outsideAssets($return['participantsNearingRetirement'][$counter],$participant[0]);
                $counter++;
            }
        }
    }
    
    function participantsWithOutsideAssets(&$plan,&$return,&$connection)
    {
        $counter = 0;
        foreach ($plan as $participant)
        {   
            if ($participant[0]['profilesRetirementNeeds_otherAssets'] > 0)
            {
                $this->commonFields($return['participantsWithOutsideAssets'][$counter],$participant[0]);
                $this->outsideAssets($return['participantsWithOutsideAssets'][$counter],$participant[0]);
                $this->assetTypes($return['participantsWithOutsideAssets'][$counter],$participant[0]);
                $counter++;
            }
        }
    } 
    function assetTypes(&$reportRow,&$participant)
    {
        $reportRow['assetType_bonds'] = 0;
        $reportRow['assetType_mutual_funds_bonds']= 0;
        $reportRow['assetType_stocks']= 0;
        $reportRow['assetType_mutual_funds_stocks']= 0;
        $reportRow['assetType_money_market_funds']= 0;
        $reportRow['assetType_certificate_of_deposit'] = 0;
        $reportRow['assetType_former_retirement_account']= 0;
        $reportRow['assetType_Individual_retirement_account']= 0;
        
        
        if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"Certificate of Deposit")  > 0)
        $reportRow['assetType_certificate_of_deposit'] = 1;
        if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"Former Retirement Account")  > 0)
        $reportRow['assetType_former_retirement_account'] = 1;
        if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"(IRA)")  > 0)
        $reportRow['assetType_Individual_retirement_account'] = 1; 
        if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"Money Market Fund")  > 0)
        $reportRow['assetType_money_market_funds'] = 1;
        
        if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"Bonds Mutual Funds") == 1)
        {
            $reportRow['assetType_mutual_funds_bonds']= 1;
            if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"Bonds") == 2)
            $reportRow['assetType_bonds'] = 1;
        }
        else if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"Bonds") == 1)
        $reportRow['assetType_bonds'] = 1;        
        
        
        if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"Stock Mutual Funds") == 1)
        {
            $reportRow['assetType_mutual_funds_stocks']= 1;
            if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"Stocks") == 2)
            $reportRow['assetType_stocks'] = 1;
        }
        else if (substr_count($participant['profilesRetirementNeeds_assetTypes'],"Stocks") == 1)
        $reportRow['assetType_mutual_funds_stocks'] = 1;  
        if (trim($participant['profilesRetirementNeeds_assetTypes']) == "")
        $reportRow['assetType_not_specified'] = 1;
        else
        $reportRow['assetType_not_specified'] = 0;    
    }
    
    function outsideAssets(&$reportRow,&$participant)
    {
        $reportRow['outsideAssets'] = $participant['profilesRetirementNeeds_otherAssets'];
    }
    
    function defChange(&$reportRow,&$participant)
    {
        $generalfunctions = $this->get('generalfunctions');
        $reportRow['preDef'] = $this->deferralRateFromRkp($participant);
        $reportRow['postDef'] = $participant['profilesContributions_pre'] + $participant['profilesContributions_roth'];
        $reportRow['deferral_rate'] = "-";
        if ($reportRow['preDef'] == $reportRow['postDef'])
        {
            $reportRow['changeDef'] ="No Change";
        }
        else
        {
            if ($reportRow['preDef'] == 0 && $reportRow['postDef'] > 0)
                $reportRow['changeDef'] = "-";
            else
            {
                $reportRow['changeDef'] = $generalfunctions->calculateDelta($reportRow['preDef'], $reportRow['postDef']);

                $reportRow['changeDef'] = round($reportRow['changeDef'],2);
                if ($reportRow['preDef'] > $reportRow['postDef'])
                    $reportRow['changeDef'] = "-".$reportRow['changeDef'];
                $reportRow['changeDef'] = $reportRow['changeDef']."%";
            }
        }
        if ($participant['profilesContributions_mode'] == "PCT" && $participant['profilesContributions_deferralPercent'] != null)
        {
            $reportRow['preDef'] = $reportRow['preDef']."%";
            $reportRow['postDef'] = $reportRow['postDef']."%";
            $reportRow['deferral_rate'] = number_format($participant['profilesContributions_deferralPercent'])."%";
        }
        else if ($participant['profilesContributions_mode'] == "DLR" && $participant['profilesContributions_deferralDollar'] != null)
        {
            $reportRow['preDef'] = "$".$reportRow['preDef'];
            $reportRow['postDef'] = "$".$reportRow['postDef'];
            $reportRow['deferral_rate'] = "$".number_format($participant['profilesContributions_deferralDollar']);
        }
        else
        {
            $reportRow['preDef'] = "-";
            $reportRow['postDef'] = "-";
        }

    }
    function planBalance(&$reportRow,&$participant)
    {
        $reportRow['planBalance'] = round($participant['profiles_currentBalance'],2);
        
    }
    function match(&$reportRow,&$participant)
    {
        $valid = 0;
        if ($participant['profilesContributions_mode'] == "DLR" && in_array($participant['profilesContributions_paidFrequency'],array("Annually","Bi-weekly","Monthly","Quarterly","Semi-monthly","Weekly")))
        {
            switch ($participant['profilesContributions_paidFrequency'])
            {
                case "Annually":
                $multipler = 1;    
                break;
                case "Bi-weekly":
                $multipler = 26;    
                break;
                case "Monthly":
                $multipler = 12;   
                break;                
                case "Quarterly":
                $multipler = 4;   
                break;  
                case "Semi-monthly":
                $multipler = 24;
                break;
                case "Weekly":
                $multipler = 52;
                break;                
            }
            if ($participant['profilesContributions_currentYearlyIncome'] == 0)
            $contributionAmount = 0;
            else
            $contributionAmount = (($multipler * ($participant['profilesContributions_pre'] + $participant['profilesContributions_roth']))/$participant['profilesContributions_currentYearlyIncome'] ) * 100;
            $valid = 1;
        } 
        else if ($participant['profilesContributions_mode'] == "PCT")
        {
            $contributionAmount = $participant['profilesContributions_pre'] + $participant['profilesContributions_roth'];
            $valid = 1;
        }
        if ($valid)
        {
            if($participant['plansModules_matchingCap'] == 0 && $participant['plansModules_secondaryMatchingCap'] == 0)
                $reportRow['match'] =  "N/A";
            else if ($contributionAmount >= $participant['plansModules_matchingCap'] && $contributionAmount >= $participant['plansModules_secondaryMatchingCap'])
                $reportRow['match'] = "Y";
            else
                $reportRow['match'] = "N";
        }
        else
            $reportRow['match'] =  "-";
        
        //$reportRow['match'] = $participant['profilesContributions_mode'].",".$multipler.",".$participant['profilesContributions_pre'].",".$participant['profilesContributions_roth'].",".$participant['profilesContributions_currentYearlyIncome'].",".$contributionAmount.",".$participant['profilesContributions_paidFrequency'];
    }
    function investementType(&$reportRow,&$participant)
    {
        $reportRow['investmentType'] = $participant['profilesInvestments_type'];
    }
    function commonFields(&$reportRow,&$participant)
    {
        
        $reportRow['firstName'] = $participant['participants_firstName'];
        $reportRow['lastName'] = $participant['participants_lastName'];
        $reportRow['email'] = $participant['participants_email'];
        $reportRow['age'] = $this->calculateAge($participant['participants_birthDate']);
        $reportRow['salary'] = (!empty($participant['profilesRetirementNeeds_currentYearlyIncome']) ? $participant['profilesRetirementNeeds_currentYearlyIncome'] : $participant['profilesContributions_currentYearlyIncome']);

    }
    function deferralRateFromRkp(&$participant)
    {
        return $participant['profilesContributions_preCurrent'] + $participant['profilesContributions_rothCurrent'];
    }
    
    function particiantNearingRetirement(&$participant,&$age)
    {
        $participantAge = $this->calculateAge($participant['participants_birthDate']);
        if ($participantAge != "N/A" && $this->calculateAge($participant['participants_birthDate']) >= $age)
        return true;
        return false;
    }
    
    function calculateAge(&$birthday)
    { 
        $age = strtotime($birthday);

        if($age === false){ 
            return "N/A";
        } 

        list($y1,$m1,$d1) = explode("-",date("Y-m-d",$age)); 

        $now = strtotime("now"); 

        list($y2,$m2,$d2) = explode("-",date("Y-m-d",$now)); 

        $age = $y2 - $y1; 

        if((int)($m2.$d2) < (int)($m1.$d1)) 
        $age -= 1; 

        return $age; 
    }
    public function plansAddedReportAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Stats");
        return $this->render('ManageManageBundle:Reports:PlansAddedReport/index.html.twig');
    }
    public function plansAddedReportGetReportAction(Request $request)
    {
        $request = $request->query->all();
        $plansAdded = $this->get("WidgetReportsPlansAddedService");
        $report = $plansAdded->calculatePlansAddedReport($request);
        $this->printReport($report,"Plans Added Report");
        return new Response("");
    }
    //widgets user activity reports
    public function irioUserActivityReportAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Stats");
        return $this->render('ManageManageBundle:Reports:IrioUserActivity/index.html.twig');         
    }
    public function irioUserActivityReportGetReportAction(Request $request)
    {
        $request = $request->query->all();
        $userActivityReport = $this->get("WidgetUserActivityReportService");
        $report = $userActivityReport->calculateData("irio",$request);
        $this->printReport($report,"Irio User Activity Report");
        return new Response("");
       // return $this->render('ManageManageBundle:Reports:IrioUserActivity/report.html.twig',array("data" => $data));
    }
    public function powerviewUserActivityReportAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Stats");
        return $this->render('ManageManageBundle:Reports:PowerviewUserActivity/index.html.twig');         
    }
    public function powerviewUserActivityReportGetReportAction(Request $request)
    {
        $request = $request->query->all(); 
        $userActivityReport = $this->get("WidgetUserActivityReportService");
        $report = $userActivityReport->calculateData("powerview",$request);
        $this->printReport($report,"Powerview User Activity Report");
        return new Response("");
    }
    //widgets performance reports
    public function irioPerformanceReportAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Stats");
        return $this->render('ManageManageBundle:Reports:IrioPerformance/index.html.twig'); 
    }
    public function powerviewPerformanceReportAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Stats");
        return $this->render('ManageManageBundle:Reports:PowerviewPerformance/index.html.twig'); 
    }
    public function irioPerformanceReportGetReportAction(Request $request)
    {
        $this->runPerformanceReport("irio",$request);
        return new Response("");
    }
    private function addEmailReportingFields(&$report,$reportType)
    {
        $emailStats = $reportType->getEmailStats();
        if ($reportType->type != "adviser")
        {
            $reportType->AddRow($report,"Emails sent",$emailStats->sent);
            $reportType->AddRow($report,"Emails delivered",$emailStats->delivered);
            $reportType->AddRow($report,"Emails undelivered",$emailStats->undelivered);
            $reportType->AddRow($report,"Emails bounced",$emailStats->bounces);
            $reportType->AddRow($report,"Emails unsubscribed",$emailStats->unsubscribes);
        }
    }
    public function powerviewPerformanceReportGetReportAction(Request $request)
    {
        $this->runPerformanceReport("powerview",$request);
        return new Response("");
    }  
    function runPerformanceReport($type,$request)
    {
        $request = $request->query->all();
        $performanceReport = $this->get("WidgetsPerformanceReportService");    
        $userActivityReport = $this->get("WidgetUserActivityReportService");
        $userActivityReport->calculateData($type,$request);
        $performanceReport2 = $userActivityReport->performance;
        $request['reportType'] = $type;
        $performanceReport->globals($request);
        $performanceReport->ga();
        $report = array();
        $performanceReport->AddRow($report, "Name",$performanceReport->name());
        $report[0]['header']  = $performanceReport->reportHeader();
        if ($performanceReport->type == "global")
        $performanceReport->AddRow($report, "Total # of Clients",$performanceReport2['numberofclients']);
        if ($performanceReport->type != "plan")
        {
            $performanceReport->AddRow($report, "Total # of Plans",$performanceReport2['numberofplans']);
            //$performanceReport->AddRow($report, "Total # of Plans (deferral off)",$performanceReport2['deferralOff']);
           // $performanceReport->AddRow($report, "Total # of Plans (deferral on)",$performanceReport2['deferralOn']);
        }
        $this->appendWidgetEntry($report,$performanceReport,$performanceReport2,$type);
        $this->printReport($report,"Report");
    }
    public function downloadWidgetCsvAction(Request $request)
    {
        set_time_limit(0);
        $request = $request->query->all();
        $performanceReport = $this->get("WidgetsPerformanceReportService");    
        $userActivityReport = $this->get("WidgetUserActivityReportService");
        $userActivityReport->calculateData($request['reportType'],$request);
        $performanceReport2 = $userActivityReport->performance;
        $reports = array();
        foreach ($performanceReport2 as $key => $performance)
        {
            $report = array();
            $request['planid'] = $key;
            $performanceReport->globals($request);
            $performanceReport->ga();
            $performanceReport->AddRow($report, "Name",$performanceReport->name());
            $performanceReport->AddRow($report, "partnerid",$performance['partnerid']);
            $performanceReport->AddRow($report, "planid",$performance['planid']);
            $this->appendWidgetEntry($report,$performanceReport,$performance,$request['reportType']);
            if (count($reports) == 0)
            {
                foreach ($report as $row)
                {
                     $reports['header'][] = $row['description'];
                }   
            }
            $reports[] = $report;
        }
        $csv = array();
        $csv[] = $reports['header'];
        foreach ($reports as $key => $report)
        {
            if ($key !== "header")
            {
                $csvNew = array();
                foreach ($report as $data)
                {
                    $csvNew[] = $data['value'];
                }
                $csv[] = $csvNew;
            }
        }
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=report.csv');
        $output = fopen('php://output', 'w');
        foreach ($csv as $row)
        fputcsv($output, $row);
        return new Response("");        
    }
    function appendWidgetEntry(&$report,&$performanceReport,&$performanceReport2,$type)
    {
        if ($type == "powerview")
        {
            $performanceReport->AddRow($report, "Total # of Users (deferral off) ",$performanceReport2['totalNumberOfUsersDeferralOff']);
            $performanceReport->AddRow($report, "Average Salary","$".number_format($performanceReport2['averageSalary'],2));
            $performanceReport->AddRow($report, "Average Total Account Balance","$".number_format($performanceReport2['averageBalance'],2));  
            $performanceReport->AddRow($report, "# of Users Received CTA - Max Match ",$performanceReport2['numberOfUsersReceivedMaxMatch']);
            $performanceReport->AddRow($report, "CTA - Maximizing Match # of Transactions ",$performanceReport2['numberOfTransactions']);
            $performanceReport->AddRow($report, "CTA - Average Maximizing Match deferral rate upon entry",number_format($performanceReport2['maxMatchdeferralEntryAverage'],2)."%");
            $performanceReport->AddRow($report, "CTA - Average Maximizing Match deferral rate upon exit",number_format($performanceReport2['maxMatchdeferralExitAverage'],2)."%");  
            $performanceReport->AddRow($report, "# of Users Received CTA - Check out provider website",$performanceReport2['providersWebsiteClicked']);   
            $performanceReport->AddRow($report, "# of Users Received CTA - Revisit SmartPlan",$performanceReport2['spbutton']);     
            $performanceReport->AddRow($report, "Top Browser",$performanceReport->topBrowser());
            $performanceReport->AddRow($report, "Top Mobile Device",$performanceReport->topMobileDevice()); 
            $performanceReport->AddRow($report, "% of users who transacted between 12:00am-8:00am ",number_format($performanceReport2['0-8_trans_percent'],2)."%");
            $performanceReport->AddRow($report, "% of users who transacted between 8:00am-4:00pm ",number_format($performanceReport2['8-16_trans_percent'],2)."%");
            $performanceReport->AddRow($report, "% of users who transacted between 4:00pm-12am ",number_format($performanceReport2['16-24_trans_percent'],2)."%");
            $performanceReport->AddRow($report, "Top 25 Cities for unique visits",$performanceReport->topCities());
            $performanceReport->AddRow($report, "# of visits",$performanceReport->totalVisits());
            $performanceReport->AddRow($report, "% of unique visits",number_format($performanceReport->percentageOfUniqueVisits(),2)."%");
            $performanceReport->AddRow($report, "% of returning visits",number_format($performanceReport->returningUsersPercent(),2)."%");
            $this->addEmailReportingFields($report,$performanceReport);    
        }
        if ($type == "irio")
        {
            $performanceReport->AddRow($report, "Total # of Users",$performanceReport->totalNumberOfUniqueUsers());
            $performanceReport->AddRow($report, "Total # of Users (deferral off) ",$performanceReport->totalNumberOfUsersDeferralOff());
            $performanceReport->AddRow($report, "Total # of Users (deferral on) ",$performanceReport->totalNumberOfUsersDeferralOn());
            $performanceReport->AddRow($report, "Total Selected 1% ",$performanceReport->select(1)); 
            $performanceReport->AddRow($report, "Total Selected 2% ",$performanceReport->select(2));
            $performanceReport->AddRow($report, "Total Selected 3% ",$performanceReport->select(3));
            $performanceReport->AddRow($report, "Average Entering Deferral Rate ",number_format($performanceReport->enteringDeferral(),2)."%");
            $performanceReport->AddRow($report, "Average Exit Deferral Rate ",number_format($performanceReport->exitDeferral(),2)."%");
            $performanceReport->AddRow($report, "Average Deferral Change",number_format($performanceReport->deferralChange(),2)."%");
            $performanceReport->AddRow($report, "Average Salary","$".number_format($performanceReport->averageSalary(),2));
            $performanceReport->AddRow($report, "Average Total Account Balance","$".number_format($performanceReport->averageBalance(),2));  
            $performanceReport->AddRow($report, "Participants Choosing 1%",$performanceReport->selectedDeferral(1));
            $performanceReport->AddRow($report, "Participants Choosing 2%",$performanceReport->selectedDeferral(2));
            $performanceReport->AddRow($report, "Participants Choosing 3%",$performanceReport->selectedDeferral(3));  
            $performanceReport->AddRow($report, "Total Other Retirement Accounts Balance","$".number_format($performanceReport->averageOtherRetirementAccountBalance(),2));      
            $performanceReport->AddRow($report, "Total Spouse Balance","$".number_format($performanceReport->totalSpouseBalance(),2));
            $performanceReport->AddRow($report, "Top Browser",$performanceReport->topBrowser());
            $performanceReport->AddRow($report, "Top Mobile Device",$performanceReport->topMobileDevice());
            $performanceReport->AddRow($report, "% of users who transacted between 12:00am-8:00am ",$performanceReport->transacted(1));
            $performanceReport->AddRow($report, "% of users who transacted between 8:00am-4:00pm ",$performanceReport->transacted(2));
            $performanceReport->AddRow($report, "% of users who transacted between 4:00pm-12am ",$performanceReport->transacted(3));
            $performanceReport->AddRow($report, "Top 25 Cities for unique visits",$performanceReport->topCities());
            $performanceReport->AddRow($report, "# of visits",$performanceReport->totalVisits());
            $performanceReport->AddRow($report, "% of unique visits",number_format($performanceReport->percentageOfUniqueVisits(),2)."%");
            $performanceReport->AddRow($report, "% of returning visits",number_format($performanceReport->returningUsersPercent(),2)."%");            
        }
    }
}
?>
