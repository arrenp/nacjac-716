<?php

namespace Manage\ManageBundle\Controller;

use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class XmlRawUiController extends Controller {

    private $fields = [
        'id' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
        'profileId' => ['orderable'=>true,'searchable'=>true,'isPulldown' => false],
        'connection' => ['orderable'=>true,'searchable'=>true,'isPulldown' => true],
        'action' => ['orderable'=>true,'searchable'=>true,'isPulldown' => true,'options'=>array('send','receive','response')],
        'timestamp' => ['orderable'=>true,'searchable'=>false,'isPulldown' => false],
        'service' => ['orderable'=>true,'searchable'=>true,'isPulldown' => true,'options'=>array()],
        'application' => ['orderable'=>true,'searchable'=>true,'isPulldown' => true],
        'data' => ['orderable'=>false,'searchable'=>false,'isPulldown' => false]
    ];

    public function indexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        $connection = $this->container->get("database_connection");
        $sql = "select * from xmlRawConnectionTypes";
        $options = $connection->fetchAll($sql);
        $optionsArray = array();
        foreach ($options as $value) {
            $optionsArray[] = $value['connectionType'];
        }
        $this->fields['application']['options'] = $this->getApplications($connection);

        $this->fields['connection']['options'] = $optionsArray;
        return $this->render("ManageManageBundle:XmlRawUi:index.html.twig", ['fields' => $this->fields]);
    }

    public function getServicesAction(Request $request) {
        $connectionType = $request->request->get('connectionType');
        $connection = $this->container->get("database_connection");
        $sql = "select * from xmlRawServices where connectionId = (select id from xmlRawConnectionTypes where connectionType = '{$connectionType}')";
        $services = $connection->fetchAll($sql);

        return new Response(json_encode($services));
    }
    
    public function searchAction(Request $request) {
        $connection = $this->get("database_connection"); 
        $req = $request->request->all();
		$searchstring = " WHERE 1 = 1 ";
        $parameters = [];
        
        foreach ($this->fields as $name => $field) {
            if (!empty($req[$name]) && $field['searchable']) {
                $searchstring .= " AND $name LIKE :$name ";
                $parameters[":$name"] = '%' . $req[$name] . '%';
            }
        }
        
        if (!empty($req['startDate'])) {
            $searchstring .= " AND timestamp >= :startDate ";
            $parameters[':startDate'] = $req['startDate'];
        }
        
        if (!empty($req['endDate'])) {
            $searchstring .= " AND timestamp <= :endDate ";
            $parameters[':endDate'] = $req['endDate'];
        }
        
		$orderByArr = [];
		foreach ($req["order"] as $val) {
            $orderBy = $req["columns"][$val["column"]]["data"];
            if (isset($this->fields[$orderBy]) && $this->fields[$orderBy]['orderable']) {
                $dir = strtolower($val["dir"]) === "desc" ? "desc" : "asc";
                $orderByArr[] = "$orderBy $dir";
            }
        }
		$orderByArr[] = "a.id ASC";
		$orderByString = " ORDER BY " . implode(", ", $orderByArr);
        
        $queryCountString = "SELECT COUNT(a.id) FROM xmlRaw a $searchstring";
        $recordsTotal = $connection->fetchColumn($queryCountString, $parameters);

		$queryFilteredCountString = "SELECT COUNT(a.id) FROM xmlRaw a $searchstring";
        $recordsFiltered = $connection->fetchColumn($queryFilteredCountString, $parameters);
        
        $limitString = "";
        if (isset($req['start']) && isset($req['length'])) {
            $limitString = sprintf("LIMIT %d, %d", (int) $req["start"], (int) $req['length']);
        }
        
        $queryString = "SELECT *, AES_DECRYPT(data, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."')) as data FROM xmlRaw a $searchstring $orderByString $limitString";
        $items = $connection->fetchAll($queryString, $parameters);
        
        foreach ($items as $key => $item) {
            $items[$key]['idRaw'] = $item['id'];
            $items[$key]['id'] = '<a href="javascript:ajaxModal(\''.$this->generateUrl('_manage_xml_raw_ui_display', ['id' => $item['id']]).'\',\'XML Log Entry\',\'large\')">'.$item['id'].'</a>';
            $items[$key]['dataRaw'] = $item['data'];
            $items[$key]['data'] = htmlspecialchars(strlen($item['data']) > 50 ? substr($item['data'], 0, 50) . "..." : $item['data']);
            $items[$key]['data'] = '<a href="javascript:ajaxModal(\''.$this->generateUrl('_manage_xml_raw_ui_display', ['id' => $item['id']]).'\',\'XML Log Entry\',\'large\')">'.$items[$key]['data'].'</a>';
        }
        
        if ($request->get("export")) {
            $response = new StreamedResponse(function() use ($items) {
                $handle = fopen('php://output', 'w+');
                fwrite($handle, "\xEF\xBB\xBF");
                fputcsv($handle, ['ID', 'profileId', 'connection', 'action', 'timestamp', 'service', 'Data']);
                foreach ($items as $row) {
                    $csvRow = [$row['idRaw'], $row['profileId'], $row['connection'], $row['action'], $row['timestamp'], $row['service'], $row['dataRaw']];
                    fputcsv($handle, $csvRow);
                }
                fclose($handle);
            });
            $response->headers->set('Content-Type', 'text/csv');
            $response->headers->set('Content-Disposition','attachment; filename="XmlLogExport.txt"');
            return $response;
        }
        
        return new JsonResponse([
            "draw" => (int) $req["draw"],
            "recordsTotal" => (int) $recordsTotal,
            "recordsFiltered" => (int) $recordsFiltered,
            "data" => $items
        ]);
    }
    
    public function displayAction($id) {
        $connection = $this->get("database_connection");
        $record = $connection->fetchAssoc("SELECT *, AES_DECRYPT(data, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."')) as data FROM xmlRaw WHERE id=:id",[':id'=>$id]);
        $simpleXml = simplexml_load_string($record['data']);
        if ($simpleXml !== false) {
            $dom = new \DOMDocument("1.0");
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->loadXML($record['data']);
            $record['data'] = $dom->saveXML();
        }
        return $this->render("ManageManageBundle:XmlRawUi:display.html.twig", ['record' => $record]);
    }
    private function getApplications($connection) {
        $sql = "select application from xmlRawApplications";

        $result = $connection->fetchAll($sql);
        $applicationSet = array();
        foreach ($result as $item) {
            if (!empty($item['application']))
            {
                $applicationSet[$item['application']] = $item['application'];
            }
        }

        return $applicationSet;
    }
}
