<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Entity\accounts;
use classes\classBundle\Entity\HubSpotLog;
use classes\classBundle\Entity\launchToolRecipients;
use classes\classBundle\Entity\plans;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Permissions\RolesBundle\Classes\rolesPermissions;


class HdmiController extends Controller {

    private $permissions;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $session = $this->get('adminsession');
        $session->set("section", "emailCampaigns");
        $this->permissions = $this->get('rolesPermissions');
        $readable = $this->permissions->readable("ManageHDMI");
        if (!$readable) {
            throw $this->createAccessDeniedException();
        }
    }

    public function indexAction() {
        $writeable = $this->permissions->writeable("ManageHDMI");
        $repository = $this->getDoctrine()->getRepository(accounts::class);
        $accounts = $repository->findBy([], ['company' => 'ASC']);
        return $this->render('ManageManageBundle:Hdmi:index.html.twig', ['accounts' => $accounts, 'writeable' => $writeable]);
    }
    
    public function searchPlansAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(plans::class);
        $query = $repository->createQueryBuilder('p')
            ->select('p.id as id, p.planid as value, p.type as type, CONCAT(case when p.type = \'smartplan\' then \'SmartPlan\' else \'SmartEnroll\' end, \' \', p.name) as label')
            ->where('p.planid LIKE :planid')
            ->andWhere("p.userid = :userid")
            ->andWhere('p.deleted = 0')
            ->setParameter('planid', '%'.$request->get("planid").'%')
            ->setParameter('userid', $request->get("userid"))
            ->orderBy('p.name', 'ASC')
            ->getQuery();

        $plans = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return new JsonResponse($plans);
                
    }
    
    protected function getPlans($userid, $getTotals = false) {
        $connection = $this->get("database_connection");
        $planRepository = $this->getDoctrine()->getRepository(plans::class);
        $entityManager = $this->getDoctrine()->getManager();
        
        $result = [];
        $sql = "
            SELECT 
                p.id, 
                p.planid, 
                p.name, 
                IF (p.type = 'smartplan', 'SP', 'SE') as type,
                p.participantDetails,
                p.participantLoginWebsiteAddress,
                p.HRFirst,
                p.HRLast,
                p.HREmail,
                p.providerLogoEmailImage,
                p.providerLogoImage,
                p.sponsorLogoEmailImage,
                p.sponsorLogoImage
            FROM
                plans AS p
            WHERE
                p.participantDetails <> 0 AND p.userid = :userid
            GROUP BY
                p.id
        ";
        $stmt = $connection->prepare($sql);
        $stmt->execute([':userid' => $userid]);

        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $errors = [];
            $ok = 1;
            if (empty($row['participantLoginWebsiteAddress'])) {
                $errors[] = 'No Website';
                $ok = 0;
            }   
            if ((int) $row['totalEnrollees2'] > 0) {
                $errors[] = 'No planid';
                $ok = 0;
            }
            if (empty($row['HRFirst']) || empty($row['HRLast']) || empty($row['HREmail'])) {
                $errors[] = 'No HR';
                $ok = 0;
            }
            if (empty($row['providerLogoEmailImage']) && empty($row['providerLogoImage'])) {
                $errors[] = 'No Provider Logo';
            }
            if (empty($row['sponsorLogoEmailImage']) && empty($row['sponsorLogoImage'])) {
                $errors[] = 'No Sponsor Logo';
            }

            $status = count($errors) > 0 ? implode(', ', $errors) : "OK";
            if (!$ok) {
                $plan = $planRepository->find($row['id']);
                $plan->participantDetails = $row['participantDetails'] = 2;
            }
            $row['ok'] = $ok;
            $row['status'] = $status;
            if ($getTotals) {
                $total1 = $connection->fetchColumn("SELECT COUNT(id) FROM launchToolRecipients WHERE planid = :planid", ['planid' => $row['id']]);
                $total2 = $connection->fetchColumn("SELECT COUNT(id) FROM launchToolRecipients WHERE recordkeeperPlanid = :recordkeeperPlanid AND partnerid = :userid AND (planid IS NULL OR planId = '')", ['recordkeeperPlanid' => $row['planid'], 'userid' => $userid]);

                $row['totalEnrollees'] = $total1 + $total2;
            }
            $result[$row['id']] = $row;
        }            
        
        $entityManager->flush();
        return $result;
    }
    
    public function checkStatusAction(Request $request) {
        return new JsonResponse($this->getPlans($request->get("accountId"), true));
    }
    
    public function updateAllAction(Request $request) {
        $planRepository = $this->getDoctrine()->getRepository(plans::class);
        $entityManager = $this->getDoctrine()->getManager();
        $action = $request->get("action");
        foreach ($this->getPlans($request->get("userid")) as $id => $planArray) {
            $plan = $planRepository->find($id);
            if ($action === "checkAll" && $plan->participantDetails === 2 && $planArray['status'] === "OK") {
                $plan->participantDetails = 1;
            }
            if ($action === "uncheckAll" && $plan->participantDetails === 1) {
                $plan->participantDetails = 2;
            }
            if ($action === "removeUnchecked" && $plan->participantDetails !== 1) {
                $plan->participantDetails = 0;
            }
        }
        $entityManager->flush();
        return new Response("success");
    }
    
    public function updateSingleAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(plans::class);

        $plan = $repository->find($request->get("smartplanid"));
        $plan->participantDetails = $request->get('participantDetails', 2);
        $entityManager->flush();
        return new Response("success");
    }
    
    public function checkProgressAction(Request $request) {
        $session = $request->getSession();
        $current = $session->get("hubSpotDataMigrationCurrent");
        $total = $session->get("hubSpotDataMigrationTotal");
        $invalidEmails = $session->get("hubSpotDataMigrationInvalidEmails");
        return new JsonResponse(compact("current", "total", "invalidEmails"));
    }
    
    public function processEnrolleesAction(Request $request) {
        $result = $this->get("HubSpotDataMigrationService")->processAccount($request->get("accountId"));
        $this->updateLog($request->get("logId"), $result["emails"]);
        unset($result['emails']);
        return new JsonResponse($result);
    }

    protected function addEmailToLog($logId, $planid, $type, $email) {
        $newEmails = [$planid => [$type => (array) $email ]];
        $this->updateLog($logId, $newEmails);
    }
    
    protected function updateLog($logId, array $emails) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();
        try {
            // Lock row to prevent writes until transaction finished
            $logItem = $entityManager->find(HubSpotLog::class, $logId, \Doctrine\DBAL\LockMode::PESSIMISTIC_WRITE);
            $data = json_decode($logItem->data, true);
            
            if (!array_key_exists("plans", $data)) {
                $data["plans"] = [];
            }
            
            $plansIndexedByPlanId = [];
            foreach ($data["plans"] as $plan) {
                $plansIndexedByPlanId[$plan["id"]] = $plan;
            }
            
            foreach ($emails as $planid => $emailsInner) {
                if (!array_key_exists($planid, $plansIndexedByPlanId)) {
                    $plansIndexedByPlanId[$planid] = [];
                }
                if (!array_key_exists("contacts", $plansIndexedByPlanId[$planid])) {
                    $plansIndexedByPlanId[$planid]["contacts"] = [];
                }
                if (!array_key_exists("id", $plansIndexedByPlanId[$planid])) {
                    $plansIndexedByPlanId[$planid]["id"] = $planid;
                }
                $existing = $plansIndexedByPlanId[$planid]["contacts"];

                $plansIndexedByPlanId[$planid]["contacts"] = array_merge_recursive($existing, $emailsInner);
            }
            $data["plans"] = array_values($plansIndexedByPlanId);
            
            $logItem->data = json_encode($data);
            $entityManager->persist($logItem);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            return false;
        }
        return true;
    }
    
    
    public function updateEnrolleeAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $enrolleeRepository = $this->getDoctrine()->getRepository(launchToolRecipients::class);
        $launchToolRecipient = $enrolleeRepository->find($request->get("id"));
        $launchToolRecipient->email = $request->get("email");
        $entityManager->flush();
        
        $result = $this->get("HubSpotDataMigrationService")->processEnrollee($launchToolRecipient->id);
        if ($result) {
            $this->addEmailToLog($request->get("logId"), $launchToolRecipient->planid, "sent", $launchToolRecipient->email);
        }
        return new JsonResponse(['success' => $result, 'email' => $launchToolRecipient->email, 'id' => $launchToolRecipient->id]);
    }
    
    public function deleteEnrolleeAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $enrolleeRepository = $this->getDoctrine()->getRepository(launchToolRecipients::class);
        $launchToolRecipient = $enrolleeRepository->find($request->get("id"));
        $launchToolRecipient->deleted = 1;
        $entityManager->flush();
        $this->addEmailToLog($request->get("logId"), $launchToolRecipient->planid, "del", $launchToolRecipient->email);
        return new JsonResponse(['success' => true]);
    }
    
    public function ignoreEnrolleeAction(Request $request) {
        $enrolleeRepository = $this->getDoctrine()->getRepository(launchToolRecipients::class);
        $launchToolRecipient = $enrolleeRepository->find($request->get("id"));
        $this->addEmailToLog($request->get("logId"), $launchToolRecipient->planid, "ign", $launchToolRecipient->email);
        return new JsonResponse(['success' => true]);
    }
    
    public function resultsAction(Request $request) {
        $results = $this->getResultsData($request->get("logId"));
        return $this->render('ManageManageBundle:Hdmi:results.html.twig', ["results" => $results, 'logId' => $request->get("logId")]);
    }
    
    public function resultsExportAction(Request $request) {
        $results = $this->getResultsData($request->get("logId"));
        $response = new StreamedResponse(function() use ($results) {
            $handle = fopen('php://output', 'w+');
            fwrite($handle, "\xEF\xBB\xBF");
            fputcsv($handle, ['Plan', 'Planid', 'Enrollees']);
            foreach ($results as $row) {
                fputcsv($handle, [$row['name'], $row['planid'], $row['sentCount']]);
            }
            fclose($handle);
        });
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition','attachment; filename="ResultsExport.csv"');
        return $response;
    }
    
    protected function getResultsData($logId) {
        $logRepository = $this->getDoctrine()->getRepository(HubSpotLog::class);
        $planRepository = $this->getDoctrine()->getRepository(plans::class);
        
        $logItem = $logRepository->find($logId);
        $data = json_decode($logItem->data, true);
        $results = array();
        
        foreach ($data["plans"] as $item) {
            $plan = $planRepository->find($item["id"]);
            $results[] = [
                "planid" => $plan->planid,
                "name" => $plan->name,
                "sentCount" => count($item["contacts"]["sent"])
            ];
        }
        
        return $results;
    }
    
    public function initializeAction(Request $request) {
        $this->get("HubSpotDataMigrationService")->initializeSessionData();
        $entityManager = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $logItem = new HubSpotLog();
        $logItem->accountsUsersId = $session->get("masterid");
        
        $data = [
            "accountId" => $request->get("accountId"),
            "plans" => []
        ];
        $logItem->data = json_encode($data);
        $entityManager->persist($logItem);
        $entityManager->flush();
        
        return new JsonResponse(['logId' => $logItem->id]);
    }
    
    public function resetPlansAction(Request $request) {
        $connection = $this->get("database_connection");
        $params = ['accountId' => $request->get('accountId')];
        $updated = $connection->executeUpdate("UPDATE plans SET participantDetails = 0 WHERE userid=:accountId", $params);
        return new Response($updated);
    }
    
}
