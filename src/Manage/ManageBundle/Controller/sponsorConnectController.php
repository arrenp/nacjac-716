<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Shared\General\GeneralMethods;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\OutreachBundle\Classes\outreach;
class sponsorConnectController extends Controller
{
    public function IndexAction()
    {  
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        return $this->render('ManageManageBundle:sponsorConnect:index.html.twig');
    }
    public function changeStatusAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:launchUsers');
        $user = $repository->findOneBy(array("id" => $request->request->get("id")));   
        $user->accountStatus = 0;
        $user->logoFilePath = null;
        $user->tableName = null;
        $user->password = null;
        $em->flush();
        return new Response("");
    }
    public function resendInvitesAction(Request $request)
    {
        $requestAll = $request->query->all();
        $users = $this->getUsersPerAccount($requestAll);
        $outreach = new outreach($this->getDoctrine());
        $templates = $outreach->sponsorConnectTemplates();
        return $this->render('ManageManageBundle:sponsorConnect:resendinvites.html.twig',array("templates" => $templates,"users" => $users,"userid" => $request->query->get("userid")));
    }
    public function resendInvitesSavedAction(Request $request)
    {
        $requestAll = $request->request->all();
        $templateid = $request->request->get("templateid");
        $connection = $this->get('doctrine.dbal.default_connection');
        $sql = "SELECT * FROM launchUsers WHERE id IN (".$requestAll['inviteUsers'].")";
        $users = $connection->executeQuery($sql)->fetchAll();
        foreach ($users as $user)
        {
            $params['html'] = file_get_contents($this->generateUrl('_manage_outreach_new_preview', array("id" => $templateid,"planid" => $user['smartplanid'],"userid" => $user['partnerid']), true));
            $params['html'] = str_replace("<vwise_sponsor_connect_link/>",$this->container->getParameter('sponsorConnect')."?uc=".uniqid()."#landing",$params['html']); 
            $message = \Swift_Message::newInstance()
            ->setSubject("Important Information from your Provider About Your Retirement Plan")
            ->setFrom('info@smartplanenterprise.com')
            ->setTo($user['email'])
            ->setBody(
            $params['html'],'text/html');
            if (!$this->get('mailer')->send($message))
            {
               return new Response("failed");
            }        
        }
        return new Response("sent");
    }    
    public function getUsersPerAccount($request)
    {
        $connection = $this->get('doctrine.dbal.default_connection');
        $sql = "SELECT * FROM launchUsers WHERE partnerid=".$request['userid']." AND email is not null AND (password is null or password = '') AND (urlPass is not null and urlPass != '')";
        $users = $connection->executeQuery($sql)->fetchAll();
        return $users;
    }
}