<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\Media;
use classes\classBundle\Entity\MediaLocalization;
class MediaController extends Controller
{
    public $mediaSections;
    public $mediaLocalizationSections;
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        return $this->render('ManageManageBundle:Media:index.html.twig');
    }
    public function init_sections($type = "default")
    {
        $media = new media();
        $this->mediaSections = array();
        $this->mediaLocalizationSections = array();
        $classMediaStatic = new \ReflectionClass('classes\classBundle\Entity\Media');
        $classMediaLocalization = new \ReflectionClass('classes\classBundle\Entity\MediaLocalization');
        $methods = $this->get('GeneralMethods');
        $em = $this->getDoctrine()->getManager();
        //media sections
        $methods->addSection($this->mediaSections,"title","Title");
        $methods->addSection($this->mediaSections,"product","Product");
        $methods->addSection($this->mediaSections,"module","Module");
        $methods->addSection($this->mediaSections,"client","Client");
        $methods->addSection($this->mediaSections,"type","Type");
        $methods->addSection($this->mediaSections,"legacyName","Legacy Name");
        $methods->addSection($this->mediaSections,"mediaType","Media Type");

        $this->mediaSections["title"]->type = "text";
        $this->mediaSections["product"]->type = "select";
        $this->mediaSections["product"]->options = $classMediaStatic->getStaticPropertyValue('PRODUCT_OPTIONS');
        $this->mediaSections["module"]->type = "text";
        $this->mediaSections["client"]->type = "select";
        $this->mediaSections["client"]->options = $classMediaStatic->getStaticPropertyValue('CLIENT_OPTIONS');
        $this->mediaSections["type"]->type = "select";
        $this->mediaSections["type"]->options = $classMediaStatic->getStaticPropertyValue('TYPE_OPTIONS');
        $this->mediaSections["mediaType"]->type = "select";
        $this->mediaSections["mediaType"]->options = $classMediaStatic->getStaticPropertyValue('MEDIA_TYPE_OPTIONS');
        $this->mediaSections["legacyName"]->type = "text";


        //media localization sections 
        if ($type == "language enabled")
        {
            $methods->addSection($this->mediaLocalizationSections,"language","Language");
            $languageArray = array();
            $repository = $em->getRepository('classesclassBundle:languageFiles');
            $languages = $repository->findAll();
            foreach($languages as $language)
            {
                $languageArray[$language->name] = $language->description;
            }
            $this->mediaLocalizationSections["language"]->type = "select";
            $this->mediaLocalizationSections["language"]->options = $languageArray;
        }
        $methods->addSection($this->mediaLocalizationSections,"masterFile","Path of Master File");
        $methods->addSection($this->mediaLocalizationSections,"captionsFile","Path of Captions File");
        $methods->addSection($this->mediaLocalizationSections,"version","Version");
        $methods->addSection($this->mediaLocalizationSections,"duration","Duration");
        $methods->addSection($this->mediaLocalizationSections,"ready","Ready");
        /*
        $this->mediaLocalizationSections["language"]->type = "select";
        $this->mediaLocalizationSections["language"]->options = array();


        $connection = $this->get('doctrine.dbal.default_connection');
        if ($options['language'])
        {
            $languages = $connection->fetchAll('SELECT * FROM languageFiles WHERE language_name != "" group by language_name');
            foreach ($languages as $language)
            {
                $languageIndex = count($this->mediaLocalizationSections["language"]->options);
                $this->mediaLocalizationSections["language"]->options[$language["language_name"]] = $language["language_description"];
            }
        }
        */

        $this->mediaLocalizationSections["masterFile"]->type = "text";
        $this->mediaLocalizationSections["captionsFile"]->type = "text";
        $this->mediaLocalizationSections["version"]->type = "text";
        $this->mediaLocalizationSections["duration"]->type = "text";
        $this->mediaLocalizationSections["ready"]->type = "select";
        $this->mediaLocalizationSections["ready"]->options = array(1 => "On",0 => "Off");
    }
    public function listMediaAction()
    {
        $classMediaStatic = new \ReflectionClass('classes\classBundle\Entity\Media');
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:Media');
        $items = $repository->findAll();
        $methods = $this->get('GeneralMethods');
        $methods->datatablesFilterData($items);
        foreach ($items as &$item)
        {
            $item->setMediaType($classMediaStatic->getStaticPropertyValue('MEDIA_TYPE_OPTIONS')[$item->getMediaType()]);
        }
        $response =  $this->render('ManageManageBundle:Media:Search/listmedia.html.twig', array("items" => $items))->getContent();;
        $methods->datatablesFilterJson($response);
        return new Response($response);
    }

    public function listMediaLocalizationsAction($id)
    {

        $this->init_sections("language enabled");
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:Media');
        $media = $repository->findOneBy(array("id" => $id));
        $items = $media->getLocalizations();
        $methods = $this->get('GeneralMethods');

        $methods->datatablesFilterData($items);
        $response =  $this->render('ManageManageBundle:Media:Search/listmedialocalizations.html.twig', array("items" => $items,"mediaLocalizationSections" => $this->mediaLocalizationSections))->getContent();;
        $methods->datatablesFilterJson($response);
        return new Response($response);
    }

    public function addMediaAction()
    {
        $this->init_sections();
        $connection = $this->get('doctrine.dbal.default_connection');
        $english = $connection->fetchAll('SELECT * FROM languageFiles WHERE language_name =  "en" LIMIT 1');
        return $this->render('ManageManageBundle:Media:addmedia.html.twig',array("mediaSections" => $this->mediaSections,"mediaLocalizationSections" =>$this->mediaLocalizationSections,"english" => $english[0]));
    }
    public function copyMedia(&$media,$request)
    {
        $media->setType($request->request->get("type"));
        $media->setProduct($request->request->get("product"));
        $media->setModule($request->request->get("module"));
        $media->setClient($request->request->get("client"));
        $media->setTitle($request->request->get("title"));
        $media->setLegacyName($request->request->get("legacyName"));
        $media->setMediaType($request->request->get("mediaType"));
        $media->setCreatedAt(new \DateTime("NOW"));
        $media->setUpdatedAt(new \DateTime("NOW"));
    }
    public function copyMediaLocalization($mediaLocalization,$request)
    {
        $mediaLocalization->setLanguage($request->request->get("language"));
        $mediaLocalization->setMasterFile($request->request->get("masterFile"));
        $mediaLocalization->setCaptionsFile($request->request->get("captionsFile"));
        $mediaLocalization->setVersion($request->request->get("version"));
        $mediaLocalization->setDuration($request->request->get("duration"));
        $mediaLocalization->setReady($request->request->get("ready"));
        $mediaLocalization->setCreatedAt(new \DateTime("NOW"));
        $mediaLocalization->setUpdatedAt(new \DateTime("NOW"));
    }
    public function addMediaSavedAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $media = new Media();
        $mediaLocalization = new MediaLocalization();
        $this->copyMedia($media,$request);
        $this->copyMediaLocalization($mediaLocalization,$request);
        $media->addLocalization($mediaLocalization);
        $em->persist($media);
        $em->flush();
        return New Response("saved");
    }
    public function testAction()
    {
        $media = new Media();
        $em = $this->getDoctrine()->getManager();
        /*
        0       => "Smartplan Enterprise",
        2       => "Promotional Documents",
        9       => "Tutorials"
        */
        $media->setProduct(0);
       
        /*
        'SP'    => "SmartPlan",
        'FD'    => "Fiduciary Type",
        'AA'    => "Advisors Access",
        'AB'    => 'Alliance Benefit Group',
        'AX'    => "AXA Equitable",
        'LFG'   => "LFG",
        'MG'    => "MGM Resorts",
        'MO'    => "Mutual of Omaha"
        */
        $media->setClient("SP");
        /*
        'e'     => "Standard Type",
        'n'     => "Non-Integrated Type",
        'a'     => "Advice Type"       
        */
        $media->setType("e");
        /*
        1       => "Audio",
        2       => "Video",
        3       => "Other"
        */
        $media->setMediaType(3);


        $media->setModule('module');
        $media->setTitle("title");
        $media->setLegacyName("legacy name");
        $media->setCreatedAt(new \DateTime("NOW"));
        $media->setUpdatedAt(new \DateTime("NOW"));

        //mediaLocatization
        $mediaLocalization = new MediaLocalization();
        $mediaLocalization->setLanguage("eng");
        $mediaLocalization->setMasterFile("file.mpeg");
        $mediaLocalization->setCaptionsFile("var/www/....");
        $mediaLocalization->setVersion("1.1");
        $mediaLocalization->setDuration(30);
        $mediaLocalization->setReady(true);
        $mediaLocalization->setCreatedAt(new \DateTime("NOW"));
        $mediaLocalization->setUpdatedAt(new \DateTime("NOW"));


        
        

        $media->addLocalization($mediaLocalization);




        $em->persist($media);
        $em->flush();
        return new Response("");
    }

    public function deleteMediaAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->request->get("id");
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:Media');
        $item = $repository->findOneBy(array("id" => $id));
        $em->remove($item);
        $em->flush();
        return new Response("");
    }

    public function editMediaAction()
    {
        $this->init_sections("language enabled");
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->query->get("id");
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:Media');
        $media = $repository->findOneBy(array("id" => $id));
        $this->mediaSections['product']->value = $media->getProduct();
        $this->mediaSections['title']->value = $media->getTitle();
        $this->mediaSections['module']->value = $media->getModule();
        $this->mediaSections['type']->value = $media->getType();
        $this->mediaSections['mediaType']->value = $media->getMediaType();
        $this->mediaSections['legacyName']->value = $media->getLegacyName();
        $this->mediaSections['client']->value = $media->getClient();
        return $this->render('ManageManageBundle:Media:editmedia.html.twig',array("mediaSections" => $this->mediaSections,"mediaLocalizationSections" =>$this->mediaLocalizationSections,"id" => $id));
    }

    public function editMediaSavedAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $em->getRepository('classesclassBundle:Media');
        $media = $repository->findOneBy(array("id" => $id));
        $this->copyMedia($media,$request);
        $em->flush();
        return new Response("");
    }

    public function saveMediaLocalizationAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $em->getRepository('classesclassBundle:MediaLocalization');
        $media = $repository->findOneBy(array("id" => $id));
        $this->copyMediaLocalization($media,$request);
        $em->flush();
        return new Response("");
    }
    public function addMediaLocalizationAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $mediaLocalization = new MediaLocalization();

        $repository = $em->getRepository('classesclassBundle:Media');
        $media = $repository->findOneBy(array("id" => $request->request->get("id")));

        $this->copyMediaLocalization($mediaLocalization,$request);
        $mediaLocalization->setMedia($media);
        $em->persist($mediaLocalization);
        $em->flush();
        return new Response("");
    }

    public function deleteMediaLocalizationAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $em->getRepository('classesclassBundle:MediaLocalization');
        $media = $repository->findOneBy(array("id" => $id));
        $em->remove($media);
        $em->flush();
        return new Response("");
    }
}
?>