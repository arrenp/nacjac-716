<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Sessions\AdminBundle\Classes\adminsession;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuditController extends Controller
{   
    const VWISE_USER_ID = 58;
    
    private function getExcludedFieldNames() {
        return ['createDate','modifiedDate','last_login','expires_at','password_requested_at','credentials_expire_at','passwordExpirationDate','time','sentDate','dateAdded','createdDate','deletedDate','dateCreated','dateModified','firstEligibleStatusDate','lastEligibleStatusDate','dateLastProfile','dateLastContributionChanged','dateFirstContributionChanged','timeStamp','uploadDate','lastModified','created_at','updated_at','adviceDate','enrollmentDate','lastVisit','dateProfile','reportDate','startDate','endDate','requestDate','scheduleDate','date'];
    }
    
    public function indexAction()
    {
    	$session = $this->get('adminsession');
    	$session->set("section","Manage");

        $repository = $this->getDoctrine()->getRepository("classesclassBundle:accounts");
        $accounts = $repository->findBy(array(), array('company' => 'ASC'));
        
        $qb = $this->getDoctrine()->getEntityManager()->getRepository("classesclassBundle:auditLog");
        $tableresult = $qb->createQueryBuilder('a')
            ->select('DISTINCT a.tablename')
            ->orderBy('a.tablename')
            ->getQuery()
            ->getResult();

        $tablenames = array();
        foreach ($tableresult as $result) {
            $tablenames[] = $result['tablename'];
        }

    	return $this->render('ManageManageBundle:Audit:index.html.twig', array("accounts" => $accounts, 'tablenames' => $tablenames));
    }
    public function viewAuditLogAction(Request $request)
    {
        return $this->render('ManageManageBundle:Audit:viewauditlog.html.twig',array('log' => $this->getLog($request->query->get("id"))));
    }

    public function getLog($id)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:auditLog');
        $log= $repository->findOneBy(array('id' => $id));
        $log->response = json_decode(stripcslashes(substr($log->record, 1, -1)),true);
        return $log;
    }

    public function searchAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository("classesclassBundle:auditLog");

        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = $repository->createQueryBuilder("a");
        $query->leftJoin("classesclassBundle:accountsUsers", "u", "WITH", "a.accountsUsersId=u.id");
        $filteredCountQuery = $repository->createQueryBuilder('a')->select("COUNT(DISTINCT a.id)");
        $filteredCountQuery->leftJoin("classesclassBundle:accountsUsers", "u", "WITH", "a.accountsUsersId=u.id");
        $vwiseCheck = ($request->get("userid") !== self::VWISE_USER_ID) ? (" OR u.userid = " . self::VWISE_USER_ID) : "";
        $parameters = array();
        if ($request->get("tablename") === "plans" && $request->get("planid")) {
            $query->andWhere("a.recordId=:planid");
            $filteredCountQuery->andWhere("a.recordId=:planid");
            $parameters['planid'] = $request->get("planid");
        }
        if (!empty($request->get("userid"))) {
            $query->andWhere("u.userid = :userid $vwiseCheck");
            $filteredCountQuery->andWhere("u.userid = :userid $vwiseCheck");
            $parameters['userid'] = $request->get("userid");
        }
        if (!empty($request->get("accountsUsersId"))) {
            $query->andWhere("a.accountsUsersId = :accountsUsersId $vwiseCheck");
            $filteredCountQuery->andWhere("a.accountsUsersId = :accountsUsersId $vwiseCheck");
            $parameters['accountsUsersId'] = $request->get("accountsUsersId");
        }
        if (!empty($request->get("tablename"))) {
            $query->andWhere("a.tablename = :tablename");
            $filteredCountQuery->andWhere("a.tablename = :tablename");
            $parameters['tablename'] = $request->get("tablename");
        }
        if (!empty($request->get("startDate"))) {
            $startDate = date('Y-m-d 00:00:00', strtotime($request->get('startDate')));
            $query->andWhere("a.time >= :startDate");
            $filteredCountQuery->andWhere("a.time >= :startDate");
            $parameters['startDate'] = $startDate;
        }
        if (!empty($request->get("endDate"))) {
            $endDate = date('Y-m-d 23:59:59', strtotime($request->get('endDate')));
            $query->andWhere("a.time <= :endDate");
            $filteredCountQuery->andWhere("a.time <= :endDate");
            $parameters['endDate'] = $endDate;
        }
        $query->setParameters($parameters);
        $filteredCountQuery->setParameters($parameters);

        foreach ($request->get("order") as $val) {
            $query->addOrderBy('a.'.$request->get("columns")[$val["column"]]["data"], $val["dir"]);
        }
        $query->groupBy("a.id");
        $query->addOrderBy("a.id", "DESC");
        $query->setFirstResult($request->get("start"));
        $query->setMaxResults($request->get("length"));
        $data = $query->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $recordsFiltered = $filteredCountQuery->getQuery()->getSingleScalarResult();
        $recordsTotal = $filteredCountQuery->getQuery()->getSingleScalarResult();
        
        $excludedFields = $this->getExcludedFieldNames();
        
        foreach ($data as &$row) {
            $updatedFields = array_keys(json_decode(trim(stripslashes($row["record"]),"'"), true));
            $keys = array_slice(array_diff($updatedFields, $excludedFields), 0, 2);
            if (count($keys) === 0) { 
                $keys = array_slice(array_intersect($excludedFields, $updatedFields), 0, 2);
            }
            $row["keys"] = '<a href="javascript:ajaxModal(\''.$this->generateUrl('_manage_audit_viewAuditLog', ['id' => $row['id']]).'\',\'View Record\',\'large\')">' . implode(', ', $keys) . '</a>';
        }

        $results = array(
            "draw" => (int) $request->get("draw"),
            "recordsTotal" => (int) $recordsTotal,
            "recordsFiltered" => (int) $recordsFiltered,
            "data" => $data
        );

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    public function getAccountsUsersAction(Request $request) {
        $connection = $this->get("database_connection");
        $sql = "SELECT id,IFNULL(CONCAT(firstname,' ',lastname, ' (', email, ')'),id) as name FROM accountsUsers WHERE userid=:userid ORDER BY firstname, lastname";
        $parameters = ['userid' => $request->get("userid")];
        $accountsUsers = $connection->fetchAll($sql, $parameters);
        return new JsonResponse($accountsUsers);
    }
    
    public function getPlansAction(Request $request) {
        $connection = $this->get("database_connection");
        $sql = "SELECT id, IFNULL(CONCAT(name, ' (', planid, ')'),id) as name FROM plans WHERE userid=:userid ORDER BY name";
        $parameters = ['userid' => $request->get("userid")];
        $plans = $connection->fetchAll($sql, $parameters);
        return new JsonResponse($plans);
    }
}
?>
