<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Sessions\AdminBundle\Classes\adminsession;
set_time_limit(0);
class LNCLNKeysController extends Controller
{

    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
    	return $this->render('ManageManageBundle:LNCLNKeys:index.html.twig');
    }

    public function editAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:LNCLNKeys');
        $key = $repository->findOneBy(array("id" => $request->query->get("id")));   
        return $this->render('ManageManageBundle:LNCLNKeys:edit.html.twig',array("key" => $key));
    }
    public function editSavedAction()
    {
        return new Response("");
    }
}
?>