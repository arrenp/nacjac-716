<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use classes\classBundle\Entity\plansUI;
use Shared\General\GeneralMethods;
class PlansUIController extends Controller
{
    public function indexAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $planid = $request->query->get("id","");
        $sections  = array();
        $methods = $this->get('GeneralMethods');
        $methods->addSection($sections,"deferralTypeUI","Deferral Type UI");
        $methods->addSection($sections,"matchingContributionsUI","Matching Contributions UI");
        $methods->addSection($sections,"catchupUI","Catchup UI");
        $methods->addSection($sections,"loansUI","Loans UI");
        $methods->addSection($sections,"enrollmentUI","Enrollment UI");
        $methods->addSection($sections,"vestingUI","Vesting UI");

        $em = $this->getDoctrine()->getManager();



        $repository = $em->getRepository('classesclassBundle:plansUI');
        $PlanUI = $repository->findOneBy(array("planid" => $planid));

        if ($PlanUI == null)
        {
            $repository = $em->getRepository('classesclassBundle:plans');
            $Plan = $repository->findOneBy(array("id" => $planid));
            $PlanUI = new plansUI();
            $PlanUI->userid = $Plan->userid;
            $PlanUI->planid = $Plan->id;

            $em->persist($PlanUI);
            $em->flush();
        }
        $id = $PlanUI->id;
        foreach ($sections as &$section)
        {
            foreach ($section as $key => $value)
            {
                if ($key == "field")
                $section->value = $PlanUI->$value;
                if ($section->value)
                $section->checked = "checked";
                else
                $section->checked = "";
            }
        }

        return $this->render('ManageManageBundle:PlansUI:index.html.twig',array("id" => $id,"sections" => $sections));
    }
}
?>