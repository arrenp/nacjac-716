<?php

namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sessions\AdminBundle\Classes\adminsession;
class RiskProfileCreatorQuestionnaireController extends Controller
{
    public function indexAction(Request $request)
    {          
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        $languages = $this->get("LanguagesService")->getAll("id");
        $params = 
        [
            "questionnaire" => $this->get("RiskProfileQuestionnaireService")->findOneBy(["id" =>$request->query->get("id")]), 
            "questions" => $this->get("RiskProfileQuestionnaireQuestionService")->getQuestions($request->query->get("id")),
            "languages" => $languages
        ];
        return $this->render("index.html.twig",$params);
    }
    public function loadQuestionsAction(Request $request)
    {
        $languages = $this->get("LanguagesService")->getAll("id");
        $params = 
        [
            "questionnaire" => $this->get("RiskProfileQuestionnaireService")->findOneBy(["id" =>$request->request->get("id")]), 
            "questions" => $this->get("RiskProfileQuestionnaireQuestionService")->getQuestions($request->request->get("id")),
            "languages" => $languages
        ];   
        return $this->render("loadquestions.html.twig",$params);
    }
    public function addQuestionAction(Request $request)
    {
        $languages = $this->get("LanguagesService")->getAll();       
        return $this->render("addquestion.html.twig",["languages" => $languages,"questionnaire" => $this->get("RiskProfileQuestionnaireService")->findOneBy(["id" =>$request->query->get("id")])]);        
    }
    public function addQuestionSavedAction(Request $request)
    {
        $questionParams = ["riskProfileQuestionnaireId" => $request->request->get("id"),"showPoints" => $request->request->get("showPoints") == "true"];
        $this->get("RiskProfileQuestionnaireQuestionService")->addQuestion($questionParams,$request->request->get("questions"));
        return new Response("saved");
    }
    public function editQuestionAction(Request $request)
    {
        $languages = $this->get("LanguagesService")->getAll("id");       
        $question = $this->get("RiskProfileQuestionnaireQuestionService")->findOneBy(["id" => $request->query->get("id")]);
        return $this->render("editquestion.html.twig",["languages" => $languages,"locales" => $this->get("RiskProfileQuestionnaireQuestionService")->getQuestionLocales($request->query->get("id")),"question" => $question] );        
    }
    public function editQuestionSavedAction(Request $request)
    {
        $params = $request->request->get("question");
        $params['showPoints'] = $params['showPoints'] == "true";
        $question = $this->get("RiskProfileQuestionnaireQuestionService")->findOneBy(["id" => $params["id"]]);
        $this->get("RiskProfileQuestionnaireQuestionService")->save($question,$params);
        $this->get("RiskProfileQuestionnaireQuestionService")->editQuestion($request->request->get("locales"));
        return new Response("");
    }
    public function deleteQuestionAction(Request $request)
    {
        return $this->render("deletequestion.html.twig");
    }
    public function deleteQuestionSavedAction(Request $request)
    {
        $this->get("RiskProfileQuestionnaireQuestionService")->deleteQuestion($request->request->get("id"));
        return new Response("");
    }
    public function saveQuestionOrderAction(Request $request)
    {
        $this->get("RiskProfileQuestionnaireQuestionService")->saveQuestionOrder($request->request->get("ids"));
        return new Response("");
    }
    public function render($file,$params = array()) 
    {
        return parent::render('ManageManageBundle:RiskProfileCreatorQuestionnaire:'.$file,$params); 
    }
}