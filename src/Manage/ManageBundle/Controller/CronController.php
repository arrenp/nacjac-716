<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Sessions\AdminBundle\Classes\adminsession;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\cronTasks;
set_time_limit(0);
class CronController extends Controller
{
    public $path = "/home/nrobertson/Desktop/cron/";
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        $sections = $this->sections();
        return $this->render('ManageManageBundle:Cron:index.html.twig',array("sections" => $sections));
    }
    public function loadCronsAction()
    {
        $sections = $this->sections();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:cronTasks');
        $crons = $repository->findAll();
        $generalMethods = $this->get('GeneralMethods');
        foreach ($crons as  &$cron)
        {
            if ($cron->hour != null)
            $cron->hour = $sections['hour']->value[$cron->hour];
            if ($cron->minute != null)
            $cron->minute = $sections['minute']->value[$cron->minute];
            if ($cron->dayOfTheWeek != null)
            $cron->dayOfTheWeek = $sections['dayOfTheWeek']->value[$cron->dayOfTheWeek]; 
            $cron       = json_decode(json_encode( $cron ), true);   
        }
        $jsoncontent = $this->render('ManageManageBundle:Cron:Search/loadcrons.html.twig', array("sections" => $sections, "crons" => $crons))->getContent();
        $generalMethods->datatablesFilterJson($jsoncontent);
        return new Response($jsoncontent);
    }
    public function addAction()
    {
        $sections = $this->sections();
        return $this->render('ManageManageBundle:Cron:add.html.twig',array("sections" => $sections));
    }
    public function editAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $sections = $this->sections($request->query->get("id"));
        return $this->render('ManageManageBundle:Cron:edit.html.twig',array("sections" => $sections,"id" => $request->query->get("id")));
    }
    public function deleteAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->request->get("id");
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:cronTasks');
        $cronTasks = $repository->findOneBy(array("id" => $id));
        $em->remove($cronTasks);
        $em->flush();
        return new Response("");
    }

    public function editSavedAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $id = $request->request->get("id");
        $arr =  $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:cronTasks');
        $cronTasks = $repository->findOneBy(array("id" => $id));
        foreach($arr as $key => $value)
        {
            if ($value != "")
            $cronTasks->$key = $value;
            else if (in_array($key,array("hour","minute","day","month","dayOfTheWeek")))
            $cronTasks->$key = null;
            else
            $cronTasks->$key  = "";
        }
        $em->flush();

        return new Response("");
    }
    public function saveActiveAction()
    {        
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $em->getRepository('classesclassBundle:cronTasks');
        $cronTasks = $repository->findOneBy(array("id" => $id));
        $cronTasks->active = $request->request->get("active");
        $em->flush();
        return new Response("");
    }
    public function addSavedAction()
    {
        $cronTasks  = new cronTasks();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $arr =  $request->request->all();
        $em = $this->getDoctrine()->getManager();
        foreach($arr as $key => $value)
        {
            if ($value != "" && $value != "skip")
            $cronTasks->$key = $value;
        }
        $em->persist($cronTasks);
        $em->flush();
        return new Response("");
    }
    public function sections($id = null)
    {
        $methods = $this->get('GeneralMethods');
        $sections = array();
        $em = $this->getDoctrine()->getManager();
        $methods->addSection($sections,"description","Description");
        $methods->addSection($sections,"filename","File Name");
        $methods->addSection($sections,"minute","Minute");
        $methods->addSection($sections,"hour","Hour");
        $methods->addSection($sections,"day","Day");
        $methods->addSection($sections,"month","Month");
        $methods->addSection($sections,"dayOfTheWeek","Day of the Week");
        $sections['minute']->value = $this->createArrayFromMinMax(0,59);
        $sections['hour']->value = array(0 => "Midnight",1 => "1AM", 2 => "2AM",3=>"3AM",4 => "4AM", 5 => "5AM", 6 => "6AM", 7 => "7AM", 8 => "8AM",
        9 => "9AM", 10 => "10AM", 11 => "11AM", 12 => "Noon", 13 => "1PM", 14 => "2PM", 15 => "3PM", 16 => "4PM", 17 => "5PM", 18 => "6PM", 19 => "7PM",
        20 => "8PM", 21 => "9PM", 22 => "10PM", 23 => "11PM" );
        $sections['day']->value = $this->createArrayFromMinMax(1,31);
        $sections['month']->value = array(1 => "January", 2 => "Feburary", 3 => "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");
        $sections['dayOfTheWeek']->value = array( 1 => "Monday", 2 => "Tuesday", 3 => "Wednesday", 4 => "Thursday", 5 => "Friday", 6 => "Saturday", 7 => "Sunday"); 

        if ($id != null)
        {
            $repository = $em->getRepository('classesclassBundle:cronTasks');
            $cron = $repository->findOneBy(array("id" => $id));
            $class_vars = get_class_vars(get_class($this));
            foreach($cron as $key => $value)
            {
                if ($key != "id" && $key != "active" && $value != null)
                $sections[$key]->currentValue = $value;
            }
        }

        return $sections;      
    }
    public function createArrayFromMinMax($min,$max)
    {
        $array = array();
        while ($min <= $max)
        {
            $array[$min] = $min;
            $min++;
        }
        return $array;
    }
}
?>