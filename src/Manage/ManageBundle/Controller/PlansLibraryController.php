<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Entity\plansLibraryDirectories;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Sessions\AdminBundle\Classes\adminsession;
use Permissions\RolesBundle\Classes\rolesPermissions;
use classes\classBundle\Entity\plansLibraryVideos;
use classes\classBundle\Entity\plansLibraryType;

class PlansLibraryController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("PlansLibraryVideos");

        return $this->render('ManageManageBundle:PlansLibrary:index.html.twig', array("session" => $session, "writeable" => $writeable, "roleType" => $session->roleType));
    }

    public function addVideoAction() {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directories = $repository->findAll();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $type = $repository->findAll();
        return $this->render('ManageManageBundle:PlansLibrary:addvideo.html.twig', array("directories" => $directories, "type" => $type));
    }

    public function addVideoSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $internalName = $request->request->get("internalName");
        $displayName = $request->request->get("displayName");
        $directory = $request->request->get("directory");
        $type = $request->request->get("type");
        $videoName = $request->request->get("videoName");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directoryid = $repository->findOneBy(array("directory" => $directory))->id;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $typeid = $repository->findOneBy(array("displayname" => $type))->id;

        $video = new plansLibraryVideos();
        $video->name = $internalName;
        $video->displayname = $displayName;
        $video->video = $videoName;

        $video->directoryid = $directoryid;
        $video->typeid = $typeid;
        $video->runtime = $request->request->get("runtime");
        $video->thumbnail = "";


        $em->persist($video);
        $em->flush();

        return new Response("Saved");
    }

    public function editVideoAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');
        $id = $request->query->get("id");
        $video = $repository->findOneBy(array('id' => $id));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directory = $repository->findOneBy(array("id" => $video->directoryid))->directory;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $type = $repository->findOneBy(array("id" => $video->typeid))->type;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directories = $repository->findAll();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $types = $repository->findAll();

        return $this->render('ManageManageBundle:PlansLibrary:editvideo.html.twig', array("currentvideo" => $video, "directory" => $directory, "type" => $type, "directories" => $directories, "types" => $types));
    }

    public function editVideoSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $internalName = $request->request->get("internalName");
        $displayName = $request->request->get("displayName");
        $directory = $request->request->get("directory");
        $type = $request->request->get("type");
        $videoName = $request->request->get("videoName");
        $id = $request->request->get("id");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directoryid = $repository->findOneBy(array("directory" => $directory))->id;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $typeid = $repository->findOneBy(array("displayname" => $type))->id;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');
        $video = $repository->findOneBy(array("id" => $id));
        $video->name = $internalName;
        $video->displayname = $displayName;
        $video->video = $videoName;
        $video->directoryid = $directoryid;
        $video->typeid = $typeid;
        $video->runtime = $request->request->get("runtime");

        $em->persist($video);
        $em->flush();

        return new Response("Saved");
    }

    public function playVideoAction(Request $request) {
        $mainDirectory = "https://c06cc6997b3d297a6674-653de9dab23a7201285f2586065c1865.ssl.cf1.rackcdn.com/video/library/";
        $m4v = "m4v/";
        $webmv = "webmv/";
        $mp4 = ".mp4";
        $webm = ".webm";

        $id = $request->query->get("id");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');
        $video = $repository->findOneBy(array('id' => $id));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directory = $repository->findOneBy(array("id" => $video->directoryid))->directory;

        $mp4link = $mainDirectory . $directory . "/" . $m4v . $video->video . $mp4;
        $webmlink = $mainDirectory . $directory . "/" . $webmv . $video->video . $webm;

        return $this->render('ManageManageBundle:PlansLibrary:playvideo.html.twig', array("mp4link" => $mp4link, "webmlink" => $webmlink));
    }

    public function editThumbnailAction(Request $request) {

        $id = $request->query->get("id");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');
        $video = $repository->findOneBy(array('id' => $id));


        return $this->render('ManageManageBundle:PlansLibrary:editthumbnail.html.twig', array("video" => $video));
    }

    public function editThumbnailSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $thumbnail = $request->request->get("thumbnail");
        $id = $request->request->get("id");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');
        $video = $repository->findOneBy(array("id" => $id));
        $video->thumbnail = $thumbnail;

        $em->persist($video);
        $em->flush();

        return new Response("Saved");
    }

    function deleteVideoAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');
        $video = $repository->findOneBy(array('id' => $request->query->get("id")));

        return $this->render('ManageManageBundle:PlansLibrary:deletevideo.html.twig', array("video" => $video));
    }

    public function deleteVideoSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryVideos');
        $video = $repository->findOneBy(array('id' => $id));

        $em->remove($video);
        $em->flush();
        return new Response("Deleted");

    }

    public function manageDirectoriesAction() {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");

        return $this->render('ManageManageBundle:PlansLibrary:directory/managedirectories.html.twig');

    }
    public function addDirectoryAction() {
        return $this->render('ManageManageBundle:PlansLibrary:directory/adddirectory.html.twig');
    }

    public function addDirectorySavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $directoryName = $request->request->get("directoryname");

        $toadd = new plansLibraryDirectories();
        $toadd->directory = $directoryName;

        $em->persist($toadd);
        $em->flush();

        return new Response("Saved");
    }

    public function editDirectoryAction(Request $request) {
        $id = $request->query->get("id");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directory = $repository->findOneBy(array('id' => $id));

        return $this->render('ManageManageBundle:PlansLibrary:directory/editdirectory.html.twig', array("currentdirectory" => $directory));
    }

    public function editDirectorySavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $directoryName = $request->request->get("directoryname");
        $id = $request->request->get("id");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directory = $repository->findOneBy(array("id" => $id));

        $directory->directory = $directoryName;

        $em->persist($directory);
        $em->flush();

        return new Response("Saved");

    }

    function deleteDirectoryAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directory = $repository->findOneBy(array('id' => $request->query->get("id")));


        return $this->render('ManageManageBundle:PlansLibrary:directory/deletedirectory.html.twig', array("directory" => $directory));
    }

    public function deleteDirectorySavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryDirectories');
        $directory = $repository->findOneBy(array("id" => $id));

        $em->remove($directory);
        $em->flush();
        return new Response("Deleted");
    }

    public function manageTypesAction() {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");

        return $this->render('ManageManageBundle:PlansLibrary:types/managetypes.html.twig');
    }

    public function addTypeAction() {
        return $this->render('ManageManageBundle:PlansLibrary:types/addtype.html.twig');
    }

    public function addTypeSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $typeName = $request->request->get("typename");
        $displayName = $request->request->get("displayname");

        $toadd = new plansLibraryType();
        $toadd->type = $typeName;
        $toadd->displayname = $displayName;

        $em->persist($toadd);
        $em->flush();

        return new Response("Saved");
    }

    public function editTypeAction(Request $request) {
        $id = $request->query->get("id");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $type = $repository->findOneBy(array('id' => $id));

        return $this->render('ManageManageBundle:PlansLibrary:types/edittype.html.twig', array("type" => $type));
    }

    public function editTypeSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $typeName = $request->request->get("typename");
        $displayName = $request->request->get("displayname");
        $id = $request->request->get("id");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $type = $repository->findOneBy(array("id" => $id));

        $type->type = $typeName;
        $type->displayname = $displayName;

        $em->persist($type);
        $em->flush();

        return new Response("Saved");
    }

    function deleteTypeAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $type = $repository->findOneBy(array('id' => $request->query->get("id")));

        return $this->render('ManageManageBundle:PlansLibrary:types/deletetype.html.twig', array("type" => $type));
    }

    public function deleteTypeSavedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansLibraryType');
        $type = $repository->findOneBy(array("id" => $id));

        $em->remove($type);
        $em->flush();
        return new Response("Deleted");
    }

}
?>