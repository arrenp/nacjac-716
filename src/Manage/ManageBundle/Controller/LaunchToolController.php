<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use classes\classBundle\Entity\launchUsers;
use Shared\General\GeneralMethods;
use Sessions\AdminBundle\Classes\adminsession;
/*
use classes\classBundle\Entity\plans;
use classes\classBundle\Entity\plansFunds;
use classes\classBundle\Classes\plansMethods;
 * 
 */
set_time_limit(0);
class LaunchToolController extends Controller
{
    public $sections;
    public $filename;
    public $filename2;
    public function indexAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $session->set("section","Manage");  
        $this->removeFile();

        return $this->render('ManageManageBundle:LaunchTool:index.html.twig',array("sections" => $this->sections,"id" => $request->query->get("id","")));
    }

    public function removeFile()
    {
        if (file_exists($this->filename))
        unlink($this->filename);
        if (file_exists($this->filename2))
        unlink($this->filename2);
    }
    public function init($page = "partnerUpload")
    {
        $this->sections = array();
        $methods = $this->get('GeneralMethods');
        if ($page == "partnerUpload")
        {
            $methods->addSection($this->sections,"firstName","First Name");
            $methods->addSection($this->sections,"planid","Plan ID");
            $methods->addSection($this->sections,"lastName","Last Name");
            $methods->addSection($this->sections,"partnerid","Partner Id");
            $methods->addSection($this->sections,"address","Address");
            $methods->addSection($this->sections,"city","City");
            $methods->addSection($this->sections,"state","State");
            $methods->addSection($this->sections,"telephone","Phone");
        }


        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $userid = "";
        if ($request->query->get("id","") != "")
        $userid = $request->query->get("id","");
        if ($request->request->get("id","") != "")
        $userid = $request->request->get("id","");       

        $this->filename = sys_get_temp_dir().'/planupload'.$userid.'.csv';
        $this->filename2 = sys_get_temp_dir().'/planuploaderrors'.$userid.'.csv';

    }

    public function writeTempFileAction()
    {
        $this->init();
        $this->removeFile();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $file = $request->files->get("upload-file");
        $tempfilecontents = file_get_contents($file);
        $fp = fopen($this->filename, 'w');
        fwrite($fp,$tempfilecontents);
        return new Response($this->displayTableBody());
    }

    public function displayTableBody($limit = null)
    {
        $this->init();
        $counter = 0;
        $csv = array_map('str_getcsv', file($this->filename));
        $tbody = "";
        $sectionCount = count($this->sections);
        foreach ($csv as $row)
        {
            if ($limit != null && $counter > $limit)
            return $tbody;
            if ($counter != 0)
            {
                $tbody = $tbody."<tr>";
                $minicounter = 0;
                foreach ($row as $key => $value)
                {
                    if ($minicounter < $sectionCount)
                    $tbody = $tbody.'<td>"'.$value.'"</td>';
                    $minicounter++;
                }
                $tbody = $tbody."</tr>";
            }
            $counter++;
        }
        return $tbody;       
    }

    public function readTempFileAction()
    {
        $this->init();
        $counter = 0;
        $csv = array_map('str_getcsv', file($this->filename));
        foreach ($csv as $row)
        {
            foreach ($row as $key => $value)
            {
                echo $key.",".$value."<br/>";
            }
            echo "<hr/>";
            $counter++;
        }
        echo "total: ".$counter;
    }

    public function stringToBoolean($data)
    {
        $data = strtolower(trim($data));
        if ($data == "yes" || $data == "y")
        return true;
        return false;
    }
    public function setBooleanString($data)
    {
        if ($this->stringToBoolean($data))
        return "yes";
        else
        return "no";
    }
    public function processUploadAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $header = explode(",",$request->request->get("header",""));
        $em = $this->getDoctrine()->getManager();
        $headerCount = count($header);


        $csv = array_map('str_getcsv', file($this->filename));
        foreach ($csv as $row)
        {
            $launchUsers = new launchUsers();
            $urlPass = $this->randSalt();
            
            for ($i = 0; $i < $headerCount; $i++)
            {
                if (isset($row[$i]))
                $launchUsers->{$header[$i]} = $row[$i];
            }
            $launchUsers->urlPass = $urlPass;
            $em->persist($launchUsers);
            
        }
        $em->flush();
        return new Response(""); 
    }
    
    public function randSalt() {
        $this->init();
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
        $size = strlen($chars);
        $str = '';
        
        for ($i = 0; $i < 6; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        

        return $str;
    } 
    
    public function singleEntryAction(){
        
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $user = new launchUsers();
        $arr =  $request->request->all();
        $urlPass = $this->randSalt();
        foreach ($arr as $key => $value)
        {
             $user->$key = $value;
        }
        $user->urlPass = $urlPass;
        $em->persist($user);
        $em->flush();

        return new Response('');
    }

    public function errorXmlAction()
    {
        $this->init();
        return new Response($this->filename2);
    }
 
}
