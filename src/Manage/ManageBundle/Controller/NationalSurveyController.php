<?php
namespace Manage\ManageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use classes\classBundle\Entity\SurveyEmployerPlans;
use classes\classBundle\Entity\surveyEmployers;

class NationalSurveyController extends Controller
{
    public function indexAction(Request $request)
    {
        $account = $this->get("AccountsService")->findOneBy(['id' => $request->query->get("userid") ]);
        if (!empty($account))
        {
            $this->get("session")->set("nationalSurveyUserId",$account->id);
            $this->get("session")->set("nationalSurveyPartnerId",$account->partnerid);
        }
        $this->setVariables();
        return $this->render('index.html.twig');
    }
    public function planListAction(Request $request)
    {
        $table = "SurveyEmployerPlans";
        $queryBuilder = $this->get("QueryBuilderDataTables")->queryBuilder($table);
        $queryBuilder->andWhere("a.partnerid = :partnerid");
        $queryBuilder->groupBy("a.planid");
        $queryBuilder->setParameters(["partnerid" => $this->partnerid]);
        $data = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder($table,["a.id","a.planid"],$request->request->all(),$queryBuilder,"distinct a.planid");
        $plans = $data['rows'];
        $response = $data['response'];
        $data = [];
        foreach ($plans as $plan)
        {
            $data[] =[
                $plan->id,
                $plan->planid,
                $this->render("configureButtons.html.twig",["plan" => $plan])->getContent()
            ];
        }
        $response['data'] = $data;
        return new JsonResponse($response);
    }
    public function addAction(Request $request)
    {
        return $this->render("add.html.twig");
    }
    public function addPlanListAction(Request $request)
    {
        $table = "plans";
        $queryBuilder = $this->get("QueryBuilderDataTables")->queryBuilder($table);
        $queryBuilder->leftJoin("classesclassBundle:SurveyEmployerPlans", "b", "WITH", "a.partnerid=b.partnerid and a.planid = b.planid");
        $queryBuilder->andWhere("a.userid = :userid");
        $queryBuilder->andWhere("a.deleted = 0");
        $queryBuilder->andWhere("b.id is null");
        $queryBuilder->setParameters(["userid" => $this->userid]);
        $data = $this->get("QueryBuilderDataTables")->jsonObjectQueryBuilder($table,["a.id","a.planid"],$request->request->all(),$queryBuilder,"distinct a.planid");
        $plans = $data['rows'];
        $response = $data['response'];
        $data = [];
        foreach ($plans as $plan)
        {
            $data[] =[
                $plan->id,
                $plan->planid,
                $this->render("addButtons.html.twig",["plan" => $plan])->getContent()
            ];
        }
        $response['data'] = $data;
        return new JsonResponse($response);
    }
    public function addPlanAction(Request $request)
    {
        $plan = $this->get("PlansService")->findOneBy(["id" => $request->request->get("planid") ]);
        $em = $this->getDoctrine()->getManager();
        $surveyEmployerPlans = new SurveyEmployerPlans();
        $surveyEmployerPlans->planid = $plan->planid;
        $surveyEmployerPlans->partnerid = $this->partnerid;        
        $surveyEmployerPlans->surveyEmployerId = 0;
        $em->persist($surveyEmployerPlans);    
        $em->flush($surveyEmployerPlans);
        return new Response("");
    }
    public function configureAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:SurveyEmployerPlans');
        $surveyPlan = $repository->findOneBy(array('id' => $request->get("id")));
        $plan = $this->get("PlansService")->findOneBy(["planid" => $surveyPlan->planid,"partnerid" => $surveyPlan->partnerid,"deleted" => 0  ]);
        $this->get("session")->set("nationalSurveyPlanId",$surveyPlan->planid);
        $this->setVariables();
        $surveyPlans = $repository->findBy(array('planid' => $this->planid,"partnerid" => $this->partnerid));
        $repository = $em->getRepository('classesclassBundle:surveyEmployers');
        foreach ($surveyPlans as &$surveyPlan)
        {
            $surveyPlan->employer = $repository->findOneBy(["id" => $surveyPlan->surveyEmployerId]) ? $repository->findOneBy(["id" => $surveyPlan->surveyEmployerId])->employerName : "";
        }
        $types = ["401k","403b","457"];
        return $this->render("configure.html.twig",["surveyPlans" => $surveyPlans,"types" => $types,"planid" => $this->planid,"plan" => $plan,"surveyPlanSelected" => $surveyPlan]);
    }
    public function saveConfigureAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:SurveyEmployerPlans');
        $surveyPlan = $repository->findOneBy(array('id' => $request->request->get("id")));
        $params = $request->request->all();
        $repository = $em->getRepository('classesclassBundle:surveyEmployers');
        $surveyEmployers = $repository->findOneBy(array('employerName' => $params['employer']));
        if (empty($surveyEmployers ))
        {
            if (empty($params['employer']))
                $surveyPlan->surveyEmployerId = 0;
            else
            {
                $surveyEmployers = new surveyEmployers();
                $surveyEmployers->employerName = $params['employer'];
                $em->persist($surveyEmployers);
                $em->flush();
            }  
        }
        foreach ($params as $field => $value)
        {
            if (empty($value))
                $value = null;
            $surveyPlan->{$field} = $value;
        }
        if (!empty($surveyEmployers))
            $surveyPlan->surveyEmployerId = $surveyEmployers->id;
        $em->flush();
        return new Response("");
    }
    public function deleteSurveyPlanAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:SurveyEmployerPlans');
        $surveyPlan = $repository->findOneBy(array('id' => $request->request->get("id")));       
        $em->remove($surveyPlan);
        $em->flush();
        $surveyPlan = $repository->findOneBy(array('partnerid' => $surveyPlan->partnerid,'planid' => $surveyPlan->planid));       
        return new JsonResponse($surveyPlan);
    }
    public function render($file,$params = [])
    {
        return parent::render("ManageManageBundle:NationalSurvey:".$file,$params);
    }
    public function setVariables()
    {
        $this->userid = $this->get("session")->get("nationalSurveyUserId") ? $this->get("session")->get("nationalSurveyUserId"): 0 ;
        $this->partnerid = $this->get("session")->get("nationalSurveyPartnerId") ? $this->get("session")->get("nationalSurveyPartnerId"): 0 ;
        $this->planid = $this->get("session")->get("nationalSurveyPlanId") ? $this->get("session")->get("nationalSurveyPlanId"): 0 ;
    }
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
       parent::setContainer($container);
       $this->setVariables();
    }
}
