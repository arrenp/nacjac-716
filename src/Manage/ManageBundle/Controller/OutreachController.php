<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Shared\OutreachBundle\Classes\outreach;
use classes\classBundle\Entity\outreachCategoriesDeny;
use classes\classBundle\Entity\outreachTemplatesDeny;
use classes\classBundle\Entity\outreachNewTemplates;
use classes\classBundle\Entity\outreachNewCategories;
use classes\classBundle\Entity\campaignsStats;
use classes\classBundle\Entity\campaigns;
use Sessions\AdminBundle\Classes\adminsession;
use Utilities\mailer\PHPMailer;
use Utilities\mailer\SMTP;
use Utilities\mailer\mailerInit;
use classes\classBundle\Classes\mailEngine;
use classes\classBundle\Classes\ga;
use Shared\General\GeneralMethods;
use classes\classBundle\Entity\mailChimpSignatures;
use classes\classBundle\Entity\mailChimpDisclaimers;
require_once(getcwd()."/../src/Utilities/dompdf/dompdf_config.inc.php");
class OutreachController extends Controller
{
    //old outreach
    public function indexAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $planid = $request->query->get("planid", "5990");
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $planid));
        $writeable = false;
        $managepage = true;
        
        return $this->render('SharedOutreachBundle:Default:index.html.twig', array('currentplan' => $currentplan, "writeable" => $writeable, "managepage" => $managepage));
    }

    public function vwiseIndexAction()
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVwiseTemplates');
        $templates = $repository->findBy(array());
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        foreach ($templates as $template)
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVwiseVideoTemplates');
            $templateVideo = $repository->findOneBy(array("id" => $template->videotemplateid));
            $template->video = "";
            if (isset($templateVideo->youtubeid))
            {
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVideoLibrary');
                $video = $repository->findOneBy(array("youtubeid" => $templateVideo->youtubeid));
                if ($video != null) $template->video = urlencode($video->filename);
            }
        }

        return $this->render('ManageManageBundle:Outreach:Vwise/index.html.twig', array("templates" => $templates));
    }

    public function vwiseRenderTemplateAction($type, $id)
    {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVwiseTemplates');
        $template = $repository->findOneBy(array("id" => $id));

        $template->link = $this->returnLink($template);

        return $this->returnTemplate($template, $type);
    }

    public function vwiseSendTestEmailFormAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $templateid = $request->query->get("id", "");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVwiseTemplates');
        $template = $repository->findOneBy(array('id' => $templateid));

        return $this->render('ManageManageBundle:Outreach:Vwise/testemail.html.twig', array("template" => $template));
    }

    public function vwiseSendTestEmailAction()
    {
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        $mailerInit = new mailerInit($mail);

        $request = Request::createFromGlobals();

        // the URI being requested (e.g. /about) minus any query parameters
        $request->getPathInfo();

        // retrieve GET and POST variables respectively
        $templateid = $request->request->get("id", "");
        $emails = $request->request->get("emails", "");



        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVwiseTemplates');
        //check if email address exists
        $template = $repository->findOneBy(array("id" => $templateid));
        ;


        //if email exists, send email
        if (isset($template))
        {
            try
            {

                $templateUrl = $this->generateUrl('_manage_outreach_vwise_render_template', array("type" => $template->type, "id" => $template->id), true);
                $emailContents = file_get_contents($templateUrl);
                //pass in name, password, id and email address into email template

                $emails = explode(",", $emails);

                for ($i = 0; $i < count($emails); $i++)
                {
                    $mail->AddAddress($emails[$i]);
                }
                echo "Message Sent OK<p></p>\n";
                $mail->Subject = $request->request->get("title", "");
                $mail->MsgHTML($emailContents);
                $mail->Send();
                //send response to user that email has been sent
                return new Response("sent");
            }
            catch (Exception $e)
            {
                return new Response("Error occured!: " . $e->getMessage());
            }
        }
        else  //else, inform user email does not exist
            return new Response("template does not exist");
    }

    public function vwiseEmbedCodeAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $templateid = $request->query->get("id", "");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVwiseTemplates');
        $template = $repository->findOneBy(array("id" => $templateid));
        ;
        $templateUrl = $this->generateUrl('_manage_outreach_vwise_render_template', array("type" => $template->type, "id" => $template->id), true);
        $emailContents = file_get_contents($templateUrl);

        return $this->render('ManageManageBundle:Outreach:Vwise/embedcode.html.twig', array("templateid" => $templateid, "emailContents" => $emailContents));
    }

    private function returnLink($template)
    {
        if ($template->videotemplateid != 0) $template->link = $this->generateUrl("_manage_outreach_video_landing_page") . "?id=" . $template->videotemplateid;

        if ($template->link == "") $template->link = "https://vwise.com";

        return $template->link;
    }

    public function videoLandingPageAction()
    {
        
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        if ($request->query->get("id") != "")
        {
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVwiseVideoTemplates');
            $videoTemplate = $repository->findOneBy(array('id' => $request->query->get("id")));
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachVideoLibrary');
            $currentVideo = $repository->findOneBy(array('youtubeid' => $videoTemplate->youtubeid));
            $mp4url = $currentVideo->mp4url;
            $webmurl = $currentVideo->webmurl;
            $params = array('mp4url' => $mp4url, 'webmurl' => $webmurl, 'videoTemplate' => $videoTemplate);
            if ($videoTemplate->redirect) $params["link"] = $videoTemplate->footerurl;           
        }
        else
        {
            $params = array();
            $params['mp4url'] = $request->query->get("mp4url");
            $params['webmurl'] = $request->query->get("webmurl");
            $params['poster'] = $request->query->get("poster","https://gateway.smartplanenterprise.com/video_hero.jpg");
        }
        
        $page = $request->query->get("page","videolandingpage");
        
        return $this->render('ManageManageBundle:Outreach:Vwise/'.$page.'.html.twig', $params);
    }

    private function returnTemplate($template, $type)
    {

        switch ($type)
        {
            case "general":
                return $this->generalTemplate($template);
            case "general left ribbion":
                return $this->generalTemplateRightRibbion($template);
            case "general right ribbion":
                return $this->generalTemplateRightRibbion($template);
            case "general double ribbion":
                return $this->generalTemplateDoubleRibbion($template);
            case "christmas 2013":
                return $this->christmas($template);
            case "static":
                return $this->staticTemplate($template);
            case "spanish":
                return $this->spanish($template);
            case "plain":
                return $this->plain($template);
            case "winacar":
                return $this->template($template, $type);
            case "getconnected":
                return $this->template($template, $type);
        }
        return new Response("invalid template type");
    }

    public function WinaCarAction()
    {
        return $this->render('ManageManageBundle:Outreach:Vwise/Templates/winacar.html.twig');
    }

    private function plain($template)
    {
        return $this->render('ManageManageBundle:Outreach:Vwise/Templates/plain.html.twig', array("template" => $template));
    }

    private function generalTemplate($template)
    {

        return $this->render('ManageManageBundle:Outreach:Vwise/Templates/general.html.twig', array("template" => $template));
    }

    private function template($template, $type)
    {
        return $this->render('ManageManageBundle:Outreach:Vwise/Templates/' . $type . '.html.twig', array("template" => $template));
    }

    private function generalTemplateLeftRibbion($template)
    {
        return $this->render('ManageManageBundle:Outreach:Vwise/Templates/general-left-ribbion.html.twig', array("template" => $template));
    }

    private function generalTemplateRightRibbion($template)
    {
        return $this->render('ManageManageBundle:Outreach:Vwise/Templates/general-right-ribbion.html.twig', array("template" => $template));
    }

    private function generalTemplateDoubleRibbion($template)
    {
        return $this->render('ManageManageBundle:Outreach:Vwise/Templates/general-double-ribbion.html.twig', array("template" => $template));
    }

    private function christmas($template)
    {
        return $this->render('ManageManageBundle:Outreach:Vwise/Templates/christmas.html.twig', array("template" => $template));
    }

    private function spanish($template)
    {
        return $this->render('ManageManageBundle:Outreach:Vwise/Templates/spanish.html.twig', array("template" => $template));
    }

    private function staticTemplate($template)
    {
        return new Response(file_get_contents($template->staticWebPageUrl));
    }
    //new outreach
    public function newIndexAction()
    {
        return $this->render('SharedOutreachBundle:Default:new/index.html.twig', $this->templateoptions());
    }
    
    public function templateoptions()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $planid = $request->query->get("planid", "5990");
        $session = $this->get('adminsession');
        $session->set("section", "emailCampaigns");
        $outreach = new outreach($this->getDoctrine(), 0);
        $categories = $outreach->allnewtemplates();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $currentplan = $repository->findOneBy(array('id' => $planid));
        $writeable = false;
        $managepage = true;
        $adviserurl = "";
        return  array('currentplan' => $currentplan, 'categories' => $categories, "writeable" => $writeable, "managepage" => $managepage,
        "adviserurl" => $adviserurl);
    }    
    public function templateListAction()
    {
        return $this->render('SharedOutreachBundle:Default:new/templatelist.html.twig', $this->templateoptions()); 
    }
    public function addCategoryAction()
    {
        return $this->render('SharedOutreachBundle:Default:new/addcategory.html.twig'); 
    }
    
    public function addCategorySavedAction(Request $request)
    {
        $outreachNewCategories = new outreachNewCategories();
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        foreach ($arr as $key => $value)
        {
            $outreachNewCategories->$key = $value;
        }
        $em->persist($outreachNewCategories);
        $em->flush();
        return new Response("");     
    }
    
    public function editCategoryAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewCategories');
        $outreachNewCategories = $repository->findOneBy(array('id' => $request->query->get("id"))); 
        return $this->render('SharedOutreachBundle:Default:new/editcategory.html.twig',array("category" => $outreachNewCategories)); 
    }    
    public function editCategorySavedAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewCategories');
        $outreachNewCategories = $repository->findOneBy(array('id' => $request->request->get("id")));        
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        foreach ($arr as $key => $value)
        {
            $outreachNewCategories->$key = $value;
        }
        $em->flush();
        return new Response("");     
    }    
    public function deleteCategoryAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewCategories');
        $outreachNewCategories = $repository->findOneBy(array('id' => $request->query->get("id"))); 
        return $this->render('SharedOutreachBundle:Default:new/deletecategory.html.twig',array("category" => $outreachNewCategories)); 
    }    
    public function deleteCategorySavedAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewCategories');
        $outreachNewCategories = $repository->findOneBy(array('id' => $request->request->get("id")));         
        $em = $this->getDoctrine()->getManager();
        $em->remove($outreachNewCategories);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $outreachNewTemplates = $repository->findBy(array('categoryid' => $request->request->get("id"))); 
        foreach ($outreachNewTemplates as &$template)
        $em->remove($template);
        $em->flush();
        return new Response("");     
    }     
    
    public function addTemplateAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewCategories');
        $outreachNewCategories = $repository->findOneBy(array('id' => $request->query->get("id"))); 
        return $this->render('SharedOutreachBundle:Default:new/addtemplate.html.twig',array("category" => $outreachNewCategories)); 
    }
    
    public function addTemplateSavedAction(Request $request)
    {
        $outreachNewTemplates = new outreachNewTemplates();
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        foreach ($arr as $key => $value)
        {
            $outreachNewTemplates->$key = $value;
        }
        $em->persist($outreachNewTemplates);
        $em->flush();
        return new Response("");     
    } 
    public function editTemplateAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewCategories');
        $outreachNewCategories = $repository->findAll();
       
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $outreachNewTemplates= $repository->findOneBy(array('id' => $request->query->get("id"))); 
        foreach ($outreachNewCategories as &$category)
        {
            if ($outreachNewTemplates->categoryid == $category->id)
            $category->selected = "selected";
            else
            $category->selected = "";
        }
        return $this->render('SharedOutreachBundle:Default:new/edittemplate.html.twig',array("template" => $outreachNewTemplates,"categories" => $outreachNewCategories)); 
    }    
    public function editTemplateSavedAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $outreachNewTemplates = $repository->findOneBy(array('id' => $request->request->get("id")));        
        $arr = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        foreach ($arr as $key => $value)
        {
            $outreachNewTemplates->$key = $value;
        }
        $em->flush();
        return new Response("");     
    }
    
    public function previewnofilterAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $outreachNewTemplates = $repository->findOneBy(array('id' => $request->query->get("id"))); 
        return new response($outreachNewTemplates->html);
    }
    
    public function previewAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $outreachNewTemplates = $repository->findOneBy(array('id' => $request->query->get("id"))); 
        $planid = $request->query->get("planid",5990);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $planid)); 
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $plan->userid)); 
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansMedia');
        $plansMedia = $repository->findOneBy(array('planid' => $plan->id)); 
        $dateStringPrint= "";
        if ($plansMedia != null)
        $dateStringPrint = $plansMedia->enddate_sc->format("n-j-Y");
        
        $adviserid = $request->query->get("adviserid",0);
        $templateid = $request->query->get("id");
        $templateid = 0;
        $logos = new \stdClass();
        
        $participantLoginPageUrl = $this->forward('PlansPlansBundle:Outreach:participantLoginWebsiteAddress', array("planid" => $planid, "adviserid" => $adviserid, "templateid" => $templateid))->getContent();
        
        $logos->leftImageUrl = $this->forward('PlansPlansBundle:Outreach:providerLogo', array("planid" => $planid))->getContent();
        $logos->rightImageUrl = $this->forward('PlansPlansBundle:Outreach:sponsorLogo', array("planid" => $planid))->getContent();
        
        $tags = get_meta_tags($this->generateUrl('_manage_outreach_new_preview_nofilter', array("id" => $request->query->get("id")), true));
        foreach ($tags as $name => $value)
        {
            if ($name == "provider_logo_url" && $value == 1)
            {
                $outreachNewTemplates->html = str_replace("<vwise_provider_logo_url/>", $logos->leftImageUrl, $outreachNewTemplates->html);
            }
            if ($name == "sponsor_logo_url" && $value == 1)
            {
                $outreachNewTemplates->html = str_replace("<vwise_sponsor_logo_url/>", $logos->rightImageUrl, $outreachNewTemplates->html);
            }
            if ($name == "participant_login_page_url" && $value == 1)
            {
                $outreachNewTemplates->html = str_replace("<vwise_participant_login_page_url/>", $participantLoginPageUrl, $outreachNewTemplates->html);
            }
            if (substr_count($name,"videopage") > 0)
            {
                $landingpageid = str_replace("videopage","",$name);
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:videoLandingPages');
                $videoLandingPage = $repository->findOneBy(array('id' => $landingpageid)); 
                $url = $this->generateUrl('_manage_videoLandingPage_preview',array("name" => $videoLandingPage->name,"id" => $videoLandingPage->id),true)."&smartplanid=".$planid."&userid=".$plan->userid."&templateid=".$templateid."&adviserid=".$adviserid;
                $outreachNewTemplates->html = str_replace("<vwise_".$name."/>",$url, $outreachNewTemplates->html);
            }
            if ($name == "domain" && $value == 1)
            {
                $outreachNewTemplates->html = str_replace("<vwise_domain/>", $request->server->get('HTTP_HOST'), $outreachNewTemplates->html);
            }
            if ($name == "planid" && $value == 1)
            {
                $outreachNewTemplates->html = str_replace("<vwise_planid/>", $planid, $outreachNewTemplates->html);
            }
            if ($name == "company"  && $value == 1)
            {
                $outreachNewTemplates->html = str_replace("<vwise_company/>", $account->company, $outreachNewTemplates->html);
            }
            if ($name == "planname"  && $value == 1)
            {
                $outreachNewTemplates->html = str_replace("<vwise_planname/>", $plan->name, $outreachNewTemplates->html);
            }
            if ($name == "providername"  && $value == 1)
            {
                $outreachNewTemplates->html = str_replace("<vwise_providername/>", $account->partnerid, $outreachNewTemplates->html);
            } 
            if ($name == "smartconversiondate"  && $value == 1)
            {
                $outreachNewTemplates->html = str_replace("<vwise_smartconversiondate/>",$dateStringPrint , $outreachNewTemplates->html);
            } 
            if (substr_count($name,"redirect_") > 0)
            {
                $outreachNewTemplates->html = str_replace("<vwise_".$name."/>",$request->query->get($name,"https://smartplanenterprise.com"),$outreachNewTemplates->html);
            }
        }
        return new response($outreachNewTemplates->html);
    }
    public function embedcodeAction(Request $request)
    {
        $html = $this->previewAction($request)->getContent();
        $html =  '<strong>Select in box to get code</strong><br/><textarea style ="width:100%;height:200px;resize:none" id ="codebox" onclick ="select_all(this.id)">'.$html.'</textarea>';
        return new Response($html);
    }
    public function previewFormAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $outreachNewTemplates = $repository->findOneBy(array('id' => $request->query->get("id"))); 
        $return = array("currentTemplate" => $outreachNewTemplates,"campaignpreview" => $request->query->get("campaignpreview",0),"planid" => 5990);
        if ($request->query->get("userid","") != "")
        {
            $return['userid'] = $request->query->get("userid","");
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
            $plan = $repository->findOneBy(array('userid' => $request->query->get("userid",""),"deleted" => 0),array("id" => "desc"));
            if ($plan != null)
            $return['planid'] = $plan->id;
        }
        return $this->render('SharedOutreachBundle:Default:new/previewform.html.twig',$return);        
    }
    public function emailFormAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $outreachNewTemplates = $repository->findOneBy(array('id' => $request->query->get("id"))); 
        return $this->render('SharedOutreachBundle:Default:new/emailform.html.twig',array("currentTemplate" => $outreachNewTemplates));        
    }    
    
    public function emailAction(Request $request)
    {
        $emailContents = $this->previewAction($request)->getContent();
        $emails = explode(",",$request->query->get("emails"));
        foreach ($emails as $email)
        {
            $email = trim($email);
            $message = \Swift_Message::newInstance()
            ->setSubject($request->query->get("title"))
            ->setFrom('info@smartplanenterprise.com')
            ->setTo($email)
            ->setBody(
            $emailContents,'text/html');
            $this->get('mailer')->send($message); 
        }
        return new Response("");
    }
    public function deleteTemplateFormAction(request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $outreachNewTemplates = $repository->findOneBy(array('id' => $request->query->get("id"))); 
        return $this->render('SharedOutreachBundle:Default:new/deletetemplate.html.twig',array("currentTemplate" => $outreachNewTemplates));        
    }
    public function deleteTemplateAction(request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $outreachNewTemplates = $repository->findOneBy(array('id' => $request->request->get("id"))); 
        $em->remove($outreachNewTemplates);
        $em->flush();
        return new Response("");
    }
    
    public function migrateTemplatesAction()
    {
        $em = $this->getDoctrine()->getManager();
        set_time_limit(0);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewCategories');
        $categories = $repository->findAll();        
        foreach ($categories as &$category)
        {       
            $em->remove($category);
        }
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $templates = $repository->findAll();
        foreach ($templates as &$template)
        {
            $em->remove($template);
        }
        $em->flush();
        
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachCategories');
        $categories = $repository->findAll();
        foreach ($categories as $category)
        {
            $newcategory = new outreachNewCategories();
            $metadata = $em->getClassMetaData(get_class($newcategory));
            $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
            foreach ($category as $key => $value)
            {
                $newcategory->$key = $value;
            }
            $em->persist($newcategory);
        }
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachTemplates');
        $templates = $repository->findAll();
        foreach ($templates as $template)
        {
            $newtemplate = new outreachNewTemplates();
            $metadata = $em->getClassMetaData(get_class($newtemplate));
            $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
            foreach ($template as $key => $value)
            {
                $newtemplate->$key = $value;
                $newtemplate->html = file_get_contents("http://localhost/admin/plans/outreach/templates/".str_replace(" ","%20",$template->category)."/".str_replace(" ","%20",$template->filename)."?templateid=".$template->id."&planid=5990");
                $newtemplate->html = str_replace("<head>",'
                    <head><meta name="provider_logo_url" content="1" >
                    <meta name="sponsor_logo_url" content="1" >
                    <meta name="domain" content="1"> 
                    <meta name="participant_login_page_url" content="1">
                    <meta name="videopage3" content="1">
                    <meta name="videopage999" content="1">
                    <meta name="videopage1000" content="1">
                    <meta name="planid" content="1">
'                   
                    
                    
                        , $newtemplate->html);  
                $newtemplate->html = str_replace("http://localhost","https://<vwise_domain/>",$newtemplate->html);
                $newtemplate->html = str_replace("&lt;vwise_videopage3/&gt;","<vwise_videopage3/>",$newtemplate->html);
                $newtemplate->html = str_replace("&lt;vwise_videopage999/&gt;","<vwise_videopage999/>",$newtemplate->html);
                $newtemplate->html = str_replace("&lt;vwise_videopage1000/&gt;","<vwise_videopage1000/>",$newtemplate->html);
                $newtemplate->html = str_replace("&lt;vwise_domain/&gt;","<vwise_domain/>",$newtemplate->html);
                $newtemplate->html = str_replace("&lt;vwise_planid/&gt;","<vwise_planid/>",$newtemplate->html);
                $newtemplate->html = str_replace("http://userurl2.com","<vwise_participant_login_page_url/>",$newtemplate->html);

                
            }
            $em->persist($newtemplate);
            $em->flush();
        }       
        
        
        return new Response("");
    }
        
    public function createCampaignAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "emailCampaigns");
        $sections = $this->campaignSettingsSections();
        $campaignTypes = $this->campaignTypes();
        $mailEngineTypes = array();
        $mailEngineTypes['channelMarketing'] =$this->channelMarketingTypes();
        $campaign = $this->returnCampaign();
        return $this->render('ManageManageBundle:Outreach:campaigns/index.html.twig',array("sections" => $sections,"campaignTypes" => $campaignTypes,"mailEngineTypes" =>$mailEngineTypes,"edit" => 0,"campaign" => $campaign));
    }
    public function editCampaignAction(Request $request)
    {
        $session = $this->get('adminsession');
        $session->set("section", "emailCampaigns");
        $sections = $this->campaignSettingsSections($request->query->get("id"));
        $campaignTypes = $this->campaignTypes($request->query->get("id"));
        $mailEngineTypes = array();
        $mailEngineTypes['channelMarketing'] =$this->channelMarketingTypes($request->query->get("id"));
        $campaign = $this->returnCampaign($request->query->get("id"));
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
       // echo $mailEngine->cancelCampaign(array("id" => $request->query->get("id")));
        //return new Response("");
        return $this->render('ManageManageBundle:Outreach:campaigns/index.html.twig',array("sections" => $sections,"campaignTypes" => $campaignTypes,"mailEngineTypes" =>$mailEngineTypes,"edit" => 1,"id" =>$request->query->get("id"),"campaign" => $campaign));
    }
    public function viewUsersAction(Request $request)
    {
        $campaign = $this->returnCampaign($request->query->get("id"));
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        $users = $mailEngine->viewUsers(array("campaignid" => $request->query->get("id")));
        
        if (count($users) == 0)
        return new Response("No Users");
        else
        return $this->render('ManageManageBundle:Outreach:campaigns/viewusers.html.twig',array("users" => $users));
        
        //return new Response("test");
    }
    public function sendCampaignAction(Request $request)
    {
        $params = $request->request->all();
        if (count($params) == 0)
        $params = $request->query->all();
        $campaign = $this->returnCampaign($params['id']);
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        return new Response($mailEngine->sendCampaign($params));   
    }
    public function cancelCampaignAction(Request $request)
    {
        $params = $request->request->all();
        $campaign = $this->returnCampaign($request->request->get("id"));
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        return new Response($mailEngine->cancelCampaign($params)); 
    }
    public function campaignTypes($campaignid = 0)
    {    
        $sections = array();
        $campaign = $this->returnCampaign($campaignid);
        $sections['channelMarketing']['name'] = "Channel Marketing";
        $sections['channelMarketing']['sections'] = $this->channelMarketingTypes($campaignid);
        $sections['channelMarketing']['selected'] = "";
        $sections['channelMarketing']['display'] = "none";
        foreach ($sections as &$type)
        {
            foreach ($type['sections'] as &$section)
            {
                if ($section['name'] == $campaign->mailEngine)
                {
                    $type['selected'] = "selected";
                    $type['display'] = "block";
                }
            }
        }
        return $sections;
    }
    public function channelMarketingTypes($campaignid = 0)
    {
        $sections = array();
        $campaign = $this->returnCampaign($campaignid);
        $sections['iContact']['name'] = "iContact";
        $sections['mailgunEngine']['name'] = "mailgunEngine";
        foreach ($sections as &$section)
        {
            $section['selected'] = "";
            if ($section['name'] == $campaign->mailEngine)
            $section['selected'] = "selected";
        }
        return $sections;
    }
    public function returnCampaign($campaignid = 0)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:campaigns');
        $campaign = $repository->findOneBy(array("id" =>$campaignid));
        if ($campaign == null)
        $campaign = new campaigns();
        return $campaign;
        
    }
    public function campaignSettingsSections($campaignid = 0)
    {
        $sections = array();
        $em = $this->getDoctrine()->getManager();
        $campaign = $this->returnCampaign($campaignid);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:campaignsCategories');
        $categories = $repository->findAll();
        $categoryArray["selectedIndex"] = "";
        foreach ($categories as $category)
        {
            $categoryArray[$category->id] = $category->name;
            if ($category->id == $campaign->categoryid)
            $categoryArray["selectedIndex"] = $category->id;
        }
        

        
        $outreach = new outreach($this->getDoctrine(), 0);
        $categories = $outreach->allnewtemplates();
        $templateArray["selectedIndex"] = "";
        foreach ($categories as $category)
        {
            $templateArray["category".$category->id] = "--------------".$category->name."--------------";
            foreach ($category->templates as $template)
            {
                $templateArray[$template->id] = $template->name;
                if ($template->id == $campaign->templateid)
                $templateArray["selectedIndex"] = $template->id;
            }
        }
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:mailChimpSignatures');
        $signatures = $repository->findAll(); 
        $signatureArray["selectedIndex"] = "";
        foreach ($signatures as $signature)
        {
            $signatureArray[$signature->id] = $signature->title;
            if ($signature->id == $campaign->signatureid)
            $signatureArray["selectedIndex"] = $signature->id;           
        }
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:mailChimpDisclaimers');
        $disclaimers = $repository->findAll(); 
        $disclaimerArray["selectedIndex"] = "";
        foreach ($disclaimers as $disclaimer)
        {
            $disclaimerArray[$disclaimer->id] = $disclaimer->title;
            if ($disclaimer->id == $campaign->disclaimerid)
            $disclaimerArray["selectedIndex"] = $disclaimer->id;
        }   
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $accounts = $repository->findAll(); 
        $accountsArray["selectedIndex"] = "";
        foreach ($accounts as $account)
        {
            $accountsArray[$account->id] = $account->id." - ".$account->partnerid;;
            if ($account->id == $campaign->userid)
            $accountsArray["selectedIndex"] = $account->id;
        }
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:communicationSpecialist');
        $communicationSpecialists = $repository->findAll(); 
        $communicationSpecialistsArray["selectedIndex"] = "";
        foreach ($communicationSpecialists as $communicationSpecialist)
        {
            $communicationSpecialistsArray[$communicationSpecialist->id] = $communicationSpecialist->emailAddress;
            if ($communicationSpecialist->id == $campaign->communicationSpecialist)
            $communicationSpecialistsArray["selectedIndex"] = $communicationSpecialist->id;
        }         
        
        $this->addCampaignSettingsSection($sections,"categoryid","Campaign Category","select",$categoryArray);
        $this->addCampaignSettingsSection($sections,"title","Campaign Title","text",$campaign->title);
        $this->addCampaignSettingsSection($sections,"subject","Subject","text",$campaign->subject);
        $this->addCampaignSettingsSection($sections,"description","Description","text not required",$campaign->description);
        $this->addCampaignSettingsSection($sections,"tags","Tags","text not required",$campaign->tags);
        $this->addCampaignSettingsSection($sections,"fromEmail","From Address","text",$campaign->fromEmail);
        $this->addCampaignSettingsSection($sections,"fromName","From Name","text",$campaign->fromName);
        $this->addCampaignSettingsSection($sections,"userid","Account","select",$accountsArray);
        $this->addCampaignSettingsSection($sections,"templateid","Select Template","select",$templateArray);
        $this->addCampaignSettingsSection($sections,"signatureid","Signature","select not required",$signatureArray);
        $this->addCampaignSettingsSection($sections,"disclaimerid","Disclaimer","select not required",$disclaimerArray);
        $this->addCampaignSettingsSection($sections,"planid","Planid","hidden",$campaign->planid);
        $this->addCampaignSettingsSection($sections,"adviserid","adviserid","hidden",$campaign->adviserid);
        $this->addCampaignSettingsSection($sections,"mailEngine","mailEngine","hidden",$campaign->mailEngine);
        $this->addCampaignSettingsSection($sections,"communicationSpecialist","Communication Specialist","select",$communicationSpecialistsArray);
        return $sections;
    }
    public function addCampaignSettingsSection(&$sections,$name,$description,$type,$value,$help = "")
    {
        $sections[$name]['name'] = $name;
        $sections[$name]['description'] = $description;
        $sections[$name]['type'] = $type;
        $sections[$name]['value'] = $value;
        $sections[$name]['help'] = $help;
    }
    public function createCampaignSavedAction(Request $request)
    {
        $dbal = $this->get('doctrine.dbal.default_connection');
        if ($request->request->get("mailEngine","") != "")
        $mailEngineName = $request->request->get("mailEngine");
        else
        {
            $sql = "SELECT * FROM campaignsMailEngines ORDER BY lastused LIMIT 1";
            $mailEngineName = $dbal->fetchAll($sql)[0]['name'];
        }
        $mailEngine = new mailEngine($mailEngineName,$this);
        $params =  $request->request->all();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:outreachNewTemplates');
        $template = $repository->findOneBy(array('id' => $request->request->get("templateid")));
        $planid = $request->request->get("planid","5990");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan= $repository->findOneBy(array('id' => $planid));
        $params['userid'] = $plan->userid;
        $params['textBody'] ="text body";
        if (!isset($params['html']))
        {
            $params['html'] = file_get_contents($this->generateUrl('_manage_outreach_new_preview', array("id" => $request->request->get("templateid"),"planid" => $plan->id,"userid" => $plan->userid), true));
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:mailChimpDisclaimers');
            $disclaimer= $repository->findOneBy(array('id' => $request->request->get("disclaimerid","")));
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:mailChimpSignatures');
            $signature= $repository->findOneBy(array('id' => $request->request->get("signatureid","")));
            if ($disclaimer != null)
            $params['html'] = str_replace("<body>",'<body>'.$this->appendToCampaignHtml($disclaimer->html),$params['html']);
            if ($signature != null)
            $params['html'] = str_replace("</body>",$this->appendToCampaignHtml($signature->html)."</body>",$params['html']);
        }
        if ($request->request->get("id","") == "")
        {
            $sql = "UPDATE campaignsMailEngines set lastused = '".time()."' WHERE name = '".$mailEngineName."'";
            $dbal->executeQuery($sql);
            $campaignid = $mailEngine->createCampaign($params);
        }
        else
        {
            $campaignid = $mailEngine->updateCampaign($params); 
        }
        return new Response($campaignid);
    }
    function appendToCampaignHtml($message)
    {
       return '<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td>
                <table style="margin:auto; width:624px;">
                    <tr>
                        <td><p style="line-height:12px; height:12px; color:#999999; font-size:9px; text-align:center!important; font-family:Gotham, \'Helvetica Neue\', Helvetica, Arial, sans-serif;">'.$message.'</p></td>                    
                    </tr>
                </table> 
            </td>                    
        </tr>
        </table>';

        
    }
    public function createListAction(Request $request)
    {
        $campaignid = $request->request->get("campaignid");
        $campaign = $this->returnCampaign($campaignid);
        $fileContent = $request->files->get("fileContent");
        $csvExplode = explode("\n", str_replace("\r", "\n", file_get_contents($fileContent)));
        $params['emails'] = array();
        $params['campaignid'] = $campaign->id;
        $counter = 0;
        foreach ($csvExplode as $csv)
        {
            $csvRowExplode = explode(",",$csv);
            $params['emails'][$counter]['emailAddress'] = $csvRowExplode[0];
            if (isset($csvRowExplode[1]))
            $params['emails'][$counter]['firstName'] = $csvRowExplode[1];
             if (isset($csvRowExplode[2]))
            $params['emails'][$counter]['lastName'] = $csvRowExplode[2];
            $counter++;
        }
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        $mailEngine->createList($params);
        return new Response("");
    }
    public function sendTestAction(Request $request)
    {
        
        $emails = explode("\n",$request->request->get("emails"));
        $campaign = $this->returnCampaign($request->request->get("id"));
        foreach ($emails as $email)
        {
            $from = $campaign->fromEmail;
            $email = trim($email);
            $message = \Swift_Message::newInstance()
            ->setSubject($campaign->subject)
            ->setFrom($from)
            ->setSender($from)
            ->setTo($email)
            ->setBody(
            $campaign->html,'text/html');
            $this->get('mailer')->send($message); 
        }
        return new Response("sent");
    }
    public function previewTemplateAction(Request $request)
    {
        $campaignid = $request->query->get("id");
        $params =  $request->query->all();
        $campaign = $this->returnCampaign($campaignid);
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        return  new Response($mailEngine->previewTemplate($params));
    }
    public function previewPropertiesAction(Request $request)
    {
        $campaignid = $request->query->get("id");
        $params =  $request->query->all();
        $campaign = $this->returnCampaign($campaignid);
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        $properties = $mailEngine->previewProperites($params);
        return $this->render('ManageManageBundle:Outreach:campaigns/previewproperties.html.twig',array("properties" => $properties));
    }
    
    public function assetManagerAction()
    {
        return $this->render('ManageManageBundle:Outreach:assetmanager/index.html.twig');
    }
    public function assetManagerEmbededAction()
    {
        
        return $this->render('ManageManageBundle:Outreach:assetmanager/body.html.twig');
    }
    public function assetManagerAddAssetAction()
    {
        return $this->render('ManageManageBundle:Outreach:assetmanager/add.html.twig');
    }
    public function assetManagerEditAssetAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:assets');
        $assets = $repository->findOneBy(array('id' => $request->query->get("id")));
        return $this->render('ManageManageBundle:Outreach:assetmanager/edit.html.twig',array("assets" => $assets));
    } 
    public function assetManagerDeleteAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:assets');
        $assets = $repository->findOneBy(array('id' => $request->query->get("id")));
        return $this->render('ManageManageBundle:Outreach:assetmanager/delete.html.twig',array("assets" => $assets));        
    }
    public function assetManagerDeleteSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:assets');
        $assets = $repository->findOneBy(array('id' => $request->request->get("id")));
        $em->remove($assets);
        $em->flush();
        return new Response("");
    }
    public function listCampaignsAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "emailCampaigns");
        return $this->render('ManageManageBundle:Outreach:campaigns/listcampaigns.html.twig');
    }
    
    public function copyCampaignAction(Request $request)
    {
        $campaignid = $request->query->get("id");
        $campaign = $this->returnCampaign($campaignid);
        
        return $this->render('ManageManageBundle:Outreach:campaigns/copy.html.twig',array("campaign" => $campaign));
    }
    public function copyCampaignSavedAction(Request $request)
    {
        $campaignid = $request->request->get("id");
        $campaign = $this->returnCampaign($campaignid);
        $params = json_decode(json_encode($campaign), true);
        $dbal = $this->get('doctrine.dbal.default_connection');
        foreach ($params as $key => $value)
        {
            if (substr_count($key,"mailEngine") == 1 && $key != "mailEngine")
            unset($params[$key]);
        }
        unset($params['sentDate']);
        $params['title'] = $params['title']." copy";
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        $copycampaignid = $mailEngine->createCampaign($params);
        $params = null;
        $params = array();
        $params['campaignid'] = $copycampaignid;
        $sql = "SELECT * FROM campaignsLists WHERE campaigns LIKE '%-".$campaignid."-%'";
        $campaignsLists = $dbal->fetchAll($sql);
        $counter = 0;
        foreach ($campaignsLists as $campaignsList)
        {
            $params['emails'][$counter] = $campaignsList;
            $counter++;
        }
        $mailEngine->createList($params);
        return new Response("saved");
    }
    public function getDescriptionAction(Request $request)
    {
        $campaignid = $request->query->get("id");
        return new Response($this->returnCampaign($campaignid)->description);
    }
    public function updateCampaignStatsAction()
    {
        $dbal = $this->get('doctrine.dbal.default_connection');
        $dateStart = new \DateTime("now");
        $dateStart->sub( new \DateInterval('P2D') );
        $dateStart = $dateStart->format("Y-m-d H:i:s");
        $dateEnd =  new \DateTime("now");
        $dateEnd = $dateEnd->format("Y-m-d H:i:s");
        
        //echo $date;
        
        $sql = "SELECT * FROM campaigns WHERE sent = 1 AND  sentDate >= '".$dateStart."' AND sentDate <= '".$dateEnd."'";
        $campaings = $dbal->fetchAll($sql);
        foreach ($campaings as $campaign)
        {
            $this->updateCampaignStat($campaign);
        }
        
        return new Response("");
    }
    
    function updateCampaignStat($campaign)
    {
        try
        {
            $em = $this->getDoctrine()->getManager();

            $mailEngine = new mailEngine($campaign['mailEngine'],$this);
            $params = array("id" => $campaign['id']);
            $emailStats = $mailEngine->getStats($params);  

            $add = false;
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:campaignsStats');
            $stats = $repository->findOneBy(array('campaignid' => $campaign['id'] )); 
            if ($stats == null)
            {
                $stats = new campaignsStats();
                $add = true;
            }
            $stats->campaignid = $campaign['id'];
            if ($emailStats->delivered > $stats->sends)
            $stats->sends = $emailStats->delivered;
            if ($emailStats->opens->unique > $stats->uniqueOpens)
            $stats->uniqueOpens = $emailStats->opens->unique;
            if ($emailStats->clicks->unique > $stats->uniqueClicks)
            $stats->uniqueClicks = $emailStats->clicks->unique;
            if ($emailStats->bounces > $stats->bounces)
            $stats->bounces = $emailStats->bounces;
            if ($emailStats->unsubscribes > $stats->unsubscribes)
            $stats->unsubscribes = $emailStats->unsubscribes;
            if ($add)
            $em->persist($stats);
            $em->flush($stats);   
        }
        catch(Exception $e)
        {
            
        }
        
    }
    public function getStatsAction(Request $request)
    {
        $campaignid = $request->query->get("id");
        $dbal = $this->get('doctrine.dbal.default_connection');
        $sql = "SELECT * FROM campaigns WHERE id = ".$campaignid;
        $campaign = $dbal->executeQuery($sql)->fetch();
        $this->updateCampaignStat($campaign); 
        $dbal = $this->get('doctrine.dbal.default_connection');
        $campaign = $this->returnCampaign($campaignid);
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $campaign->planid ));
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        $params["id"]  = $campaignid;
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:campaignsStats');
        $stats = $repository->findOneBy(array('campaignid' => $campaignid ));
        
        if ($stats == null)
        {
            $emailStats = new \stdClass();
            $emailStats->delivered = 0;
            $emailStats->opens = new \stdClass();
            $emailStats->opens->unique = 0;
            $emailStats->clicks = new \stdClass();
            $emailStats->clicks->unique = 0;
            $emailStats->bounces = 0;
            $emailStats->unsubscribes = 0;   
        }
        
        
        else
        {
            $emailStats = new \stdClass();
            $emailStats->delivered = $stats->sends;
            $emailStats->opens = new \stdClass();
            $emailStats->opens->unique = $stats->uniqueOpens;
            $emailStats->clicks = new \stdClass();
            $emailStats->clicks->unique = $stats->uniqueClicks;
            $emailStats->bounces = $stats->bounces;
            $emailStats->unsubscribes = $stats->unsubscribes;
        }
        
        
        //$emailStats = $mailEngine->getStats($params);
        $graphs = array();
        $graphs['email']['json'] = '
                {
           "chart": {
                "type": "column"
            },
           "title": {
                "text": "Email Stats"
            },
            "xAxis": {
                "categories": ["Stats"]
              
            },
            "yAxis": {

                "title": {
                    "text": "Total Emails"
                }
            } ,   
            "credits": {
                "enabled": false
            },
            "series": 
            [
                {
                    "name":"Sends",
                    "data": ['.$emailStats->delivered.']
                },
                {
                    "name":"Unique Opens",
                    "data": ['.$emailStats->opens->unique.']
                },
                {
                    "name":"Unique Clicks",
                    "data": ['.$emailStats->clicks->unique.']
                },  
                {
                    "name":"Bounces",
                    "data": ['.$emailStats->bounces.']
                },
                {
                    "name":"Unsubscribes",
                    "data": ['.$emailStats->unsubscribes.']
                }                  
            ]
        }    
                ';
        
        $sqlLanding[0] = "SELECT COUNT(*) as total,max(watched) as maxwatched,min(watched) as minwatched,avg(watched) as avgwatched FROM  videoLandingPagesStats WHERE campaignid LIKE '%".$campaign->gacampaignid."%'";
        $sqlLanding[1] = "SELECT COUNT(*) as watched FROM  videoLandingPagesStats WHERE campaignid LIKE '%".$campaign->gacampaignid."%' AND complete = 1";
        $landingStats = array();
        foreach ($sqlLanding as $sql)
        $landingStats = array_merge($landingStats,$dbal->fetchAll($sql)[0]);
        
        $formatKeys = array("minwatched","avgwatched","maxwatched");
        foreach ($formatKeys as $key)
        $landingStats[$key] = number_format($landingStats[$key],2);   
        
        $graphs['landing']['json'] = '
                {
           "chart": {
                "type": "column"
            },
           "title": {
                "text": "Landing Stats Totals"
            },
            "xAxis": {
                "categories": ["Stats"]
            },
            "yAxis": {

                "title": {
                    "text": "Total Visits"
                }
            } ,   
            "credits": {
                "enabled": false
            },
            "series": 
            [
                {
                    "name":"Total",
                    "data": ['.$landingStats['total'].']
                },
                {
                    "name":"Watched Completely",
                    "data": ['.$landingStats['watched'].']
                }              
            ]
        }    
                ';        
            $graphs['landing2']['json'] = '
                {
           "chart": {
                "type": "column"
            },
           "title": {
                "text": "Landing Stats Times"
            },
            "xAxis": {
                "categories": ["Stats"]
            },
            "yAxis": {

                "title": {
                    "text": "Total Time (Seconds)"
                }
            } ,   
            "credits": {
                "enabled": false
            },
            "series": 
            [

                {
                    "name":"Max Time Spent",
                    "data": ['.$landingStats['maxwatched'].']
                },  
                {
                    "name":"Min Time Spent",
                    "data": ['.$landingStats['minwatched'].']
                },
                {
                    "name":"Avg. Time Spent",
                    "data": ['.$landingStats['avgwatched'].']
                }                  
            ]
        }    
                ';         
        $startDate = "2005-01-01";
        $endDate = date('Y-m-d');

        $ga = new ga("99605889");
        $campaignstring = $plan->planid."_".$plan->partnerid."_".$campaign->gacampaignid;
        //echo $campaignstring;
        $date = date('Y-m-d');
        $date = strtotime($date);
        $date = strtotime("+1 day", $date);
        $date =  date('Y-m-d', $date);
        $params = array(
            'metrics' => 'ga:sessions,ga:users,ga:pageviews,ga:pageviewsPerSession,ga:sessionDuration,ga:bounces',
            'max-results' => 100000,
            'start-date' => "2005-01-01",
            'end-date' => $date,
            'dimensions' => 'ga:customVarValue2',
            'filters' => 'ga:customVarValue2==' . $campaignstring
        );


        $gavisitsApi = $ga->query($params)['totalsForAllResults'];
        foreach ($gavisitsApi as $key => $value)
        {
            $gavisits[str_replace("ga:","",$key)] = $value;
        }
        //var_dump($gavisits);
        $gavisits['pageviewsPerSession'] = number_format($gavisits['pageviewsPerSession'],2);       
        $gavisits['sessionDuration'] = number_format($gavisits['sessionDuration']/60,2);
        $gavisits['sessionDurationPerPage'] = 0;
        if ($gavisits['pageviewsPerSession'] > 0)
        $gavisits['sessionDurationPerPage'] =number_format($gavisits['sessionDuration']/$gavisits['pageviewsPerSession'],2);
        $graphs['google']['json'] = '
                {
           "chart": {
                "type": "column"
            },
           "title": {
                "text": "Google Stats Totals"
            },
            "xAxis": {
                "categories": ["Stats"]
            },
            "yAxis": {

                "title": {
                    "text": "Total Visits"
                }
            } ,   
            "credits": {
                "enabled": false
            },
            "series": 
            [
                {
                    "name":"Sessions",
                    "data": ['.$gavisits['sessions'].']
                },
                {
                    "name":"Users",
                    "data": ['.$gavisits['users'].']
                },
                {
                    "name":"Pageviews",
                    "data": ['.$gavisits['pageviews'].']
                }            
            ]
        }    
                ';           
        $graphs['google2']['json'] = '
                {
           "chart": {
                "type": "column"
            },
           "title": {
                "text": "Google Stats Duration Per Page"
            },
            "xAxis": {
                "categories": ["Stats"]
            },
            "yAxis": {

                "title": {
                    "text": "Time (Minutes)"
                }
            } ,   
            "credits": {
                "enabled": false
            },
            "series": 
            [
                {
                    "name":"Average Duration per Page",
                    "data": ['.$gavisits['sessionDurationPerPage'].']
                },
                {
                    "name":"Average Duration per Session",
                    "data": ['.$gavisits['sessionDuration'].']
                }
            
            ]
        }    
                ';            
        
        $this->generateGraphsObject($graphs);

        return $this->render('ManageManageBundle:Outreach:campaigns/stats.html.twig',array("emailStats" => $emailStats,"graphs" => $graphs,"landingStats" => $landingStats,"gavisits" => $gavisits));
    }
    public function getStatsPdfAction(Request $request)
    {
        $data = $request->query->get("emailStats").'<img src = "'.$request->query->get("emailGraph").'" /><div style="page-break-after: always;"></div>'.$request->query->get("uiUxStats")
        .'<img src = "'.$request->query->get("landingGraph").'" /><img src = "'.$request->query->get("landing2Graph").'" /><div style="page-break-after: always;"></div>'.$request->query->get("googleStats").'<img src = "'.$request->query->get("googleGraph").'" /><img src = "'.$request->query->get("google2Graph").'" />' ;      
        $dompdf = new \DOMPDF();
        $dompdf->load_html($data);
        $dompdf->render();
        $output = $dompdf->output(); 
        return new Response($dompdf->stream("campaignStats_".time().".pdf"));
    }
    function generateGraphsObject(&$graphs)
    {
        foreach ($graphs as &$graph)
        {
            $graph['json'] = str_replace("\n","", $graph['json'] );
            $graph['options'] = json_decode($graph['json'],true);
            $graph['stringify'] = json_encode($graph['options'],false);
        }
    }
    public function setSponsorConnectTemplatesAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $request->query->get("id") ));
        $templateids = explode(",",$plan->sponsorConnectTemplate);
        $em = $this->getDoctrine()->getManager();
        $outreach = new outreach($em);
        $templates = $outreach->sponsorConnectTemplates();
        foreach($templates as &$template)
        if (in_array($template->id,$templateids))
        $template->checked = "checked";
        else
        $template->checked = "";
        return $this->render('ManageManageBundle:Outreach:SponsorConnect/listtemplates.html.twig',array("templates" => $templates,"request" => $request->query->all(),"plan" => $plan));
    }
    public function setSponsorConnectTemplatesSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $connection = $this->get('doctrine.dbal.default_connection');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
        $plan = $repository->findOneBy(array('id' => $request->request->get("id") ));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPlan');
        $defaultPlan = $repository->findOneBy(array('userid' => $plan->userid ));
        
        if ($request->request->get("sponsorConnectEnabled") == "true")
        $plan->sponsorConnect = 1;
        else
        $plan->sponsorConnect = 0;    
        
        $plan->sponsorConnectTemplate = $request->request->get("templates");
        
        if ($request->request->get("sponsorConnectAllPlans") == "true")
        $connection->executeQuery("UPDATE plans SET sponsorConnectTemplate='".$plan->sponsorConnectTemplate."',sponsorConnect=".$plan->sponsorConnect."  WHERE userid ='".$plan->userid."'");
        
        if ($request->request->get("sponsorConnectFuturePlans") == "true") 
        {
            $defaultPlan->sponsorConnectTemplate = $plan->sponsorConnectTemplate;
            $defaultPlan->sponsorConnect = $plan->sponsorConnect;
        }
        $em->flush();
        return new Response("");
    }
    public function campaignStatusAction(Request $request)
    {
        $campaignid = $request->query->get("id");
        $campaign = $this->returnCampaign($campaignid);
        $mailEngine = new mailEngine($campaign->mailEngine,$this);
        $params = array("id" => $campaign->id);
        $campaignStatus = $mailEngine->campaignStatus($params);     
        return new Response($campaignStatus->sentStatus);
    }

    public function init()
    {
        $session = $this->get('adminsession');
        $session->set("section", "MailChimp");
        $session->set("currentpage", "MailChimp");
    }    
    public function signaturesAction()
    {
        $this->init();
       return $this->render('ManageManageBundle:Outreach:Messages/signatures.html.twig');
    }
    
    public function disclaimersAction()
    {
        $this->init();
        return $this->render('ManageManageBundle:Outreach:Messages/disclaimers.html.twig');
    }
    
    public function addMessageAction(Request $request)
    {
        $message = $request->query->all();
        return $this->render('ManageManageBundle:Outreach:Messages/messagesadd.html.twig',array("message" => $message));
    }
    public function editMessageAction(Request $request)
    {
        $message = $request->query->all();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:'.$message['table']);
        $message['data'] = $repository->findOneBy(array("id" => $message['id']));
        return $this->render('ManageManageBundle:Outreach:Messages/messagesedit.html.twig',array("message" => $message));       
    }
    public function deleteMessageAction(Request $request)
    {
        $message = $request->query->all();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:'.$message['table']);
        $message['data'] = $repository->findOneBy(array("id" => $message['id']));
        return $this->render('ManageManageBundle:Outreach:Messages/messagesdelete.html.twig',array("message" => $message));       
    }    
    
    public function addMessageSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->request->get("table") == "mailChimpSignatures")
        $message = new mailChimpSignatures();
        else if ($request->request->get("table") == "mailChimpDisclaimers")
        $message = new mailChimpDisclaimers();
        
        $arr =  $request->request->all();
        foreach ($arr as $key => $value)
        {
            $message->$key = $value;
        }
        $em->persist($message);
        $em->flush();
        return new Response("");
    }
    
    public function editMessageSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:'.$request->request->get("table"));
        $message = $repository->findOneBy(array("id" => $request->request->get("id")));
        $arr =  $request->request->all();
        foreach ($arr as $key => $value)
        {
            $message->$key = $value;
        }
        $em->flush();
        return new Response("");
    }
    public function deleteMessageSavedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('classesclassBundle:'.$request->request->get("table"));
        $message = $repository->findOneBy(array("id" => $request->request->get("id")));
        $em->remove($message);
        $em->flush();
        return new Response("");
    }  
}
