<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use classes\classBundle\Entity\plansUI;
use Shared\General\GeneralMethods;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Entity\plans;
use classes\classBundle\Entity\plansFunds;
use classes\classBundle\Entity\plansPortfolios;
use classes\classBundle\Entity\plansRiskBasedFunds;
use classes\classBundle\Classes\plansMethods;
set_time_limit(0);
class PlanUploadController extends Controller
{
    public $sections;
    public $filename;
    public $filename2;
    public function indexAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $session->set("section","Manage");  
        $this->removeFile();

        return $this->render('ManageManageBundle:PlanUpload:index.html.twig',array("sections" => $this->sections,"id" => $request->query->get("id","")));
    }

    public function removeFile()
    {
        if (file_exists($this->filename))
        unlink($this->filename);
        if (file_exists($this->filename2))
        unlink($this->filename2);
    }
    public function init($page = "plansUpload")
    {
        $this->sections = array();
        $methods = $this->get('GeneralMethods');
        if ($page == "plansUpload")
        {
            $methods->addSection($this->sections,"plans.name","Plan Name");
            $methods->addSection($this->sections,"plans.planid","Plan ID");
            $methods->addSection($this->sections,"roth","Roth");
            $methods->addSection($this->sections,"matching","Matching");
            $methods->addSection($this->sections,"catchup","Catchup");
            $methods->addSection($this->sections,"loans","Loans");
            $methods->addSection($this->sections,"autoenroll","Auto Enroll");
            $methods->addSection($this->sections,"vesting","Vesting");
            $methods->addSection($this->sections,"targetfunds","Target Funds");
            $methods->addSection($this->sections,"riskbasedportfolios","Risk Based Portfolios");
            $methods->addSection($this->sections,"customchoice","Custom Choice");
            $methods->addSection($this->sections,"defaultinvestmentselector","Default Investment Selector");
            $methods->addSection($this->sections,"401k","401k");
            $methods->addSection($this->sections,"matchingPercent","Matching %");
            $methods->addSection($this->sections,"matchingCap","Matching Cap");
            $methods->addSection($this->sections,"secondaryMatchingPercent","Secondary Matching %");
            $methods->addSection($this->sections,"secondaryMatchingCap","Secondary Matching Cap");
        }
        else if ($page == "fundsUpload")
        {
            $methods->addSection($this->sections,"planid","Planid");
            $methods->addSection($this->sections,"name","Name");
            $methods->addSection($this->sections,"ticker","Ticker");
        }
        else if ($page == "portfoliosUpload")
        {
            $methods->addSection($this->sections,"planid","Planid");
            $methods->addSection($this->sections,"name","Name");
            $methods->addSection($this->sections,"profile","Profile");
            $methods->addSection($this->sections,"minscore","Minscore");
            $methods->addSection($this->sections,"maxscore","Maxscore");
        }


        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $userid = "";
        if ($request->query->get("id","") != "")
        $userid = $request->query->get("id","");
        if ($request->request->get("id","") != "")
        $userid = $request->request->get("id","");       

        $this->filename = sys_get_temp_dir().'/planupload'.$userid.'.csv';
        $this->filename2 = sys_get_temp_dir().'/planuploaderrors'.$userid.'.csv';

    }

    public function writeTempFileAction()
    {
        $this->init();
        $this->removeFile();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $file = $request->files->get("upload-file");
        $tempfilecontents = file_get_contents($file);
        $fp = fopen($this->filename, 'w');
        fwrite($fp,$tempfilecontents);
        return new Response($this->displayTableBody());
    }

    public function displayTableBody($limit = null)
    {
        $counter = 0;
        $csv = array_map('str_getcsv', file($this->filename));
        $tbody = "";
        $sectionCount = count($this->sections);
        foreach ($csv as $row)
        {
            if ($limit != null && $counter > $limit)
            return $tbody;
            if ($counter != 0)
            {
                $tbody = $tbody."<tr>";
                $minicounter = 0;
                foreach ($row as $key => $value)
                {
                    if ($minicounter < $sectionCount)
                    $tbody = $tbody.'<td>"'.$value.'"</td>';
                    $minicounter++;
                }
                $tbody = $tbody."</tr>";
            }
            $counter++;
        }
        return $tbody;       
    }

    public function readTempFileAction()
    {
        $this->init();
        $counter = 0;
        $csv = array_map('str_getcsv', file($this->filename));
        foreach ($csv as $row)
        {
            foreach ($row as $key => $value)
            {
                echo $key.",".$value."<br/>";
            }
            echo "<hr/>";
            $counter++;
        }
        echo "total: ".$counter;
    }

    public function stringToBoolean($data)
    {
        $data = strtolower(trim($data));
        if ($data == "yes" || $data == "y")
        return true;
        return false;
    }
    public function setBooleanString($data)
    {
        if ($this->stringToBoolean($data))
        return "yes";
        else
        return "no";
    }
    public function processUploadAction()
    {
        $this->init();
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $header = explode(",",$request->request->get("header",""));
        $em = $this->getDoctrine()->getManager();
        $headerCount = count($header);

        if ($request->request->get("id","") != "")
        $userid = $request->request->get("id","");
        $csv = array_map('str_getcsv', file($this->filename));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:defaultPlan');
        $defaultPlan = $repository->findOneBy(array('userid' => $userid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:accounts');
        $account = $repository->findOneBy(array('id' => $userid));
        $plans = array();
        $counter = 0;
        $planMethods = new plansMethods($this);
        $valid = false;
        $duplicatePlans = array();
        $csvcounter = -1;



        for ($i = 0; $i < $headerCount; $i++)
        {
            if ($header[$i] == "plans.planid")
            {
                $valid = true;
                $planidIndex = $i;
            }
        }    

        if ($valid)    
        foreach ($csv as $row)
        {   
            $csvcounter++;
            if ($csvcounter > 0)
            {
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $searchedPlan = $repository->findOneBy(array('userid' => $userid,"planid" => trim($row[$planidIndex])));
                if ($searchedPlan == null)
                {
                    $plantobeadded = new plans();
                    $plantobeadded->partnerid = $account->partnerid;
                    $planMethods->addPlan($plantobeadded,$userid,$em,$defaultPlan);
                    $plans[$counter] = $plantobeadded;
                    $plantobeadded->csvdata = array();
                    for ($i = 0; $i < $headerCount; $i++)
                    {

                        if ($header[$i] != "")
                        {
                            if (substr_count($header[$i],"plans.") > 0)
                            {
                                $header2 = str_replace("plans.","",$header[$i]);
                                $plantobeadded->$header2 = trim($row[$i]);
                            }
                            if ($header[$i] == "401k")
                            {
                                if ($this->stringToBoolean($row[$i]))
                                $plantobeadded->IRSCode = "401k";
                            }

                            $plantobeadded->csvdata[$header[$i]] = $row[$i];
                        }
                    }
                    $em->persist($plantobeadded);
                    
                    if ($counter % 100 == 0)
                    $em->flush();
                    $counter++;
                }
                else
                $duplicatePlans[] = $row;
            }
        }
        $em->flush();
        $counter = 0;
        $Modules = "";
        $LibraryHotTopics = "";
        $Messaging = "";
        foreach ($plans as $plan)
        {
            $planid = $plan->id;
            $planMethods->copyTableData("plansModules","defaultModules","userid",$userid,$planid,$Modules);
            $planMethods->copyTableData("plansLibraryHotTopics","defaultLibraryHotTopics","userid",$userid,$planid,$LibraryHotTopics);
            $planMethods->copyTableData("PlansMessages", "DefaultMessages", "userid", $userid, $planid, $Messaging);
            if (isset($plan->csvdata['roth']))
            {
                if ($this->stringToBoolean($plan->csvdata['roth']))
                $Modules->deferralType = "traditional+roth";
                else
                $Modules->deferralType = "traditional";
            }
            if (isset($plan->csvdata['matching']))
            $Modules->matchingContributions = $this->setBooleanString($plan->csvdata['matching']);
            
            if (isset($plan->csvdata['catchup']))
            $Modules->catchup = $this->setBooleanString($plan->csvdata['catchup']);

            if (isset($plan->csvdata['loans']))
            $Modules->loans = $this->setBooleanString($plan->csvdata['loans']);

            if (isset($plan->csvdata['autoenroll']))
            {
                if ($this->stringToBoolean($plan->csvdata['autoenroll']))
                $Modules->enrollment = "auto_enroll";    
                else 
                $Modules->enrollment = "emp_enroll"; 
            }                       
            if (isset($plan->csvdata['vesting']))
            {
                $comparestring = strtolower(trim($plan->csvdata['vesting']));
                if ($comparestring == "3 year")
                $Modules->vesting = "vest_3yr";
                if ($comparestring == "5 year")
                $Modules->vesting = "vest_5yr"; 
                if ($comparestring == "6 year")
                $Modules->vesting = "vest_6yr"; 
                if ($comparestring == "generic")
                $Modules->vesting = "vest_gen"; 
                if ($comparestring == "none")
                $Modules->vesting = "none";                 
            }
            
            if (isset($plan->csvdata['targetfunds']))
            $Modules->investmentSelectorTargetMaturityFund = $this->stringToBoolean($plan->csvdata['targetfunds']);
            
            if (isset($plan->csvdata['riskbasedportfolios']))
            $Modules->investmentSelectorRiskBasedPortfolio = $this->stringToBoolean($plan->csvdata['riskbasedportfolios']);

            if (isset($plan->csvdata['customchoice']))
            $Modules->investmentSelectorCustomChoice = $this->stringToBoolean($plan->csvdata['customchoice']);

            if (isset($plan->csvdata['defaultinvestmentselector']))
            {
                $comparestring = strtolower(trim($plan->csvdata['defaultinvestmentselector']));
                if ($comparestring == "target")
                $Modules->investmentSelectorDefault =  "Target Maturity Fund";
                if ($comparestring == "risk")
                $Modules->investmentSelectorDefault =  "Risk Based Portfolio";
                if ($comparestring == "custom")
                $Modules->investmentSelectorDefault =  "Custom Choice";                
            }
            if (isset($plan->csvdata['matchingPercent']))
            $Modules->matchingPercent = $plan->csvdata['matchingPercent'];

            if (isset($plan->csvdata['matchingCap']))
            $Modules->matchingCap = $plan->csvdata['matchingCap'];

            if (isset($plan->csvdata['secondaryMatchingPercent']))
            $Modules->secondaryMatchingPercent = $plan->csvdata['secondaryMatchingPercent'];    

            if (isset($plan->csvdata['secondaryMatchingCap']))
            $Modules->secondaryMatchingCap = $plan->csvdata['secondaryMatchingCap'];


            $em->persist($Modules);
            $em->persist($LibraryHotTopics);
            $em->persist($Messaging);
            if ($counter % 100 == 0)
            $em->flush();
            $counter++;
        }
        $em->flush();
 
        $fp = fopen($this->filename2, 'w');
        fwrite($fp,$this->returnJson($duplicatePlans,$header));
        return new Response("");
    }



    public function errorXmlAction()
    {
        $this->init();
        return new Response(file_get_contents($this->filename2));
    }


    public function fundUploadAction()
    {
        $this->init("fundsUpload");
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $session->set("section","Manage");  
        $this->removeFile();
        return $this->render('ManageManageBundle:PlanUpload:fundupload.html.twig',array("sections" => $this->sections,"id" => $request->query->get("id","")));
    }
    public function portfoliosUploadAction()
    {
        $this->init("portfoliosUpload");
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $session->set("section","Manage");  
        $this->removeFile();
        return $this->render('ManageManageBundle:PlanUpload:portfolioupload.html.twig',array("sections" => $this->sections,"id" => $request->query->get("id","")));        
    }
    public function portfolioUploadProcessAction($riskbasedfunds = false)
    {

        $this->init("portfoliosUpload");
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $header = explode(",",$request->request->get("header",""));
        $headerCount = count($header);
        $em = $this->getDoctrine()->getManager();
        if ($request->request->get("id","") != "")
        $userid = $request->request->get("id","");
        $csv = array_map('str_getcsv', file($this->filename));
        $counter = 0;
        $csvcounter = 0;
        for ($i = 0; $i < $headerCount; $i++)
        {
            if ($header[$i] == "planid")
            $planidPosition = $i;    
        }
        foreach ($csv as $row)
        {
            if ($csvcounter > 0)
            {
                $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                $plan = $repository->findOneBy(array('planid' => $row[$planidPosition],'userid' => $userid,'deleted' => 0));
                if ($plan != null)
                {   
                    if (!$riskbasedfunds)
                    $plansPortfolios = new plansPortfolios();
                    else
                    $plansPortfolios = new  plansRiskBasedFunds();
                    for ($i = 0; $i < $headerCount; $i++)
                    {
                        $plansPortfolios->{$header[$i]} = $row[$i];
                    }
                    $plansPortfolios->planid = $plan->id;
                    $plansPortfolios->userid = $plan->userid;
                    $em->persist($plansPortfolios);
                    if ($counter % 100 == 0)
                    $em->flush();
                    $counter++;
                }
            }
            $csvcounter++;
        }
        $em->flush();
        return new Response("");

    }
    public function riskBasedFundsUploadAction()
    {
        $this->init("portfoliosUpload");
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $session = $this->get('adminsession');
        $session->set("section","Manage");  
        $this->removeFile();
        return $this->render('ManageManageBundle:PlanUpload:riskbasedfundsupload.html.twig',array("sections" => $this->sections,"id" => $request->query->get("id","")));
    }
    public function riskBasedFundsUploadProcessAction()
    {
        return $this->portfolioUploadProcessAction(true);
    }
    public function fundUploadProcessAction()
    {
        $this->init("fundsUpload");
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $header = explode(",",$request->request->get("header",""));
        $headerCount = count($header);
        $em = $this->getDoctrine()->getManager();
        if ($request->request->get("id","") != "")
        $userid = $request->request->get("id","");
        $valid = true;
        for ($i = 0; $i < $headerCount; $i++)
        {
            if ($header[$i] == "")
            $valid = false;
            if ($header[$i] == "ticker")
            $tickerPosition = $i;
            if ($header[$i] == "planid")
            $planidPosition = $i;           
        }

        $connection = $this->get('doctrine.dbal.default_connection');

        $csv = array_map('str_getcsv', file($this->filename));
        $counter = 0;
        $duplicateFunds = array();
        $csvcounter = 0;
        if ($valid)
        {
            foreach ($csv as $row)
            {   
                if ($csvcounter > 0)
                {
                    $repository = $this->getDoctrine()->getRepository('classesclassBundle:plans');
                    $plan = $repository->findOneBy(array('planid' => $row[$planidPosition],'userid' => $userid,'deleted' => 0));
                    if ($plan != null)
                    {
                        $tickerComparision = substr(trim($row[$tickerPosition]),0,3);

                        $fundComparsion = $connection->fetchAll('SELECT * FROM plansFunds WHERE planid = '.$plan->id.' AND TRIM(ticker) LIKE "'.$tickerComparision.'%" LIMIT 1');
                        
                        if (count($fundComparsion) == 0)
                        {
                            $fund = new plansFunds();
                            $fund->userid = $userid;
                            $fund->planid = $plan->id;
                            $fund->display = 1;
                            for ($i = 1; $i < $headerCount; $i++)
                            {
                                $fund->{$header[$i]} = $row[$i];
                            }
                            $em->persist($fund);
                            if ($counter % 100 == 0)
                            $em->flush();
                        }
                        else
                        {
                            $duplicateFunds[] = $row;
                        }
                    }
                }
                $csvcounter++;
            }
        }

        $fp = fopen($this->filename2, 'w');
        fwrite($fp,$this->returnJson($duplicateFunds,$header));
        return new Response("");
    }



    public function returnJson($duplicateData,$header)
    {
        $returnJson = "";
        $headerCount = count($header);
        $returnJsonHeader = '{"data":[';
        $returnJsonRowCounter = 0;
        foreach ($duplicateData as $data)
        {
            $returnJsonColumnCounter = 0;
            if ($returnJsonRowCounter)
            $returnJson = $returnJson.',';
            $returnJsonRowCounter++;
            $returnJson = $returnJson."[";
            for ($i = 0; $i < $headerCount; $i++)
            {
                if ($header[$i] != "")
                {
                    if ($returnJsonColumnCounter > 0)
                    $returnJson = $returnJson.',';
                    $returnJson = $returnJson.'"'.$data[$i].'"';
                    $returnJsonColumnCounter++;
                }
            }
            $returnJson = $returnJson."]";
        }
        $returnJsonFooter = ']}';

        return $returnJsonHeader.$returnJson.$returnJsonFooter;
    }
}
?>