<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use classes\classBundle\Entity\members;
use Sessions\AdminBundle\Classes\adminsession;
use classes\classBundle\Entity\portalnews;
use Permissions\RolesBundle\Classes\rolesPermissions;
class PortalController extends Controller
{
	public $manageid;
	public $permissionlevel;
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");
        $newsArray = $this->news();
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("ManagePortal");
    	return $this->render('ManageManageBundle:Portal:index.html.twig',array("newsArray" => $newsArray,"writeable" => $writeable));
    }
    public function loadnewsAction()
    {
    	$newsArray = $this->news();
		return $this->render('ManageManageBundle:Portal:loadnews.html.twig',array("newsArray" => $newsArray));
    }
    public function news()
    {
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:portalnews');
		$newsArray = $repository->findBy(array(),array('orderid' => 'DESC'));
		return $newsArray;
    }
    public function editPortalNewsAction()
    {
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$id = $request->query->get("id","");
		$repository = $this->getDoctrine()->getRepository('classesclassBundle:portalnews');
		$newsarticle = $repository->findOneBy(array('id' => $id));
    	return $this->render('ManageManageBundle:Portal:editportalnews.html.twig',array('newsarticle' => $newsarticle));
    }
    public function addPortalNewsAction()
    {


    	return $this->render('ManageManageBundle:Portal:addportalnews.html.twig');
    }
    public function addPortalNewsSavedAction()
    {
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$newNews = new portalnews();
		$em = $this->getDoctrine()->getManager();

		$repository = $this->getDoctrine()->getRepository('classesclassBundle:portalnews');
		$newsArray = $repository->findBy(array(),array('orderid' => 'ASC'));

		$newNews->title = $request->request->get("title");
		$newNews->description = $request->request->get("description");
		$newNews->orderid = 0;
		$i = 1;
		foreach ($newsArray as &$news)
		{
			$news->orderid = $i * -1;
			$i++;
		}
	    $em->persist($newNews);
	    $em->flush();
		return new Response("saved1");
    }
    public function savePortalOrderAction()
    {
		$request = Request::createFromGlobals();
		$request->getPathInfo();
		$idstring = $request->request->get("ids","");
		$ids = explode(",",$idstring);


		for ($i = 0; $i < count($ids); $i++)
		{
			$repository = $this->getDoctrine()->getRepository('classesclassBundle:portalnews');
			$newsarticle = $repository->findOneBy(array('id' => $ids[$i]));
			$newsarticle->orderid = $i * -1 ;
			$this->getDoctrine()->getManager()->flush();
		}

    	return new Response("");
    }
}
?>
