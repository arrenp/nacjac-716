<?php

namespace Manage\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Sessions\AdminBundle\Classes\adminsession;
set_time_limit(0);
class ExportDatabaseController extends Controller
{

    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section","Manage");

    	return $this->render('ManageManageBundle:ExportDatabase:index.html.twig');
    }

    public function checkKeyAction()
    {
        return new Response($this->checkKey());
    }

    public function checkKey()
    {
        $currentTime = (int)(date("Y").date("m").date("d"));
        $birthday = 19860403;
        $computed = ceil(($currentTime-$birthday) * 3.14);
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        if ($request->request->get("key","")  == $computed)
        return 1;
        return 0;
    }

    public function exportDatabaseAction()
    {
        if ($this->checkKey())
        {
            $mysqlHostName = $this->container->getParameter('database_host');
            $mysqlUserName = $this->container->getParameter('database_user');
            $mysqlPassword = $this->container->getParameter('database_password');
            $mysqlDatabaseNameFrom = $this->container->getParameter('database_name');
            $mysqlDatabaseNameTo = "smartplan_qa";
            $file = "toqa.sql";
            if (file_exists($file))
            unlink($file);   

            $con = mysqli_connect($mysqlHostName,$mysqlUserName,$mysqlPassword ,$mysqlDatabaseNameFrom );

            if (mysqli_connect_errno())
            {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }

            mysqli_query($con, "DROP schema '".$mysqlDatabaseNameTo."' IF EXISTS");
            mysqli_query($con, "CREATE schema ".$mysqlDatabaseNameTo);

            $command='mysqldump --opt --lock-tables=false -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseNameFrom .' > ' .$file; 
            
            
            exec($command,$output=array(),$worked);        
            switch($worked)
            {
                case 1:
                return new Response('There was a warning during the export of <b>' .$mysqlDatabaseNameFrom .'</b> to <b>~/' .$file .'</b>');
                break;
                case 2:
                return new Response('There was an error during export. Please check your values:<br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' .$mysqlDatabaseNameFrom .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$mysqlUserName .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$mysqlHostName .'</b></td></tr></table>');
                break;
            }
            
            $command='mysql  -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseNameTo .' < ' .$file;

            exec($command,$output=array(),$worked);
            
            switch($worked)
            {
                case 1:
                return new Response('There was an error during import. <br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' .$mysqlDatabaseNameTo .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$mysqlUserName .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$mysqlHostName .'</b></td></tr><tr><td>MySQL Import Filename:</td><td><b>' .$file .'</b></td></tr></table>');
                break;
            }

            if (file_exists($file))
            unlink($file); 
            return new Response(1);
        }
        return new Response("");
        
    }


}
?>