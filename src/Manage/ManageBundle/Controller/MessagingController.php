<?php

namespace Manage\ManageBundle\Controller;

use classes\classBundle\Entity\messagesPanes;
use classes\classBundle\Entity\messagesPanesGroups;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Sessions\AdminBundle\Classes\adminsession;
use Permissions\RolesBundle\Classes\rolesPermissions;
use classes\classBundle\Entity\plansLibraryVideos;
use classes\classBundle\Entity\plansLibraryType;

class MessagingController extends Controller
{
    public function indexAction()
    {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");
        $permissions = $this->get('rolesPermissions');
        $writeable = $permissions->writeable("ManageMessagingPanes");

        return $this->render('ManageManageBundle:Messaging:index.html.twig', array("session" => $session, "writeable" => $writeable, "roleType" => $session->roleType));
    }

    public function addMessageAction()
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $groups = $repository->findAll();
        return $this->render('ManageManageBundle:Messaging:addmessage.html.twig', array("groups" => $groups));
    }

    public function addMessageSavedAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $internalName = $request->request->get("internalName");
        $displayName = $request->request->get("displayName");
        $group = $request->request->get("group");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $groupid = $repository->findOneBy(array("name" => $group))->id;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanes');
        $displayOrder = $repository->findBy(array(), array('displayOrder' => 'desc'))[0]->displayOrder + 1;

        $message = new messagesPanes();
        $message->name = $internalName;
        $message->description = $displayName;
        $message->messagesPanesGroupsId = $groupid;
        $message->displayOrder = $displayOrder;


        $em->persist($message);
        $em->flush();

        return new Response("Saved");
    }

    public function editMessageAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanes');
        $id = $request->query->get("id");
        $message = $repository->findOneBy(array('id' => $id));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $group = $repository->findOneBy(array("id" => $message->messagesPanesGroupsId))->name;
        $groups = $repository->findAll();

        return $this->render('ManageManageBundle:Messaging:editmessage.html.twig', array("currentmessage" => $message, "groupname" => $group, "groups" => $groups));
    }

    public function editMessageSavedAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $internalName = $request->request->get("internalName");
        $displayName = $request->request->get("displayName");
        $group = $request->request->get("group");
        $id = $request->request->get("id");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $groupid = $repository->findOneBy(array("name" => $group))->id;

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanes');
        $message = $repository->findOneBy(array("id" => $id));
        $message->name = $internalName;
        $message->description = $displayName;
        $message->messagesPanesGroupsId = $groupid;

        $em->persist($message);
        $em->flush();

        return new Response("Saved");
    }

    function deleteMessageAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanes');
        $message = $repository->findOneBy(array('id' => $request->query->get("id")));

        return $this->render('ManageManageBundle:Messaging:deletemessage.html.twig', array("message" => $message));
    }

    public function deleteMessageSavedAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanes');
        $message = $repository->findOneBy(array('id' => $id));

        $em->remove($message);
        $em->flush();
        return new Response("Deleted");

    }

    public function groupsIndexAction() {
        $session = $this->get('adminsession');
        $session->set("section", "Manage");

        return $this->render('ManageManageBundle:Messaging:groups/index.html.twig');

    }

    public function addGroupAction() {
        return $this->render('ManageManageBundle:Messaging:groups/addgroup.html.twig');

    }

    public function addGroupSavedAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $groupName = $request->request->get("groupname");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $displayOrder = $repository->findBy(array(), array('displayOrder' => 'desc'))[0]->displayOrder + 1;

        $toadd = new messagesPanesGroups();
        $toadd->name = $groupName;
        $toadd->displayOrder = $displayOrder;

        $em->persist($toadd);
        $em->flush();

        return new Response("Saved");
    }

    public function editGroupAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $id = $request->query->get("id");
        $group = $repository->findOneBy(array('id' => $id));

        return $this->render('ManageManageBundle:Messaging:groups/editgroup.html.twig', array("currentgroup" => $group));
    }

    public function editGroupSavedAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $name = $request->request->get("groupname");
        $id = $request->request->get("id");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $group = $repository->findOneBy(array("id" => $id));
        $group->name = $name;

        $em->persist($group);
        $em->flush();

        return new Response("Saved");
    }

    function deleteGroupAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $group = $repository->findOneBy(array('id' => $request->query->get("id")));

        return $this->render('ManageManageBundle:Messaging:groups/deletegroup.html.twig', array("group" => $group));
    }

    public function deleteGroupSavedAction() {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get("id");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $group = $repository->findOneBy(array('id' => $id));

        $em->remove($group);
        $em->flush();
        return new Response("Deleted");

    }

    public function reorderMessagesAction()
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanes');
        $messages = $repository->findBy(array(), array('messagesPanesGroupsId' => 'asc', 'displayOrder' => 'asc'));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $groups = $repository->findAll();
        $groupMap = array();
        $groupIdToOrderMap = array();
        foreach ($groups as $group) {
            $groupMap[$group->id] = $group->name;
            $groupIdToOrderMap[$group->id] = $group->displayOrder;
            $groupOrderToId[$group->displayOrder] = $group->id;
        }

        $messageMap = array();
        foreach ($messages as $message) {
            $messageMap[$groupIdToOrderMap[$message->messagesPanesGroupsId]][] = $message;
        }

        ksort($messageMap); // to sort by group displayorder
        $newMessageMap = array();
        foreach ($messageMap as $key => $value) {
            $newMessageMap[$groupMap[$groupOrderToId[$key]]] = $value;
        }

        return $this->render('ManageManageBundle:Messaging:reordermessages.html.twig',array("messageMap" => $newMessageMap));
    }

    public function reorderMessagesSavedAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $ids = $request->request->get("ids");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanes');

        foreach ($ids as $key => $value) {
            $messageObject = $repository->findOneBy(array('id' => $value));
            $messageObject->displayOrder = $key+1; // +1 keeps 1 indexing
            $em->persist($messageObject);
        }
        $em->flush();

        return new Response('success');
    }

    public function reorderGroupsAction()
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');
        $groups = $repository->findBy(array(), array('displayOrder' => 'asc'));

        return $this->render('ManageManageBundle:Messaging:groups/reordergroups.html.twig',array("groups" => $groups));
    }

    public function reorderGroupsSavedAction()
    {
        $request = Request::createFromGlobals();
        $request->getPathInfo();
        $em = $this->getDoctrine()->getManager();
        $ids = $request->request->get("ids");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:messagesPanesGroups');

        foreach ($ids as $key => $value) {
            $groupObject = $repository->findOneBy(array('id' => $value));
            $groupObject->displayOrder = $key+1; // +1 keeps 1 indexing
            $em->persist($groupObject);
        }
        $em->flush();

        return new Response('success');
    }

}
?>