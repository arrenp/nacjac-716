<?php

namespace Permissions\RolesBundle\Classes;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Shared\General\GeneralMethods;

class rolesPermissions
{

    private $currentuser;
    private $currentaccount;
    private $currentRole;
    private $session;
    private $currentRoles;
    private $isIntegrated;
    private $container;

    private $masteruser;
    private $masterRole;
    private $masterRoles;

    public function __construct($container)
    {
        $this->container = $container;
        $this->methods = $container->get("GeneralMethods");
        $this->session = $container->get("session");
        $repository = $container->get("doctrine")->getRepository('classesclassBundle:accounts');
        $this->currentaccount = $repository->findOneBy(array('id' => $this->session->get("userid")));
        $repository = $container->get("doctrine")->getRepository('classesclassBundle:accountsUsers');
        $this->currentuser = $repository->findOneBy(array('id' => $this->session->get("clientid")));
        $this->masteruser = $repository->findOneBy(array('id' => $this->session->get("masterid")));
        $repository = $container->get("doctrine")->getRepository('classesclassBundle:accountsRoles');
        $this->currentRole = $repository->findOneBy(array('id' => $this->currentuser->roleid));
        $this->masterRole = $repository->findOneBy(array('id' => $this->masteruser->roleid));

        $repository = $container->get("doctrine")->getRepository('classesclassBundle:plans');
        $this->currentplan = $repository->findOneBy(array('id' => $this->session->get("planid")));
        $repository = $container->get("doctrine")->getRepository('classesclassBundle:accountsUsersAdditionalRoles');
        $this->additionalRoles = $repository->findBy(array('accountsUsersId' => $this->session->get("clientid")));
        $this->currentRoles = array();
        $this->currentRoles[] = $this->currentRole;
        $this->masterRoles = array();
        $this->masterRoles[] = $this->masterRole;
        $counter = 0;
        $this->isIntegrated = $this->methods->isIntegrated($this->currentaccount);
        foreach ($this->additionalRoles as $role)
        {

            $repository = $container->get("doctrine")->getRepository('classesclassBundle:accountsRoles');
            $newrole = $repository->findOneBy(array('id' => $role->roleid));
            if ($newrole != null) $this->currentRoles[] = $newrole;
        }
    }

    public function readable($field, $roleType = "currentRoles")
    {
        $readable = 0;
        foreach ($this->$roleType as $role)
        {

            $comparefield = "permissions" . $field;

            if ($role->$comparefield == "read" || $role->$comparefield == "full") $readable = 1;
        }
        $this->exceptions($readable, $field);
        return $readable;
    }

    public function writeable($field, $roleType = "currentRoles")
    {
        $writeable = 0;
        foreach ($this->$roleType as $role)
        {
            $comparefield = "permissions" . $field;

            if ($role->$comparefield == "full") $writeable = 1;
        }
        //Exceptions
        $this->exceptions($writeable, $field);
        return $writeable;
    }

    public function exceptions(&$type, $field)
    {
        if ($type)
        {
            if ($field == "PlansAdvice") $type = $this->adviceException();
            if (($field == "PlansInvestmentsFunds" || $field == "PlansInvestmentsPortfolios" || $field == "SettingsInvestmentsFunds" || $field == "SettingsInvestmentsPortfolios")) $type = $this->investmentsAvailable();
            if ($field == "PlansQdia") $type = $this->qdiaException();

            if (($field == "PlansInvestmentsFunds" || $field == "PlansInvestmentsPortfolios" || $field == "SettingsDeploymentUrls" ) && $this->isIntegrated) $type = 0;

            if ($field == "PlansAssociate" && ($this->currentaccount->connectionType == "DST" || $this->currentaccount->connectionType == "RetRev")) $type = 0;
            if ($field == "PlansAssociate") $type = $this->riskProfileConfigurationException();
            if ($field == "plansNavSponsorConnect" || $field == "plansNavSponsorConnectList")
            $type = $this->sponsorConnectException();
            if ($field == "PlansMedia")
            $type = $this->vwiseRoleException();
            if (in_array($field,array("PlansPowerView","PlansPowerViewEmailList","PlansIrio")) )
            {
                $type = $this->widgetException();
            }
            if ($field == "PlansRiskBasedFunds")
            {
                $type = $this->riskBasedFundsException();
            }
            if ($field == "PlansInvestorProfile" || $field == "SettingsInvestorProfile")
            {
                $type = $this->personalInvestorProfileException();
            }
        }
        if ($field =="PIPWidget")
        {
            $type = $this->PIPWidgetException();
        }
    }
    public function PIPWidgetException()
    {
        return $this->currentaccount->PIPWidget;
    }
    public function riskBasedFundsException()
    {
        if ($this->currentaccount->connectionType == "Alerus")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function widgetException()
    {
        $repository = $this->container->get("doctrine")->getRepository('classesclassBundle:CTAAccountList');       
        $ctas= $repository->findOneBy(array("userid" => $this->currentaccount->id)); 
        if ($ctas != null)
        return true;
        return false;   
    }
    public function vwiseRoleException()
    {
        if ($this->session->get("masterRoleType") == "vwise_admin" || $this->session->get("masterRoleType") == "vwise") 
        return true;
        return false;
    }
    
    public function sponsorConnectException()
    {
        if ($this->currentplan != null && $this->currentaccount->sponsorConnect && $this->currentplan->sponsorConnect)
        return true;
        return false;    
    }
    


    public function adviceException()
    {
        if ($this->currentaccount->adviceAvailable) return true;
        return false;
    }

    public function investmentsAvailable()
    {
        if ($this->currentaccount->connectionType == "Relius" || $this->methods->isSrt($this->currentaccount) || $this->currentaccount->connectionType == "ExpertPlan") return false;
        return true;
    }

    public function qdiaException()
    {
        if ($this->currentaccount->qdiaAvailable) return true;
        return false;
    }

    public function riskProfileConfigurationException()
    {
        if ($this->currentaccount->connectionType == strpos($this->currentaccount->connectionType, 'omni')) return false;
        return true;
    }

    public function permissions()
    {
        $permissions = new \stdClass();
        foreach ($this->currentRoles as $role)
        {
            foreach ($role as $key => $value)
            {
                if (substr_count($key, "permissions") > 0)
                {
                    $comparekey = str_replace("permissions", "", $key);
                    $readablekey = $comparekey . "Readable";
                    $writeablekey = $comparekey . "Writeable";
                    if (!isset($permissions->$readablekey) || $permissions->$readablekey == 0) $permissions->$readablekey = $this->readable($comparekey);
                    if (!isset($permissions->$writeablekey) || $permissions->$writeablekey == 0) $permissions->$writeablekey = $this->writeable($comparekey);
                }
            }
        }
        return $permissions;
    }
    public function personalInvestorProfileException()
    {
        return $this->currentaccount->personalInvestorProfile && $this->vwiseRoleException();
    }
}

?>
