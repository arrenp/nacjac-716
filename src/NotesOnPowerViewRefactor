==================================================
Notes on PowerView Refactor Organization & Layouts
==================================================

###### PowerViewBase ######

  * base
  	- The base layout for all PowerView files, including the header and body content.

  * desktop
  	- Extends the base layout, provides the overall content layout for desktop versions.

  * mobile
  	- Extends the base layout, provides the overall content layout for mobile versions.

  * macros
  	- Creates the individual section/article layouts to be used in the content for desktop versions.



###### PowerViewSlides ######

  ## Common screens used for all versions ##
    * intro
    * balance
    * contribution
    * emp_match
    * est_payout
    * est_rtmt_balance

  ## Common groups of slides used for SmartPlan & Redirect ##
    * slideGroup_Intro
      - Includes the following slides with timing:
        - intro
        - balance
    * slideGroup_A
      - Includes the following slides with timing:
        - contribution
        - emp_match
        - est_rtmt_balance
        - est_payout
    * slideGroup_B
      - Includes the following slides with timing:
        - contribution
        - est_rtmt_balance
        - est_payout

  ## Redirect Version CTA Screens ##
    * RedirectCTA_1
    * RedirectCTA_2
    * RedirectCTA_3
    * redirectForm2

  ## SmartPlan Version CTA Screen ##
    * SmartPlanCTA

  ## Specific to Version 1132 ##
    * contribution_bar
    * max_payout
    * max_rtmt_balance
    * matchForm
      - CTA prompt (would you like to maximize...?)
    * confirmation[...]
      - Confirmation screens based on CTA prompt answer.



###### PowerViewMobileSlides ######

  ## Base layouts for content ##
    * base
      - The general layout for all slide content (except confirmation screens in 1132).

    * confirmationBase
      - The general layout for confirmation screens (in version 1132).

  ## Common Slides (used for all versions) ##
    * intro
    * balance
    * contribution
    * emp_match

  ## Slide Groups ##
    * slideGroup_Intro
      - Includes the following slides (used for all versions):
        - intro
        - balance
        - contribution

    * slideGroup_Confirmation
      - Includes the following slides (used for 1132 only):
        - confirmationConnected
        - confirmationCTA
        - confirmationManual
        - confirmationError
        - confirmationThankYou
