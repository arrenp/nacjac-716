<?php
$plan = $app->getSession()->get('plan');
$interface = array(
    'blue' => array('#003377','#0055AA','#37AAED','#0055AA','#5B798E'),
    'burgundy' => array('#85002c','#990033','#CC5566','#D87C89','#EAB8BF'),
    'charcoal' => array('#333333','#666666','#999999','#BBBBBB','#DDDDDD'),
    'gold' => array('#776644','#998866','#CCBB99','#CCBB99','#CCCCBB'),
    'green' => array('#005555','#007777','#339999','#99CCCC','#CCCCCC'),
    'red' => array('#660000','#990000','#CC0000','#DF0000','#FFCBCB'),
    'abgil' => array('#005555','#007777','#005555','#9acccd','#CCCCCC'),
    'LNCLN' => array('#666666','#4F88A4','#4F88A4','#6FAAC7','#B7D4E3'),
    'ascensus' => array('#72A637','#31617B','#FFFFFF','#31617B','#72A637'),
    'ATcustom' => array('#445b6d', '#445b6d','#ffffff','#445b6d','#213342'),
    'moa' => array('#000000','#0055AA','#37AAED','#0055AA','#5B798E')
);
$color = array_key_exists(trim($plan['interfaceColor']),$interface) ? $interface[trim($plan['interfaceColor'])] : $interface['blue'];
$layout = trim($plan['interfaceLayout']) ? $plan['interfaceLayout'] : 'side';
?>

#container {background-color:<?php echo $color[1] ?>;}
#container-main {background-color:<?php echo $color[3] ?>;}
#container-main .navigation-block ul {margin:0;padding:0;overflow:hidden;}
#container-main .navigation-block #mainNav {padding:15px 0;}
#container-main .navigation-block ul li {list-style:none;line-height:5rem;}
#container-main .navigation-block ul li a {display:block;color:#FFF;font-size:18px; font-weight:normal;text-decoration:none;padding-right:20px;text-align:right;}
#container-main .navigation-block ul li.highlight:hover, #container-main .navigation-block ul .highlight.active {background:<?php echo $color[2] ?>; }
#container-main .navigation-block ul li a:hover, #container-main .navigation-block ul li.active a {color: #000;}
h1.title {color:#FFF;}

#container-main .content-block .content-area .head-title{color:<?php echo $color[0] ?>}
#container-main .content-block .content-area .title {color:<?php echo $color[0] ?>;}
#container-main .content-block .content-area .details {color:<?php echo $color[1] ?>;}
#container-main .content-block .content-area .details .slider_box{ border-color: <?php echo $color[0] ?>;}
#container-main .content-block .content-area .details .arrow .upArrow{ background: url(../images/upArrow.png) no-repeat; height: 12px;background-color:<?php echo $color[0] ?>;}
#container-main .content-block .content-area .details .arrow .downArrow {background:url(../images/downArrow.png) no-repeat; height:12px;background-color:<?php echo $color[0] ?>;background-position:center bottom;}

#container-main .actions .actionBtn .action-button {
background: <?php echo $color[1] ?>;
color: #ffffff;
}

#container-main .actions .actionBtn .action-button:hover {
background: <?php echo $color[3] ?>;
background-image: -webkit-linear-gradient(top, <?php echo $color[3] ?>, <?php echo $color[2] ?>);
background-image: -moz-linear-gradient(top, <?php echo $color[3] ?>, <?php echo $color[2] ?>);
background-image: -ms-linear-gradient(top, <?php echo $color[3] ?>, <?php echo $color[2] ?>);
background-image: -o-linear-gradient(top, <?php echo $color[3] ?>, <?php echo $color[2] ?>);
background-image: linear-gradient(to bottom,<?php echo $color[3] ?>, <?php echo $color[2] ?>);
text-decoration: none;
}
<?php if ($color[1] == $interface['ATcustom'][1]) {?>
#container-main .actions .actionBtn .action-button:hover {
    background: #213342;
}

.btn-primary {
    background-color: <?php echo $color[1] ?> !important;
    border-color: <?php echo $color[1] ?> !important;
}

.btn-primary:hover {
    background: <?php echo $color[4] ?> !important;
}
<?php } ?>

#total_bar {background: <?php echo $color[0] ?>;}
#target_bar {background: <?php echo $color[1] ?>;}

.jquery-msgbox {color: <?php echo $color[1] ?>}
.jquery-msgbox .title {color: <?php echo $color[0] ?>;}

.theme-bgcolor{background:<?php echo $color[0] ?>;}

#footer {background: <?php echo $color[0] ?>;}
#footer .item, #footer .item a {color:<?php echo $color[2] ?>;}
#footer select, #footer select option {background: <?php echo $color[0] ?>;}

.audioPanel {background: <?php echo $color[0] ?>;}
.audioPanel2 {background: <?php echo $color[0] ?>;}

.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {background: <?php echo $color[0] ?> !important; border: 1px solid <?php echo $color[2] ?> !important;color: #FFF !important;}
.ui-state-default.ui-state-highlight {background: <?php echo $color[4] ?> !important;}
.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus, .ui-state-default.ui-state-active {background: <?php echo $color[2] ?> !important;border: 1px solid <?php echo $color[0] ?> !important;}
.box {overflow:hidden;}
<?php if(in_array($layout , array('top','bottom'))): ?>
@media (min-width:768px) {
    #container-main .navigation-block ul li {
        float:left;
    }
}
<?php endif; ?>


