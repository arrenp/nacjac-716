<!DOCTYPE html>
<html lang="{{ request.lang|default('en') }}">
    <head>
        <title><?php echo $view['translator']->trans('smartplan_enterprise', array(), 'my_profile') ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl('favicon.ico') ?>" />
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link type="text/css" rel="stylesheet" href="<?php echo $view['assets']->getUrl('spe/css/bootstrap.min.css') ?>" media="all" >
        <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" media="all" >
        <link type="text/css" rel="stylesheet" href="<?php echo $view['assets']->getUrl('spe/css/main.css?v='.rand(0,1000)) ?>" media="all" >
        <script src="<?php echo $view['assets']->getUrl('spe/js/vendor/jquery/jquery.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo $view['assets']->getUrl('spe/js/vendor/utils/bootstrap.min.js') ?>" type="text/javascript"></script>
        <?php echo $view->render('SpeAppBundle:Template:highcharts.js.twig') ?>
    </head>
    <body class="report">
        <?php echo $view->render('SpeAppBundle:Template:report-inner.html.php', array('profile' => $profile, 'plan' => $plan)) ?>
    </body>

</html>
