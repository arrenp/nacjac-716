<?php
namespace Spe\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * RetirementNeeds controller.
 * @Route("/", name="retirementneeds", condition="request.headers.get('X-Requested-With') == 'XMLHttpRequest'")
 */
class RetirementNeedsController extends Controller
{
    const LAST_PAGE = 7;

    /**
     * @Route("/retirementneeds", name="retirementneeds")
     */
    public function retirementneedsAction(Request $request)
    {
        $this->retirementneeds = $this->get("spe.app.retirementneeds");
        return $this->render("index.html.twig", $this->retirementneeds->getData());
    }

    /**
     * @Route("/retirementneeds/nextpage", name="nextpage")
     */
    public function nextPageAction(Request $request)
    {
        $currentPage = (int)($request->request->get("page"));
        $this->retirementneeds = $this->get("spe.app.retirementneeds");
        $this->retirementneeds->saveData($request->request->all());
        if (self::LAST_PAGE == $currentPage) {
            $this->get("spe.app.retirementneeds")->finishRN();
            return new Response('done');
        }
        $currentPage++;
        return $this->render("page".$currentPage.".html.twig", $this->retirementneeds->getData());
    }

    /**
     * @Route("/retirementneeds/prevpage", name="prevpage")
     */
    public function prevPageAction(Request $request)
    {
        $currentPage = (int)($request->request->get("page"));
        $currentPage--;
        $this->retirementneeds = $this->get("spe.app.retirementneeds");
        return $this->render("page".$currentPage.".html.twig", $this->retirementneeds->getData());
    }


    public function render($file,$params = [])
    {
        return parent::render("SpeAppBundle:Template:retirementneeds/".$file,$params);
    }


}