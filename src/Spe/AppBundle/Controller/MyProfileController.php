<?php
namespace Spe\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Ajax controller.
 * @Route("/", name="investments", condition="request.headers.get('X-Requested-With') == 'XMLHttpRequest'")
 */
class MyProfileController extends Controller
{
     /**
     * @Route("/myProfile", name="myProfile")
     */
    public function myProfileAction()
    {   
        return (!$this->get("spe.app.profile")->uiData()['beneficiary']['on']) ? $this->myProfileReportAction(): $this->myProfileBeneficiaryAction();
    }  
     /**
     * @Route("/myProfileReport", name="myProfileReport")
     */
    public function myProfileReportAction()
    {   
        return $this->render('SpeAppBundle:Template:myprofile/ui/index.html.twig',$this->get("spe.app.profile")->uiData());      
    }  
     /**
     * @Route("/myProfileBeneficiary", name="myProfileBeneficiary")
     */
    public function myProfileBeneficiaryAction()
    {   
        return $this->render('SpeAppBundle:Template:myprofile/ui/beneficiary.html.twig',$this->get("spe.app.profile")->uiData());      
    } 
     /**
     * @Route("/myProfileBeneficiaryAdd", name="myProfileBeneficiaryAdd")
     */
    public function myProfileBeneficiaryAddAction()
    {   
        $data = $this->get("spe.app.profile")->uiData();
        $beneficiaryData = $this->get("spe.app.profile")->beneficiaryDataByType($data['beneficiary']['file']);
        return $this->render('SpeAppBundle:Template:myprofile/ui/beneficiary/'.$data['beneficiary']['file'].'.html.twig',["data" => $data,"beneficiaryData" => $beneficiaryData,"mode" => "add"] );      
    }
     /**
     * @Route("/myProfileBeneficiaryAddSaved", name="/myProfileBeneficiaryAddSaved")
     */
    public function myProfileBeneficiaryAddSavedAction(Request $request)
    {
        $beneficiaryList = $this->get("session")->get("beneficiaryList",[]);
        $beneficiary = !empty($beneficiaryList[$request->request->get("id")]) ? $beneficiaryList[$request->request->get("id")]: [];
        $allowedKeys = ["firstName","lastName","ssn","relationship","gender","type","maritalStatus","percent","address1","address2","city","country","state","trustEstateCharityOther","foreignaddress","middleInitial",'dob','phone','zip','isSpouse','name','level'];
        foreach ($allowedKeys as $key)
        {
            $beneficiary[$key] = $request->request->get($key,null);
        }        
        if (!empty($beneficiary['name']))
        {
            $nameExplode = explode(" ",trim($beneficiary['name']));
            $beneficiary['firstName'] = trim($nameExplode[0]);
            $beneficiary['lastName'] = trim($nameExplode[1]);
        }
        if (!empty($beneficiary['isSpouse']))
            $beneficiary['relationship'] = "spouse";
        $beneficiary['id'] = uniqid("bene");
        if (!empty($request->request->get("id")))
            $beneficiary['id'] = $request->request->get("id");
        $beneficiaryList[$beneficiary['id']] = $beneficiary;
        $this->get("session")->set("beneficiaryList",$beneficiaryList);
        return new Response("success");
    }
     /**
     * @Route("/myProfileBeneficiaryDelete", name="/myProfileBeneficiaryDelete")
     */
    public function myProfileBeneficiaryDeleteAction(Request $request)
    {   
        $beneficiaryListDeleted = $this->get("session")->get("beneficiaryListDeleted",[]);
        $beneficiaryList = $this->get("session")->get("beneficiaryList",[]);
        $beneficiaryListCopy =  $this->get("session")->get("beneficiaryListCopy",[]);
        if (!empty($beneficiaryListCopy[$request->request->get("id")]['number']))
            $beneficiaryListDeleted[] = $beneficiaryListCopy[$request->request->get("id")];
        unset($beneficiaryList[$request->request->get("id")]);
        $this->get("session")->set("beneficiaryList",$beneficiaryList);
        $this->get("session")->set("beneficiaryListDeleted",$beneficiaryListDeleted);
        return new Response("");
    }   
     /**
     * @Route("/myProfileBeneficiaryEdit", name="/myProfileBeneficiaryEdit")
     */
    public function myProfileBeneficiaryEditAction(Request $request)
    {   
        $data = $this->get("spe.app.profile")->uiData();
        $beneficiaryList = $this->get("session")->get("beneficiaryList",[]);
        $beneficiary = $beneficiaryList[$request->request->get("id")];
        $beneficiaryData = $this->get("spe.app.profile")->beneficiaryDataByType($data['beneficiary']['file'],$beneficiary);
        return $this->render('SpeAppBundle:Template:myprofile/ui/beneficiary/'.$data['beneficiary']['file'].'.html.twig',["data" => $data,"beneficiaryData" => $beneficiaryData,"mode" => "edit","id" => $request->request->get("id")] );              
    } 
     /**
     * @Route("/myProfile/data", name="myProfileData")
     */
    public function getProfileDataAction()
    {
        return new JsonResponse($this->get("spe.app.profile")->uiData());
    }   
     /**
     * @Route("/myProfile/transact", name="myProfileTransact")
     */
    public function myProfileTransactAction()
    {   
        return $this->render('SpeAppBundle:Template:myprofile/ui/transact.html.twig',$this->get("spe.app.profile")->uiData());      
    }  
     /**
     * @Route("/myProfile/transact/segment", name="myProfileTransactSegment")
     */
    public function myProfileTransactSegmentAction(Request $request)
    {   
        
        return $this->render('SpeAppBundle:Template:myprofile/ui/transactSegment.html.twig',$request->request->all());      
    } 
     /**
     * @Route("/myProfile/email", name="myProfileEmail")
     */
    public function myProfileEmailAction()
    {       
        //var_dump($this->get("session")->all());
        $params = [];
        $params['name'] =  "";
        $planData = $this->get("session")->get("rkp")['plan_data'];
        if (!empty(trim($planData->firstName." ".$planData->lastName)))
            $params['name'] = $this->get("session")->get("rkp")['plan_data']->firstName." ".$this->get("session")->get("rkp")['plan_data']->lastName;
        $params['email'] = $this->getEmail();
        return $this->render('SpeAppBundle:Template:myprofile/ui/email.html.twig',$params);      
    } 
     /**
     * @Route("/myProfile/thankyou", name="myProfileThankyou")
     */
    public function myProfileThankYouAction()
    {
        $session = $this->get('session');
        $redirectUrl = $session->get('plan')['redirectWebsiteAddress'];
        if (filter_var($redirectUrl, FILTER_VALIDATE_URL) === FALSE) {
            $redirectUrl = null;
        }
        return $this->render('SpeAppBundle:Template:myprofile/ui/thankyou.html.twig', array('redirectUrl' => $redirectUrl));
    } 
     /**
     * @Route("/myProfile/ECOMMModalSubmit", name="ECOMMModalSubmit")
     */
    public function ECOMMModalSubmitAction(Request $request)
    {           
        $common = $this->get("session")->get("common");
        $common['email'] = $request->request->get("email");
        $common['edelivery'] = $request->request->get("edelivery") == "true" ? 1 : 0 ;
        $this->get("session")->set("common",$common);
        return new Response("");
    } 
    public function getEmail()
    {
        $params['email'] = "";
        $planData = $this->get("session")->get("rkp")['plan_data'];
        if (!empty($planData->email))
            $params['email'] = $planData->email;
        if (!empty($this->get("session")->get("common")['email']))
            $params['email']  = $this->get("session")->get("common")['email'];
        return $params['email'];
    }
     /**
     * @Route("/myProfile/rollover", name="myProfileRollover")
     */
    public function rolloverAction(Request $request)
    {           
        return $this->render('SpeAppBundle:Template:myprofile/ui/rollover.html.twig',['email' => $this->getEmail()]);     
    }

    /**
     * @Route("/myProfile/disclaimerAccepted", name="disclaimerAccepted")
     */
    public function disclaimerAcceptedAction(Request $request)
    {
        $session = $this->get('session');
        $session->set('moaDisclaimerAccepted', true);
        $this->ECOMMModalSubmitAction($request);
        return new Response("");
    }
}
