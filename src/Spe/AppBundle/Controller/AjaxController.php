<?php

namespace Spe\AppBundle\Controller;

use classes\classBundle\Entity\ATBlueprintOutlook;
use classes\classBundle\Entity\ATBlueprintPersonalInformation;
use classes\classBundle\Entity\ATBlueprintPersonalInformationOtherAccounts;
use classes\classBundle\Entity\ATBlueprintPersonalInformationOtherIncome;
use classes\classBundle\Entity\ATWsAccounts;
use Doctrine\ORM\EntityManager;
use classes\classBundle\Entity\participants;
use classes\classBundle\Entity\profiles;
use classes\classBundle\Entity\profilesContributions;
use classes\classBundle\Entity\videoStatsSpe;
use classes\classBundle\Entity\ATArchitect;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Shared\General\GeneralMethods;
/**
 * Ajax controller.
 * @Route("/", name="ajax")
 */

class AjaxController extends Controller
{
    /**
     * @Route("/initialize", name="initialize")
     * @Template("SpeAppBundle:Layout:layout.html.twig")
     */
    public function initializeAction(Request $request)
    {
        return $this->initialize($request);
    }
    
    public function initializeJsonAction(Request $request) 
    {
        return new JsonResponse($this->initialize($request));
    }

    protected function initialize(Request $request) {
        if($request->get('switch')){
            $this->get('spe.app.smart')->setDefaultData();
            $this->get('spe.app.smart')->setPlaylist();
            $this->get('spe.app.smart')->setCommonVariables();
            return array();
        }

        $session = $this->get("session");
        $session->set('api_url',$this->container->getParameter( 'api_url' ));
                    $session->set('profileId', uniqid('SPE_'));
        $this->get('spe.app.rkp')->getRKPData();
        $rkp = $session->get('rkp');
        if($rkp && isset($rkp['plan_data']) && count($rkp['plan_data'])) {
            $session->set('spe_plan_id',$rkp['plan_id']);
            $this->get('spe.app.plan')->setPlanData();
            $this->get('spe.app.smart')->setLocale();
            $this->get('spe.app.plan')->setPlanMessages();
            $this->get('spe.app.plan')->setLibraryData();
            $this->get('spe.app.smart')->setDefaultData();
            $this->get('spe.app.smart')->setSmart401kData($this->getFunds());
            $this->get('spe.app.smart')->setClientCalling();
            $this->get('spe.app.smart')->setPlaylist();
            $this->get('spe.app.smart')->setRetirementNeeds();
            $this->get('spe.app.smart')->setRiskProfile();
            $this->get('spe.app.smart')->setInvestments();
            $this->get('spe.app.smart')->setContributions();
            $this->get('spe.app.smart')->setMyProfiles();
            $this->get('spe.app.smart')->setBeneficiaries();
            $this->get('spe.app.profile')->createProfile();
            $this->get('spe.app.smart')->setCommonVariables();
            $this->get('spe.app.smart')->setAutosCommonVariables();
            $module = $session->get("module");
            if ($module['ATBlueprint'])
            {
                $ACInitialize = $this->get("spe.app.rkp")->ACInitialize();
                if ($ACInitialize['WsPlan']['AllowBlueprint'] == 'true') {

                    $session->set("ACInitialize", $ACInitialize);
                    $this->setATBlueprintPersonalInformation();
                    $module['ATArchitect'] = false;
                    if ($ACInitialize['WsPlan']['AllowArchitect'] == "true") {
                        $module['ATArchitect'] = true;
                    }
                    $module['BlueprintRecordExist'] = $ACInitialize['WsIndividual']['BlueprintRecordExist'];
                    $module['ArchitectIsEnrolled'] = $ACInitialize['WsIndividual']['ArchitectIsEnrolled'];

                    if ($module['BlueprintRecordExist'] == 'true') {
                        $this->get("session")->set("ATLastPath", "/atBlueprint/outlook/main");
                    }

                    if ($module['ArchitectIsEnrolled'] == 'true') {
                        $this->getArchitect();
                        $this->get("session")->set("ArchitectLastPath", "/investments/atarchitect/4");
                    }
                    $this->get("session")->set("VisitedOutlook", 0);
                    $session->set("module", $module);
                    $this->getOrCreateOutlook();
                    $common = $session->get('common');
                    $common['ATemail'] = $ACInitialize['WsIndividual']['Email'];
                    $session->set('common', $common);
                } else {
                    $module['ATBlueprint'] = false;
                }

            }
            $session->set("module",$module);
            $this->get('spe.app.smart')->setPlaylist();

        }else{
            $this->get('spe.app.plan')->setErrorEmail();

            if (isset($rkp['app_error']) || isset($rkp['error']))
                $status = array('error' => 1, 'data' => $this->get('spe.app.plan')->getErrorMessage($rkp['app_error']));
            else
                $status = array('error' => 1, 'data' => $this->get('spe.app.plan')->getErrorMessage('connection'));   

            $emailStatus = array('success' => false, 'message' => $this->get('spe.app.plan')->getErrorMessage('connection'));

            $data = (isset($session->get('REQUEST')['plan'])) ? $session->get('REQUEST')['plan'] : array();

            $this->get('spe.app.functions')->sendProfileErrorEmail($emailStatus, $data, FALSE);

            return $status;
        }
        return array();
    }


    /**
     * @Route("/welcome", name="welcome")
     */
    public function WelcomeAction(){
        $session = $this->get("session");
        $translator = $this->get('translator')->getMessages();
        if ($session->get('partBeneficiaries') != null)
        $benevalue = $session->get('partBeneficiaries');
        else
        $benevalue = $session->get('beneficiary'); 
        $additionalFundHeader = "";
        $session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader = $session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader;
        if (isset($translator['investments'][$session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader]))
        {
            $additionalFundHeader  = $translator['investments'][$session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader];
        }
        else if (isset($session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader))
        {
            $additionalFundHeader = $session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader;
        }
        return $this->toJSON(array(
            'library' =>  $session->get('library'),
            'playlist' => $session->get('playlist'),
            'mediaPath'  => array(
                'video_path' => $session->get('video_path'),
                'audio_path' => $session->get('audio_path'),
                'media_base' => $session->get('media_base'),
                'library_path' => $session->get('library_video_path'),
                'popup_path' => $session->get('popup_video_path'),
            ),
            'common' => $session->get('common'),
            'messaging' => $session->get('messaging'),
            'retirement_needs' => $session->get('retirement_needs'),
            'risk_profile' => $session->get('risk_profile'),
            'investments' => $session->get('investments'),
            'contributions' => $session->get('contributions'),
            'my_profiles' => $session->get('my_profiles'),
            'partBeneficiaries' => $session->get('partBeneficiaries'),
            'partBeneficiariesCopy' => $session->get('partBeneficiariesCopy'),
            'partBeneficiariesDeleted' => $session->get('partBeneficiariesDeleted'),
            'beneficiary' => $benevalue,
            'profile_transaction' => null,
            'translation' => $this->get('translator')->getMessages()['alert'],
            'serverConfig' => $session->get('serverConfig'),
            'participantid' => $session->get('participantid'),
            'originalUrl' => $session->get('originalUrl'),
            "module" => $session->get('module'),
            "translations" => $translator,
            "additionalFundHeader" => $additionalFundHeader,
            "pretaxslidercheckbox" => true,
            "rothslidercheckbox" => true,
            "spe2profileId" => $session->get("spe2profileId")
        ));
        
    }



    /**
     * @Route("/saveInvestments", name="saveInvestments")
     */
    public function saveInvestmentsAction(Request $request){
        $data = $request->get("data");
        if($data){
            foreach($data as $key => $item){
                if(is_array($item)){
                    $section = $this->get("session")->get($key);
                    foreach($item as $skey => $sitem)
                        $section[$skey]  = $sitem;
                    $this->get("session")->set($key, $section);
                }
                else{
                    $this->get("session")->set($key, $item);
                }
            }
        }
        //$this->updateSession($data);
        $this->updateProfile('investments');
        $investments = $this->get("session")->get('investments');
        $investments['defaultInvestmentFund'] = $this->get("session")->get('defaultInvestmentFund');
        return $this->toJSON($investments);
        //return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/updateSession", name="updateSession")
     */
    public function updateSessionAction(Request $request){
        $data = $request->get("data");
        $this->updateSession($data);
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/saveRetirementNeeds", name="saveRetirementNeeds")
     */
    public function saveRetirementNeedsAction(Request $request){
        $data = $request->get("data");
        $this->updateSession($data);
        $this->get("session")->set("currentYearlyIncome",$data['retirement_needs']['RN_CurrentYearlyIncome']);
        $this->updateProfile('retireNeeds');
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/saveContributions", name="saveContributions")
     */
    public function saveContributionsAction(Request $request){
        $data = $request->get("data");
        $this->updateSession($data);
        $this->get("session")->set("currentYearlyIncome",$data['contributions']['C_CurrentYearlyIncome']);
        $this->updateProfile('contributions');
        $flexPlan = $this->get('session')->get('common')['flexPlan'];
        if($flexPlan) {
            $this->flexPlanInvestor();
        }

        if ($this->get("session")->get("contributions")["C_ACAoptOut"]) {
            $this->get("session")->remove("autoIncreaseSave");
        }

        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/saveMyProfile", name="saveMyProfile")
     */
    public function saveMyProfileAction(Request $request){
        $data = $request->get("data");
        $session = $this->get("session")->all();
        isset($data['common']['email']) ? $session['common']['email'] = $data['common']['email'] : '';
        isset($data['common']['edelivery']) ? $session['common']['edelivery'] = $data['common']['edelivery'] : '';
        isset($data['common']['updateEPProfile']) ? $session['common']['updateEPProfile'] = $data['common']['updateEPProfile'] : '';
        isset($data['common']['updateBeneficiary']) ? $session['common']['updateBeneficiary'] = $data['common']['updateBeneficiary'] : '';
        isset($data['common']['updateProfileBeneficiary']) ? $session['common']['updateProfileBeneficiary'] = $data['common']['updateProfileBeneficiary'] : '';
        isset($data['common']['spousalWaiverFormStatus']) ? $session['common']['spousalWaiverFormStatus'] = $data['common']['spousalWaiverFormStatus'] : '';
        isset($data['my_profiles']['updateProfile']) ? $session['my_profiles']['updateProfile'] = $data['my_profiles']['updateProfile'] : '';
        $session['partBeneficiaries'] = isset($data['partBeneficiaries']) ?  $data['partBeneficiaries'] : '';
        $session['beneficiary'] = isset($data['beneficiary']) ? $data['beneficiary'] : '';
        isset($data['partBeneficiariesDeleted']) ? $session['partBeneficiariesDeleted'] = $data['partBeneficiariesDeleted'] : '';

        $fixedPartBeneficiariesFormat = function($data){
            if(isset($data['BeneficiaryBirthDate']) && $data['BeneficiaryBirthDate'] != '') {
                $data['BeneficiaryBirthDate'] = date('Y-m-d', strtotime($data['BeneficiaryBirthDate']));
            }
            return $data;
        };

        if($session['partBeneficiaries'] && !empty($session['partBeneficiaries'])){
            $session['partBeneficiaries'] = array_map($fixedPartBeneficiariesFormat, $session['partBeneficiaries']);
        }

        if($session['partBeneficiariesDeleted'] && !empty($session['partBeneficiariesDeleted'])){
            $session['partBeneficiariesDeleted'] = array_map($fixedPartBeneficiariesFormat, $session['partBeneficiariesDeleted']);
        }

        $this->get('session')->set('common', $session['common']);
        $this->get('session')->set('my_profiles', $session['my_profiles']);
        $this->get('session')->set('partBeneficiaries', $session['partBeneficiaries']);
        $this->get('session')->set('partBeneficiariesDeleted', $session['partBeneficiariesDeleted']);
        $this->get('session')->set('beneficiary', $session['beneficiary']);
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/saveProfileInfo", name="saveProfileInfo")
     */
    public function saveProfileInfoAction(Request $request){
        $em = $this->get('doctrine')->getManager();
        $data = $request->get("data");
        $session = $this->get("session");
        $this->updateSession($data);

        try {
            $profileId = $session->get('profileId') ? $session->get('profileId') : uniqid('SPE_');
            $data_user = array(
                'userId' => isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null,
                'planId' => isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null,
                'planName' => isset($session->get('rkp')['plan_data']->planName) ? $session->get('rkp')['plan_data']->planName : null,
                'phone' => isset($session->get('common')['phone']) ? $session->get('common')['phone'] : null,
                'availability' => isset($session->get('common')['availability']) ? $session->get('common')['availability'] : null,
                'profilestatus' => 0,
                'profileId' => $profileId,
                'firstName' => $session->get('common')['firstName'] ? strtoupper($session->get('common')['firstName']) : null,
                'lastName' => $session->get('common')['lastName'] ? strtoupper($session->get('common')['lastName']) : null,
                'dateOfBirth' => $session->get('common')['dateOfBirth'] ? $session->get('common')['dateOfBirth'] : null,
                'email' => $session->get('common')['email'] ? $session->get('common')['email'] : null,
                'address' => isset($session->get('common')['address']) && $session->get('common')['address'] ? $session->get('common')['address']: null,
                'city' => isset($session->get('common')['city']) ? $session->get('common')['city'] : null,
                'state' => isset($session->get('common')['state']) ? $session->get('common')['state'] : null,
                'zip' => isset($session->get('common')['postalCode']) ? $session->get('common')['postalCode'] : null,
                'maritalStatus' => isset($session->get('common')['maritalStatus']) && $session->get('common')['maritalStatus'] ? $session->get('common')['maritalStatus']: null,
                'gender' => isset($session->get('common')['gender']) ? $session->get('common')['gender']: null,
                'ssn' => isset($session->get('common')['ssn']) && $session->get('common')['ssn'] ? $session->get('common')['ssn']: null,
                'email2' => isset($session->get('common')['email2']) && $session->get('common')['email2'] ? $session->get('common')['email2'] : null,
                'preferredEmail' => isset($session->get('common')['preferredEmail']) && $session->get('common')['preferredEmail'] ? $session->get('common')['preferredEmail'] : null,
                'mobile' => isset($session->get('common')['mobile']) ? $session->get('common')['mobile'] : null,
                'location' => isset($session->get('common')['location']) ? $session->get('common')['location'] : null,
                'advisor' => isset($session->get('common')['advisor']) ? $session->get('common')['advisor'] : null
            );

            if ($session->get('profileId') && $profileId) {
                $profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
                $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('id' => $profiles->getParticipant()));
            } else {
                $participants = new participants();
                $profiles = new profiles();
            }

            $gender = $data_user['gender'] ? $data_user['gender'] : ($session->get('common')['firstName'] ? $this->get('spe.app.plan')->findParticipantGender($session->get('common')['firstName']) : null);

            $participants->setFirstName($data_user['firstName']);
            $participants->setLastName($data_user['lastName']);
            $participants->setBirthDate($data_user['dateOfBirth']);
            $participants->setGender($gender ? $gender : null);
            $participants->setEmail($data_user['email']);
            $participants->setEmail2($data_user['email2']);
            $participants->setPreferredEmail($data_user['preferredEmail']);
            $participants->setAddress($data_user['address']);
            $participants->setCity($data_user['city']);
            $participants->setState($data_user['state']);
            $participants->setZip($data_user['zip']);
            $participants->setMaritalStatus($data_user['maritalStatus']);
            $participants->setEmployeeId($data_user['ssn']);
            $participants->setPhone($data_user['phone']);
            $participants->setMobile($data_user['mobile']);
            $participants->setLocation($data_user['location']);
            $advisor =  $em->getRepository('classesclassBundle:plansAdvisor')->findOneBy(array('id' => $data_user['advisor']));
            if (!empty($advisor))
            {
                $participants->setAdvisorFirstName($advisor->firstName);
                $participants->setAdvisorLastName($advisor->lastName);
            }
            foreach (["Address","City","State","Zip"] as $field)
            {
               $participants->{"mailingAddress$field"} = $session->get('common')["mailingAddress$field"] ? $session->get('common')["mailingAddress$field"]  : "";
            }
            
            $em->persist($participants);

            $profiles->setParticipant($participants);
            $profiles->setUserid($data_user['userId']);
            $profiles->setPlanid($data_user['planId']);
            $profiles->setPlanName($data_user['planName']);
            $profiles->setAvailability($data_user['availability']);
            $profiles->setProfilestatus($data_user['profilestatus']);
            $profiles->sessionData = json_encode($request->getSession()->all());
            $profiles->userAgent = $request->headers->get('User-Agent');
            $profiles->smartEnrollPartId = isset($session->get("REQUEST")["smartenrollpartid"]) ? $session->get("REQUEST")["smartenrollpartid"] : null;
            $em->persist($profiles);

            $em->flush();
            $session->set('profileId', $profileId);
        }catch (\Exception $e){
            return $this->toJSON(array('status' => 'fail'));
        }
        return $this->toJSON(array('status' => 'success'));
    }

    public function updateSession($data){
        if($data){
            foreach($data as $key => $d){
                $this->get("session")->set($key, $d);
            }
        }
    }

    public function updateProfile($section) {
        $session = $this->get("session");
        $em = $this->get('doctrine')->getManager();

        if($session->get('rkp')['rkp'] == 'educate'){
            return;
        }

        $profile = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('id' => $session->get('pid')));
        if($section == 'retireNeeds'){
            $this->get('spe.app.profile')->addRetirementNeeds($profile);
        }elseif($section == 'riskProfile'){
            $this->get('spe.app.profile')->addRiskProfile($profile);
        }elseif($section == 'investments'){
            $this->get('spe.app.profile')->addInvestments($profile);
        }elseif($section == 'contributions'){
            $this->get('spe.app.profile')->addContributions($profile);
        }elseif($section == "autoIncrease"){
            $this->get('spe.app.profile')->addAutoIncrease($profile);
        }
    }
    /**
     * @Route("/silentSaveProfile", name="silentSaveProfile")
     */
    public function SilentSaveProfileAction(){
        $this->get('spe.app.profile')->updateProfile(0);
        return $this->toJSON(array('success' => 1));
    }
    /**
     * @Route("/riskProfileData", name="riskProfile")
     */
    public function RiskProfileAction(Request $request){
        $session = $this->get("session");
        $fileName = $request->request->get("filename");
        if (empty($fileName)) {
            $fileName = $request->query->get("filename", 'default');
        }
        $filePath = $path = $session->get('translation_path').'/riskProfile/'. $fileName.".xml";
        if(!file_exists($filePath)){
            $filePath = $path = $session->get('translation_path').'/riskProfile/'.'default.xml';
            $fileName = 'default';
        }
        $risk_profile = $this->get("session")->get('risk_profile');
        $risk_profile['RP_xml'] = $fileName;
        $this->get("session")->set('risk_profile', $risk_profile);
        $records = $this->get("RiskProfileQuestionnaireService")->loadQuestionsForApp($session->get('rkp')['plan_id'],$this->get("spe.app.smart")->getProfileLanguageWithoutPrefix());
        return $this->toJSON($records);
    }

    private function flexPlanInvestor(){
        $session = $this->get("session");
        $retirement_needs =  $session->get('retirement_needs');
        $risk_profile = $session->get('risk_profile');
        $contribution = $session->get('contributions');

        $age = isset($session->get('rkp')['plan_data']->dateOfBirth) ? floor((strtotime(date('m/d/Y')) - strtotime($session->get('rkp')['plan_data']->dateOfBirth)) / 31556926) : 0;
        $yearUntilRetirement = $retirement_needs['RN_NumberOfYearsBeforeRetirement'];
        $annualIncome = $retirement_needs['RN_CurrentYearlyIncome'];
        $totalAssets = $retirement_needs['RN_TotalAssets'];
        $deferralPercent = ($contribution['C_PreTaxContributionPct'] + $contribution['C_RothContributionPct'] + $contribution['C_PostTaxContributionPct'])/100;

        // For Testing
//        $age = 30;
//        $yearUntilRetirement = 35;
//        $annualIncome = 100000;
//        $totalAssets = 400000;
//        $deferralPercent = 0/100;
//        $retirement_needs['return'] = 7.95;
//        $retirement_needs['inflation'] = 2.41;
//        $retirement_needs['RN_RetirementAge'] = 65;
//        echo $this->get('spe.app.functions')->futureValue(7.95/100,35,-(0*100000),-400000);
//        echo "age: ".$age."<br>yearUntilRetirement: ".$yearUntilRetirement."<br>annualIncome: ".$annualIncome."<br>totalAssets: ".$totalAssets."<br>deferralPercent: ".$deferralPercent;
//        var_dump($retirement_needs);

        $fv = $yearUntilRetirement > 0 ?
            $this->get('spe.app.functions')->futureValue($retirement_needs['return']/100,$yearUntilRetirement,-($deferralPercent*$annualIncome),-$totalAssets) :
            $totalAssets;
        $pmt = $yearUntilRetirement > 0 ?
            $this->get('spe.app.functions')->payment($retirement_needs['inflation']/100,$retirement_needs['RN_LifeExpectancy']-$retirement_needs['RN_RetirementAge'],-$fv,0) :
            $this->get('spe.app.functions')->payment($retirement_needs['inflation']/100,$retirement_needs['RN_LifeExpectancy']-$age,-$fv,0);

        $need = $yearUntilRetirement > 0 ?
            $this->get('spe.app.functions')->futureValue($retirement_needs['inflation']/100,$yearUntilRetirement,0,-$annualIncome) :
            $annualIncome;

        $ratio = $pmt/$need*100;

        $plan_portfolios = $session->get('plan_portfolios');
        $investorType = 0;
        if(is_array($plan_portfolios) && count($plan_portfolios)){
            foreach ($plan_portfolios as $port) {
                if ($port['min_score'] <= $ratio && $ratio <= $port['max_score'] && $port['th_min_score'] <= $yearUntilRetirement && $yearUntilRetirement <= $port['th_max_score']) {
                    $investorType = (int) $port['profile'];
                    $risk_profile['recommendedPortfolioName'] = $port['name'];
                }
            }
        }
        $risk_profile['RP_InvestorType'] = (int)$investorType;
        $risk_profile['RP_Score'] = round($ratio);
        $risk_profile['RP_THScore'] = $yearUntilRetirement;
        $session->set('risk_profile', $risk_profile);
        //var_dump($risk_profile); exit;
    }

    /**
     * @Route("/riskProfileInvestor", name="riskProfileInvestor")
     */
public function riskProfileInvestorAction(Request $request){
        $session = $this->get("session");
        $risk_profile = $session->get('risk_profile');
        $investor_available = array(0,1,2,3,4);
        $answers = $request->get("answers");        
        $points = array();
        $questionsArray= array();
        $score = $th_score = 0;
        $xml = $risk_profile['RP_xml'];

        if(count($answers)) {
            $questions = $this->get("RiskProfileQuestionnaireService")->getQuestionnaireByPlanId($session->get('rkp')['plan_id'],$this->get("spe.app.smart")->getProfileLanguageWithoutPrefix());
            $scorings = simplexml_load_string($questions['questionnaire']->scoringXml);
            
            foreach ($answers as $index => $answer)
            {
                $questionObject = $questions['questions'][$index];
                $scoreObject = $questionObject['answers'][$answer];
                $points[] = $scoreObject["points"];
                $score += $scoreObject["points"];
                $questionsArray[] = ["question"=> $questionObject,"answer" => $scoreObject ];
                $th_score += $scoreObject["thpoints"];
            }
            if($session->get('module')['investmentSelectorRiskBasedPortfolio'] == 2) {
                $minScore = $questions["min"];
                $maxScore = $questions["max"];
                if(isset($maxScore) && isset($minScore)) {
                    
                    $low = $minScore;
                    $high = 0;
                    $mul1 = $maxScore/count($investor_available);
                    $mul2 = $minScore/count($investor_available);
                    $investor_type = (isset($investor_available[0]))? $investor_available[0] : 0;

                    foreach($investor_available as $investor){
                        $high = $low + $mul1 - $mul2;
                        //$scoring[] = array('investor' => $investor, 'low' => ceil($low),'high' => ceil($high));
 
                        
                        if($score > ceil($low) && $score <= ceil($high)) {
                            $investor_type = $investor;
                            break;
                        }
                        $low = $high;
                    }
                }
            }
            else if ($xml !== "smart401k") {  // Get Investor type, name from plan Portfolios (DB). Do not do for Kinetik RTQ as we want to use defaults in smart401k.xml
                $plan_portfolios = $session->get('plan_portfolios');
                if(is_array($plan_portfolios) && count($plan_portfolios)){
                    if($questions['thscoresEnabled']){
                        foreach ($plan_portfolios as $port) {
                            if ($port['min_score'] <= $score && $score <= $port['max_score'] && $port['th_min_score'] <= $th_score && $th_score <= $port['th_max_score']) {
                                $investor_type = (int) $port['profile'];
                                $risk_profile['recommendedPortfolioName'] = $port['name']; // Session variable
                                //break;
                            }
                        }
                    }
                    else{
                        foreach ($plan_portfolios as $port) {
                            if ($port['min_score'] == 0) $port['min_score'] = -1;
                            if ($port['min_score'] <= $score && $score <= $port['max_score']) {
                                $investor_type = (int) $port['profile'];
                                $risk_profile['recommendedPortfolioName'] = $port['name']; // Session variable
                                break;
                            }
                        }
                    }
                }
            }

            ### Default Scoring logic Based on XML ###
            if(!isset($investor_type) && isset($scorings->defaultScoring)) {
                $scoring_attr = $scorings->defaultScoring->attributes();
                if ($xml === "smart401k") { // don't bother doing the below if we using kinetik advice
                    $score_section = 0;
                }
				else if ($session->get('module')['investmentSelectorRiskBasedPortfolio'] == 0) {
					$tmp_count = 0;
					foreach(['riskProfileConservative','riskProfileModeratelyConservative','riskProfileModerate','riskProfileModeratelyAggressive','riskProfileAggressive'] as $key) {
						if ($session->get('module')[$key] == 1) {
							$tmp_count++;
						}
					}
					if($tmp_count <= 3) $score_section = 1;
					if(3 < $tmp_count && $tmp_count <= 5) $score_section = 0;
					if(5 < $tmp_count && $tmp_count <= 10) $score_section = 2;
				}
                elseif($scoring_attr['portfolioCount'] == 'yes'){
                    // count portfolios
                    $portfolios = $session->get('rkp')['plan_data']->portfolios;
                    if(count($portfolios) <= 3) $score_section = 1;
                    if(3 < count($portfolios) && count($portfolios) <= 5) $score_section = 0;
                    if(5 < count($portfolios) && count($portfolios) <= 10) $score_section = 2;
                } else{
                    $score_section = 0;
                }

                if($questions['thscoresEnabled']){
                    foreach($scorings->defaultScoring->scoreSection[$score_section]->profiles->profile as $profile){
                        $profile_attr = $profile->attributes();
                        if($profile_attr['low'] == '0') $profile_attr['low'] = -1;
                        if($profile_attr['low'] <= $score && $score <= $profile_attr['high'] && $profile_attr['tlow'] <= $th_score && $th_score <= $profile_attr['thigh']){
                            $investor_type = (int)$profile_attr['id'];
                            $investor_portfolio = (string)$profile_attr['investment'];
                            $risk_profile['recommendedPortfolioName'] = $investor_portfolio; // Session variable
                            break;
                        }
                    }
                }else{
                    foreach($scorings->defaultScoring->scoreSection[$score_section]->profiles->profile as $profile){
                        $profile_attr = $profile->attributes();
                        if(!$profile->ranges){
                            if($profile_attr['low'] == '0') $profile_attr['low'] = -1;
                            if($profile_attr['low'] < $score && $score <= $profile_attr['high']){
                                $investor_type = (int)$profile_attr['id'];
                                $investor_portfolio = (string)$profile_attr['investment'];
                                break;
                            }
                        } else {
                            foreach($profile->ranges->range as $range ){
                                $range_attr = $range->attributes();
                                if($range_attr['low'] == '0')$range_attr['low'] = -1;
                                if($range_attr['low'] < $score && $score <= $range_attr['high']){
                                    $investor_type = (int)$profile_attr['id'];
                                    $investor_portfolio = (string)$profile_attr['investment'];
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            ### Custom scoring logic based on XML ###
            switch ($questions['questionnaire']->customScoringType) {

                case 'portfolioMatch':
                    foreach ($scorings->customScoring->investments->investment as $investment) {
                        $invest_attr = $investment->attributes();

                        if ($invest_attr['low'] == '0')
                            $invest_attr['low'] = -1;
                        if ($invest_attr['low'] < $score && $score <= $invest_attr['high']) {
                            $cust_investor_type = $invest_attr['idArray'];
                            $idArray = explode(',', $invest_attr['idArray']);
                            break;
                        }
                        $cust_investor_type_name = $invest_attr['name'];
                    }

                    $portfolio_match = 0;
                    $portfolios = $session->get('rkp')['plan_data']->portfolios;
                    foreach ($portfolios as $portfolio) {
                        if (in_array($portfolio[0], $idArray)) {
                            $portfolio_match = $portfolio[0];
                            break;
                        }
                    }
                    $risk_profile['RP_Portfolio_Match'] = $portfolio_match; // Session variable
                    break;

                case 'smart401k':
                    $smart401k_advice_profile_id = 0;
                    foreach ($scorings->customScoring->investments->investment as $investment) {
                        $invest_attr = $investment->attributes();
                        if ($invest_attr['low'] == '0') $invest_attr['low'] = -1;
                        if ($score > $invest_attr['low'] && $score <= $invest_attr['high']) {
                            $smart401k_advice_profile_id = (int) $invest_attr['id'];
                            break;
                        }
                    }
                    $risk_profile['smart401k_advice_profile_id'] = $smart401k_advice_profile_id; // Session variable
                    break;

                default:
                    break;
            }

//            echo "<br>score:".$score;
//            echo "<br>th_score:".$th_score;
//            echo "<br>type:".$investor_type;
//            echo "<br>smart401k_advice_profile_id:".$smart401k_advice_profile_id;
//            exit;

            if (!isset($investor_type)) $investor_type = 0;
            $risk_profile['RP_InvestorType'] = (int)$investor_type; // Session variable
            $risk_profile['RP_Score'] = $score; // Session variable
            $risk_profile['RP_THScore'] = $th_score; // Session variable
            $risk_profile['RP_Points'] = implode("|",$points); // Session variable
            $risk_profile['RP_Answers'] = $answers; // Session variable
            $risk_profile['RP_Questions'] = $questionsArray;
            //$risk_profile['RP_xml'] = $xml; // Session variable

            $session->set('smart401k_completed', $risk_profile['RP_xml'] == 'smart401k' ? 1:0 );
            $session->set('risk_profile', $risk_profile );

            //var_dump($risk_profile);
            //var_dump($session->get('risk_profile'));
            if ($this->get("session")->get("smart401k"))
            {
                $this->kinetikAddParticipant();
                $kinetikParticipant = $this->get("kinetik.participant");
                $adviceFunds = $kinetikParticipant->getParticipant($this->kinetikPartnerParticipantId());
                $sessionAdviceFunds = [];
                foreach ($adviceFunds as $adviceFund)
                {
                    $sessionAdviceFunds[$adviceFund['ticker']] = $adviceFund;
                }
                $this->get("session")->set("sessionAdviceFunds",$sessionAdviceFunds);
            }
            if ($xml === 'stadionManagedAccount') {
                $this->get('spe.app.smart')->setStadionQuestionnaireSession();
                $risk_profile = $this->get('spe.app.smart')->setStadionManagedAccountData();
            }
            else {
                $risk_profile = $this->get('spe.app.smart')->setRiskProfileInvestors();
            }
            $this->updateProfile('riskProfile');
        }
        return $this->toJSON($risk_profile);
    }
    public function kinetikPartnerParticipantId()
    {
        $session = $this->get("session");
        return "{$session->get('plan')['partnerid']}*sep*{$session->get('rkp')['participant_uid']}";
    }
    public function kinetikAddParticipant($profileId = "")
    {
        $session = $this->get("session");
        $risk_profile = $session->get("risk_profile");
        $kinetikParticipant = $this->get("kinetik.participant");
        $partnerParticipantId = $this->kinetikPartnerParticipantId();
        $firstName = $session->get('rkp')['plan_data']->firstName;
        $lastName = $session->get('rkp')['plan_data']->lastName;
        $birthDate = $session->get('rkp')['plan_data']->dateOfBirth;
        $birthDate = date('Y-m-d', strtotime($birthDate));
        $partnerPlanId = "{$session->get('plan')['partnerid']}*sep*{$session->get('plan')['planid']}";
        $prePct = $rothPct = null;
        $salary = "";
        $employerContributionPct = ""; // always send empty string for now

        if (isset($session->get('retirement_needs')['RN_CurrentYearlyIncome']) && $session->get('retirement_needs')['RN_CurrentYearlyIncome'] != "")
        {
            $salary = (float)$session->get('retirement_needs')['RN_CurrentYearlyIncome'];
        }
        if (isset($session->get('contributions')['C_PreTaxContributionPct']) && $session->get('contributions')['C_PreTaxContributionPct'] != "")
        {
            $prePct = (float)$session->get('contributions')['C_PreTaxContributionPct'];
        }
        if (isset($session->get('contributions')['C_RothContributionPct']) && $session->get('contributions')['C_RothContributionPct'] != "")
        {
            $rothPct = (float)$session->get('contributions')['C_RothContributionPct'];
        }
        $contributionPct = $prePct + $rothPct;
        if (empty($contributionPct)) { // need to send "" instead of null if prePct and rothPct are both null
            $contributionPct = "";
        }
        $risk = $risk_profile['RP_Score'];

        $response = $kinetikParticipant->addParticipant($partnerParticipantId, $firstName, $lastName, $birthDate, $partnerPlanId, $profileId,
            $contributionPct, $employerContributionPct, $salary, $risk, array());
        return $response;
    }
    /**
     * @Route("/updateTransaction", name="updateTransaction")
     */
    public function updateTransactionAction(Request $request, $isSpe2 = false){
        $session = $this->get("session");
        $translator = $this->get('translator')->getMessages();

        $t = $request->request->get("id");
        $isTest = $session->get('testing') ? 1 : 0;
        $testingTrans = $session->get('testingTrans');
        $actions = array(
            'Personal' => 'personalConfirmationStatus',
            'Elections' => 'investmentsConfirmationStatus',
            'Realignment' => 'realignmentConfirmationStatus',
            'Contributions' => 'contributionsConfirmationStatus',
            'Catch Up' => 'catchupContributionConfirmationStatus',
            'Beneficiary' => 'beneficiaryConfirmationStatus',
            'Enrollment' => 'enrollmentConfirmationStatus',
            'Selections' => 'investmentsConfirmationStatus',
            'ATFutureBuilder' => 'ATTransactStatus'
        );
        $response = array();

        if($t == '0' && !$isTest){
            $response = $this->get('spe.app.rkp')->postPersonal();
        }elseif($t == '1' && (!$isTest || $testingTrans)){
            $isTest = false;
            $response = $this->get('spe.app.rkp')->postElections();
        }elseif($t == '2' && (!$isTest || $testingTrans)){
            $isTest = false;
            $response = $this->get('spe.app.rkp')->postRealignment();
        }elseif($t == '3' && (!$isTest || $testingTrans)){
            $isTest = false;
            $response = $this->get('spe.app.rkp')->postContributions();
        }elseif($t == '4' && (!$isTest || $testingTrans)){
            $isTest = false;
            $response = $this->get('spe.app.rkp')->postCatchUp();
        }elseif($t == '5' && (!$isTest || $testingTrans)){
            $response = $this->get('spe.app.rkp')->postBeneficiary();
        }elseif($t == '6' && !$isTest){
            sleep(5);
            $response = $this->get('spe.app.rkp')->postEnrollment();
        }elseif($t == '7' && (!$isTest || $testingTrans)){
            $isTest = false;
            $responses = $this->get('spe.app.rkp')->postCombined();
            $responses = json_decode($responses,true);
            if(count($responses) == 1) {
                $response = json_encode(current($responses));
            }else {
                $response = json_encode($responses);
            }
            $first = current($responses);
            if (
                $session->get('module')['stadionManagedAccountOn'] 
                && $session->get('managedAccountOptInValue')
                && isset($first['confirmation'])
                && isset($first['success']) && $first['success']
            ) {
                $this->get('spe.app.rkp')->postStadionConfirmation($first['confirmation']);
            }
        }elseif($t == '8' && !$isTest) {
            $profileid = $session->get('pid');
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
            $outlook= $repository->findOneBy(array('profileid' => $profileid));
            $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATArchitect');
            $architect = $repository->findOneBy(array('profileid' => $profileid));
            $transData = $this->getWsBlueprint();

            if (($outlook != null && $outlook->complete))
            {
                $transData['WsAccounts'] = $this->getCurrentATWsAccountsAsXmlArray($profileid);
            }

            $architectIsEnrolled = $session->get("ACInitialize")['WsIndividual']['ArchitectIsEnrolled'];
            $transData['WsIndividual']['OverrideAnnualCompensation'] =  'true';

            if ($architectIsEnrolled == 'true') {
                if ($architect->acceptedInvestment == 0) {
                    $transData['ArchitectOptOut'] = 'true';
                } else {
                    $transData['ArchitectOptOut'] = 'false';
                }
            } else {
                if ($architect->acceptedInvestment == 1) {
                    $transData['ArchitectOptIn'] = 'true';
                } else {
                    $transData['ArchitectOptIn'] = 'false';
                }
            }

            $response = $this->get("spe.app.rkp")->ACTransact($transData);
            $response['success'] = 1;
            $response['message'] = 'Transaction request processed';
            $response['confirmation'] = $response['ConfirmationId'];
            $response = json_encode($response);

        }
        elseif($t == "9")
        {
            $kinetikResponse = $this->kinetikAddParticipant($session->get('profileId'));
            $response['success']= $kinetikResponse == null;
            $response['message'] = ($response['success'] ? "Advice funds processed successfully." : "Advice funds failed.");
            $response['confirmation'] = "";
            $response = json_encode($response);
        }
        elseif($t == '10'){
            if ($isSpe2) {
                $this->get("Spe2Service")->updateProfile();
                $response = json_encode(['success' => 2, 'message' => 'Profile ID: ', 'confirmation' => $session->get('spe2profileId')]);
            }
            else{
                $response = $this->get('spe.app.profile')->updateProfile();
            }
        }
        elseif($t == '11')
        {
            $response = $this->get('spe.app.rkp')->postACAOptOut();
        }
        elseif ($t == '12')
        {
            $response = $this->get('spe.app.rkp')->postPartASISettings();
        }
        
        if ($isTest && !in_array($t, ['9', '10', '11', '12']))
        {
            $response = json_encode(array(
                'success' => 1,
                'message' => array_keys($actions)[$t] . ' '.$translator['my_profile']['change_processed_successfully'],
                'confirmation' => time()
            ));
        }       
        
        $checkResponse = json_decode($response, true);
        $emailData = json_decode(json_encode($session->get('rkp')['plan_data']), true);
        
        if (count($checkResponse) > 1) {
            if (!array_key_exists('success', $checkResponse)) {
                $response = $first;
            }
            foreach($checkResponse as $key => $resp){
                if(isset($resp['success']) &&  !$resp['success']){
                   $this->get('spe.app.functions')->sendProfileErrorEmail($resp, $emailData);
                   $response['success'] = false;
                   $response['confirmation'] = $key . " failed";
                   break;
                }
            }
            if (!array_key_exists('success', $checkResponse)) {
                $response = json_encode($response);
            }
        }else if(isset($checkResponse['success']) ||  !$checkResponse['success']) {
            
            $this->get('spe.app.functions')->sendProfileErrorEmail($checkResponse, $emailData);
          
        }        
        $responseSet = false;
        if(isset($responses))
        {
            foreach ($responses as $key => $responseObject)
            {
                if (in_array($key,["enrollment","beneficiary","contributions","elections","realignment"]))
                {
                    $response = json_encode($responseObject);
                    $responseSet = true;
                    if ($key == "enrollment")
                        $session->set("enrollmentConfirmationStatus",$response); 
                    if ($key == "beneficiary")
                        $session->set("beneficiaryConfirmationStatus",$response); 
                    if ($key == "contributions")
                        $session->set("contributionsConfirmationStatus",$response); 
                    if ($key == "elections")
                        $session->set("investmentsConfirmationStatus",$response);
                    if ($key == "realignment")
                        $session->set("realignmentConfirmationStatus",$response);
                }
            }
        }
        if(isset(array_values($actions)[$t]) && !$responseSet){
            $session->set(array_values($actions)[$t], $response);
        }
        return $this->toJSON(json_decode($response,true));
    }

    /**
     * @Route("/sendProfile", name="sendProfile")
     */
    public function sendProfileAction(Request $request){
        $response = $this->sendProfileEmail();
        if($response === null){
            return $this->toJSON(array('status' => 'null'));
        }
        else if($response === false){
            return $this->toJSON(array('status' => 'fail'));
        }
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/sendProfileEmail", name="sendProfileEmail")
     */
    public function sendProfileEmailAction(Request $request){
        $session = $this->get("session");
        $em = $this->get('doctrine')->getManager();
        $email = $request->get('email');
        $profileId = $session->get('profileId');
        if($email && $profileId) {
            if (!$this->sendProfileEmail($email)) {
                return $this->toJSON(array('status' => 'fail'));
            }
            $profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
            $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('id' => $profiles->getParticipant()));
            $participants->setEmail($email);
            $em->persist($participants);
            $em->flush();
        }
        return $this->toJSON(array('status' => 'success'));
    }

    /**
     * @Route("/postACAOptOut", name="postACAOptOut")
     */
    public function postACAOptOutAction(){
        if($this->get('session')->get('testing')){
            $response = json_encode(array('success' => 1, 'message' => 'ACA change processed sucessfully.', 'confirmation' => time()));
        }else{
            $response = $this->get('spe.app.rkp')->postACAOptOut();
        }
        $this->get('session')->set('ACARequestResponse', $response);
        return $this->toJSON(array('status' => 'success', 'response' => $response));
    }

    private function sendProfileEmail($email = null, $bcc = null){
        $session = $this->get("session");
        $translator = $this->get('translator')->getMessages();
        $planName = isset($session->get('plan')['name']) && $session->get('plan')['name'] ? $session->get('plan')['name'] : $session->get('rkp')['plan_data']->planName;
        $emails = array();
        if ($email != null){
            $emails[] = trim($email);
        }
        else if(isset($session->get('plan')['profileNotificationEmail']) && $session->get('plan')['profileNotificationEmail'] != ""){
            $emailSplit = explode(",",$session->get('plan')['profileNotificationEmail']);
            foreach ($emailSplit as $email){
                $emails[] = trim($email);
            }
        }
        else 
        return null;
        

        $emailer = $this->get('spe.app.mailer');
        foreach ($emails as $email){
            $from = $session->get("plan")['investorProfileEmail'];
            $subject = 'Your Investor Profile';
            $content = "<div style='margin-top:30px;font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height: 25px;color: black;'>
                                <span style='font-size: 20px;line-height: 30px;'>". $translator['my_profile']['download_your_investor_profile_for'] ." <br> $planName <br></span>
                                <a href='".$this->get('spe.app.functions')->getBaseUrl()."/profile/report/".$session->get('profileId')."' target='_blank'>". $translator['my_profile']['your_investor_profile'] ."</a>
                                <br><br>Thank you!<br>
                                <span style='padding-top:10px;margin-top:10px;font-family: Arial, Helvetica, sans-serif;font-size: 12px;line-height: 10px;'>&copy; ".date('Y')." ". $translator['my_profile']['smartplan_enterprise_is_trademark_of_vwise'] ."</span>
                            </div>";
            if(!$emailer->sendMail($email, $from, $subject, $content, null, $bcc)){
                return false;
            }
        }
        return true;
    }

    /**
     * @Route("/ping", name="ping")
     */
    public function pingAction(){
        $this->get("session")->set('last_activity',time());
        $response = $this->get('spe.app.rkp')->ping();
        return $this->toJSON(json_decode($response,true));
    }

    /**
     * @Route("/sessionTimeLeft", name="sessionTimeLeft")
     */
    public function sessionTimeLeftAction(){
        $session = $this->get("session");
        $exp_time = $session->get('session_expiration');
        $last_activity = $session->get('last_activity');
        $cur_time = time();
        $time_left = ($exp_time - ($cur_time - $last_activity)) * 1000;
        return $this->toJSON(array('time_left' => $time_left));
    }

    protected function toJSON($data){
        $request = $this->get("session")->get('REQUEST');
        if(isset($request['server']) && $request['server'] == 1) {
            $data['ip'] = $_SERVER['SERVER_ADDR'];
        }
        return new Response(json_encode($data));
    }

    /**
     * @Route("/postVideoStats", name="postVideoStats")
     */
    public function postVideoStatsAction(Request $request){
        $em = $this->get('doctrine')->getManager();
        $videoStatsSpe = new videoStatsSpe();
        $arr =  $request->request->all();
        $generalMethods = $this->get('GeneralMethods');
        $session = $this->get("session");
        $repository = $em->getRepository('classesclassBundle:profiles');
        $profile = $repository->findOneBy(array("profileId" => $session->get("profileId")));
        foreach ($arr as $key => $value)
        {
            $videoStatsSpe->$key = $value;
        }
        $videoStatsSpe->sessionid = $session->getId();
        $videoStatsSpe->ipaddress = $generalMethods->get_client_ip();
        $videoStatsSpe->planid = $session->get("plan")['id'];
        $videoStatsSpe->recordkeeperPlanid = $session->get("plan")['planid'];
        $videoStatsSpe->userid = $session->get("plan")['userid'];
        $videoStatsSpe->partnerid = $session->get("plan")['partnerid'];
        $videoStatsSpe->participantid = $profile->participant->id;
        $videoStatsSpe->uniqid = $profile->uniqid;
        $em->persist($videoStatsSpe);
        $em->flush();
        return new Response("");
    }
    /**
     * @Route("investments/table", name="investmentsTable")
     */
    public function investmentsTableAction(Request $request)
    {
        return $this->render('SpeAppBundle:Template:investments/graph/table.html.twig');
    }
    /**
     * @Route("investments/graph", name="investmentsGraph")
     */
    public function investmentsGraphAction()
    {       
        return $this->render('SpeAppBundle:Template:investments/graph/graph.html.twig');
    }    
    /**
     * @Route("investments/graph2", name="investmentsGraph2")
     */
    public function investmentsGraph2Action(Request $request)
    {
        if ($request->request->get("id","") != "")
        {
            $this->get("session")->set("PlansInvestmentsGraphSelectedMinScore",$request->request->get("id"));
        }
        return $this->render('SpeAppBundle:Template:investments/graph/graph2.html.twig',$this->calculateInvestmentsGraphTable());
    } 
    /**
     * @Route("investments/graph3", name="investmentsGraph3")
     */    
    public function investmentsGraph3Action(Request $request)
    {
        $scores = $request->request->get("scores");
        $savedScores = [];
        foreach ($scores['groups'] as $score)
        {
            foreach ($score['funds'] as $fund)
                if (!empty($fund['percent']))               
                    $savedScores[] =$fund;
        }
        $this->get("session")->set("graphScores",$request->request->get("scores"));
        $this->get("session")->set("savedFunds",$savedScores);
        $this->get("session")->set("investmentSection","RISK");
        return $this->render('SpeAppBundle:Template:investments/graph/graph3.html.twig',$this->calculateInvestmentsGraphTable());
    }
    /**
     * @Route("investments/completedGraph", name="completedGraph")
     */    
    public function completedGraphAction(Request $request)
    {
        $this->get("session")->set("investmentsComplete",true);
        $this->get("session")->set("investmentsGraphComplete",1);
        return new Response("");
    }

    private function _populateEntity ($input, $extras, $entity)
    {
        $exemptComma = ['monthlyRetirementGoal', 'socialSecurityMonthlyAmount', 'spouseSocialSecurityMonthlyAmount','annualCompensation','spouseAnnualCompensation','accountBalance','projectedAnnualReturn','estimatedMonthlyIncome'];

        if ($input != null) {
            foreach ($input as $item) {
                if (in_array($item['name'], $exemptComma)) {
                    $entity->{$item['name']} = str_replace(',', '', $item['value']);
                } else {
                    $entity->{$item['name']} = $item['value'];
                }
                //exceptions time
                if ($item['name'] == "mobilePhone") {
                    $entity->setMobilePhone($item['value']);
                } else if ($item['name'] == "balanceAsOf") {
                    $time = strtotime($item['value']);
                    $newformat = date('Y-m-d', $time);
                    $newformat .= 'T00:00:00';
                    $entity->{$item['name']} = $newformat;
                } else if (($item['name'] == "eeAdditions" || $item['name'] == "erAdditions") && $item['value'] == "") {
                    $entity->{$item['name']} = "0.00";
                }
            }
        }

        if ($extras != null) {
            foreach ($extras as $key => $value) {
                $entity->$key = $value;
            }
        }

        return $entity;
    }

    private function _checkIfSpouseExists()
    {
        $spouseInfo = $this->ATBlueprintPersonalInformation();
        $hidden = true;
        if ($spouseInfo->spouseFirstName != '' || $spouseInfo->spouseLastName != '') {
            $hidden = false;
        }
        return $hidden;
    }

    /**
     * @Route("/atBlueprint/personalInfo/update", name="atBlueprintPersonalInfoUpdate")
     */
    public function atBlueprintPersonalInfoUpdateAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $table = $request->request->get('table');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $table);
        $id = $request->request->get('id');
        $toedit = $repository->findOneBy(array('id' => $id));

        $input = $request->request->get("fields");
        $extras = $request->request->get("extras");

        $toedit = $this->_populateEntity($input, $extras, $toedit);

        $em->persist($toedit);
        $em->flush();

        return new Response('saved');
    }

    /**
     * @Route("/atBlueprint/personalInfo/save", name="atBlueprintPersonalInfoSave")
     */
    public function atBlueprintPersonalInfoSaveAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $table = $request->request->get('table');
        $fullclass = 'classes\\classBundle\\Entity\\' . $table;
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $table);
        $profileid = $this->get("session")->get("pid");
        $userid = $this->get("session")->get("plan")['userid'];
        $planid = $this->get("session")->get("plan")['id'];

        $oneToMany = $request->request->get('oneToMany');
        $toadd = null;
        if ($oneToMany != true) {
            $toadd = $repository->findOneBy(array('profileid' => $profileid));
        }
        if ($toadd == null) {
            $toadd = new $fullclass;
            $toadd->profileid = $profileid;
            $toadd->userid = $userid;
            $toadd->planid = $planid;
        }
        $input = $request->request->get("fields");
        $extras = $request->request->get("extras");

        $toadd = $this->_populateEntity($input, $extras, $toadd);

        foreach ($input as $item)
        {
            if ($item['name'] == "includeSpousalInformation")
            {
                if ($item['value'] == 'on') {
                    $toadd->includeSpousalInformation = "true";
                } else {
                    $toadd->includeSpousalInformation = "false";
                }
            }
        }
        if ($table == 'ATWsAccounts' && $toadd->balanceAsOf == null) { // null balanceAsOf messes with call
            $dateObject = new \DateTime();
            $toadd->balanceAsOf = $dateObject->format("Y-m-d")."T00:00:00";
        }
        $em->persist($toadd);
        $em->flush();

        return new Response('saved');
    }

    /**
     * @Route("/atBlueprint/personalInfo/delete", name="atBlueprintPersonalInfoDelete")
     */
    public function atBlueprintPersonalInfoDeleteAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $table = $request->request->get('table');
        $id = $request->request->get('id');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:' . $table);
        $userid = $this->get("session")->get("plan")['userid'];

        $personalInfo = $repository->findOneBy(array('id' => $id));
        if ($userid == $personalInfo->userid) {
            $em->remove($personalInfo);
            $em->flush();
        }

        return new Response('saved');
    }
    function ATBlueprintPersonalInformationHash($ATBlueprintPersonalInformation)
    {
        $individual = $this->get("session")->get("ACInitialize")['WsIndividual'];
        $rkp = $this->get("session")->get('rkp');
        $params= [
            "firstName" =>
            [
                "entityField" => "firstName",
                "entityValue" => $ATBlueprintPersonalInformation->firstName,
                "rkpValue" => $this->get("session")->get("rkp")["plan_data"]->firstName
            ],
            "lastName" =>
            [
                "entityField" => "lastName",
                "entityValue" => $ATBlueprintPersonalInformation->lastName,
                "rkpValue" => $this->get("session")->get("rkp")["plan_data"]->lastName
            ],
            "email" =>
            [
                "entityField" => "email",
                "entityValue" => $ATBlueprintPersonalInformation->email,
                "rkpValue" => $individual['Email'],
                "rkpField" => "Email"
            ],
            "mobilePhone" =>
            [
                "entityField" => "mobilePhone",
                "entityValue" => $ATBlueprintPersonalInformation->mobilePhone,
                "rkpValue" => $individual['MobilePhone'],
                "rkpField" => "MobilePhone"
            ],
            "annualCompensation" =>
            [
                "entityField" => "annualCompensation",
                "entityValue" => $ATBlueprintPersonalInformation->annualCompensation,
                "rkpValue" => $individual['AnnualCompensation'],
                "rkpField" => "AnnualCompensation"
            ],
            "dateOfBirth" =>
            [
                "entityField" => "dateOfBirth",
                "entityValue" => $ATBlueprintPersonalInformation->dateOfBirth,
                "rkpValue" => $rkp['plan_data']->dateOfBirth,
                "rkpField" => "BirthDate"
            ],
            "spouseFirstName" =>
            [
                "entityField" => "spouseFirstName",
                "entityValue" => $ATBlueprintPersonalInformation->spouseFirstName,
                "rkpValue" => $individual['SpouseFirstName'],
                "rkpField" => "SpouseFirstName"
            ],
            "spouseLastName" =>
            [
                "entityField" => "spouseLastName",
                "entityValue" => $ATBlueprintPersonalInformation->spouseLastName,
                "rkpValue" => $individual['SpouseLastName'],
                "rkpField" => "SpouseLastName"
            ],
            "spouseAnnualCompensation" =>
            [
                "entityField" => "spouseAnnualCompensation",
                "entityValue" => $ATBlueprintPersonalInformation->spouseAnnualCompensation,
                "rkpValue" => (float)$individual['SpouseAnnualCompensation'],
                "rkpField" => "SpouseAnnualCompensation"
            ],
            "spouseDateOfBirth" =>
            [
                "entityField" => "spouseDateOfBirth",
                "entityValue" => $ATBlueprintPersonalInformation->spouseDateOfBirth,
                "rkpValue" => $individual['SpouseBirthDate'],
                "rkpField" => "SpouseBirthDate"
            ],
            "includeSpousalInformation" =>
            [
                "entityField" => "includeSpousalInformation",
                "entityValue" => $ATBlueprintPersonalInformation->includeSpousalInformation,
                "rkpValue" => $individual['SpouseIncludeInBlueprint'],
                "rkpField" => "SpouseIncludeInBlueprint"
            ]
        ];
        if ($individual['OverrideReplacementIncome'] == 'true') {
            $params['monthlyRetirementGoal'] = array(
                "entityField" => "monthlyRetirementGoal",
                "entityValue" => $ATBlueprintPersonalInformation->monthlyRetirementGoal,
                "rkpValue" => $individual['ReplacementIncomeNeeded'],
                "rkpField" => "ReplacementIncomeNeeded"
            );
        }
        if ($individual['OverrideRetirementAge'] == 'true') {
            $params['retirementAge'] = array(
                "entityField" => "retirementAge",
                "entityValue" => $ATBlueprintPersonalInformation->retirementAge,
                "rkpValue" => $individual['RetirementAge'],
                "rkpField" => "RetirementAge"
            );
        }
        if ($individual['OverrideSsRetirementAge'] == 'true') {
            $params['socialSecurityBeginAge'] = array(
                "entityField" => "socialSecurityBeginAge",
                "entityValue" => $ATBlueprintPersonalInformation->socialSecurityBeginAge,
                "rkpValue" => $individual['SsRetirementAge'],
                "rkpField" => "SsRetirementAge"
            );
        }
        if ($individual['OverrideSsBenefit'] == 'true') {
            $params['socialSecurityMonthlyAmount'] = array(
                "entityField" => "socialSecurityMonthlyAmount",
                "entityValue" => $ATBlueprintPersonalInformation->socialSecurityMonthlyAmount,
                "rkpValue" => $individual['SsBenefit'],
                "rkpField" => "SsBenefit"
            );
        }
        if ($individual['OverrideSsSpousalBenefit'] == 'true') {
            $params['spouseSocialSecurityMonthlyAmount'] = array(
                "entityField" => "spouseSocialSecurityMonthlyAmount",
                "entityValue" => $ATBlueprintPersonalInformation->spouseSocialSecurityMonthlyAmount,
                "rkpValue" => $individual['SsSpousalBenefit'],
                "rkpField" => "SsSpousalBenefit"
            );
        }
        if ($individual['OverrideYearsInRetirement'] == 'true') {
            $params['yearsInRetirement'] = array(
                "entityField" => "yearsInRetirement",
                "entityValue" => $ATBlueprintPersonalInformation->yearsInRetirement,
                "rkpValue" => $individual['YearsInRetirement'],
                "rkpField" => "YearsInRetirement"
            );
        }
        $dateFields = array("dateOfBirth","spouseDateOfBirth");
        foreach ($dateFields as $field)
        {
            $params[$field]['rkpValue'] = $this->atConvertRkpDate($params[$field]['rkpValue']);
            $params[$field]['entityValue'] = $this->atConvertEntityDate($params[$field]['entityValue']);
        }
        $numberFields = array("annualCompensation","spouseAnnualCompensation");
        foreach($numberFields as $field)
        {
            $params[$field]['entityValue'] = $this->atConvertEntityNumber($params[$field]['entityValue']);
        }
        return $params;
    }
    function atConvertEntityNumber($number)
    {
        return number_format($number, 2, '.', '');
    }
    function atConvertRkpDate($date)
    {
        if ($date == null)
        {
            return "";
        }
        $dateObject = new \DateTime($date);
        return $dateObject->format("m/d/Y");
    }
    function initializeATWsAccounts()
    {
        $em = $this->get('doctrine')->getManager();
        foreach($this->get("session")->get('ACInitialize')['WsAccounts'] as $arr) {
            $account = new ATWsAccounts();
            $account->profileid = $this->get("session")->get("pid");
            $account->userid = $this->get("session")->get("plan")['userid'];
            $account->planid = $this->get("session")->get("plan")['id'];
            foreach ($arr as $key => $value) {
                $theKey = lcfirst($key);
                $account->$theKey = $value;
            }
            $em->persist($account);
        }
        $em->flush();
    }

    function atConvertEntityDate($date)
    {
        if ($date == null)
        {
            return "";
        }
        $dateObject = new \DateTime($date);
        return $dateObject->format("Y-m-d")."T00:00:00";
    }
    function initializeATBlueprintPersonalInformation()
    {
        $personalInfo = new ATBlueprintPersonalInformation();
        $initializeHash = $this->ATBlueprintPersonalInformationHash($personalInfo);
        foreach ($initializeHash as $row)
        {
            $personalInfo->{$row['entityField']} = $row['rkpValue'];
        }
        $personalInfo->profileid = $this->get("session")->get("pid");
        $personalInfo->userid = $this->get("session")->get("plan")['userid'];
        $personalInfo->planid = $this->get("session")->get("plan")['id'];

        $this->initializeATWsAccounts();

        return $personalInfo;
    }
    function ATBlueprintPersonalInformation()
    {

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $profileid = $this->get("session")->get("pid");
        $personalInfo = $repository->findOneBy(array('profileid' => $profileid));
        if ($personalInfo == null) {
            $personalInfo = $this->initializeATBlueprintPersonalInformation();
        }

        return $personalInfo;
    }
    function setATBlueprintPersonalInformation()
    {
        $em = $this->get('doctrine')->getManager();
        $personalInfo = $this->initializeATBlueprintPersonalInformation();
        $em->persist($personalInfo);
        $em->flush();
    }
    /**
     * @Route("/atBlueprint/personalInfo/required", name="atBlueprintPersonalInfoRequired")
     */
    public function atBlueprintPersonalInfoRequiredAction(Request $request)
    {
        $this->setATLastPath($request);
        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/required.html.twig', array('personalInfo' => $this->ATBlueprintPersonalInformation()));
    }
    /**
     * @Route("/atBlueprint/personalInfo/bridge", name="atBlueprintPersonalInfoBridge")
     */
    public function atBlueprintPersonalInfoBridgeAction(Request $request)
    {
        $this->setATLastPath($request);
        $personalInfo = $this->ATBlueprintPersonalInformation();
        $filledInMeasurements = false;
        if ($personalInfo != null) {
            if ($personalInfo->retirementAge != null || $personalInfo->yearsInRetirement != null || $personalInfo->socialSecurityBeginAge != null ||
                $personalInfo->monthlyRetirementGoal != null || $personalInfo->socialSecurityMonthlyAmount != null || $personalInfo->spouseSocialSecurityMonthlyAmount != null) {
                $filledInMeasurements = true;
            }
        }

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $profileid = $this->get("session")->get("pid");
        $accounts = $repository->findBy(array('profileid' => $profileid, 'isIncomeAccount' => 'false', 'includeInCalcs' => 'true'));

        $filledInAccounts = false;
        if ($accounts != null) {
            $blueprint = $this->getWsBlueprint()['WsBlueprint'];
            foreach($accounts as $account) {
                if($account->accountNumber != $blueprint['AccountNumber']) {
                    $filledInAccounts = true;
                    break;
                }
            }
        }

        $income = $repository->findBy(array('profileid' => $profileid, 'isIncomeAccount' => 'true', 'includeInCalcs' => 'true'));
        $filledInIncome = false;
        if ($income != null) {
            $filledInIncome = true;
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/bridge.html.twig', array('filledInMeasurements' => $filledInMeasurements, 'filledInAccounts' => $filledInAccounts, 'filledInIncome' => $filledInIncome));
    }

    /**
     * @Route("/atBlueprint/personalInfo/measurements", name="atBlueprintPersonalInfoMeasurements")
     */
    public function atBlueprintPersonalInfoMeasurementsAction(Request $request)
    {
        $this->setATLastPath($request);
        $personalInfo = $this->ATBlueprintPersonalInformation();
        $hidden = $this->_checkIfSpouseExists();

        $birthDate = $personalInfo->dateOfBirth;
        $birthDate = explode("/", $birthDate);
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));
        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/measurements.html.twig', array('personalInfo' => $personalInfo, 'hidden' => $hidden, 'age' => $age));
    }

    /**
     * @Route("/atBlueprint/personalInfo/otheraccounts", name="atBlueprintPersonalInfoOtherAccounts")
     */
    public function atBlueprintPersonalInfoOtherAccountsAction(Request $request)
    {
        $this->setATLastPath($request);
        $playAudio = $request->request->get('playAudio');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $profileid = $this->get("session")->get("pid");

        $blueprint = $this->getWsBlueprint();

        $personalInfo = $repository->findBy(array('profileid' => $profileid, 'isIncomeAccount' => 'false', 'includeInCalcs' => 'true'));
        foreach ($personalInfo as $key => $value) {
            if ($value->accountNumber == $blueprint['WsBlueprint']['AccountNumber']) {
                unset($personalInfo[$key]);
                break;
            }
        }
        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/otherretirementaccounts.html.twig', array('personalInfo' => $personalInfo, 'playAudio' => $playAudio));
    }

    /**
     * @Route("/atBlueprint/personalInfo/otherRetirementIncome", name="atBlueprintPersonalInfoOtherRetirementIncome")
     */
    public function atBlueprintPersonalInfoOtherRetirementIncomeAction(Request $request)
    {
        $this->setATLastPath($request);
        $playAudio = $request->request->get('playAudio');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $profileid = $this->get("session")->get("pid");

        $personalInfo = $repository->findBy(array('profileid' => $profileid, 'isIncomeAccount' => 'true', 'includeInCalcs' => 'true'));

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/otherretirementincome.html.twig', array('personalInfo' => $personalInfo, 'playAudio' => $playAudio));
    }

    /**
     * @Route("/atBlueprint/personalInfo/addretirementaccount", name="atBlueprintPersonalInfoAddRetirementAccount")
     */
    public function atBlueprintPersonalInfoAddRetirementAccountAction(Request $request)
    {
        $hidden = $this->_checkIfSpouseExists();

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/addretirementaccounts.html.twig', array('hidden' => $hidden));
    }

    /**
     * @Route("/atBlueprint/personalInfo/viewretirementaccount", name="atBlueprintPersonalInfoViewRetirementAccount")
     */
    public function atBlueprintPersonalInfoViewRetirementAccountAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $id = $request->query->get('id');
        $readonly = $request->query->get('readonly');
        $personalInfo = $repository->findOneBy(array('id' => $id));

        $hidden = $this->_checkIfSpouseExists();

        $returnArr = array('personalInfo' => $personalInfo, 'edit' => true, 'hidden' => $hidden);

        if ($readonly == 'true') {
            $returnArr['view'] = true;
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/addretirementaccounts.html.twig', $returnArr);
    }

    /**
     * @Route("/atBlueprint/personalInfo/viewotherretirementincome", name="atBlueprintPersonalInfoViewOtherRetirementIncome")
     */
    public function atBlueprintPersonalInfoViewOtherRetirementIncomeAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $id = $request->query->get('id');
        $readonly = $request->query->get('readonly');
        $personalInfo = $repository->findOneBy(array('id' => $id));

        $returnArr = array('personalInfo' => $personalInfo, 'edit' => true);

        if ($readonly == 'true') {
            $returnArr['view'] = true;
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/addotherrequirementincome.html.twig', $returnArr);
    }

    /**
     * @Route("/atBlueprint/personalInfo/addotherrequirementincome", name="atBlueprintPersonalInfoAddOtherRequirementIncome")
     */
    public function atBlueprintPersonalInfoAddOtherRequirementIncomeAction(Request $request)
    {
        return $this->render('SpeAppBundle:Template:ATBlueprint/personalInformation/addotherrequirementincome.html.twig');
    }

    private function setATLastPath($request)
    {
        $uri = $request->server->get('REQUEST_URI');
        $this->get("session")->set("ATLastPath",$uri);
    }

    private function setArchitectLastPath($request)
    {
        $uri = $request->server->get('REQUEST_URI');
        $this->get("session")->set("ArchitectLastPath",$uri);
    }

    private function getWsBlueprint()
    {
        $blueprint = $this->get("session")->get("ACBluePrint");
        if ($blueprint == null) {
            return $this->get("session")->get("ACInitialize");
        }
        return $blueprint;
    }

    public function getOrCreateOutlook()
    {
        $wsBlueprint = $this->getWsBlueprint();
        $blueprint = $wsBlueprint['WsBlueprint'];
        $em = $this->get('doctrine')->getManager();
        $profileid = $this->get("session")->get("pid");
        $userid = $this->get("session")->get("plan")['userid'];
        $planid = $this->get("session")->get("plan")['id'];
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));


        $addOutlook = false;
        if ($outlook == null)
        {

            $outlook = new ATBlueprintOutlook();
            $outlook->profileid = $profileid;
            $outlook->userid = $userid;
            $outlook->planid = $planid;
            $outlook->dollarsDeferred = $blueprint['DefRateDol'];
            if ($this->get("session")->get("module")['BlueprintRecordExist'] == 'true') {
                $outlook->improvedOutlook = true;
            }
            $acc = $this->getATPlanAccount();
            if ($acc) {
                $outlook->pretaxPercent = $acc->preTaxPct * 100;
                $outlook->rothPercent = $acc->rothPct * 100;
            }
            $addOutlook = true;
        }
        $outlook->currentPerMonth = $blueprint['AtIndexTrendDol'];
        if ($outlook->originalDeferralPercent == null || $outlook->originalDeferralPercent == 0) {
            $outlook->originalDeferralPercent = (int)($blueprint['DefRatePct'] * 100);
        }
        $outlook->deferralPercent = $blueprint['DefRatePct'] * 100.0;
        if ($addOutlook)
        {
            $em->persist($outlook);
        }
        $em->flush();

        return $outlook;

    }


    /**
     * @Route("/atBlueprint/outlook/main", name="atBlueprintOutlookMain")
     */
    public function atBlueprintOutlookMainAction(Request $request)
    {
        $this->setATLastPath($request);
        $wsBlueprint = $this->getWsBlueprint();
        $visitedOutlook = false;
        if ($this->get("session")->get('VisitedOutlook')) {
            $visitedOutlook = true;
        }
        $individual = $wsBlueprint['WsIndividual'];
        $blueprint = $wsBlueprint['WsBlueprint'];

        $personalInfo =  $this->ATBlueprintPersonalInformation();

        $outlook = $this->getOrCreateOutlook();

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/mainoutlook.html.twig', array('personalInfo' => $personalInfo, 'outlook' => $outlook,'individual' => $individual, 'blueprint' => $blueprint, 'visitedOutlook' => $visitedOutlook));
    }

    /**
     * @Route("/atBlueprint/outlook/saveoutlook", name="atBlueprintOutlookSaveOutlook")
     */
    public function atBlueprintOutlookSaveOutlookAction(Request $request)
    {
        $blueprint = $this->getWsBlueprint()['WsBlueprint'];
        $em = $this->get('doctrine')->getManager();
        $profileid = $this->get("session")->get("pid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));
        $outlook->deferralPercent = $blueprint['DefRatePct'] * 100.0;
        $outlook->currentPerMonth = $blueprint['IncomeMedian'];
        $outlook->dollarsDeferred = $blueprint['DefRateDol'];
        $outlook->improvedOutlook = true;
        $em->persist($outlook);
        $em->flush();

        return new Response('success');
    }

    /**
     * @Route("/atBlueprint/outlook/reminderpopup", name="atBlueprintOutlookShowReminderPopup")
     */
    public function atBlueprintOutlookShowReminderPopupAction(Request $request)
    {
        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/reminder-popup.html.twig');
    }

    /**
     * @Route("/atBlueprint/outlook/popup", name="atBlueprintOutlookShowPopup")
     */
    public function atBlueprintOutlookShowPopupAction(Request $request)
    {
        $profileid = $this->get("session")->get("pid");
        $blueprint = $this->getWsBlueprint()['WsBlueprint'];
        $data = array();
        $data['currentAge'] = $blueprint['Age'];
        $data['retirementAge'] = $blueprint['RetirementAge'];
        $data['socialSecurityStartingAge'] = $blueprint['SsRetirementAge'];
        $data['monthlySocialSecurityIncome'] = '$' . number_format($blueprint['SsBenefit'], 2, '.', ',');

        $wsAccounts = $this->getCurrentATWsAccountsAsXmlArray($profileid, true);
        $accounts = array();
        $i = 0;
        $totalBalance = 0;
        $totalIncome = 0;
        $totalAnnualAdditions = 0;

        $ATPlan = $this->getATPlanAccount();

        foreach ($wsAccounts as $account) {
            $accounts[$i]['name'] = $account['AccountName'];
            $accounts[$i]['currentBalance'] = $account['Balance'];
            $accounts[$i]['monthlyIncome'] = $account['MonthlyIncome'];
            $accounts[$i]['isIncome'] = $account['IsIncomeAccount'];
            if($account['AccountNumber'] == $ATPlan->accountNumber) {
                $accounts[$i]['projectedAnnualAdditions'] = $blueprint['PlanEeAdditions'] + $blueprint['PlanErAdditions'];
                $totalAnnualAdditions += $blueprint['PlanEeAdditions'] + $blueprint['PlanErAdditions'];
            } else {
                $accounts[$i]['projectedAnnualAdditions'] = $account['EeAdditions'] + $account['ErAdditions'];
                $totalAnnualAdditions += $account['EeAdditions'] + $account['ErAdditions'];
            }
            $totalBalance += $account['Balance'];
            $totalIncome += $account['MonthlyIncome'];
            $i++;
        }

        $projectedProgress['high'] = round($blueprint['AtIndexTrendHigh'] * 100, 1);
        $projectedProgress['low'] = round($blueprint['AtIndexTrendLow'] * 100, 1);

        $retirementAge = array();
        if($blueprint['AtIndexTrendHigh'] > 1) {
            $retirementAge['best'] = $blueprint['RetirementAge'] - $blueprint['AtAgeIndexHigh'];
        } else {
            $retirementAge['best'] = $blueprint['RetirementAge'] + $blueprint['AtAgeIndexHigh'];
        }
        if($blueprint['AtIndexTrendLow'] > 1) {
            $retirementAge['worst'] = $blueprint['RetirementAge'] - $blueprint['AtAgeIndexLow'];
        } else {
            $retirementAge['worst'] = $blueprint['RetirementAge'] + $blueprint['AtAgeIndexLow'];
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/popup.html.twig', array('data' => $data, 'accounts' => $accounts, 'totalBalance' => $totalBalance,
            'totalAnnualAdditions' => $totalAnnualAdditions, 'blueprint' => $blueprint, 'projectedProgress' => $projectedProgress, 'retirementAge' => $retirementAge, 'totalIncome' => $totalIncome));
    }

    /**
     * @Route("/atBlueprint/outlook/2", name="atBlueprintOutlook2")
     */
    public function atBlueprintOutlook2Action(Request $request)
    {
        $this->setATLastPath($request);
        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/monthly.html.twig');
    }

    /**
     * @Route("/atBlueprint/outlook/improve", name="atBlueprintOutlookImprove")
     */
    public function atBlueprintOutlookImproveAction(Request $request)
    {
        $this->setATLastPath($request);
        $this->get("session")->set("VisitedOutlook", 1);
        $blueprint = $this->getWsBlueprint()['WsBlueprint'];
        $profileid = $this->get("session")->get("pid");
        $personalInfo = $this->ATBlueprintPersonalInformation();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/improve.html.twig', array('personalInfo' => $personalInfo, 'outlook' => $outlook,'blueprint' => $blueprint));
    }

    /**
     * @Route("/atBlueprint/outlook/distributor", name="atBlueprintOutlookDistribute")
     */
    public function atBlueprintOutlookDistributeAction(Request $request)
    {
        $profileid = $this->get("session")->get("pid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $personalInfo = $repository->findOneBy(array('profileid' => $profileid));
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));

        $showAsterisk = false;

        if (is_numeric( $outlook->deferralPercent ) && floor( $outlook->deferralPercent ) != $outlook->deferralPercent) {
            $showAsterisk = true;
        }

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/distributor.html.twig', array('personalInfo' => $personalInfo, 'outlook' => $outlook, 'showAsterisk' => $showAsterisk));
    }

    /**
     * @Route("/atBlueprint/outlook/savedistribution", name="atBlueprintOutlookSaveDistribute")
     */
    public function atBlueprintOutlookSaveDistributeAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $profileid = $this->get("session")->get("pid");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));

        $pretax = $request->request->get('pretax');
        $roth = $request->request->get('roth');

        $common = $this->get('session')->get('common');

        if ($pretax) {
            $outlook->pretaxPercent = $pretax;
            $pretax = $pretax/100.0;
        } else {
            $outlook->pretaxPercent = '0.00';
            $pretax = '0.00';
        }
        if ($roth) {
            $outlook->rothPercent = $roth;
            $roth = $roth/100.0;
        } else {
            $outlook->rothPercent = '0.00';
            $roth = '0.00';
        }

        $ATPlan = $this->getATPlanAccount();

        if ($ATPlan) {
            $ATPlan->preTaxPct = $pretax;
            $ATPlan->rothPct = $roth;
            $ATPlan->defRateMethod = 'Percent';
        }

        $outlook->complete = 1;
        $em->flush();

        $this->setModule('ATPassedBlueprint', true);

        $c = array(
            'contribution_mode' => 'DEFERPCT',
            'C_PreTaxContributionPct' => null,
            'C_PreTaxContributionValue' => null,
            'C_RothContributionPct' => null,
            'C_RothContributionValue' => null,
            'C_PostTaxContributionPct' => null,
            'C_PostTaxContributionValue' => null,
            'updateContributions' => 1,
        );
        if ($outlook->pretaxPercent != null)
        {
            $c['C_PreTaxContributionPct'] = $outlook->pretaxPercent;
        }
        if ($outlook->rothPercent != null && $common['roth'])
        {
            $c['C_RothContributionPct'] = $outlook->rothPercent;
        }
        $this->get("session")->set('contributions', $c);

        return new Response('saved');
    }

    public function getATPlanAccount()
    {
        $profileid = $this->get("session")->get("pid");
        $blueprint = $this->getWsBlueprint()['WsBlueprint'];
        $accountNumber = $blueprint['AccountNumber'];

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        $wsAccounts = $repository->findBy(array('profileid' => $profileid));

        foreach ($wsAccounts as $account) {
            if ($account->accountNumber != $accountNumber) {
                continue;
            } else {
                return $account;
            }
        }

        return null;
    }

    /**
     * @Route("/atBlueprint/outlook/4", name="atBlueprintOutlook4")
     */
    public function atBlueprintOutlook4Action(Request $request)
    {

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/submit.html.twig');
    }

    /**
     * @Route("/atBlueprint/outlook/5", name="atBlueprintOutlook5")
     */
    public function atBlueprintOutlook5Action(Request $request)
    {

        return $this->render('SpeAppBundle:Template:ATBlueprint/outlook/contributionselector.html.twig');
    }

    /**
     * @Route("/investments/atarchitect/1", name="atArchitect1")
     */
    public function atArchitect1Action(Request $request)
    {
        $this->setArchitectLastPath($request);
        return $this->render('SpeAppBundle:Template:investments/atarchitect/enablepage.html.twig',array("architect" =>$this->getArchitect(),'optInMessage' => $this->get("spe.app.rkp")->ACInitialize()['WsPlan']['ArchitectOptInMessage']));
    }
    /**
     * @Route("/investments/atarchitect/1/saved", name="atArchitect1Saved")
     */
    public function atArchitect1SavedAction(Request $request)
    {
        $this->updateArchitect(array("takeAdvantage" => $request->request->get("enablepageAnswer")));
        if ($request->request->get("enablepageAnswer") == 0)
        {
            $this->setModule('ATArchitect', false);
            $this->updateArchitect(array("acceptedInvestment" => 0));
        }
        return new Response("");
    }
    /**
     * @Route("/investments/atarchitect/1/enable", name="atArchitect1Enable")
     */
    public function atArchitect1EnableAction(Request $request)
    {
        $this->setModule('ATArchitect', true);
        if ($this->get('session')->get('ATArchitectPreviouslyEnrolled')) {
            $this->updateArchitect(array("acceptedInvestment" => 1));
            $this->setModule('ArchitectIsEnrolled', true);
        }

        return $this->redirect('/investments');
    }

    public function setModule($name, $bool)
    {
        $module = $this->get("session")->get("module");
        $module[$name] = $bool;
        $this->get("session")->set("module",$module);

    }
    public function getArchitect()
    {
        $em = $this->get('doctrine')->getManager();
        $repository = $em->getRepository('classesclassBundle:ATArchitect');
        $ATArchitect = $repository->findOneBy(array("profileid" => $this->get("session")->get("pid")));
        $initialize = $this->get("session")->get("ACInitialize");
        $architectEnrolled = $initialize['WsIndividual']['ArchitectIsEnrolled'];
        if ($ATArchitect == null)
        {
            $ATArchitect = new ATArchitect();
            $ATArchitect->profileid = $this->get("session")->get("pid");
            $ATArchitect->userid = $this->get("session")->get("plan")['userid'];
            $ATArchitect->planid = $this->get("session")->get("plan")['id'];
            if ($architectEnrolled == 'true') {
                $ATArchitect->acceptedInvestment = 1;
                $ATArchitect->currentInvestment = $initialize['WsArchitectTask']['CurrentBalanceToFundId'];
                $ATArchitect->currentInvestmentPercent = $initialize['WsArchitectTask']['CurrentBalancePct'];
                $ATArchitect->suggestedInvestment = $initialize['WsArchitectTask']['NewMoneyToFundId'];
                $ATArchitect->suggestedInvestmentPercent = $initialize['WsArchitectTask']['NewMoneyPct'];
            }

            $em->persist($ATArchitect);
            $em->flush();
        }
        return $ATArchitect;
    }
    public function updateArchitect($array)
    {
        $em = $this->get('doctrine')->getManager();
        $ATArchitect = $this->getArchitect();
        foreach ($array as $key => $value)
        {
            $ATArchitect->$key = $value;
        }
        if ($ATArchitect->id == null)
        {
            $em->persist($ATArchitect);
        }
        $em->flush();
    }
    /**
     * @Route("/investments/atarchitect/2", name="atArchitect2")
     */
    public function atArchitect2Action(Request $request)
    {
        return $this->render('SpeAppBundle:Template:investments/atarchitect/disclaimer.html.twig',array("architect" =>$this->getArchitect()));
    }
    /**
     * @Route("/investments/atarchitect/2/saved", name="atArchitect2Saved")
     */
    public function atArchitect2SavedAction(Request $request)
    {
        $this->updateArchitect(array("agreement" => 1));
        $this->get("session")->set('investments', $this->transactInvestmentsAT());
        return new Response("");
    }
    public function getArchitectInvestments()
    {
        $data = $this->getWsBlueprint()['WsArchitectTask'];
        return
        [
            "currentInvestment" =>
            [
                "name" => $data['CurrentBalanceToFundId'],
                "percent" => $data['CurrentBalancePct'],
                "description" => $data['CurrentBalanceDescription']
            ],
            "suggestedInvestment" =>
            [
                "name" => $data['NewMoneyToFundId'],
                "percent" => $data['NewMoneyPct'],
                "description" => $data['NewMoneyDescription']
            ]
        ];
    }
    /**
     * @Route("/investments/atarchitect/3", name="atArchitect3")
     */
    public function atArchitect3Action(Request $request)
    {
        $this->setArchitectLastPath($request);
        return $this->render('SpeAppBundle:Template:investments/atarchitect/investmentselector.html.twig',array("investments"=> $this->getArchitectInvestments()));
    }
    /**
     * @Route("/investments/atarchitect/3/saved", name="atArchitect3Saved")
     */
    public function atArchitect3SavedAction(Request $request)
    {
        $session = $this->get("session");
        $investments = $this->getArchitectInvestments();
        $params =
        [
            "acceptedInvestment" => 1,
            "currentInvestment" => $investments['currentInvestment']['name'],
            "currentInvestmentPercent" => $investments['currentInvestment']['percent'],
            "suggestedInvestment" => $investments['suggestedInvestment']['name'],
            "suggestedInvestmentPercent" => $investments['suggestedInvestment']['percent'],
        ];
        $this->updateArchitect($params);
        $common = $session->get('common');
        $common['updateElections'] = true;
        $common['updateRealignment'] = true;
        $session->set('common', $common);
        $this->setModule('ArchitectIsEnrolled', true);
        return new Response("");
    }
    /**
     * @Route("/investments/atarchitect/4", name="atArchitect4")
     */
    public function atArchitect4Action(Request $request)
    {
        $this->setArchitectLastPath($request);
        $this->get("session")->set('investments', $this->transactInvestmentsAT());
        return $this->render('SpeAppBundle:Template:investments/atarchitect/perfreport.html.twig', array("chart" => $this->atArchitectChart("All")));
    }
    /**
     * @Route("/investments/atarchitect/4/data", name="atArchitect4Data")
     */
    public function atArchitect4DataAction(Request $request)
    {
        $time = $request->request->get("time");
        $timeTranslated = '';
        switch ($time) {
            case 'All': $timeTranslated = 'ITD';
                        break;
            case '1':   $timeTranslated = 'OneYear';
                        break;
            case '3':   $timeTranslated = 'ThreeYear';
                        break;
            case 'YTD': $timeTranslated = 'YTD';
                        break;
        }
        $returnDataAT = $this->get("session")->get("ACInitialize")['WsArchitectSummary']['ATReturn'.$timeTranslated] * 100;
        $returnDataQdia = $this->get("session")->get("ACInitialize")['WsArchitectSummary']['QdiaReturn'.$timeTranslated] * 100;
        return $this->render('SpeAppBundle:Template:investments/atarchitect/chart.html.twig', array("chart" => $this->atArchitectChart($time), 'ATReturn' => $returnDataAT, 'QdiaReturn' => $returnDataQdia));
    }
    /**
     * @Route("/investments/atarchitect/4/saved", name="atArchitect4Saved")
     */
    public function atArchitect4Saved(Request $request)
    {
        return new Response("");
    }
    public function atArchitectChart($time)
    {
        $generalMethods = $this->get('GeneralMethods');
        $graphData = $this->get("session")->get("ACInitialize")['WsArchitectSummaryReturns']['WsArchitectSummaryReturn'];
        if ($generalMethods->isAssocArray($graphData)) { // kicks off if there's only one WsArchitectSummaryReturn in $graphData
            $temp[0] = array();
            foreach ($graphData as $key => $value) {
                $temp[0][$key] = $value;
            }
            $graphData = $temp;
        }
        $chart = array();
        foreach ($graphData as $data)
        {
            $dt = \DateTime::createFromFormat('!m', $data["ReturnMonth"]);
            $month = strtoupper($dt->format('M'));
            $date = $month." ".$data["ReturnYear"];
            $yearCompare = $data["ReturnYear"] + ($time-1);
            $yearcondition = date("Y") <= $yearCompare++;
            if (!$yearcondition && (date("Y") <= $yearCompare))
            {
                $yearcondition = $data["ReturnMonth"] >= date("n");
            }
            if ((date("Y") == $data["ReturnYear"] && $time == "YTD") || $time == "All" || $yearcondition )
            {
                $this->addArchitectChartData($chart,$date,number_format($data['ArchitectMonthReturn'] * 100,2),number_format($data['AgeMonthReturn'] * 100,2));
            }
        }
        return $chart;
    }

    public function addArchitectChartData(&$chart,$label,$ATArchitectScore,$ageScore)
    {
        $chart[] = array("label" => $label,"ATArchitectScore" => $ATArchitectScore, "ageScore" => $ageScore);
    }

    /**
     * @Route("/investments/atarchitect/5", name="atArchitect5")
     */
    public function atArchitect5Action(Request $request)
    {

        return $this->render('SpeAppBundle:Template:investments/atarchitect/optout.html.twig');
    }

    /**
     * @Route("/investments/atarchitect/5/saved", name="atArchitect5Saved")
     */
    public function atArchitect5SavedAction(Request $request)
    {
        $this->updateArchitect(array("acceptedInvestment" => 0));
        $this->setModule('ATArchitect', false);
        $this->setModule('ArchitectIsEnrolled', false);
        $this->get('session')->set('ATArchitectPreviouslyEnrolled', true);
        $this->setModule('ATArchitectPreviouslyEnrolled', true);
        return new Response("");
    }


    /**
     * @Route("/loadingScreen", name="loadingScreen")
     */
    public function loadingScreenAction(Request $request)
    {
        $common = $this->get('session')->get('common');
        $profileid = $this->get("session")->get('pid');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $personalInformation = $repository->findOneBy(array('profileid' => $profileid));
        $common['ATemail'] = $personalInformation->email;
        $this->get('session')->set('common', $common);
        return $this->render('SpeAppBundle:Template:ATBlueprint/at-loading-screen.html.twig',array("loadingTitle" => $request->request->get("loadingTitle")));
    }

    public function updateWsIndividual(&$transData, $personalInformation)
    {
        $array = ['annualCompensation', 'email', 'mobilePhone', 'spouseAnnualCompensation', 'spouseFirstName', 'spouseLastName'];
        foreach ($array as $row) {
            $transData['WsIndividual'][ucfirst($row)] = $personalInformation->$row;
        }
        $transData['WsIndividual']['SpouseIncludeInBlueprint'] = $personalInformation->includeSpousalInformation;

        if(!$this->isNull($personalInformation->spouseDateOfBirth)) {
            $time = strtotime($personalInformation->spouseDateOfBirth);
            $newformat = date('Y-m-d', $time);
            $newformat .= 'T00:00:00';
            $transData['WsIndividual']['SpouseBirthDate'] = $newformat;
        }
    }

    /**
     * @Route("/loadingScreenTransacts", name="loadingScreenTransacts")
     */
    public function loadingScreenTransactsAction(Request $request)
    {
        $profileid = $this->get("session")->get('pid');
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $personalInformation = $repository->findOneBy(array('profileid' => $profileid));
        $transData = $this->getWsBlueprint();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profileid));
        if ($outlook != null) {
            $defString = $outlook->deferralPercent/100.0;
            $transData['WsBlueprint']['NewDefRatePct'] = "$defString";
        }

        $this->updateWsIndividual($transData, $personalInformation);
        $transData['WsAccounts'] = $this->getCurrentATWsAccountsAsXmlArray($profileid);

        $transData['WsIndividual']['OverrideAnnualCompensation'] = 'true';
        $transData['WsIndividual']['OverrideReplacementIncome'] = 'false';
        $transData['WsIndividual']['OverrideRetirementAge'] = 'false';
        $transData['WsIndividual']['OverrideYearsInRetirement'] = 'false';
        $transData['WsIndividual']['OverrideSsRetirementAge'] = 'false';
        $transData['WsIndividual']['OverrideSsBenefit'] = 'false';
        $transData['WsIndividual']['OverrideSsSpousalBenefit'] = 'false';

        $transData['WsBlueprint']['OtherMonthlyIncome'] = '0'; // prevent compounding monthly income

        if (!$this->isNull($personalInformation->monthlyRetirementGoal)) {
            $transData['WsIndividual']['OverrideReplacementIncome'] = 'true';
            $transData['WsIndividual']['ReplacementIncomeNeeded'] = $personalInformation->monthlyRetirementGoal;
        }
        if (!$this->isNull($personalInformation->retirementAge)) {
            $transData['WsIndividual']['OverrideRetirementAge'] = 'true';
            $transData['WsIndividual']['RetirementAge'] = $personalInformation->retirementAge;
        }
        if (!$this->isNull($personalInformation->yearsInRetirement)) {
            $transData['WsIndividual']['OverrideYearsInRetirement'] = 'true';
            $transData['WsIndividual']['YearsInRetirement'] = $personalInformation->yearsInRetirement;
        }
        if (!$this->isNull($personalInformation->socialSecurityBeginAge)) {
            $transData['WsIndividual']['OverrideSsRetirementAge'] = 'true';
            $transData['WsIndividual']['SsRetirementAge'] = $personalInformation->socialSecurityBeginAge;
        }
        if (!$this->isNull($personalInformation->socialSecurityMonthlyAmount)) {
            $transData['WsIndividual']['OverrideSsBenefit'] = 'true';
            $transData['WsIndividual']['SsBenefit'] = $personalInformation->socialSecurityMonthlyAmount;
        }
        if (!$this->isNull($personalInformation->spouseSocialSecurityMonthlyAmount)) {
            $transData['WsIndividual']['OverrideSsSpousalBenefit'] = 'true';
            $transData['WsIndividual']['SsSpousalBenefit'] = $personalInformation->spouseSocialSecurityMonthlyAmount;
        }

        $this->get("session")->set("ACBluePrint",$this->get("spe.app.rkp")->ACBluePrint($transData));
        return new Response('hi');
    }

    /**
     * @Route("/transactDeferral", name="transactDeferral")
     */
    public function transactDeferralAction(Request $request)
    {
        $profileid = $this->get("session")->get('pid');
        $deferral = $request->request->get('percentage')/100.0;
        $transData = $this->getWsBlueprint();
        $transData['WsAccounts'] = $this->getCurrentATWsAccountsAsXmlArray($profileid);
        $transData['WsBlueprint']['NewDefRatePct'] = "$deferral";

        $transData['WsBlueprint']['OtherMonthlyIncome'] = '0'; // prevent compounding monthly income

        $this->get("session")->set("ACBluePrint",$this->get("spe.app.rkp")->ACBluePrint($transData));
        return new Response('hi');
    }

    function isNull($tocheck)
    {
        if ($tocheck == null || $tocheck === '') {
            return true;
        }
        return false;
    }

    function getCurrentATWsAccountsAsXmlArray($profileid, $sorted = false)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATWsAccounts');
        if ($sorted) {
            $ATWsAccounts = $repository->findBy(array('profileid' => $profileid), array('isIncomeAccount' => "ASC"));
        } else {
            $ATWsAccounts = $repository->findBy(array('profileid' => $profileid));
        }
        $ATWsAccountsArray = array();
        foreach($ATWsAccounts as $account)
        {
            $ATWsAccountsArray[]  = $account->getXmlArray();
        }
        return $ATWsAccountsArray;
    }
    public function ATBlueprintPersonalInformationFirstNameAction()
    {
        $profileid = $this->get("session")->get("pid");

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');

        $personalInfo = $repository->findOneBy(array('profileid' => $profileid));
        if ($personalInfo != null)
        {
            return new Response($personalInfo->firstName);
        }
        else
        {
            return new Response($this->get("session")->get("rkp")["plan_data"]->firstName);
        }
    }
    private function calculateInvestmentsGraphTable()
    {
        $graphValues = $this->get("session")->get("PlansInvestmentsConfigurationFundsGroupValuesColorsHash")[$this->get("session")->get("PlansInvestmentsGraphSelectedMinScore")];
        $existingScores = $this->get("session")->get("graphScores");
        $table = array();
        $FundsGroupsValuesHash = $this->get("session")->get("FundsGroupsValuesHash");
        $plansFundsHash = array();
        foreach ($this->get("session")->get("plansFunds") as $fund)
        {
            $fund['percent'] = "";
            $plansFundsHash[$fund['groupValueId']][] = $fund;
        }        
        foreach ($graphValues as $value)
        {
            $FundsGroupsValue = $FundsGroupsValuesHash[$value['fundGroupValueId']];
            $row['header'] = array("description" => $FundsGroupsValue['value'],"id" =>$FundsGroupsValue['id']); 
            $row['funds'] = $plansFundsHash[$value['fundGroupValueId']];
            $row['color'] = $value;
            $row['FundsGroupsValue'] = $FundsGroupsValue;
            $row['total'] = 0;
            foreach ($existingScores['groups'] as $group)
            {
                if ($value['fundGroupValueId'] == $group['id'])
                {
                    $groupFunds = $group['funds'];
                    $groupFundsCounter = 0;
                    foreach ($row['funds'] as &$fund)
                    {
                        $fund['percent'] = $groupFunds[$groupFundsCounter++]['percent'];
                    }
                    $row['total'] = $group['total'];
                }                 
            }
            $table[] = $row;
        }
        $data = array("graphValues"  => $graphValues,"table" => $table);
        $this->get("session")->set("investmentsGraphData",$data);
        return $data;
    }

    public function transactInvestmentsAT()
    {
        $blueprint = $this->getWsBlueprint();

        $investment = array();
        $investment['I_SelectedInvestmentOption'] = "ATCUSTOM";
        $investment['I_SelectedInvestmentDetails'] = $blueprint['WsArchitectTask']['NewMoneyToFundId'] . "^" . $blueprint['WsArchitectTask']['NewMoneyPct'];
        $investment['I_RealignSelectedInvestmentDetails'] = $blueprint['WsArchitectTask']['CurrentBalanceToFundId'] . "^" . $blueprint['WsArchitectTask']['CurrentBalancePct'];

        $this->get('session')->set("investmentSection", 'ATCUSTOM');
        $fundTemp = $this->getFunds();
        $funds[$blueprint['WsArchitectTask']['NewMoneyToFundId']] = $fundTemp[$blueprint['WsArchitectTask']['NewMoneyToFundId']];
        $funds[$blueprint['WsArchitectTask']['NewMoneyToFundId']]['id'] = $blueprint['WsArchitectTask']['NewMoneyToFundId'];
        $funds[$blueprint['WsArchitectTask']['NewMoneyToFundId']]['percent'] = $blueprint['WsArchitectTask']['NewMoneyPct'];
        $this->get("session")->set("savedFunds",$funds);

        return $investment;
    }

    public function getFunds() // remove later
    {
        $rkpFunds= $this->get("session")->get('rkp')['plan_data']->funds;
        $funds = array();
        foreach ($rkpFunds as $fund)
        {
            $rkpFund = array();
            $rkpFund['riskDescription'] = "";
            $rkpFund['step'] = false;
            $fields = array("ticker","id","name","currentElection","pendingElection","customUrl","prospectUrl","risk","assetClass","decimalScale","redemptionFeeMessage","performanceUrl","fundGroup","display","adviceId");
            $i = 0;
            foreach ($fields as $field)
            {
                if (isset($fund[$i]) && $field != "fundGroup")
                {
                    $rkpFund[$field] = $fund[$i];
                }
                else if (isset($fund[$i]) && $field == "fundGroup")
                {
                    $rkpFund[$field] = array();
                    $rkpFund[$field]['id'] = $fund[$i][0];
                    $rkpFund[$field]['name'] = $fund[$i][1];
                }
                else if (!isset($fund[$i]) && $field == "display" )
                {
                    $rkpFund[$field] = true;
                }
                else
                {
                    $rkpFund[$field] = null;
                }
                if (isset($fund[$i]) &&  $fund[$i] !== null  && $field == "risk")
                {
                    $rkpFund['riskDescription'] = $fund[$i];
                    if (!empty($fund[$i]))
                        $this->get("session")->set('showRiskColumn', true);
                }
                if (isset($fund[$i]) &&  $fund[$i] !== null  && $field == "assetClass")
                {
                    if (!empty($fund[$i]))
                        $this->get("session")->set('showAssetClassColumn', true);
                }
                if (isset($fund[$i]) &&  $fund[$i] !== null  && $field == "decimalScale")
                {
                    $rkpFund['step'] = 1;
                    for ($j = 0; $j < (int)$fund[$i]; $j++)
                    {
                        $rkpFund['step'] /= 10;
                    }
                }
                $i++;
            }
            if ($rkpFund['step'] == 1)
            {
                $rkpFund['currentElection'] = (int)$rkpFund['currentElection'];
            }
            $funds[$rkpFund["id"]] = $rkpFund;
        }
        return $funds;
    }

    /**
     * @Route("/commonData", name="commonData")
     */
    public function commonDataAction(Request $request)
    {
        $attr = $request->request->get('attr');
        return new JsonResponse($this->get("session")->get($attr));
    }

    /**
     * @Route("/getXmlRaw")
     */
    public function getXmlRawAction(Request $request) {
        $session = $this->get("session");
        $xmlUid = isset($session->get("rkp")["xmlUid"]) ? $session->get("rkp")["xmlUid"] : null;
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery("SELECT w.ip FROM classesclassBundle:WhitelistedIps w");
		$whitelistedIps = array_column($query->getScalarResult(), 'ip');

        if ($xmlUid && in_array($request->getClientIp(), $whitelistedIps)) {
            $sql = "
                SELECT
                    *
                FROM
                    xmlRaw
                WHERE
                    xmlUid = :xmlUid
                ORDER BY 
                    timestamp ASC
            ";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute(array(':xmlUid' => $xmlUid));
            $xmlRawData = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            $xmlRawData = array();
        }
        $response = new  Response(json_encode($xmlRawData));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/saveRolloverOption")
     */
    public function saveRolloverOptionAction(Request $request) {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $profileId = $session->get('profileId');
        $profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));

        if ($request->request->has("emailMe")) {
            $profiles->emailMe = $request->request->get("emailMe");
            $profiles->callMe = null;
            $profiles->callUs = null;
        }
        else if ($request->request->has("callMe")) {
            $profiles->callMe = $request->request->get("callMe");
            $profiles->emailMe = null;
            $profiles->callUs = null;
        }
        else {
            $profiles->callUs = 1;
            $profiles->emailMe = null;
            $profiles->callMe = null;
        }
        $em->flush();
        return $this->toJSON(array('status' => 1));
    }

	/**
	 * @Route("/incrementContactViewCount")
	 */
	public function incrementContactViewCount(Request $request) {
		$session = $request->getSession();
		$em = $this->getDoctrine()->getManager();
		$profileId = $session->get('profileId');
		$profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
		$profiles->enrollmentContactViewedTimes++;
		$em->flush();
		return $this->toJSON(array('status' => 1));
	}
    /**
     * @Route("/contributions/AutoIncrease", name="contributionsAutoIncrease")
     */
    public function contributionsAutoIncreaseAction(Request $request)
    {
        $autoIncreaseSave = $this->get("session")->get("autoIncreaseSave");
        if ($autoIncreaseSave == null)
        {
            $autoIncreaseSave = array();
            $autoIncreaseSave['mode'] = "";
            $autoIncreaseSave['deferrals'] = [];
        }
        $savedDeferrals = $autoIncreaseSave['deferrals'];
        $autoIncrease = $this->get("session")->get("autoIncrease");
        $savedDeferralsHash = array();
        foreach ($savedDeferrals as $deferral)
        {
            $savedDeferralsHash[$deferral['source']] = $deferral;
        }
        foreach ($autoIncrease['data'] as $key => &$data)
        {
            if (isset($savedDeferralsHash[$data['source']]))
            {
                $deferralEndingKey = "pct";
                if ($autoIncreaseSave['mode'] == "dollar")
                {
                    $deferralEndingKey = "amount";
                }
                $data['autoincrease'.$deferralEndingKey] = (float)$savedDeferralsHash[$data['source']]['value'];
                $data[$autoIncreaseSave['mode']] = true;
                $data['autoincreasestart'] = $savedDeferralsHash[$data['source']]['date'];
            }
        }
        $this->get("session")->set("autoIncreaseProcessed",$autoIncrease);
        $params['autoIncrease'] =$autoIncrease;
        $params['autoIncreaseSave'] = $autoIncreaseSave;
        $params['common'] = $this->get("session")->get("common");
        $em = $this->getDoctrine()->getManager();
        $databaseContributions = $em->getRepository('classesclassBundle:profilesContributions')->findOneBy(array("profileid" => $this->get("session")->get("pid") ));
        $sessionContributions = $this->get("session")->get("contributions");
        $contributions['database'] = $databaseContributions;
        $contributions['session'] = $sessionContributions;
        $params['contributions'] = $contributions;
        return $this->render('SpeAppBundle:Template:contributions/autoincrease/index.html.twig',$params);
    }
    /**
     * @Route("/contributions/AutoIncreaseSaved", name="contributionsAutoIncreaseSaved")
     */
    public function contributionsAutoIncreaseSavedAction(Request $request)
    {
        $autoIncrease = $request->request->get("autoIncrease");
        foreach($autoIncrease['deferrals'] as &$deferral)
        {
            foreach ($this->get("session")->get("autoIncreaseProcessed")['data'] as $deferral2)
            {
                if ($deferral['source'] == $deferral2['source'])
                {
                    $deferral['rkp'] = $deferral2;
                }
            }
        }
        $this->get("session")->set("autoIncreaseSave",$autoIncrease);
        $this->updateProfile("autoIncrease");
        return new Response("");
    }
    /**
     * @Route("/addProfileTransaction", name="addProfileTransaction")
     */
    public function addProfileTransactionAction(Request $request)
    {
        $this->get("ParticipantsTransactionsService")->add($request->request->all());
        return new Response("saved");
    }
    
    /**
	 * @Route("/saveTrustedContact")
	 */
	public function saveTrustedContactAction(Request $request) {
		
		$session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
		$profileId = $session->get('profileId');
		$profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
        $profiles->trustedContactStatus = $request->get("trustedContactOptIn") ? 1 : 0;
        $session->set("trustedContactOptIn", $request->get("trustedContactOptIn"));
        $response = $this->get('spe.app.rkp')->postTrustedContact();
        $data = json_decode($response, true);
        if (!$data['success'])
        {
            $profiles->trustedContactStatus = 2;
        }
        $em->flush();
        return $this->toJSON($data);
	}
        /**
         * @Route("/retirementneeds/stadionResult", name="stadionResult")
         */
        public function stadionResultAction(Request $request)
        {   
            $em = $this->getDoctrine()->getManager();
            $session = $request->getSession();
            $response = $session->get("stadionQuestionnaireResponse");
            $type = "standard";
            if ($session->get('rkp')['plan_data']->rkpExtras->stadionManagedAccountId == 1)
                $type = "advisor";
            if (!empty($response['success']))
            {
                $data = $response['data'];  
                $params['riskType'] = $session->get('risk_profile')['stadionLabels'][$data['pathId']];
                $params['graph'] = $em->getRepository('classesclassBundle:StadionGraph')->findOneBy(["pathId" => $data['pathId'],"type" => $type]);
                return $this->render('SpeAppBundle:Template:stadionresult.html.twig',$params);
            }
            return new Response("");
        }    
        /**
         * @Route("/retirementneeds/stadionAgreement", name="stadionAgreement")
         */
        public function stadionAgreementAction()
        {
            $percentage = "0.35";
            $qdialink = "qdia_link_35";
            $flyerlink = "flyer_link_35";
            $agreementText = "aggreement_text";
            $session = $this->get('session');
            $managedaccountid = $session->get('rkp')['plan_data']->rkpExtras->stadionManagedAccountId;
            if ($managedaccountid == 2) {
                $percentage = "0.55";
                $qdialink = "qdia_link_55";
                $flyerlink = "flyer_link_55";
                $agreementText = "aggreement_text19";
            }
            if ($managedaccountid == 1)
            {
                $percentage = "0.35";
                $qdialink = "qdia_link_managedaccountid_1";
                $flyerlink = null;
                $agreementText = "aggreement_text18";
            }
            return $this->render('SpeAppBundle:Template:stadionagreement.html.twig', array('percentage' => $percentage, 'qdialink' => $qdialink, 'flyerlink' => $flyerlink,"agreementText" => $agreementText));
        }     
         


    /**
     * @Route("/saveSession", name="saveSession")
     */
    public function saveSessionAction(Request $request)
    {
        $yearlyIncome = $request->get('yearlyIncome');
        $getPaid = $request->get('getPaid');

        $contributions = $this->get("session")->get("contributions");
        $retirementNeeds = $this->get("session")->get("retirement_needs");

        $contributions['C_CurrentYearlyIncome'] = $yearlyIncome;
        $retirementNeeds['RN_CurrentYearlyIncome'] = $yearlyIncome;
        $contributions['C_GetPaid'] = $getPaid;

        $this->get("session")->set("contributions",$contributions);
        $this->get("session")->set("retirement_needs",$retirementNeeds);
        if ($request->get("accepted") !== null)
        {
            $this->autosAccept($request->get("accepted"));
        }
        return new Response('saved');
    }
    private function autosAccept($accepted)
    {
        $autoTables = ["AutoEnroll","AutoEscalate"];
        $planAutoTables = [];
        $planid = $this->get("session")->get("plan")['id'];
        $autosService = $this->get("AutosService");
        $autosService->set(["planid" => $planid]);
        $autosServiceProfile =  clone $autosService;
        $autosServiceProfile->set(["profileid" => $this->get("session")->get('pid')]);
        foreach ($autoTables as $autoTable)
        {
            $planData = $autosService->getData($autoTable);
            if ($accepted)
            {
                $autosServiceProfile->addData($autoTable,$planData);  
            }
            else
            {
                $autosServiceProfile->removeData($autoTable);  
            }
        } 
        $this->get("session")->set("autosAccepted",$accepted);
    }
    /**
     * @Route("/contributions/Autos", name="contributionsAutos")
     */
    public function contributionsAutosAction(Request $request)
    {
        $contributionSections = $this->autosContributionsSections();
        $retirementNeeds = $this->get("session")->get('retirement_needs');
        $contribution = $this->get("session")->get('contributions');
        $common = $this->get("session")->get("common");
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:Apr');
        $apr = $repository->findOneBy(array('age' => $common['retireAge'], 'year' => date('Y')));
        return $this->render('SpeAppBundle:Template:contributions/autos/index.html.twig',["retirementneeds" => $retirementNeeds, "data" => $this->get("session")->all(),"sections" => $contributionSections,"sectionLength" => 12/count($contributionSections),"contribution" => $contribution, "graphViewMode" => $request->get("graphViewMode"), "common" => $common, "apr" => $apr ]);
    }
    public function autosContributionsSections()
    {
        $translator = $this->get('translator')->getMessages();
        $contribution = $this->get("session")->get('contributions');
        $common = $this->get("session")->get("common");
        $contributionSections = [];      
        $autosService = $this->get("AutosService");
        $autosService->set(["profileid" => $this->get("session")->get('pid')]);
        $autoEnroll = $autosService->getData("AutoEnroll");
        $autoEnrollEnabled = !empty($autoEnroll->enabled);
        $this->get("session")->set("autoEnrollEnabled",$autoEnrollEnabled);
        if ($this->get("session")->get("common")['pre'])
        {
            $contributionSections[] = 
            [
                "name" => "pre",
                "saveFieldPercent" => "C_PreTaxContributionPct",
                "saveFieldDollar" => "C_PreTaxContributionValue"
            ];                
        }
        if ($this->get("session")->get("common")['roth'])
        {
            $contributionSections[] = 
            [
                "name" => "roth",
                "saveFieldPercent" => "C_RothContributionPct",
                "saveFieldDollar" => "C_RothContributionValue"
            ];                
        }
        if ($this->get("session")->get("common")['post'])
        {
            $contributionSections[] = 
            [
                "name" => "post",
                "saveFieldPercent" => "C_PostTaxContributionPct",
                "saveFieldDollar" => "C_PostTaxContributionValue"
            ];                
        }    
        foreach ($contributionSections as &$section)
        {
            $section['title'] = $translator['autos'][$section['name']];
            $section['minPercent'] = $common[$section['name']."Min"];
            $section['maxPercent'] = $common[$section['name']."Max"];
            $section['stepPercent'] = $common[$section['name']."PctPre"];
            $section['stepDollar'] = $common[$section['name']."DlrPre"];
            $section['percent'] = $common[$section['name']."Current"];
            $section['dollar'] = 0;
            if ($section['percent'] < $section['minPercent'])
            {
                $section['percent'] = $section['minPercent'];
            }
            if (!empty($contribution['contribution_mode']))
            {
                if ($contribution['contribution_mode'] == "DEFERPCT" && $contribution[$section['saveFieldPercent']] !== null)
                {
                    $section['percent'] = $contribution[$section['saveFieldPercent']];
                }
                else if($contribution['contribution_mode'] == "DEFERDLR" && $contribution[$section['saveFieldDollar']] !== null)
                {
                    $section['dollar'] = $contribution[$section['saveFieldDollar']];
                }
            }
            if ($autoEnrollEnabled)
            {
                $section[$autoEnroll->type] = $autoEnroll->{$section['name']};
                $contribution['contribution_mode'] = $autoEnroll->type == "percent" ? "DEFERPCT":"DEFERDLR";
                $this->get("session")->set("contributions",$contribution);
            }
        }                                    
        return $contributionSections;
    }
    /**
     * @Route("/contributions/Autos/saved", name="contributionsAutosSaved")
     */
    public function contributionsAutosSavedAction(Request $request)
    {
        $deferrals = $request->request->get("deferrals");

        $contribution = $this->get("session")->get('contributions');  
        $sections = $this->autosContributionsSections();
        foreach ($sections as $section)
        {
            $contribution[$section['saveFieldPercent']]  = null;
            $contribution[$section['saveFieldDollar']]  = null;
        }
        foreach ($deferrals as $deferral)
        {
            $contribution[$deferral['saveField']] = (float)$deferral['value'];
        }
        $contribution['contribution_mode'] = $request->request->get("mode");
        $contribution['contributionsStatus'] = 1;
        $this->get("session")->set("contributions",$contribution);
        $this->updateProfile('contributions');

        $autoservice = $this->get("AutosService");
        $autoservice->set(["userid" => $this->get("session")->get('plan')['userid'], "planid" => $this->get("session")->get('plan')['id']]);
        $autoenroll = $autoservice->getData('AutoEnroll');
        if ($autoenroll->defaultFund) {
            $this->saveInvestmentsAutos();
        }
        return new Response("");
    }
    /**
     * @Route("/contributions/Autos/switch", name="contributionsAutosSwitch")
     */
    public function contributionsAutosSwitchAction(Request $request)
    {
        $sessionContributions = $this->get("session")->get("contributions");
        $contributions = new profilesContributions();
        $contributions->paidFrequency = $sessionContributions['C_GetPaid'];
        $contributions->currentYearlyIncome = $sessionContributions['C_CurrentYearlyIncome'];
        $requestAll = $request->request->all();
        foreach ($requestAll as  $key => $field)
        {
            $contributions->$key = $field;
        }
        $this->get("spe.app.profile")->calculateContributions($contributions);
        return new JsonResponse($contributions);
    }
    private function contributionsAutosdollarMinOrMax($deferrals,$tag)
    {
        $contributions = new profilesContributions();
        $sessionContributions = $this->get("session")->get("contributions");
        $contributions->paidFrequency = $sessionContributions['C_GetPaid'];
        $contributions->currentYearlyIncome = $sessionContributions['C_CurrentYearlyIncome'];
        $contributions->mode = "PCT";
        foreach ($deferrals as $deferral)
        {
            $field = $deferral['deferralLimits']['name'];
            $contributions->$field = $deferral['deferralLimits'][$tag];
        }    
        $this->get("spe.app.profile")->calculateContributions($contributions);
        return $contributions;
    }
    /**
     * @Route("/contributions/Autos/dollarMinAndMax", name="contributionsAutosdollarMinAndMax")
     */
    public function contributionsAutosdollarMinAndMaxAction(Request $request)
    {
        $deferrals = $request->request->get("deferrals");
        $minContributions = $this->contributionsAutosdollarMinOrMax($deferrals,"min");
        $maxContributions  = $this->contributionsAutosdollarMinOrMax($deferrals,"max");
        return new JsonResponse
        ([
            "min" => $minContributions,
            "max" =>  $maxContributions
        ]);
    }    
    /**
     * @Route("/contributions/Autos/combinedMax", name="contributionsAutosCombinedMax")
     */
    public function contributionsAutosCombinedMinAndMaxAction(Request $request)
    {
        $sessionContributions = $this->get("session")->get("contributions");
        $max = $this->get("session")->get("module")['combinedContributionAmount'];
        if (empty($max))
        {
            $max = max([$this->get("session")->get("common")['preMax'],$this->get("session")->get("common")['rothMax'],$this->get("session")->get("common")['postMax']]);
        }
        $min = min([$this->get("session")->get("common")['preMin'],$this->get("session")->get("common")['rothMin'],$this->get("session")->get("common")['postMin']]);
        if ($request->request->get("mode") == "DEFERDLR")
        {
            $contributions = new profilesContributions();
            $contributions->paidFrequency = $sessionContributions['C_GetPaid'];
            $contributions->currentYearlyIncome = $sessionContributions['C_CurrentYearlyIncome'];
            $contributions->mode = "PCT";
            $contributions->pre = $min;
            $this->get("spe.app.profile")->calculateContributions($contributions);
            $min = $contributions->preDollarPerPayCheck;
            $contributions->pre = $max;
            $this->get("spe.app.profile")->calculateContributions($contributions);
            $max = $contributions->preDollarPerPayCheck;
        }
        return new JsonResponse
        ([
            "min" => $min,
            "max" => $max
        ]);
    } 
    /**
     * @Route("/contributions/Autos/accept", name="contributionsAutosAccept")
     */
    public function contributionsAutosAcceptAction(Request $request)
    {
        $this->autosAccept(1);
        return new Response("");
    }
    /**
     * @Route("/contributions/Autos/optout", name="contributionsAutosOptout")
     */
    public function contributionsAutosOptoutAction(Request $request)
    {
        $this->autosAccept(0);
        return new Response("");
    }

    public function saveInvestmentsAutos()
    {
        $session = $this->get("session");
        $em = $this->get('doctrine')->getManager();
        $autosService = $this->get("AutosService");
        $autosService->set(["planid" => $this->get("session")->get('plan')['id']]);
        $autoEnroll = $autosService->getData("AutoEnroll");

        $profileService = $this->get('spe.app.profile');
        $profile = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('id' => $session->get('pid')));
        $investment = array();
        $investment['isAutos'] = true;
        $investment['I_SelectedInvestmentOption'] = "CUSTOM";
        $investment['I_SelectedInvestmentDetails'] = "{$autoEnroll->defaultFundTicker}^100";
        $this->get("session")->set('investments', $investment);
        $profileService->addInvestments($profile);
    }

    /**
     * @Route("/contributions/Autos/autoIncrease", name="contributionsAutosAutoIncrease")
     */
    public function contributionsAutosAutoIncreaseAction(Request $request)
    {
        $howOftenValues = ["monthly", "quarterly", "semi-annual", "annual"];
        $howOften  = [];
        foreach ($howOftenValues as $value)
        {
            $howOftenEntry = [];
            $howOftenEntry['value']  = $value;
            $howOften[] = $howOftenEntry;
        }
        $sections = $this->autosContributionsSections();
        $contribution = $this->get("session")->get('contributions');
        $autosService = $this->get("AutosService");
        $autosService->set(["planid" => $this->get("session")->get("plan")['id']]);
        $autosServiceProfile =  clone $autosService;
        $autosServiceProfile->set(["profileid" => $this->get("session")->get('pid')]);
        $data = $autosServiceProfile->getData("AutoIncrease2");
        if (empty($data))
        {
            $data = $autosService->getData("AutoIncrease");
            $data->enabled = 0;
            $now = new \DateTime();
            if (empty($data->startDate) || $now > $data->startDate)
            {
                $data->startDate = $now;
            }           
        }
        else
        {
            if ($contribution['contribution_mode'] != "DEFER".$data->type)
            {
                $data->pre = $data->roth = $data->post = 0;
            }
        }
        $data->step = 1;
        if ($contribution['contribution_mode'] == "DEFERDLR" && $data->amountDecimalPlaces >= 0 && $data->amountDecimalPlaces <= 2)
        {
           while($data->amountDecimalPlaces > 0)
           {
               $data->step = $data->step * .1;
               $data->amountDecimalPlaces--;
           }
        }
        $limit = $contribution['contribution_mode'] == "DEFERPCT" ? $data->percentLimit:$data->amountLimit;
        $common = $this->get("session")->get('common');
        return $this->render('SpeAppBundle:Template:contributions/autos/autoincrease.html.twig',["howOften" => $howOften,"sections" => $sections,"sectionLength" => 12/count($sections),"contribution" => $contribution,"limit" => $limit,"autoIncrease" => $data, "common" => $common, "graphViewMode" => $request->get("graphViewMode")]);
    }
    /**
     * @Route("/contributions/Autos/autoIncrease/saved", name="contributionsAutosAutoIncreaseSaved")
     */
    public function contributionsAutosAutoIncreaseSavedAction(Request $request)
    {
        $autosService = $this->get("AutosService");
        $autosService->set(["planid" => $this->get("session")->get("plan")['id']]);
        $autosServiceProfile =  clone $autosService;
        $autosServiceProfile->set(["profileid" => $this->get("session")->get('pid')]);
        $planData = $autosService->getData("AutoIncrease");
        if ($request->request->get("enabled"))
        {
            $autosServiceProfile->addData("AutoIncrease2",$planData);
            $autosServiceProfile->addData("AutoIncrease2",$request->request->all());
        }
        else
        {
            $autosServiceProfile->removeData("AutoIncrease2");
        }
        return new Response("");
    }
    /**
     * @Route("/getReportingParameters", name="contributionsAutosAutoIncreaseSaved")
     */
    public function getReportingParametersAction(Request $request)
    {
        $data = [];
        $data['profileId'] = $this->get("session")->get("profileId");
        $data['userid'] = $this->get("session")->get("plan")['userid'];
        $data['partnerid'] = $this->get("session")->get("plan")['partnerid'];
        $data['planid'] = $this->get("session")->get("plan")['planid'];
        $data['smartplanid'] = $this->get("session")->get("plan")['id'];
        try
        {
            $data['serverTag']  = $this->getParameter("serverTag",$request->getHost());           
        } catch (\Exception $ex) {
            $data['serverTag'] = $request->getHost();
        }
        return new JsonResponse($data);
    }

    /**
     * @Route("/retireExpress", name="retireExpress")
     */
    public function retireExpressAction(Request $request)
    {
        $session = $this->get("session");
        $response = $this->container->get('spe.app.rkp')->postStadionQuestionnaire(null);
        $session->set("stadionQuestionnaireResponse",$response);
        $session->set("stadionQuestionnaireAnswers",null);
        $this->get('spe.app.smart')->setStadionManagedAccountData(true);

        $this->updateProfile('riskProfile');
        return $this->forward('Spe\AppBundle\Controller\InvestmentsController::stadionAgreementSavedAction');
    }
}
