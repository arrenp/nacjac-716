<?php

namespace Spe\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use classes\classBundle\Entity\profilesInvestments;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="loading")
     * @Template("SpeAppBundle:Layout:loading.html.twig")
     */
    public function loadingAction(Request $request)
    {
        return $this->loading($request);
    }
    
    public function loadingJsonAction(Request $request) 
    {
        return new JsonResponse($this->loading($request));
    }
    
    protected function loading(Request $request) {
        $counter = 0;
        $countRequest = count($_REQUEST);
        $newArray = array();
        if ($countRequest > 0)
        {
            foreach ($_REQUEST as $key => $value)
            {
                
                $newArray[$counter] = $key;
                $counter++;
            }

            $newArrayCount = count($newArray);
            $requestLastKey = $newArray[$newArrayCount-1];
            $_REQUEST[$requestLastKey] = rtrim($_REQUEST[$requestLastKey],'/');
        }
        
        $session = $this->get("session");
        $session->clear();
        // Set Environment
        $session->set('environment', $this->container->getParameter('kernel.environment'));

        if(isset($_SERVER['HTTP_REFERER'])){
            extract(parse_url($_SERVER['HTTP_REFERER']));
            $session->set('referer', $scheme .'://'. $host);
        }
        if (!isset($_REQUEST['id']) && isset($_REQUEST['planid']))
        $_REQUEST['id'] = $_REQUEST['planid'];
        if (!isset($_REQUEST['id']) && isset($_REQUEST['plan_id']))
        $_REQUEST['id'] = $_REQUEST['plan_id'];        
        
        $session->set('REQUEST', $_REQUEST);
        $host = explode("?",$request->getUri())[0]."?";
        $hostcounter = 0;
        foreach ($_REQUEST as $key => $value)
        {
            if ($hostcounter > 0)
            $host = $host."&";
            $host = $host.$key."=".$value;
            $hostcounter++;
        }
        $session->set('originalUrl',$host);

        //todo: replace this with proper redirect condition and parameters

        $appVersion = $this->getDoctrine()->getRepository('classesclassBundle:plans')->findOneBy(array('planid' => $_REQUEST['id'], 'partnerid' => $_REQUEST['partner_id']))->appVersion;
        if ($appVersion === 2) {
            $params = $request->query->all();
            $params['productCode'] = 'SP';
            $params['app_domains'] = $request->getHost();
            return $this->redirectToRoute('vue_index', $params);
        }
        return array("request" => $_REQUEST);
    }

    /**
     * @Route("/style", name="style")
     */
    public function styleAction()
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/css');
        return $this->render('SpeAppBundle:Layout:style.css.php', array(), $response);
    }

    /**
     * @Route("/pdf/{filename}", name="pdf")
     */
    public function pdfAction($filename = null){
        if($filename){
            $path = $this->get("session")->get('translation_path');
            return new BinaryFileResponse($path.'pdf/'.$filename.'.pdf');
        }
    }
    
    /**
     * @Route("/profile/report/{profileId}", name="profileReport")
     */
    public function profileReportAction($profileId = null,Request $request)
    {
        $report = $this->get("spe.app.profile")->getProfileReport($profileId,$request->query->get("fromAdmin",0),$request->query->get("ajax",0));
        if($report['investments']['type'] == 'at') {
            $repo = $this->getDoctrine()->getRepository('classesclassBundle:ProfilesATInitialize');
            $ACInitialize = $repo->findOneBy(array('profileid' => $profileId));
            $repo = $this->getDoctrine()->getRepository('classesclassBundle:ProfilesATBlueprint');
            $ACBlueprint = $repo->findOneBy(array('profileid' => $profileId));

            $report['investments']['ATArchitect'] = $this->ATArchitect($report['investments']['profile'], $ACInitialize, $ACBlueprint);
            $report['investments']['ATBlueprint'] = $this->ATBlueprint($report['investments']['profile'], $ACInitialize, $ACBlueprint);
        }
        return $this->render("SpeAppBundle:Template:{$report['template']}",$report);
    }
    public function ATArchitect($profile, $ACInitialize, $ACBlueprint)
    {
        if ($ACBlueprint != null) {
            $ATArchitect = (array)json_decode($ACBlueprint->wsArchitectTask);
        } else {
            $ATArchitect = (array)json_decode($ACInitialize->wsArchitectTask);
        };

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATArchitect');
        $architect = $repository->findOneBy(array('profileid' => $profile->id, 'userid' => $profile->userid, 'planid' => $profile->planid));

        if ($ATArchitect == null || ($architect != null && !$architect->acceptedInvestment) || json_decode($ACInitialize->wsPlan)->AllowArchitect === 'false' || json_decode($ACInitialize->wsIndividual)->ArchitectIsEnrolled === 'false') {
            return new Response("");
        } else {
            $session = $this->get('session');
            $rkp = $session->get('rkp');
            $reliusFund = null;
            foreach ($rkp['plan_data']->funds as $fund) {
                if ($fund[3] > 0) {
                    $reliusFund = $fund[1];
                    break;
                }
            }
            if (explode('^', $session->get('investments')['I_SelectedInvestmentDetails'])[0] !== $reliusFund) {
                $common = $session->get('common');
                $common['updateElections'] = true;
                $common['updateRealignment'] = true;
                $session->set('common', $common);
            }
            return $this->render("SpeAppBundle:Template:atarchitect.html.twig",array("ATArchitect" => $ATArchitect))->getContent();
        }
    }
    public function ATBlueprint($profile, $ACInitialize, $ACBlueprint)
    {
        if ($ACBlueprint != null) {
            $individual = (array)json_decode($ACBlueprint->wsIndividual);
            $blueprint = (array)json_decode($ACBlueprint->wsBlueprint);
        } else {
            $individual = (array)json_decode($ACInitialize->wsIndividual);
            $blueprint = (array)json_decode($ACInitialize->wsBlueprint);
        }
        
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
        $personalInfo = $repository->findOneBy(array('profileid' => $profile->id, 'userid' => $profile->userid, 'planid' => $profile->planid));

        $repository = $this->getDoctrine()->getRepository('classesclassBundle:ATBlueprintOutlook');
        $outlook = $repository->findOneBy(array('profileid' => $profile->id, 'userid' => $profile->userid, 'planid' => $profile->planid));
        
        if ($personalInfo == null || $outlook == null)
        {
            return new Response("");
        }
        return $this->render('SpeAppBundle:Template:ATBlueprint/atblueprint.html.twig', array('personalInfo' => $personalInfo, 'outlook' => $outlook,'individual' => $individual, 'blueprint' => $blueprint,"profile" => $profile))->getContent();
    }
    /**
     * @param $profileId
     * @return Response
     * @Route("/profile/graphReport", name="graphReport")
     */    
    public function graphReportAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:profiles');
        $profile = $repository->findOneBy(array('id' => $request->request->get("id")));   
        return $this->render('SpeAppBundle:Template:investments/graph/profilegraph.html.twig',(array)json_decode($profile->sessionData)->investmentsGraphData);
    }
    
    function autoEscape(&$data)
    {
        foreach ($data as $key=> &$row)
        {
            if (is_array($row))
            $this->autoEscape($data[$key]);
            else
            $data[$key] = $this->autoEscapeField ($row);
        }
    }
    function autoEscapeField($row)
    {
        return htmlspecialchars($row, ENT_QUOTES, 'ini_get("default_charset")', false);
    }


    protected  function toJSON($data){
        return new Response(json_encode($data));
    }
}
