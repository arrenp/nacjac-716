<?php
namespace Spe\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * Ajax controller.
 * @Route("/", name="investments", condition="request.headers.get('X-Requested-With') == 'XMLHttpRequest'")
 */
class InvestmentsController extends Controller
{
    /**
     * @Route("/investments", name="investments")
     */
    public function investmentsAction(Request $request)
    {
        $session = $this->get("session");
        $modules = $session->get("module");
        $planData = $session->get('rkp')['plan_data'];
        if ($modules['ATArchitect'])
        {
            return $this->atBlueprintMainAction($request, "ArchitectLastPath");
        }
        $filteredSections = $this->get("spe.app.investments")->getSections();
        $params['sections'] = $filteredSections;
        $params['audio'] = "investments_0600aNI";
        $params['planData'] = $planData;
        $this->get("session")->set("investmentSectionUrl",$request->getUri());
        $smart401k = $this->get("session")->get('smart401k_completed');
        if ($smart401k)
        {
            $params = array();
            $params['audio'] = "investments_0602_SMA_VO";
            $params['funds'] =  $this->adviceFunds();
            $params['fundsHeader'] = $this->fundsHeader();
            $params['advice'] = true;
            $this->setInvestmentsSection("ADVICE");
            return $this->render('SpeAppBundle:Template:investments/general/investmentselector.html.twig',$params);            
        }
        return $this->render('SpeAppBundle:Template:investments.html.twig',$params);
    }  
    public function adviceFunds()
    {
        $funds = $this->getFunds();
        $adviceFunds = $this->get("session")->get("sessionAdviceFunds");
        foreach ($funds as &$fund)
        {
            $fund['currentElection'] = $adviceFunds[$fund['adviceId']]['allocation'] * 100;
        }
        return $funds;
    }
    public function getFunds()
    {
        return $this->get("spe.app.investments")->getFunds();
    }
    public function getTargetDateFunds()
    {
        return $this->get("spe.app.investments")->getTargetDateFunds();
    }
    public function getPortfolios()
    {
        return $this->get("spe.app.investments")->getPortfolios();
    }
    /**
     * @Route("/investments/qdia", name="investmentsQdia")
     */
    public function investmentsQdiaAction(Request $request)
    {
        $this->setInvestmentsSection("DEFAULT");
        $session = $request->getSession();
        $repository = $this->getDoctrine()->getRepository('classesclassBundle:plansFunds');
        $fund = $repository->findOneBy(['planid' => $session->get('spe_plan_id'), 'qdiaDefault' => 1]);
        $funds = [];
        $funds[0]['name'] = $fund->name;
        $funds[0]['customUrl'] = $fund->link;
        $funds[0]['percent'] = 100;
        $params['funds'] =  $funds;
        $params['fundsHeader'] = $this->fundsHeaderConfirm();
        $params['translation']['title'] = "QDIA";
        $params['realignment'] = $session->get('common')['planAllowsRealignments'];
        $params['rebalance'] = false;
        $session->set("savedFundsTemp",$funds);
        return $this->render('SpeAppBundle:Template:investments/general/confirm.html.twig',$params);
    }
    /**
     * @Route("/investments/default", name="investmentsDefault")
     */
    public function investmentsDefaultAction(Request $request)
    {
        $planData = $this->get("session")->get('rkp')['plan_data'];
        $this->setInvestmentsSection("DEFAULT");
        $funds = [];
        $funds[0] = $this->getFunds()[$planData->rkpExtras->defaultInvestment->fund[1]];
        $funds[0]['percent'] = 100;  
        $params['funds'] =  $funds;
        $params['translation']['title'] = "Default";
        $this->commonConfirmVariables($params);
        $this->get("session")->set("savedFundsTemp",$funds);
        return $this->render('SpeAppBundle:Template:investments/general/confirm.html.twig',$params);
    }
    /**
     * @Route("/investments/risk", name="investmentsRisk")
     */
    public function investmentsRiskAction(Request $request)
    {
        $this->setInvestmentsSection("RISK");
        $portfolios = $this->getPortfolios();
        foreach ($portfolios as &$portfolio)
        {
            $portfolio['checked'] = false;
            if ($portfolio['name'] == $this->get("session")->get("risk_profile")['recommendedPortfolioName'])
            {
                $portfolio['checked'] = true;
            }
        }       
        $params['portfolios'] = $portfolios;
        $params['recommendedPortfolioName'] = $this->get("session")->get("risk_profile")['recommendedPortfolioName'];
        $this->get("session")->set("investmentSectionUrl",$request->getUri());
        return $this->render('SpeAppBundle:Template:investments/risk/selectportfolio.html.twig',$params);

    }
    public function getTargetFundsByYearsToRetirement()
    {
        return $this->get("spe.app.investments")->getTargetFundsByYearsToRetirement();
    }
    public function getTargetFundsByYearsToRetirementCorrectFormat()
    {
        return $this->get("spe.app.investments")->getTargetFundsByYearsToRetirementCorrectFormat();
    }
    /**
     * @Route("/atBlueprint/main", name="atBlueprintMain")
     */
    public function atBlueprintMainAction(Request $request, $path = "ATLastPath")
    {
        $session = $request->getSession();
        if ($path === 'ATLastPath') {
            if ($session->get("ATLastPath") == null) {
                $session->set("ATLastPath", "/atBlueprint/personalInfo/required");
            }
        } else {
            if ($session->get("ArchitectLastPath") == null) {
                $session->set("ArchitectLastPath", "/investments/atarchitect/1");
            }
        }
        return $this->render('SpeAppBundle:Template:ATBlueprint/main.html.twig',["path" => $session->get($path)]);
    }
    /**
     * @Route("/investments/targetdate", name="investmentsTargetDate")
     */
    public function investmentsTargetDateAction(Request $request)
    {
        $funds = $this->getTargetFundsByYearsToRetirementCorrectFormat();   
        $this->setInvestmentsSection("TARGET");
        $params = array();
        if (count($funds) == 1)
        {
            $funds[0]['percent'] = 100;  
            $params['funds'] =  $funds;
            $params['translation']['title'] = "Target";
            $this->commonConfirmVariables($params);
            $this->get("session")->set("savedFundsTemp",$funds);
            $file ="confirm";            
        }
        else
        {
            $params['funds'] =  $funds;
            $params['fundsHeader'] = $this->fundsHeader();
            $params['audio'] = "investments_0601a_VAe";
            $file = "investmentselector";
            $this->get("session")->set("investmentSectionUrl",$request->getUri());
        }
        return $this->render('SpeAppBundle:Template:investments/general/'.$file.".html.twig",$params);
    } 
    public function fundsHeaderSimple()
    {
        return $this->get("spe.app.investments")->fundsHeaderSimple();
    }
    public function fundsHeaderProspectUrl()
    {
        return $this->get("spe.app.investments")->fundsHeaderProspectUrl();
    }
    public function fundsHeaderConfirm()
    {
        return $this->get("spe.app.investments")->fundsHeaderConfirm();
    }
    public function fundsHeader()
    {
        return $this->get("spe.app.investments")->fundsHeader();
    }
    /**
     * @Route("/investments/customchoice", name="investmentsCustomChoice")
     */
    public function investmentsCustomChoiceAction(Request $request)
    {
        $params = array();
        $params['audio'] = "investments_0601a";
        $params['funds'] =  $this->getFunds();
        $params['fundsHeader'] = $this->fundsHeader();
        $this->setInvestmentsSection("CUSTOM");
        $this->get("session")->set("investmentSectionUrl",$request->getUri());
        $params['sectionDescription'] =$this->get('translator')->trans('custom_choice_description', array(), 'investments');
        return $this->render('SpeAppBundle:Template:investments/general/investmentselector.html.twig',$params);
    }
    /**
     * @Route("/investments/confirm", name="investmentsConfirm")
     */
    public function investmentsConfirmAction(Request $request)
    {
        $savedFunds = $request->request->get("funds");
        $funds = $this->getFunds();
        foreach ($savedFunds as $fund)
        {
            $funds[$fund['id']]['percent'] = $fund['percent'];
        }
        foreach ($funds as $fund)
        {
            if (!isset($fund['percent']))
            {
                unset($funds[$fund['id']]);
            }            
        }
        $params['funds'] = $funds;
        $this->get("session")->set("savedFundsTemp",$funds);
        if ($request->request->get("portfolioid"))
        {
            $portfolios = $this->getPortfolios();
            foreach ($portfolios as $portfolio)
            {
                if ($portfolio['id'] == (int)$request->request->get("portfolioid"))
                {
                   $this->get("session")->set("investmentsSelectedPortfolio",$portfolio);
                }
            }
        }
        $this->commonConfirmVariables($params);
        return $this->render('SpeAppBundle:Template:investments/general/confirm.html.twig',$params);
    }
    public function setInvestmentsSection($section)
    {
        $this->get("spe.app.investments")->setInvestmentsSection($section);
    }
    public function commonConfirmVariables(&$params)
    {
        $params['fundsHeader'] = $this->fundsHeaderConfirm();
        $params['rebalance'] = false;
        $params['rebalanceFrequency'] = false;
        $params['realignmentFile'] = "realignment";
        if ($this->get("session")->get("investmentSectionTemp") == "CUSTOM")
        {
            $params['rebalance'] = $this->get("session")->get('common')['planAllowsRebalance'];
            $params['rebalanceFrequency'] = $this->get("session")->get('common')['planAllowsRebalanceFrequency']; 
            $translator= $this->get('translator');
            $params['rebalanceFrequencyOptions'] = 
            [
                [
                    "value" => "daily",
                    "text" => $translator->trans('rebalance_daily', array(), 'investments')                    
                ],
                [
                    "value" => "weekly",
                    "text" => $translator->trans('rebalance_weekly', array(), 'investments')                    
                ],
                [
                    "value" => "bi_weekly",
                    "text" => $translator->trans('rebalance_bi_weekly', array(), 'investments')                    
                ],
                [
                    "value" => "semi_monthly",
                    "text" => $translator->trans('rebalance_semi_monthly', array(), 'investments')                    
                ],
                [
                    "value" => "monthly",
                    "text" => $translator->trans('rebalance_monthly', array(), 'investments')                    
                ],
                [
                    "value" => "quarterly",
                    "text" => $translator->trans('rebalance_quarterly', array(), 'investments')
                ],
                [
                    "value" => "semi-annual",
                    "text" => $translator->trans('rebalance_semi_annual', array(), 'investments')
                ],
                [
                    "value" => "annual",
                    "text" => $translator->trans('rebalance_annual', array(), 'investments')
                ],
            ];
            foreach ($params['rebalanceFrequencyOptions'] as &$option)
            {
                $option['selected'] = false;
                if ($option['value'] == $this->get("session")->get('common')['rebalanceFrequency'])
                $option['selected'] = true;
            }
        }
        if (strtolower($this->get("session")->get("account")['connectionType']) == "omnimoa" && $this->get("session")->get("investmentSectionTemp") == "CUSTOM")
            $params['realignmentFile'] = "realignmentRebalanceVersion";
        $params['realignment'] = $this->get("session")->get('common')['planAllowsRealignments'] ;
    }
    /**
     * @Route("/investments/finish", name="investmentsFinish")
     */
    public function investmentsFinishAction(Request $request)
    {
        $common = $this->get("session")->get("common");
        if ($request->request->get("realignment",null) !== null)
        {           
            $common['updateRealignment'] = $request->request->get("realignment");
        }
        if ($request->request->get("rebalance",null) !== null)
        {           
            $common['updateRebalance'] = $request->request->get("rebalance");
        }
        if ($request->request->get("rebalanceFrequency",null) !== null)
        {           
            $common['rebalanceFrequency'] = $request->request->get("rebalanceFrequency");
        }
        if (empty($common['updateRebalance']))
        {
            $common['rebalanceFrequency'] = null;
        }
        $this->get("session")->set("savedFunds",$this->get("session")->get("savedFundsTemp"));
        $this->get("session")->set("investmentSection",$this->get("session")->get("investmentSectionTemp"));        
        $this->get("session")->set("investmentsComplete",true);
        $common['updateElections'] = true;
        $this->get("session")->set("common",$common);
        $this->get("session")->set("investmentsGraphComplete",0);//adp is bad 4 u
        return new Response("");
    }
        /**
         * @Route("/investments/stadionAgreementSaved", name="stadionAgreementSaved")
         */
        public function stadionAgreementSavedAction(Request $request)
        {   
            $session = $request->getSession();
            $response = $session->get("stadionQuestionnaireResponse");
            $investments = $session->get("investments");
            $investments['I_SelectedInvestmentOption'] = "STADION";   
            $funds = $response['data']['vWiseModel'];
            $fundTransactString = "";
            $savedFunds = [];
            foreach ($funds as $ticker => $percent)
            {
                if ($fundTransactString  != "")
                {
                    $fundTransactString = $fundTransactString."|";
                }
                $fundTransactString = $fundTransactString.$ticker."^".$percent."^".$ticker;   
                $savedFund['ticker'] = $ticker;
                $savedFund['percent'] = $percent;
                $savedFunds[] = $savedFund;
            }
            $investments['I_SelectedInvestmentDetails'] = $fundTransactString;
            $investments['investmentsStatus'] = 1;
            $session->set("investments",$investments);
            $common = $session->get('common');
            $common['updateElections'] = 1;
            $session->set("investmentSection",$investments['I_SelectedInvestmentOption']);
            $session->set("investmentsSection",$investments['I_SelectedInvestmentOption']);
            $session->set("savedFunds",$savedFunds);
            $session->set("common",$common);
            return new Response("");
        }  
}
