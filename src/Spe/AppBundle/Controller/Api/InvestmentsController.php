<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Investments controller.
 * @Route("/api/v2/investments")
 */
class InvestmentsController extends AbstractFOSRestController {

    /**
     * @Route("/getInvestmentSections")
     */
    public function getInvestmentSectionsAction() {
        return $this->view($this->get("spe.app.investments")->getSections(), Response::HTTP_OK);
    }

    /**
     * @Route("/getFunds")
     */
    public function getFundsAction() {
        return $this->view($this->get("spe.app.investments")->getFunds(), Response::HTTP_OK);
    }
    
    /**
     * @Route("/getTargetDateFunds")
     */
    public function getTargetDateFunds() {
        return $this->view($this->get("spe.app.investments")->getTargetDateFunds(), Response::HTTP_OK);
    }
    
    /**
     * @Route("/getPortfolios")
     */
    public function getPortfolios() {
        return $this->view($this->get("spe.app.investments")->getPortfolios(), Response::HTTP_OK);
    }

    /**
     * @Route("/getPortfolioByName/{name}")
     */
    public function getPortfolioByName($name) {
        return $this->view($this->get("spe.app.investments")->getPortfolioByName($name), Response::HTTP_OK);
    }
    
    /**
     * @Route("/getTargetFundsByYearsToRetirement")
     */
    public function getTargetFundsByYearsToRetirement() {
        return $this->view($this->get("spe.app.investments")->getTargetFundsByYearsToRetirement(), Response::HTTP_OK);
    }
    
    /**
     * @Route("/getTargetFundsByYearsToRetirementCorrectFormat")
     */
    public function getTargetFundsByYearsToRetirementCorrectFormat() {
        return $this->view($this->get("spe.app.investments")->getTargetFundsByYearsToRetirementCorrectFormat(), Response::HTTP_OK);
    }
    
    /**
     * @Route("/fundsHeaderSimple")
     */
    public function fundsHeaderSimple() {
        return $this->view($this->get("spe.app.investments")->fundsHeaderSimple(), Response::HTTP_OK);
    }
    
    /**
     * @Route("/fundsHeaderConfirm")
     */
    public function fundsHeaderConfirm() {
        return $this->view($this->get("spe.app.investments")->fundsHeaderConfirm(), Response::HTTP_OK);
    }
    
    /**
     * @Route("/fundsHeader")
     */
    public function fundsHeader() {
        return $this->view($this->get("spe.app.investments")->fundsHeader(), Response::HTTP_OK);
    }
    
    /**
     * @Route("/setInvestmentsSection")
     */
    public function setInvestmentsSection(Request $request) {
        $this->get("spe.app.investments")->setInvestmentsSection($request->get("section"));
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/targetdate")
     */
    public function investmentsTargetDateAction(Request $request) {
        $response = $this->forward('SpeAppBundle:Investments:investmentsTargetDate', array('request' => $request));
        return $response;
    }

    /**
     * @Route("/customchoice")
     */
    public function investmentsCustomChoiceAction(Request $request) {
        $this->get("spe.app.investments")->setInvestmentsSection("CUSTOM");
        $response = $this->forward('SpeAppBundle:Investments:investmentsConfirm', array('request' => $request));
        return $response;
    }

    /**
     * @Route("/risk")
     */
    public function investmentsRiskAction(Request $request) {
        $this->get("spe.app.investments")->setInvestmentsSection("RISK");
        $response = $this->forward('SpeAppBundle:Investments:investmentsConfirm', array('request' => $request));
        return $response;
    }

    /**
     * @Route("/qdia")
     */
    public function investmentsQdiaAction(Request $request) {
        $response = $this->forward('SpeAppBundle:Investments:investmentsQdia', array('request' => $request));
        return $response;
    }

    /**
     * @Route("/default")
     */
    public function investmentsDefaultAction(Request $request) {
        $response = $this->forward('SpeAppBundle:Investments:investmentsDefault', array('request' => $request));
        return $response;
    }

    /**
     * @Route("/finish")
     */
    public function investmentsFinishAction(Request $request) {
        $response = $this->forward('SpeAppBundle:Investments:investmentsFinish', array('request' => $request));
        $this->get("Spe2Service")->updateProfile();
        return $response;
    }
    
    public function setContainer($container)
    {
        parent::setContainer($container);
        $this->get("translator")->setLocale($this->get("session")->get("common")['locale']);
    }   
}
