<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class BaseApiController extends AbstractFOSRestController 
{
    public function setContainer($container)
    {
        parent::setContainer($container);
        $this->get("translator")->setLocale($this->get("session")->get("common")['locale']);
    }           
}