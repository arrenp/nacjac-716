<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Retirement Needs controller.
 * @Route("/api/v2/retirementneeds")
 */
class RetirementNeedsController extends BaseApiController
{
    /**
     * @Route("/getData")
     */
    public function getDataAction() {
        $data = $this->get("spe.app.retirementneeds")->getData();
        if (!is_array($data['data']['RN_AssetTypes'])) {
            $data['data']['RN_AssetTypes'] = [];
        }
        return $this->view($data, Response::HTTP_OK);
    }

    /**
     * @Route("/saveData")
     */
    public function saveDataAction(Request $request) {
        $data = array_diff_assoc($request->request->all(), $this->get("spe.app.retirementneeds")->getData()['data']);
        $data["RN_AssetTypes"] = $request->request->get("RN_AssetTypes");
        $this->get("spe.app.retirementneeds")->saveData($data);
        $this->get('Spe2Service')->updateProfile();
        return $this->view($this->get("spe.app.retirementneeds")->getData(), Response::HTTP_OK);
    }

    /**
     * @Route("/resultsData")
     */
    public function resultsDataAction() {
        $calcsService = $this->get("RetirementNeedsCalcs");
        $results = $calcsService->calculateResults();
        return $this->view($results, Response::HTTP_OK);
    }
    
    /**
     * @Route("/finishRN")
     */
    public function finishRN() {
        $this->get("spe.app.retirementneeds")->finishRN();
        return $this->view($this->get("spe.app.retirementneeds")->getData(), Response::HTTP_OK);
    }
    
}