<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * My Profile controller.
 * @Route("/api/v2/path")
 */
class PathsController extends BaseApiController 
{
    /**
     * @Route("/getPathAccount/{userid}/{type}/{section}")
     */
    public function getPathAccountAction($userid,$type,$section = null)
    {
        $account = $this->get("AccountsService")->findOneBy(["id" => $userid]);
        $this->get("PathsService")->setUserId($account->id);
        $fullPath = $this->getPathFull($account,$type);
        if (!empty($fullPath->$section))
            $fullPath = $fullPath->$section;
        return $this->json($fullPath);
    }  
    /**
     * @Route("/getPathPlan/{planid}/{type}/{section}")
     */
    public function getPathPlanAction($planid,$type,$section = null,Request $request)
    {
        $plan = $this->get("PlansService")->findOneBy(["id" => $planid]);
        $this->get("PathsService")->setUserId($plan->userid);
        $this->get("PathsService")->setPlanId($plan->id);
        if (!empty($request->query->get("lang")))
            $this->get("PathsService")->setLang($request->query->get("lang"));        
        $fullPath = $this->getPathFull($plan,$type);
        if (!empty($plan->userid) && empty($fullPath))
            return $this->getPathAccountAction ($plan->userid, $type);
        if (!empty($fullPath->$section))
            $fullPath = $fullPath->$section;
//        return $this->view($fullPath,Response::HTTP_OK);
        return $this->json($fullPath);
    }    
    public function getPathId($entity,$type)
    {
        return $entity->{$type."PathId" };
    }
    public function getPathFull($entity,$type)
    {
        $pathId = $this->getPathId($entity,$type);
        $this->get("PathsService")->setPathId($pathId);
        $pathFull =  $this->get("PathsService")->getPathFullApi(["id" => $pathId]);
        if (!isset($pathFull->id))
        return false;
        return $pathFull;
    }
    /**
     * @Route("/getAvailablePaths/{planid}/")
     */
    public function getAvailablePathsAction($planid)
    {
        $types = $this->get("PathsService")->getPlanPathsApp($planid);
        return $this->json($types);
    }

    /**
     * @Route("/setPath/")
     */
    public function setPathAction(Request $request) {
        $spe2 = $request->getSession()->get('spe2', []);
        $spe2['pathId'] = $request->get("id");
        $request->getSession()->set('spe2', $spe2);
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
}