<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Contributions controller.
 * @Route("/api/v2/contributions")
 */
class ContributionsController extends BaseApiController {
    
    /**
     * @Route("/saveContributions")
     */
    public function saveContributionsAction()
    {
        $response = $this->forward('SpeAppBundle:Contributions:saveContributions');
        $this->get("Spe2Service")->updateProfile();
        return $this->view(json_decode($response->getContent(), true), Response::HTTP_OK);
    }
    
    /**
     * @Route("/calculateData")
     */
    public function calculateDataAction(Request $request) {
        $contributionsService = $this->get("spe.vue.contributions");
        $saveData = $request->request->get("saveData");
        $contributions = $this->get("session")->get("contributions");
        $contributions['C_MonthlyContributionTotal'] = $contributionsService->stringToDouble($contributions['C_MonthlyContributionTotal']);
        $orgContributions = $contributions;
        $orgCurrentYearlyIncome = $this->get("session")->get("currentYearlyIncome");
        $contributions = array_merge($contributions,$saveData);
        $this->get("session")->set("currentYearlyIncome",$contributionsService->stringToDouble($request->request->get("currentYearlyIncome")));
        $this->get("session")->set("contributions",$contributions);
        $params = array();
        $params['blockPercents'] = $request->request->get("blockPercents") == "true";
        $data = $contributionsService->calculate($params);
        $this->get("session")->set("currentYearlyIncome",$orgCurrentYearlyIncome);
        $this->get("session")->set("contributions",$orgContributions);
        return $this->view($data, Response::HTTP_OK);
    }

    /**
     * @Route("/saveAutoIncrease")
     * @param Request $request
     */
    public function saveAutoIncrease(Request $request) {
        $data = $request->request->all();
        $this->get("spe.app.contributions")->saveAutoIncrease($data);
        $this->get("Spe2Service")->updateProfile();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/updateACAOptOut")
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function updateAcaOptOut(Request $request)
    {
        $session = $request->getSession();
        $session->set('ACAOptOutProfile', $request->get('ACAOptOut'));
        $this->get("spe.app.contributions")->saveAutosOptOuts($request->get('ACAOptOut'));
        $this->get("Spe2Service")->updateProfile();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    
}
