<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * My Profile controller.
 * @Route("/api/v2/style")
 */
class StyleController extends BaseApiController 
{
    /**
     * @Route("/getHexColorsAccount/{userid}")
     */
    public function getHexColorsAccount($userid)
    {
        $hexColors = $this->get("AccountAppColorsService")->findOneBy(["userid" => $userid]);
        return $this->json($this->get("StyleService")->hexColorValues($hexColors));
    }
    /**
     * @Route("/getHexColorsPlan/{planid}")
     */
    public function getHexColorsPlan($planid)
    {
        $hexColors = $this->get("PlanAppColorsService")->findOneBy(["planid" => $planid]);
        return $this->json($this->get("StyleService")->hexColorValues($hexColors));
    }
}