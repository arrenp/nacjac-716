<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Profile controller.
 * @Route("/api/v2/profile")
 */
class ProfileController extends BaseApiController {
    
    /**
     * @Route("/createProfile")
     */
    public function createProfileAction() {
        $this->get('spe.app.profile')->createProfile();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
    
    /**
     * @Route("/updateProfile")
     */
    public function updateProfileAction(Request $request) {
        $json = $this->get('spe.app.profile')->updateProfile($request->request->get("saveDate"));
        return $this->view(json_decode($json, true), Response::HTTP_OK);
    }
    
    /**
     * @Route("/getProfileReport")
     */
    public function getProfileReportAction(Request $request) {
        $data = $this->get('spe.app.profile')->getProfileReport($request->get("profileId"), $request->get("showPersonalInformation"), $request->get("ajax"));
        return $this->view($data, Response::HTTP_OK);
    }
    
    /**
     * @Route("/uiData")
     */
    public function uiDataAction() {
        return $this->view($this->get('spe.app.profile')->uiData(), Response::HTTP_OK);
    }
}
