<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * My Profile controller.
 * @Route("/api/v2/myprofile")
 */
class MyProfileController extends BaseApiController {
    
    /**
     * @Route("/beneficiaries/save")
     */
    public function saveBeneficiaryAction() {
        $response = $this->forward('SpeAppBundle:MyProfile:myProfileBeneficiaryAddSaved');
        $this->get("Spe2Service")->updateProfile();
        if ($response->getContent() === "success") {
            return $this->view(null, Response::HTTP_NO_CONTENT);
        }
        return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
    
    /**
     * @Route("/beneficiaries/delete")
     */
    public function deleteBeneficiaryAction() {
        $response = $this->forward('SpeAppBundle:MyProfile:myProfileBeneficiaryDelete');
        $this->get("Spe2Service")->updateProfile();
        if ($response->getContent() === "") {
            return $this->view(null, Response::HTTP_NO_CONTENT);
        }
        return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @Route("/beneficiaries/all")
     */
    public function allBeneficiariesAction(Request $request) {
        return $this->view($request->getSession()->get("beneficiaryList", []), Response::HTTP_OK);
    }
    
    /**
     * @Route("/updateTransaction")
     */
    public function updateTransactionAction() {
        $response = $this->forward('SpeAppBundle:Ajax:updateTransaction', ['isSpe2' => true]);
        return $this->view(json_decode($response->getContent(), true), Response::HTTP_OK);
    }
    
    /**
     * @Route("/ECOMMModalSubmit")
     */
    public function ECOMMModalSubmitAction() {
        $response = $this->forward('SpeAppBundle:MyProfile:ECOMMModalSubmit');
        if ($response->getContent() === "") {
            return $this->view(null, Response::HTTP_NO_CONTENT);
        }
        return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
    
    /**
     * @Route("/saveRolloverOption")
     */
    public function saveRolloverOptionAction() {
        $response = $this->forward('Spe\AppBundle\Controller\AjaxController::saveRolloverOption');
        return $this->view(json_decode($response->getContent(), true), Response::HTTP_OK);
    }
    /**
     * @Route("/getProfileData")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getProfileDataAction(Request $request)
    {
        $cookie = $request->request->get("cookie");
        $data = $this->get("Profiles2Service")->getAppData(["cookie" => $cookie]);
        if ($data) {
            $datefields = ['autoEscalateStartDate','autoIncreaseStartDate'];
            foreach ($datefields as $field) {
                if ($data['appData'][$field]) {
                    $data['appData'][$field] = date('M. j, Y', strtotime($data['appData'][$field]));
                }
            }
            return $this->json($data);
        }

        return $this->json(["error" => "cookie doesn't exist"]);
    }   
    /**
     * @Route("/addProfileData/")
     */    
    public function addProfileDataAction(Request $request) 
    {    
        if ($request->isMethod('POST'))
        {
            $json = $request->getContent();                
            return $this->json(["success" => $this->get("Profiles2Service")->add(json_decode($json,true)) ]);
        }
        return $this->json(["success" => false]);       
    }      
    /**
     * @Route("/putProfileData/")
     */    
    public function putProfileDataAction(Request $request) 
    {
        if ($request->isMethod('PUT'))
        {
            $json =  $request->getContent();            
            $data = json_decode($json,true);        
            return $this->json(["success" => $this->get("Profiles2Service")->edit(["cookie" => $data['cookie']],$data)]);
        }
        return $this->json(["success" => false]);       
    }

    /**
     * @Route("/sendProfileEmail")
     */
    public function sendProfileEmailAction(Request $request){
        $email = $request->get('email');
        if($email) {
            if (!$this->sendProfileEmail($email)) {
                return $this->json(array('status' => 'fail'));
            }
        }
        return $this->json(array('status' => 'success'));
    }

    private function sendProfileEmail($email = null, $bcc = null){
        $session = $this->get("session");
        $translator = $this->get('translator')->getMessages();
        $planName = isset($session->get('plan')['name']) && $session->get('plan')['name'] ? $session->get('plan')['name'] : $session->get('rkp')['plan_data']->planName;
        $emails = array();
        if ($email != null){
            $emails[] = trim($email);
        }
        else if(isset($session->get('plan')['profileNotificationEmail']) && $session->get('plan')['profileNotificationEmail'] != ""){
            $emailSplit = explode(",",$session->get('plan')['profileNotificationEmail']);
            foreach ($emailSplit as $email){
                $emails[] = trim($email);
            }
        }
        else
            return null;


        $emailer = $this->get('spe.app.mailer');
        foreach ($emails as $email){
            $from = $session->get("plan")['investorProfileEmail'];
            $subject = 'Your Investor Profile';
            $content = "<div style='margin-top:30px;font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height: 25px;color: black;'>
                                <span style='font-size: 20px;line-height: 30px;'>". $translator['my_profile']['download_your_investor_profile_for'] ." <br> $planName <br></span>
                                <a href='".$this->get('spe.app.functions')->getBaseUrl()."/spe2/profile/report/".$session->get('spe2profileId')."' target='_blank'>". $translator['my_profile']['your_investor_profile'] ."</a>
                                <br><br>Thank you!<br>
                                <span style='padding-top:10px;margin-top:10px;font-family: Arial, Helvetica, sans-serif;font-size: 12px;line-height: 10px;'>&copy; ".date('Y')." ". $translator['my_profile']['smartplan_enterprise_is_trademark_of_vwise'] ."</span>
                            </div>";
            if(!$emailer->sendMail($email, $from, $subject, $content, null, $bcc)){
                return false;
            }
        }
        return true;
    }


}
