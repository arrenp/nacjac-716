<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Default controller.
 * @Route("/api/v2/default")
 */
class DefaultController extends BaseApiController {
    
    /**
     * @Route("/load")
     */
    public function loadAction(Request $request) {
        foreach ($request->request->all() as $key => $value) {
            $_REQUEST[$key] = $value;
        }
        $response = $this->forward('SpeAppBundle:Default:loadingJson');
        return $this->view(json_decode($response->getContent(), true), Response::HTTP_OK);
    }
    
    /**
     * @Route("/initialize")
     */
    public function initializeAction(Request $request) {
        $response = $this->forward('SpeAppBundle:Ajax:initializeJson');
        $data = json_decode($response->getContent(), true);
        if (isset($data['error']) || !is_array($data)) {
            return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $this->get('Spe2Service')->createProfile();
        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/getData")
     */
    public function getDataAction(Request $request) {
        $session = $request->getSession();
        $common = $session->get('common');
        $locale = isset($common['locale']) ? $common['locale'] : 'en';
        $response = $this->forward('SpeAppBundle:Ajax:welcome');
        $data = json_decode($response->getContent(), true);
        $spe_plan_id = $request->getSession()->get('spe_plan_id');
        $data['colors'] = $this->get("PlanAppColorsService")->getColorsByPlan($spe_plan_id);
        $data['plan'] = $request->getSession()->get("plan");
        $data['fork'] = $this->get("PlanAppForkContentService")->getForkContentByPlan($spe_plan_id, $locale);
        return $this->view($data, Response::HTTP_OK);
    }
    /**
     * @Route("/changeLocale", name="changeLocale")
     */
    public function changeLocaleAction(Request $request)
    {
        $common = $this->get("session")->get("common");
        $common['locale'] = $request->request->get("locale");
        $this->get("session")->set("common",$common);
        $REQUEST = $this->get("session")->get('REQUEST');
        $REQUEST['lang'] = $common['locale'];
        $this->get("session")->set("REQUEST",$REQUEST);
        return new Response("");
    }
    /**
     * @Route("/saveRolloverOption", name="saveRolloverOption")
     */
    public function saveRolloverOptionAction(Request $request)
    {
        $response = $this->forward('SpeAppBundle:Ajax:saveRolloverOption');
        return new Response("");
    }
}
