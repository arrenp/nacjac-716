<?php

namespace Spe\AppBundle\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Risk Profile controller.
 * @Route("/api/v2/riskprofile")
 */
class RiskProfileController extends BaseApiController {
    
    /**
     * @Route("/getRP")
     */
    public function getRPAction(Request $request) {
        $filename = null;
        if ($request->get('kinetikAccepted') !== null) {
            $request->getSession()->set('smart401k', $request->get('kinetikAccepted'));
            $filename = "smart401k";
        }
        if (!empty($request->get('stadionAccepted')) ) {
            $filename = "stadionManagedAccount";
            $request->getSession()->set('managedAccountOptInValue', true);
        }

        $response = $this->forward('SpeAppBundle:Ajax:riskProfile', array(), ['filename' => $filename]);
        $data['data'] = json_decode($response->getContent(), true);
        $data['isStadion'] = $request->getSession()->get('module')['stadionManagedAccountOn'];
        $data['isKinetik'] = $request->getSession()->get('smart401k');
        $data['managedaccountid'] = $request->getSession()->get('rkp')['plan_data']->rkpExtras->stadionManagedAccountId;
        return $this->view($data, Response::HTTP_OK);
    }
    /**
     * @Route("/riskProfileInvestor")
     */
    public function riskProfileInvestorAction(Request $request) {
        $session = $request->getSession();
        if ($session->get('risk_profile')['stadionManagedAccountOn']) {
            $session->set('managedAccountOptInValue', $request->request->get('stadionAccepted'));
        }
        $response = $this->forward('SpeAppBundle:Ajax:riskProfileInvestor');
        $this->get('Spe2Service')->updateProfile();
        return $this->view(json_decode($response->getContent(), true), Response::HTTP_OK);
    }

    /**
     * @Route("/getStadionGraphs")
     */
    public function getStadionGraphsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $response = $session->get("stadionQuestionnaireResponse");
        $type = "standard";
        if ($session->get('rkp')['plan_data']->rkpExtras->stadionManagedAccountId == 1)
            $type = "advisor";
        if (!empty($response['success']))
        {
            $data = $response['data'];
            $params['riskType'] = $session->get('risk_profile')['stadionLabels'][$data['pathId']];
            $params['graph'] = $em->getRepository('classesclassBundle:StadionGraph')->findOneBy(["pathId" => $data['pathId'],"type" => $type]);
            $params = json_encode($params);
        }
        return $this->view(json_decode($params, true), Response::HTTP_OK);
    }

    /**
     * @Route("/getStadionDisclaimerText")
     */
    public function getStadionDisclaimerTextAction(Request $request) {
        $percentage = "0.35";
        $qdialink = "qdia_link_35";
        $flyerlink = "flyer_link_35";
        $agreementText = "aggreement_text";
        $session = $request->getSession();
        $managedaccountid = $session->get('rkp')['plan_data']->rkpExtras->stadionManagedAccountId;
        if ($managedaccountid == 2) {
            $percentage = "0.55";
            $qdialink = "qdia_link_55";
            $flyerlink = "flyer_link_55";
            $agreementText = "aggreement_text19";
        }
        if ($managedaccountid == 1)
        {
            $percentage = "0.35";
            $qdialink = "qdia_link_managedaccountid_1";
            $flyerlink = null;
            $agreementText = "aggreement_text18";
        }
        $params = json_encode(array('percentage' => $percentage, 'qdialink' => $qdialink, 'flyerlink' => $flyerlink,"agreementText" => $agreementText));
        return $this->view(json_decode($params, true), Response::HTTP_OK);
    }

    /**
     * @Route("/stadionRetireExpress", name="stadionRetireExpress")
     */
    public function stadionRetireExpressAction(Request $request)
    {
        $session = $request->getSession();
        $response = $this->container->get('spe.app.rkp')->postStadionQuestionnaire(null);
        $session->set("stadionQuestionnaireResponse",$response);
        $session->set("stadionQuestionnaireAnswers",null);
        $this->get('spe.app.smart')->setStadionManagedAccountData(true);

        $em = $this->get('doctrine')->getManager();
        if ($session->get('rkp')['rkp'] !== 'educate') {
            $profile = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('id' => $session->get('pid')));
            $this->get('spe.app.profile')->addRiskProfile($profile);
        }

        $response = $this->forward('SpeAppBundle:Investments:stadionAgreementSaved');
        return $this->view(json_decode($response->getContent(), true), Response::HTTP_OK);
    }
}