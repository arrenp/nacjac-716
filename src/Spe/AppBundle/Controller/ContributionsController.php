<?php
namespace Spe\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Contributions controller.
 * @Route("/", name="contributions", condition="request.headers.get('X-Requested-With') == 'XMLHttpRequest'")
 */
class ContributionsController extends Controller
{    
    /**
     * @Route("/contributions", name="contributions")
     */
    public function contributionsAction(Request $request)
    {    
        $contributions = $this->get("spe.app.contributions");
        return $this->render("index.html.twig",array_merge($contributions->calculate(),["payFrequency" => $contributions->payFrequency() ]));
    }
    
    public function render($file,$params = [])
    {
        return parent::render("SpeAppBundle:Template:contributions/".$file,$params);
    }
    /**
     * @Route("/contributions/save", name="saveContributions")
     */
    public function saveContributionsAction(Request $request)
    {

        $contributionsService = $this->get("spe.app.contributions");
        $json = $request->request->get("request");
        $data = json_decode($json,true);
        $saveData = $data['saveData'];
        $contributions = $this->get("session")->get("contributions");
        $contributions = array_merge($contributions,$saveData);
        $contributions['contributionsStatus'] = 1;
        $contributions['updateContributions'] = 1;
        $contributions['C_MonthlyContributionTotal'] = $contributionsService->stringToDouble($contributions['C_MonthlyContributionTotal']);
        if (isset($saveData['catchUpContributionValue']))
        {
            $contributions['catchUpContributionValue'] = $contributionsService->stringToDouble($saveData['catchUpContributionValue']);
        }
        $this->get("session")->set("currentYearlyIncome",$contributionsService->stringToDouble($data["currentYearlyIncome"]));
        $this->get("session")->set("contributions",$contributions);
        if ($request->request->has("autosOptOut")) {
            $this->get("spe.app.contributions")->saveAutosOptOuts($request->request->get("autosOptOut"));
        }

        return new JsonResponse($contributions);
    }
    /**
     * @Route("/contributions/calculateData", name="calculateData")
     */     
    public function calculateDataAction(Request $request)
    {
        $contributionsService = $this->get("spe.app.contributions");
        $saveData = $request->request->get("saveData");
        $contributions = $this->get("session")->get("contributions");
        $contributions['C_MonthlyContributionTotal'] = $contributionsService->stringToDouble($contributions['C_MonthlyContributionTotal']);
        $orgContributions = $contributions;
        $orgCurrentYearlyIncome = $this->get("session")->get("currentYearlyIncome");
        $contributions = array_merge($contributions,$saveData);
        $this->get("session")->set("currentYearlyIncome",$contributionsService->stringToDouble($request->request->get("currentYearlyIncome")));
        $this->get("session")->set("contributions",$contributions);
        $params = array();
        $params['blockPercents'] = $request->request->get("blockPercents") == "true";
        $response = new JsonResponse($contributionsService->calculate($params));
        $this->get("session")->set("currentYearlyIncome",$orgCurrentYearlyIncome);
        $this->get("session")->set("contributions",$orgContributions);    
        return $response;
    }
}