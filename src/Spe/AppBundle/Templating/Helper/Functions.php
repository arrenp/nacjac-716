<?php

namespace Spe\AppBundle\Templating\Helper;

use Spe\AppBundle\Services\SmartService;
use Spe\AppBundle\Services\RkpService;
use Spe\AppBundle\Services\PlanService;
use Spe\AppBundle\Services\EmailService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Helper\Helper;
use Symfony\Component\HttpFoundation\Session\Session;

class Functions extends Helper
{
    protected $smartService;

    public function __construct(Session $session, ContainerInterface $containerInterface, SmartService $smartService, RkpService $rkpService, PlanService $planService, EmailService $emailService)
    {
        $this->session = $session;
        $this->container = $containerInterface;
        $this->smartService = $smartService;
        $this->rkpService = $rkpService;
        $this->planService = $planService;
        $this->emailService = $emailService;
        $this->translator = $containerInterface->get('translator');
    }

    public function getName()
    {
        return 'Functions';
    }

    // Get Get Smart401K Advice Allocation Pct
    public function getAdviceAllocationPct($ticker) {
            $adviceFunds = $this->session->get("sessionAdviceFunds");
            if (!empty($adviceFunds[$ticker]))
            {
                return $adviceFunds[$ticker]['allocation'] * 100;
            }
            return "";
    }

    public function findP($needle, $items){
        $indx = null;
        foreach($items as $i => $item){
            if (in_array(strtolower(trim($needle)), array_map('strtolower', array_map('trim', $item)), true)) {
                $indx = $i;
                break;
            }
        }
        return $indx;
    }

    public function speEncrypt($data){
        return $this->smartService->speEncrypt($data);
    }

    public function speDecrypt($data){
        return $this->smartService->speDecrypt($data);
    }

    public function postSmart401k(){
        return $this->rkpService->postSmart401k();
    }

    public function findParticipantGender($name){
        return $this->planService->findParticipantGender($name);
    }

    public function sendEmail($to, $from, $subject, $content, $cc = null, $attachments = null){
        return $this->emailService->sendMail($to, $from, $subject, $content, $cc = null, $attachments = null);
    }

    public function getRPQuestionAnswer($xml, $riskProfileAnswers, $riskProfilePath){
        $options = array();
        if($xml && $riskProfileAnswers && $riskProfilePath) {
            $riskProfile_data = simplexml_load_file($riskProfilePath.$xml.'.xml');
            $riskProfileAnswers = explode('|', $riskProfileAnswers);
            $i = 0;
            foreach($riskProfile_data->questions as $questions){
                $currentQuestion = [];
                foreach($questions as $question){
                    $question_text = $question->text;
                    if($question->options->children()){
                        foreach($question->options->children() as $option){
                            if($option->attributes() == $riskProfileAnswers[$i]) {
                                $currentQuestion['question'] = trim($question_text);
                                $currentQuestion['answer'] = (string)$option[0];
                                $points = (array)($option->attributes()->value[0]);
                                $currentQuestion['points'] = $points[0];
                                $options[] = $currentQuestion;
                                $i++;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return $options;
    }

    public function getRNQuestionAnswer($answer = null, $retirementNeedsVersion = 'standard'){
        $options = array();
        $trans = $this->translator->getMessages()['alert'];
        if($answer){
            $options = array(
                $trans['rn_question_1'] => '$'.number_format($answer['currentYearlyIncome']),
                $trans['rn_question_2'] => '$'.number_format($answer['estimatedRetirementIncome']),
                $trans['rn_question_3'] => '$'.number_format($answer['estimatedSocialSecurityIncome']),
                $trans['rn_question_3a'] => '$'.number_format($answer['estimatedPensionIncome']),
                $trans['rn_question_4'] => '$'.number_format($answer['replacementIncome']),
                ($retirementNeedsVersion == 'by age') ? $trans['rn_question_5'] : $trans['rn_question_5a'] => ($retirementNeedsVersion == 'by age') ? $answer['retirementAge'] : $answer['yearsBeforeRetirement'],
                $trans['rn_question_6'] => '$'.number_format($answer['inflationAdjustedReplacementIncome']),
                $trans['rn_question_7'] => '$'.number_format((double)$answer['currentPlanBalance']),
                $trans['rn_question_8'] => '$'.number_format((double)$answer['otherAssets']),
                $trans['rn_question_9'] => '$'.number_format($answer['totalAssets']),
                $trans['rn_question_10'] => str_replace('^', ', ', $answer['assetTypes']),
                ($retirementNeedsVersion == 'by age') ? $trans['rn_question_11'] : $trans['rn_question_11a'] => ($retirementNeedsVersion == 'by age') ? $answer['lifeExpectancy'] : $answer['yearsLiveInRetirement'],
                $trans['rn_question_12'] => '$'.number_format($answer['annualIncomeNeededInRetirement']),
                $trans['rn_question_13'] => '$'.number_format($answer['estimatedSavingsAtRetirement']),
                $trans['rn_question_14'] => '$'.number_format($answer['recommendedMonthlyPlanContribution']),
                $trans['rn_question_15'] => $answer['percentageOfIncomeFromPlan'].'%',

            );
        }
        return $options;
    }

    public function getInvestorName($type = 0){
        $translate = $this->container->get('translator')->getMessages();
        $invesmentsTranslated = $translate['investments'];
        $investorLabels = array($invesmentsTranslated['conservative'], $invesmentsTranslated['moderately_conservative'], $invesmentsTranslated['moderate'], $invesmentsTranslated['moderately_aggressive'], $invesmentsTranslated['aggressive']);
        $investorAxaLabels = array($invesmentsTranslated['conservative'], $invesmentsTranslated['conservative_plus'], $invesmentsTranslated['moderate'], $invesmentsTranslated['moderate_plus'], $invesmentsTranslated['aggressive']);

        $risk_profile = $this->session->get('risk_profile');
        $client_calling = $this->session->get('client_calling');
        $xml = $risk_profile['RP_xml'];
        $name = "";
        if($client_calling == 'AXA' && $xml <> 'riskProfileAXA2013'){
            if (isset($investorAxaLabels[$type]))
            $name = $investorAxaLabels[$type];
        }else{
            if (isset($investorLabels[$type]))
            $name = $investorLabels[$type];
        }
        return $name;
    }
    function getPlainTextFromHtml($html) {

        $html = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $html);
        $html = preg_replace('/<style\b[^>]*>(.*?)<\/style>/is', "", $html);
        // Remove the HTML tags
        $html = strip_tags($html);

        // Convert HTML entities to single characters
        $html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
        if (strlen($html) > 100)
        {
            $html =  substr($html,0,100)." ..";
        }
        return $html;

    }
    public function getConfirmationStatus($type, $confirmation, $display_label = TRUE, $showType = true){
		$type = $showType ? $type : '';
        $confirmation = is_object(json_decode($confirmation)) ? json_decode($confirmation) : $confirmation;
        $trans = $this->translator->getMessages()['alert'];
        if($confirmation) {
            if (is_object($confirmation)) {
                if(isset($confirmation->success)) {
                    if ($confirmation->success) {
                        return "<div class='statusSuccess'> " . $type . " " . $trans['update_successful'] . ". <br/>" . $trans['confirmation'] . ": " . $confirmation->confirmation . "</div>";
                    } else {
                        return "<div class='statusError'> " . $type . " " . $trans['update_error'] . ". <br/>" . $trans['error'] . ": " . $confirmation->message . "</div>";
                    }
                }else {
                    $item = null;
                    $success = true;
                    $labels = array('elections' => 'Investments', 'asset_allocation' => 'Asset Allocation Model', 'realignment' => 'Reallocation', 'contributions' => 'Contributions');
                    foreach ($confirmation as $name => $row){
                        $label = isset($labels[$name]) ? $labels[$name] : ucfirst($name);
						if (!$display_label) {
							$label = '';
						}

                        $hasMessage = trim($row->message) <> "";
                        if($hasMessage)
                            $item .= "<li>".$label." Fail Message: ".$row->message."</li>";
                        elseif($row->confirmation)
                            $item .= "<li>". $label.' '.$trans['confirmation'].': '.$row->confirmation."</li>";
                        if(!$row->success && $hasMessage) $success = false;
                    };
                    $orginalItem = $item;
                    try
                    {
                        $item = html_entity_decode($item);
                        $item = $this->getPlainTextFromHtml($item); 
                    } 
                    catch (\Exception $ex) 
                    {
                        $item = $orginalItem;
                    }
                    return '<div>'.$type.' '.($success?$trans['update_successful']:$trans['update_error']).'<br/><ol>'.$item.'</ol></div>';
                }
            } else {
                $confirmationArray = explode("&", $confirmation);
                $confirmation2 = new \stdClass();
                foreach ($confirmationArray as $conf) {
                    $conf = explode("=", $conf);
                    if (count($conf) > 1)
                        $confirmation2->{$conf[0]} = $conf[1];
                }

                if (isset($confirmation2->sts) && $confirmation2->sts && isset($confirmation2->confMessage)) {
                    return "<div class='statusSuccess'> " . $type . " " . $trans['update_successful'] . ". <br/>" . $trans['confirmation'] . ": " . $confirmation2->confMessage . "</div>";
                } else if (isset($confirmation2->err) && trim($confirmation2->err) != "") {
                    return "<div class='statusError'> " . $type . " " . $trans['update_error'] . ". <br/>" . $trans['error'] . ": " . $confirmation2->err . "</div>";
                } else {
                    return $trans['not_applicable'];
                }
            }
        }else{
            return $trans['not_applicable'];
        }
    }

    public function getBaseUrl(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $scheme = $request->getScheme();
        $host = $request->getHttpHost();
        return sprintf('%s://%s', $scheme, $host);
    }

    public function sendProfileErrorEmail($status, $data, $send_to_client = TRUE){
            return true;
            $session = $this->session;
            $planID = $session->get('REQUEST')['id'];
            
            if(!isset($data['firstName'])){
                
               $content = "<div>An error occurred while viewing plan " . $planID .".</div><br>";

            }else{
               $content = "<div>The following Errors occurred when " . $data['firstName'] ." ". $data['lastName'] . " updated their account.</div><br>"; 
               $content .= "<br><div>You can view the profile here : <a href='".$this->getBaseUrl()."/profile/report/" . $session->get('profileId') . "'>".$this->getBaseUrl()."/profile/report/" . $session->get('profileId') . "</a></div><br><br>";               
            }
            
            if ($session->get('plan')['errorNotificationEmail'] != "" && $send_to_client){
                $emails = explode(",",$session->get('plan')['errorNotificationEmail']);
                foreach ($emails as $email){
                    $this->sendEmail($email,'info@smartplanenterprise.com','SmartPlan Enterprise Update Error', $content );
                }
            }
            //$this->sendEmail('cgillespie@vwise.com','info@smartplanenterprise.com','SmartPlan Enterprise Update Error', $content );
    }

    /**
     * Returns the Future Value of a cash flow with constant payments and interest rate (annuities)
     * Excel equivalent: FV
     */
    public function futureValue($rate, $nper, $pmt, $pv = 0, $type = 0){
        if ($rate) {
            $fv = -$pv * pow(1 + $rate, $nper) - $pmt * (1 + $rate * $type) * (pow(1 + $rate, $nper) - 1) / $rate;
        } else {
            $fv = -$pv - $pmt * $nper;
        }
        return $fv;
    }

    /**
     * Returns the constant payment (annuity) for a cash flow with a constant interest rate
     * Excel equivalent: PMT
     */
    public function payment($rate, $nper, $pv, $fv = 0, $type = 0){
        if ($rate) {
            $pmt = (-$fv - $pv * pow(1 + $rate, $nper)) / (1 + $rate * $type) / ((pow(1 + $rate, $nper) - 1) / $rate);
        } else {
            $pmt = (-$pv - $fv) / $nper;
        }
        return $pmt;
    }

}
