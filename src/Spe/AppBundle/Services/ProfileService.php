<?php
namespace Spe\AppBundle\Services;

use classes\classBundle\Entity\profilesBeneficiaries;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Session\Session;
use Spe\AppBundle\Templating\Helper\Functions;
use classes\classBundle\Entity\profiles;
use classes\classBundle\Entity\participants;
use classes\classBundle\Entity\profilesRetirementNeeds;
use classes\classBundle\Entity\profilesRiskProfile;
use classes\classBundle\Entity\profilesRiskProfileAnswers;
use classes\classBundle\Entity\profilesRiskProfileQuestions;
use classes\classBundle\Entity\profilesInvestments;
use classes\classBundle\Entity\profilesContributions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use classes\classBundle\Entity\profilesAutoIncrease;
use classes\classBundle\Entity\ProfilesAdpGraphs;
use classes\classBundle\Entity\ProfilesATBlueprint;
use classes\classBundle\Entity\ProfilesATInitialize;
class ProfileService
{
    public function __construct(EntityManager $entityManager, Session $session, Functions $functions,ContainerInterface $container){
        $this->em = $entityManager;
        $this->session = $session;
        $this->functions = $functions;
        $this->testing = $session->get('testing') ? 1 : 0;
        $this->container = $container;
    }

    public function createProfile(){
        $em = $this->em;
        $session = $this->session;
        $plan = $session->get('plan');
        $rkp = $session->get('rkp')['plan_data'];
        $flexPlan =  $session->get('flexPlan');
        $planChange = $em->getRepository('classesclassBundle:plans')->findOneBy(array('id' => $plan['id']));
        $participantUniqid = isset($session->get('rkp')['participant_uid']) && $session->get('rkp')['participant_uid'] ? $session->get('rkp')['participant_uid'] : null;
        $filterData = array(
            'userId' => $plan['userid'],
            'clientCalling' => $session->get('client_calling'),
            'planId' => $plan['id'],
            'planName' => isset($rkp->planName) ? $rkp->planName : $plan['name'],
            'firstName' => isset($rkp->firstName) ? $rkp->firstName : '',
            'lastName' => isset($rkp->lastName) ? $rkp->lastName : '',
            'maritalStatus' => isset($rkp->maritalStatus) ? $rkp->maritalStatus : '',
            'address' => isset($rkp->address) ? $rkp->address : '',
            'city' => isset($rkp->city) ? $rkp->city : '',
            'state' => isset($rkp->state) ? $rkp->state : '',
            'region' => isset($rkp->region) ? $rkp->region : '',
            'postalCode' => isset($rkp->postalCode) ? $rkp->postalCode : '',
            'dateOfBirth' => isset($rkp->dateOfBirth) ? $rkp->dateOfBirth : '',
            'asOfDate' => isset($rkp->asOfDate) ? $rkp->asOfDate : '',
            'email' => isset($rkp->email) ? $rkp->email : '',
            'ssn' => isset($rkp->partSSN) ? $rkp->partSSN : ''
        );

        $profileId = $session->get('profileId') ?  $session->get('profileId') : uniqid('SPE_');
        $gender = $filterData['firstName'] ? $this->functions->findParticipantGender($filterData['firstName']) : null;

        $participants = null;
        if($participantUniqid){
            $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('uniqid' => $participantUniqid));
        }
        if(!$participants){
            $participants = new participants();
        }
        $participants->setFirstName($filterData['firstName'] ? strtoupper($filterData['firstName']) : '');
        $participants->setLastName($filterData['lastName'] ? strtoupper($filterData['lastName']) : '');
        $participants->setAddress($filterData['address'] ? $filterData['address'] : '');
        $participants->setMaritalStatus($filterData['maritalStatus'] ? $filterData['maritalStatus'] : '');
        $participants->setBirthDate($filterData['dateOfBirth'] ? $filterData['dateOfBirth'] : '');
        $participants->setEmploymentDate($filterData['asOfDate'] ? $filterData['asOfDate'] : '');
        $participants->setGender($gender ? $gender : null);
        $participants->setEmail($filterData['email'] ? $filterData['email'] : '');
        $participants->setEmployeeId($filterData['ssn'] ? $filterData['ssn'] : '');
        $participants->setCity($filterData['city']);
        $participants->setState($filterData['state']);
        $participants->setZip($filterData['postalCode']);
        $participants->setConnection($session->get('rkp')['rkp']);
        $participants->setUniqid($participantUniqid);
        $participants->setTesting($this->testing);
        if ($plan['type'] == "smartenroll")
        $smartEnroll = 1;
        else
        $smartEnroll = 0;
        $participants->setSmartEnroll($smartEnroll);
        $em->persist($participants);
        $em->flush();

        $profile = new profiles();
        $profile->setParticipant($participants);
        $profile->setUserid($filterData['userId']);
        $profile->setPlanid($filterData['planId']);
        $profile->setClientCalling($filterData['clientCalling']);
        $profile->setPlanName($filterData['planName']);
        //$profile->setRkdData(json_encode($data));
        //$profile->setUniqid($uniqid);
        $profile->setProfilestatus(0);
        $profile->setProfileId($profileId);
        $profile->setReportDate(null);
        $profile->setFlexPlan($flexPlan);
        $request = Request::createFromGlobals();
        $profile->sessionData = json_encode($session->all());
        $profile->userAgent = $request->headers->get('User-Agent');  
        $profile->language = $this->container->get('translator')->getLocale();
        $profile->smartEnrollPartId = isset($session->get("REQUEST")["smartenrollpartid"]) ? $session->get("REQUEST")["smartenrollpartid"] : null;
        $em->persist($profile);

        if ($session->get("smart401k") !== null &&  $session->get("smart401k") == 0)
        $planChange->adviceStatus = 2;
        
        $em->flush();

        $session->set('pid', $profile->getId());
        $session->set('profileId', $profileId);
        $session->set("participantid",$participants->getId());
        
        
    }
    
    public function addRetirementNeeds($profile){
        $em = $this->em;
        $session = $this->session;
        $translator= $this->container->get('translator');
        if($profile && $session->get('retirement_needs')['retireNeedsStatus'] && $session->get('retirement_needs')['RN_CurrentYearlyIncome']){
            $profilesRetirementNeeds = $em->getRepository('classesclassBundle:profilesRetirementNeeds')->findOneBy(array('profileid' => $profile->getId()));
            if(!$profilesRetirementNeeds){
                $profilesRetirementNeeds = new profilesRetirementNeeds();
            }
            $assetTypes = [];
            if(isset($session->get('retirement_needs')['RN_AssetTypes'])){
                foreach($session->get('retirement_needs')['RN_AssetTypes'] as $as){
                    $assetTypes[] = $translator->trans($as, array(), 'retirement_needs');
                }
  
            }
            $profilesRetirementNeeds->setUserid(isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null);
            $profilesRetirementNeeds->setPlanid(isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null);
            $profilesRetirementNeeds->setProfile($profile);
            $profilesRetirementNeeds->setCurrentYearlyIncome($session->get('currentYearlyIncome'));
            $profilesRetirementNeeds->setEstimatedRetirementIncome($session->get('retirement_needs')['RN_EstimatedRetirementIncome']);
            $profilesRetirementNeeds->setEstimatedSocialSecurityIncome($session->get('retirement_needs')['RN_EstimatedSocialSecurityIncome']);
            $profilesRetirementNeeds->setReplacementIncome($session->get('retirement_needs')['RN_ReplacementIncome']);
            $profilesRetirementNeeds->setYearsBeforeRetirement($session->get('retirement_needs')['RN_NumberOfYearsBeforeRetirement']);
            $profilesRetirementNeeds->setRetirementAge($session->get('retirement_needs')['RN_RetirementAge']);
            $profilesRetirementNeeds->setInflationAdjustedReplacementIncome($session->get('retirement_needs')['RN_InflationAdjustedReplacementIncome']);
            $profilesRetirementNeeds->setCurrentPlanBalance($session->get('common')['planBalance']);
            $profilesRetirementNeeds->setOtherAssets($session->get('retirement_needs')['RN_OtherAssets']);
            $profilesRetirementNeeds->setTotalAssets($session->get('retirement_needs')['RN_TotalAssets']);
            $profilesRetirementNeeds->setAssetTypes(implode("^",$assetTypes));
            $profilesRetirementNeeds->setYearsLiveInRetirement($session->get('retirement_needs')['RN_YearsYouWillLiveInRetirement']);
            $profilesRetirementNeeds->setLifeExpectancy($session->get('retirement_needs')['RN_LifeExpectancy']);
            $profilesRetirementNeeds->setAnnualIncomeNeededInRetirement($session->get('retirement_needs')['RN_AnnualIncomeNeededInRetirement']);
            $profilesRetirementNeeds->setEstimatedSavingsAtRetirement($session->get('retirement_needs')['RN_EstimatedSavingsatRetirement']);
            $profilesRetirementNeeds->setRecommendedMonthlyPlanContribution($session->get('retirement_needs')['RN_RecommendedMonthlyPlanContribution']);
            $profilesRetirementNeeds->setPercentageOfIncomeFromPlan($session->get('retirement_needs')['RN_IncomePct']);
            $profilesRetirementNeeds->setParticipantsId($profile->participant->getId());
            $profilesRetirementNeeds->estimatedPensionIncome = $session->get('retirement_needs')['RN_EstimatedPensionIncome'];
            $em->persist($profilesRetirementNeeds);
            $em->flush();
        }
    }

    public function addRiskProfile($profile){
        $em = $this->em;
        $session = $this->session;
        $risk_profile = $session->get('risk_profile');

        if($profile && $risk_profile['riskProfileStatus'] && $risk_profile['RP_xml']){
            $profilesRiskProfile = $em->getRepository('classesclassBundle:profilesRiskProfile')->findOneBy(array('profileid' => $profile->getId()));
            if(!$profilesRiskProfile){
                $profilesRiskProfile = new profilesRiskProfile();
            }
            $profilesRiskProfile->setUserid($session->get('plan')['userid']);
            $profilesRiskProfile->setPlanid($session->get('plan')['id']);
            $profilesRiskProfile->setProfile($profile);
            $profilesRiskProfile->setXml($risk_profile['RP_xml']);
            $profilesRiskProfile->setAns($risk_profile['RP_Points']);
            $profilesRiskProfile->setPosition($risk_profile['RP_InvestorType']);
            $profilesRiskProfile->setName($risk_profile['RP_label']);
            $profilesRiskProfile->setDescription($risk_profile['RP_desc']);
            $profilesRiskProfile->setParticipantsId($profile->participant->getId());
            if (isset($risk_profile['stadionPathId'])) {
                $profilesRiskProfile->setStadionPathId($risk_profile['stadionPathId']);
            }
            $em->persist($profilesRiskProfile);
            $em->flush();
            $oldQuestions = $em->getRepository('classesclassBundle:profilesRiskProfileQuestions')->findBy(["profileid" => $profile->getId() ]);
            foreach ($oldQuestions as $question)
            {
                $em->remove($question);
            }
            $em->flush();
            
            $oldAnswers = $em->getRepository('classesclassBundle:profilesRiskProfileAnswers')->findBy(["profileid" => $profile->getId() ]);
            foreach ($oldAnswers as $answer)
            {
                $em->remove($answer);
            }
            $em->flush();
            $number = 1;
            foreach ($risk_profile['RP_Questions'] as $question)
            {
                $questionid = $question['question']['question']['id'];
                $questionLocales  = $question['question']['questionLocales'];
                foreach ($questionLocales as $questionLocale)
                {
                    $savedQuestion = new profilesRiskProfileQuestions();
                    $savedQuestion->userid = $session->get('plan')['userid'];   
                    $savedQuestion->planid = $session->get('plan')['id'];
                    $savedQuestion->profileid = $profile->getId();
                    $savedQuestion->used = $questionLocale->id == $questionid;
                    $savedQuestion->languageId = $questionLocale->languageId;
                    $savedQuestion->question = $questionLocale->question;
                    $savedQuestion->number = $number;
                    $em->persist($savedQuestion);
                    $em->flush();
                    $chosenAnswer = $question['answer']; 
                    $answers = $question['question']['answers'];
                    foreach ($answers as $answer)
                    {
                        foreach ($answer['locales'] as $locale)
                        {
                            if ($locale->languageId == $questionLocale->languageId)
                            {
                                $savedAnswer = new profilesRiskProfileAnswers();
                                $savedAnswer->userid = $session->get('plan')['userid'];   
                                $savedAnswer->planid = $session->get('plan')['id'];
                                $savedAnswer->profileid = $profile->getId();
                                $savedAnswer->questionId = $savedQuestion->id;
                                $savedAnswer->answer = $locale->answer;
                                $savedAnswer->selected = $chosenAnswer['id']== $answer['id'];
                                $savedAnswer->points = $answer['points'];
                                $savedAnswer->thpoints = $answer['thpoints'];
                                $em->persist($savedAnswer);
                            }
                        }
                    }
                    $em->flush();
                }
                $number++;
            }
        }
    }

    public function addInvestments($profile){
        $em = $this->em;
        $session = $this->session;
        $investments = $session->get('investments');

        if ($profile && $session->get('ACInitialize')['WsPlan']['AllowBlueprint'] == 'true') {
            $profilesATInitialize = $this->em->getRepository('classesclassBundle:ProfilesATInitialize')->findBy(array('profileid' => $profile->profileId));
            if($profilesATInitialize){
                foreach($profilesATInitialize as $row):
                    $em->remove($row);
                endforeach;
            }
            $profilesATBlueprint = $this->em->getRepository('classesclassBundle:ProfilesATBlueprint')->findBy(array('profileid' => $profile->profileId));
            if($profilesATBlueprint){
                foreach($profilesATBlueprint as $row):
                    $em->remove($row);
                endforeach;
            }

            $profilesATBlueprint = new ProfilesATBlueprint();
            $profilesATBlueprint->profileid = $session->get('profileId');
            $profilesATBlueprint->userid = $session->get('plan')['userid'];
            $profilesATBlueprint->planid = $session->get('plan')['id'];
            $profilesATBlueprint->wsBlueprint = json_encode($session->get('ACBluePrint')['WsBlueprint']);
            $profilesATBlueprint->wsArchitectTask = json_encode($session->get('ACBluePrint')['WsArchitectTask']);
            $profilesATBlueprint->wsIndividual = json_encode($session->get('ACBluePrint')['WsIndividual']);


            $profilesATInitialize = new ProfilesATInitialize();
            $profilesATInitialize->profileid = $session->get('profileId');
            $profilesATInitialize->userid = $session->get('plan')['userid'];
            $profilesATInitialize->planid = $session->get('plan')['id'];
            $profilesATInitialize->wsArchitectTask = json_encode($session->get('ACInitialize')['WsArchitectTask']);
            $profilesATInitialize->wsBlueprint = json_encode($session->get('ACInitialize')['WsBlueprint']);
            $profilesATInitialize->wsPlan = json_encode($session->get('ACInitialize')['WsPlan']);
            $profilesATInitialize->wsIndividual = json_encode($session->get('ACInitialize')['WsIndividual']);
            $this->em->persist($profilesATBlueprint);
            $this->em->persist($profilesATInitialize);
            $this->em->flush();
        }

        if($profile && ($investments['investmentsStatus'] || $this->session->get("investmentsGraphComplete",0) || $session->get('module')['ATArchitect'] || $this->session->get("investmentsComplete"))){
            $profilesInvestments = $em->getRepository('classesclassBundle:profilesInvestments')->findBy(array('profileid' => $profile->getId()));
            if($profilesInvestments){
                foreach($profilesInvestments as $row):
                    $em->remove($row);
                endforeach;
            }
            
            $profilesAdpGraphs = $em->getRepository('classesclassBundle:ProfilesAdpGraphs')->findBy(array('profileid' => $profile->getId()));
            foreach ($profilesAdpGraphs as $row)
                $em->remove($row);

            
            if (!empty($this->session->get("investmentsGraphComplete",0))) 
            {               
                $table = $this->session->get("investmentsGraphData")['table'];
                foreach ($table as $row)
                {
                    if ($row['total'] > 0)
                    {
                        $profilesAdpGraph = new ProfilesAdpGraphs();
                        $profilesAdpGraph->userid = $this->session->get('plan')['userid'];
                        $profilesAdpGraph->planid = $this->session->get('plan')['id'];
                        $profilesAdpGraph->profileid = $profile->getId();
                        $profilesAdpGraph->description = $row['header']['description'];
                        $profilesAdpGraph->total = $row['total'];
                        $profilesAdpGraph->color = $row['color']['color'];
                        $profilesAdpGraph->hoverColor = $row['color']['hoverColor'];
                        $this->em->persist($profilesAdpGraph);
                    }
                }                
            }            
            
            $plansPortfolios = $this->session->get("plan_portfolios");
            $plansPortfoliosNameHash = array();
            foreach ($plansPortfolios as $portfolio)
            {
                $plansPortfoliosNameHash[$portfolio['name']] = $portfolio;
            }
            foreach($this->session->get("savedFunds") as $fund)
            {
                $profilesInvestments = new profilesInvestments();
                $profilesInvestments->setUserid($this->session->get('plan')['userid']);
                $profilesInvestments->setPlanid($this->session->get('plan')['id']);
                $profilesInvestments->setProfile($profile);
                $profilesInvestments->setType($this->session->get("investmentSection"));
                $profilesInvestments->setPname("");
                if ($this->session->get("investmentSection") == "RISK")
                {
                    $profilesInvestments->setPname($this->session->get("investmentsSelectedPortfolio")['name']);
                }
                $profilesInvestments->setFname(trim($fund['name']));   
                $profilesInvestments->setPercent(trim($fund['percent']));
                $profilesInvestments->setProspectusLink(trim($fund['prospectUrl']));
                $profilesInvestments->setParticipantsId($profile->participant->id);
                $profilesInvestments->setFundFactLink($fund['customUrl']);
                $profilesInvestments->fundGroupValueId = (int)$fund['fundGroup']['id'];
                if (isset($plansPortfoliosNameHash[trim($this->session->get("investmentsSelectedPortfolio")['name'])]))
                {
                    $profilesInvestments->customid = $plansPortfoliosNameHash[trim($this->session->get("investmentsSelectedPortfolio")['name'])]['customid'];
                }
                $this->em->persist($profilesInvestments);
            }
            $profile->setInvestmentType($this->session->get("investmentSection"));
            $this->em->flush();
        }
    }
    public function addContributions($profile){
        $em = $this->em;
        $session = $this->session;
        $contributionsSession = $session->get('contributions');
        if($profile && ($session->get('contributions')['contributionsStatus'] || $session->get('ACInitialize')['WsPlan']['AllowBlueprint'] === 'true')){
            $pre = $session->get('common')['pre'];
            $roth = $session->get('common')['roth'];
            $post = $session->get('common')['post'];
            $mode = $session->get('contributions')['contribution_mode'] == 'DEFERPCT' ? 'PCT' : ($session->get('contributions')['contribution_mode'] == 'DEFERDLR' ? 'DLR' : null);
            $cmode = $session->get('contributions')['catchup_mode'] == 'DEFERPCT' ? 'PCT' : ($session->get('contributions')['catchup_mode'] == 'DEFERDLR' ? 'DLR' : null);

            $preContributionsCurrent = isset($session->get('common')['preCurrent']) ? $session->get('common')['preCurrent'] : null;
            $rothContributionsCurrent = isset($session->get('common')['rothCurrent']) ? $session->get('common')['rothCurrent'] : null;
            $postContributionsCurrent = isset($session->get('common')['postCurrent']) ? $session->get('common')['postCurrent'] : null;

            $prePct = $session->get('contributions')['C_PreTaxContributionPct'];
            $preVal = $session->get('contributions')['C_PreTaxContributionValue'];

            $rothPct = $session->get('contributions')['C_RothContributionPct'];
            $rothVal = $session->get('contributions')['C_RothContributionValue'];

            $postPct = $session->get('contributions')['C_PostTaxContributionPct'];
            $postVal = $session->get('contributions')['C_PostTaxContributionValue'];

            $precontributions = 'Not Applicable';
            $rothContributions = 'Not Applicable';
            $postContributions = 'Not Applicable';

            if($mode == 'PCT'){
                $precontributions = $pre ? $prePct: null;
                $rothContributions = $roth ? $rothPct : null;
                $postContributions = $post ? $postPct : null;
            }elseif ($mode == 'DLR'){
                $precontributions = $pre ? $preVal: null;
                $rothContributions = $roth ? $rothVal : null;
                $postContributions = $post ? $postVal : null;
            }

            if ($session->get('ACInitialize')['WsPlan']['AllowBlueprint'] === 'true') { // AT
                $repository = $em->getRepository('classesclassBundle:ATWsAccounts');
                $wsAccount = $repository->findOneBy(array('profileid' => $profile->id, 'accountType' => 'Plan'));
                $precontributions = $wsAccount->preTaxPct * 100.0;
                $rothContributions = $wsAccount->rothPct * 100.0;
                $postContributions = $post ? $postPct : null;
                $mode = 'PCT';
            }

            $catchupContribution = ($mode == 'PCT' || $mode == 'DLR') ? $session->get('contributions')['C_CatchUpContributionValue'] : null;
            $catchupContribution = $session->get('contributions')['updateCatchUp'] ? $catchupContribution : null;

            $profilesContributions = $em->getRepository('classesclassBundle:profilesContributions')->findOneBy(array('profileid' => $profile->getId()));
            if(!$profilesContributions){
                $profilesContributions = new profilesContributions();
            }
            $currentYearlyIncome = null;
            if (isset($session->get('retirement_needs')['RN_CurrentYearlyIncome'])) {
                $currentYearlyIncome = $session->get('retirement_needs')['RN_CurrentYearlyIncome'];
            }
            if ($session->get('currentYearlyIncome')) {
                $currentYearlyIncome = $session->get('currentYearlyIncome');
            }
            if (!$currentYearlyIncome) {
                $repo = $this->em->getRepository('classesclassBundle:ATBlueprintPersonalInformation');
                $info = $repo->findOneBy(array('profileid' => $profile->getId()));
                $currentYearlyIncome = $info->annualCompensation;
            }
                                
            $profilesContributions->setUserid(isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null);
            $profilesContributions->setPlanid(isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null);
            $profilesContributions->setProfile($profile);
            $profilesContributions->setCurrentYearlyIncome($currentYearlyIncome);
            $profilesContributions->setPaidFrequency(isset($session->get('contributions')['C_GetPaid']) ? $session->get('contributions')['C_GetPaid'] : null);
            $profilesContributions->setTarget(isset($session->get('retirement_needs')['RN_RecommendedMonthlyPlanContribution']) ? $session->get('retirement_needs')['RN_RecommendedMonthlyPlanContribution'] : null);
            $profilesContributions->setProjected(isset($session->get('contributions')['C_MonthlyContributionTotal']) ? $session->get('contributions')['C_MonthlyContributionTotal'] : null);
            $profilesContributions->setProjectedEmployer(isset($session->get('contributions')['C_MonthlyContributionTotalEmployer']) ? $session->get('contributions')['C_MonthlyContributionTotalEmployer'] : null);
            $profilesContributions->setMode($mode);
            $profilesContributions->setCmode($cmode);
            $profilesContributions->setPre($precontributions);
            $profilesContributions->setRoth($rothContributions);
            $profilesContributions->setPost($postContributions);
            $profilesContributions->setCatchup($catchupContribution);
            $profilesContributions->setPreCurrent($preContributionsCurrent);
            $profilesContributions->setRothCurrent($rothContributionsCurrent);
            $profilesContributions->setPostCurrent($postContributionsCurrent);
            $profilesContributions->setIsAca($session->get('common')['ACAon']);
            $profilesContributions->setIsAcaOptout($session->get('contributions')['C_ACAoptOut']);
            $this->calculateContributions($profilesContributions);
            $profilesContributions->setParticipantsId($profile->participant->getId());
            $em->persist($profilesContributions);
            $em->flush();
            $contributionsSession['C_MonthlyContributionPercent'] = $prePct + $rothPct + $postPct;
            $contributionsSession['C_MatchingPercent'] = intval(floor($contributionsSession['C_MonthlyContributionTotalEmployer']/$contributionsSession['C_MonthlyContributionTotal'] * $contributionsSession['C_MonthlyContributionPercent']));
            $this->session->set("contributions",$contributionsSession);
        }
    }
    function calculateContributions(&$contributions)
    {
        $sections = array("pre","roth","post");
        if ($contributions != null)
        {
            $daysInYear = 365.242199;
            $multipler = 1;
            switch ($contributions->paidFrequency)
            {
                case 'Bi-weekly':
                $multipler = $daysInYear/14;
                break;
                case 'Weekly':
                $multipler = $daysInYear/7;
                break;            
                case 'Monthly':
                $multipler = 12;
                break; 
                case 'Semi-monthly':
                $multipler = 24;
                break;    
                case 'Quarterly':
                $multipler = 4;
                break;
            }       
            $contributions->deferralPercent = 0;
            $contributions->deferralDollar = 0; 
            $contributions->rothPercent = 0; 
            $contributions->prePercent = 0;
            $contributions->preDollar = 0;
            $contributions->rothDollar = 0;
            $contributions->postPercent = 0;
            $contributions->postDollar = 0;
            $contributions->annualSalary = $contributions->currentYearlyIncome;
            if ($contributions->mode == "PCT")
            {
                $contributions->deferralPercent = $contributions->pre + $contributions->roth + $contributions->post;
                $contributions->prePercent = $contributions->pre;
                $contributions->rothPercent = $contributions->roth;
                $contributions->postPercent = $contributions->post;
                if ($contributions->currentYearlyIncome != 0)
                {
                    $contributions->deferralDollar =  ($contributions->deferralPercent / 100) * $contributions->currentYearlyIncome;
                    $contributions->preDollar  = ($contributions->prePercent / 100) * $contributions->currentYearlyIncome;
                    $contributions->rothDollar  = ($contributions->rothPercent / 100) * $contributions->currentYearlyIncome;
                    $contributions->postDollar  = ($contributions->postPercent / 100) * $contributions->currentYearlyIncome;
                }                
                foreach ($sections as $section)
                {
                    $field = $section."Dollar";
                    $payFrequencyField = $field."PayFrequency";
                    $contributions->$payFrequencyField = $contributions->$field/$multipler;
                }
            }
            if ($contributions->mode == "DLR")
            {
                $contributions->deferralDollar = $contributions->pre + $contributions->roth + $contributions->post;
                $contributions->preDollar = $contributions->pre;
                $contributions->rothDollar = $contributions->roth;
                $contributions->postDollar = $contributions->post;
                if ($contributions->currentYearlyIncome != 0)
                {
                    $contributions->deferralPercent = ($contributions->deferralDollar/$contributions->currentYearlyIncome) * 100;
                    $contributions->rothPercent = ($contributions->rothDollar/$contributions->currentYearlyIncome) * 100;
                    $contributions->prePercent = ($contributions->preDollar/$contributions->currentYearlyIncome) * 100;
                    $contributions->postPercent = ($contributions->postDollar/$contributions->currentYearlyIncome) * 100;
                }
                $contributions->deferralDollar = $contributions->deferralDollar * $multipler;
                $contributions->deferralPercent = $contributions->deferralPercent * $multipler;
                $contributions->rothPercent = $contributions->rothPercent * $multipler;
                $contributions->rothDollar = $contributions->rothDollar * $multipler;
                $contributions->prePercent = $contributions->prePercent * $multipler;
                $contributions->preDollar = $contributions->preDollar * $multipler;
                $contributions->postPercent = $contributions->postPercent * $multipler;
                $contributions->postDollar = $contributions->postDollar * $multipler;
            }
        }
        else
        {
            $contributions->deferralPercent = 0;
            $contributions->deferralDollar = 0; 
            $contributions->annualSalary = 0; 
            $contributions->rothPercent = 0; 
            $contributions->prePercent = 0;
            $contributions->preDollar = 0;
            $contributions->rothDollar = 0;
            $contributions->postDollar = 0;
        } 
    }
    public function addBeneficiary($profile)
    {
        $em = $this->em;
        $session = $this->session;

        $deleteBeneficiary = $em->getRepository('classesclassBundle:profilesBeneficiaries')->findBy(array('profileid' => $profile->getId()));
        foreach($deleteBeneficiary as $row)
            $em->remove($row);
            
        foreach($session->get('beneficiaryList') as $b)
        {
            $beneficiary = new profilesBeneficiaries();
            $beneficiary->setUserid(isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null);
            $beneficiary->setPlanid(isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null);
            $beneficiary->setProfile($profile);
            $beneficiary->setType(isset($b['type']) ? $b['type'] : "");
            $beneficiary->setRelationship(isset($b['relationship']) ? $b['relationship'] : "");
            $beneficiary->setPercent(isset($b['percent']) ? $b['percent'] : 0);
            $beneficiary->setFirstName(isset($b['firstName']) ? $b['firstName'] : "");
            $beneficiary->setLastName(isset($b['lastName']) ? $b['lastName'] : "");
            $beneficiary->setGender(isset($b['gender']) ? $b['gender'] :"");
            $beneficiary->setMaritalStatus(isset($b['maritalStatus']) ? $b['maritalStatus'] : "");
            $beneficiary->setSsn(isset($b['ssn']) ? $b['ssn'] : "");
            $beneficiary->setParticipantsId($profile->participant->getId());
            $beneficiary->setState(isset($b['state']) ? $b['state'] : "");
            $beneficiary->setZip(isset($b['zip']) ? $b['zip'] : "");
            $beneficiary->setDob(isset($b['dob']) ? $b['dob'] : "");
            $beneficiary->setAddress1(isset($b['address1']) ? $b['address1'] : "");
            $beneficiary->setAddress2(isset($b['address2']) ? $b['address2'] : "");
            $beneficiary->setCity(isset($b['city']) ? $b['city'] : "");
            $beneficiary->setPhone(isset($b['phone']) ? $b['phone'] : "");
            $beneficiary->setTrustEstateCharityOther(isset($b['trustEstateCharityOther']) ? $b['trustEstateCharityOther'] : 0);
            $beneficiary->setForeignaddress(isset($b['foreignaddress']) ? $b['foreignaddress'] : 0);
            $beneficiary->setCountry(isset($b['country']) ? $b['country'] : "");
            $beneficiary->setMiddleInitial(isset($b['middleInitial']) ? strtoupper($b['middleInitial']) : "");
            $beneficiary->setLevel(isset($b['level']) ? $b['level'] : "");
            $em->persist($beneficiary);
        }
        $em->flush();
    }
    
    private function fixBeneficiary(&$b)
    {
        if (!is_array($b))
        {
            $b = (array)$b;
        }
        foreach ($b as $key => $value)
        {
            if ($value == null)
            {
                unset($b[$key]);
            }
        }        
    }

    private function getBeneficiaryRelationship($code){
        switch($code){
            case '0':
                return 'Spouse';
            break;
            case '1':
                return 'Aunt';
            break;
            case '2':
                return 'Business Partner';
            break;
            case '3':
                return 'Daughter';
            break;
            case '4':
                return 'Father';
            break;
            case '5':
                return 'Friend';
            break;
            case '6':
                return 'Father In Law';
            break;
            case '7':
                return 'Grandchild';
            break;
            case '8':
                return 'Grandfather';
            break;
            case '9':
                return 'Grandmother';
            break;
            case 'A':
                return 'Mother';
            break;
            case 'B':
                return 'Mother In Law';
            break;
            case 'C':
                return 'Niece Nephew';
            break;
            case 'D':
                return 'Step Daughter';
            break;
            case 'E':
                return 'Step Father';
            break;
            case 'F':
                return 'Step Mother';
            break;
            case 'G':
                return 'Step Son';
            break;
            case 'H':
                return 'Sister Brother';
            break;
            case 'I':
                return 'Son';
            break;
            case 'J':
                return 'Uncle';
            break;
            case 'K':
                return 'Charity';
            break;
            case 'L':
                return 'Church';
            break;
            case 'M':
                return 'Creditor';
            break;
            case 'N':
                return 'Employer';
            break;
            case 'O':
                return 'Estate';
            break;
            case 'P':
                return 'School';
            break;
            case 'Q':
                return 'Trust';
            break;
            case 'R':
                return 'Other';
            break;
            case 'S':
                return 'Originator';
            break;
            case 'T':
                return 'Domestic Partner';
            break;
            default:
                return null;
            break;
        }
    }

    public function updateProfile($saveDate = 1){
        $em = $this->em;
        $session = $this->session;
        $profileError = $isError = false;
        $status = array(
            'investmentsConfirmationStatus',
            'realignmentConfirmationStatus',
            'contributionsConfirmationStatus',
            'catchupContributionConfirmationStatus',
            'ATTransactStatus',
            'beneficiaryConfirmationStatus',
            'profileConfirmationStatus'
        );

        $profileId = $session->get('profileId') ?  $session->get('profileId') : uniqid('SPE_');
        $data = array(
            'userId' => isset($session->get('plan')['userid']) ? $session->get('plan')['userid'] : null,
            'planId' => isset($session->get('rkp')['plan_id']) ? $session->get('rkp')['plan_id'] : null,
            'planName' => isset($session->get('rkp')['plan_data']->planName) ? $session->get('rkp')['plan_data']->planName : null,
            'phone' => isset($session->get('common')['phone']) ? $session->get('common')['phone'] : null ,
            'availability' => isset($session->get('common')['availability']) ? $session->get('common')['availability'] : null ,
            'profilestatus' => 1,
            'profileId' => $profileId,
            'firstName' => $session->get('common')['firstName'] ? strtoupper($session->get('common')['firstName']) : null,
            'lastName' => $session->get('common')['lastName'] ? strtoupper($session->get('common')['lastName']) : null,
            'dateOfBirth' => $session->get('common')['dateOfBirth'] ? $session->get('common')['dateOfBirth'] : null,
            'dateOfHire' => $session->get('common')['dateOfHire'] ? $session->get('common')['dateOfHire'] : null,
            'marketingOptIn' => $session->get('common')['marketing_opt_in'] ? 1 : 0,
            'email' => $session->get('common')['email'] ? $session->get('common')['email'] : null,
            'ssn' => isset($session->get('common')['ssn']) && $session->get('common')['ssn'] ? $session->get('common')['ssn']: null,
            'reportDate' => date("Y-m-d H:i:s"),
            'legalName' => $session->get('common')['firstName'] ? $session->get('common')['firstName'] : null . ' ' . $session->get('common')['lastName'] ? $session->get('common')['lastName'] : null,
            'planBalance' => isset($session->get('common')['planBalance']) ? $session->get('common')['planBalance'] : null,
            'address' => isset($session->get('common')['address']) && $session->get('common')['address'] ? $session->get('common')['address'] : null,
            'city' => isset($session->get('common')['city']) ? $session->get('common')['city'] : null,
            'state' => isset($session->get('common')['state']) ? $session->get('common')['state'] : null,
            'zip' => isset($session->get('common')['postalCode']) ? $session->get('common')['postalCode'] : null,
            'maritalStatus' => isset($session->get('common')['maritalStatus']) && $session->get('common')['maritalStatus'] ? $session->get('common')['maritalStatus'] : null,
            'gender' => isset($session->get('common')['gender']) ? $session->get('common')['gender'] : null,
            'asOfDate' => isset($session->get('common')['asOfDate']) && $session->get('common')['asOfDate'] ? $session->get('common')['asOfDate'] : null,
            'investmentsConfirmationStatus' => $session->get('investmentsConfirmationStatus'),
            'realignmentConfirmationStatus' => $session->get('realignmentConfirmationStatus'),
            'contributionsConfirmationStatus' => $session->get('contributionsConfirmationStatus'),
            'catchupContributionConfirmationStatus' => $session->get('catchupContributionConfirmationStatus'),
            'ATTransactStatus' => $session->get('ATTransactStatus'),
            'beneficiaryConfirmationStatus' => $session->get('beneficiaryConfirmationStatus'),
            'ACAOptOutConfirmationStatus' => $session->get('ACAOptOutConfirmationStatus'),
            'enrollmentConfirmationStatus' => $session->get('enrollmentConfirmationStatus'),
        );

        $data['smart401kStatus'] = '';
        if($session->get('smart401k_completed')){
            $data['smart401kStatus'] = $this->functions->postSmart401k();
        }

        $data['ACARequestResponse'] = $session->get('ACARequestResponse');

        try{
            $session->set('profileConfirmationStatus', null);
            if($session->get('profileId') && $profileId){
                $profiles = $em->getRepository('classesclassBundle:profiles')->findOneBy(array('profileId' => $profileId));
                $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('id' => $profiles->getParticipant()));
            }else{
                $participantUniqid = isset($session->get('rkp')['participant_uid']) && $session->get('rkp')['participant_uid'] ? $session->get('rkp')['participant_uid'] : null;
                $participants = null;
                if($participantUniqid){
                    $participants = $em->getRepository('classesclassBundle:participants')->findOneBy(array('uniqid' => $participantUniqid));
                }
                if(!$participants){
                    $participants = new participants();
                }
                $profiles = new profiles();
            }

            $gender = $data['gender'] ? $data['gender'] : ($session->get('common')['firstName'] ? $this->functions->findParticipantGender($session->get('common')['firstName']) : null);

            $participants->setFirstName($data['firstName']);
            $participants->setLastName($data['lastName']);
            $participants->setBirthDate($data['dateOfBirth']);
            $participants->setHireDate($data['dateOfHire']);
            $participants->setMarketingOptIn($data['marketingOptIn']);
            $participants->setGender($gender ? $gender : null);
            $participants->setEmail($data['email']);
            $participants->setLegalName(trim($data['legalName']) ? $data['legalName'] : null);
            $participants->setAddress($data['address']);
            $participants->setMaritalStatus($data['maritalStatus']);
            $participants->setEmploymentDate($data['asOfDate']);
            $participants->setEmployeeId($data['ssn']);
            $participants->setCity($data['city']);
            $participants->setState($data['state']);
            $participants->setZip($data['zip']);
            $participants->setPhone($data['phone']);
            $participants->setTesting($this->testing);
            $em->persist($participants);
            $em->flush();

            $profiles->setParticipant($participants);
            $profiles->setUserid($data['userId']);
            $profiles->setPlanid($data['planId']);
            $profiles->setPlanName($data['planName']);
            $profiles->setAvailability($data['availability']);
            $profiles->setProfilestatus($data['profilestatus']);
            $profiles->setClientCalling($session->get('client_calling'));
            $profiles->setCurrentBalance($data['planBalance']);
            $profiles->setAvailability($data['availability']);
            $profiles->setInvestmentsStatus($data['investmentsConfirmationStatus']);
            $profiles->setRealignmentStatus($data['realignmentConfirmationStatus']);
            $profiles->setContributionsStatus($data['contributionsConfirmationStatus']);
            $profiles->setCatchupContributionStatus($data['catchupContributionConfirmationStatus']);
            $profiles->setATTransactStatus($data['ATTransactStatus']);
            $profiles->setBeneficiaryStatus($data['beneficiaryConfirmationStatus']);
            $profiles->setSmart401kStatus($data['smart401kStatus']);
            $profiles->setACAOptOutStatus($data['ACARequestResponse']);
            $profiles->setEnrollmentStatus($data['enrollmentConfirmationStatus']);
            if ($saveDate)
            {
                $profiles->setReportDate(new \DateTime());
            }
            $profiles->setProfilestatus($data['profilestatus']);
            $profiles->setProfileId($profileId);
            $profiles->setECOMMModal($session->get("common")['edelivery']);
            $request = Request::createFromGlobals();
            $profiles->sessionData = json_encode($session->all());
            $profiles->userAgent = $request->headers->get('User-Agent');
            $profiles->smartEnrollPartId = isset($session->get("REQUEST")["smartenrollpartid"]) ? $session->get("REQUEST")["smartenrollpartid"] : null;
            $em->persist($profiles);
            $em->flush();

            if($profiles){
                $this->addRetirementNeeds($profiles);
                $this->addRiskProfile($profiles);
                $this->addInvestments($profiles);
                $this->addContributions($profiles);
                $this->addBeneficiary($profiles);
                $this->addAutoIncrease($profiles);
            }
        }catch (Exception $e){
            $session->set('profileConfirmationStatus', json_encode(array('success' => 0, 'message' => 'There was an error saving the Investor Profile.')));
            $profileError = true;
        }
        $session->set('profileId', $profileId);

        if($this->hasError($status)){
            $isError = true;
            if(isset($session->get('plan')['errorNotificationEmail']) && $session->get('plan')['errorNotificationEmail']){
                $this->functions->sendProfileErrorEmail($status, $data);
            }
        }

        if(!$profileError){
            $resp = array('success' => 2, 'message' => 'Profile ID: ', 'confirmation' => $profileId);
        }else{
            $resp = array('success' => 0, 'message' => 'Error Saving Profile');
        }
        return json_encode($resp);
    }

    private function hasError($status){
        $session = $this->session;
        foreach($status as $d){
            $s = json_decode($session->get($d));
            if(isset($s->success) && !$s->success){
                return true;
            }
        }
        return false;
    }
    
    public function addAutoIncrease($profile) {
        $repository = $this->em->getRepository('classesclassBundle:profilesAutoIncrease');
        $profilesAutoIncreaseRows = $repository->findBy(array('profileid' => $profile->getId()));
        foreach($profilesAutoIncreaseRows as $row){
            $this->em->remove($row);
        }
        
        if ($this->session->has("autoIncreaseSave")) {
            $autoIncreaseSave = $this->session->get("autoIncreaseSave");
            foreach ($autoIncreaseSave['deferrals'] as $deferral) {
                $profilesAutoIncrease = new profilesAutoIncrease();
                $profilesAutoIncrease->userid = $this->session->get('plan')['userid'];
                $profilesAutoIncrease->planid = $this->session->get('plan')['id'];
                $profilesAutoIncrease->profile = $profile;
                $profilesAutoIncrease->source = $deferral['source'];
                $profilesAutoIncrease->amount = $deferral['value'];
                $profilesAutoIncrease->mode = $autoIncreaseSave['mode'];
                if (!empty($deferral['date'])) {
                    $profilesAutoIncrease->startDate = new \DateTime($deferral['date']);
                }
                if (!empty($this->session->get("autoIncrease")['frequency'])) {
                    $profilesAutoIncrease->frequency = $this->session->get("autoIncrease")['frequency'];
                }
                if (!empty($this->session->get("autoIncrease")['enddate'])) {
                    $profilesAutoIncrease->endDate = new \DateTime($this->session->get("autoIncrease")['enddate']);
                }
                if (!empty($this->session->get("autoIncrease")['occurrences'])) {
                    $profilesAutoIncrease->occurrences = $this->session->get("autoIncrease")['occurrences'];
                }
                
                $this->em->persist($profilesAutoIncrease);
            }
        }
        $this->em->flush();
    }
    public function getProfileReportDatabaseData($profileId)
    {
        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder->select('profiles','plans','accounts','defaultModules','plansModules','profilesRetirementNeeds','profilesContributions','profilesRiskProfile');     
        $queryBuilder->from('classesclassBundle:profiles', 'profiles');
        $queryBuilder->where('profiles.profileId = :profileId');
        $joinedTables  = 
        [
            [
                "name" => "plans",
                "joinBy" => 'plans.id = profiles.planid'              
            ],
            [           
                "name" => "accounts",
                "joinBy" => 'accounts.id = profiles.userid'              
            ],
            [           
                "name" => "defaultModules",
                "joinBy" => 'defaultModules.userid = profiles.userid'              
            ], 
            [           
                "name" => "plansModules",
                "joinBy" => 'plansModules.planid = profiles.planid'              
            ],
            [           
                "name" => "profilesRetirementNeeds",
                "joinBy" => 'profilesRetirementNeeds.profileid = profiles.id',
            ],   
            [           
                "name" => "profilesContributions",
                "joinBy" => 'profilesContributions.profileid = profiles.id',
            ], 
            [           
                "name" => "profilesRiskProfile",
                "joinBy" => 'profilesRiskProfile.profileid = profiles.id',
            ],  
        ];
        $this->joinByArray($queryBuilder,$joinedTables);
        $queryBuilder->setParameter('profileId', $profileId);
        $result = $queryBuilder->getQuery()->getResult();
        $data = [];
        $i = 0;
        $data['profiles'] = $result[$i++];
        $data['participants'] = $data['profiles']->participant;
        foreach ($joinedTables as $table)
            $data[$table['name']] = $result[$i++];   
        
        $arrayTables = ["ProfilesAdpGraphs","profilesBeneficiaries","profilesAutoIncrease"];
        foreach ($arrayTables as $table)
            $data[$table] = $this->getProfileRows($table, $data['profiles']->id);         
        $data['investments'] = $this->getInvestments($data['profiles']->id);
        $data['riskQuestions']  = $this->getRiskProfileQuestions($data['profiles']->id);
        if (empty($data['riskQuestions']))
            $data['riskQuestions'] = $this->getRiskProfileQuestionsBackwardsCompatible($data);
        $data['investorProfileDisclosure'] = $this->getInvestorProfileDisclosure($data['profiles']->planid,$data['profiles']->language);
        $data['documents'] = $this->em->getRepository('classesclassBundle:plansDocuments')->findBy(array('planid' => $data['profiles']->planid));
        return $data;
    }
    private function joinByArray($queryBuilder,$joinedTables)
    {
        foreach ($joinedTables as $table)
        {
            $queryBuilder->leftJoin(
               'classesclassBundle:'.$table['name'],
                $table['name'],
                \Doctrine\ORM\Query\Expr\Join::WITH,
                $table['joinBy']
            );              
        }        
    }
    private function formatResult($tables,$results)
    {
        $table = reset($tables);
        $return = [];
        $params = [];
        foreach ($results as $result)
        {
            $className = explode('\\',get_class ($result));
            $className = end($className);
            if (in_array($className,$tables))
            {
                if (isset($params[$className]))
                {
                    $return[] = $params;
                    $params = [];
                }
                $params[$className] = $result;
            }
        }
        if ($params != [])
        {
            $return[] = $params;
        }
        foreach ($return as $row)
            foreach($tables as $table)
                if (!isset($row[$table]))
                    $row[$table] = null;       
        return $return;
    }
    public function getProfileReport($profileId,$showPersonalInformation = 0,$ajax = 0)
    {
        $params = [];
        $data  = $this->getProfileReportDatabaseData($profileId);
        $params['reportDate'] = $data['profiles']->reportDate;
       
        $this->profileReportSetPersonalInfo($params,$data);
        $this->profileReportSetContributions($params,$data);
        $this->profileReportSetInvestments($params,$data);
        $this->profileReportSetRiskProfile($params,$data);
        $this->profileReportSetRetirementNeeds($params,$data);
        $this->profileReportSetMessaging($params,$data);
        $this->profileReportSetDocuments($params,$data);
        $this->profileReportSetBeneficiaries($params, $data);
        $this->profileReportSetConfirmationCodes($params,$data);
        $this->profileReportSetAutoIncrease($params,$data);
        $params['logo'] = 
        [
            "show" => !empty($data['plans']->providerLogoImage),
            "url" => $data['plans']->providerLogoImage
        ];
        $params['ajax'] = $ajax;  
        $params['showPersonalInformation'] = $data['plans']->profileNotificationShowEmail || $showPersonalInformation;
        $params['template'] = $params['ajax']  ? "report-inner.html.twig" : "report.html.twig";
        
        return $params;
    }
    private function formatDollar($value)
    {
        return "$".number_format($value,0);
    }
    public function profileReportSetPersonalInfo(&$params,$data)
    {
        $email =!empty($data['participants']->preferredEmail) ? $data['participants']->preferredEmail: !empty($data['participants']->email) ? $data['participants']->email:"";
        $params['personalInfo'] =
        [
            "planName" => $data['plans']->name,
            "planId" => $data['plans']->planid,
            "name" => trim(strtoupper($data['participants']->firstName)." ".strtoupper($data['participants']->lastName)),
            "email" => $email,
            "reachMeAvailable" => $data["plans"]->IRSCode == "403b" && !empty($email) && !empty($data['participants']->phone) && !empty($data['profiles']->availability),
            "phone" => $data['participants']->phone,
            "avability" => $data['profiles']->availability
        ];
    }
    public function profileReportSetContributions(&$params,$data)
    {
        $params['contributions'] =
        [
            "balance" => $data['profiles']->currentBalance,
            "income" => (!empty($data['profilesContributions']->currentYearlyIncome)) ? $data['profilesContributions']->currentYearlyIncome: (float)$data['profilesRetirementNeeds']->currentYearlyIncome,
            "monthlyContribution" => $data['profilesContributions']->projected,
            "projectedEmployer" =>
            [
                "show" => !empty($data['profilesContributions']->projectedEmployer),
                "value" => $data['profilesContributions']->projectedEmployer
            ],
            "catchupShow" => !($data['profilesContributions']->catchup === null),
        ];
        if ($params['contributions']['catchupShow'])
            $params['contributions']['catchupFormatted'] = $data['profilesContributions']->cmode == "PCT" ? $data['profilesContributions']->catchup."%" : "$".$data['profilesContributions']->catchup;
        foreach (["pre","roth","post"]as $section)
        {
            $params['contributions'][$section."Show"] = $data['profilesContributions']->{$section} > 0;
            $params['contributions'][$section."Formatted"] = $data['profilesContributions']->mode == "PCT" ? $data['profilesContributions']->{$section}."%" : "$".$data['profilesContributions']->{$section};
        }      
    }
    public function profileReportSetInvestments(&$params,$data)
    {
        $translationSections = 
        [
            "risk" => 
            [
                "header" => 
                [
                    "key" => "risk_title_default",
                    "source" => "investments"
                ]
            ],
            "custom" => 
            [
                "header" => 
                [
                    "key" => "custom_choice",
                    "source" => "investments"
                ]
            ],
            "target" => 
            [
                "header" => 
                [
                    "key" => "target_title",
                    "source" => "investments"
                ]
            ],
            "advice" => 
            [
                "header" => 
                [
                    "key" => "advice_fund",
                    "source" => "investments"
                ]
            ],
            "default" => 
            [
                "header" => 
                [
                    "key" => "default_investment",
                    "source" => "investments"
                ]
            ],
        ];
        
        //start dirty logic
        if ($data['profiles']->clientCalling == "Envoy")
        {
            $translationSections['risk']['header'] =
            [
                "key" => "account_managed_for_you",
                "source" => "investments"
            ];
            $translationSections['custom']['header'] =
            [
                "key" => "custom_title_envoy",
                "source" => "investments"
            ];            
        }
        else if ($data['profiles']->clientCalling == "AXA")
        {
            $translationSections['risk']['header'] =
            [
                "key" => "risk_title_axa",
                "source" => "investments"
            ];
            $translationSections['target']['header'] =
            [
                "key" => "target_axa_title",
                "source" => "investments"
            ];               
        }
        else if ($data['profiles']->clientCalling == "LNCLN")
        {
            $translationSections['risk']['header'] =
            [
                "key" => "risk_title_lncln",
                "source" => "investments"
            ];               
        }  
        else if ($data['plans']->riskType == "RBF")
        {
            $translationSections['risk']['header'] =
            [
                "key" => "risk_title_rbf",
                "source" => "investments"
            ];               
        }            
        //end dirty logic
        $firstInvestment = $data['investments'][0]['profilesInvestments'];
        $investmentsComplete = true;
        if ( empty($firstInvestment))
        {
            $firstInvestment = new profilesInvestments();
            $investmentsComplete = false;
        }
        
        $params['investments'] = 
        [
            "type" => trim(strtolower($firstInvestment->type)),
            "header" => $this->fundsHeader($data),
            "file" => "investments",
            "portfolioName" => $firstInvestment->pname,
            "complete" => $investmentsComplete
        ];  
              
        $params['investments']['translations'] = 
        [
            "section" => $translationSections[$params['investments']['type']]            
        ];
        $connectionType = strtolower($data['accounts']->connectionType );
        if (substr( $connectionType, 0, 4 ) == "omni")
            $params['investments']['file'] = "omniInvestments";
        
        if (!empty($data['ProfilesAdpGraphs']))
        {
             $params['investments']['type'] = "adp";
             $params['investments']['file'] = "adpinvestments";
        }
        if (!empty($data['plansModules']->ATBlueprint))
        {
            $params['investments']['complete'] = true;
            $params['investments']['type'] = "at";
            $params['investments']['file'] = "atinvestments";
            $params['investments']['profile'] = $data['profiles'];
        }
        if ($params['investments']['type'] == "stadion") {
            $params['investments']['file'] = "stadion";
            if ($this->session->get('risk_profile')['retireExpress']) {
                $params['investments']['file'] = "stadionRetireExpress";
            }
        }
        $params['investments']['funds'] = $data['investments'];
        $params['investments']['adpGraphs'] = $data['ProfilesAdpGraphs'];        
    }
    public function profileReportSetRiskProfile(&$params,$data)
    {        
        $thPoints = 0;
        foreach ($data['riskQuestions'] as $riskQuestion)
        {
            $thPoints += $riskQuestion['profilesRiskProfileAnswers']->thpoints;
        }
        $params['riskProfile'] = 
        [
            "complete" => !empty($data['riskQuestions']),
            "riskQuestions" => $data['riskQuestions'],
            "profileBar" => "/images/profilebar_".$data['profilesRiskProfile']->position.".gif",
            "file" => "riskprofile",
            "totalPoints" => array_sum(explode("|",$data['profilesRiskProfile']->ans))
        ];       
        if ($data['profiles']->clientCalling == "AXA")
        {           
            if ($data['profilesRiskProfile']->xml == "riskProfileAXA2013")
            {
                $params['riskProfile']['profileBar'] = "/images/profile/axa2013/".$data['profilesRiskProfile']->position.".png";
            }
            else
            {
                $params['riskProfile']['profileBar'] = "/images/profile/axa/".$data['profilesRiskProfile']->position.".png";
                $params['riskProfile']['file'] = "riskprofileaxa";
            }
        }
        elseif ($data['profilesRiskProfile']->xml == "riskProfileLNCLN7")
        {
            $params['riskProfile']['profileBar'] = "/images/profile/lncln7/".$data['profilesRiskProfile']->position."_$thPoints.png";
        }
        elseif (!empty($data['plansModules']->investmentsGraph))       
            $params['riskProfile']['file'] = "personalInvestorProfile";
        
        
        $riskLabels = ["conservative","moderately_conservative","moderate","moderately_aggressive","aggressive"];
        $params['riskProfile']['translations'] =
        [
            "investorLabel" =>
            [
                "key" => $riskLabels[$data['profilesRiskProfile']->position],
                "source" => "investments"                
            ],
            "investorDescription" => 
            [
                "key" => "investor_description_".($data['profilesRiskProfile']->position+1),
                "source" => "investments"
            ]
        ];         
    }
    public function profileReportSetRetirementNeeds(&$params,$data)
    {
        $params['retirementNeeds'] = 
        [
            "complete" => !empty($data['profilesRetirementNeeds']),
            "recommendedMonthlyPlanContribution" => $this->formatDollar($data['profilesRetirementNeeds']->recommendedMonthlyPlanContribution)
        ];
        $retireQuestions = 
        [   
            "income" =>
            [
                "key" => "rn_question_1",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->currentYearlyIncome)
            ],
            "retirementIncome" => 
            [
                "key" => "rn_question_2",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->estimatedRetirementIncome)
            ],
            "socialSecurityIncome" => 
            [
                "key" => "rn_question_3",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->estimatedSocialSecurityIncome)
            ], 
            "pensionIncome" => 
            [
                "key" => "rn_question_3a",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->estimatedPensionIncome)
            ], 
            "replacementIncome" => 
            [
                "key" => "rn_question_4",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->replacementIncome)
            ], 
            "numberOfYearsBeforeRetirement" => 
            [
                "key" => "rn_question_5a",
                "source" => "alert",
                "value" => $data['profilesRetirementNeeds']->yearsBeforeRetirement
            ], 
            "inflation_adjusted_replacement_income" => 
            [
                "key" => "rn_question_6",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->inflationAdjustedReplacementIncome)
            ], 
            "planBalance" => 
            [
                "key" => "rn_question_7",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->currentPlanBalance)
            ], 
            "otherAssets" => 
            [
                "key" => "rn_question_8",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->otherAssets)
            ], 
            "totalAssets" => 
            [
                "key" => "rn_question_9",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->totalAssets)
            ], 
            "assetTypes" => 
            [
                "key" => "rn_question_10",
                "source" => "alert",
                "value" => str_replace("^","<br/>", $data['profilesRetirementNeeds']->assetTypes)
            ], 
            "yearsInRetirement" => 
            [
                "key" => "rn_question_11a",
                "source" => "alert",
                "value" => $data['profilesRetirementNeeds']->yearsLiveInRetirement
            ], 
            "annualIncomeNeededInRetirement" => 
            [
                "key" => "rn_question_12",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->annualIncomeNeededInRetirement)
            ], 
            "estimatedSavingsAtRetirement" => 
            [
                "key" => "rn_question_13",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->estimatedSavingsAtRetirement)
            ], 
            "recommendedPlanContribution" => 
            [
                "key" => "rn_question_14",
                "source" => "alert",
                "value" => $this->formatDollar($data['profilesRetirementNeeds']->recommendedMonthlyPlanContribution)
            ], 
            "percentageOfIncomeFromPlan" => 
            [
                "key" => "rn_question_15",
                "source" => "alert",
                "value" => $data['profilesRetirementNeeds']->percentageOfIncomeFromPlan."%"
            ], 
        ];
        $params['retirementNeeds']['questions'] = $retireQuestions;    
        if ($data['profiles']->trustedContactStatus !== null)
        {
            $params['retirementNeeds']['trustedContactEnabled'] = true;
            if ($data['profiles']->trustedContactStatus == 1)
                $params['retirementNeeds']['translations']['trustedContactStatus']  = 
                [
                    "key" => "yes",
                    "source" =>  "global"
                ]; 
            elseif (!$data['profiles']->trustedContactStatus)
                $params['retirementNeeds']['translations']['trustedContactStatus']  = 
                [
                    "key" => "no",
                    "source" =>  "global"
                ]; 
            else
                $params['retirementNeeds']['translations']['trustedContactStatus']  = 
                [
                    "key" => "error",
                    "source" =>  "global"
                ]; 
        }
        else       
            $params['retirementNeeds']['trustedContactEnabled'] = false;
    }
    public function profileReportSetMessaging(&$params,$data)
    {
        $params['investorProfileDisclosure'] =
        [
            "show" => !empty($data['investorProfileDisclosure']->message),
            "message" => $data['investorProfileDisclosure']->message
        ];        
    }
    public function profileReportSetDocuments(&$params,$data)
    {
        $params['documents'] =
        [
            "show" => !empty($data['documents']),
            "list" => $data['documents']
        ];        
    }
    public function getInvestorProfileDisclosure($planid,$language)
    {
        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder->select('PlansMessages');     
        $queryBuilder->from('classesclassBundle:PlansMessages', 'PlansMessages');
        $joinedTables  = 
        [
            [
                "name" => "messagesPanes",
                "joinBy" => 'messagesPanes.id = PlansMessages.messagesPanesId'              
            ]
        ];
        $this->joinByArray($queryBuilder,$joinedTables);
        $queryBuilder->where('PlansMessages.planid = :planid and messagesPanes.name = :name and PlansMessages.language = :language');

        $queryBuilder->setParameter('planid', $planid);
        $queryBuilder->setParameter('name', 'investorProfileDisclosure');
        $queryBuilder->setParameter('language', $language);
        try
        {
        
            return $queryBuilder->getQuery()->getSingleResult();
        }
        catch(\Exception $e)
        {
            return null;
        }
        
    }
    public function profileReportSetBeneficiaries(&$params,$data)
    {
        $connectionType = strtolower($data['accounts']->connectionType );
        $params['beneficiaries'] = 
        [
            "show" => !empty($data['profilesBeneficiaries']),
            "beneficiaries" => $data['profilesBeneficiaries'],
            "header" => $connectionType != "relius" ? $this->beneficiariesHeader($data):$this->beneficiariesHeaderRelius($data),
            "spouseWaiverFormUrl" => $data['plansModules']->spousalWaiverURL,
            "showSpouseWaiverFormUrl" => !empty($data['plansModules']->spousalWaiverURL)
        ];      
    }
    public function profileReportSetAutoIncrease(&$params,$data)
    {
        $sourceMappingTranslation = 
        [
            "A" => 
            [
                "key" => "pretax",
                "source" => "widget"
            ],
            "B" => 
            [
                "key" => "roth",
                "source" => "widget"
            ]
        ];
        $autoIncreaseFullHeader = 
        [
            "amount" =>
            [
                "field" => "amount",
                "translation" => 
                [
                    "title" => "amount",
                    "title_source" => "my_profile"
                ]
            ], 
            "source" =>
            [
                "field" => "source",
                "translation" => 
                [
                    "title" => "source",
                    "title_source" => "my_profile"
                ]
            ], 
            "startDate" =>
            [
                "field" => "startDate",
                "translation" => 
                [
                    "title" => "start_date",
                    "title_source" => "my_profile"
                ]
            ], 
            "frequency" =>
            [
                "field" => "frequency",
                "translation" => 
                [
                    "title" => "frequency",
                    "title_source" => "my_profile"
                ]
            ], 
            "occurrences" =>
            [
                "field" => "occurrences",
                "translation" => 
                [
                    "title" => "occurrences",
                    "title_source" => "my_profile"
                ]
            ], 
            "endDate" =>
            [
                "field" => "endDate",
                "translation" => 
                [
                    "title" => "end_date",
                    "title_source" => "my_profile"
                ]
            ], 
        ];
        foreach (["amount","source"] as $key)
            $autoIncreaseHeader[$key] = $autoIncreaseFullHeader[$key];
        foreach ($data['profilesAutoIncrease'] as &$autoIncrease)
        {
            $autoIncrease->sourceTranslated = $sourceMappingTranslation[$autoIncrease->source];
            $autoIncrease->amountFormatted = $autoIncrease->mode == "percent" ? $autoIncrease->amount."%": "$".$autoIncrease->amount;
            
            foreach ($autoIncreaseFullHeader as $key => $header )
                if (!isset($autoIncreaseHeader[$key]) && !empty($autoIncrease->$key))
                    $autoIncreaseHeader[$key] = $autoIncreaseFullHeader[$key];       
        }   
        $params['autoIncrease'] = 
        [
            "complete" => !empty($data['profilesAutoIncrease']),
            "header" => $autoIncreaseHeader,
            "autoIncreases" => $data['profilesAutoIncrease']
        ];
    }
    public function profileReportSetConfirmationCodes(&$params,$data)
    {
        $translations = 
         [
            "statusSuccess" => 
            [
                "key" => "update_successful",
                "source" => "alert",
            ],
            "statusFail" => 
            [
                "key" => "update_failed",
                "source" => "alert",
            ],    
        ];
        $confirmationCodes = 
        [
            "investments" => 
            [
                "field" => "investmentsStatus",
                "displayField" => "Investment",
                "translations" => 
                [
                    "title" => 
                    [
                        "key" => "investment_selections",
                        "source" => "my_profile",
                    ]
                ]
            ],
            "contributions" => 
            [
                "field" => "contributionsStatus",
                "displayField" => "Contribution",
                "translations" => 
                [
                    "title" => 
                    [
                        "key" => "contribution_amount_update",
                        "source" => "my_profile",
                    ]
                ]
            ],  
            "catchupContributions" =>
            [
                "field" => "catchupContributionStatus",
                "displayField" => "Contribution Catch Up ",
                "translations" => 
                [
                    "title" => 
                    [
                        "key" => "contribution_catch_up_update",
                        "source" => "my_profile",
                    ]
                ]  
            ],
            "realignment" => 
            [
                "field" => "realignmentStatus",
                "displayField" => "Realignment",
                "translations" => 
                [
                    "title" => 
                    [
                        "key" => "realignment_update",
                        "source" => "my_profile",
                    ]
                ]
            ],  
            "enrollmentStatus" => 
            [
                "field" => "enrollmentStatus",
                "displayField" => "Enrollment",
                "translations" => 
                [
                    "title" => 
                    [
                        "key" => "enrollment_update",
                        "source" => "my_profile",
                    ]
                ]                
            ],
            "beneficiaryStatus" => 
            [
                "field" => "beneficiaryStatus",
                "displayField" => "Beneficiary",
                "translations" => 
                [
                    "title" => 
                    [
                        "key" => "beneficiary_update",
                        "source" => "my_profile",
                    ]
                ]                
            ]
        ];
        
        $connectionType = strtolower($data['accounts']->connectionType );
        //really dirty logic below
        if ($connectionType == "omniamrts")
        {
            foreach(["contributions","enrollmentStatus","catchupContributions"] as $field)
                $confirmationCodes[$field]['field'] = "investmentsStatus";
        }
        if (in_array($connectionType,["omniinspty","alerus"]))
        {
            $confirmationCodes = 
            [
                "investments" => $confirmationCodes['investments']
            ];
        }
        if (in_array($connectionType,["alerus","dst"]))
        {
            $confirmationCodes['investments']['translations']['title']['key'] ="selections_update";
            $confirmationCodes['investments']['displayField'] = "Selections";
        }
        //end dirty logic
        $params['confirmationCodes'] = [];
        $confirmationCodesPass = [];
        foreach ($confirmationCodes as $key => $confirmationCode)
        {
           $status = json_decode($data['profiles']->{$confirmationCode['field']});
           if (isset($status->success)   && isset($status->confirmation))
           {
               $confirmationCodes[$key]['confirmation'] = $status->confirmation;
               $confirmationCodes[$key]['translations']['status'] = $status->success ? $translations['statusSuccess']:$translations['statusFail'];
               $confirmationCodesPass[] = $confirmationCodes[$key];
           }
        }
        $params['confirmationCodes'] =
        [
            "transactions" => $confirmationCodesPass,
            "investorProfileCode" => $data['profiles']->profileId,
            "complete" => !empty($data['profiles']->reportDate),
            "rolloverEnabled" => !empty($data['profiles']->callUs) || !empty($data['profiles']->callMe) ||  !empty($data['profiles']->emailMe),
            "callUsEnabled" => !empty($data['profiles']->callUs),
            "callMeEnabled" => !empty($data['profiles']->callMe),
            "emailMeEnabled" => !empty($data['profiles']->emailMe),
            "showDisplayField" => $connectionType != "omniamrts"
        ];
    }
    public function getProfileRows($table,$profileId)
    {
        return $this->em->getRepository('classesclassBundle:'.$table)->findBy(array('profileid' => $profileId));
    } 
    public function getFundGroupTag($planid)
    {
        $tag = false;
        $plan =  $this->em->getRepository('classesclassBundle:plans')->findOneBy(["id" =>$planid ]);
        if (!empty($plan))
            $group =  $this->em->getRepository('classesclassBundle:FundsGroups')->findOneBy(["id" =>$plan->fundGroupid]);
        if (!empty($group))
            $tag = $group->tag;
        return $tag;
    }
    public function getInvestments($profileId)
    {
        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder->select('profilesInvestments','plansFunds','FundsGroupsValues');     
        $queryBuilder->from('classesclassBundle:profilesInvestments', 'profilesInvestments');
        $joinedTables  = 
        [
            [
                "name" => "plansFunds",
                "joinBy" => 'profilesInvestments.planid = plansFunds.planid and plansFunds.name = profilesInvestments.fname'              
            ],
            [           
                "name" => "FundsGroupsValues",
                "joinBy" => 'plansFunds.groupValueId = FundsGroupsValues.id'              
            ]
        ];
        $queryBuilder->where('profilesInvestments.profileid = :profileId');
        $queryBuilder->setParameter('profileId', $profileId);
        $this->joinByArray($queryBuilder,$joinedTables);
        $results = $queryBuilder->getQuery()->getResult();
        $tables = ['profilesInvestments','plansFunds','FundsGroupsValues'];
        return $this->formatResult($tables,$results);
    }
    public function getRiskProfileQuestions($profileId)
    {
        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder->select('profilesRiskProfileQuestions','profilesRiskProfileAnswers');     
        $queryBuilder->from('classesclassBundle:profilesRiskProfileQuestions', 'profilesRiskProfileQuestions');
        $joinedTables  = 
        [
            [
                "name" => "profilesRiskProfileAnswers",
                "joinBy" => 'profilesRiskProfileQuestions.id = profilesRiskProfileAnswers.questionId'              
            ]
        ];  
        $this->joinByArray($queryBuilder,$joinedTables); 
        $queryBuilder->where('profilesRiskProfileQuestions.profileid = :profileId and profilesRiskProfileQuestions.used = 1 and profilesRiskProfileAnswers.selected = 1');
        $queryBuilder->orderBy('profilesRiskProfileQuestions.number','asc');
        $queryBuilder->setParameter('profileId', $profileId);
        $results = $queryBuilder->getQuery()->getResult() ;   
        $tables = ['profilesRiskProfileQuestions','profilesRiskProfileAnswers'];
        return $this->formatResult($tables,$results);
    }
    public function getRiskProfileQuestionsBackwardsCompatible($data)
    {
        if ((!empty($data['profilesRiskProfile']->xml) && !empty($data['profilesRiskProfile']->ans)))
        {
            $prefix  = "";
            $language = "en";
            if (!empty($data['accounts']->languagePrefix) && in_array($data['accounts']->languagePrefix,["adp","horacemann","lincoln","omnisp","smartenroll"]))
            {
                $prefix = $data['accounts']->languagePrefix . '_';
            }
            if (!empty($data['profiles']->language) && $data['profiles']->language == "es")
            {
                $language = "es";
            }       
            $combined = strtolower($prefix.$language);
            $file = $this->container->get('kernel')->locateResource('@SpeAppBundle/Resources/translations/'.$combined.'/riskProfile/');           
            $questions = $this->container->get('spe.app.functions')->getRPQuestionAnswer($data['profilesRiskProfile']->xml, $data['profilesRiskProfile']->ans, $file);
            $data['riskQuestions'] = [];
            foreach ($questions as $question)
            {
                $row = [];
                $row["profilesRiskProfileQuestions"]['question'] = $question['question'];
                $row["profilesRiskProfileAnswers"]['answer']   =$question['answer']; 
                $data['riskQuestions'][] = $row;
            }
        } 
        return $data['riskQuestions'];
    }
    public function beneficiariesHeader($data)
    {
        //dirty logic for the beneficiaries
        $beneficiariesHeader=   
        [
            "type" =>
            [
                "field" => "type",
                "translation" => 
                [
                    "title" => "beneficiary_type",
                    "title_source" => "beneficiary"
                ]
            ], 
            "relationship" =>
            [
                "field" => "relationship",
                "translation" => 
                [
                    "title" => "beneficiary_relationship",
                    "title_source" => "beneficiary"
                ]
            ], 
           "name" =>
            [
                "field" => "name",
                "translation" => 
                [
                    "title" => "beneficiary_name",
                    "title_source" => "beneficiary"
                ]
            ], 
           "percent" =>
            [
                "field" => "percent",
                "translation" => 
                [
                    "title" => "beneficiary_percent",
                    "title_source" => "beneficiary"
                ]
            ] 
        ];
        $spousalWaiverForm = 
        [
            "field" => "spousalWaiverForm",
            "translation" => 
            [
                "title" => "spousal_wavier_form",
                "title_source" => "beneficiary"
            ]
        ];
        foreach ($data['profilesBeneficiaries'] as $beneficiary)
        {
            if ($beneficiary->level == "Primary" && !empty($beneficiary->spousalWaiverForm) && $beneficiary->relationship != "Spouse")
            {
                $beneficiary->spousalWaiverForm =1;
                $beneficiariesHeader['spousalWaiverForm'] =   $spousalWaiverForm;                
                break;
            }
        }
        return $beneficiariesHeader;
    }
    public function beneficiariesHeaderRelius($data)
    {
        $beneficiariesHeader=   
        [
            "name" =>
            [
                "field" => "name",
                "translation" => 
                [
                    "title" => "name",
                    "title_source" => "beneficiary"
                ]
            ], 
            "dob" =>
            [
                "field" => "dob",
                "translation" => 
                [
                    "title" => "dob",
                    "title_source" => "beneficiary"
                ]
            ], 
           "type" =>
            [
                "field" => "type",
                "translation" => 
                [
                    "title" => "type",
                    "title_source" => "beneficiary"
                ]
            ], 
           "relationship" =>
            [
                "field" => "relationship",
                "translation" => 
                [
                    "title" => "spouse",
                    "title_source" => "beneficiary"
                ]
            ],
           "percent" =>
            [
                "field" => "percent",
                "translation" => 
                [
                    "title" => "percent",
                    "title_source" => "beneficiary"
                ]
            ] 
        ];
        return $beneficiariesHeader;
    }
    public function fundsHeader($data)
    {
        $connectionType = strtolower($data['accounts']->connectionType );
        //dirty logic for the funds
        $fundsHeader= 
        [
            "name" =>
            [
                "field" => "name",
                "translation" => 
                [
                    "title" => "fund_name",
                    "title_source" => "investments"
                ]
            ],   
            "percent" =>
            [
                "field" => "percent",
                "translation" => 
                [
                    "title" => "percent_new",
                    "title_source" => "global"
                ]
            ],
         
        ]; 
        $tag = $this->getFundGroupTag($data['profiles']->planid);
        
        if (!empty($tag))
            $fundsHeader["additionalFundHeader"] = 
            [
                "field" => "additionalFundHeader",
                "translation" => 
                [
                    "title" => $tag,
                    "title_source" => "investments"
                ]
            ];
        if (substr( $connectionType, 0, 4 ) == "omni")
        {
            $addFundFactLink = false;
            $addProspectus = false;
            foreach ($data['investments'] as $investment)
            {
                $addFundFactLink = $addFundFactLink || !empty($investment['profilesInvestments']->fundFactLink);
                $addProspectus = $addProspectus ||   !empty($investment['profilesInvestments']->prospectusLink);      
                if ($addFundFactLink && $addProspectus)
                    break;
            }
            if ($addFundFactLink)
                $fundsHeader["fundFact"] =
                [
                    "field" => "fundFact",
                    "translation" => 
                    [
                        "title" => "fund_fact",
                        "title_source" => "investments"
                    ]                
                ]; 
            if ($addProspectus)
                $fundsHeader["prospectus"] =
                [
                    "field" => "prospectus",
                    "translation" => 
                    [
                        "title" => "prospectus",
                        "title_source" => "investments"
                    ]                
                ];
        }
        return $fundsHeader;
    }
    public function uiData()
    {
        $translations = $this->container->get('translator')->getMessages();
        $translator= $this->container->get('translator');
        $sessionData = $this->session->all();
        $type = strtolower($sessionData['account']['beneficiaryType']);
        $isOmni =  substr(trim($sessionData['common']['data_source']),0,4)  == 'omni';
        if ($type == "standard" && $isOmni)
           $type = "omni";
        if ($type == "standard" && trim($sessionData['common']['data_source'])  == 'relius')
            $type = "relius";
        $transactionList = 
        [
            "elections" => 
            [
                "section" => "elections",
                "id" => "1",
                "sectionTitleSuccess" => $translations['alert']['elections']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['alert']['elections']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['alert']['elections']
            ],
            "realigment" => 
            [
                "section" => "realignemnt",
                "id" => "2",
                "sectionTitleSuccess" => $translations['alert']['realignment']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['alert']['realignment']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['alert']['realignment']
            ], 
            "contributions" => 
            [
                "section" => "contributions",
                "id" => "3",
                "sectionTitleSuccess" => $translations['alert']['contributions']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['alert']['contributions']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['alert']['contributions']
            ],
            "catchup" =>
            [
                "section" => "catchup",
                "id" => "4",
                "sectionTitleSuccess" => $translations['alert']['catch_up']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['alert']['catch_up']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['alert']['catch_up']                
            ],
            "beneficiary" =>
            [
                "section" => "beneficiary",
                "id" => "5",
                "sectionTitleSuccess" => $translations['beneficiary']['beneficiary']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['beneficiary']['beneficiary']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['beneficiary']['beneficiary']                
            ],
            "enrollment" =>
            [
                "section" => "enrollment",
                "id" => "6",
                "sectionTitleSuccess" => $translations['alert']['enrollment']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['alert']['enrollment']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['alert']['enrollment']  
            ],
            "combined" =>
            [
                "section" => "combined",
                "id" => "7",
                "sectionTitleSuccess" => $translations['alert']['selections']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['alert']['selections']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['alert']['selections']  
            ],
            "profile" =>
            [
                "section" => "profile",
                "id" => "10",
                "sectionTitleSuccess" => $translations['alert']['profile']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['alert']['profile']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['alert']['profile']
            ],
            "advice" =>
            [
                "section" => "advice",
                "id" => "9",
                "sectionTitleSuccess" => $translations['investments']['advice']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['investments']['advice']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['investments']['advice']
            ],
            "at_futurebuilder" =>
            [
                "section" => "at_futurebuilder",
                "id" => "8",
                "sectionTitleSuccess" => $translations['alert']['at_futurebuilder']." ".$translations['alert']['updated'],
                "sectionTitleFail" => $translations['alert']['at_futurebuilder']." ".$translations['alert']['updated_failed'],
                "sectionTitle" => $translations['alert']['at_futurebuilder']
            ],
            "ACAOptOut" =>
            [
                "section" => "ACAOptOut",
                "id" => "11",
                "sectionTitleSuccess" => "ACA Opt Out Sent",
                "sectionTitleFail" => "ACA Opt Out Failed",
                "sectionTitle" => "ACA Opt Out",
            ],
            "autoincrease" =>
            [
                "section" => 'autoincrease',
                "id" => "12",
                "sectionTitleSuccess" => "ASI Updated",
                "sectionTitleFail" => "ASI Update Failed",
                "sectionTitle" => "ASI",
            ]
        ];
        $transactions = [];
        if (in_array("postCombined",$sessionData['common']['post_calls']))
            $transactions['combined'] =  $transactionList['combined'];
        else
        {
            if (($sessionData['common']['updateElections'] && $sessionData['common']['planAllowsElections'] && in_array("postElections",$sessionData['common']['post_calls'])) || ($sessionData['common']['planAllowsElections'] && $sessionData['managedAccountOptInValue'] == '1'))
                $transactions['elections'] =  $transactionList['elections'];                            
            if ($sessionData['common']['updateRealignment'] &&   in_array("postRealignment",$sessionData['common']['post_calls']))
               $transactions['realigment'] =  $transactionList['realigment'];
            if ($sessionData['contributions']['updateContributions'] && $sessionData['common']['planAllowsDeferrals'] && in_array("postContributions",$sessionData['common']['post_calls']))
               $transactions['contributions'] =  $transactionList['contributions'];  
            if (isset($sessionData['contributions']['updateCatchUp']) && $sessionData['common']['planAllowsCatchup'] && in_array("postCatchUp",$sessionData['common']['post_calls']))
               $transactions['catchup'] =  $transactionList['catchup'];
            if (($sessionData['common']['beneficiaries']   && in_array("postBeneficiary",$sessionData['common']['post_calls'])) ||  $sessionData['common']['data_source'] == 'relius')
                if (!empty( $sessionData['beneficiaryList']) || !empty( $sessionData['beneficiaryListDeleted']) || in_array($sessionData['common']['data_source'],["srt2","envisage"]))
                    $transactions['beneficiary'] =  $transactionList['beneficiary'];
            if ($sessionData['common']['enrollment'] && in_array("postEnrollment",$sessionData['common']['post_calls']))
               $transactions['enrollment'] =  $transactionList['enrollment'];
            if ($sessionData['module']['ATBlueprint'])
                $transactions['at_futurebuilder'] =  $transactionList['at_futurebuilder'];
            if (isset($sessionData['ACAOptOutProfile']) && $sessionData['ACAOptOutProfile'] && in_array("postACAOptOut",$sessionData['common']['post_calls']))
                $transactions['ACAOptOut'] = $transactionList['ACAOptOut'];
            if ($sessionData['autos']['autoIncreaseOptIn'] && in_array("postPartASISettings", $sessionData['common']['post_calls']))
                $transactions['autoincrease'] = $transactionList['autoincrease'];

        }
        if ($sessionData['investmentSection']  == "ADVICE")
            $transactions['advice'] =  $transactionList['advice'];
        $transactions['profile'] = $transactionList['profile'];


        $uiData = 
        [
            "profileId" => $sessionData['profileId'],
            "translations" => $this->container->get('translator')->getMessages(),
            "transactions" => $transactions,
            "beneficiary" =>
            [
                "on" => false,
                "optional" => $sessionData['common']['beneficiaries'] == 1,
                "type" => strtolower($sessionData['account']['beneficiaryType']),
                'autoShow' => $sessionData['common']['autoShowBeneficairy'],
                "primaryTotalPercent" => $this->getBeneficiaryTotalPercent("primary"),
                "secondaryTotalPercent" => $this->getBeneficiaryTotalPercent("secondary"),
                "file" => $type,
                "title" =>  $translator->trans('beneficiary', array(), 'beneficiary'),
                "emptyListMessage" => "",
                "listHeader" => $this->beneficiaryListHeaderStandard(),
                "emptyBeneficiaryError" => !$sessionData['common']['planRequiresBeneficiary'] || $sessionData['common']['data_source'] != 'relius' ? $translator->trans('add_beneficiary', array(), 'alert') : $translator->trans('beneficiary_information_required', array(), 'alert'),
                "beneficiaryData" => $this->beneficiaryDataByType($type),
                "audioFile" => $translator->trans('profile_0703', array(), 'audio'),
                "showAudioFile" => $sessionData['account']['beneficiaryAudioEnabled']                
            ],
            "beneficiaryList" => $sessionData['beneficiaryList']
        ];
        if (in_array($type,["omni","relius"]))
        {
            $uiData['beneficiary']['title'] = $translator->trans('your_plan_beneficiaries', array(), 'beneficiary');
            $uiData['beneficiary']['emptyListMessage'] = $translator->trans('you_havent_setup_any_beneficiaries_yet', array(), 'beneficiary');
            $uiData['beneficiary']["listHeader"] = $this->beneficiaryListHeaderRelius();
        }
        if($type == "relius")
            $uiData['beneficiary']["listHeader"] = $this->beneficiaryListHeaderRelius();
        if($type == "omni")
            $uiData['beneficiary']["listHeader"] = $this->beneficiaryListHeaderOmni();         

        if (($sessionData['common']['beneficiaries'] || ($sessionData['common']['data_source'] == 'relius' && $sessionData['common']['planRequiresBeneficiary'])) && !$sessionData['common']['updateProfileBeneficiary'])
        {
            $uiData['beneficiary']['on'] = true;
            if ($sessionData['common']['data_source'] == 'relius') {
                if($sessionData['common']['planRequiresBeneficiary'] == 2)
                    $uiData['beneficiary']['optional'] = true;
                 else
                    $uiData['beneficiary']['optional'] = false;
            }
        }
        $uiData['beneficiary']['primaryTotalPercentOffset'] = 100-$uiData['beneficiary']['primaryTotalPercent'];
        $uiData['beneficiary']['secondaryTotalPercentOffset'] = 100-$uiData['beneficiary']['secondaryTotalPercent'];
        $uiData['beneficiary']['showPrimaryPercentError'] = !in_array($uiData['beneficiary']['primaryTotalPercent'],[0,100]);
        $uiData['beneficiary']['showSecondaryPercentError'] = !in_array($uiData['beneficiary']['secondaryTotalPercent'],[0,100]);     
        if ($uiData['beneficiary']['file'] == "srt")
            $uiData['beneficiary']['modalSize'] = "large";
        else
            $uiData['beneficiary']['modalSize'] = "medium"; 
        $uiData['lockNavigation'] = $isOmni;
        $uiData['audio']['investorProfile'] = $translator->trans('profile_0706AXA', array(), 'audio');
        $uiData['audio']['investorProfileTransact'] = $translator->trans('profile_0719AXA', array(), 'audio');
        $uiData['ECOMMModal'] = 
        [
            "show" => $sessionData['common']['ECOMMModal'] && $isOmni && $sessionData['common']['data_source'] != 'omnimoa',
            "email" => $sessionData['rkp']['plan_data']->email,
            "checked" => $sessionData['common']['ECOMMModalChecked']
        ];
        $uiData['showNoDeferralsModal'] = !$sessionData['common']['planAllowsDeferrals'];
        $uiData['showNoElectionsModal'] = !$sessionData['common']['planAllowsElections'];
        $uiData['rollover'] = 
        [
            "enabled" => $sessionData['module']['rolloverWorkflow'] && 
            (in_array('asset_type_option_4',$sessionData["retirement_needs"]['RN_AssetTypes']) ||
             in_array('asset_type_option_8',$sessionData["retirement_needs"]['RN_AssetTypes'])),           
        ];
        $uiData['viewOnly'] = !empty($sessionData['common']['planAllowsViewOnly']);
        $uiData['moaDisclaimer'] = false;
        if ($sessionData['common']['data_source'] == 'omnimoa' && empty($sessionData['moaDisclaimerAccepted']))
            $uiData['moaDisclaimer'] = true;
        return $uiData;
    }
    public function beneficiaryListHeader()
    {
        $translator= $this->container->get('translator');
        $header = 
        [
            "name" =>
            [
                "field" => "name",
                "translation" => $translator->trans('name', array(), 'beneficiary'),
                "length" => 3
            ],
            "type" =>
            [
                "field" => "type",
                "translation" => $translator->trans('type', array(), 'beneficiary'),
                "length" => 3
            ],
            "relationship" =>
            [
                "field" => "relationship",
                "translation" => $translator->trans('relationship', array(), 'beneficiary'),
                "length" => 2
            ],   
            "percent" =>
            [
                "field" => "percent",
                "translation" => $translator->trans('PCT', array(), 'beneficiary'),
                "length" => 1
            ],
            "actions" =>
            [
                "field" => "actions",
                "translation" => $translator->trans('actions', array(), 'beneficiary'),
                "length" => 3               
            ],
            'dob' =>
            [
                "field" => "dob",
                "translation" => $translator->trans('date_of_birth', array(), 'beneficiary'),
                "length" => 2
            ],
            'spouse' =>
            [
                "field" => "spouse",
                "translation" => $translator->trans('spouse', array(), 'beneficiary'),
                "length" => 2                
            ],
        ];
        return $header;
    }
    public function beneficiaryListHeaderStandard()
    {
        $header = $this->beneficiaryListHeader();
        return 
        [
            "name" => $header["name"],
            "type" => $header["type"],
            "relationship" => $header["relationship"],
            "percent" => $header["percent"],
            "actions" => $header["actions"]
        ];
    }
    public function beneficiaryListHeaderRelius()
    {
        $header = $this->beneficiaryListHeader();
        $header['type']['length'] =  $header['actions']['length'] = 2;
        return 
        [
            "name" => $header["name"],
            "dob" => $header["dob"],
            "percent" => $header["percent"],
            "type" => $header["type"],
            "spouse" => $header["spouse"],
            "actions" => $header["actions"]
        ];
    }
    public function beneficiaryListHeaderOmni()
    {
        $header = $this->beneficiaryListHeader();
        $header['type']['length'] =  $header['actions']['length'] = 2;
        return 
        [
            "name" => $header["name"],
            "dob" => $header["dob"],
            "percent" => $header["percent"],
            "type" => $header["type"],
            "relationship" => $header["relationship"],
            "actions" => $header["actions"]
        ];
    }
    public function beneficiaryData($beneficiary = null)
    {
        $sessionData = $this->session->all();
        $translator= $this->container->get('translator');
        $beneData =  
        [
            "firstName" =>
            [
                "field" => "firstName",
                "type" => "text",
                "validators" => ["required"],
                "translation" => $translator->trans('first_name', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_enter_first_name",
                    "source" => "alert"
                ]
            ],

            "lastName" =>
            [
                "field" => "lastName",
                "type" => "text",
                "validators" => ["required"],
                "translation" => $translator->trans('last_name', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_enter_last_name",
                    "source" => "alert"
                ]
            ],
            "ssn" =>
            [
                "field" => "ssn",
                "type" => "ssn",
                "mask" => "###-##-####",
                "translation" => $translator->trans('ssn', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_enter_valid_ssn",
                    "source" => "alert"
                ]  
            ],
            "relationship" =>
            [
                "field" => "relationship",
                "type" => "select",
                "validators" => ["required"],
                "translation" => $translator->trans('relationship', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_select_beneficiary_relationship",
                    "source" => "alert"
                ],
                "options" =>
                [
                    "spouse" => 
                    [
                        "value" => "spouse",
                        "translation" => $translator->trans('spouse', array(), 'beneficiary')
                    ],
                    "son" => 
                    [
                        "value" => "son",
                        "translation" => $translator->trans('son', array(), 'beneficiary')

                    ],
                    "daughter" =>
                    [
                        "value" => "daughter",
                        "translation" => $translator->trans('daughter', array(), 'beneficiary')

                    ],
                    "mother" =>
                    [
                        "value" => "mother",
                        "translation" => $translator->trans('mother', array(), 'beneficiary')

                    ], 
                    "father" =>
                    [
                        "value" => "father",
                        "translation" => $translator->trans('father', array(), 'beneficiary')                       
                    ],
                    "sister" =>
                    [
                        "value" => "sister",
                        "translation" => $translator->trans('sister', array(), 'beneficiary')                      
                    ],
                    "brother" =>
                    [
                        "value" => "brother",
                        "translation" => $translator->trans('brother', array(), 'beneficiary')

                    ],
                    "other" =>
                    [
                        "value" => "other",
                        "translation" => $translator->trans('other', array(), 'beneficiary')
                    ]
                ]               
            ],
            "gender" =>
            [
                "field" => "gender",
                "type" => "select",
                "validators" => ["required"],
                "translation" => $translator->trans('gender', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_select_gender",
                    "source" => "alert"
                ],
                "options" =>
                [
                    "male" => 
                    [
                        "value" => "male",
                        "translation" => $translator->trans('male', array(), 'beneficiary')

                    ], 
                    "female" => 
                    [
                        "value" => "female",
                        "translation" => $translator->trans('female', array(), 'beneficiary')

                    ],  
                    "other" => 
                    [
                        "value" => "other",
                        "translation" => $translator->trans('other', array(), 'beneficiary')
                    ] 
                ]
            ],
            "type" =>
            [
                "field" => "type",
                "type" => "select",
                "validators" => ["required"],
                "translation" => $translator->trans('type', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_select_beneficiary_type",
                    "source" => "alert"
                ],
                "options" =>
                [
                    "primary" => 
                    [
                        "value" => "primary",
                        "translation" => $translator->trans('primary', array(), 'beneficiary')

                    ],    
                    "secondary" => 
                    [
                        "value" => "secondary",
                        "translation" => $translator->trans('secondary', array(), 'beneficiary')
                    ]  
                ]
            ],
            "maritalStatus" =>
            [
                "field" => "maritalStatus",
                "type" => "select",
                "validators" => ["required"],
                "translation" => $translator->trans('marital_status', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_select_marital_status",
                    "source" => "alert"
                ],
                "options" =>
                [
                    "single" => 
                    [
                        "value" => "single",
                        "translation" => $translator->trans('single', array(), 'beneficiary')
                    ],  
                    "married" => 
                    [
                        "value" => "married",
                        "translation" => $translator->trans('married', array(), 'beneficiary')

                    ], 
                    "widow" => 
                    [
                        "value" => "widow",
                        "translation" => $translator->trans('widow', array(), 'beneficiary')
                    ],   
                    "divorced" => 
                    [
                        "value" => "divorced",
                        "translation" => $translator->trans('divorced', array(), 'beneficiary')
                    ],  
                ]
            ],
            "percent" =>
            [
                "field" => "percent",
                "type" => "number",
                "mask" => "###",
                "validators" => ["required"],
                "min" => 1,
                "max" => 100,
                "translation" => $translator->trans('percent', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_enter_valid_percentage",
                    "source" => "alert"
                ],
            ],
            "address1" =>
            [
                "field" => "address1",
                "type" => "text",
                "translation" => $translator->trans('address_line_1', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_enter_address1",
                    "source" => "alert"
                ]  
            ],
            "address2" =>
            [
                "field" => "address2",
                "type" => "text",
                "translation" => $translator->trans('address_line_2', array(), 'beneficiary'),
            ],
            "city" =>
            [
                "field" => "city",
                "type" => "text",
                "translation" => $translator->trans('city', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_enter_city",
                    "source" => "alert"
                ]  
            ],
            "country" =>
            [
                "field" => "country",
                "type" => "select",
                "translation" => $translator->trans('country', array(), 'beneficiary'),
                "options" => $this->convertListToOptions($this->countryList())
            ],
            "state" =>
            [
                "field" => "state",
                "type" => "select",
                "translation" => $translator->trans('state', array(), 'beneficiary'),
                "options" => $this->convertListToOptions($this->stateList()),
                "translationEmpty" =>
                [
                    "key" => "please_select_state",
                    "source" => "alert"
                ]  
            ],
            "trustEstateCharityOther" =>
            [
                "field" => "trustEstateCharityOther",
                "type" => "checkbox",
                "translation" => $translator->trans('trust', array(), 'beneficiary')."/".$translator->trans('estate', array(), 'beneficiary')."/".$translator->trans('charity', array(), 'beneficiary')."/".$translator->trans('other', array(), 'beneficiary'),
            ],
            "foreignaddress" =>
            [
                "field" => "foreignaddress",
                "type" => "checkbox",
                "translation" => $translator->trans('foreign_address_other_than_usa', array(), 'beneficiary'),                
            ],
            "middleInitial" =>
            [
                "field" => "middleInitial",
                "type" => "text",
                "mask" => "A",
                "translation" => $translator->trans('middle_initial', array(), 'beneficiary'),                
            ],
            "dob" =>
            [
                "field" => "dob",
                "type" => "text",
                "mask" => "##/##/####",
                "translation" => $translator->trans('date_of_birth', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_enter_date_of_birth",
                    "source" => "alert"
                ]  
            ],
            "phone" =>
            [
                "field" => "phone",
                "type" => "text",
                "mask" => "(###) ###-####",
                "translation" => $translator->trans('phone_number', array(), 'beneficiary')                
            ],
            "zip" =>
            [
                "field" => "zip",
                "type" => "text",
                "mask" => "#####",
                "translation" => $translator->trans('postal_code', array(), 'beneficiary'),
                "validators" => ["required"],
                "translationEmpty" =>
                [
                    "key" => "please_enter_valid_postal_code",
                    "source" => "alert"
                ],                
            ],
            "isSpouse" =>
            [
                "field" => "isSpouse",
                "type" => "checkbox",
                "translation" => $translator->trans('is_spouse', array(), 'beneficiary'),
            ],
            "name" => 
            [
                "field" => "name",
                "type" => "text",
                "validators" => ["required"],
                "translation" => $translator->trans('name', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_enter_name",
                    "source" => "alert"
                ]                
            ],
            "level" =>
            [
                "field" => "level",
                "type" => "select",
                "validators" => ["required"],
                "translation" => $translator->trans('beneficiary_level', array(), 'beneficiary'),
                "translationEmpty" =>
                [
                    "key" => "please_select_beneficiary_level",
                    "source" => "alert"
                ],
                "options" =>
                [
                    "primary" => 
                    [
                        "value" => "primary",
                        "translation" => $translator->trans('primary', array(), 'beneficiary')

                    ],    
                    "secondary" => 
                    [
                        "value" => "secondary",
                        "translation" => $translator->trans('secondary', array(), 'beneficiary')
                    ]  
                ]
            ],
        ];
        if($sessionData['common']['data_source'] == 'omnimoa') { //moa sux
            unset($beneData['zip']['validators']);
        }
        if (empty($beneficiary))
            foreach ($beneData as $key => $value)
                $beneData[$key]['value']  = null;
        else
            foreach ($beneficiary as $key => $value)
                if (isset($beneData[$key]))
                    $beneData[$key]['value']  = $value;
        return $beneData;
    }
    public function beneficiaryDataByType($type,$beneficiary = null)
    {
        if (strtolower($type) == "srt")
            return $this->srtBeneficiaryData($beneficiary);
        else if (strtolower($type) == "relius")
            return $this->reliusBeneficiaryData($beneficiary);
        else if (strtolower($type) == "omni")
            return $this->omniBeneficiaryData($beneficiary);
        else            
            return $this->standardBeneficiaryData($beneficiary);
    }
    public function standardBeneficiaryData($beneficiary = null)
    {
        $beneData = $this->beneficiaryData($beneficiary);
        $fields = ['firstName','ssn','lastName','relationship','gender','type','maritalStatus','percent'];
        $return = [];
        foreach ($fields as $field)
            $return[$field] = $beneData[$field];
        return $return;
    }
    public function srtBeneficiaryData($beneficiary = null)
    {
        $translator= $this->container->get('translator');
        $beneData = $this->beneficiaryData($beneficiary);
        $fields = ['trustEstateCharityOther','type','relationship','firstName','middleInitial','lastName','percent','ssn','dob'];
        $return = [];
        foreach ($fields as $field)
        {
            $return[$field] = $beneData[$field];
            $return[$field]['area'] = "left";
        }
        $return['percent']['translation'] = $translator->trans('share_percentage', array(), 'beneficiary');
        $return['type']['translation'] = $translator->trans('beneficiary_type', array(), 'beneficiary');
        $return['dob']['translation'] = $translator->trans('date_of_birth', array(), 'beneficiary')." (".$translator->trans('date_format', array(), 'beneficiary').")" ;
        $fields = ['foreignaddress','phone','address1','address2','city','state','zip','country'];
        foreach ($fields as $field)
        {
            $return[$field] = $beneData[$field];
            $return[$field]['area'] = "right";
        }
        if (empty($return['country']['value']))
            $return['country']['value'] = "US";
        if ($return['country']['value'] == "US"){
            $return['country']['disabled'] = true;
        }
        foreach ($return as &$row)
            $row['descriptionLength'] = 6;
        $return['foreignaddress']['descriptionLength'] = 9;
        foreach ($return as &$row)
            $row['inputLength'] = 12-$row['descriptionLength'];
        $return['state']['translation'] = $translator->trans('state', array(), 'beneficiary')."/".$translator->trans('region', array(), 'beneficiary');
        return $return;
    }
    public function reliusBeneficiaryData($beneficiary = null)
    {
        $translator= $this->container->get('translator');
        $beneData = $this->beneficiaryData($beneficiary);
        $fields = ['name','dob','address1','ssn','address2','isSpouse','city','type','state','percent','zip'];
        $return = [];
        foreach ($fields as $field)
            $return[$field] = $beneData[$field];
        $return['address1']['translation'] = $translator->trans('address', array(), 'beneficiary')." 1";
        $return['address2']['translation'] = $translator->trans('address', array(), 'beneficiary')." 2";
        $return['zip']['translation'] = $translator->trans('zip', array(), 'beneficiary');
        $return['type']['translation'] = $translator->trans('beneficiary_type', array(), 'beneficiary');
        $return['type']['options']['secondary']['translation'] = $translator->trans('contingent', array(), 'beneficiary');
        $return['percent']['translation'] = $translator->trans('benefit_allocation_percentage', array(), 'beneficiary');        
        return $return;
    }
    public function omniBeneficiaryData($beneficiary = null)
    {
        $beneData = $this->beneficiaryData($beneficiary);
        $reliusData = $this->reliusBeneficiaryData($beneficiary);
        $omniData = 
        [
            "name" => $reliusData['name'],
            "dob" => $reliusData['dob'],
            "address1" => $reliusData['address1'],
            "ssn" => $reliusData['ssn'],
            "address2" => $reliusData['address2'],
            "relationship" =>  $beneData['relationship'],
            "city" =>  $beneData['city'],
            "gender" =>  $beneData['gender'],
            "state" =>  $reliusData['state'],
            "type" => $reliusData['type'],   
            "zip" => $reliusData['zip'],
            "level" => $beneData['level'],
            "percent" =>  $reliusData['percent']
        ];
        $newValidated = ["address1","city","state","dob","ssn"];
        foreach ($newValidated as $key)
            $omniData[$key]['validators'][] = "required";     
        unset($omniData['gender']['options']['other']);
        $omniData['relationship']['options'] = $this->omniRelationships();
        $omniData['type']['options'] = $this->omniTypes();
        return $omniData;
    }
    public function getBeneficiaryTotalPercent($type)
    {
        $percent = 0;
        $beneficiaryList = $this->session->get("beneficiaryList");
        foreach ($beneficiaryList as $beneficiary)
            if ($beneficiary['type'] == $type)
                $percent += $beneficiary['percent'];    
        return $percent;           
    }
    public function countryList()           
    {
        return 
        [
            "US" => "United States","AF" => "Afghanistan","AX" => "Åland Islands","AL" => "Albania","DZ" => "Algeria","AS" => "American Samoa","AD" => "Andorra","AO" => "Angola","AI" => "Anguilla","AQ" => "Antarctica","AG" => "Antigua and Barbuda","AR" => "Argentina","AM" => "Armenia","AW" => "Aruba","AU" => "Australia","AT" => "Austria","AZ" => "Azerbaijan","BS" => "Bahamas","BH" => "Bahrain","BD" => "Bangladesh","BB" => "Barbados","BY" => "Belarus","BE" => "Belgium","BZ" => "Belize","BJ" => "Benin","BM" => "Bermuda","BT" => "Bhutan","BO" => "Bolivia, Plurinational State of","BQ" => "Bonaire, Sint Eustatius and Saba","BA" => "Bosnia and Herzegovina","BW" => "Botswana","BV" => "Bouvet Island","BR" => "Brazil","IO" => "British Indian Ocean Territory","BN" => "Brunei Darussalam","BG" => "Bulgaria","BF" => "Burkina Faso","BI" => "Burundi","KH" => "Cambodia","CM" => "Cameroon","CA" => "Canada","CV" => "Cape Verde","KY" => "Cayman Islands","CF" => "Central African Republic","TD" => "Chad","CL" => "Chile","CN" => "China","CX" => "Christmas Island","CC" => "Cocos (Keeling) Islands","CO" => "Colombia","KM" => "Comoros","CG" => "Congo","CD" => "Congo, the Democratic Republic of the","CK" => "Cook Islands","CR" => "Costa Rica","CI" => "Côte d'Ivoire","HR" => "Croatia","CU" => "Cuba","CW" => "Curaçao","CY" => "Cyprus","CZ" => "Czech Republic","DK" => "Denmark","DJ" => "Djibouti","DM" => "Dominica","DO" => "Dominican Republic","EC" => "Ecuador","EG" => "Egypt","SV" => "El Salvador","GQ" => "Equatorial Guinea","ER" => "Eritrea","EE" => "Estonia","ET" => "Ethiopia","FK" => "Falkland Islands (Malvinas)","FO" => "Faroe Islands","FJ" => "Fiji","FI" => "Finland","FR" => "France","GF" => "French Guiana","PF" => "French Polynesia","TF" => "French Southern Territories","GA" => "Gabon","GM" => "Gambia","GE" => "Georgia","DE" => "Germany","GH" => "Ghana","GI" => "Gibraltar","GR" => "Greece","GL" => "Greenland","GD" => "Grenada","GP" => "Guadeloupe","GU" => "Guam","GT" => "Guatemala","GG" => "Guernsey","GN" => "Guinea","GW" => "Guinea-Bissau","GY" => "Guyana","HT" => "Haiti","HM" => "Heard Island and McDonald Islands","VA" => "Holy See (Vatican City State)","HN" => "Honduras","HK" => "Hong Kong","HU" => "Hungary","IS" => "Iceland","IN" => "India","ID" => "Indonesia","IR" => "Iran, Islamic Republic of","IQ" => "Iraq","IE" => "Ireland","IM" => "Isle of Man","IL" => "Israel","IT" => "Italy","JM" => "Jamaica","JP" => "Japan","JE" => "Jersey","JO" => "Jordan","KZ" => "Kazakhstan","KE" => "Kenya","KI" => "Kiribati","KP" => "Korea, Democratic People's Republic of","KR" => "Korea, Republic of","KW" => "Kuwait","KG" => "Kyrgyzstan","LA" => "Lao People's Democratic Republic","LV" => "Latvia","LB" => "Lebanon","LS" => "Lesotho","LR" => "Liberia","LY" => "Libya","LI" => "Liechtenstein","LT" => "Lithuania","LU" => "Luxembourg","MO" => "Macao","MK" => "Macedonia, the former Yugoslav Republic of","MG" => "Madagascar","MW" => "Malawi","MY" => "Malaysia","MV" => "Maldives","ML" => "Mali","MT" => "Malta","MH" => "Marshall Islands","MQ" => "Martinique","MR" => "Mauritania","MU" => "Mauritius","YT" => "Mayotte","MX" => "Mexico","FM" => "Micronesia, Federated States of","MD" => "Moldova, Republic of","MC" => "Monaco","MN" => "Mongolia","ME" => "Montenegro","MS" => "Montserrat","MA" => "Morocco","MZ" => "Mozambique","MM" => "Myanmar","NA" => "Namibia","NR" => "Nauru","NP" => "Nepal","NL" => "Netherlands","NC" => "New Caledonia","NZ" => "New Zealand","NI" => "Nicaragua","NE" => "Niger","NG" => "Nigeria","NU" => "Niue","NF" => "Norfolk Island","MP" => "Northern Mariana Islands","NO" => "Norway","OM" => "Oman","PK" => "Pakistan","PW" => "Palau","PS" => "Palestinian Territory, Occupied","PA" => "Panama","PG" => "Papua New Guinea","PY" => "Paraguay","PE" => "Peru","PH" => "Philippines","PN" => "Pitcairn","PL" => "Poland","PT" => "Portugal","PR" => "Puerto Rico","QA" => "Qatar","RE" => "Réunion","RO" => "Romania","RU" => "Russian Federation","RW" => "Rwanda","BL" => "Saint Barthélemy","SH" => "Saint Helena, Ascension and Tristan da Cunha","KN" => "Saint Kitts and Nevis","LC" => "Saint Lucia","MF" => "Saint Martin (French part)","PM" => "Saint Pierre and Miquelon","VC" => "Saint Vincent and the Grenadines","WS" => "Samoa","SM" => "San Marino","ST" => "Sao Tome and Principe","SA" => "Saudi Arabia","SN" => "Senegal","RS" => "Serbia","SC" => "Seychelles","SL" => "Sierra Leone","SG" => "Singapore","SX" => "Sint Maarten (Dutch part)","SK" => "Slovakia","SI" => "Slovenia","SB" => "Solomon Islands","SO" => "Somalia","ZA" => "South Africa","GS" => "South Georgia and the South Sandwich Islands","SS" => "South Sudan","ES" => "Spain","LK" => "Sri Lanka","SD" => "Sudan","SR" => "Suriname","SJ" => "Svalbard and Jan Mayen","SZ" => "Swaziland","SE" => "Sweden","CH" => "Switzerland","SY" => "Syrian Arab Republic","TW" => "Taiwan, Province of China","TJ" => "Tajikistan","TZ" => "Tanzania, United Republic of","TH" => "Thailand","TL" => "Timor-Leste","TG" => "Togo","TK" => "Tokelau","TO" => "Tonga","TT" => "Trinidad and Tobago","TN" => "Tunisia","TR" => "Turkey","TM" => "Turkmenistan","TC" => "Turks and Caicos Islands","TV" => "Tuvalu","UG" => "Uganda","UA" => "Ukraine","AE" => "United Arab Emirates","GB" => "United Kingdom","UM" => "United States Minor Outlying Islands","UY" => "Uruguay","UZ" => "Uzbekistan","VU" => "Vanuatu","VE" => "Venezuela, Bolivarian Republic of","VN" => "Viet Nam","VG" => "Virgin Islands, British","VI" => "Virgin Islands, U.S.","WF" => "Wallis and Futuna","EH" => "Western Sahara","YE" => "Yemen","ZM" => "Zambia","ZW" => "Zimbabwe"
        ];  
    }
    public function stateList()
    {
        return 
        [
            'AL' => 'Alabama','AK' => 'Alaska', 'AZ' => 'Arizona', 'AR' => 'Arkansas', 'CA' => 'California', 'CO' => 'Colorado', 'CT' => 'Connecticut', 'DE' => 'Delaware', 'DC' => 'District Of Columbia', 'FL' => 'Florida', 'GA' => 'Georgia', 'HI' => 'Hawaii', 'ID' => 'Idaho', 'IL' => 'Illinois', 'IN' => 'Indiana', 'IA' => 'Iowa', 'KS' => 'Kansas', 'KY' => 'Kentucky', 'LA' => 'Louisiana', 'ME' => 'Maine', 'MD' => 'Maryland', 'MA' => 'Massachusetts', 'MI' => 'Michigan', 'MN' => 'Minnesota', 'MS' => 'Mississippi', 'MO' => 'Missouri', 'MT' => 'Montana','NE' => 'Nebraska','NV' => 'Nevada','NH' => 'New Hampshire','NJ' => 'New Jersey','NM' => 'New Mexico','NY' => 'New York','NC' => 'North Carolina','ND' => 'North Dakota','OH' => 'Ohio', 'OK' => 'Oklahoma', 'OR' => 'Oregon', 'PA' => 'Pennsylvania', 'RI' => 'Rhode Island', 'SC' => 'South Carolina', 'SD' => 'South Dakota','TN' => 'Tennessee', 'TX' => 'Texas', 'UT' => 'Utah', 'VT' => 'Vermont', 'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia', 'WI' => 'Wisconsin', 'WY' => 'Wyoming'            
        ];
    }
    public function convertListToOptions($list)
    {
        $return = [];
        foreach ($list as $key => $value)
        {
            $row = [];
            $row['value'] = $key;
            $row['translation'] = $value;
            $return[] = $row;
        }
        return $return;
    }
    public function omniRelationships()
    {
        $translator= $this->container->get('translator');
        $sections = 
        [
            "spouse","aunt","business_partner","daughter","father","friend","father_in_law",
            "grandchild","grandfather","grandmother","mother","mother_in_law","niece_nephew",
            "step_daughter","step_father","step_mother","step_son","sister_brother","son","uncle",
            "charity","church","creditor","employer","estate","school","trust","other","originator","domestic_partner"
        ];
        $relationships = [];
        foreach ($sections as $section)
        {
            $relationships[$section] = 
            [
                "value" => $section,
                "translation" => $translator->trans($section, array(), 'beneficiary')
            ];
        }
        return $relationships;
    }
    public function omniTypes()
    {
        $translator= $this->container->get('translator');
        $sections = 
        [
            "individual","organization","trust"
        ];
        $types = [];
        foreach ($sections as $section)
        {
            $types[$section] = 
            [
                "value" => $section,
                "translation" => $translator->trans($section, array(), 'beneficiary')
            ];
        }
        return $types;
    }
}
