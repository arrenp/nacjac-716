<?php

namespace Spe\AppBundle\Services;

use classes\classBundle\Services\BaseService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InvestmentsService extends BaseService {

    var $translator;
    var $functions;
    public function __construct(ContainerInterface $containerInterface, $translator, $functions)
    {
        parent::__construct($containerInterface);
        $this->translator = $translator;
        $this->functions = $functions;
    }

    public function getSections() {
        $modules = $this->session->get("module");
        $plan = $this->session->get('plan');
        $planData = $this->session->get('rkp')['plan_data'];
        $messages = $this->translator->getMessages();
        $messagesInvestments = $messages['investments'];
        $pathId = null;
        if (!empty($this->session->get('spe2'))) {
            $pathId = isset($this->session->get('spe2')['pathId']) ? $this->session->get('spe2')['pathId'] : null;
        }

        $sections = [
            "investmentQDIA" =>
                [
                    "name" => "qdia",
                    "defaultValue" => "QDIA",
                    "translation" =>
                        [
                            "title" => "QDIA",
                            "description" => "QDIA"
                        ],
                    "path" => "/investments/qdia",
                    "componentName" => ['InvestmentQDIA'],
                    "modulesCheck" =>false,
                    "checked" => true
                ],
            "ATArchitectPreviouslyEnrolled" =>
                [
                    "name" => "default",
                    "defaultValue" => "Custom Choice",
                    "translation" =>
                        [
                            "title" => "ATArchitect",
                            "description" => $messagesInvestments["back_to_atarchitect"],
                        ],
                    "path" => "/investments/atarchitect/1/enable",
                    "modulesCheck" =>true,
                    "checked" => true
                ],
            "investmentSelectorDefault" =>
                [
                    "name" => "default",
                    "defaultValue" => "Default Investment",
                    "translation" =>
                        [
                            "title" => $messagesInvestments["default_investment"],
                            "description" => $messagesInvestments["default_investment_displayed_desc"]
                        ],
                    "path" => "/investments/default",
                    "componentName" => ['InvestmentDefault'],
                    "modulesCheck" =>false,
                    "checked" => true
                ],
            "investmentSelectorRiskBasedPortfolio" =>
                [
                    "name" => "risk",
                    "defaultValue" => "Risk Based Portfolio",
                    "translation" =>
                        [
                            "title" => $messagesInvestments["risk_title_default"],
                            "description" => $messagesInvestments["risk_desc_default"],
                            "audio" => $messages['audio']["investments_riskBased"],
                            "pathTitle" => "Risk_Based2",
                            "pathStep" => "Risk_Based",
                            "pathLabel" => "Risk_Based3"

                        ],
                    "path" => "/investments/risk",
                    "componentName" => ['RiskBasedInvestments', 'RiskBasedPortfolios'],
                    "modulesCheck" =>true
                ],
            "investmentSelectorTargetMaturityFund" =>
                [
                    "name" => "target",
                    "defaultValue" => "Target Maturity Fund",
                    "translation" =>
                        [
                            "title" => $messagesInvestments["target_title"],
                            "description" => $messagesInvestments["target_desc"],
                            "audio" => $messages['audio']["investments_TDP"],
                            "pathTitle" => "TDF1",
                            "pathStep" => "TDF",
                            "pathLabel" => "TDF2"
                        ],
                    "path" => "/investments/targetdate",
                    "componentName" => ['TDF'],
                    "modulesCheck" =>true
                ],
            "investmentSelectorCustomChoice" =>
                [
                    "name" => "custom",
                    "defaultValue" => "Custom Choice",
                    "translation" =>
                        [
                            "title" => $messagesInvestments["custom_choice"],
                            "description" => $messagesInvestments["custom_desc"],
                            "audio" => $messages['audio']["investments_CustomChoiceINFO"],
                            "pathTitle" => "CC1",
                            "pathStep" => "CC",
                            "pathLabel" => "CC2"
                        ],
                    "path" => "/investments/customchoice",
                    "componentName" => ['CC','CChoice_Review'],
                    "modulesCheck" =>true
                ],
            "investmentSelectorInvestmentsGraph" =>
                [
                    "name" => "investmentGraph",
                    "defaultValue" => "Custom Choice",
                    "translation" =>
                        [
                            "title" => $messagesInvestments["asset_allocation_model"],
                            "description" => $messagesInvestments["investor_profile_score"]." = ".$this->session->get('risk_profile')['RP_Score'],
                            "audio" => $messages['audio']["investments_riskBased"]
                        ],
                    "path" => "/investments/table",
                    "modulesCheck" =>false,
                    "checked" => true
                ]
        ];

        if(!empty($this->session->get('ACInitialize')['WsPlan']['AllowBlueprint']) && $this->session->get('ACInitialize')['WsPlan']['AllowArchitect'] == 'true' ) {
            $modules["ATArchitectPreviouslyEnrolled"] = true;
            $this->session->set("module", $modules);
        }

        if ($plan["riskType"] == "RBF")
        {
            $sections['investmentSelectorRiskBasedPortfolio']['translation']['title'] = $messagesInvestments["risk_title_rbf"];
            $sections['investmentSelectorRiskBasedPortfolio']['translation']['description'] = $messagesInvestments["risk_desc_rbf"];
        }
        foreach ($sections as  $key => &$section)
        {
            if($section['modulesCheck'])
            {
                $section['checked'] = false;
                if ($modules['investmentSelectorDefault'] == $section['defaultValue'])
                {
                    $section['checked'] = true;
                }
                if (!$modules[$key])
                {
                    unset($sections[$key]);
                }
            }
        }
        if (empty($planData->rkpExtras->defaultInvestment) || !$this->session->get("managedAccountOptInValue"))
        {
            unset($sections['investmentSelectorDefault']);
        }
        else
        {
            if (!isset($planData->rkpExtras->defaultInvestment->fund))
            {

                $sections['investmentSelectorDefault']['translation']['errorModal'] = $messagesInvestments["selection_not_allowed alert"];
            }
            else
            {
                $sections['investmentSelectorDefault']['translation']['investment'] = $this->getFunds()[$planData->rkpExtras->defaultInvestment->fund[1]]['name'];
                if (!empty($planData->rkpExtras->defaultInvestment->video))
                {
                    $sections['investmentSelectorDefault']['translation']['video'] = $planData->rkpExtras->defaultInvestment->video;
                }
                if (!empty($planData->rkpExtras->defaultInvestment->description))
                {
                    $sections['investmentSelectorDefault']['translation']['description'] = $planData->rkpExtras->defaultInvestment->description;
                }
                if (!empty($planData->rkpExtras->defaultInvestment->link))
                {
                    $sections['investmentSelectorDefault']['translation']['link'] = $planData->rkpExtras->defaultInvestment->link;
                }
                if (!empty($planData->rkpExtras->defaultInvestment->type) && $planData->rkpExtras->defaultInvestment->type == "ManagedAccount")
                {
                    $sections['investmentSelectorDefault']['translation']['title'] = $messagesInvestments['default_investment']." : Managed Account ";
                    $sections['investmentSelectorDefault']['translation']['audio'] = $messages['audio']["investments_0601x_GRD"];
                }
            }
        }
        if(!isset($sections['investmentSelectorDefault']) && $this->session->get('account')['connectionType'] === "Educate") {
            $spe_plan_id = $plan['id'];
            $spe_user_id = $plan['userid'];
            $fund = $this->em->getRepository('classesclassBundle:plansFunds')->findOneBy(array('planid' => $spe_plan_id, 'userid' => $spe_user_id,'qdiaDefault' => 1));
            if($fund && (empty($fund->pathId) || $fund->pathId === $pathId)){
                $defaultInvestmentFund = array($fund->ticker,$fund->id,$fund->name,null,null,$fund->link,$fund->qdiaVideo,$fund->qdiaUrl);
                $sections['investmentQDIA']['translation']['investment'] = $fund->name;
                $sections['investmentQDIA']['translation']['link'] = $fund->link;

            }
            if (empty($defaultInvestmentFund)) {
                unset($sections['investmentQDIA']);
            }
        } else {
            unset($sections['investmentQDIA']);
        }       
        if ($this->session->get("risk_profile")['RP_InvestorType'] < 0 || !$this->validPortfolio($this->session->get("risk_profile")['recommendedPortfolioName']))
        {
            $sections['investmentSelectorRiskBasedPortfolio']['translation']['error'] = $messagesInvestments["please_complete_risk_profile"];
            $sections['investmentSelectorRiskBasedPortfolio']['translation']['errorModal'] = $messages['alert']["please_complete_risk_profile_to_select_risk_based_portfolio"];
            $sections['investmentSelectorInvestmentsGraph']['translation']['errorModal'] = $messages['alert']['please_complete_personal_investor_profile'];
        }
        else
            $sections['investmentSelectorRiskBasedPortfolio']['translation']['investment'] = $this->session->get("risk_profile")['recommendedPortfolioName'] ;
        $sections['investmentSelectorRiskBasedPortfolio']['translation']['label'] = $this->session->get("risk_profile")['RP_label'] ;

        if (!$this->session->get("retirement_needs")['RN_NumberOfYearsBeforeRetirement'] || empty( $this->getTargetFundsByYearsToRetirementCorrectFormat()))
        {
            $sections['investmentSelectorTargetMaturityFund']['translation']['errorModal'] = $messages['alert']["please_complete_retirement_needs_to_select_target_date_investment"];
            $sections['investmentSelectorTargetMaturityFund']['translation']['error'] = $messagesInvestments["please_complete_retirement_needs"];
        }
        else
        {
            $targetFunds = $this->getTargetFundsByYearsToRetirementCorrectFormat();
            $targetFundString = "";
            foreach ($targetFunds as $targetFund)
            {
                $targetFundString = $targetFundString.$targetFund['name']." ";
            }
            $sections['investmentSelectorTargetMaturityFund']['translation']['investment'] = $targetFundString;
        }

        if(!empty($this->session->get('ACInitialize')['WsPlan']['AllowBlueprint']) && $this->session->get('ACInitialize')['WsPlan']['AllowBlueprint'] == 'true' ) {
            $sections['investmentSelectorRiskBasedPortfolio']['translation']['special'] = "AT";
        }

        $filteredSections = array();
        if (isset($sections['investmentSelectorDefault']) && !empty($planData->rkpExtras->defaultInvestment->type) && $planData->rkpExtras->defaultInvestment->type == "Aca")
        {
            $sections['investmentSelectorDefault']['translation']['audio']  = $messages['audio']["investments_MoreInfo_090512"];
        }
        if (!empty($this->session->get("module")['investmentsGraph']))//adp is bad 4 u
        {
            $copySections = ['investmentSelectorInvestmentsGraph','investmentSelectorCustomChoice'];
            foreach ($copySections as $copySection)
            {
                if (isset($sections[$copySection]))
                {
                    $filteredSections[$copySection] = $sections[$copySection];
                }
            }
        }
        else
        {
            $filteredSections = $sections;
            unset($filteredSections['investmentSelectorInvestmentsGraph']);
        }
        if (isset($filteredSections['investmentSelectorInvestmentsGraph']) || isset($filteredSections['investmentSelectorDefault']))
        {
            foreach ($filteredSections as  $key => $section)
            {
                if($section['modulesCheck'])
                {
                    $filteredSections[$key]['checked'] = false;
                }
            }
        }
        foreach ($filteredSections as $key => $section)
        {
            if ($section['checked'])
            {
                unset($filteredSections[$key]);
                $filteredSections = array_merge([$key => $section],$filteredSections );
                break;
            }
        }

        return $filteredSections;
    }
    
    public function getFunds() {
        $rkpFunds= $this->session->get('rkp')['plan_data']->funds;
        $funds = array();
        foreach ($rkpFunds as $fund)
        {
            $rkpFund = array();
            $rkpFund['riskDescription'] = "";
            $rkpFund['step'] = false;
            $fields = array("ticker","id","name","currentElection","pendingElection","customUrl","prospectUrl","risk","assetClass","decimalScale","redemptionFeeMessage","performanceUrl","fundGroup","display","adviceId");
            $i = 0;
            foreach ($fields as $field)
            {
                if (isset($fund[$i]) && $field != "fundGroup")
                {
                    $rkpFund[$field] = $fund[$i];
                }
                else if (isset($fund[$i]) && $field == "fundGroup")
                {
                    $rkpFund[$field] = array();
                    $rkpFund[$field]['id'] = $fund[$i][0];
                    $rkpFund[$field]['name'] = $fund[$i][1];
                }
                else if (!isset($fund[$i]) && $field == "display" )
                {
                    $rkpFund[$field] = true;
                }
                else 
                {
                    $rkpFund[$field] = null;
                }
                if (isset($fund[$i]) &&  $fund[$i] !== null  && $field == "risk")
                {
                    $rkpFund['riskDescription'] = $fund[$i];
                    if (!empty($fund[$i]))
                        $this->session->set('showRiskColumn', true);
                }
                if (isset($fund[$i]) &&  $fund[$i] !== null  && $field == "assetClass")
                {
                    if (!empty($fund[$i]))
                        $this->session->set('showAssetClassColumn', true);
                }
                if (isset($fund[$i]) &&  $fund[$i] !== null  && $field == "decimalScale")
                {
                    $rkpFund['step'] = 1;
                    for ($j = 0; $j < (int)$fund[$i]; $j++)
                    {
                        $rkpFund['step'] /= 10;
                    }
                }
                $i++;
            }
            if ($rkpFund['step'] == 1)
            {
                $rkpFund['currentElection'] = (int)$rkpFund['currentElection'];
            }
            $rkpFund['preCurrentElection'] = $rkpFund['currentElection'] ;

            $funds[$rkpFund["id"]] = $rkpFund;
        }       
        return $funds;
    }
    
    public function getTargetDateFunds() {
        $targetFunds = array();
        $rkpTargetFunds = $this->session->get('rkp')['plan_data']->targetDateFunds;
        foreach ($rkpTargetFunds as $fund)
        {
            $targetFund = array();
            $targetFund['year'] = $fund[0];
            $targetFund['id'] = $fund[1];
            $targetFund['name'] = $fund[2];
            $targetFunds[$fund[0]][] = $targetFund;
        }
        ksort($targetFunds);
        return $targetFunds;
    }
    
    public function getPortfolios()
    {
        $portfolios = $this->session->get("rkp")['plan_data']->portfolios;
        $returnedPortfolios = array();
        foreach ($portfolios as $portfolio)
        {
            $fundsArray = array();
            foreach ($portfolio[2] as $fund)
            {
                $fundsArray[] = 
                [
                    "id" => $fund[0],
                    "id2" => $fund[1],
                    "percent" => $fund[2],
                    "name" => $fund[3]
                ];
            }
            if (!empty($fundsArray))
            $returnedPortfolios[] = 
            [
                "id" => $portfolio[0],
                "name" => $portfolio[1],
                "funds" => $fundsArray
            ];
        }
        return $returnedPortfolios;
    }

    public function getPortfolioByName($name) {
        $portfolios = $this->session->get("rkp")['plan_data']->portfolios;
        $returnedPortfolio = array();
        $availableFunds = $this->getFunds();
        foreach ($portfolios as $portfolio)
        {
            if ($portfolio[1] === $name) {
                $fundsArray = array();
                foreach ($portfolio[2] as $fund)
                {
                    $fundsArray[] =
                        [
                            "id" => $fund[0],
                            "id2" => $fund[1],
                            "percent" => $fund[2],
                            "name" => $fund[3],
                            "customUrl" => $availableFunds[$fund[1]]['customUrl']
                        ];
                }
                $returnedPortfolio =
                    [
                        "id" => $portfolio[0],
                        "name" => $portfolio[1],
                        "funds" => $fundsArray
                    ];
                break;

            }
        }
        return $returnedPortfolio;
    }
    public function validPortfolio($name = "")
    {
        $portfolio = $this->getPortfolioByName($name);
        return !empty($portfolio['funds']);
    }
    
    public function getTargetFundsByYearsToRetirement()
    {
        $retireYear = (int)date("Y") + (int)$this->session->get("retirement_needs")['RN_NumberOfYearsBeforeRetirement'];
        $targetFunds = $this->getTargetDateFunds();
        $currentFunds = null;
        foreach ($targetFunds as $key => $funds)
        {
            if ($key > $retireYear)
            {
                if (isset($currentFunds))
                {
                    return $currentFunds;
                }
                return $funds;
            }
            $currentFunds = $funds;
        }
        return $currentFunds;
    }
    
    public function getTargetFundsByYearsToRetirementCorrectFormat()
    {
        $currentFunds = $this->getTargetFundsByYearsToRetirement();
        $funds = $this->getFunds();
        $returnFunds = array(); 
        foreach ($currentFunds as $fund)
        {
            if ($funds[$fund['id']] != null)
            {
                $returnFunds[] = $funds[$fund['id']];
            }
        }
        return $returnFunds;
    }
    
    public function fundsHeaderSimple()
    {
        $fundsHeader= 
        [
            "percent" =>
            [
                "field" => "percent",
                "translation" => 
                [
                    "title" => "percent_new",
                    "title_source" => "global"
                ]
            ],
            "name" =>
            [
                "field" => "name",
                "translation" => 
                [
                    "title" => "fund_name",
                    "title_source" => "investments"
                ]
            ]
        ]; 
        if (!empty($this->session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader))
        {
            $fundsHeader["additionalFundHeader"] = 
            [

                "field" => "additionalFundHeader",
                "translation" => 
                [
                    "title" => $this->session->get("rkp")['plan_data']->rkpExtras->additionalFundHeader,
                    "title_source" => "investments"
                ]

            ];
        }
        if ($this->session->get('smart401k_completed'))
        {
            $fundsHeader["percent"]["translation"]["title"]= "recommended";
            $fundsHeader["percent"]["translation"]["title_source"]= "global";
        }
        return $fundsHeader;
        
    }
    
    public function fundsHeaderConfirm()
    {
        $investmentSetting = $this->session->get("rkp")['plan_data']->investmentSettings;
        $fundHeader = $investmentSetting->fundHeader;
        $fundsHeader = $this->fundsHeaderSimple();
        if (!empty($fundHeader->prospectUrl->show))
        {
            $fundsHeader = array_merge($fundsHeader,$this->fundsHeaderProspectUrl());
        }
        if (!empty($fundHeader->acknowledged->show))
        {
            $fundsHeader["acknowledged"]  = 
            [
                "field" => "acknowledged",
                "translation" => 
                [
                    "title" => "acknowledged",
                    "title_source" => "investments"
                ]
            ];
        }
        if (empty($this->session->get('smart401k_completed')))
        {
            $fundsHeader['percent']['translation']['title'] = "percent";
        }
        return $fundsHeader;
    }
    
    public function fundsHeaderProspectUrl()
    {
        $section["prospectUrl"]  = 
        [
            "field" => "prospectUrl",
            "translation" => 
            [
                "title" => "prospectus",
                "title_source" => "investments"
            ]
        ];      
        return $section;
    }
    
    public function fundsHeader()
    {
        $fundsHeader = $this->fundsHeaderSimple();
        $investmentSetting = $this->session->get("rkp")['plan_data']->investmentSettings;
        $fundHeader = $investmentSetting->fundHeader;
        $section = [];
        if (!empty($fundHeader->pendingElection->show))
        {
            $section["pendingElection"]  = 
            [
                "field" => "pendingElection",
                "translation" => 
                [
                    "title" => "pending",
                    "title_source" => "global"
                ]
            ];
        }
        if (!empty($fundHeader->currentElection->show))
        {
            $section["currentElection"]  = 
            [
                "field" => "currentElection",
                "translation" => 
                [
                    "title" => "current_percent",
                    "title_source" => "global"
                ]
            ];
        }

        $fundsHeader= array_merge($section,$fundsHeader);
        $section = [];
        if (!empty($fundHeader->risk->show))
        {
            $section["risk"]  = 
            [
                "field" => "risk",
                "translation" => 
                [
                    "title" => "risk_level",
                    "title_source" => "investments"
                ]
            ];
        }
        if (!empty($fundHeader->assetClass->show))
        {
            $section["assetClass"]  = 
            [
                "field" => "assetClass",
                "translation" => 
                [
                    "title" => "asset_class",
                    "title_source" => "investments"
                ]
            ];
        }
        if (!empty($fundHeader->prospectUrl->show))
        {
            $section = array_merge($section,$this->fundsHeaderProspectUrl());
        }
        if (!empty($fundHeader->performanceUrl->show))
        {
            $section["performanceUrl"]  = 
            [
                "field" => "performanceUrl",
                "translation" => 
                [
                    "title" => "performance_url",
                    "title_source" => "investments"
                ]
            ];           
        }
        if (!empty($fundHeader->redemptionFeeMessage->show))
        {
            $section["redemptionFeeMessage"]  = 
            [
                "field" => "redemptionFeeMessage",
                "translation" => 
                [
                    "title" => "redemption_fee_message",
                    "title_source" => "investments"
                ]
            ];           
        }
        if (!empty($fundHeader->fundFact->show))
        {
            $section["fundFact"]  = 
            [
                "field" => "fundFact",
                "translation" => 
                [
                    "title" => "fund_fact",
                    "title_source" => "investments"
                ]
            ];           
        }
        $fundsHeader= array_merge($fundsHeader,$section);
        return $fundsHeader;
    }
    
    public function setInvestmentsSection($section)
    {
        $this->session->set("investmentSectionTemp",$section);
    }
    
}
