<?php
namespace Spe\AppBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;

class RkpService
{
    public function __construct(Session $session){
        $this->session = $session;
        $this->api_url = $this->session->get('api_url');
        $this->environment = $this->session->get('environment');
    }

    //Call RKP
    private function callRkp($action, $param){
        if($this->environment == 'dev'){
            $param['debug'] = true;
        }
        $data = json_encode($param);
        $ch = curl_init($this->api_url.'/'.$action.'.json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data)));
        $response = curl_exec($ch);
        return $response;
    }

    //GET RKP Data
    public function getRKPData(){
        $param = $this->session->get('REQUEST');
		if ($this->session->get('profileId')) {
			$param['profileId'] = $this->session->get('profileId');
		}
        $data = (array) json_decode($this->callRkp('initialize',$param));
        if(isset($data['rkp']) && strpos($data['rkp'], 'retrev') !== false){
            $data['rkp'] = 'retrev';
        }
        $this->session->set('rkp', $data);
    }

    //Ping Call
    public function ping(){
        $session = $this->session;
        $param = array(
            "key"  => $session->get('rkp')['key']
        );
        $result = $this->callRkp('ping',$param);
        return $result;
    }

    //POST Personal Call
    public function postPersonal(){
        $session = $this->session;

        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => array(
                "partSSN" =>  $session->get('common')['ssn'],
                "firstName" => $session->get('common')['firstName'],
                "lastName" => $session->get('common')['lastName'],
                "maritalStatus" => $session->get('common')['maritalStatus'],
                "address" => $session->get('common')['address'],
                "city" => $session->get('common')['city'],
                "region" => $session->get('common')['region'],
                "postalCode" => $session->get('common')['postalCode'],
                "dateOfBirth" => $session->get('common')['dateOfBirth'],
                "asOfDate" => $session->get('common')['asOfDate'],
                "email" => $session->get('common')['email']
            )
        );

        $result = $this->callRkp('postPersonal',$param);
        return $result;
    }

    //POST Elections Call
    public function postElections(){
        $session = $this->session;
        $investments = $session->get('investments');
        $plan = $session->get('plan');
        $rkp = $session->get('rkp')['plan_data'];

        $selectedPortfolioId = $selectedPortfolioName = null;

        if ($this->session->get("investmentsSelectedPortfolio") != null)
        {
            $selectedPortfolioId = $this->session->get("investmentsSelectedPortfolio")['id'];
            $selectedPortfolioName = $this->session->get("investmentsSelectedPortfolio")['name'];
        }

        $param = array(
                        "key"  => $session->get('rkp')['key'],
                        "data" => array(
                            "selectedInvestmentOption" =>  $session->get("investmentSection"),
                            "selectedInvestmentDetails" => $investments['I_SelectedInvestmentDetails'],
                            "updateRealignment" => isset($session->get('common')['updateRealignment']) && isset($session->get('common')['planAllowsRealignments']) && $session->get('common')['planAllowsRealignments'] ? (boolean) $session->get('common')['updateRealignment'] : false,
                            "riskType" => $plan['riskType'],
                            "selectedPortfolioId" => $selectedPortfolioId,
                            "selectedPortfolioName" => $selectedPortfolioName,
                            "savedFunds" => (array)$session->get("savedFunds"),
                            'ATRealignmentInvestmentDetails' => $investments['I_RealignSelectedInvestmentDetails'],
                            )
                        );

        $result = $this->callRkp('postElections',$param);
        return $result;
    }

    //POST Realignment Call
    public function postRealignment(){
    $session = $this->session;
    $investments = $session->get('investments');
    $plan = $session->get('plan');
    $rkp = $session->get('rkp')['plan_data'];

    $selectedPortfolioId = $selectedPortfolioName = null;
    if($investments['I_SelectedInvestmentOption'] == 'RISK'){
        $portfolios = isset($rkp->IPMs) ? $rkp->IPMs : $rkp->portfolios;
        foreach($portfolios as $port):
            if($port[0] == $investments['I_SelectedInvestmentDetails']):
                $selectedPortfolioId = $port[0];
                $selectedPortfolioName = $port[1];
                break;
            endif;
        endforeach;
    }

    $param = array(
        "key"  => $session->get('rkp')['key'],
        "data" => array(
            "selectedInvestmentOption" =>  $investments['I_SelectedInvestmentOption'],
            "selectedInvestmentDetails" => $investments['I_SelectedInvestmentDetails'],
            "updateRealignment" => isset($session->get('common')['updateRealignment']) && isset($session->get('common')['planAllowsRealignments']) && $session->get('common')['planAllowsRealignments'] ? (boolean) $session->get('common')['updateRealignment'] : false,
            "riskType" => $plan['riskType'],
            "selectedPortfolioId" => $selectedPortfolioId,
            "selectedPortfolioName" => $selectedPortfolioName,
            "savedFunds" => $session->get("savedFunds")
        )
    );

    $result = $this->callRkp('postRealignment',$param);
    return $result;
}

    //POST Enrollment Call
    public function postEnrollment(){
        $session = $this->session;
        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => array(
                "status" =>  1
            )
        );
        $result = $this->callRkp('postEnrollment',$param);
        return $result;
    }

    //POST Realignment Call
    public function postContributions(){
        $result = $this->callRkp('postContributions',$this->postContributionsParams());
        return $result;
    }

    //POST CatchUp Call
    public function postCatchUp(){
        $session = $this->session;
        $contributions = $session->get('contributions');
        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => array(
                "catchUpMode" =>  $contributions['catchup_mode'],
                "catchUpContributionValue" => $contributions['C_CatchUpContributionValue']
            )
        );
        $result = $this->callRkp('postCatchUp',$param);
        return $result;
    }

    //POST Beneficiary Call
    public function postBeneficiary(){
        $session = $this->session;
        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => array(
                "maritalStatus" => $session->get('rkp')['plan_data']->maritalStatus,
                "beneficiaryList" => $session->get('beneficiaryList'),
                "beneficiaryListDeleted" => $session->get('beneficiaryListDeleted')
            )
        );

        $result = $this->callRkp('postBeneficiary',$param);
        return $result;
    }

    //POST Combined Call
    public function postCombined(){
        $session = $this->session;
        $investments = $session->get('investments');
        $contributions = $session->get('contributions');
        $retirement_needs = $session->get('retirement_needs');
        $risk_profile = $session->get('risk_profile');
        $plan = $session->get('plan');
        $rkp = $session->get('rkp')['plan_data'];
		$source = $session->get('rkp')['rkp'];

        $selectedPortfolioId = $selectedPortfolioName = null;
        if ($this->session->get("investmentsSelectedPortfolio") != null)
        {
            $selectedPortfolioId = $this->session->get("investmentsSelectedPortfolio")['id'];
            $selectedPortfolioName = $this->session->get("investmentsSelectedPortfolio")['name'];
        }
        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => array(
                "selectedInvestmentOption" =>  $session->get("investmentsSection"),
                "selectedInvestmentDetails" => $investments['I_SelectedInvestmentDetails'],
                "updatePersonal" => isset($session->get('common')['updatePersonal']) ? (boolean) $session->get('common')['updatePersonal'] : false,
                "updateBeneficiary" => !empty($session->get('common')['beneficiaries']) && (!empty($session->get('beneficiaryList')) || !empty($session->get('beneficiaryListDeleted'))),
                "updateElections" => isset($session->get('common')['updateElections']) && isset($session->get('common')['planAllowsElections']) && $session->get('common')['planAllowsElections']  ? (boolean) $session->get('common')['updateElections'] : false,
                "updateRealignment" => isset($session->get('common')['updateRealignment']) && isset($session->get('common')['planAllowsRealignments']) && $session->get('common')['planAllowsRealignments'] ? (boolean) $session->get('common')['updateRealignment'] : false,
                "updateContributions" => isset($session->get('contributions')['updateContributions']) && ((isset($session->get('common')['planAllowsDeferrals']) && $session->get('common')['planAllowsDeferrals']) || ($source == 'guardian')) ? (boolean) $session->get('contributions')['updateContributions'] : false,
                "updateCatchUp" => isset($session->get('contributions')['updateCatchUp']) && isset($session->get('common')['planAllowsCatchup']) && $session->get('common')['planAllowsCatchup'] ? (boolean) $session->get('contributions')['updateCatchUp'] : false,
                "navigationLocked" => isset($session->get('common')['navigationLocked']) ? $session->get('common')['navigationLocked'] : false,
                "riskType" => $plan['riskType'],
                "selectedPortfolioId" => $selectedPortfolioId,
                "selectedPortfolioName" => $selectedPortfolioName,
                "dateOfBirth" => $session->get('common')['dateOfBirth'],
                "numberOfYearsBeforeRetirement" => $retirement_needs['RN_NumberOfYearsBeforeRetirement'],
                "estimatedRetirementIncome" => $retirement_needs['RN_NumberOfYearsBeforeRetirement'],
                "investorType" => $risk_profile['RP_InvestorType'],
                "replacementIncome" => $retirement_needs['RN_ReplacementIncome'],
                "currentYearlyIncome" => $retirement_needs['RN_CurrentYearlyIncome'],
                "contributionMode" =>  $contributions['contribution_mode'],
                "preTaxContributionPct" => $contributions['C_PreTaxContributionPct'],
                "preTaxContributionValue" => $contributions['C_PreTaxContributionValue'],
                "rothContributionPct" => $contributions['C_RothContributionPct'],
                "rothContributionValue" => $contributions['C_RothContributionValue'],
                "postTaxContributionPct" => $contributions['C_PostTaxContributionPct'],
                "postTaxContributionValue" => $contributions['C_PostTaxContributionValue'],
                "beneficiaryList" => $session->get('beneficiaryList'),
                "beneficiaryListDeleted" => $session->get('beneficiaryListDeleted'),
                "optout" => $contributions['C_ACAoptOut'],
                "autoIncrease" => $session->get("autoIncreaseSave"),
                "savedFunds" => (array)$session->get("savedFunds"),
                "updateRebalance" => isset($session->get('common')['updateRebalance']) ? $session->get('common')['updateRebalance']: 0 ,
                "rebalanceFrequency" => isset($session->get('common')['rebalanceFrequency']) ? $session->get('common')['rebalanceFrequency']: "" ,               
            )
        );

        if($session->get('rkp')['rkp'] == strpos($session->get('rkp')['rkp'],'omni') && isset($session->get('rkp')['plan_data']->rkpExtras->allowecomm) && $session->get('rkp')['plan_data']->rkpExtras->allowecomm){
            $param['data']['rkpExtras']=  array(
                "email" => isset($session->get('common')['email']) ? $session->get('common')['email'] : '',
                "ecomm" => isset($session->get('common')['edelivery']) && $session->get('common')['allowecomm'] ? $session->get('common')['edelivery'] : '',
            );
        }
        
        if ($session->has("autoIncreaseSave")) {
            $param['data']['optout'] = empty($session->get("autoIncreaseSave")['deferrals']) ? 1 : $param['data']['optout'];
        }

        $result = $this->callRkp('postCombined',$param);
        return $result;
    }

    //POST Extra Transaction Call
    public function postExtraTransaction($extras){
        $session = $this->session;
        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => array(
                "rkpExtras" =>  array(
                    "email" => $extras['email'],
                    "ecomm" => $extras['edelivery'],
                )
            )
        );
        $result = $this->callRkp('postCombined',$param);
        return $result;
    }

    //POST ACAOptOut Call
    public function postACAOptOut(){
        $session = $this->session;
        $contributions = $session->get('contributions');
        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => $contributions['C_ACAoptOut']
        );
        $result = $this->callRkp('postACAOptOut',$param);
        return $result;
    }

    public function postPartASISettings() {
        $session = $this->session;
        $autos = $session->get("autos");
        $data = array_intersect_key($autos, array_flip([
            'autoIncreaseOptIn','autoIncreaseStartDate',
            'autoIncreasePre','autoIncreaseRoth',
            'autoIncreasePost','autoIncreaseCap',
            'autoIncreaseFrequency'
        ]));
        $param = [
            "key" => $this->session->get('rkp')['key'],
            "data" => $data
        ];
        return $this->callRkp('postPartASISettings', $param);
    }

    //POST Smart401k Call
    public function postSmart401k(){
        $session = $this->session;
        $risk_profile = $session->get('risk_profile');
        $rkp = $session->get('rkp')['plan_data'];
        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => array(
                "smart401kAdviceProfileId" => isset($risk_profile['smart401k_advice_profile_id']) ? $risk_profile['smart401k_advice_profile_id'] : null,
                "editRecommended" => $session->get('editRecommended'),
                "participantId" => $session->get('profileId'),
                "firstName" => $session->get('common')['firstName'],
                "lastName" => $session->get('common')['lastName'],
                "region" => $session->get('common')['region'],
                "planBalance" => $rkp->planBalance,
                "riskProfileAnswers" => $risk_profile['RP_Answers'],
                "riskProfileScore" => $risk_profile['RP_Score']
            )
        );
        $result = $this->callRkp('postSmart401k',$param);
        return $result;
    }

    public function ACInitialize()
    {
        $params = [
            "key" =>  $this->session->get('rkp')['key']
        ];
        $result = $this->callRkp('acinitialize',$params);
        return json_decode($result,true);
    }
    public function ACBluePrint($params = null)
    {
        if ($params == null)
        {
            $params = $this->ACInitialize();
        }
        $params['key'] = $this->session->get('rkp')['key'];
        $result = $this->callRkp('acblueprint',$params);
        return json_decode($result,true);
    }
    public function ACTransact($params = null)
    {
        $params['key'] = $this->session->get('rkp')['key'];
        $result = $this->callRkp('actransact',$params);
        return json_decode($result,true);
    }
    public function postTrustedContact(){
        $session = $this->session;
        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => array(
                "partSSN" =>  $session->get('common')['ssn'],
                "firstName" => $session->get('common')['firstName'],
                "lastName" => $session->get('common')['lastName'],
                "address" => $session->get('common')['address'],
                "city" => $session->get('common')['city'],
                "region" => $session->get('common')['state'],
                "postalCode" => strtok($session->get('common')['postalCode'], '-')
            ),
            "optIn" => $session->get('trustedContactOptIn')
        );
        $result = $this->callRkp('postTrustedContact',$param);
        return $result;
    }
    function postContributionsParams()
    {
        $session = $this->session;
        $contributions = $session->get('contributions');
        if (!$session->get('common')['roth'])
        {
            $contributions['C_RothContributionPct'] = null;
            $contributions['C_RothContributionValue'] = null;
        }
        $param = array(
            "key"  => $session->get('rkp')['key'],
            "data" => array(
                "contributionMode" =>  $contributions['contribution_mode'],
                "preTaxContributionPct" => $contributions['C_PreTaxContributionPct'],
                "preTaxContributionValue" => $contributions['C_PreTaxContributionValue'],
                "rothContributionPct" => $contributions['C_RothContributionPct'],
                "rothContributionValue" => $contributions['C_RothContributionValue'],
                "postTaxContributionPct" => $contributions['C_PostTaxContributionPct'],
                "postTaxContributionValue" => $contributions['C_PostTaxContributionValue']
            )
        );
        return $param;
    }
    public function postContributionsWidget(){
        $contributionsParams = $this->postContributionsParams(); 
        $contributions = $this->session->get('contributions');
        $params = $this->session->get("REQUEST");
        $header = $this->session->get("header");
        $params['user_id'] = $header->get('user_id');
        $params['ContributionsInfo'] = $contributionsParams['data'];
        $params['ContributionSources'] = [];
        $params['optout'] = !empty($contributions['C_ACAoptOut']);
        $params['testingTrans'] = !empty($this->session->get("testingTrans"));
        $possibleSources= ["preTax","roth"];
        foreach ($possibleSources as $source)
        {            
            if (!empty($header->get($source."Source")))
            {
                $contributionSource = [];
                $contributionSource["source"] = $header->get($source."Source");
                $contributionSource["sourceType"] = $header->get($source."SourceType");
                $contributionSource["name"] = $header->get($source."Name");
                if ($source == "preTax")
                {
                    $contributionSource['sourceValue'] = $this->session->get("rkp")['plan_data']->preCurrent;
                }
                else
                {
                    $contributionSource['sourceValue'] = $this->session->get("rkp")['plan_data']->rothCurrent;
                }
                $contributionSource["key"] = $source;
                $params['ContributionSources'][] = $contributionSource;
            }
        }
        $result = $this->callRkp('postContributionsWidget',$params);
        return $result;
    }
    public function postStadionQuestionnaire($answers)
    {
        $params['key'] = $this->session->get('rkp')['key'];
        $params['data'] = [];
        $params['data']['answers'] = $answers;
        if (empty($params['data']['answers'])) {
            $params['data']['defaultFlow'] = true;
            unset($params['data']['answers']);
        }
        $result = $this->callRkp('poststadionquestionnaire',$params);
        $response = json_decode($result,true);
        $hashPTilesToPathId = 
        [
             5 => 7, 20  =>  6, 35 => 5, 50 => 4, 65 => 3, 80 => 2, 95 => 1
        ];
        $response['data']['pathId'] = $hashPTilesToPathId[$response['data']["ptile"]];
        return $response;
    }
    
    public function postStadionConfirmation($confirmationCode) {
        $params = [
            'key' => $this->session->get('rkp')['key'],
            'data' => [
                'answers' => $this->session->get('stadionQuestionnaireAnswers'),
                'confirmationCode' => $confirmationCode,
                'action' => 1,
            ]
        ];
        $result = $this->callRkp('postStadionConfirmation',$params);
        return json_decode($result,true);

    }
}
