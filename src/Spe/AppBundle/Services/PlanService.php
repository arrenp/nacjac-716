<?php
namespace Spe\AppBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PlanService
{
    public function __construct(EntityManager $entityManager, Session $session,ContainerInterface $container){
        $this->em = $entityManager;
        $this->session = $session;
        $this->container = $container;
    }

    // Load Plan Data
    public function setPlanData(){
        $rkp = $this->session->get('rkp');
        $em = $this->em;
        $session = $this->session;
        $spe_plan_id = $session->get('spe_plan_id');

        // Load Plan
        $query = $em->createQuery('SELECT p FROM classesclassBundle:plans p WHERE p.id = :id')
            ->setParameter('id',$spe_plan_id);
        $plan = $query->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $plan['sections'] = $plan['sections'] ? $plan['sections'] : 'default';
        $plan['video'] = 6;

        if(isset($session->get('rkp')['plan_data']->rkpExtras->defaultInvestment)){
            $plan['defaultInvestment'] = 3;
        }
        if($plan['defaultInvestment']){
            $plan['defaultInvestmentType'] = isset($session->get('rkp')['plan_data']->rkpExtras->defaultInvestmentType) ? $session->get('rkp')['plan_data']->rkpExtras->defaultInvestmentType : 'fund';
        }
        
        if (!$plan['investorProfileEmail'])
        $plan['investorProfileEmail'] = "info@smartplanenterprise.com";
        
        $plan['logos'] = array();
        $logos = array('providerLogoImage','bannerImage','sponsorLogoImage');
        foreach ($logos as $logo)
        {
            if ($plan[$logo] != "")
            {
                $redirect = null;
                if ($logo == "bannerImage" && $plan['bannerLinkUrl'] != "")
                {
                    $redirect = $plan['bannerLinkUrl'];
                }
                $plan['logos'][] = array("url" => $plan[$logo],"redirect" => $redirect);
            }
        }


        $spe_user_id = $plan['userid'];

        // Default Modules
        $query = $em->createQuery('SELECT m FROM classesclassBundle:defaultModules m WHERE m.userid = :userid')
            ->setParameter('userid',$spe_user_id);
        $defaultModules = $query->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $session->set('default_modules', $defaultModules);

        // Load Account
        $query = $em->createQuery('SELECT a FROM classesclassBundle:accounts a WHERE a.id = :userid')
            ->setParameter('userid',$spe_user_id);
        $account = $query->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $account['languagesAvailable'] = array_map('strtolower', explode("|",$account['languagesAvailable']));
        $session->set("customNavigation", null);
        if (strtolower($account['connectionType']) == "omnimoa" )
        {
            $plan['interfaceLayout'] = "top";
            $session->set("customNavigation", "moa");
        }
        $session->set('plan', $plan);
        $session->set('account', $account);
        $module = (array)$rkp['plan_data']->plansModules;
        $module['ATArchitect'] = false;
        $isStadion = isset($rkp['plan_data']->rkpExtras->isStadion) && $rkp['plan_data']->rkpExtras->isStadion;
        $module['stadionManagedAccountOn'] = $account['stadionManagedAccountEnabled'] && $isStadion;
        $session->set('module', $module);
        // Load Video Skin
        $query = $em->createQuery('SELECT s FROM classesclassBundle:skinVideo s WHERE s.id = :skinid')
            ->setParameter('skinid',$plan['video']);
        $skin = $query->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $session->set('video_folder', $skin['folder']);
        

        // Load Available Plan portfolios
        $query = $em->createQuery("SELECT p FROM classesclassBundle:".($plan['riskType'] == 'RBF' ? 'plansRiskBasedFunds' : 'plansPortfolios')." p WHERE p.planid = :planid AND p.userid = :userid AND p.profile IN (:profiles) ORDER BY p.orderid,p.profile")
            ->setParameter('planid',$spe_plan_id)
            ->setParameter('userid',$spe_user_id)
            ->setParameter('profiles',array('0','1','2','3','4'));
        $portfolios = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $plan_portfolios = array();
        foreach ($portfolios as $row) {
            $plan_portfolios[] = array('name' =>  $row['name'],'profile' =>  $row['profile'],'min_score' =>  $row['minscore'],'max_score' =>  $row['maxscore'],'th_min_score' =>  $row['thMinscore'],'th_max_score' =>  $row['thMaxscore'],'customid' => $row['customid']);
        }
        $session->set('plan_portfolios', $plan_portfolios);

        // Load 402 Limit
        $query = $em->createQuery('SELECT l FROM classesclassBundle:serverConfig402Limits l');
        $limits = $query->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $session->set('limits', $limits);

        // Available Standard App Languages
        $languages = $em->getRepository('classesclassBundle:languages')->findAll();
        $lang = array();
        foreach($languages as $language){
            $lang[strtolower($language->getName())] = ucwords($language->getDescription());
        }
        $session->set('app_languages', $lang);

        // Available App Language Files
        $session->set('language_files', $this->getLanguageFiles());

        // Available Plan Languages
        $plan_languages = array();
        foreach($account['languagesAvailable'] as $code){
            if(isset($lang[$code]) && in_array($code,$session->get('language_files'))) {
                $plan_languages[$code] = $lang[$code];
            }
        }

        if(!$plan_languages){
            $plan_languages = array_slice($lang, 0, 1);
        }

        $session->set('plan_languages', $plan_languages);

        $serverConfig = $em->getRepository('classesclassBundle:serverConfig')->findOneBy(array());
        $session->set('serverConfig',json_decode(json_encode($serverConfig),true));

        $query = $em->createQuery("SELECT m FROM classesclassBundle:plansMedia m WHERE m.planid = :planid")
         ->setParameter('planid',$spe_plan_id);
        $plan_media = $query->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $session->set('plan_media', $plan_media);

        // Load Plan Locations
        $locations = $em->getRepository('classesclassBundle:plansLocation')->findBy(array('planid' => $spe_plan_id, 'userid' => $spe_user_id));

        $plan_locations = array();
        foreach($locations as $location){
			$tmp = [
				'id' => $location->id,
				'location' => $location->location,
			];
            $plan_locations[] = $tmp;
        }
        $session->set('plan_locations', $plan_locations);
        $session->set('advisors', $em->getRepository('classesclassBundle:plansAdvisor')->findBy(array('planid' => $spe_plan_id),["firstName" => "asc","lastName" => "asc"]));
        //Load Plan Documents
        $session->set('plan_documents', $this->getPlanDocuments($spe_plan_id, $spe_user_id));

        //Verify & Fix Participant Name and Find Participant Gender based on first name
        $this->censusParticipant();
        
        $findBySmartplanidArray = array();
        $findBySmartplanidArray[] = array("multiple" => 0,"name" => "PlansInvestmentsConfiguration");
        $findBySmartplanidArray[] = array("multiple" => 0,"name" => "PlansInvestmentsConfigurationColors");
        $findBySmartplanidArray[] = array("multiple" => 1,"name" => "PlansInvestmentsConfigurationFundsGroupValuesColors","orderby" => "min");
        $findBySmartplanidArray[] = array("multiple" => 1,"name" => "PlansInvestmentsYearsToRetirement","orderby" => "min");
        $findBySmartplanidArray[] = array("multiple" => 1,"name" => "PlansInvestmentsScore","orderby" => "min");
        $findBySmartplanidArray[] = array("multiple" => 1,"name" => "PlansInvestmentsLinkYearsToRetirementAndScore");
        $findBySmartplanidArray[] = array("multiple" => 1,"name" => "plansFunds", "orderby" =>"orderid");
        $fundGroupFailed = false;
        foreach ($findBySmartplanidArray as $entry)
        {
            $dsql = "SELECT m FROM classesclassBundle:".$entry['name']." m WHERE m.planid = :planid";
            if (isset($entry['orderby']))
            {
                $dsql = $dsql." ORDER BY m.".$entry['orderby'];
            }
            $query = $em->createQuery($dsql)
            ->setParameter('planid',$spe_plan_id);
            if ($entry['multiple'])
            {
                $result = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                if (!count($result))
                {
                    $fundGroupFailed = true;
                }
            }
            else
            {
                $result = $query->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                if ($result == null)
                {
                    $fundGroupFailed = true;
                }
            }
            $session->set($entry['name'], $result);
        }
        $PlansInvestmentsLinkYearsToRetirementAndScore =  $session->get("PlansInvestmentsLinkYearsToRetirementAndScore");
        $PlansInvestmentsLinkYearsToRetirementAndScoreHash = array();
        foreach ($PlansInvestmentsLinkYearsToRetirementAndScore as $link)
        {
            $PlansInvestmentsLinkYearsToRetirementAndScoreHash[$link['InvestmentsYearsToRetirementId']][$link['InvestmentsScoreId']] = $link;
        }
        $session->set("PlansInvestmentsLinkYearsToRetirementAndScoreHash", $PlansInvestmentsLinkYearsToRetirementAndScoreHash);
        $PlansInvestmentsConfigurationFundsGroupValuesColorsHash = array();
        foreach($session->get("PlansInvestmentsConfigurationFundsGroupValuesColors") as $color)
        {
            $PlansInvestmentsConfigurationFundsGroupValuesColorsHash[$color['scoreMinId']][] = $color;
        }
        $session->set("PlansInvestmentsConfigurationFundsGroupValuesColorsHash", $PlansInvestmentsConfigurationFundsGroupValuesColorsHash);
        $query = $em->createQuery("SELECT m FROM classesclassBundle:FundsGroupsValues m WHERE m.groupid = :groupid")->setParameter('groupid',$plan['fundGroupid']);
        $FundsGroupsValues = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $FundsGroupsValuesHash = array();
        foreach($FundsGroupsValues as $fund)
        {
            $FundsGroupsValuesHash[$fund['id']] = $fund;
        }
        $session->set("FundsGroupsValuesHash",$FundsGroupsValuesHash);
        if ($fundGroupFailed)
        {
            $module['investmentsGraph'] = 0;
            $session->set("module",$module);
        }
    }

    public function getPlanDocuments($spe_plan_id, $spe_user_id){
        $query = $this->em->createQuery('SELECT d FROM classesclassBundle:plansDocuments d WHERE d.planid = :planid AND d.userid = :userid')
            ->setParameter('planid',$spe_plan_id)
            ->setParameter('userid',$spe_user_id);
        $documents = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $plan_documents = array();
        foreach ($documents as $row) {
            $plan_documents[$row['name']] = $row['url'];
        }
        return $plan_documents;
    }

    public function getLanguageFiles(){
        $em = $this->em;
        $languages = $em->getRepository('classesclassBundle:languageFiles')->findAll();
        $data = array();
        if(!empty($languages)){
            foreach($languages as $language){
                $data[strtolower($language->getDescription())] = strtolower($language->getName());
            }
        }
        return $data;
    }

    public function censusParticipant(){
        $rkp = $this->session->get('rkp');
        if(str_word_count(trim($rkp['plan_data']->firstName)) > 1 && !$rkp['plan_data']->lastName){
            $first = $last = null;
            $name = explode(" ", trim($rkp['plan_data']->firstName));

            if(strlen($name[1]) == 1){
                $first = $name[0]. " ". $name[1];
                if(count($name) > 2)
                    $last = str_replace($first, '',trim($rkp['plan_data']->firstName));
            }
            else{
                $first = $name[0];
                $last = str_replace($first, '',trim($rkp['plan_data']->firstName));
            }

            $gender = $this->findParticipantGender($name[0]);
            if($gender <> 'T'){
                $rkp['plan_data']->firstName = trim($first);
                $rkp['plan_data']->lastName = trim($last);
                $this->session->set('rkp', (array) $rkp);
            }
        }
    }

    public function findParticipantGender($name){
        return 'T';
    }

    /**
     * returns an array of investment and mcgrawhill videos
     * making this the point of record for this information
     */
    public function getLibraryData() {
        $videosRepo = $this->em->getRepository('classesclassBundle:plansLibraryVideos');
        $directoryRepo = $this->em->getRepository('classesclassBundle:plansLibraryDirectories');
        $directoryMap = array();
        foreach($directoryRepo->findAll() as $item) {
            $directoryMap[$item->id] = $item->directory;
        }
        $vidTypeRepo = $this->em->getRepository('classesclassBundle:plansLibraryType');
        $vidTypeMap = array();
        foreach($vidTypeRepo->findAll() as $item) {
            $vidTypeMap[$item->id]['type'] = $item->type;
            $vidTypeMap[$item->id]['displayname'] = $item->displayname;
        }

        $videosList = $videosRepo->findAll();
        $files = array();
        foreach($videosList as $video) {
            array_push($files, array(
                'title' => $video->displayname,
                'value' => $video->name,
                'video' => $video->video,
                'directory' => $directoryMap[$video->directoryid],
                'type' => $vidTypeMap[$video->typeid]['type'],
                'displayname' => $vidTypeMap[$video->typeid]['displayname'],
                'thumbnail' => $video->thumbnail,
                'id' => $video->id,
                'runtime' => $video->runtime
            ));
        }

        return $files;
    }

    // Load Library Data
    public function setLibraryData()
    {
        $em = $this->em;
        $session = $this->session;
        $plan = $session->get('plan');
        $spe_plan_id = $plan['id'];
        $spe_user_id = $plan['userid'];

        $library = $this->getLibraryData();

        // build library items dynamically
        foreach($library as $lib) {
            $libraryItems[$lib['displayname']][] = array(
                                        'title' => $lib['title'],
                                        'value' => $lib['value'],
                                        'video' => $lib['video'],
                                        'directory' => $lib['directory']);
        }

        $library = array();
        $plan_documents = $this->session->get('plan_documents');
        if(count($plan_documents)) {
            $library[] = array(
                'name' => 'PLAN DOCUMENTS',
                'document' => $plan_documents
            );
        }

        // get Users and Videos and get an array of planid checked videos to display
        $query = $em->createQuery('SELECT l FROM classesclassBundle:plansLibraryUsers l WHERE l.planid = :planid')
            ->setParameter('planid',$spe_plan_id);
        $users = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $query = $em->createQuery('SELECT l FROM classesclassBundle:plansLibraryVideos l');
        $videos = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $planCheckedVideos = array();
        $videosArray = array();

        foreach($videos as $item) {

            $videosArray[$item['id']] = array("name" => $item['name']);
        }
        foreach($users as $item) {
            array_push($planCheckedVideos, $videosArray[$item['videoid']]["name"]);
        }

        if($planCheckedVideos) {
            foreach($libraryItems as $key => $libCollection) {

                $tinyCollection = array();
                foreach($libCollection as $row) {

                    if(in_array($row['value'], $planCheckedVideos)) {
                        $item = array(
                            'video' => $row['video'],
                            'directory' => $row['directory']
                        );
                        $tinyCollection[$row['title']] = $item;
                    }

                }
                if(count($tinyCollection)) {
                    $library[] = array(
                        'name' => $key,
                        'video' => $tinyCollection
                    );
                }
            }
        }

        $session->set('library', $library);
    }

    //Set Default Investment funds
    function setDefaultInvestmentsFunds(){
        $em = $this->em;
        $session = $this->session;
        $plan = $session->get('plan');
        $spe_plan_id = $plan['id'];
        $spe_user_id = $plan['userid'];

        $defaultInvestmentFund = null;
        if($plan['defaultInvestment'] == 1){
            $fund = $em->getRepository('classesclassBundle:plansFunds')->findOneBy(array('planid' => $spe_plan_id, 'userid' => $spe_user_id,'qdiaDefault' => 1));
            if($fund){
                $defaultInvestmentFund = array($fund->ticker,$fund->id,$fund->name,null,null,$fund->link,$fund->qdiaVideo,$fund->qdiaUrl);
            }
        }
        elseif($plan['defaultInvestment'] == 2){
            $retirement_needs = $session->get('retirement_needs');
            $RN_NumberOfYearsBeforeRetirement = $retirement_needs['RN_NumberOfYearsBeforeRetirement'];
            if($RN_NumberOfYearsBeforeRetirement) {
                $retire_year = (int)date('Y') + (int)$RN_NumberOfYearsBeforeRetirement;
                $closest = null;
                $result = $em->getRepository('classesclassBundle:tmdfDefault')->findOneBy(array('planid' => $spe_plan_id, 'userid' => $spe_user_id, ));
                if($result){
                    $set = explode("|",$result->investmentSet);
                    $funds = $em->getRepository('classesclassBundle:plansFunds')->findBy(array('id' => $set, 'planid' => $spe_plan_id, 'userid' => $spe_user_id, ));
                    foreach ($funds as $fund) {
                        if (preg_match('/\b\d{4}\b/', $fund->name, $year)) {
                            if( $closest == null || abs($retire_year - $closest) > abs($year[0] - $retire_year) )  {
                                $closest = $year[0];
                                $video = $fund->qdiaVideo ? $fund->qdiaVideo : $result->video;
                                $video_url = $fund->qdiaUrl ? $fund->qdiaUrl : $result->qdiaUrl;
                                $defaultInvestmentFund = array($fund->ticker,$fund->id,$fund->name,null,null,$fund->link,$video,$video_url);
                            }
                        }
                    }
                }
            }
        }
        elseif($plan['defaultInvestment'] == 3){
            if(isset($session->get('rkp')['plan_data']->rkpExtras->defaultInvestment)){
                $defaultInvestmentFund = (array) $session->get('rkp')['plan_data']->rkpExtras->defaultInvestment;
            }
        }

        if($defaultInvestmentFund){
            $session->set('defaultInvestmentFund', $defaultInvestmentFund);
        }
    }
    
    public function setErrorEmail(){
        $session = $this->session;
        $em = $this->em;
                
        $eplanId = $session->get('REQUEST')['id'];
                
        $query = $em->createQuery('SELECT z.errorNotificationEmail FROM classesclassBundle:plans z WHERE z.planid = :planid')
        ->setParameter('planid', $eplanId);
        $errorEmail = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $plan['errorNotificationEmail'] = (isset($errorEmail[0]['errorNotificationEmail']) && $errorEmail[0]['errorNotificationEmail'] !== '') ? $errorEmail[0]['errorNotificationEmail'] : '';  
        
        $session->set('plan', $plan);
    }    

    public function getErrorMessage($type, $error = null){
        $partner_id = $this->session->get('rkp')['partner_id'];
        $data = array();
        switch($type){
            case 'connection':
                $data = array(
                    'title' => 'SmartPlan Enterprise',
                    'message' => (strpos($partner_id, 'GRDN') === FALSE) ? 'Connection Error' : 'We cannot process your request at this time. Please try again later or contact Customer Support for assistance.',
                );
            break;
        
            case 'no_deferrals':
                $data = array(
                    'title' => 'SmartPlan Enterprise',
                    'message' =>$this->container->get('translator')->getMessages()['global'][$type],
                );                
                break;
            default:
                $data = array(
                    'title' => $type,
                    'message' => 'Oops ... it appears the application did not load correctly. Please refresh this page, and if the problem persists contact your plan administrator.'. $error
                );
        }
        return $data;
    }
    
    public function getPlanMessages($planid,$userid,$language) {
        $planMessages = array('planid'=>$planid, 'userid'=>$userid); 
        if ($language == "es" || substr_count($language,"_es"))
        {
            $this->getPlanMessagesLanguage($planMessages,$planid, $userid,"es");
        }
        $this->getPlanMessagesLanguage($planMessages,$planid, $userid,"en");
        return $planMessages;
    }
    public function getPlanMessagesLanguage(&$planMessages,$planid, $userid,$language) {
        $sql = "
            SELECT
                m.*,
                p.name
            FROM
                messagesPanes AS p
                LEFT JOIN plansMessages AS m ON m.messagesPanesId = p.id AND m.planid=:planid AND m.userid=:userid AND m.language =:language
        ";
        $messagesStmt = $this->em->getConnection()->prepare($sql);
        $messagesStmt->execute(array(':planid'=>$planid, ':userid'=>$userid,':language' => $language));
        while($row = $messagesStmt->fetch(\PDO::FETCH_ASSOC)) {
            if (!isset($planMessages[$row['name']])  || trim(empty($planMessages[$row['name']])))
            {
                $planMessages[$row['name']] = !empty($row['message']) ? $row['message'] : '';
                $planMessages[$row['name'] . 'Active'] = !empty($row['isActive']) ? $row['isActive'] : 0;
            }
        }
        return $planMessages;
    }    
    public function setPlanMessages()
    {
        $plan = $this->session->get('plan');
        $planid = $plan['id'];
        $userid= $plan['userid'];
        $this->session->set('messaging', $this->getPlanMessages($planid,$userid,$this->container->get('translator')->getLocale()));
    }

}
