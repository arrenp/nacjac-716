<?php
namespace Spe\AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

define('CurrentSocialSecurityOnly', 1);
define('EnhancedSocialSecurityOnly', 2);
define('PensionOnly', 3);
define('EnhancedSocialSecurityAndPension', 4);

class SmartService
{

    private $container;

    /**
     * @param mixed $container ContainerInterface
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return ContainerInterface symfony2 container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return Request object
     */
    protected function getRequest(){
        return $this->getContainer()->get("request_stack")->getCurrentRequest();
    }

    /**
     * @return Session object
     */
    protected function getSession(){
        return $this->getContainer()->get('session');
    }

    // Set Default Locale
    public function setLocale(){
        $session = $this->getContainer()->get('session');
        $request = $session->get('REQUEST');
        $locales = $session->get('language_files');
        $language = null;
        if(isset($request['lang']) && $request['lang'] && array_key_exists(strtolower($request['lang']),$locales)){
            $language = $request['lang'];
        }else{
            $plan_languages = $session->get('plan_languages');
            $default_language = strtolower($session->get('module')['language']);
            if(count($plan_languages) > 1 && $default_language) {
                if(array_key_exists($default_language,$plan_languages)){
                    $language = $default_language;
                }
                else{
                    $language = array_search(ucwords($default_language), $plan_languages);
                }
            }

            if(!$language){
                $language = current(array_keys($plan_languages));
            }

            $request['lang'] = $language;
            $session->set('REQUEST', $request);
        }

        if($language){
            if(isset($locales[strtolower($language)])){
                $this->getRequest()->setLocale($locales[strtolower($language)]);
                $this->getContainer()->get('translator')->setLocale($locales[strtolower($language)]);
            }
        }        
        if (substr_count(strtolower($language),"_es") > 0)
        {           
            $this->getContainer()->get('translator')->setFallbackLocale(array_merge(array('es'),$this->getContainer()->get('translator')->getFallbackLocales()));
        }
    }
    
    public function getAppLanguage()
    {
        $session = $this->getContainer()->get('session');
        $request = $session->get('REQUEST');
        return $request['lang'];       
    }
    
    public function getProfileLanguageWithoutPrefix($language = null)
    {
        if ($language == null)
        {
            $language = $this->getAppLanguage();
        }
        $languages  = $this->getContainer()->get("LanguagesService")->getAll();
        if (substr_count($language,"_") > 0)
        {
            $language = explode("_",$language)[1];
        }
        if (!isset($languages[$language]))
        {
            $language = "en";
        }
        return $language;
    }
    public function getProfileLanguageIdWithoutPrefix($language = null)
    {
        return $this->getContainer()->get("LanguagesService")->findOneBy(["name" => $this->getProfileLanguageWithoutPrefix($language)])->id;
    }

    // Partner ID mappings for UI customizations
    public function setClientCalling(){
        $clients = array(
            'AXA' => array('DST_TEST','DST_PROD','AXA_EDUCATE_PROD','AXA_DST'),
            'Envoy' => array('ENVOY_RELIUS_PROD'),
            'MOO' => array('MOO_EDUCATE_PROD'),
            'LNCLN' => array('LNCLN_DST_DEV','LNCLN_DST_QA','LNCLN_DST_PROD')
        );
        $clientCalling = 'default';
        $session = $this->getSession()->all();
        $partnerId = $session['rkp']['partner_id'];

        
        foreach ($clients as $key => $client) {
            if(in_array($partnerId,$client)) $clientCalling = $key;
        }
        $this->getSession()->set('client_calling', $clientCalling);
    }

    // Load Default Data
    public function setDefaultData(){
        $session = $this->getSession();
        $params = $session->get('REQUEST');
        $source = $this->getSession()->get('rkp')['rkp'];
         
        // Path to testing xml
        if(isset($params['testing'])) {
            //$session->set('testing', 'https://xml.smartplanenterprise.com/xml/'.$session->get('testing'));
            $session->set('testing', $params['testing'] ? 1 : 0);
        }

        $session->set('testingTrans',!empty($params['testingTrans']));
        // Session expiration time
        
        switch($source){
            case 'guardian':
                            $session->set('session_expiration', 1800); // 30 Minutes
				$session->set('session_time_to_update', 180);    // 3 Minute
                break;
            case 'omniamrts':
                $session->set('session_expiration', 1800); // 30 Minutes
				$session->set('session_time_to_update', 180);    // 3 Minute
                break;
            default:
                $session->set('session_expiration', 600); // 10 Minute
				$session->set('session_time_to_update', 60);    // 1 Minute
                break;
        }       
        

        //FlexPlan
        $session->set('flexPlan', isset($session->get('module')['flexPlan']) && $session->get('module')['flexPlan'] && isset($session->get('account')['flexPlanAvailable']) && $session->get('account')['flexPlanAvailable'] ? 1 : 0);

        // Set isACA
        $session->set('isACA',
            $session->get('account')['ACAon']
            && isset($session->get('rkp')['plan_data']->rkpExtras->isACA)
            && $session->get('rkp')['plan_data']->rkpExtras->isACA
            );

        $prefix = $session->get("account")["languagePrefix"] ? $session->get("account")["languagePrefix"] . "_" : "";
        //Set Translation Path
        $session->set('translation_path',$this->getContainer()->get('kernel')->locateResource('@SpeAppBundle/Resources/translations/'.strtolower($prefix) . $this->getRequest()->getLocale().'/'));

        // Media Base Path - CDN
        $media_base = 'https://c06cc6997b3d297a6674-653de9dab23a7201285f2586065c1865.ssl.cf1.rackcdn.com/';
        $session->set('media_base', $media_base);

        // Fund fact link
        $session->set('fund_fact_link', "http://quote.morningstar.com/fund/f.aspx?t=");

        // Video Base Path
        $session->set('video_path', $media_base. 'video/'.$session->get('video_folder').'/');

        // Audio Base Path
        $session->set('audio_path', $media_base. 'audio/standard/');

        // Library Video Base Path
        $session->set('library_video_path', $media_base. 'video/library/');

        // Popup Video Base Path
        $session->set('popup_video_path', $media_base. 'video/popup/');

        $session->set('investmentsConfirmationStatus', null);
        $session->set('realignmentConfirmationStatus', null);
        $session->set('contributionsConfirmationStatus', null);
        $session->set('catchupContributionConfirmationStatus', null);
        $session->set('beneficiaryConfirmationStatus', null);
        $session->set('ACAOptOutConfirmationStatus', null);
        $session->set('enrollmentConfirmationStatus', null);
    }

    //Smart401k check compare and update.
    function setSmart401kData($funds = []){   
        $plan = $this->getSession()->get('plan');
        if ((int)$plan['adviceStatus'] == 1)
        {
            $this->getSession()->set('debugAdviceFundsXml', $funds);
            $adviceFunds = $this->container->get("RecordkeeperAdviceFundsService")->findByWithHash(["planid" => $this->getSession()->get('plan')['id'],"status" => 1],"ticker");
            $this->getSession()->set('debugAdviceFundsDB', $adviceFunds);
            foreach ($funds as $fund)            
                unset($adviceFunds[$fund['adviceId']]);       
            $this->getSession()->set('debugAdviceFundsDBAfter', $adviceFunds);
            $this->getSession()->set('smart401k', (int) empty($adviceFunds));
        }
    }

    public function setBeneficiaries(){
        $session = $this->getSession();
        $beneficiaryList= isset($session->get('rkp')['plan_data']->rkpExtras->beneficiaryList) ? $session->get('rkp')['plan_data']->rkpExtras->beneficiaryList : [];
        $fixedPartBeneficiariesFormat = function($data){
            $data = (array)$data;
            foreach($data as $key => $value)
                if ($data[$key] === [])
                    $data[$key] = null;
            $data['id'] = uniqid("bene");
            $keys = ["firstName","lastName","ssn","relationship","gender","type","maritalStatus","percent","address1","address2","city","country","state","trustEstateCharityOther","foreignaddress","middleInitial",'dob','phone','zip','isSpouse','name'];
            foreach ($keys as $key)
                if (!isset($data[$key]))
                    $data[$key] = null;
            if (!empty($data['name']))
            {
                $nameExplode = explode(" ",trim($data['name']));
                $data['firstName'] = trim($nameExplode[0]);
                $data['lastName'] = trim($nameExplode[1]);
            }
            if (!empty($data['dob']))
            {
                $data['dob'] = date('m/d/Y', strtotime($data['dob']));              
            }
            return $data;
        };

        if($beneficiaryList){
            $beneficiaryList = get_object_vars($beneficiaryList);;
            $beneficiaryList = array_map($fixedPartBeneficiariesFormat, $beneficiaryList);
        }
        $beneficiaryListHash = [];
        foreach ($beneficiaryList as $beneficiary)
          $beneficiaryListHash[$beneficiary['id']] = $beneficiary;
        $session->set('beneficiaryList', $beneficiaryListHash);
        $session->set('beneficiaryListCopy', $beneficiaryListHash);
        $session->set('beneficiaryListDeleted', null);

        // Set Beneficiaries
        $session->set('beneficiary', isset($session->get('rkp')['plan_data']->rkpExtras->beneficiary) ? $this->addUniqueId($session->get('rkp')['plan_data']->rkpExtras->beneficiary) : null);
    }

    public function setPlaylist(){
        $session = $this->getSession();
        $plan = $session->get('plan');
        $module = $session->get('module');
        $clientCalling = $session->get('client_calling');
        $IRScode = $plan['IRSCode'];
        if ($plan['type'] == "smartenroll")
        $smartEnroll = 1;
        else
        $smartEnroll = 0;
        $playlist = array();
        $translate = $this->container->get('translator')->getMessages()['video'];
        $flexPlan =  $session->get('flexPlan');
        $planMedia =  $session->get('plan_media');

        $cookies = $this->getRequest()->cookies;
        $sections = $plan['sections'];

        if ($module['ATBlueprint']) {
            if ($session->get("ACInitialize")['WsPlan']['AllowArchitect'] == 'true') {
                $translate['welcome_0100'] = $translate['ATa_welcome_0100'];
                $playlist['investments']['videos'][] = $translate['ATa_investments_0320'];
            } else {
                $translate['welcome_0100'] = $translate['ATf_welcome_0100'];
                $playlist['investments']['videos'][] = $translate['investments_0600a'];
            }

            $arrayOfVideoNames = ['planBasics_0200', 'planBasics_0201a', 'planBasics_0201b', 'planBasics_0201c', 'planBasics_0202', 'planBasics_0203', 'planBasics_0204',
                'planBasics_0204a', 'planBasics_0204b', 'planBasics_0205', 'planBasics_0206', 'planBasics_0207', 'planBasics_0207a', 'planBasics_0207b', 'planBasics_0207c',
                'planBasics_0207d', 'planBasics_0208'];

            foreach ($arrayOfVideoNames as $videoName) {
                $translate[$videoName] = $translate['ATf_'.$videoName];
            }

            $playlist['atBlueprint']['videos'][] = $translate['ATb_atBlueprint_0300'];
            $playlist['atBlueprint']['completed'] = false;
            $playlist['atBlueprint']['warning'] = false;

            $playlist['investments']['completed'] = false;
            $playlist['investments']['warning'][] = $translate['investments_0699'];

        }

        //welcome Playlist
        if($cookies->has('visited') && !$smartEnroll && !$module['ATBlueprint']){
            if ($sections == 'riskProfileAA'){
                $playlist['welcome']['videos'][] = $translate['welcome_videos_0100_AAe'];
            } else if ($clientCalling == 'AXA') {
                if($IRScode == '457') {
                    $playlist['welcome']['videos'][] = $translate['welcome_0101_AX7'];
                } else {
                    $playlist['welcome']['videos'][] = $translate['welcome_0101_AXA'];
                }
            } else if ($clientCalling == 'LNCLN') {
                $playlist['welcome']['videos'][] = $translate['welcome_0101_LFG'];
            } else {
                $playlist['welcome']['videos'][] = $translate['welcome_0101'];
            }
        }
        else{
            $response = new Response();
            $response->headers->setCookie(new Cookie('visited', 1, time() + 3600, '/', null, true, true, false, "None"));
            $response->sendHeaders();
			if (isset($planMedia['enddate_sc']) && ($planMedia['enddate_sc'] > new \DateTime()) && isset($planMedia['welcome_sc']) && $planMedia['welcome_sc']) {
                $playlist['welcome']['videos'][] = $translate[$planMedia['welcome_sc']];
			}
			else {
				if(isset($planMedia['welcome']) && $planMedia['welcome']){
					$playlist['welcome']['videos'][] = $translate[$planMedia['welcome']];
				} elseif ($sections == 'riskProfileAA'){
					$playlist['welcome']['videos'][] = $translate['welcome_videos_0100_AAe'];
				} elseif ($clientCalling == 'AXA') {
					if($IRScode == '457') {
						$playlist['welcome']['videos'][] = $translate['welcome_0100_AX7'];
					} else {
						$playlist['welcome']['videos'][] = $translate['welcome_0100_AXA'];
					}
				} else if ($clientCalling == 'LNCLN') {
					$playlist['welcome']['videos'][] = $translate['welcome_0100_LFG'];
				} else {
					$playlist['welcome']['videos'][] = $translate['welcome_0100'];
				}
			}
        }

        $playlist['welcome']['completed'] = false;
        $playlist['welcome']['warning'] = false;

        //Plan Basics Playlist
        $deferralType = isset($session->get('rkp')['plan_data']->rkpExtras->deferralType) ? ($session->get('rkp')['plan_data']->rkpExtras->deferralType == 'yes' ? 'traditional+roth' : 'traditional') : $module['deferralType'];
        $matchingContributions = isset($session->get('rkp')['plan_data']->rkpExtras->matchingContributions) ? $session->get('rkp')['plan_data']->rkpExtras->matchingContributions : $module['matchingContributions'];
        $catchup = $module['catchup'];
        $data_source = $session->get('rkp')['rkp'];
        $enrollment = isset($session->get('rkp')['plan_data']->rkpExtras->enrollment) ? $session->get('rkp')['plan_data']->rkpExtras->enrollment : $module['enrollment'];
        if (strtolower($data_source) === 'relius') { // fix auto_enroll video playing in relius.
            $enrollment = $module['enrollment'];
        }
        $loans = isset($session->get('rkp')['plan_data']->rkpExtras->allowloans) && $session->get('rkp')['plan_data']->rkpExtras->allowloans ? 'yes' : $module['loans'];
        $vesting = $module['vesting'];

        $playlist['planBasics']['videos'][] = $translate['planBasics_0200'];

        if ($deferralType == "traditional") {
            if ($clientCalling == 'AXA') {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0201a_AXA'];
            } else {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0201a'];
            }
        } elseif ($deferralType == "traditional+roth") {
            if ($clientCalling == 'AXA') {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0201b_AXA'];
            } else {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0201b'];
            }
        } elseif ($deferralType == "roth") {
            if ($clientCalling == 'AXA') {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0201c_AXA'];
            } else {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0201c'];
            }
        }

        if($matchingContributions == 'yes') {
            $playlist['planBasics']['videos'][] = $translate['planBasics_0202'];
        }

        if($catchup == 'yes') {
            if ($clientCalling == 'AXA' && $IRScode == '457') {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0203_AX7'];
            } else {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0203'];
            }
        }

        $playlist['planBasics']['videos'][] = $translate['planBasics_0204'];

        if ($enrollment == 'auto_enroll') {
            if ($clientCalling == 'AXA' && $IRScode == '457') {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0204a_AX7'];
            } else if ($clientCalling == 'LNCLN') {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0204a_LFG'];
            } else {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0204a'];
            }
        } else {
            if ($clientCalling == 'AXA' && $IRScode == '457') {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0204b_AX7'];
            } else if ($clientCalling == 'LNCLN') {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0204b_LFG'];
            } else {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0204b'];
            }
        }
        if ($module['compounding'])
        {
            $playlist['planBasics']['videos'][] = $translate['planBasics_0205'];
        }

        if($loans == 'yes'){
            if($clientCalling == 'AXA' && $IRScode != '401k') {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0206_AX7'];
            } else {
                $playlist['planBasics']['videos'][] = $translate['planBasics_0206'];
            }
        }

        if($vesting == 'vest_gen'){
            $playlist['planBasics']['videos'][] = $translate['planBasics_0207'];
            $playlist['planBasics']['videos'][] = $translate['planBasics_0207a'];
        }elseif($vesting == 'vest_3yr'){
            $playlist['planBasics']['videos'][] = $translate['planBasics_0207'];
            $playlist['planBasics']['videos'][] = $translate['planBasics_0207b'];
        }elseif($vesting == 'vest_5yr'){
            $playlist['planBasics']['videos'][] = $translate['planBasics_0207'];
            $playlist['planBasics']['videos'][] = $translate['planBasics_0207c'];
        }elseif($vesting == 'vest_6yr'){
            $playlist['planBasics']['videos'][] = $translate['planBasics_0207'];
            $playlist['planBasics']['videos'][] = $translate['planBasics_0207d'];
        }
        
        $playlist['planBasics']['videos'][] = $translate['planBasics_0208'];

        $playlist['planBasics']['completed'] = false;
        $playlist['planBasics']['warning'] = false;

        $playlist['retirementNeeds']['videos'][] = $translate['retirementNeeds_0400'];
        $playlist['retirementNeeds']['completed'] = false;
        $playlist['retirementNeeds']['warning'] = false;

        if(!$flexPlan) {
            if ($module['riskBasedQuestionnaire']){
                if ($session->get('module')['stadionManagedAccountOn']) {
                    if ($session->get('rkp')['plan_data']->rkpExtras->stadionManagedAccountId != 1)
                        $playlist['riskProfile']['videos'][] = $translate['riskProfile_0500_SDN'];
                    else
                        $playlist['riskProfile']['videos'][] = $translate['riskProfile_0500b_SDN'];
                } else if ($session->get('smart401k')) {
                    $playlist['riskProfile']['videos'][] = $translate['riskProfile_0500b'];
                } else {
                    $playlist['riskProfile']['videos'][] = $translate['riskProfile_0500a'];
                }
            }
            $playlist['riskProfile']['completed'] = false;
            $playlist['riskProfile']['warning'][] = $translate['riskProfile_0599'];
        }

        $investment = array();
        if ($module['riskBasedQuestionnaire']){
            if ($sections == 'riskProfileAA'){
                $investment['videos'][] = $translate['investments_0600_AAe'];
            }
            else{
                $investment['videos'][] = $translate['investments_0600a'];
            }
        }
        $investment['completed'] = false;
        if ($module['riskBasedQuestionnaire']){
            if($flexPlan) {
                $investment['warning'][] = isset($translate['investments_0699_flex']) ? $translate['investments_0699_flex'] : $translate['investments_0699'];
            }else{
                $investment['warning'][] = $translate['investments_0699'];
            }
        }

        $contribution = array();
        $contribution['videos'][] = $translate['contributions_0700'];
        $contribution['completed'] = false;
        if($flexPlan){
            $contribution['warning'][] = isset($translate['contributions_0799_flex']) ? $translate['contributions_0799_flex'] : $translate['contributions_0799'];
        }else {
            $contribution['warning'][] = $translate['contributions_0799'];
        }

        if($flexPlan){
            $playlist['contributions'] = $contribution;
            $playlist['investments'] = $investment;
        } else if (!$module['ATBlueprint']) {
            $playlist['investments'] = $investment;
            $playlist['contributions'] = $contribution;
        }

        $session->set('playlist', $playlist);
    }

    public function setCommonVariables(){
        $session = $this->getSession()->all();
        $translate = $this->container->get('translator')->getMessages();
        $common = array();

        $common['environment'] = $this->container->get('kernel')->getEnvironment();
        $common['locale'] = $this->getRequest()->getLocale();
        $common['testing'] = isset($session['testing']) && $session['testing'] ? 1 : 0;
        $common['providerLogoImage'] = isset($session['plan']['providerLogoImage']) ? $session['plan']['providerLogoImage'] : null;
        $common['newVersion'] = isset($session['rkp']['plan_data']->rkpExtras->newVersion) ? $session['rkp']['plan_data']->rkpExtras->newVersion : null;
        $common['session_id'] = isset($session['rkp']['session_id']) ? $session['rkp']['session_id'] : null;
        $common['data_source'] = isset($session['rkp']['rkp']) ? $session['rkp']['rkp'] : null;
        $common['post_calls'] = isset($session['rkp']['post_calls']) ? $session['rkp']['post_calls'] : null;
        $common['partner_id'] = isset($session['rkp']['partner_id']) ? $session['rkp']['partner_id'] : null;
        $common['user_id'] = isset($session['plan']['userid']) ? $session['plan']['userid'] : null;
        $common['planBalance'] = isset($session['rkp']['plan_data']->planBalance)  ? (double) $session['rkp']['plan_data']->planBalance : null;
        $common['client_calling'] = isset($session['client_calling']) ? $session['client_calling'] : 'default';
        $common['pipContent'] = isset($session['plan']['pipContent']) ? $session['plan']['pipContent'] : 0;
        $common['pipPosition'] = isset($session['plan']['pipPosition']) ? $session['plan']['pipPosition'] : 0;
        $common['pipCompleted'] = isset($session['pipCompleted']) ? $session['pipCompleted'] : 0;
        $common['adviceStatus'] = isset($session['smart401k']) ? $session['smart401k'] : null;
        $common['riskType'] = isset($session['plan']['riskType']) ? $session['plan']['riskType'] : null;
        $common['IRSCode'] = isset($session['plan']['IRSCode']) ? $session['plan']['IRSCode'] : null;
        $common['defaultInvestment'] = isset($session['plan']['defaultInvestment']) ? $session['plan']['defaultInvestment'] : null;
        $common['ACAon'] = isset($session['isACA']) && $session['isACA'] ? $session['isACA'] : 0;
        $common['planName'] = isset($session['rkp']['plan_data']->planName) ? $session['rkp']['plan_data']->planName : '';
        $common['firstName'] = isset($session['rkp']['plan_data']->firstName) ? $session['rkp']['plan_data']->firstName : null;
        $common['lastName'] = isset($session['rkp']['plan_data']->lastName) ? $session['rkp']['plan_data']->lastName : null;
        $common['address'] = isset($session['rkp']['plan_data']->address) ? $session['rkp']['plan_data']->address : null;
        $common['city'] = isset($session['rkp']['plan_data']->city) ? $session['rkp']['plan_data']->city : null;
        $common['region'] = isset($session['rkp']['plan_data']->region) ? $session['rkp']['plan_data']->region : null;
        $common['postalCode'] = isset($session['rkp']['plan_data']->postalCode) ? $session['rkp']['plan_data']->postalCode : null;
        $common['maritalStatus'] = isset($session['rkp']['plan_data']->maritalStatus) ? $session['rkp']['plan_data']->maritalStatus : null;
        $common['gender'] = isset($session['rkp']['plan_data']->gender) ? $session['rkp']['plan_data']->gender : null;
        $common['ssn'] = isset($session['rkp']['plan_data']->partSSN) ? $session['rkp']['plan_data']->partSSN : null;
        $common['asOfDate'] = isset($session['rkp']['plan_data']->asOfDate) && $session['rkp']['plan_data']->asOfDate ? date('m/d/Y', strtotime($session['rkp']['plan_data']->asOfDate)) : null;
        $common['dateOfBirth'] = isset($session['rkp']['plan_data']->dateOfBirth) && $session['rkp']['plan_data']->dateOfBirth ? date('m/d/Y', strtotime($session['rkp']['plan_data']->dateOfBirth)) : null;
        $common['email'] = isset($session['rkp']['plan_data']->email) ? $session['rkp']['plan_data']->email : null;
        $common['phone'] = isset($session['rkp']['plan_data']->phone) ? $session['rkp']['plan_data']->phone : null;     /* todo : session location ?? */
        $common['availability'] = isset($session['rkp']['plan_data']->availability) ? $session['rkp']['plan_data']->availability : null;    /* todo : session location ?? */
        $common['video'] = isset($session['REQUEST']['video']) ? $session['REQUEST']['video'] : 'on';
        $common['navigationLocked'] =( isset($session['rkp']['plan_data']->navigationLocked) ? $session['rkp']['plan_data']->navigationLocked : null );
        $common['age'] = $common['dateOfBirth'] ? date_diff(date_create($common['dateOfBirth']), date_create('today'))->y : null;
        $common['retirementNeedsVersion'] = (isset($session['module']['retirementNeedsVersion']) && $session['module']['retirementNeedsVersion']) ? $session['module']['retirementNeedsVersion'] : ((isset($session['default_modules']['retirementNeedsVersion']) && $session['default_modules']['retirementNeedsVersion']) ? $session['default_modules']['retirementNeedsVersion'] : 'standard');
        $common['flexPlan'] =  isset($session['flexPlan']) ? $session['flexPlan'] : 0;
        $common['session_expiration'] = isset($session['session_expiration']) ? $session['session_expiration'] : null;
        $common['session_time_to_update'] = isset($session['session_time_to_update']) ? $session['session_time_to_update'] : null;

        $age = isset($session['rkp']['plan_data']->dateOfBirth) ? floor((strtotime(date('m/d/Y')) - strtotime($session['rkp']['plan_data']->dateOfBirth)) / 31556926) : 65;
        $age = $age > 0 ? $age : 0;
        $retireAge = isset($session['rkp']['plan_data']->retireAge) && $session['rkp']['plan_data']->retireAge ? $session['rkp']['plan_data']->retireAge : ($age > 62) ? $age + 3 : 65;
        $common['retireAge'] = $retireAge;

        $common['enrollment'] = isset($session['rkp']['plan_data']->rkpExtras->enrollment) ? $session['rkp']['plan_data']->rkpExtras->enrollment : null;
        $common['enrollmentPath'] = isset($session['rkp']['plan_data']->rkpExtras->enrollmentPath) ? $session['rkp']['plan_data']->rkpExtras->enrollmentPath : null;

        $common['thankyouAudio'] = isset($session['module']['thankyouAudio']) ? $session['module']['thankyouAudio'] : 0;

        $common['allowecomm'] = isset($session['rkp']['plan_data']->rkpExtras->allowecomm) ? (int) $session['rkp']['plan_data']->rkpExtras->allowecomm : null;
        $common['spousalWaiverOn'] = isset($session['module']['spousalWaiverOn']) ? $session['module']['spousalWaiverOn'] : null;
        $common['spousalWaiverUrl'] = isset($session['module']['spousalWaiverURL']) ? $session['module']['spousalWaiverURL'] : null;
        $common['spousalWaiverFormStatus'] = null;

        $common['customerServiceNumber'] = (isset($session['module']['customerServiceNumber']) && $session['module']['customerServiceNumber']) ? $session['module']['customerServiceNumber'] : ((isset($session['default_modules']['customerServiceNumber']) && $session['default_modules']['customerServiceNumber']) ? $session['default_modules']['customerServiceNumber'] : '');

        if(isset($session['plan']['sniff_host']) && $session['plan']['sniff_host'] && isset($common['referer']) && $common['referer'] && $common['enrollmentPath']){
            $parsePath = parse_url($common['enrollmentPath']);
            $common['enrollmentPath'] = $common['referer'];
            if($parsePath['path']){
                $common['enrollmentPath'] .= '/' . $parsePath['path'];
            }
            if($parsePath['query']){
                $common['enrollmentPath'] .= '/?' . $parsePath['query'];
            }
        }

        $common['initialEnrollment'] = isset($session['rkp']['plan_data']->rkpExtras->initialEnrollment) ? $session['rkp']['plan_data']->rkpExtras->initialEnrollment : null;
        $common['updateRealignment'] = isset($session['plan']['updateRealignment']) ? $session['plan']['updateRealignment'] : null;

        $common['redirectWebsiteAddress'] = isset($session['plan']['redirectWebsiteAddress']) ? $session['plan']['redirectWebsiteAddress'] : null;
		$common['timeoutWebsiteAddress'] = isset($session['plan']['timeoutWebsiteAddress']) ? $session['plan']['timeoutWebsiteAddress'] : null;
        $common['enrollmentRedirectOn'] = isset($session['account']['enrollmentRedirectEnabled']) && $session['account']['enrollmentRedirectEnabled']
                && isset($session['plan']['enrollmentRedirectOn']) && $session['plan']['enrollmentRedirectOn'];
        $common['enrollmentRedirectAddress'] = isset($session['plan']['enrollmentRedirectAddress']) ? $session['plan']['enrollmentRedirectAddress'] : null;

        $common['planAllowsElections'] = isset($session['rkp']['plan_data']->planAllowsElections) ? (int) $session['rkp']['plan_data']->planAllowsElections : null;
        $common['planAllowsRealignments'] = isset($session['rkp']['plan_data']->planAllowsRealignments) ? (int) $session['rkp']['plan_data']->planAllowsRealignments : null;
        $common['planAllowsRebalance'] = isset($session['rkp']['plan_data']->planAllowsRebalance) ? (int) $session['rkp']['plan_data']->planAllowsRebalance : null;
        $common['planAllowsRebalanceFrequency'] = isset($session['rkp']['plan_data']->planAllowsRebalanceFrequency) ? (int) $session['rkp']['plan_data']->planAllowsRebalanceFrequency : null;
        $common['rebalanceFrequency'] = isset($session['rkp']['plan_data']->rebalanceFrequency) ? (string) $session['rkp']['plan_data']->rebalanceFrequency : "";        
        $common['planAllowsDeferrals'] = isset($session['rkp']['plan_data']->planAllowsDeferrals) ? (int) $session['rkp']['plan_data']->planAllowsDeferrals : null;

        $common['sections'] = isset($session['plan']['sections']) ? $session['plan']['sections'] : null;
        $common['isFundLink'] = $session['plan']['fundLink'];
        $common['fundFactLink'] = $session['fund_fact_link'];

        $common['showCatchup'] = 0;
        $common['sc'] = 0;
        if(isset($session['rkp']['plan_data']->dateOfBirth) && (int) date('Y') - (int) date('Y', strtotime($session['rkp']['plan_data']->dateOfBirth)) >= 50) {     // change to check if turning 50 this year
            $common['showCatchup'] = 1;
            $common['sc'] = 1;
        }

        if(strtolower($session['rkp']['rkp']) == 'educate'){
            $common['pre'] = (int)in_array($session['module']['deferralType'],array('traditional+roth','traditional',''));
            $common['preMin'] = $session['module']['minimumContribution'] ? $session['module']['minimumContribution'] : 0;
            $common['preMax'] =  $session['module']['maximumContribution'] ? $session['module']['maximumContribution'] : 25;
            $common['rothMin'] = $session['module']['minimumContributionRoth'] ? $session['module']['minimumContributionRoth'] : 0;
            $common['rothMax'] =  $session['module']['maximumContributionRoth'] ? $session['module']['maximumContributionRoth'] : 25;
            $common['postMin'] = $session['module']['minimumContributionPostTax'] ? $session['module']['minimumContributionPostTax'] : 0;
            $common['postMax'] =  $session['module']['maximumContributionPostTax'] ? $session['module']['maximumContributionPostTax'] : 25;
            $common['roth'] = $session['module']['deferralType'] == 'traditional+roth' || $session['module']['deferralType'] == 'roth' ? 1 : 0;
            $common['post'] = $session['module']['postTax'];
            $common['preCurrent'] = $common['rothCurrent'] = $common['postCurrent'] = 0;
            $common['prePctPre'] = $common['preDlrPre'] = $common['rothPctPre'] = $common['rothDlrPre'] = $common['postPctPre'] = $common['postDlrPre'] = 1;

        }else if(strtolower($session['rkp']['rkp']) == 'relius') {
            $deferrals = isset($session['rkp']['plan_data']->deferrals) ? $session['rkp']['plan_data']->deferrals : null;
            $pre = array();
            $roth = array();
            foreach($deferrals as $deferral) {
                if ($deferral[1] == 'D') {
                    foreach ($deferral as $options) {
                        $pre[] = $options;
                    }
                } else if ($deferral[1] == 'R') {
                    foreach ($deferral as $options) {
                        $roth[] = $options;
                    }
                }
            }

            if ($pre) {
                $common['pre'] = true;
                $common['preMin'] = isset($pre[2]) ? $pre[2] : 0;
                $common['preMax'] = isset($pre[3]) ? $pre[3] : 25;
                $common['preCurrent'] = isset($pre[4]) ? $pre[4] : 0;
            }

            if ($roth) {
                $common['roth'] = true;
                $common['rothMin'] = isset($roth[2]) ? $roth[2] : 0;
                $common['rothMax'] = isset($roth[3]) ? $roth[3] : 25;
                $common['rothCurrent'] = isset($roth[4]) ? $roth[4] : 0;
            }

            $common['post'] = isset($deferrals[2]) ? 1 : 0;
            $common['postMin'] = isset($deferrals[2][2]) ? $deferrals[2][2] : 0;
            $common['postMax'] = isset($deferrals[2][3]) ? $deferrals[2][3] : 25;
            $common['postCurrent'] = isset($deferrals[2][4]) ? $deferrals[2][4] : 0;

            if($session['isACA']){
                $common['preCurrent'] = (isset($pre[6]) && $pre[3] >= $pre[6]) ? $pre[6] : 0;
                $common['rothCurrent'] = (isset($roth[6]) && $roth[3] >= $roth[6]) ? $roth[6] : 0;
                $common['postCurrent'] = (isset($deferrals[2][6]) && $deferrals[2][3] >= $deferrals[2][6]) ? $deferrals[2][6] : 0;
            }

            $common['prePctPre'] = isset($pre[7]) ? $pre[7] : 1;
            $common['preDlrPre'] = isset($pre[8]) ? $pre[8] : 1;
            $common['rothPctPre'] = isset($roth[7]) ? $roth[7] : 1;
            $common['rothDlrPre'] = isset($roth[8]) ? $roth[8] : 1;
            $common['postPctPre'] = isset($deferrals[2][7]) ? $deferrals[2][7] : 1;
            $common['postDlrPre'] = isset($deferrals[2][8]) ? $deferrals[2][8] : 1;

        } else {
            $common['pre'] = true;//there is currently no rkp logic for non pre for not relius
            $deferrals = isset($session['rkp']['plan_data']->deferrals) ? $session['rkp']['plan_data']->deferrals : null;

            // kmc 2018-05-17 BSD-1051 bandaid to keep things running until the root cause of defferals being an object can be found
            $deferrals = (array)$deferrals;
            
            $common['preMin'] = isset($deferrals[0][2]) ? $deferrals[0][2] : 0;
            $common['preMax'] = isset($deferrals[0][3]) ? $deferrals[0][3] : 25;
            $common['preCurrent'] = isset($deferrals[0][4]) ? $deferrals[0][4] : 0;

            $common['roth'] = isset($deferrals[1]) ? 1 : 0;
            $common['rothMin'] = isset($deferrals[1][2]) ? $deferrals[1][2] : 0;
            $common['rothMax'] = isset($deferrals[1][3]) ? $deferrals[1][3] : 25;
            $common['rothCurrent'] = isset($deferrals[1][4]) ? $deferrals[1][4] : 0;

            $common['post'] = isset($deferrals[2]) ? 1 : 0;
            $common['postMin'] = isset($deferrals[2][2]) ? $deferrals[2][2] : 0;
            $common['postMax'] = isset($deferrals[2][3]) ? $deferrals[2][3] : 25;
            $common['postCurrent'] = isset($deferrals[2][4]) ? $deferrals[2][4] : 0;

            if($session['isACA']){
                $common['preCurrent'] = (isset($deferrals[0][6]) && $deferrals[0][3] >= $deferrals[0][6]) ? $deferrals[0][6] : 0;
                $common['rothCurrent'] = (isset($deferrals[1][6]) && $deferrals[1][3] >= $deferrals[1][6]) ? $deferrals[1][6] : 0;
                $common['postCurrent'] = (isset($deferrals[2][6]) && $deferrals[2][3] >= $deferrals[2][6]) ? $deferrals[2][6] : 0;
            }

            $common['prePctPre'] = isset($deferrals[0][7]) ? $deferrals[0][7] : 1;
            $common['preDlrPre'] = isset($deferrals[0][8]) ? $deferrals[0][8] : 1;
            $common['rothPctPre'] = isset($deferrals[1][7]) ? $deferrals[1][7] : 1;
            $common['rothDlrPre'] = isset($deferrals[1][8]) ? $deferrals[1][8] : 1;
            $common['postPctPre'] = isset($deferrals[2][7]) ? $deferrals[2][7] : 1;
            $common['postDlrPre'] = isset($deferrals[2][8]) ? $deferrals[2][8] : 1;
        }
        $common['rothCurrent'] = (strlen($common['rothCurrent']) === 0) ? 0 : $common['rothCurrent'];
        $common['preCurrent'] = (strlen($common['preCurrent']) === 0) ? 0 : $common['preCurrent'];
        $common['postCurrent'] = (strlen($common['postCurrent']) === 0) ? 0 : $common['postCurrent'];
        
        $defTypes = array("pre","roth","post");
        $common['defTypes'] = $defTypes;
        $common['autoIncrease']['enabled'] = false;
        foreach ($defTypes as $key => $type)
        {
            if ($deferrals[$key][10]->user || $deferrals[$key][10]->provider )
            {
                $common['autoIncrease']['enabled'] = true;
                $common['autoIncrease']['user'] = $deferrals[$key][10]->user;
                $autoIncreaseXml = (array)$deferrals[$key][10]->xml;
                $common['autoIncrease']['data'][$type] =$autoIncreaseXml;
                $common['autoIncrease']['frequency'] = $deferrals[$key][10]->frequency;
                $common['autoIncrease']['occurrences'] = $deferrals[$key][10]->occurrences;
                $common['autoIncrease']['enddate'] = $deferrals[$key][10]->enddate;
                
                $increaseSections = array("autoincreasemaxpct","autoincreasemaxamount");
                foreach ($increaseSections as $increaseSection)
                {
                    if (!isset($common['autoIncrease'][$increaseSection]))
                    {
                        $common['autoIncrease'][$increaseSection] = 0;
                    }
                    if ($autoIncreaseXml[$increaseSection] != null)
                    {
                        $common['autoIncrease'][$increaseSection] += $autoIncreaseXml[$increaseSection];
                    }
                }
            }
        }
        $this->getSession()->set("autoIncrease",$common['autoIncrease']);
        
        if(in_array($session['rkp']['rkp'], array('educate','relius','retrev'))){
            $common['mode'] = ($session['module']['deferralFormat'] == 'dollar') ? 'dlr' : ($session['module']['deferralFormat'] == 'percent' ? 'pct' : 'option');
        }else{
            $common['planSetting'] = isset($session['rkp']['plan_data']->planDeferralSetting) ? $session['rkp']['plan_data']->planDeferralSetting : 0;
            $common['planAllowsCatchup'] = isset($session['rkp']['plan_data']->planAllowsCatchupDeferral) ? $session['rkp']['plan_data']->planAllowsCatchupDeferral : 0;
            $common['planAllowsViewOnly'] = isset($session['rkp']['planAllowsViewOnly']) ? $session['rkp']['planAllowsViewOnly'] : 0;
            $common['planCatchupSetting'] = isset($session['rkp']['plan_data']->planCatchupDeferralSetting) ? $session['rkp']['plan_data']->planCatchupDeferralSetting : 0;
            $common['mode'] = $common['planSetting'] == 2 ? 'option' :(($common['planSetting'] == 1) ? 'dlr' : 'pct');
            $common['showCatchup'] = ($common['planAllowsCatchup'] && $common['sc']) ? 1 : 0;
        }
        $common['catchupChange'] = $common['showCatchup'] && isset($common['planAllowsCatchup']) && $common['planAllowsCatchup'];

        $limit = $session['limits'];
        $common['limit_402'] = isset($limit['limit402g']) ? $limit['limit402g'] : null;
        $common['limit_50y'] = isset($limit['fiftyYears']) ? $limit['fiftyYears'] : null;
        $common['limit_402_50'] = $common['limit_402'] + $common['limit_50y'];
        $common['limit_402_per_month'] = floor($common['limit_402'] / 12);
        $common['limit_402_50_per_month'] = floor($common['limit_402_50'] / 12);
        $common['lastThreeYears'] = isset($limit['lastThreeYears']) ? $limit['lastThreeYears'] : null;
        $common['longTermEm'] = isset($limit['longTermEm']) ? $limit['longTermEm'] : null;

        $common['matchPercent'] = isset($session['module']['matchingPercent']) ? $session['module']['matchingPercent'] : 0;
        $common['matchCap'] = isset($session['module']['matchingCap']) ? $session['module']['matchingCap'] : 0;
        $common['secondaryMatchPercent'] = isset($session['module']['secondaryMatchingPercent']) ? $session['module']['secondaryMatchingPercent'] : 0;
        $common['secondaryMatchCap'] = isset($session['module']['secondaryMatchingCap']) ? $session['module']['secondaryMatchingCap'] : 0;

        $common['matchingContributions'] = $session['module']['matchingContributions'] ;
        $common['matchRateA'] = isset($session['module']['matchingPercent']) ? $session['module']['matchingPercent'] : 0;
        $common['matchCapA'] = isset($session['module']['matchingCap']) ? $session['module']['matchingCap'] : 0;
        $common['matchRateB'] = isset($session['module']['secondaryMatchingPercent']) ? $session['module']['secondaryMatchingPercent'] : 0;
        $common['matchCapB'] = isset($session['module']['secondaryMatchingCap']) ? $session['module']['secondaryMatchingCap'] : 0;
        $common['showMatchDescriptions'] = false;
        if ($common['matchingContributions'] === 'yes')
        {
            foreach (['A','B'] as $matchletter)
            {
                foreach(['matchRate','matchCap'] as $matchvalue)
                {
                    if ($common[$matchvalue.$matchletter] > 0)
                    {
                        $common['showMatchDescriptions']  = true;
                    }
                }
            }            
        }
        $common['deferralMinRateAgg'] = isset($session['rkp']['plan_data']->deferralMinRateAgg) ? $session['rkp']['plan_data']->deferralMinRateAgg : 0;
        $common['deferralMaxRateAgg'] = isset($session['rkp']['plan_data']->deferralMaxRateAgg) ? $session['rkp']['plan_data']->deferralMaxRateAgg : 0;

        // select correct audio files for retirement needs
        $retirementNeedsSuffix = '';
        $retirementNeedsFlow = $session['module']['retirementNeedsFlow'];
        if ($retirementNeedsFlow == EnhancedSocialSecurityOnly) {
            $retirementNeedsSuffix = 'a';
        } else if ($retirementNeedsFlow == PensionOnly) {
            $retirementNeedsSuffix = 'b';
        } else if ($retirementNeedsFlow == EnhancedSocialSecurityAndPension) {
            $retirementNeedsSuffix = 'c';
        }
        $common['RN_Audio'] = array(
            $translate['audio']['retirementNeeds_0401'],
            $translate['audio']['retirementNeeds_0402-0403' . $retirementNeedsSuffix],
            $translate['audio']['retirementNeeds_0404'],
            $translate['audio']['retirementNeeds_0405a'],
            $translate['audio']['retirementNeeds_0405b'],
            $translate['audio']['retirementNeeds_0406'],
            $translate['audio']['retirementNeeds_0407'],
            $translate['audio']['retirementNeeds_0408']
        );

        $common['I_Audio'] = array(
            $translate['audio']['investments_0600aNI'], //0
            $translate['audio']['investments_0600bNI'], //1
            $translate['audio']['investments_0601a'], //2
            $translate['audio']['investments_0602_SMA_VO'], //3
            $translate['audio']['investments_RealignVO'], //4
            $translate['audio']['investments_riskBased'], //5
            $translate['audio']['investments_TDP'], //6
            $translate['audio']['investments_CustomChoiceINFO'], //7
            $translate['audio']['investments_0601a_MOe'], //8
            $translate['audio']['investments_0601a_VAe'], //9 Audio for multiple Target Date Investment Page | Partner = Mututal of Ohmaha only.
            $translate['audio']['investments_0601d_SPE_091912'], //10 Default ACA landing audio
            $translate['audio']['investments_MoreInfo_090512'], //11 Default ACA explanation audio
            $translate['audio']['investments_riskBased_AXA'], //12 AXA Risk Based
            $translate['audio']['investments_TDP_AXA'], //13 AXA Target Date
            $translate['audio']['investments_riskBased_AX7'], //14 AXA 457 Risk Based
            $translate['audio']['investments_TDP_AX7'], //15 AXA 457 Target Date
            $translate['audio']['investments_customChoice_AX7'], //16 AXA 457 Custom Choice
            $translate['audio']['investments_0600b_AX7'], //17
            $translate['audio']['investments_0601a_LFG'], //18 Lincoln Financial Combination target-date/risk portfolios​ audio
            $translate['audio']['investments_CustomChoice_Omni'], //19
            $translate['audio']['investments_CustomChoiceCalculated_Omni'], //20
            $translate['audio']['investments_0600a_SPe'], //21 RETREV: Investment Models
            $translate['audio']['investments_0602b'], //22 RETREV: Investment Models Selector
            $translate['audio']['investments_0601_GRD'], //23 Guardian: Managed accounts
            $translate['audio']['investments_0601x_GRD'], //24 Guardian: Managed accounts Menu
        );
        $common['C_Audio'] = array($translate['audio']['contributions_0702'], $translate['audio']['contributions_0702a'], $translate['audio']['contributions_0702_DO'], $translate['audio']['contributions_0701x_SPe_090512']);
        $common['P_Audio'] = array(
            $translate['audio']['profile_0706AXA'], //0
            $translate['audio']['profile_0719AXA'],
            $translate['audio']['profile_0802_CANCEL_AXA'],
            $translate['audio']['profile_0719_DO'],
            $translate['audio']['profile_0703'],
            $translate['audio']['profile_0706'],
            $translate['audio']['profile_0801a_SPn'],
            $translate['audio']['profile_0706NI_AXA'],
            $translate['audio']['profile_0780_AXn'],
            $translate['audio']['profile_0780a_SPe'],
            $translate['audio']['profile_0780b_SPe'],
            $translate['audio']['profile_0802_EXe'],
            $translate['audio']['profile_0780_SPe'],    //12
            $translate['audio']['profile_0802_SPe'],
            $translate['audio']['profile_thank_you_0821'],
            $translate['audio']['profile_0780a_SPb'],   //15
            $translate['audio']['profile_0801a_SPb']    //16
        );
        $common['EP_Audio'] = $common['initialEnrollment'] == 'YES' ? 9 : 10;
        $common['PR_Audio'] = null;
        
        $common['RP_Audio'] = $translate['audio']['riskProfile_0500a'];
        if ($session['module']['stadionManagedAccountOn']) {
            $common['RP_Audio'] = $translate['audio']['riskProfile_0500a_SDN'];
        }
		if (isset($session['plan_media']['enddate_sc']) && ($session['plan_media']['enddate_sc'] > new \DateTime()) && isset($session['plan_media']['goodbye_sc']) && $session['plan_media']['goodbye_sc']) {
			$common['thankYouVideo'] = $translate['video'][$session['plan_media']['goodbye_sc']];
		}
		else {
			if(isset($session['plan_media']['goodbye']) && $session['plan_media']['goodbye']){
				$common['thankYouVideo'] = $translate['video'][$session['plan_media']['goodbye']];
			}elseif($session['plan']['sections'] == 'riskProfileAA'){
				$common['thankYouVideo'] = $translate['video']['thankYou_0820_AAe'];
			}elseif($common['data_source'] == 'axa'){
				$common['thankYouVideo'] = $translate['video']['thankYou_0820_AXA'];
			}elseif($common['data_source'] == 'lncln') {
                $common['thankYouVideo'] = $translate['video']['thankYou_0820_LFG'];
            }elseif($session['module']['ATBlueprint']) {
                $common['thankYouVideo'] = $translate['video']['ATf_thankYou_0820'];
			}else{
				$common['thankYouVideo'] = $translate['video']['thankYou_0820'];
			}
		}

        if(isset($session['rkp']['partner_id']) && $session['rkp']['partner_id'] != 'SNWSTLW_EDUCATE_PROD'){
            if($common['IRSCode'] == "403b"){
                $common['PR_Audio'] = $common['P_Audio'][7];
            }else if($common['data_source'] == 'educate'){
                $common['PR_Audio'] = $common['P_Audio'][5];
            }else{
                $common['PR_Audio'] = $common['P_Audio'][0];
            }
        }

        if($session['rkp']['partner_id'] <> 'SNWSTLW_EDUCATE_PROD' && ($common['client_calling'] <> "AXA" && $common['IRSCode'] <> "403b")){
            if($session['plan']['type']  == "smartenroll"){
                $common['PU_Audio'] = $common['P_Audio'][16];
            }elseif($common['data_source'] == 'educate'){
                $common['PU_Audio'] = $common['P_Audio'][6];
            }else{
                $common['PU_Audio'] = isset($common['planAllowsDeferrals']) && $common['planAllowsDeferrals'] ? $common['P_Audio'][1] : $common['P_Audio'][3];
            }
        }

        $common['updateEPProfile'] =  null;
        $common['updateProfileBeneficiary'] =  null;        /* todo : updateBeneficiary in rkp plan_data */
        $common['beneficiaries'] = isset($session['rkp']['plan_data']->rkpExtras->allowBeneficiaryChange) ? (int) $session['rkp']['plan_data']->rkpExtras->allowBeneficiaryChange :
            ((!isset($session['module']['beneficiaries']) || $session['module']['beneficiaries'] == '' || $session['module']['beneficiaries'] == 'off') ? 0 : ($session['module']['beneficiaries'] == 'on_optional' ? 1 : 2));
        $common['planRequiresBeneficiary'] = isset($session['rkp']['plan_data']->rkpExtras->planRequiresBeneficiary) ? $session['rkp']['plan_data']->rkpExtras->planRequiresBeneficiary : 0;
        $common['autoShowBeneficairy'] = !empty($session['module']['beneficiaryAutoAdd']) ? 1:0 ;
        $common['edelivery'] = 0;
        $common['ECOMMModal'] = isset($session['rkp']['plan_data']->rkpExtras->ECOMMModal) ? $session['rkp']['plan_data']->rkpExtras->ECOMMModal : 0;
        $common['dateOfHire'] = '';
        $common['marketing_opt_in'] = 0;
        $common['ECOMMModalChecked'] = isset($session['rkp']['plan_data']->rkpExtras->ECOMMModalChecked) ? $session['rkp']['plan_data']->rkpExtras->ECOMMModalChecked : 0;
        $common['allowContributionChange'] = isset($session['rkp']['plan_data']->rkpExtras->allowContributionChange) ? $session['rkp']['plan_data']->rkpExtras->allowContributionChange : 1;
        $common['allowElectionChange'] = isset($session['rkp']['plan_data']->rkpExtras->allowElectionChange) ? $session['rkp']['plan_data']->rkpExtras->allowElectionChange : 1;
        $common['profileId'] = $session['profileId'];
        $minTotalDefTypes = 0;
        foreach ($defTypes as $defType )
        {
            if ($common[$defType.'Min'] > 0)
            {
                $minTotalDefTypes++;
            }
        }
        $common['contributionsSliderCheckbox'] = in_array($session['rkp']['rkp'],array("alerus","relius")) && $minTotalDefTypes > 1;
		$common['rolloverVideo'] = $translate['video']['rollover_0810_SPe'];
		$common['rolloverAudio'] = $translate['audio']['rollover_0811'];
        $autoService = $this->container->get("AutosService");
        $autoService->set(["planid" => $this->getSession()->get('plan')['id']]);
        $common['autoNavigation'] = !empty($autoService->getData("AutoEnroll")->enabled) || !empty($autoService->getData("AutoEscalate")->enabled)  || !empty($autoService->getData("AutoIncrease")->enabled)?1:0;
        $common['autoIncreaseDependentOnAutoEnroll'] = isset($session['rkp']['plan_data']->rkpExtras->autoIncreaseDependentOnAutoEnroll) ? $session['rkp']['plan_data']->rkpExtras->autoIncreaseDependentOnAutoEnroll:1;


        //Plan Basics Playlist
        $module = $session['module'];
        $deferralType = isset($session['rkp']['plan_data']->rkpExtras->deferralType) ? ($session['rkp']['plan_data']->rkpExtras->deferralType == 'yes' ? 'traditional+roth' : 'traditional') : $module['deferralType'];
        $matchingContributions = isset($session['rkp']['plan_data']->rkpExtras->matchingContributions) ? $session['rkp']['plan_data']->rkpExtras->matchingContributions : $module['matchingContributions'];
        $catchup = $module['catchup'];
        $enrollment = isset($session['rkp']['plan_data']->rkpExtras->enrollment) ? $session['rkp']['plan_data']->rkpExtras->enrollment : $module['enrollment'];
        $vesting = $module['vesting'];
        $loans = isset($session['rkp']['plan_data']->rkpExtras->allowloans) && $session['rkp']['plan_data']->rkpExtras->allowloans ? 'yes' : $module['loans'];
        $compounding = ($module['compounding'] == 1 ? true : false);

        $modules = ['enrollment' => $enrollment, 'vesting' => $vesting, 'catchup' => $catchup, 'loans' => $loans, 'matchingContributions' => $matchingContributions, 'compounding' => $compounding];
        $common['SPE2Modules'] = $modules;



        $this->getSession()->set('common', $common);
    }

    public function setAutosCommonVariables() {
        $autoservice = $this->container->get("AutosService");
        $common = $this->getSession()->get('common');
        $planid = $this->getSession()->get('plan')['id'];
        $common['autos']['autoEnroll'] = false;
        $common['autos']['autoEscalate'] = false;
        $common['autos']['autoIncrease'] = false;
        $common['autos']['defaultInvestment'] = false;
        $translator = $this->container->get('translator');
        $translate = $this->container->get('translator')->getMessages()['autos'];

        $autoservice->set(["planid" => $planid]);
        $autoenroll = $autoservice->getData('AutoEnroll');
        $autoescalate = $autoservice->getData('AutoEscalate');
        $autoincrease = $autoservice->getData('AutoIncrease');
        if (!empty($autoenroll)) {
            $defaultpct = $autoenroll->deferralPercent();
            $common['autos']['defaultpct'] = $defaultpct;
        }

        $increasepct = 0;
        $defaultFundName = "";

        if ($autoenroll->enabled) {
            $common['autos']['autoEnroll'] = true;
        }

        if($autoenroll->defaultFund) {
            $common['autos']['defaultInvestment'] = true;
            $defaultFundName = $autoenroll->defaultFundName;
            $common['autos']['defaultFundName'] = $defaultFundName;
            $common['autos']['moneyHolderFundName'] = $autoenroll->moneyHolderFundName;
            $common['autos']['daysInMoneyHolder'] = $autoenroll->daysInMoneyHolder;
        }

        if ($autoescalate->enabled) {
            $common['autos']['autoEscalate'] = true;
            $increasepct = $autoescalate->percent;
            $common['autos']['autoEscalateData']['startDate'] = null;
            if (!is_null($autoescalate->startDate)) {
                $common['autos']['autoEscalateData']['startDate'] = $autoescalate->startDate->format('Y-m-d');
            }
        }

        if ($autoincrease->enabled) {
            $common['autos']['autoIncrease'] = true;
        }

        foreach (['pre', 'roth', 'post'] as $field) {
            $common['autos']['autoEnrollData'][$field] = $autoenroll->$field;
            $common['autos']['autoEscalateData'][$field] = $autoescalate->$field;
        }
        foreach (['startDate', 'frequencyEnabled', 'frequency', 'percentLimit', 'pre', 'roth', 'post', 'increaseCap'] as $field) {
            $common['autos']['autoIncreaseData'][$field] = $autoincrease->$field;
        }
        $common['autos']['autoIncreaseData']['autoIncreaseOptIn'] = false;
        if (!is_null($autoincrease->startDate)) {
            $common['autos']['autoIncreaseData']['startDate'] = $autoincrease->startDate->format('Y-m-d');
        }


        $title = "";
        $description = "";
        $yestext = "";
        $notext = "";

        if ($common['autos']['autoEscalate'] && $common['autos']['defaultInvestment']) {
            $title = 'default_investment_escalate';
            $description = 'default_investment_escalate';
            $yestext = 'default_investment_escalate';
            $notext = 'default_investment_escalate';
        } else if ($common['autos']['autoIncrease'] && $common['autos']['defaultInvestment']) {
            $title = 'default_investment_asi';
            $description = 'default_investment_asi';
            $yestext = 'default_investment_asi';
            $notext = 'default_investment_asi';
        } else if ($common['autos']['defaultInvestment']) {
            $title = 'default_investment';
            $description = 'default_investment';
            $yestext = 'default_investment';
            $notext = 'default_investment';
        } else if ($common['autos']['autoEscalate']) {
            $title = 'escalate';
            $description = 'escalate';
            $yestext = 'escalate';
            $notext = 'escalate';
        } else if ($common['autos']['autoIncrease']) {
            $title = 'asi';
            $description = 'asi';
            $yestext = 'asi';
            $notext = 'asi';
        } else if ($common['autos']['autoEnroll']) {
            $title = 'aca';
            $description = 'aca';
            $yestext = 'aca';
            $notext = 'aca';
        }

        $common['autos']['title'] = $translate["autos_title_{$title}"];
        $common['autos']['description'] = $translate["autos_description_{$description}"];
        $common['autos']['yestext'] = $translator->trans("autos_yestext_{$yestext}", array("%percent%" => $defaultpct, "%fundname%" => $defaultFundName, "%increasepct%" => $increasepct), "autos");
        $common['autos']['notext'] = $translate["autos_notext_{$notext}"];

        $autos = [
            'autoIncreaseOptIn' => false,
            'autoIncreaseStartDate' => null,
            'autoIncreasePercentLimit' => null,
            'autoIncreasePre' => null,
            'autoIncreaseRoth' => null,
            'autoIncreasePost' => null,
            'autoEscalateOptOut' => false,
            'autoEnrollOptOut' => false,
            'autoIncreaseCap' => null
        ];
        $this->getSession()->set('autos', $autos);

        $this->getSession()->set('common', $common);
    }

    // Set Retirement Needs variables
    public function setRetirementNeeds(){
        $session = $this->getSession();
        $retirement_needs = array(
            'RN_IncomePct' => isset($session->get('module')['percentageOfIncomeFromPlan'])?$session->get('module')['percentageOfIncomeFromPlan']:30,
            'RN_CurrentYearlyIncome' => null,
            'RN_EstimatedRetirementIncome' => null,
            'RN_EstimatedSocialSecurityIncome' => null,
            'RN_ReplacementIncome' => null,
            'RN_NumberOfYearsBeforeRetirement' => null,
            'RN_InflationAdjustedReplacementIncome' => null,
            'RN_OtherAssets' => 0,
            'RN_PlanAssets' => (int) isset($session->get('rkp')['plan_data']->planBalance) ? $session->get('rkp')['plan_data']->planBalance : 0,
            'RN_TotalAssets' => (int) isset($session->get('rkp')['plan_data']->planBalance) ? $session->get('rkp')['plan_data']->planBalance : 0,
            'RN_AssetTypes' => null,
            'RN_YearsYouWillLiveInRetirement' => null,
            'RN_AnnualIncomeNeededInRetirement' => null,
            'RN_EstimatedSavingsatRetirement' => null,
            'RN_RecommendedMonthlyPlanContribution' => null,
            'RN_RetirementAge' => isset($session->get('module')['retirementAge']) && $session->get('module')['retirementAge'] ? $session->get('module')['retirementAge'] : (isset($session->get('default_modules')['retirementAge'])? $session->get('default_modules')['retirementAge'] : 0),
            'RN_LifeExpectancy' => isset($session->get('module')['lifeExpectancy']) && $session->get('module')['lifeExpectancy'] ? $session->get('module')['lifeExpectancy'] : (isset($session->get('default_modules')['lifeExpectancy'])? $session->get('default_modules')['lifeExpectancy'] : 0),
            'inflation' => isset($session->get('module')['inflation']) && $session->get('module')['inflation'] ? $session->get('module')['inflation'] : (isset($session->get('default_modules')['inflation'])? $session->get('default_modules')['inflation'] : 0),
            'return' => isset($session->get('module')['return']) && $session->get('module')['return'] ? $session->get('module')['return'] : (isset($session->get('default_modules')['return'])? $session->get('default_modules')['return'] : 0),
            'retireNeedsStatus' => 0,
            'replacementIncomePercent' => isset($session->get('module')['replacementIncomePercent']) && $session->get('module')['replacementIncomePercent'] ? (double) $session->get('module')['replacementIncomePercent'] : (isset($session->get('default_modules')['replacementIncomePercent']) ? (double) $session->get('default_modules')['replacementIncomePercent'] : 0),
            'retirementNeedsFlow' => $session->get('module')['retirementNeedsFlow'],
            'pensionEstimatorURL' => $session->get('module')['pensionEstimatorURL'],
            'socialSecurityMultiplier' => $session->get('module')['socialSecurityMultiplier'],
            'RN_EstimatedPensionIncome' => 0,

            // Results Data SPE2
            'InitialAnnualIncome' => null,
            'MonthlyContribution' => null,
            'ProjectedOutsideAssets' => null,
            'RetirementAge' => null,
            'DeferralArray' => null,
            'InitialContributionDlr' => null,
            'InitialContributionPct' => null,
            'ContributionDlr' => 0,
            'ContributionPct' => null,
            'annualIncomeDisplay' => 0,
            'RN_EstimatedMonthlyIncomeAtRetirement' => 0
        );
        $this->getSession()->set('retirement_needs', $retirement_needs);
    }

    // Set Risk Profile variables
    public function setRiskProfile(){
        $session = $this->getSession();
        $risk_profile = array(
            'active' => isset($session->get('module')['riskProfile']) && $session->get('module')['riskProfile'] && isset($session->get('default_modules')['riskProfile']) && $session->get('default_modules')['riskProfile'] ? 1 : 0,
            'RP_xml' => null,
            'RP_Score' => 0,
            'RP_THScore' => 0,
            'RP_Points' => null,
            'RP_InvestorType' => -1,
            'RP_label' => null,
            'RP_desc' => null,
            'recommendedPortfolioName' => null,
            'riskProfileStatus' => 0,
            'RP_Questions' => null,
            'stadionManagedAccountOn' => ($session->get('module')['stadionManagedAccountOn'] ? 1 : 0),
            'stadionManagedAccountId' => (isset($this->getSession()->get('rkp')['plan_data']->rkpExtras->stadionManagedAccountId) ? $this->getSession()->get('rkp')['plan_data']->rkpExtras->stadionManagedAccountId : null)
        );
        $this->getSession()->set('risk_profile', $risk_profile);
    }

    // Set Investment variables
    public function setInvestments(){
        $investments = array(
            'I_SelectedInvestmentOption' => null,
            'I_SelectedInvestmentDetails' => null,
            'I_Realign' => null,
            'investmentsStatus' => 0
        );
        $this->getSession()->set('investments', $investments);
    }


    // Set Contributions variables
    public function setContributions(){
        $contribution = array(
            'contribution_mode' => null,
            'catchup_mode' => null,
            'C_PreTaxContributionPct' => null,
            'C_PreTaxContributionValue' => null,
            'C_RothContributionPct' => null,
            'C_RothContributionValue' => null,
            'C_PostTaxContributionPct' => null,
            'C_PostTaxContributionValue' => null,
            'C_GetPaid' => null,
            'C_ACAoptOut' => 0,
            'contributionsStatus' => 0
        );
        $this->getSession()->set('contributions', $contribution);
    }

    // Set My Profile variables
    public function setMyProfiles(){
        $session = $this->getSession();
        $myProfiles = array(
            'beneficiary' => 0,
            'updateProfile' => 0,
            'updateProfileEmail' => 0,
            'updateProfileProceed' => 0,
            'endVideoStatus' => 0,
            'P_ShowCurrentProfile' => 0,
            'rolloverCompleted' => 0,
            'rolloverWorkflow' => ($session->get('module')['rolloverWorkflow'] && $session->get('account')['rolloverWorkflowEnabled'] ? 1 : 0),
            'trustedContactOn' => ($session->get('module')['trustedContactOn'] && $session->get('account')['trustedContactEnabled'] ? 1 : 0)
        );
        $this->getSession()->set('my_profiles', $myProfiles);
    }

    public function setRiskProfileInvestors(){
        $translate = $this->container->get('translator')->getMessages();
        $invesmentsTranslated = $translate['investments'];
        $RP_InvestorVideo = array($translate['video']['investor_0514'], $translate['video']['investor_0515'], $translate['video']['investor_0513'], $translate['video']['investor_0512'], $translate['video']['investor_0511']);
        $RP_InvestorVideoAXA = array($translate['video']['investor_0515_AXA'], $translate['video']['investor_0514_AXA'], $translate['video']['investor_0513_AXA'], $translate['video']['investor_0512_AXA'], $translate['video']['investor_0512_AXA']);
        $RP_InvestorLabels = array($invesmentsTranslated['conservative'], $invesmentsTranslated['moderately_conservative'], $invesmentsTranslated['moderate'], $invesmentsTranslated['moderately_aggressive'], $invesmentsTranslated['aggressive']);
        $RP_InvestorLabelsAXA = array($invesmentsTranslated['conservative'], $invesmentsTranslated['conservative_plus'], $invesmentsTranslated['moderate'], $invesmentsTranslated['moderate_plus'], $invesmentsTranslated['aggressive']);
        $RP_InvestorDescriptions = array($invesmentsTranslated['investor_description_1'], $invesmentsTranslated['investor_description_2'], $invesmentsTranslated['investor_description_3'], $invesmentsTranslated['investor_description_4'], $invesmentsTranslated['investor_description_5']);
        $RP_InvestorDescriptionsAXA = array($invesmentsTranslated['investor_description_axa_1'],$invesmentsTranslated['investor_description_axa_2'],$invesmentsTranslated['investor_description_axa_3'],$invesmentsTranslated['investor_description_axa_4'],$invesmentsTranslated['investor_description_axa_5']);
        $RP_InvestorDescriptionsAXA2 = array($invesmentsTranslated['investor_description_axa2_1'],$invesmentsTranslated['investor_description_axa2_2'],$invesmentsTranslated['investor_description_axa2_3'],$invesmentsTranslated['investor_description_axa2_4'],$invesmentsTranslated['investor_description_axa2_5']);

        $risk_profile = $this->getSession()->get('risk_profile');
        $client_calling = $this->getSession()->get('client_calling');
        $xml = $risk_profile['RP_xml'];
        $type = $risk_profile['RP_InvestorType'];
        if($client_calling == 'AXA' && $xml <> 'riskProfileAXA2013'){
            $risk_profile['RP_video'] = $RP_InvestorVideoAXA[$type];
            $risk_profile['RP_label'] = $RP_InvestorLabelsAXA[$type];
            $risk_profile['RP_desc'] = $xml == 'riskProfileAXA2014_Ibbotson' ? $RP_InvestorDescriptionsAXA2[$type] : $RP_InvestorDescriptionsAXA[$type];
        }else{
            $risk_profile['RP_video'] = $RP_InvestorVideo[$type];
            $risk_profile['RP_label'] = $RP_InvestorLabels[$type];
            $risk_profile['RP_desc'] = $RP_InvestorDescriptions[$type];
        }
        $risk_profile['riskProfileStatus'] = 2;
        $risk_profile['stadionLabels'] =
        [
            1 => $invesmentsTranslated['conservative'],
            2 => $invesmentsTranslated['conservative'],
            3 => $invesmentsTranslated['moderate'],
            4 => $invesmentsTranslated['moderate'],
            5 => $invesmentsTranslated['aggressive'],
            6 => $invesmentsTranslated['aggressive']            
        ];
        $this->getSession()->set('risk_profile', $risk_profile);
        return $risk_profile;
    }
    
    public function setStadionManagedAccountData($retireExpress = null) {
        $translate = $this->container->get('translator')->getMessages();
        $invesmentsTranslated = $translate['investments'];
        $risk_profile = $this->getSession()->get('risk_profile');
        unset($risk_profile['retireExpress']);
        $risk_profile['stadionVideos'] = 
        [
            1 => $translate['video']['0531_SDN'],
            2 => $translate['video']['0531_SDN'],
            3 => $translate['video']['0533_SDN'],
            4 => $translate['video']['0533_SDN'],
            5 => $translate['video']['0535_SDN'],
            6 => $translate['video']['0535_SDN'],
        ];
        $risk_profile['stadionLabels'] =
        [
            1 => $invesmentsTranslated['aggressive'],
            2 => $invesmentsTranslated['aggressive'],
            3 => $invesmentsTranslated['moderate'],
            4 => $invesmentsTranslated['moderate'],
            5 => $invesmentsTranslated['conservative'],
            6 => $invesmentsTranslated['conservative'],
        ];
        $risk_profile['stadionDescriptions'] =
        [
            1 => $invesmentsTranslated['investor_description_5'],
            2 => $invesmentsTranslated['investor_description_5'],
            3 => $invesmentsTranslated['investor_description_3'],
            4 => $invesmentsTranslated['investor_description_3'],
            5 => $invesmentsTranslated['investor_description_1'],
            6 => $invesmentsTranslated['investor_description_1'],
        ];
        $pathId = $this->getSession()->get("stadionQuestionnaireResponse")['data']['pathId'];
        if (in_array($pathId, [1,2])) {
            $risk_profile['RP_InvestorType'] = 4;
        }
        if (in_array($pathId, [3,4])) {
            $risk_profile['RP_InvestorType'] = 2;
        }
        if (in_array($pathId, [5,6])) {
            $risk_profile['RP_InvestorType'] = 0;
        }
        if ($this->getSession()->get('rkp')['plan_data']->rkpExtras->stadionManagedAccountId == 1)
        {
            $risk_profile['stadionVideos'] = 
            [
                1 => $translate['video']['0531'],
                2 => $translate['video']['0531'],
                3 => $translate['video']['0533'],
                4 => $translate['video']['0533'],
                5 => $translate['video']['0533'],
                6 => $translate['video']['0535'],
                7 => $translate['video']['0535'],
            ];       
            $risk_profile['stadionLabels'] =
            [
                1 => $invesmentsTranslated['aggressive'],
                2 => $invesmentsTranslated['aggressive'],
                3 => $invesmentsTranslated['moderate'],
                4 => $invesmentsTranslated['moderate'],
                5 => $invesmentsTranslated['moderate'],
                6 => $invesmentsTranslated['conservative'],
                7 => $invesmentsTranslated['conservative'],
            ];
            $risk_profile['stadionDescriptions'] =
            [
                1 => $invesmentsTranslated['investor_description_5'],
                2 => $invesmentsTranslated['investor_description_5'],
                3 => $invesmentsTranslated['investor_description_3'],
                4 => $invesmentsTranslated['investor_description_3'],
                5 => $invesmentsTranslated['investor_description_3'],
                6 => $invesmentsTranslated['investor_description_1'],
                7 => $invesmentsTranslated['investor_description_1'],
            ];
            if (in_array($pathId, [1,2])) {
                $risk_profile['RP_InvestorType'] = 4;
            }
            if (in_array($pathId, [3,4,5])) {
                $risk_profile['RP_InvestorType'] = 2;
            }
            if (in_array($pathId, [6,7])) {
                $risk_profile['RP_InvestorType'] = 0;
            }
        }
        $risk_profile['RP_video'] = $risk_profile['stadionVideos'][$pathId];
        $risk_profile['RP_label'] = $risk_profile['stadionLabels'][$pathId];
        $risk_profile['RP_desc'] = $risk_profile['stadionDescriptions'][$pathId];
        $risk_profile['riskProfileStatus'] = 2;
        $risk_profile['stadionPathId'] = $pathId;
        if ($retireExpress) {
            $risk_profile['retireExpress'] = true;
        }
        
        $this->getSession()->set('risk_profile', $risk_profile);
        return $risk_profile;
    }

    public function speEncrypt($data){
        $connection = $this->container->get('doctrine.dbal.default_connection');
        $sql = "SELECT AES_ENCRYPT(:data, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."')) as encryptedString";  
        $stmt = $connection->prepare($sql);
        $stmt->bindValue("data", $data); 
        $stmt->execute();
        return $stmt->fetch()['encryptedString'];
    }

    public function speDecrypt($data){
        if (trim($data) == "")
        return $data;    
        
        $connection = $this->container->get('doctrine.dbal.default_connection');
        $sql = "SELECT AES_DECRYPT(:data, UNHEX('".$this->container->getParameter('HEX_AES_KEY')."')) as decryptedString";  
        $stmt = $connection->prepare($sql);
        $stmt->bindValue("data", $data); 
        $stmt->execute();
        $mysqlResult = $stmt->fetch()['decryptedString'];
        if ($mysqlResult == null)
        {
            $key = $this->container->getParameter('HEX_AES_KEY');
            $data = is_resource($data) ? stream_get_contents($data) : $data;
            $returnData = $this->decrypt($data, $key, MCRYPT_RIJNDAEL_128);
            $returnData = trim(rtrim($returnData,"\x00..\x1F"));
            if (ctype_print($returnData))
            return $returnData;
        }

        return $mysqlResult;        
    }

    function decrypt(string $data, string $key, string $method): string
    {
        $data = base64_decode($data);
        $ivSize = openssl_cipher_iv_length($method);
        $iv = substr($data, 0, $ivSize);
        $data = openssl_decrypt(substr($data, $ivSize), $method, $key, OPENSSL_RAW_DATA, $iv);

        return $data;
    }

    public function addUniqueId($data){
        if(!empty($data)){
            foreach($data as $k => $d){
                $data[$k]['uniqid'] = uniqid();
            }
        }
        return $data;
    }

	public function pad_key($key){
		if(strlen($key) > 32) return false;
		$sizes = array(16,24,32);

		foreach($sizes as $s){
		while(strlen($key) < $s) $key = $key."\0";
			if(strlen($key) == $s) break;
		}
		return $key;
	}
    public function setStadionQuestionnaireSession()
    {
       $session = $this->getSession();
       $answers = explode("|",$session->get("risk_profile")['RP_Points']); 
       $start = 1;
       $answersModified = [];
       foreach ($answers as $answer)
       {
           $answersModified["q".$start++] = $answer;
       }
       $response = $this->container->get('spe.app.rkp')->postStadionQuestionnaire($answersModified);  
       $session->set("stadionQuestionnaireResponse",$response);
       $session->set("stadionQuestionnaireAnswers",$answersModified);
    }
}
