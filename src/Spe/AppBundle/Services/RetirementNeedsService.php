<?php
namespace Spe\AppBundle\Services;
use classes\classBundle\Services\BaseService;
class RetirementNeedsService extends BaseService
{

    public function saveData($data)
    {
        $recalculateSS = false;
        $ssEnabled = false;
        $modules = $this->session->get("module");
        if ($modules['retirementNeedsFlow'] != 3) { // 3 = pension only
            $ssEnabled = true;
        }

        foreach($data as $key => $value) {
            if($key == 'page')
                continue;
            $name = explode('_', $key);
            if ($name[0] == "common") {
                $common = $this->session->get("common");
                $common[$name[1]] = str_replace(',', '', $value);
                $this->session->set('common', $common);
            } else {
                $retirement_needs = $this->session->get("retirement_needs");
                $retirement_needs[$key] = str_replace(',', '', $value);
                $this->session->set('retirement_needs', $retirement_needs);
                if ($key == 'RN_CurrentYearlyIncome' && $ssEnabled) {
                    $recalculateSS = true;
                }
            }
        }
        $this->setPreviousYearlyIncome();

        $recalculateEstimatedRetirementIncome = isset($data['RN_CurrentYearlyIncome']);
        $recalculateReplacementIncome = $recalculateEstimatedRetirementIncome
            || isset($data['RN_EstimatedRetirementIncome'])
            || isset($data['RN_EstimatedSocialSecurityIncome'])
            || isset($data['RN_EstimatedPensionIncome']);

        $this->calculateFromIncome($recalculateSS, $recalculateEstimatedRetirementIncome, $recalculateReplacementIncome);
    }

    public function getData() {
        $retArray = array();
        $retArray['data'] = $this->session->get("retirement_needs");
        $retArray['common'] = $this->session->get("common");
        $retArray['module'] = $this->session->get("module");
        $retArray['serverConfig'] = $this->session->get("serverConfig");
        return $retArray;
    }

    private function setPreviousYearlyIncome() {
        $retirement_needs = $this->session->get("retirement_needs");
        $retirement_needs['RN_PreviousYearlyIncome'] = $retirement_needs['RN_CurrentYearlyIncome'];
        $this->session->set('retirement_needs',$retirement_needs);
    }

    private function setReplacementIncome() {
        $retirement_needs = $this->session->get("retirement_needs");
        $retirement_needs['RN_ReplacementIncome'] = round($retirement_needs['RN_EstimatedRetirementIncome'] -
            $retirement_needs['RN_EstimatedSocialSecurityIncome'] - $retirement_needs['RN_EstimatedPensionIncome']);
        $this->session->set('retirement_needs',$retirement_needs);
    }

    private function setEstimatedSocialSecurityIncome() {
        $retirement_needs = $this->session->get("retirement_needs");
        $serverConfig = $this->session->get('serverConfig');
        $retirement_needs['RN_EstimatedSocialSecurityIncome'] =
            ($retirement_needs['RN_CurrentYearlyIncome'] * $retirement_needs['socialSecurityMultiplier']) > $serverConfig['estimatedSocialSecurityIncomeMax'] ?
                $serverConfig['estimatedSocialSecurityIncomeMax'] : round($retirement_needs['RN_CurrentYearlyIncome'] * $retirement_needs['socialSecurityMultiplier']);
        $this->session->set('retirement_needs',$retirement_needs);
    }

    private function setEstimatedRetirementIncome() {
        $retirement_needs = $this->session->get("retirement_needs");
        $retirement_needs['RN_EstimatedRetirementIncome'] = round($retirement_needs['RN_CurrentYearlyIncome'] * ($retirement_needs['replacementIncomePercent'] / 100),2);
        $this->session->set('retirement_needs',$retirement_needs);
    }

    public function calculateFromIncome($recalculateSS, $recalculateEstimatedRetirementIncome, $recalculateReplacementIncome) {
        if ($recalculateEstimatedRetirementIncome) {
            $this->setEstimatedRetirementIncome();
        }
        if ($recalculateSS) {
            $this->setEstimatedSocialSecurityIncome();
        }
        if ($recalculateReplacementIncome) {
            $this->setReplacementIncome();
        }
    }

    public function finishRN() {
        $retirement_needs = $this->session->get("retirement_needs");
        $retirement_needs['retireNeedsStatus'] = 2;
        $this->session->set('retirement_needs', $retirement_needs);
        $this->session->set('currentYearlyIncome', $retirement_needs['RN_CurrentYearlyIncome']);
    }

}
