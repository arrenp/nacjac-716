<?php
namespace Spe\AppBundle\Services;
use classes\classBundle\Services\BaseService;
use classes\classBundle\Entity\profilesContributions;
class ContributionsService extends BaseService
{
    public function payFrequency()
    {       
        return $this->payFrequencyCalculation(["selectedValue" => $this->session->get("contributions")['C_GetPaid']]);
    }    
    public function payFrequencyCalculation($params)
    {
        $payFrequencyArray = ["weekly","bi_weekly","semi_monthly","monthly","quarterly","annually"];
        $payFrequency = [];
        foreach ($payFrequencyArray  as $frequency)
        {
            $value = ucfirst(str_replace("_","-",$frequency));
            $newFrequency = 
            [
                "value" => $value,
                "key" => $frequency,
                "selected" => false
            ];
            if ($value == $params['selectedValue'])
            {
                $newFrequency["selected"]=true;
            }
            $payFrequency[$value] = $newFrequency;
        }        
        $payFrequency['Quarterly']['key'] = "quartely";
        return $payFrequency;        
    }
    public function calculate($params = [])
    {
        $contributions = $this->session->get("contributions");
        $rkp = $this->session->get("rkp");
        $translate = $this->container->get('translator')->getMessages();
        $params['mode'] = "percent";
        $params['payFrequencyValue'] = $contributions['C_GetPaid'];
        $common = $this->session->get("common");
        if ($contributions['contribution_mode'] =="DEFERDLR" || $common['mode'] == "dlr" )
        {
            $params["mode"] = "dollar";
        }        
        if (!isset($params['blockPercents']))
        {
            $params['blockPercents'] = $params["mode"]  == "percent";
        }         
        $params['contributions'] = $contributions;
        $common['showMatchDescriptions'] = $common['showMatchDescriptions']  == "true";
        if ($contributions['C_ACAoptOut'] == 1)
        {
            $common["ACAon"] = 0;
        }
        if (empty($common['planCatchupSetting']))
        {
            $common['planCatchupSetting'] = 0;
        }
        $params["common"] = $common;
        $params['targetSavings'] = $this->targetSavings();
        $params['targetMonthlyContribution'] = $this->targetMonthlyContribution();
        if (!isset($params['currentYearlyIncome']))
        {
            $params['currentYearlyIncome'] = $this->currentYearlyIncome();
        }
        $deferrals = $this->deferrals(["currentYearlyIncome" => $params['currentYearlyIncome'],"mode" => $params['mode'],"payFrequencyValue" => $params['payFrequencyValue'],"blockPercents" => $params['blockPercents'] ]);
        $params["deferrals"] = $deferrals;        
        if (!isset($params['totalContributionPercent']))
        {
            $params['totalContributionPercent'] = 0;
            foreach ($deferrals as $deferral)
            {
                $params['totalContributionPercent'] += $deferral['currentPercent']; 
            }
        }
        $params['totalAssets'] = $this->totalAssets();
        $params['yearsBeforeRetirement'] = $this->numberOfYearsBeforeRetirement();
        $params['estimatedMonthlyContribution'] = $this->estimatedMonthlyContribution($params);  
        $params['projectedSavings'] = $this->projectedSavings($params);  
        $matchingSections = ['matchPercent','matchCap','secondaryMatchPercent','secondaryMatchCap'];
        foreach ($matchingSections as $matchingSection)
        {
            $params[$matchingSection] = $common[$matchingSection];
        }
        $params['matchingAmount'] = $common['showMatchDescriptions']  ? $this->matchingAmount($params) : 0; 
        $params['matchingAmountMonthly'] = $this->matchingAmountMonthly($params);
        $params['totalSavings'] = $params['projectedSavings'] + $params['matchingAmount'];
        $params['graphHeight'] = $params['graphHeight2'] = 160;
        if ($params['totalSavings'] <  $params['targetSavings'])
        {
            $params['graphHeight'] = ($params['totalSavings']/$params['targetSavings']) * 160;
        }
        else if ($params['totalSavings'] >  $params['targetSavings'])
        {
            $params['graphHeight2'] = ($params['targetSavings']/$params['totalSavings']) * 160;
        }
        $params['graphHeightYou'] = intval(($params['projectedSavings']/$params['totalSavings']) * $params['graphHeight']);
        $params['graphHeightEmployer'] = intval(($params['matchingAmount']/$params['totalSavings']) * $params['graphHeight']);
        $mingraphHeightSection = 18;
        if ($params['graphHeightYou'] < $mingraphHeightSection)
        {
            $params['graphHeightYou'] = $mingraphHeightSection;
        }
        if ($params['graphHeightEmployer'] < $mingraphHeightSection)
        {
            $params['graphHeightEmployer']= $mingraphHeightSection;
        }  
        if ($common['showMatchDescriptions'])
        {
            $params['graphHeight'] = $params['graphHeightYou']  + $params['graphHeightEmployer'];
        }
        $params['maxDeferral'] =   $params['maxDeferralDollar'] = $params['minDeferralDollar'] = 0;
        $params['hasDefaults'] = false;
        foreach ($deferrals as $deferral)
        {
            if (!isset($params['minDeferral']) || $params['minDeferral'] > $deferral['minPercent'])
            {
                $params['minDeferral'] = $deferral['minPercent'];
            }
            if ($deferral['maxPercent'] > $params['maxDeferral'])
            {
                $params['maxDeferral'] = $deferral['maxPercent'];
            }
            if (!empty($deferral['currentPercentRaw']))
            {
                $params['hasDefaults'] = true;
            }
        }
        if (!empty($this->session->get("module")["combinedContributionAmount"]))
        {
            $params['maxDeferral'] = $this->session->get("module")["combinedContributionAmount"];
        }
        if ($common["deferralMaxRateAgg"])
        {
            $params['maxDeferral'] = $common["deferralMaxRateAgg"];
        }
        if ($common["deferralMinRateAgg"])
        {
            $params['minDeferral'] = $common["deferralMinRateAgg"];
        }
        $calculation = $this->contributionsCalculate($params);
        $calculation->mode = "PCT";
        $calculation->pre = $params['minDeferral'];
        $calculation->roth = $params['maxDeferral'];
        $this->container->get("spe.app.profile")->calculateContributions($calculation);
        $params['minDeferralDollar'] = $calculation->preDollarPayFrequency;
        $params['maxDeferralDollar'] = $calculation->rothDollarPayFrequency;
        $params['maxMatch'] = $this->maxMatch($params);
        $paramsIRS['totalContributionPercent'] = $deferrals['pre']['currentPercent'] + $deferrals['roth']['currentPercent'] ;
        $paramsIRS['currentYearlyIncome'] = $params['currentYearlyIncome'];
        $paramsIRS['round'] = true;
        $params['limit402'] = $common['limit_402'];
        if ($common['showCatchup'] && empty($rkp['plan_data']->catchupChange))
        {
            $params['limit402'] = $common['limit_402_50'];
        }
        $params['catchupChange'] = $common['catchupChange'];
        $params['limit402Monthly']  = $params['limit402']/12;
        $params['IRSLimitExceeded'] = $this->estimatedMonthlyContribution($paramsIRS) > $params['limit402Monthly'];
        $params["audioFile"] = $translate['audio']['contributions_0702'];
        if (!$common['planAllowsDeferrals'])
        {
            $params["audioFile"] = $translate['audio']['contributions_0702_DO'];
        }
        if ($common['ACAon'])
        {
            $params["audioFile"] = $translate['audio']['contributions_0701x_SPe_090512'];            
        }
        //number format keep old values for calculations
        $params['maxDeferralDollarDisplay'] = number_format($params['maxDeferralDollar'],2);
        $params['minDeferralDollarDisplay'] = number_format($params['minDeferralDollar'],2);        
        $params['totalSavingsDisplay'] = number_format($params['totalSavings']);
        $params['matchingAmountDisplay'] = number_format($params['matchingAmount']);
        $params['matchingAmountMonthlyDisplay'] = number_format($params['matchingAmountMonthly']);
        $params['projectedSavingsDisplay']  = number_format($params['projectedSavings']);
        $params['estimatedMonthlyContributionDisplay'] = number_format($params['estimatedMonthlyContribution']);
        return $params;        
    }
    public function targetSavings()
    {
        return (int)$this->session->get("retirement_needs")['RN_EstimatedSavingsatRetirement'];
    }
    public function targetMonthlyContribution()
    {
        return (int)$this->session->get("retirement_needs")['RN_RecommendedMonthlyPlanContribution'];
    }
    public function totalAssets()
    {
        return (float)$this->session->get("retirement_needs")['RN_TotalAssets'];
    }
    public function numberOfYearsBeforeRetirement()
    {
        return (int) $this->session->get("retirement_needs")['RN_NumberOfYearsBeforeRetirement'];
    }
    public function currentYearlyIncome()
    {
        if (!empty($this->session->get("currentYearlyIncome")))
        {
            return (float)$this->session->get("currentYearlyIncome");
        }
        return (float)$this->session->get("retirement_needs")['RN_CurrentYearlyIncome'];
    }
    public function contributionsCalculate($params)
    {
        $contributionsCalculate = new profilesContributions();
        $contributionsCalculate->paidFrequency = $params["payFrequencyValue"];
        $contributionsCalculate->currentYearlyIncome = $params["currentYearlyIncome"];        
        return $contributionsCalculate;
    }
    public function deferrals($params = [])
    {
        $common = $this->session->get("common");
        $contributions = $this->session->get("contributions");
        $sections = array("pre","roth","post");
        $messages = $this->container->get('translator')->getMessages();
        $deferrals =
        [
            "pre" => 
            [
                "description" => $messages['contributions']['pre_tax_per_paycheck'],
                "percentField" => "C_PreTaxContributionPct",
                "dollarField" => "C_PreTaxContributionValue",
                "field" => "pre",
                "gaslider" => "Pre-tax per paycheck",
                "shortdescription" => "Pre-tax"
            ],
            "roth" =>
            [
                "description" => $messages['contributions']['after_tax_roth_per_paycheck'],
                "percentField" => "C_RothContributionPct",
                "dollarField" => "C_RothContributionValue",
                "field" => "roth",
                "gaslider" => "After-tax (Roth) per paycheck",
                "shortdescription" => "Roth"
            ],
            "post" =>
            [
                "description" => $messages['contributions']['post_tax_per_paycheck'],
                "percentField" => "C_PostTaxContributionPct",
                "dollarField" => "C_PostTaxContributionValue",
                "field" => "post",
                "gaslider" => "Post-tax per paycheck",
                "shortdescription" => "Post-tax"
            ]
        ];
        foreach ($deferrals as &$deferral)
        {
            $deferral['currentPercent'] = $deferral['currentPercentRaw'] = (float)$common[$deferral['field']."Current"];
            if ($contributions[$deferral['percentField']] !== null)
            {
                $deferral['currentPercent'] = (float)$contributions[$deferral['percentField']];
            }
            $deferral['minPercent'] =  (float)$common[$deferral['field']."Min"];
            $deferral['maxPercent'] =  (float)$common[$deferral['field']."Max"];
            $deferral['currentDollar'] = "";
            if ($contributions[$deferral['dollarField']] !== null)
            {
                $deferral['currentDollar'] = (float)$contributions[$deferral['dollarField']];
            }
            $deferral['percentStep'] = $common[$deferral['field']."PctPre"];
            $deferral['dollarStep'] = $common[$deferral['field']."DlrPre"];
        }
        foreach ($sections as  $section)
        {           
            if (empty($common[$section]))
            {
                unset($deferrals[$section]);
            }
        }
        $contributionsCalculate= $this->contributionsCalculate($params);
        if ($params['mode'] == "dollar")//calculate percents
        {
            $contributionsCalculate->mode = "DLR";
            foreach ($deferrals as &$deferral)
            {
                $field = $deferral["field"];
                $contributionsCalculate->$field = (float)$deferral['currentDollar'];
            }
            $this->container->get("spe.app.profile")->calculateContributions($contributionsCalculate);
            foreach ($deferrals as &$deferral)
            {
                $field = $deferral["field"]."Percent";
                $deferral['currentPercent'] = (float)$contributionsCalculate->$field;
            }
        }
        else
        {
            $contributionsCalculate->mode = "PCT";
            foreach ($deferrals as &$deferral)
            {
                $field = $deferral["field"];
                $contributionsCalculate->$field = (float)$deferral['currentPercent'];
            }
            $this->container->get("spe.app.profile")->calculateContributions($contributionsCalculate);
            foreach ($deferrals as &$deferral)
            {
                $field = $deferral["field"]."DollarPayFrequency";
                $deferral['currentDollar'] = (float)$contributionsCalculate->$field;
            }            
        }
        if (!empty($params['blockPercents']))
        {
            foreach ($deferrals as &$deferral)
            {
                if ($deferral['minPercent'] > $deferral['currentPercent'])
                {
                     $deferral['currentPercent'] = $deferral['minPercent'];
                }
                if ($deferral['maxPercent'] <  $deferral['currentPercent'])
                {
                    $deferral['currentPercent'] = $deferral['maxPercent'];
                }
            }
        }
        $contributionsCalculate= $this->contributionsCalculate($params);
        foreach ($deferrals as &$deferral)
        {
            $field = $deferral['field'];
            $fieldPayFrequency = $deferral["field"]."DollarPayFrequency";
            $contributionsCalculate->mode = "PCT";
            $contributionsCalculate->$field = $deferral['minPercent'];
            $this->container->get("spe.app.profile")->calculateContributions($contributionsCalculate);
            $deferral['minDollar'] = $contributionsCalculate->$fieldPayFrequency;
            $contributionsCalculate->$field = $deferral['maxPercent'];
            $this->container->get("spe.app.profile")->calculateContributions($contributionsCalculate);
            $deferral['maxDollar'] = $contributionsCalculate->$fieldPayFrequency;
        }
        return $deferrals;        
    }
    public function estimatedMonthlyContribution($params)
    {
        if (!isset($params['round']))
        {
            $params['round'] = true;
        }
        $estimatedMonthlyContribution =  (($params['currentYearlyIncome'] * ($params['totalContributionPercent']/100))/12);
        if ($params['round'])
        {
            $estimatedMonthlyContribution = round($estimatedMonthlyContribution);
        }
        return $estimatedMonthlyContribution;
    }
    public function projectedSavings($params)
    {
        $orgRound = true;
        if (isset($params['round']))
        {
            $orgRound = $params['round'];
        }
        $apr = .06;
        $aprPlusOne = $apr + 1;
        $params['round'] = false;
        $params['estimatedMonthlyContribution'] = $this->estimatedMonthlyContribution($params); 
        $params['projectedSavings'] = 0;
        if (empty($params['ignoreAssets']))
        {
            $params['projectedSavings'] = ($params['totalAssets'] * pow($aprPlusOne,$params['yearsBeforeRetirement']));
        }
        $params['projectedSavings'] = $params['projectedSavings'] + ($params['estimatedMonthlyContribution'] * 12 * (pow($aprPlusOne,$params['yearsBeforeRetirement']+1)-($aprPlusOne)))/$apr;
        if ($orgRound)
        {
            $params['projectedSavings'] = round($params['projectedSavings']);
        }
        return $params['projectedSavings'] ;
    }
    public function matchingAmount($params)
    {
        $params['round'] = false;
        $params['ignoreAssets'] = true;
        $deferralPercent = $params['totalContributionPercent'];
        $totalContributionPercent = $params['totalContributionPercent'];
        if ($params['totalContributionPercent'] > $params['matchCap'])
        {
            $params['totalContributionPercent'] = $params['matchCap'];
        }     
        $matchingAmount= $this->projectedSavings($params);
        $matchingAmount = $matchingAmount * ($params['matchPercent']/100);
        
        if ($totalContributionPercent > $params['matchCap'] && $params['secondaryMatchCap'] > $params['matchCap'] )
        {
            if ($totalContributionPercent > $params['secondaryMatchCap'])
            {
                $totalContributionPercent = $params['secondaryMatchCap'];
            }
            $params['totalContributionPercent'] = $totalContributionPercent-$params['matchCap'];
            $secondMatchAmount = $this->projectedSavings($params);
            $secondMatchAmount = $secondMatchAmount * ($params['secondaryMatchPercent']/100);
            $matchingAmount = $matchingAmount + $secondMatchAmount;
        }
        return round($matchingAmount);
    }
    public function matchingAmountMonthly($params)
    {
        $totalContributionPercent = $params['totalContributionPercent'];
        $estimatedMonthlyContribution1  = $estimatedMonthlyContribution2 = 0;
        if ((float)$params['totalContributionPercent'] > (float)$params['matchCap'])
        {
            $params['totalContributionPercent'] = $params['matchCap'];
        }
        $estimatedMonthlyContribution1 =  (($params['currentYearlyIncome'] * ($params['totalContributionPercent']/100))/12);
        $estimatedMonthlyContribution1 = $estimatedMonthlyContribution1 * ($params['matchPercent']/100);
        if ((float)$totalContributionPercent > (float)$params['matchCap'] && (float)$params['secondaryMatchCap'] > (float)$params['matchCap'] )
        {
            if ((float)$totalContributionPercent > (float)$params['secondaryMatchCap'])
            {
                $totalContributionPercent = $params['secondaryMatchCap'];
            }
            $totalContributionPercent  = $totalContributionPercent - $params['matchCap'];
            $estimatedMonthlyContribution2 =  (($params['currentYearlyIncome'] * ($totalContributionPercent/100))/12);
            $estimatedMonthlyContribution2 = $estimatedMonthlyContribution2 * ($params['secondaryMatchPercent']/100);
        }
        $estimatedMonthlyContribution = $estimatedMonthlyContribution1 + $estimatedMonthlyContribution2;
        return $estimatedMonthlyContribution;
    }
    public function maxMatch($params)
    {
        $params['totalContributionPercent'] = $params['matchCap'];
        if ((float)$params['secondaryMatchCap'] > (float)$params['totalMonthlyContribution'])
        {
            $params['totalContributionPercent'] = $params['secondaryMatchCap'];
        }
        $params['matchingAmount'] = $this->matchingAmount($params);
        $return = ["percent" => $params['totalContributionPercent'], "amount" => $params['matchingAmount'] ,"monthly" => $this->matchingAmountMonthly($params)];
        $return["monthlyDisplay"] = number_format($return["monthly"]);
        return $return;
    }
    public function stringToDouble($variable)
    {
        return (double)filter_var($variable, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    }
    public function saveAutoIncrease($data)
    {
        $fields = ['autoIncreaseFrequency', 'autoIncreaseCap', 'autoIncreaseRoth',
            'autoIncreasePre', 'autoIncreasePost', 'autoIncreaseStartDate', 'autoIncreaseOptIn'
        ];
        $autos = $this->session->get("autos");

        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $autos[$field] = $data[$field];
            }
        }

        $this->session->set("autos", $autos);
    }
    public function saveAutosOptOuts($isOptOut) {
        $autos = $this->session->get("autos");
        $common = $this->session->get("common");

        foreach (['autoEnroll', 'autoEscalate'] as $autoType) {
            if ($common['autos'][$autoType]) {
                $autos[$autoType . 'OptOut'] = $isOptOut;
            }
        }

        $this->session->set('autos', $autos);

    }
}
