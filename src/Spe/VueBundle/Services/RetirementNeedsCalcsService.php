<?php

namespace Spe\VueBundle\Services;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WidgetsBundle\Services\FormulaService;

class RetirementNeedsCalcsService extends FormulaService
{
    private $data = null;
    var $retirementNeeds;

    public function __construct(containerInterface $container, $retirementNeeds)
    {
        parent::__construct($container);
        $this->retirementNeeds = $retirementNeeds;
    }

    public function getData() {
        if ($this->data === null) {
            $this->data = parent::getData();
        }
        return $this->data;
    }

    public function getAge() { // might not need this function
        $this->getData();
        $date = new \DateTime($this->data['dateOfBirth']);
        $now = new \DateTime();
        $interval = $now->diff($date);
        return $interval->y;
    }

    public function calculateResults() {
        $data = $this->retirementNeeds->getData();
        $ytr = $data['data']['RN_NumberOfYearsBeforeRetirement'];
        $apr = $this->getApr($ytr);
        $results = array();

        $results['results']['InitialAnnualIncome'] = $this->calculateInitialAnnualIncome($data['data']['RN_InflationAdjustedReplacementIncome'], 0.3);
        $results['results']['InitialContributionDlr'] = $this->calculateInitialContributionDlr($results['results']['InitialAnnualIncome'], $apr, $data['data']['RN_NumberOfYearsBeforeRetirement']);
        $results['results']['InitialContributionPct'] = $this->calculateInitialContributionPct($results['results']['InitialContributionDlr'], ceil($data['data']['RN_CurrentYearlyIncome']/12));
        $results['results']['InitialContributionLabel'] = "$".$results['results']['InitialContributionDlr']."/mo";
        $results['results']['RetirementAge'] = $this->getAge() + $data['data']['RN_NumberOfYearsBeforeRetirement'];
        $results['results']['DeferralArray'] = $this->createDeferralArray($data, $results);

        $FullContributionDlr = $this->calculateInitialContributionDlr($data['data']['RN_InflationAdjustedReplacementIncome'], $apr, $data['data']['RN_NumberOfYearsBeforeRetirement']);
        $results['results']['FullContributionCalc'] = $this->createDeferralArraySingle($FullContributionDlr);

        $results['results']['ProjectedOutsideAssets'] = $this->calculateProjectedOutsideAssetVal($data['data']['RN_OtherAssets'], $data['data']['RN_NumberOfYearsBeforeRetirement']);

        return $results;
    }

    public function calculateInitialContributionPct($initialContribution, $incomePerMonth) {
        $pct = round($initialContribution / $incomePerMonth * 100, 1);
        return $pct;
        //$initialContribution / $incomePerMonth * 100, 1) . "%
    }

    public function calculateInitialContributionDlr($income, $apr, $ytr) {
//
//        $a = $income/12;
//        $b = $a * $apr;
//        $c = $b * pow(1.03, $ytr);
//        $d = $c/pow(1.07,$ytr)-pow(1.03,$ytr)-0.04;
//
//        $initialContribution = $d/($ytr * 12);
//
//        return ceil($initialContribution);

        $mi = $income/12;

        $x = ($mi*$apr + pow(1.03,$ytr) + 0.04)/(($ytr*12)*pow(1.07,$ytr));

        return ceil($x);
    }

    public function calculateInitialAnnualIncome($annualIncome, $percentage) {
        return ceil($annualIncome * $percentage);
    }

    public function calculateMonthlyContribution($estTargetSavings, $totalAssets, $ytr) {
        $r = 1.07;
        return ($estTargetSavings - ($totalAssets * pow($r, $ytr))/pow($r, $ytr)/($ytr * 12));
    }

    public function createDeferralArray($data, $results) {
        $initialContribution = $results['results']['InitialContributionDlr'];

        $returnArray = array();
        $incomePerMonth = ceil($data['data']['RN_CurrentYearlyIncome']/12);
        $ytr = $data['data']['RN_NumberOfYearsBeforeRetirement'];
        $apr = $this->getApr($ytr);

        $i = 1;
        while($i<100 ) {
            $contributionAmount = ceil($incomePerMonth * ($i/100));
            $contributionNext = ceil($incomePerMonth * (($i + 1) / 100));
            if ($contributionAmount > $initialContribution && $i == 1) {
                $returnArray["$" . $initialContribution . "/mo"] = $this->_DeferralArrayHelper($initialContribution, $data, $ytr, $apr, round($initialContribution / $incomePerMonth * 100, 1), $results['results']['InitialAnnualIncome']/12);
            }
            $returnArray["$".$contributionAmount . "/mo"] = $this->_DeferralArrayHelper($contributionAmount, $data, $ytr, $apr, $i);
            if ($contributionAmount < $initialContribution && $contributionNext > $initialContribution) {
                $returnArray["$" . $initialContribution . "/mo"] = $this->_DeferralArrayHelper($initialContribution, $data, $ytr, $apr, round($initialContribution / $incomePerMonth * 100, 1), $results['results']['InitialAnnualIncome']/12);
            }
            $i++;
        }

        return $returnArray;
    }

    public function createDeferralArraySingle($contributionAmount) {
        $data = $this->retirementNeeds->getData();
        $ytr = $data['data']['RN_NumberOfYearsBeforeRetirement'];
        $apr = $this->getApr($ytr);
        $percent = round(($contributionAmount * 12) / $data['data']['RN_CurrentYearlyIncome'] * 100, 1);
        return $this->_DeferralArrayHelper($contributionAmount, $data, $ytr, $apr, $percent);
    }

    private function _DeferralArrayHelper($contributionAmount, $data, $ytr, $apr, $pct, $overrideMonthlyIncome = null){
        //$obj['ProjectedBalanceAtRetirement'] = $contributionAmount * pow(1.07, $ytr) * ($ytr*12);
        $obj['RawDlr'] = $contributionAmount;
        $obj['RawPct'] = $pct;
        $obj['ProjectedBalanceAtRetirement'] = $contributionAmount * (($ytr) * 12) * pow(1.07, $ytr) - pow(1.03, $ytr) - (0.07 - 0.03);
        $obj['MonthlyIncome'] = $this->getMonthlyIncomeFromProjectedBalance($obj['ProjectedBalanceAtRetirement'], $apr);
        if (!empty($overrideMonthlyIncome))
            $obj['MonthlyIncome'] = $overrideMonthlyIncome;
        $obj['PercentageOfRetirementIncomeFromPlan'] = $obj['ProjectedBalanceAtRetirement'] / $data['data']['RN_ReplacementIncome'];
        $obj['PercentageOfRetirementIncomeFromOutside'] = (($data['data']['RN_TotalAssets']* pow(1.06, $ytr))*$apr) / $data['data']['RN_ReplacementIncome'];
        $obj['ProjectedTotalRetirementSavings'] = $obj['ProjectedBalanceAtRetirement'] + ($data['data']['RN_TotalAssets']*(1.06*$ytr));
        $obj['ProjectedPercentageOfSavingsPlan'] = ($obj['ProjectedBalanceAtRetirement'] / (($contributionAmount * pow(1.07, $ytr)*($ytr*12) + ($data['data']['RN_TotalAssets'] * pow(1.06, $ytr)))));
        $obj['ProjectedPercentageOfSavingsAssets'] = $data['data']['RN_TotalAssets'] * pow(1.06,$ytr) / ($contributionAmount * pow(1.07, $ytr)*($ytr*12) + $data['data']['RN_TotalAssets'] * pow(1.06, $ytr));

        return $obj;
    }

    public function getMonthlyIncomeFromProjectedBalance($projectedBalance, $apr = null)
    {
        $data = $this->retirementNeeds->getData();
        if (is_null($apr) && isset($data['data']['RN_NumberOfYearsBeforeRetirement']))
        {
            $apr = $this->getApr($data['data']['RN_NumberOfYearsBeforeRetirement']);
        }
        if ($apr === null)
        {
            return 0;
        }
        return $projectedBalance / $apr;
    }

    public function getApr($ytr)
    {
        $age = $this->getAge();
        $retireYear = $age + (int)$ytr;
        $repository = $this->em->getRepository('classesclassBundle:Apr');
        $apr = $repository->findOneBy(array('year' => date("Y"), 'age' => $retireYear));
        if ($apr)
        {
            return $apr->apr;
        }
        return null;
    }


    public function getProjectedAnnualIncome() {
        $this->getData();
        $this->data['projectedAnnualIncome'] = intval(preg_replace('/[^\d.]/', '', $this->data['projectedMonthlyIncome'])) * 12;
    }

    public function calculateInflationAdjustedReplacementIncome($replacementIncome, $years) {
        $inflationFactor = [1.16, 1.34, 1.56, 1.81, 2.09, 2.43, 2.81, 3.26];
        $index = ceil($years/5) - 1;
        return ($replacementIncome * $inflationFactor[$index]);
    }

    public function calculateProjectedOutsideAssetVal($outsideAssets, $yearsBeforeRetirement) {
        $r = 0.06;
        return round($outsideAssets * pow((1+$r), $yearsBeforeRetirement), 2);
    }

}
