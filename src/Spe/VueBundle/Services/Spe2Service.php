<?php


namespace Spe\VueBundle\Services;


use classes\classBundle\Services\Entity\Profiles2Service;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Spe2Service
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var Profiles2Service
     */
    protected $profiles2Service;

    public function __construct(RequestStack $requestStack, Profiles2Service $profiles2Service)
    {
        $this->requestStack = $requestStack;
        $this->session = $requestStack->getCurrentRequest()->getSession();
        $this->profiles2Service = $profiles2Service;
    }

    public function createProfile()
    {
        $profileId = bin2hex(random_bytes(12));

        $this->session->set('spe2profileId', $profileId);

        $this->profiles2Service->add([
            'cookie' => $profileId,
            'planid' => $this->session->get('spe_plan_id'),
            'participantid' => $this->session->get('participantid'),
            'enrollmentStatus' => 0
        ]);
        $this->updateProfile();
    }

    public function updateProfile() {
        $profileId = $this->session->get('spe2profileId');

        $commonMap = [
            'firstName' => 'firstName',
            'lastName' => 'lastName',
            'email' => 'email',
            'currentBalance' => 'planBalance'
        ];

        $planMap = [
            'planName' => 'name',
            'planId' => 'planid'
        ];

        $retirementNeedsMap = [
            'currentYearlyIncome' => 'RN_CurrentYearlyIncome',
            'estimatedRetirementIncome' => 'RN_EstimatedRetirementIncome',
            'estimatedSavingsAtRetirement' => 'RN_EstimatedSavingsatRetirement',
            'estimatedSocialSecurityIncome' => 'RN_EstimatedSocialSecurityIncome',
            'otherAssets' => 'RN_OtherAssets',
            'totalAssets' => 'RN_TotalAssets',
            'yearsBeforeRetirement' => 'RN_NumberOfYearsBeforeRetirement',
            'inflationAdjustedReplacementIncome' => 'RN_InflationAdjustedReplacementIncome',
            'replacementIncome' => 'RN_ReplacementIncome',
            'estimatedPensionIncome' => 'RN_EstimatedPensionIncome',
            'annualIncomeDisplay' => 'annualIncomeDisplay',
            'retireAge' => 'RetirementAge',
            'ContributionDlr' => 'ContributionDlr',
            'ContributionPct' => 'ContributionPct'
        ];
        $riskProfileMap = [
            'riskXml' =>'RP_xml',
            'riskScore' => 'RP_Score',
            'riskThScore' => 'RP_THScore',
            'riskAnswers' => 'RP_Points',
            'riskPosition' => 'RP_InvestorType',
            'riskName' => 'RP_label',
            'riskDescription' => 'RP_desc',
            'recommendedPortfolioName' => 'recommendedPortfolioName',
            'riskProfileStatus' => 'riskProfileStatus'
        ];

        $contributionsMap = [
            'monthlyContributionTotal' => 'C_MonthlyContributionTotal',
            'monthlyContributionTotalEmployer' => 'C_MonthlyContributionTotalEmployer'
        ];

        $autosMap = [
            'autoIncreaseOptIn' => 'autoIncreaseOptIn',
            'autoIncreaseStartDate' => 'autoIncreaseStartDate',
            'autoIncreasePercentLimit' => 'autoIncreasePercentLimit',
            'autoIncreasePre' => 'autoIncreasePre',
            'autoIncreaseRoth' => 'autoIncreaseRoth',
            'autoIncreasePost' => 'autoIncreasePost',
            'autoEscalateOptOut' => 'autoEscalateOptOut',
            'autoEnrollOptOut' => 'autoEnrollOptOut',
            'autoIncreaseCap' => 'autoIncreaseCap'
        ];

        $autoEscalateMap = [
            'autoEscalatePre' => 'pre',
            'autoEscalatePost' => 'post',
            'autoEscalateRoth' => 'roth',
            'autoEscalateStartDate' => 'startDate'
        ];

        $common = $this->session->get('common');
        $plan = $this->session->get('plan');
        $retirement_needs = $this->session->get('retirement_needs');
        $risk_profile = $this->session->get('risk_profile');
        $contributions = $this->session->get('contributions');
        $autos = $this->session->get('autos');

        $appData = [];
        foreach ($commonMap as $key => $value) {
            $appData[$key] = $common[$value];
        }
        foreach ($planMap as $key => $value) {
            $appData[$key] = $plan[$value];
        }
        foreach ($retirementNeedsMap as $key => $value) {
            $appData[$key] = $retirement_needs[$value];
        }
        foreach ($riskProfileMap as $key => $value) {
            $appData[$key] = $risk_profile[$value];
        }
        foreach ($contributionsMap as $key => $value) {
            $appData[$key] = $contributions[$value];
        }
        foreach ($appData['assetTypes'] as $assetType) {
            $appData['assetType'][$assetType] = in_array($assetType, $risk_profile['RN_AssetTypes']);
        }
        foreach ($autosMap as $key => $value) {
            $appData[$key] = $autos[$value];
        }
        $appData['portfolioName'] = null;
        if ($common['autos']['autoEnroll'] && !$appData['autoEnrollOptOut']) {
            $appData['investmentType'] = "DEFAULT";
            $appData['investments'][] = [
                'fundName' => isset($common['autos']['defaultFundName']) ? $common['autos']['defaultFundName'] : null,
                'percent' => 100,
            ];
        }
        else {
            foreach ($this->session->get("savedFunds") as $fund) {
                $newFund = [
                    'fundName' => $fund['name'],
                    'percent' => $fund['percent'],
                    'prospectusLink' => $fund['prospectUrl'],
                    'fundFactLink' => $fund['customUrl']
                ];
                $appData['investments'][] = $newFund;
            }
            if ($this->session->get("investmentSection") == "RISK")
            {
                $appData['portfolioName'] = $this->session->get("investmentsSelectedPortfolio")['name'];
            }
            $appData['investmentType'] = $this->session->get("investmentSection");
        }

        $appData['pre'] = null;
        $appData['roth'] = null;
        $appData['post'] = null;
        $mode = $this->session->get('contributions')['contribution_mode'] == 'DEFERPCT' ? 'PCT' :
            ($this->session->get('contributions')['contribution_mode'] == 'DEFERDLR' ? 'DLR' : null);
        if($mode == 'PCT') {
            $appData['pre'] = $common['pre'] ? $contributions['C_PreTaxContributionPct'] : null;
            $appData['roth'] = $common['roth'] ? $contributions['C_RothContributionPct'] : null;
            $appData['post'] = $common['post'] ? $contributions['C_PostTaxContributionPct'] : null;
        }
        if ($mode == 'DLR') {
            $appData['pre'] = $common['pre'] ? $contributions['C_PreTaxContributionValue'] : null;
            $appData['roth'] = $common['roth'] ? $contributions['C_RothContributionValue'] : null;
            $appData['post'] = $common['post'] ? $contributions['C_PostTaxContributionValue'] : null;
        }
        $appData['mode'] = $mode;


        foreach ($autoEscalateMap as $key => $value) {
            $appData[$key] =  $common['autos']['autoEscalateData'][$value];
        }
        $appData['autoEnrollEnabled'] = $common['autos']['autoEnroll'];
        $appData['autoEscalateEnabled'] = $common['autos']['autoEscalate'];
        $appData['autoIncreaseEnabled'] = $common['autos']['autoIncrease'];
        $appData['autoIncreasePercentLimit'] = $common['autos']['autoIncreaseData']['percentLimit'];

        $appData['beneficiaries'] = [];
        foreach ($this->session->get("beneficiaryList") as $b) {
            $appData['beneficiaries'][] = [
                "type" => isset($b['type']) ? $b['type'] : "",
                "firstName" => isset($b['firstName']) ? $b['firstName'] : "",
                "lastName" => isset($b['lastName']) ? $b['lastName'] : "",
                "gender" => isset($b['gender']) ? $b['gender'] : "",
                "relationship" => isset($b['relationship']) ? $b['relationship'] : "",
                "maritalStatus" => isset($b['maritalStatus']) ? $b['maritalStatus'] : "",
                "ssn" => isset($b['ssn']) ? $b['ssn'] : "",
                "city" => isset($b['city']) ? $b['city'] : "",
                "state" => isset($b['state']) ? $b['state'] : "",
                "zip" => isset($b['zip']) ? $b['zip'] : "",
                "dob" => isset($b['dob']) ? $b['dob'] : "",
                "address1" => isset($b['address1']) ? $b['address1'] : "",
                "address2" => isset($b['address2']) ? $b['address2'] : "",
                "phone" => isset($b['phone']) ? $b['phone'] : "",
                "level" => isset($b['level']) ? $b['level'] : "",
                "country" => isset($b['country']) ? $b['country'] : "",
                "middleInitial" => isset($b['middleInitial']) ? strtoupper($b['middleInitial']) : "",
                "percent" => isset($b['percent']) ? floatval($b['percent']) : "",
            ];
        }

        $this->profiles2Service->edit(["cookie" => $profileId], ['appData' => $appData,]);
    }

}