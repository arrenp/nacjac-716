<?php

namespace Spe\VueBundle\Services;

use classes\classBundle\Services\BaseService;
use classes\classBundle\Entity\profilesContributions;
use Spe\VueBundle\Services\RetirementNeedsCalcsService;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ContributionsService extends \Spe\AppBundle\Services\ContributionsService
{
    /** @var RetirementNeedsCalcsService */
    protected $retirementNeedsCalcs;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->retirementNeedsCalcs = $container->get('RetirementNeedsCalcs');
    }

    public function calculate($params=[]) {
        $params = parent::calculate($params);
        // SPE2 New Calculations
        $calculation = $this->retirementNeedsCalcs->createDeferralArraySingle($params['estimatedMonthlyContribution']);
        $params['projectedMonthlyEmployee'] = max($calculation['MonthlyIncome'], 0);
        $params['projectedSavings'] = $this->retirementNeedsCalcs->calculateProjectedOutsideAssetVal($this->totalAssets(), $this->numberOfYearsBeforeRetirement());
        $params['projectedSavings'] += $calculation['ProjectedBalanceAtRetirement'];

        $calculation = $this->retirementNeedsCalcs->createDeferralArraySingle($params['matchingAmountMonthly']);
        $params['projectedMonthlyMatching'] = max($calculation['MonthlyIncome'], 0);
        $params['matchingAmount'] = $calculation['ProjectedBalanceAtRetirement'];

        $params['projectedMonthlyTotal'] = $params['projectedMonthlyEmployee'] + $params['projectedMonthlyMatching'];
        $params['totalSavings'] = $params['projectedSavings'] + $params['matchingAmount'];

        $params['projectedMonthlyTotalDisplay'] = number_format($params['projectedMonthlyTotal'], 2);
        $params['totalSavingsDisplay'] = number_format($params['totalSavings'], 2);

        $params['targetSavingsDisplay'] = number_format($params['targetSavings'], 2);
        $params['targetMonthlyIncome'] = $this->targetMonthlyIncome();
        $params['targetMonthlyIncomeDisplay'] = number_format($params['targetMonthlyIncome'], 2);

        $params['monthlyContributionTotal'] = $params['estimatedMonthlyContribution'] + $params['matchingAmountMonthly'];
        $params['monthlyContributionTotalDisplay'] = number_format($params['monthlyContributionTotal'], 2);
        return $params;

    }

    public function targetMonthlyIncome()
    {
        return (float)$this->session->get("retirement_needs")['RN_EstimatedMonthlyIncomeAtRetirement'];
    }
}