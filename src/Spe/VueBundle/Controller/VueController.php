<?php

namespace Spe\VueBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use classes\classBundle\Entity\profilesInvestments;

/**
 * Vue controller.
 * @Route("/", name="vue")
 */
class VueController extends Controller {

    /**
     * @Route("/spe2/profile/report/{profileId}", name="profileReportSpe2")
     * @param $profileId
     * @return Response
     */
    public function profileReportAction($profileId) {
        return $this->render("SpeVueBundle:Vue:report.html.twig", ['profileId' => $profileId]);
    }

    /**
     * @Route("/spe2/style", name="Spe2Style")
     * @param Request $request
     * @return Response
     */
    public function styleAction(Request $request)
    {
        $spe_plan_id = $request->getSession()->get('spe_plan_id');
        $colors = $this->get("PlanAppColorsService")->getColorsByPlan($spe_plan_id);
        $response = new Response();
        $response->headers->set('Content-Type', 'text/css');
        return $this->render('SpeVueBundle:Vue:style.css.twig', ["colors" => $colors], $response);
    }

    /**
     * @Route("/{productCode}/", name="_index")
     * @Route("/{productCode}/{moduleCode}/")
     * @Route("/{productCode}/{moduleCode}/{stepName}/")
     * @Route("/{productCode}/{moduleCode}/{stepName}/{id}")
     * @Template("SpeVueBundle:Vue:app.html.twig")
     */
    public function indexAction(Request $request, $productCode, $moduleCode=null, $stepName=null) {
        $appVersion = $this->getDoctrine()->getRepository('classesclassBundle:plans')->findOneBy(array('planid' => $_REQUEST['id'], 'partnerid' => $_REQUEST['partner_id']))->appVersion;
        if ($appVersion === 1) {
            $params = $request->query->all();
            $params['app_domains'] = $request->getHost();
            return $this->redirectToRoute('loading', $params);
        }
        $data = array("request" => $_REQUEST);
        $data['productCode'] = $productCode;
        $data['moduleCode'] = $moduleCode;
        $data['stepName'] = $stepName;
        return $data;
    }
}
